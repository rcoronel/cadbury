<?php
/*
|--------------------------------------------------------------------------
| Aruba API Class
|--------------------------------------------------------------------------
|
| Handles the API calls to the Access Controller that is handled by Aruba
|
| @category		Extensions
| @author		Ryan Coronel
*/
class Link
{
	public function addUser($url, $key, $ip, $mac, $role)
	{
		$xml = '<aruba command="user_add">
		  <ipaddr>' . $ip . '</ipaddr>
		  <macaddr>' . $mac . '</macaddr>
		  <role>' . $role . '</role>
		  <key>' . $key . '</key>
		  <authentication>cleartext</authentication>
		  <version>1.0</version>
		</aruba>';

		return self::api($url, $xml);
	}


	public function queryUser($url, $key, $ip, $mac)
	{
		$xml = '<aruba command="user_query">
		  <ipaddr>' . $ip . '</ipaddr>
		  <macaddr>' . $mac . '</macaddr>
		  <key>' . $key . '</key>
		  <authentication>cleartext</authentication>
		  <version>1.0</version>
		</aruba>';

		return self::api($url, $xml);
	}


	public function deleteUser($url, $key, $ip, $mac)
	{
		$xml = '<aruba command="user_delete">
		  <ipaddr>' . $ip . '</ipaddr>
		  <macaddr>' . $mac . '</macaddr>
		  <key>' . $key . '</key>
		  <authentication>cleartext</authentication>
		  <version>1.0</version>
		</aruba>';

		return self::api($url, $xml);
	}

	public function blacklistUser($url, $key, $ip, $mac)
	{
		$xml = '<aruba command="user_blacklist">
		  <ipaddr>' . $ip . '</ipaddr>
		  <macaddr>' . $mac . '</macaddr>
		  <key>' . $key . '</key>
		  <authentication>cleartext</authentication>
		  <version>1.0</version>
		</aruba>';

		return self::api($url, $xml);
	}

	/**
	 * 
	 * @param type $url
	 * @param type $key = hello
	 * @param type $ip
	 * @param type $mac
	 * @param type $name
	 * @param type $password
	 * @return type
	 */
	public function authenticateUser($url, $key, $ip, $mac, $name, $password)
	{
		$xml = '<aruba command="user_authenticate">
		  <ipaddr>' . $ip . '</ipaddr>
		  <macaddr>' . $mac . '</macaddr>
		  <name>' . $name . '</name>
		  <password>' . $password . '</password>
		  <key>' . $key . '</key>
		  <authentication>cleartext</authentication>
		  <version>1.0</version>
		</aruba>';

		return self::api($url, $xml);
	}

	private function api($url, $xml)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT , 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "xml=$xml");
		$output = curl_exec($ch);
		curl_close($ch);
		return $output;
	}
}
if($_POST) {
	echo "Aruba Call: <br />";
	$url = "https://124.6.162.170/auth/command.xml";
	$act = $_REQUEST['action'];
	$key = $_REQUEST['key'];
	$ip = $_REQUEST['ip'];
	$name = $_REQUEST['name'];
	$password = $_REQUEST['password'];
	$l = new Link();

	echo '<pre>';
	print_r($_REQUEST);
	echo '</pre>';
	switch ($act) {
		case 'authenticate':
			$r = $l->authenticateUser($url, $key, $ip, $mac, $name, $password);
			break;
		case 'delete user':
			$r = $l->deleteUser($url, $key, $ip, $mac);
			break;
		default:
			break;
	}

	echo '<pre>';
	echo $r;
	echo '</pre>';
}


?>

<form action="aruba_api.php" method="post" >
	IP: <input type="text" name="ip" value="<?php echo $_REQUEST['ip']?>"/> <br />
	MAC: <input type="text" name="mac" value="<?php echo $_REQUEST['mac']?>"/><br />
	Key: <input type="text" name="key" value="<?php echo $_REQUEST['key']?>"/><br />

	Name: <input type="text" name="name" value="<?php echo $_REQUEST['name']?>"/><br />
	Password: <input type="text" name="password" value="<?php echo $_REQUEST['password']?>"/><br />

	<input type="submit" name="action" value="authenticate" />
	<input type="submit" name="action" value="delete user" />
</form>







