<?php

/**
 * This is the model class for table "aruba_log".
 *
 * The followings are the available columns in table 'aruba_log':
 * @property integer $Aruba_log_ID
 * @property integer $Device_ID
 * @property integer $Status
 * @property string $Url
 * @property string $Request
 * @property string $Response
 * @property string $Created_at
 */
class ArubaLog extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'aruba_log_'.date('Y-m-d');
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Status, Url, Request, Created_at', 'required'),
			array('Nas_ID, Device_ID, Status', 'numerical', 'integerOnly'=>true),
			array('Url', 'length', 'max'=>128),
			array('Created_at', 'date', 'format'=>'yyyy-MM-dd hh:mm:ss'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Aruba_log_ID' => 'ID',
			'Device_ID' => 'Device',
			'Status' => 'Status',
			'Url' => 'URL',
			'Request' => 'Request',
			'Response' => 'Response',
			'Created_at' => 'Date Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Aruba_log_ID',$this->Aruba_log_ID);
		$criteria->compare('Device_ID',$this->Device_ID);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('Url',$this->Url,true);
		$criteria->compare('Request',$this->Request,true);
		$criteria->compare('Response',$this->Response,true);
		$criteria->compare('Created_at',$this->Created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ArubaLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Initialize new database connection
	 * 
	 * @return type
	 */
	public function getDbConnection()
	{
		return Yii::app()->getComponent('db_cp');
	}
	
	public static function createTable($tablename)
	{
		Yii::app()->db_cp->createCommand()->createTable($tablename,
			array(	'Aruba_log_ID' => 'pk',
					'Nas_ID' => "int(11) NOT NULL COMMENT 'NAS'",
					'Device_ID' => "int(11) NOT NULL COMMENT 'Device'",
					'Status' => "int(11) NOT NULL COMMENT 'Status'",
					'Url' => "varchar(128) NOT NULL COMMENT 'URL'",
					'Request' => "text NOT NULL COMMENT 'Request'",                    
					'Response' => "text NOT NULL COMMENT 'Response'",                  
					'Created_at' => "datetime NOT NULL COMMENT 'Date Added'"),
			'ENGINE=InnoDB DEFAULT CHARSET=utf8'
			);
	}
	
	public static function pollSuccess($date, $nas_id)
	{
		$where = "Status = 0";
		if ($nas_id) {
			$where .= " AND Nas_ID = {$nas_id}"; 
		}
		$count = Yii::app()->db_cp->createCommand()
			->select('COUNT(Status) as count')
			->from("aruba_log_{$date} al")
			->where($where)
			->queryRow();
		
			return (int)$count['count'];
	}
	
	public static function pollFail($date, $nas_id)
	{
		$where = "Status <> 0";
		if ($nas_id) {
			$where .= " AND Nas_ID = {$nas_id}"; 
		}
		$count = Yii::app()->db_cp->createCommand()
			->select('COUNT(Status) as count')
			->from("aruba_log_{$date} al")
			->where($where)
			->queryRow();
		
			return (int)$count['count'];
	}
}
