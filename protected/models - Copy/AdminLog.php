<?php

/**
 * This is the model class for table "admin_log".
 *
 * The followings are the available columns in table 'admin_log':
 * @property integer $Admin_log_ID
 * @property integer $Admin_ID
 * @property string $Classname
 * @property string $Action
 * @property integer $Reference
 * @property string $Created_at
 */
class AdminLog extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'admin_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Admin_ID, Classname, Action, Reference, Created_at', 'required'),
			array('Admin_ID, Reference', 'numerical', 'integerOnly'=>true),
			array('Classname', 'length', 'max'=>32),
			array('Action', 'length', 'max'=>16),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Admin_log_ID, Admin_ID, Classname, Action, Reference, Created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Admin_log_ID' => 'ID',
			'Admin_ID' => 'Admin',
			'Classname' => 'Classname',
			'Action' => 'Action',
			'Reference' => 'Reference',
			'Created_at' => 'Date Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Admin_log_ID',$this->Admin_log_ID);
		$criteria->compare('Admin_ID',$this->Admin_ID);
		$criteria->compare('Classname',$this->Classname,true);
		$criteria->compare('Action',$this->Action,true);
		$criteria->compare('Reference',$this->Reference);
		$criteria->compare('Created_at',$this->Created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AdminLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
