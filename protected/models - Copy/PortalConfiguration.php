<?php

/**
 * This is the model class for table "portal_configuration".
 *
 * The followings are the available columns in table 'portal_configuration':
 * @property integer $Portal_ID
 * @property string $Config
 * @property string $Value
 * @property string $Created_at
 */
class PortalConfiguration extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'portal_configuration';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Portal_ID, Config, Created_at', 'required'),
			array('Portal_ID', 'numerical', 'integerOnly'=>true),
			array('Config, Value', 'length', 'max'=>128),
			array('Created_at', 'date', 'format'=>'yyyy-MM-dd hh:mm:ss'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Portal_ID' => 'NAS',
			'Config' => 'Config',
			'Value' => 'Value',
			'Created_at' => 'Date Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Portal_ID',$this->Portal_ID);
		$criteria->compare('Config',$this->Config,true);
		$criteria->compare('Value',$this->Value,true);
		$criteria->compare('Created_at',$this->Created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cms;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PortalConfiguration the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Get Configuration Value
	 * 
	 * @access	public
	 * @param string $config Configuration Value to be search
	 * @param mixed $default Default Value to be set
	 * @return string
	 */
	public static function getValue($config = '', $default = '')
	{
		$portal_id = 0;
		$context = Yii::app()->controller->context;
		if ($context->portal) {
			$portal_id = $context->portal->portal_id;
		}
		else {
			$portal_id = Yii::app()->controller->portal->portal_id;
		}
		$conf = PortalConfiguration::model()->findByAttributes(array('Config' => $config, 'Portal_ID'=>$portal_id));
		
		if (empty($conf)) {
			return $default;
		}
		return $conf->Value;
	}
	
	/**
	 * Set Configuration Value
	 * 
	 * @access	public
	 * @param string $config Configuration Value
	 * @param mixed $value
	 * @return string
	 */
	public static function setValue($config, $value = '')
	{
		$context = Context::getContext();
		
		if ($context->portal) {
			$portal_id = $context->portal->portal_id;
		}
		else {
			$portal_id = Yii::app()->controller->portal->portal_id;
		}
		
		echo "<pre>";
		print_r($portal_id);
		die();
		
		// Delete previous Config
		PortalConfiguration::model()->deleteAllByAttributes(array('portal_id'=>$portal_id, 'config'=>$config));
		
		// Insert
		$object = new PortalConfiguration;
		$object->Portal_ID = $context->portal->Portal_ID;
		$object->Config = $config;
		$object->Value = $value;
		$object->validate() && $object->save();
		
		return TRUE;
	}
}
