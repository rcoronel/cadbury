<?php

/**
 * This is the model class for table "admin_role".
 *
 * The followings are the available columns in table 'admin_role':
 * @property integer $Admin_role_ID
 * @property string $Role
 * @property int $Is_active
 * @property string $Created_at
 * @property string $Updated_at
 */
class AdminRole extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'admin_role';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Role, Is_active, Created_at, Updated_at', 'required'),
			array('Role', 'length', 'max'=>16),
			array('Role', 'unique'),
			array('Admin_role_ID, Is_active', 'numerical', 'integerOnly'=>true),
			array('Created_at, Updated_at', 'date', 'format'=>'yyyy-MM-dd hh:mm:ss'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Admin_role_ID' => 'ID',
			'Role' => 'Role',
			'Is_active'=>'Status',
			'Created_at' => 'Date Added',
			'Updated_at' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Admin_role_ID',$this->Admin_role_ID);
		$criteria->compare('Role',$this->Role,true);
		$criteria->compare('Created_at',$this->Created_at,true);
		$criteria->compare('Updated_at',$this->Updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AdminRole the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
