<?php

/**
 * This is the model class for table "Scenario".
 *
 * The followings are the available columns in table 'Scenario':
 * @property integer $Scenario_ID
 * @property integer $Portal_ID
 * @property string $Title
 * @property string $Created_at
 * @property string $Updated_at
 */
class Scenario extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'scenario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Title, Portal_ID', 'required'),
			array('Portal_ID', 'numerical', 'integerOnly'=>true),
			array('Title', 'length', 'max'=>64),
			array('Created_at, Updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Scenario_ID, Portal_ID, Title, Created_at, Updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'new'=>array(self::HAS_MANY, 'ScenarioAvailment', array('Scenario_ID'=>'Scenario_ID')),
			'returning'=>array(self::HAS_MANY, 'ScenarioReturning', array('Scenario_ID'=>'Scenario_ID'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Scenario_ID' => 'ID',
			'Portal_ID' => 'NAS',
			'Title' => 'Title',
			'Created_at' => 'Date Added',
			'Updated_at' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Scenario_ID',$this->Scenario_ID);
		$criteria->compare('Portal_ID',$this->Portal_ID);
		$criteria->compare('Title',$this->Title,true);
		$criteria->compare('Created_at',$this->Created_at,true);
		$criteria->compare('Updated_at',$this->Updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cms;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Scenario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
