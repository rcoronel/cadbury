<?php

/**
 * This is the model class for table "nas".
 *
 * The followings are the available columns in table 'nas':
 * @property int $id
 * @property string $nasname
 * @property string $switchip
 * @property string $shortname
 * @property string $type
 * @property int $ports
 * @property string $secret
 * @property string $server
 * @property string $community
 * @property string $description
 */
class RadiusNas extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'nas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('nasname, switchip, shortname, brand', 'required'),
			array('nasname, switchip', 'length', 'max'=>128),
			array('shortname', 'length', 'max'=>32),
			array('type', 'length', 'max'=>30),
			array('id, ports', 'numerical', 'integerOnly'=>true),
			array('secret', 'length', 'max'=>60),
			array('server', 'length', 'max'=>64),
			array('community', 'length', 'max'=>50),
			array('description', 'length', 'max'=>200),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'=>'ID',
			'nasname'=>'Private IP',
			'switchip'=>'Public IP',
			'shortname'=>'Short name',
			'type'=>'Type',
			'ports'=>'Ports',
			'secret'=>'Secret Key',
			'server'=>'Server',
			'community'=>'Community',
			'description'=>'Description',
			'brand'=>'Brand'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('nasname', $this->nasname);
		$criteria->compare('switchip', $this->switchip);
		$criteria->compare('shortname', $this->shortname);
		$criteria->compare('type', $this->type);
		$criteria->compare('ports', $this->ports, true);
		$criteria->compare('server', $this->server);
		$criteria->compare('community', $this->community, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('brand', $this->brand);
		$criteria->order = 'shortname ASC';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Configuration the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Initialize new database connection
	 * 
	 * @return type
	 */
	public function getDbConnection()
	{
		return Yii::app()->getComponent('db_radius');
	}
}
