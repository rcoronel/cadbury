<?php

/**
 * This is the model class for table "tbl_avail_tattoo".
 *
 * The followings are the available columns in table 'tbl_avail_tattoo':
 * @property integer $Tattoo_subscriber_ID
 * @property integer $Plan
 * @property string $Landline
 * @property string $Name
 * @property string $Account_no
 * @property string $Created_at
 */
class TattooSubs extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tattoo_subscriber';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Landline, Created_at', 'required'),
			array('Landline', 'length', 'max'=>32),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Tattoo_subscriber_ID, Landline, Plan, Name, Created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Tattoo_subscriber_ID' => 'ID',
			'Plan' => 'Plan',
			'Landline' => 'Landline',
			'Name' => 'Name',
			'Account_no' => 'Account Number',
			'Created_at' => 'Date Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Tattoo_subscriber_ID',$this->Tattoo_subscriber_ID);
		$criteria->compare('Plan',$this->Plan);
		$criteria->compare('Landline',$this->Landline,true);
		$criteria->compare('Name',$this->Name,true);
		$criteria->compare('Account_no',$this->Account_no,true);
		$criteria->compare('Created_at',$this->Created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AvailTattoo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Initialize new database connection
	 * 
	 * @return type
	 */
	public function getDbConnection()
	{
		return Yii::app()->getComponent('db');
	}
	
	
}
