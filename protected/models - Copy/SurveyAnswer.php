<?php

/**
 * This is the model class for table "survey_answer".
 *
 * The followings are the available columns in table 'survey_answer':
 * @property integer $Survey_answer_ID
 * @property integer $Survey_question_ID
 * @property string $Answer
 * @property string $Answer_type
 * @property integer $Position
 * @property string $Created_at
 * @property string $Updated_at
 */
class SurveyAnswer extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'survey_answer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Survey_question_ID, Answer_type, Created_at, Updated_at', 'required'),
			array('Survey_question_ID, Position', 'numerical', 'integerOnly'=>true),
			array('Answer', 'length', 'max'=>64),
			array('Answer_type', 'length', 'max'=>16),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Survey_answer_ID, Survey_question_ID, Answer, Answer_type, Position, Created_at, Updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
//s                        'question'=>array(self::BELONGS_TO, 'SurveyQuestion', 'Survey_question_ID', 'together'=>TRUE),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Survey_answer_ID' => 'ID',
			'Survey_question_ID' => 'Question ID',
			'Answer' => 'Answer',
			'Answer_type' => 'Type',
			'Position' => 'Position',
			'Created_at' => 'Date Added',
			'Updated_at' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Survey_answer_ID',$this->Survey_answer_ID);
		$criteria->compare('Survey_question_ID',$this->Survey_question_ID);
		$criteria->compare('Answer',$this->Answer,true);
		$criteria->compare('Answer_type',$this->Answer_type,true);
		$criteria->compare('Position',$this->Position);
		$criteria->compare('Created_at',$this->Created_at,true);
		$criteria->compare('Updated_at',$this->Updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SurveyAnswer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        /**
	 * Initialize new database connection
	 * 
	 * @return type
	 */
        public function getDbConnection()
        {
            return Yii::app()->getComponent('db_cms');
        }
}
