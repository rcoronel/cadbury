<?php

/**
 * This is the model class for table "device_connection".
 *
 * The followings are the available columns in table 'device_connection':
 * @property integer $Device_connection_ID
 * @property integer $Nas_ID
 * @property integer $Site_ID
 * @property integer $Portal_ID
 * @property integer $Access_point_group_ID
 * @property integer $Access_point_ID
 * @property integer $Device_ID
 * @property integer $Ip_address
 * @property string $Username
 * @property string $Password
 * @property string $Connection_token
 * @property integer $Level
 * @property string $Created_at
 * @property string $Updated_at
 */
class DeviceConnection extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'device_connection_'.date('Y-m-d');
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Nas_ID, Site_ID, Portal_ID, Access_point_group_ID, Access_point_ID, Device_ID, Ip_address, Connection_token, Created_at', 'required'),
			array('Nas_ID, Site_ID, Portal_ID, Access_point_group_ID, Access_point_ID, Device_ID, Level', 'numerical', 'integerOnly'=>true),
			array('Username, Password, Connection_token', 'length', 'max'=>256),
			array('Created_at, Updated_at', 'date', 'format'=>'yyyy-MM-dd hh:mm:ss'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'device'=>array(self::BELONGS_TO, 'Device', array('Device_ID'=>'Device_ID'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Device_connection_ID' => 'ID',
			'Device_ID' => 'Device',
			'Ip_address' => 'IP Address',
			'Connection_token'=>'Token',
			'Created_at' => 'Date Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Device_connection_ID',$this->Device_connection_ID);
		$criteria->compare('Device_ID',$this->Device_ID);
		$criteria->compare('Ip_address',$this->Ip_address);
		$criteria->compare('Created_at',$this->Created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DeviceConnection the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Initialize new database connection
	 * 
	 * @return type
	 */
	public function getDbConnection()
	{
		return Yii::app()->getComponent('db_cp');
	}
	
	public static function createTable($tablename)
	{
		Yii::app()->db_cp->createCommand()->createTable($tablename,
			array(	'Device_connection_ID' => 'pk',
					'Nas_ID' => "int(11) NOT NULL COMMENT 'NAS'",
					'Device_ID' => "int(11) NOT NULL COMMENT 'Device'",
					'Location_ID' => "int(11) NOT NULL COMMENT 'Location'",
					'Ip_address' => "varchar(32) NOT NULL COMMENT 'IP Address'",
					'Username' => "varchar(128) NOT NULL COMMENT 'Username'",
					'Password' => "varchar(128) NOT NULL COMMENT 'Password'",
					'Connection_token' => "varchar(128) NOT NULL COMMENT 'Token'",
					'Level' => "int(11) NOT NULL COMMENT 'Level'",
					'Created_at' => "datetime NOT NULL COMMENT 'Date Added'",
					'Updated_at' => "datetime NOT NULL COMMENT 'Date Updated'"),
			'ENGINE=InnoDB DEFAULT CHARSET=utf8'
			);
	}
	
	public static function poll($date, $nas_id)
	{
		$count = Yii::app()->db_cp->createCommand()
			->select('COUNT(*) as count')
			->from('device_connection_'.$date)
			->where("Nas_ID = {$nas_id}")
			->queryRow();
		
			return (int)$count['count'];
	}
	
	/**
	 * Get count of all unique users
	 * 
	 * @param date $date
	 * @param int $nas_id
	 * @return int
	 */
	public static function pollAll($date, $nas_id)
	{
		$where = 1;
		if ($nas_id) {
			$where = "Nas_ID = {$nas_id}"; 
		}
		$count = Yii::app()->db_cp->createCommand()
			->select('COUNT(DISTINCT d.Mac_address, dc.Nas_ID) as count')
			->from("device_connection_{$date} dc")
			->join('device d', 'd.Device_ID = dc.Device_ID')
			->where($where)
			->queryRow();
		
			return (int)$count['count'];
	}
	
	/**
	 * Get count of new unique users
	 * 
	 * @param date $date
	 * @param int $nas_id
	 * @return int
	 */
	public static function pollAllNew($date, $nas_id)
	{
		$where = "DATE(d.Created_at) = '{$date}'";
		if ($nas_id) {
			$where .= " AND Nas_ID = {$nas_id}"; 
		}
		$count = Yii::app()->db_cp->createCommand()
			->select('COUNT(DISTINCT d.Mac_address, dc.Nas_ID) as count')
			->from("device_connection_{$date} dc")
			->join('device d', 'd.Device_ID = dc.Device_ID')
			->where($where)
			->queryRow();
		
			return (int)$count['count'];
	}
	
	/**
	 * Get count of returning unique users
	 * 
	 * @param date $date
	 * @param int $nas_id
	 * @return int
	 */
	public static function pollAllReturning($date, $nas_id)
	{
		$where = "DATE(d.Created_at) < '{$date}'";
		if ($nas_id) {
			$where .= " AND Nas_ID = {$nas_id}"; 
		}
		$count = Yii::app()->db_cp->createCommand()
			->select('COUNT(DISTINCT d.Mac_address, dc.Nas_ID) as count')
			->from("device_connection_{$date} dc")
			->join('device d', 'd.Device_ID = dc.Device_ID')
			->where($where)
			->queryRow();
		
			return (int)$count['count'];
	}
	
	/**
	 * Get count of all authenticated users
	 * 
	 * @param date $date
	 * @param int $nas_id
	 * @return int
	 */
	public static function pollAuthAll($date, $nas_id)
	{
		$where = "Username != '' AND Password != ''";
		if ($nas_id) {
			$where .= " AND Nas_ID = {$nas_id}"; 
		}
		$count = Yii::app()->db_cp->createCommand()
			->select('COUNT(DISTINCT d.Mac_address, dc.Nas_ID) as count')
			->from("device_connection_{$date} dc")
			->join('device d', 'd.Device_ID = dc.Device_ID')
			->where($where)
			->queryRow();
		
			return (int)$count['count'];
	}
	
	/**
	 * Get count of all new authenticated users
	 * 
	 * @param date $date
	 * @param int $nas_id
	 * @return int
	 */
	public static function pollAuthNew($date, $nas_id)
	{
		$where = "DATE(d.Created_at) = '{$date}' AND Username != '' AND Password != ''";
		if ($nas_id) {
			$where .= " AND Nas_ID = {$nas_id}"; 
		}
		$count = Yii::app()->db_cp->createCommand()
			->select('COUNT(DISTINCT d.Mac_address, dc.Nas_ID) as count')
			->from("device_connection_{$date} dc")
			->join('device d', 'd.Device_ID = dc.Device_ID')
			->where($where)
			->queryRow();
		
			return (int)$count['count'];
	}
	
	/**
	 * Get count of all returning authenticated users
	 * 
	 * @param date $date
	 * @param int $nas_id
	 * @return int
	 */
	public static function pollAuthReturning($date, $nas_id)
	{
		$where = "DATE(d.Created_at) < '{$date}' AND Username != '' AND Password != ''";
		if ($nas_id) {
			$where .= " AND Nas_ID = {$nas_id}"; 
		}
		$count = Yii::app()->db_cp->createCommand()
			->select('COUNT(DISTINCT d.Mac_address, dc.Nas_ID) as count')
			->from("device_connection_{$date} dc")
			->join('device d', 'd.Device_ID = dc.Device_ID')
			->where($where)
			->queryRow();
		
			return (int)$count['count'];
	}
}
