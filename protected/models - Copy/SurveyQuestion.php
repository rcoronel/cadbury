<?php

/**
 * This is the model class for table "survey_question".
 *
 * The followings are the available columns in table 'survey_question':
 * @property integer $Survey_question_ID
 * @property integer $Survey_ID
 * @property string $Question
 * @property integer $Position
 * @property integer $Is_required
 * @property string $Created_at
 * @property string $Updated_at
 */
class SurveyQuestion extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
        public $answer_count;
	public function tableName()
	{
		return 'Cadbury_CMS.survey_question';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Survey_ID, Question, Is_required, Created_at, Updated_at', 'required'),
			array('answer_count, Survey_ID, Position, Is_required', 'numerical', 'integerOnly'=>true),
			array('Question', 'length', 'max'=>256),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('answer_count, Survey_question_ID, Survey_ID, Question, Position, Is_required, Created_at, Updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
//                    'survey'=>array(self::BELONGS_TO, 'Survey', 'Survey_ID', 'together'=>TRUE),
//                    'answer'=>array(self::HAS_MANY, 'SurveyAnswer', 'Survey_question_ID', 'together'=>TRUE),
                );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Survey_question_ID' => 'ID',
			'Survey_ID' => 'Survey ID',
			'Question' => 'Question',
			'Position' => 'Position',
			'Is_required' => 'Required',
			'Created_at' => 'Date Added',
			'Updated_at' => 'Date Updated',
			'answer_count' => 'Number of Answers',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Survey_question_ID',$this->Survey_question_ID);
		$criteria->compare('Survey_ID',$this->Survey_ID);
		$criteria->compare('Question',$this->Question,true);
		$criteria->compare('Position',$this->Position);
		$criteria->compare('Is_required',$this->Is_required);
		$criteria->compare('Created_at',$this->Created_at,true);
		$criteria->compare('Updated_at',$this->Updated_at,true);
		$criteria->compare("(SELECT COUNT(*) FROM survey_answer WHERE t.Survey_question_ID = Survey_question_ID)",$this->answer_count);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SurveyQuestion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        /**
	 * Initialize new database connection
	 * 
	 * @return type
	 */
        public function getDbConnection()
        {
            return Yii::app()->getComponent('db_cms');
        }
}
