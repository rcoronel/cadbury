<?php

/**
 * This is the model class for table "tattoo_subscriber".
 *
 * The followings are the available columns in table 'tattoo_subscriber':
 * @property integer $Tattoo_subscriber_ID
 * @property string $Plan
 * @property string $Name
 * @property string $Account_no
 * @property integer $Network_service_ID
 * @property string $Service_type
 * @property string $Description
 * @property string $Created_at
 */
class TattooSubscriber extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tattoo_subscriber';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Plan, Account_no', 'required', 'on'=>'login'),
			array('Plan, Account_no, Name, Service_type, Created_at', 'required', 'on'=>'insert'),
			array('Plan, Network_service_ID', 'numerical', 'integerOnly'=>true),
			array('Name', 'length', 'max'=>256),
			array('Account_no', 'length', 'max'=>16),
			array('Service_type', 'length', 'max'=>2),
			array('Description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Tattoo_subscriber_ID, Plan, Name, Account_no, Network_service_ID, Service_type, Description, Created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Tattoo_subscriber_ID' => 'ID',
			'Plan' => 'Plan',
			'Name' => 'Name',
			'Account_no' => 'Account No.',
			'Network_service_ID' => 'Network Service ID',
			'Service_type' => 'Service Type',
			'Description' => 'Description',
			'Created_at' => 'Date Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Tattoo_subscriber_ID',$this->Tattoo_subscriber_ID);
		$criteria->compare('Plan',$this->Plan,true);
		$criteria->compare('Name',$this->Name,true);
		$criteria->compare('Account_no',$this->Account_no,true);
		$criteria->compare('Network_service_ID',$this->Network_service_ID);
		$criteria->compare('Service_type',$this->Service_type,true);
		$criteria->compare('Description',$this->Description,true);
		$criteria->compare('Created_at',$this->Created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TattooSubscriber the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
