<?php

/**
 * This is the model class for table "availment".
 *
 * The followings are the available columns in table 'availment':
 * @property integer $Availment_ID
 * @property string $Name
 * @property string $Code
 * @property string $Description
 * @property integer $Is_active
 * @property string $Created_at
 * @property string $Updated_at
 */
class Plan extends ActiveRecord
{
	public $plan_id;
	public $telco_id;
	public $brand_name;
	public $plan_type;
	public $is_default_plan;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Cadbury_Captive.plan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			// array('Name, Code, Model, Is_active, Created_at, Updated_at', 'required'),
			// array('Code', 'unique'),
			// array('Is_active', 'numerical', 'integerOnly'=>true),
			// array('Name, Model', 'length', 'max'=>32),
			// array('Code', 'length', 'max'=>64),
			// array('Description', 'length', 'max'=>255)
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'plan_id' => 'Plan ID',
			'telco_id' => 'Telco ID',
			'brand_name' => 'Brand Name',
			'plan_type' => 'Plan Type',
			'is_default_plan' => 'Default Plan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('plan_id',$this->plan_id);
		$criteria->compare('telco_id',$this->telco_id,true);
		$criteria->compare('brand_name',$this->brand_name,true);
		$criteria->compare('plan_type',$this->plan_type);
		$criteria->compare('is_default_plan',$this->is_default_plan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Availment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Initialize new database connection
	 * 
	 * @return type
	 */
	public function getDbConnection()
	{
		return Yii::app()->getComponent('db_cp');
	}
}
