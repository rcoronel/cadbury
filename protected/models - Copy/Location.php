<?php

/**
 * This is the model class for table "location".
 *
 * The followings are the available columns in table 'location':
 * @property integer $Location_ID
 * @property integer $Nas_ID
 * @property string $Name
 * @property string $Type
 * @property integer $Parent_location_ID
 * @property string $Parent_type
 * @property string $Latlon
 * @property string $Address
 * @property string $Category
 * @property string $Created_at
 * @property string $Updated_at
 */
class Location extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'location';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Name, Type', 'required'),
			array('Location_ID, Nas_ID, Parent_location_ID', 'numerical', 'integerOnly'=>true),
			array('Name, Type, Parent_type', 'length', 'max'=>64),
			array('Latlon', 'length', 'max'=>32),
			array('Address, Category, Created_at, Updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Location_ID, Name, Type, Parent_location_ID, Parent_type, Latlon, Address, Category, Created_at, Updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Location_ID' => 'ID',
			'Nas_ID'=>'NAS',
			'Name' => 'Location',
			'Type' => 'Type',
			'Parent_location_ID' => 'Parent Location',
			'Parent_type' => 'Parent Type',
			'Latlon' => 'Latitude Longitude',
			'Address' => 'Address',
			'Category' => 'Categories',
			'Created_at' => 'Date Added',
			'Updated_at' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Location_ID',$this->Location_ID);
		$criteria->compare('Name',$this->Name,true);
		$criteria->compare('Type',$this->Type,true);
		$criteria->compare('Parent_location_ID',$this->Parent_location_ID);
		$criteria->compare('Parent_type',$this->Parent_type,true);
		$criteria->compare('Latlon',$this->Latlon,true);
		$criteria->compare('Address',$this->Address,true);
		$criteria->compare('Category',$this->Category,true);
		$criteria->compare('Created_at',$this->Created_at,true);
		$criteria->compare('Updated_at',$this->Updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Location the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
