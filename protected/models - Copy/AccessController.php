<?php

/**
 * This is the model class for table "access_controller".
 *
 * The followings are the available columns in table 'access_controller':
 * @property integer $access_controller_id
 * @property string $switch_ip
 * @property string $public_ip
 * @property string $created_at
 * @property string $updated_at
 */
class AccessController extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'access_controller';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('switch_ip, public_ip, created_at, updated_at', 'required'),
			array('switch_ip, public_ip', 'length', 'max'=>128),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('access_controller_id, switch_ip, public_ip, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'access_controller_id' => 'ID',
			'switch_ip' => 'Switch IP Address',
			'public_ip' => 'Public IP Address',
			'created_at' => 'Date Added',
			'updated_at' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('access_controller_id',$this->access_controller_id);
		$criteria->compare('switch_ip',$this->switch_ip,true);
		$criteria->compare('public_ip',$this->public_ip,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AccessController the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
