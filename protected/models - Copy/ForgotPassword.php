<?php

/**
 * This is the model class for table "user_facebook".
 *
 * The followings are the available columns in table 'user_facebook':
 * @property integer $entry_id
 * @property integer $nas_id
 * @property integer $email
 * @property date $created_at
 * @property string $expired_at
 */
class ForgotPassword extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Cadbury_CMS.forgot_password';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			// array('Device_ID, Mobile_number', 'required'),
   //                  //, Auth_code, Is_Validated, Created_at
			// array('Avail_mobile_ID, Device_ID, Mobile_number, Auth_code', 'numerical', 'integerOnly'=>true),
			// array('Mobile_number', 'length', 'max'=>11),
			// array('Auth_code', 'length', 'max'=>5),
			// // The following rule is used by search().
			// // @todo Please remove those attributes that should not be searched.
			// array('Avail_mobile_ID, Device_ID, Mobile_number, Created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Entry_ID' => 'Entry ID',
			'Nas_ID' => 'NAS ID',	
			'Email' => 'Email',
			'Created_at' => 'Date Added',
			'Expired_at' => 'Expiration Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Entry_ID',$this->Entry_ID);
		$criteria->compare('Nas_ID',$this->Nas_ID);
		$criteria->compare('Email',$this->Email);
		$criteria->compare('Created_at',$this->Created_at);
		$criteria->compare('Expired_at',$this->Expired_at);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserFacebook the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
