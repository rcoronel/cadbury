<?php

/**
 * This is the model class for table "device".
 *
 * The followings are the available columns in table 'device':
 * @property integer $Device_ID
 * @property string $Mac_address
 * @property date $Created_at
 * @property date $Updated_at
 * @property string Correlor_ID
 */
class Device extends ActiveRecord
{
	// string IP Address
	public $ip_address;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'device';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Mac_address, Created_at, Updated_at', 'required'),
			array('Device_ID', 'numerical', 'integerOnly'=>true),
			array('Correlor_ID', 'length', 'max'=>64),
			array('Created_at, Updated_at', 'date', 'format'=>'yyyy-MM-dd hh:mm:ss'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'connection'=>array(self::HAS_ONE, 'DeviceConnection', array('Device_ID'=>'Device_ID'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Device_ID' => 'ID',
			'Mac_address' => 'MAC Address',
			'Created_at' => 'Date Added',
			'Updated_at' => 'Date Updated'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('Device_ID',$this->Device_ID);
		$criteria->compare('Mac_address',$this->Mac_address);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Device the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Initialize new database connection
	 * 
	 * @return type
	 */
	public function getDbConnection()
	{
		return Yii::app()->getComponent('db_cp');
	}
	
	public static function getSessionTime(DeviceConnection $connection)
	{
		// Get latest availed method
		$latest_avail = ConnectionAvail::model()->findByAttributes(array('Device_connection_ID'=>$connection->Device_connection_ID), array('order'=>'Created_at DESC'));
		
		if ( ! empty($latest_avail)) {
			// Get sessions without availment ID and set with current availment ID
			self::_setAvailmentSession($latest_avail->Availment_ID, $connection->Username);

			//Get session total session time
			$current_session_time = self::_getAvailmentSession($latest_avail->Availment_ID, $connection->Username);
			return $current_session_time[0];
		}
		
		return 0;
	}
	
	/**
	 * Set unreferenced session with the latest availment method
	 *  
	 * @param int $availment_id Latest availment ID
	 * @param string $username Connection username
	 */
	private static function _setAvailmentSession($availment_id, $username)
	{
		// Get sessions without availment ID
		$curl = "http://localhost/radius-ws/current_session.php?username={$username}";
		$link = curl_init($curl);
		curl_setopt($link, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($link, CURLOPT_RETURNTRANSFER, true);
		$data = curl_exec($link);
		curl_close($link);
		
		$data = CJSON::decode($data);
		if ( ! empty($data)) {
			foreach ($data as $d) {
				$curl = "http://localhost/radius-ws/radacct_availment.php?radacct_id={$d['radacctid']}&availment_id={$availment_id}";
				$link = curl_init($curl);
				curl_setopt($link, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($link, CURLOPT_RETURNTRANSFER, true);
				$data = curl_exec($link);
				curl_close($link);
			}
		}
	}
	
	private static function _getAvailmentSession($availment_id, $username)
	{
		// Get sessions
		$curl = "http://localhost/radius-ws/current_total_sessiontime.php?username={$username}&availment_id={$availment_id}";
		$link = curl_init($curl);
		curl_setopt($link, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($link, CURLOPT_RETURNTRANSFER, true);
		$data = curl_exec($link);
		curl_close($link);
		
		return CJSON::decode($data);
	}
}
