<?php

/**
 * This is the model class for table "avail_mobile".
 *
 * The followings are the available columns in table 'avail_mobile':
 * @property integer $Avail_mobile_ID
 * @property integer $Nas_ID
 * @property integer $Site_ID
 * @property integer $Portal_ID
 * @property integer $Access_point_group_ID
 * @property integer $Access_point_ID
 * @property integer $Device_ID
 * @property integer $Mobile_number
 * @property integer $Auth_code
 * @property integer $Is_Validated
 * @property integer $Limit
 * @property integer $Created_at
 */
class AvailMobile extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Cadbury_Captive.avail_mobile';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Nas_ID, Site_ID, Portal_ID, Access_point_group_ID, Access_point_ID, Device_ID, Mobile_number', 'required'),
			array('Avail_mobile_ID, Nas_ID, Site_ID, Portal_ID, Access_point_group_ID, Access_point_ID, Device_ID, Mobile_number, Auth_code', 'numerical', 'integerOnly'=>true),
			array('Mobile_number', 'length', 'max'=>11),
			array('Auth_code', 'length', 'max'=>5)
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'device'=>array(self::BELONGS_TO, 'Device', array('Device_ID'=>'Device_ID'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Avail_mobile_ID' => 'Mobile ID',
			'Device_ID' => 'Device',	
			'Auth_code' => 'Verification Code',
			'Mobile_number' => 'Mobile No.',
			'Created_at' => 'Date Added',
			'Limit' => 'Resend Tries',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Avail_mobile_ID',$this->Avail_mobile_ID);
		$criteria->compare('Device_ID',$this->Device_ID);
		$criteria->compare('Mobile_number',$this->Mobile_number,true);
		$criteria->compare('Created_at',$this->Created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserFacebook the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public static function poll($from, $to, $nas_id = 0)
	{
		$where = "DATE(am.Created_at) BETWEEN '{$from}' AND '{$to}' AND Is_Validated = 1";
		if ($nas_id) {
			$where .= " AND Nas_ID = {$nas_id}";
		}
		$count = Yii::app()->db_cp->createCommand()
			->select('COUNT(DISTINCT d.Mac_address, am.Nas_ID) as count')
			->from('avail_mobile am')
			->join('device d', 'd.Device_ID = am.Device_ID')
			->where($where)
			->queryRow();
		
		return (int)$count['count'];
	}
}
