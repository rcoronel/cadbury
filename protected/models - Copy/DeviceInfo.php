<?php

/**
 * This is the model class for table "device_info".
 *
 * The followings are the available columns in table 'device_info':
 * @property integer $Device_ID
 * @property string $Brand
 * @property string $Model
 * @property string $OS
 * @property string $OS_ver
 * @property string $Device_type
 * @property string $Created_at
 */
class DeviceInfo extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'device_info';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Device_ID, Created_at', 'required'),
			array('Device_ID', 'numerical', 'integerOnly'=>true),
			array('Brand, Model, OS, OS_ver, Device_type', 'length', 'max'=>16),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Device_ID, Brand, Model, OS, OS_ver, Device_type, Created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Device_ID' => 'Device',
			'Brand' => 'Brand name',
			'Model' => 'Model',
			'OS' => 'OS',
			'OS_ver' => 'OS version',
			'Device_type' => 'Device type (computer/mobile/tablet)',
			'Created_at' => 'Date added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Device_ID',$this->Device_ID);
		$criteria->compare('Brand',$this->Brand,true);
		$criteria->compare('Model',$this->Model,true);
		$criteria->compare('OS',$this->OS,true);
		$criteria->compare('OS_ver',$this->OS_ver,true);
		$criteria->compare('Device_type',$this->Device_type,true);
		$criteria->compare('Created_at',$this->Created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cp;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DeviceInfo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
