<?php

/**
 * This is the model class for table "tbl_tattoo_accts".
 *
 * The followings are the available columns in table 'tbl_tattoo_accts':
 * @property integer $Network_service_ID
 * @property string $Cust_Acc_No
 * @property string $Aggregate_name
 */
class TattooAccts extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'unifi_cms.tbl_tattoo_accts';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Cust_Acc_No', 'length', 'max'=>20),
			array('Aggregate_name', 'length', 'max'=>40),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Network_service_ID, Cust_Acc_No, Aggregate_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Network_service_ID' => 'Network Service',
			'Cust_Acc_No' => 'Cust Acc No',
			'Aggregate_name' => 'Aggregate Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Network_service_ID',$this->Network_service_ID);
		$criteria->compare('Cust_Acc_No',$this->Cust_Acc_No,true);
		$criteria->compare('Aggregate_name',$this->Aggregate_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TattooAccts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
