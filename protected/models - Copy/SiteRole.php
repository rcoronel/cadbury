<?php

/**
 * This is the model class for table "site_role".
 *
 * The followings are the available columns in table 'site_role':
 * @property integer $Site_role_ID
 * @property integer $Site_ID
 * @property string $Role
 * @property integer $Is_active
 * @property string $Created_at
 * @property string $Updated_at
 */
class SiteRole extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'site_role';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Site_ID, Role, Is_active, Created_at, Updated_at', 'required'),
			array('Site_ID, Is_active', 'numerical', 'integerOnly'=>true),
			array('Role', 'length', 'max'=>16),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Site_role_ID, Site_ID, Role, Is_active, Created_at, Updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'site'=>array(self::BELONGS_TO, 'Site', array('Site_ID'=>'Site_ID')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Site_role_ID' => 'ID',
			'Site_ID' => 'Site',
			'Role' => 'Role',
			'Is_active' => 'Status',
			'Created_at' => 'Date Added',
			'Updated_at' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Site_role_ID',$this->Site_role_ID);
		$criteria->compare('Site_ID',$this->Site_ID);
		$criteria->compare('Role',$this->Role,true);
		$criteria->compare('Is_active',$this->Is_active);
		$criteria->compare('Created_at',$this->Created_at,true);
		$criteria->compare('Updated_at',$this->Updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cms;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SiteRole the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
