<?php

/**
 * This is the model class for table "scenario_returning".
 *
 * The followings are the available columns in table 'scenario_returning':
 * @property integer $Scenario_ID
 * @property integer $Availment_ID
 * @property integer $Level
 * @property string $Created_at
 */
class ScenarioReturning extends ActiveRecord
{
	public $has_record = 0;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'scenario_returning';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Scenario_ID, Availment_ID, Level, Created_at', 'required'),
			array('Scenario_ID, Availment_ID, Level', 'numerical', 'integerOnly'=>true),
			
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'availment'=>array(self::BELONGS_TO, 'Availment', 'Availment_ID', 'together'=>TRUE),
			'scenario'=>array(self::BELONGS_TO, 'Scenario', 'Scenario_ID', 'together'=>TRUE)
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Scenario_ID' => 'Scenario',
			'Availment_ID' => 'Availment',
			'Level' => 'Level',
			'Created_at' => 'Date Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Scenario_ID',$this->Scenario_ID);
		$criteria->compare('Availment_ID',$this->Availment_ID);
		$criteria->compare('Level',$this->Level);
		$criteria->compare('Created_at',$this->Created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cms;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ScenarioReturning the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public static function findScenario($availments = array(), $portal_id = 0)
	{
		$cnt = (int)count($availments);
		$where = '';
		foreach ($availments as $index=>$a) {
			if ($index != 0) {
				$where .= " OR ";
			}
			$where .= "(Level = {$a->Level} AND Availment_ID = {$a->Availment_ID})";
		}
		$where .= " AND Portal_ID = {$portal_id}";
		
		// Check for returning availments
		$ids = Yii::app()->db_cp->createCommand()
			->select("sa.Scenario_ID")
			->from("Cadbury_CMS.scenario_returning sa")
			->join('Cadbury_CMS.scenario s', 's.Scenario_ID = sa.Scenario_ID')
			->where($where)
			->group(array("sa.Scenario_ID"))
			->having("COUNT(sa.Scenario_ID) = {$cnt}")
			->queryAll();
		
		$id_array = array();
		if ( !empty($ids)) {
			foreach ($ids as $i) {
				$id_array[] = $i['Scenario_ID'];
			}
		}
		
		// if empty returning availment, check previous
		if (empty($id_array)) {
			$ids = Yii::app()->db_cp->createCommand()
			->select("sa.Scenario_ID")
			->from("Cadbury_CMS.scenario_availment sa")
			->join('Cadbury_CMS.scenario s', 's.Scenario_ID = sa.Scenario_ID')
			->where($where)
			->group(array("sa.Scenario_ID"))
			->having("COUNT(sa.Scenario_ID) = {$cnt}")
			->queryAll();
		
			$id_array = array();
			if ( !empty($ids)) {
				foreach ($ids as $i) {
					$id_array[] = $i['Scenario_ID'];
				}
			}
		}
		
		return $id_array;
	}
}
