<?php

/**
 * This is the model class for table "survey".
 *
 * The followings are the available columns in table 'survey':
 * @property integer $Survey_ID
 * @property integer $Nas_ID
 * @property string $Title
 * @property integer $Position
 * @property integer $Is_active
 * @property string $Created_at
 * @property string $Updated_at
 * @property integer $Is_ad
 */
class Survey extends ActiveRecord
{
        /**
         * @return string the associated database table name
         */
        public $question_count;
        public function tableName()
        {
            return 'Cadbury_CMS.survey';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules()
        {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                array('Nas_ID, Title, Is_active, Created_at, Updated_at, Is_ad', 'required'),
                array('question_count, Nas_ID, Position, Is_active, Is_ad', 'numerical', 'integerOnly'=>true),
                array('Title', 'length', 'max'=>64),
                // The following rule is used by search().
                // @todo Please remove those attributes that should not be searched.
                array('question_count, Survey_ID, Nas_ID, Title, Position, Is_active, Created_at, Updated_at, Is_ad', 'safe', 'on'=>'search'),
            );
        }

        /**
         * @return array relational rules.
         */
        public function relations()
        {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
//                'question'=>array(self::HAS_MANY, 'SurveyQuestion', 'Survey_ID', 'together'=>TRUE),
            );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels()
        {
            return array(
                'Survey_ID' => 'ID',
                'Nas_ID' => 'NAS',
                'Title' => 'Title',
                'Position' => 'Position',
                'Is_active' => 'Status',
                'Created_at' => 'Date Added',
                'Updated_at' => 'Date Updated',
                'Is_ad' => 'Ad Survey',
                'question_count' => 'Number of Questions',
            );
        }

        /**
         * Retrieves a list of models based on the current search/filter conditions.
         *
         * Typical usecase:
         * - Initialize the model fields with values from filter form.
         * - Execute this method to get CActiveDataProvider instance which will filter
         * models according to data in model fields.
         * - Pass data provider to CGridView, CListView or any similar widget.
         *
         * @return CActiveDataProvider the data provider that can return the models
         * based on the search/filter conditions.
         */
        public function search()
        {
            // @todo Please modify the following code to remove attributes that should not be searched.

            $criteria=new CDbCriteria;

            $criteria->compare('Survey_ID',$this->Survey_ID);
            $criteria->compare('Nas_ID',$this->Nas_ID);
            $criteria->compare('Title',$this->Title,true);
            $criteria->compare('Position',$this->Position);
            $criteria->compare('Is_active',$this->Is_active);
            $criteria->compare('Created_at',$this->Created_at,true);
            $criteria->compare('Updated_at',$this->Updated_at,true);
            $criteria->compare('Is_ad',$this->Is_ad);
            $criteria->compare("(SELECT COUNT(*) FROM survey_question WHERE t.Survey_ID = Survey_ID)", $this->question_count);


            return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
            ));
        }

        /**
         * @return CDbConnection the database connection used for this class
         */
        public function getDbConnection()
        {
            return Yii::app()->db_cms;
        }

        /**
         * Returns the static model of the specified AR class.
         * Please note that you should have this exact method in all your CActiveRecord descendants!
         * @param string $className active record class name.
         * @return Survey the static model class
         */
        public static function model($className=__CLASS__)
        {
            return parent::model($className);
        }
}