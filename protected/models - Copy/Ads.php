<?php

/**
 * This is the model class for table "ads".
 *
 * The followings are the available columns in table 'ads':
 * @property integer $Ads_ID
 * @property integer $Portal_ID
 * @property string $Image
 * @property string $Url
 * @property string $Close_url
 * @property string $Title
 * @property string $Created_at
 * @property string $Updated_at
 */
class Ads extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ads';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Portal_ID, Url, Close_url, Title, Created_at, Updated_at', 'required'),
			array('Portal_ID', 'numerical', 'integerOnly'=>true),
			array('Title', 'length', 'max'=>32),
			array('Image', 'file', 'allowEmpty'=>false, 'types'=>'jpg, jpeg, png, gif'),
			array('Url, Close_url', 'length', 'max'=>256),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Ads_ID' => 'ID',
			'Portal_ID' => 'NAS',
			'Image' => 'Image',
			'Url' => 'Link',
			'Close_url' => 'Link on close',
			'Title' => 'Title',
			'Created_at' => 'Date Added',
			'Updated_at' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Ads_ID',$this->Ads_ID);
		$criteria->compare('Portal_ID',$this->Portal_ID);
		$criteria->compare('Image',$this->Image,true);
		$criteria->compare('Url',$this->Url,true);
		$criteria->compare('Title',$this->Title,true);
		$criteria->compare('Created_at',$this->Created_at,true);
		$criteria->compare('Updated_at',$this->Updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Ads the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
	/**
	 * Initialize new database connection
	 * 
	 * @return type
	 */
	public function getDbConnection()
	{
		return Yii::app()->getComponent('db_cms');
	}
}
