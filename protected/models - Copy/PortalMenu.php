<?php

/**
 * This is the model class for table "portal_menu".
 *
 * The followings are the available columns in table 'portal_menu':
 * @property integer $Portal_menu_ID
 * @property integer $Parent_portal_menu_ID
 * @property string $Title
 * @property string $Classname
 * @property string $Img
 * @property integer $Display
 * @property integer $Position
 * @property string $Created_at
 * @property string $Updated_at
 */
class PortalMenu extends ActiveRecord
{
	// int Has Subpages
	public $has_subpages;
	
	public $View;
	public $Add;
	public $Edit;
	public $Delete;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'portal_menu';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Parent_portal_menu_ID, Title, Classname, Img, Display, Position, Created_at, Updated_at', 'required'),
			array('Parent_portal_menu_ID, Display, Position', 'numerical', 'integerOnly'=>true),
			array('Title', 'length', 'max'=>32),
			array('Classname', 'length', 'max'=>16),
			array('Img', 'length', 'max'=>128),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Portal_menu_ID, Parent_portal_menu_ID, Title, Classname, Img, Display, Position, Created_at, Updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'portal'=>array(self::BELONGS_TO, 'Portal', array('Portal_ID'=>'Portal_ID')),
			'permission'=>array(self::HAS_ONE, 'PortalRolePermission', array('Portal_menu_ID'=>'Portal_menu_ID'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Portal_menu_ID' => 'ID',
			'Parent_portal_menu_ID' => 'Parent Menu',
			'Title' => 'Title',
			'Classname' => 'Classname',
			'Img' => 'Image',
			'Display' => 'Display',
			'Position' => 'Position',
			'Created_at' => 'Date Added',
			'Updated_at' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Portal_menu_ID',$this->Portal_menu_ID);
		$criteria->compare('Parent_portal_menu_ID',$this->Parent_portal_menu_ID);
		$criteria->compare('Title',$this->Title,true);
		$criteria->compare('Classname',$this->Classname,true);
		$criteria->compare('Img',$this->Img,true);
		$criteria->compare('Display',$this->Display);
		$criteria->compare('Position',$this->Position);
		$criteria->compare('Created_at',$this->Created_at,true);
		$criteria->compare('Updated_at',$this->Updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cms;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PortalMenu the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public static function getMainMenus()
	{
		$context = Context::getContext();
		
		$pages = PortalMenu::model()->with('permission')->findAll(
					array(	'order' => 'position', 'condition' => 'Portal_role_ID = :role_id AND View = 1 AND Display = 1 AND Parent_portal_menu_ID = 0',
							'params' => array(':role_id' => $context->portal_admin->Portal_role_ID)));
		return $pages;
	}
	
	public static function getSubMenus()
	{
		$context = Context::getContext();
		$pages = PortalMenu::model()->with('permission')->findAll(
					array(	'order' => 'position', 'condition' => 'Portal_role_ID = :role_id AND view = 1 AND Display = 1 AND Parent_portal_menu_ID != 0',
							'params' => array(':role_id' => $context->portal_admin->Portal_role_ID)));
		return $pages;
	}
}
