<?php

/**
 * This is the model class for table "device_connection_count".
 *
 * The followings are the available columns in table 'device_connection_count':
 * @property string $Date
 * @property integer $Nas_ID
 * @property integer $Count
 */
class DeviceConnectionCount extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'device_connection_count';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Date, Nas_ID, Success, Count', 'required'),
			array('Nas_ID, Success, Count', 'numerical', 'integerOnly'=>true)
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Date' => 'Date',
			'Nas_ID' => 'NAS',
			'Success' => 'Logged',
			'Count' => 'Total',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Date',$this->Date,true);
		$criteria->compare('Nas_ID',$this->Nas_ID);
		$criteria->compare('Count',$this->Count);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DeviceConnectionCount the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Initialize new database connection
	 * 
	 * @return type
	 */
	public function getDbConnection()
	{
		return Yii::app()->getComponent('db_cp');
	}
	
	/**
	 * Save the count of the specified DeviceConnection table in the DB
	 * 
	 * @param string $table DeviceConnection table
	 * @param date $date yyyy-mm-dd
	 * @param int $nas_id NAS ID
	 * @return type
	 */
	public function poll($table, $date, $nas_id)
	{
		// Get total
		$total = Yii::app()->db_cp->createCommand()
			->select('COUNT(*) as count')
			->from("$table dc")
			->join("device d", "dc.Device_ID = d.Device_ID")
			->where("Nas_ID = {$nas_id}")
			->queryRow();
		
		// Get only logged in
		$success = Yii::app()->db_cp->createCommand()
			->select('COUNT(*) as count')
			->from("$table dc")
			->join("device d", "dc.Device_ID = d.Device_ID")
			->where("Nas_ID = {$nas_id} AND Username != '' AND Password != ''")
			->queryRow();	
		
		// Delete existing records
		DeviceConnectionCount::model()->deleteAllByAttributes(array('Date'=>$date, 'Nas_ID'=>$nas_id));
		
		// Save new records
		$counter = new DeviceConnectionCount;
		$counter->Date = $date;
		$counter->Nas_ID = $nas_id;
		$counter->Success = (int)$success['count'];
		$counter->Count = (int)$total['count'];
		$counter->save();
	}
}
