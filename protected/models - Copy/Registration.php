<?php

/**
 * This is the model class for table "avail_registration".
 *
 * The followings are the available columns in table 'avail_registration':
 * @property integer $Avail_registration_ID
 * @property integer $Device_ID
 * @property string $F_name
 * @property string $L_name
 * @property string $Email
 * @property string $Birthday
 * @property integer $Gender
 * @property string $Created_at
 */
class Registration extends ActiveRecord
{
    
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'avail_registration';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('F_name, L_name, Email, Birthday, Gender, Created_at', 'required'),
			array('Device_ID, Gender', 'numerical', 'integerOnly'=>true),
			array('F_name, L_name', 'length', 'max'=>32),
			array('Email', 'length', 'max'=>128),
                        array('Email','email'),
                        //array('Device_ID','unique', 'message'=>'Device already in use.'),
//                        array('Email','unique', 'message'=>' Email already in use.'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Avail_registration_ID, Device_ID, F_name, L_name, Email, Birthday, Gender, Created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Avail_registration_ID' => 'ID',
			'Device_ID' => 'Device',
			'F_name' => 'First name',
			'L_name' => 'Last name',
			'Email' => 'E-mail Address',
			'Birthday' => 'Birthday',
			'Gender' => 'Gender',
			'Created_at' => 'Date Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Avail_registration_ID',$this->Avail_registration_ID);
		$criteria->compare('Device_ID',$this->Device_ID);
		$criteria->compare('F_name',$this->F_name,true);
		$criteria->compare('L_name',$this->L_name,true);
		$criteria->compare('Email',$this->Email,true);
		$criteria->compare('Birthday',$this->Birthday,true);
		$criteria->compare('Gender',$this->Gender);
		$criteria->compare('Created_at',$this->Created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Registration the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        /**
	 * Initialize new database connection
	 * 
	 * @return type
	 */
        public function getDbConnection()
        {
            return Yii::app()->getComponent('db_cp');
        }
}
