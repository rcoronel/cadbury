<?php

/**
 * This is the model class for table "tbl_tattoo_accts".
 *
 * The followings are the available columns in table 'tbl_tattoo_accts':
 * @property integer $record_id
 * @property string $acct_num
 * @property string $acct_name
 * @property string $date_registered
 */
class TblTattooAccts extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'unifi_cms.tbl_tattoo_accts';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('acct_num', 'required'),//, 'length', 'max'=>20),
			array('acct_name', 'length', 'max'=>40),
			array('date_registered', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('record_id, acct_num, acct_name, date_registered', 'safe', 'on'=>'search'),
                        //array('acct_num', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'record_id' => 'Record',
			'acct_num' => 'Acct Num',
			'acct_name' => 'Acct Name',
			'date_registered' => 'Date Registered',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('record_id',$this->record_id);
		$criteria->compare('acct_num',$this->acct_num,true);
		$criteria->compare('acct_name',$this->acct_name,true);
		$criteria->compare('date_registered',$this->date_registered,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TblTattooAccts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
