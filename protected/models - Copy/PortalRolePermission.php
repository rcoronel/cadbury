<?php

/**
 * This is the model class for table "portal_role_permission".
 *
 * The followings are the available columns in table 'portal_role_permission':
 * @property integer $Portal_role_ID
 * @property integer $Portal_menu_ID
 * @property integer $View
 * @property integer $Add
 * @property integer $Edit
 * @property integer $Delete
 */
class PortalRolePermission extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'portal_role_permission';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Portal_role_ID, Portal_menu_ID, View, Add, Edit, Delete', 'required'),
			array('Portal_role_ID, Portal_menu_ID, View, Add, Edit, Delete', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Portal_role_ID, Portal_menu_ID, View, Add, Edit, Delete', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Portal_role_ID' => 'Role',
			'Portal_menu_ID' => 'Menu',
			'View' => 'View',
			'Add' => 'Add',
			'Edit' => 'Edit',
			'Delete' => 'Delete',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Portal_role_ID',$this->Portal_role_ID);
		$criteria->compare('Portal_menu_ID',$this->Portal_menu_ID);
		$criteria->compare('View',$this->View);
		$criteria->compare('Add',$this->Add);
		$criteria->compare('Edit',$this->Edit);
		$criteria->compare('Delete',$this->Delete);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cms;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PortalRolePermission the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
