<?php

/**
 * This is the model class for table "admin_menu".
 *
 * The followings are the available columns in table 'admin_menu':
 * @property integer $Admin_menu_ID
 * @property integer $Parent_admin_menu_ID
 * @property string $Title
 * @property string $Classname
 * @property string $Img
 * @property integer $Display
 * @property integer $Position
 * @property string $Created_at
 * @property string $Updated_at
 */
class AdminMenu extends ActiveRecord
{
	// int Has Subpages
	public $has_subpages;
	
	public $View;
	public $Add;
	public $Edit;
	public $Delete;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'admin_menu';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Parent_admin_menu_ID, Title, Classname, Display, Created_at, Updated_at', 'required'),
			array('Admin_menu_ID, Parent_admin_menu_ID, Display, Position, View, Add, Edit, Delete', 'numerical', 'integerOnly'=>true),
			array('Title, Classname', 'length', 'max'=>16),
			array('Img', 'length', 'max'=>128),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'permission'=>array(self::HAS_ONE, 'AdminRolePermission', array('Admin_menu_ID'=>'Admin_menu_ID'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Admin_menu_ID' => 'ID',
			'Parent_admin_menu_ID' => 'Parent Menu',
			'Title' => 'Title',
			'Classname' => 'Classname',
			'Img' => 'Icon class',
			'Display' => 'Display',
			'Position' => 'Position',
			'Created_at' => 'Date Added',
			'Updated_at' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Admin_menu_ID',$this->Admin_menu_ID);
		$criteria->compare('Parent_admin_menu_ID',$this->Parent_admin_menu_ID);
		$criteria->compare('Title',$this->Title,true);
		$criteria->compare('Classname',$this->Classname,true);
		$criteria->compare('Img',$this->Img,true);
		$criteria->compare('Display',$this->Display);
		$criteria->compare('Position',$this->Position);
		$criteria->compare('Created_at',$this->Created_at,true);
		$criteria->compare('Updated_at',$this->Updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AdminMenu the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public static function getMainMenus()
	{
		$context = Context::getContext();
		
		$pages = AdminMenu::model()->with('permission')->findAll(
					array(	'order' => 'position', 'condition' => 'Admin_role_ID = :role_id AND View = 1 AND Display = 1 AND Parent_admin_menu_ID = 0',
							'params' => array(':role_id' => $context->admin->Admin_role_ID)));
		return $pages;
	}
	
	public static function getSubMenus()
	{
		$context = Context::getContext();
		$pages = AdminMenu::model()->with('permission')->findAll(
					array(	'order' => 'position', 'condition' => 'Admin_role_ID = :role_id AND View = 1 AND Display = 1 AND Parent_admin_menu_ID != 0',
							'params' => array(':role_id' => $context->admin->Admin_role_ID)));
		return $pages;
	}
}
