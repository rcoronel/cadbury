<?php

/**
 * This is the model class for table "device_avail".
 *
 * The followings are the available columns in table 'device_avail':
 * @property integer $Device_avail_ID
 * @property integer $Device_ID
 * @property integer $Nas_ID
 * @property integer $Site_ID
 * @property integer $Portal_ID
 * @property integer $Access_point_group_ID
 * @property integer $Access_point_ID
 * @property integer $Availment_ID
 * @property integer $Level
 * @property string $Scene
 * @property string $Created_at
 * @property string $Updated_at
 */
class DeviceAvail extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'device_avail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Device_ID, Nas_ID, Site_ID, Portal_ID, Access_point_group_ID, Access_point_ID, Availment_ID, Level, Scene, Created_at, Updated_at', 'required'),
			array('Device_ID, Nas_ID, Site_ID, Portal_ID, Access_point_group_ID, Access_point_ID, Availment_ID, Level', 'numerical', 'integerOnly'=>true),
			array('Created_at, Updated_at', 'date', 'format'=>'yyyy-MM-dd hh:mm:ss'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Device_avail_ID' => 'ID',
			'Device_ID' => 'Device',
			'Nas_ID' => 'NAS',
			'Availment_ID' => 'Availment',
			'Level' => 'Level',
			'Created_at' => 'Date Added',
			'Updated_at' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Device_avail_ID',$this->Device_avail_ID);
		$criteria->compare('Device_ID',$this->Device_ID);
		$criteria->compare('Nas_ID',$this->Nas_ID);
		$criteria->compare('Availment_ID',$this->Availment_ID);
		$criteria->compare('Level',$this->Level);
		$criteria->compare('Created_at',$this->Created_at,true);
		$criteria->compare('Updated_at',$this->Updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cp;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DeviceAvail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
