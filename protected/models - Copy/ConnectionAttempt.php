<?php

/**
 * This is the model class for table "device_availment".
 *
 * The followings are the available columns in table 'device_availment':
 * @property integer $Device_ID
 * @property integer $Availment_ID
 * @property string $Created_at
 * @property string $Status
 * @property string $Nas_ID
 */
class ConnectionAttempt extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'connection_attempt_'.date('Y-m-d');
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Device_ID, Nas_ID, Availment_ID, Created_at', 'required'),
			array('Device_ID, Nas_ID, Availment_ID, Status', 'numerical', 'integerOnly'=>true),
			array('Created_at', 'date', 'format'=>'yyyy-MM-dd hh:mm:ss'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'device'=>array(self::BELONGS_TO, 'Device', array('Device_ID'=>'Device_ID'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Device_ID' => 'Device',
			'Nas_ID' => 'NAS ID',
			'Availment_ID' => 'Availment',
			'Status' => 'Status',
			'Created_at' => 'Date Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Device_ID',$this->Device_ID);
		$criteria->compare('Availment_ID',$this->Availment_ID);
		$criteria->compare('Nas_ID',$this->Availment_ID);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('Created_at',$this->Created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DeviceAvailment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Initialize new database connection
	 * 
	 * @return type
	 */
	public function getDbConnection()
	{
		return Yii::app()->getComponent('db_cp');
	}

	public static function createTable($tablename)
	{
		Yii::app()->db_cp->createCommand()->createTable($tablename,
			array(	'Connection_attempt_ID' => 'pk',
					'Nas_ID' => "int(11) NOT NULL COMMENT 'NAS'",
					'Device_ID' => "int(11) NOT NULL COMMENT 'Device'",
					'Availment_ID' => "int(11) NOT NULL COMMENT 'Availment'",
					'Status' => "int(11) NOT NULL COMMENT 'Status'",
					'Created_at' => "datetime NOT NULL COMMENT 'Date Added'"),
			'ENGINE=InnoDB DEFAULT CHARSET=utf8'
			);
	}

}
