<?php

/**
 * This is the model class for table "portal_role".
 *
 * The followings are the available columns in table 'portal_role':
 * @property integer $Portal_role_ID
 * @property integer $Portal_ID
 * @property string $Role
 * @property integer $Is_active
 * @property string $Created_at
 * @property string $Updated_at
 */
class PortalRole extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'portal_role';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Portal_ID, Role, Is_active, Created_at, Updated_at', 'required'),
			array('Portal_role_ID, Portal_ID, Is_active', 'numerical', 'integerOnly'=>true),
			array('Role', 'length', 'max'=>16),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'portal'=>array(self::BELONGS_TO, 'Portal', array('Portal_ID'=>'Portal_ID')),
			'site'=>array(self::BELONGS_TO, 'Site', array('Site_ID'=>'Site_ID'), 'through'=>'portal')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Portal_role_ID' => 'ID',
			'Portal_ID' => 'Portal',
			'Role' => 'Role',
			'Is_active' => 'Status',
			'Created_at' => 'Date Added',
			'Updated_at' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Portal_role_ID',$this->Portal_role_ID);
		$criteria->compare('Portal_ID',$this->Portal_ID);
		$criteria->compare('Role',$this->Role,true);
		$criteria->compare('Is_active',$this->Is_active);
		$criteria->compare('Created_at',$this->Created_at,true);
		$criteria->compare('Updated_at',$this->Updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cms;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PortalRole the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
