<?php

/**
 * This is the model class for table "access_point".
 *
 * The followings are the available columns in table 'access_point':
 * @property integer $Access_point_ID
 * @property integer $Access_point_group_ID
 * @property string $Name
 * @property integer $Is_active
 * @property string $Created_at
 * @property string $Updated_at
 */
class AccessPoint extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'access_point';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Access_point_group_ID, Name, Is_active, Created_at, Updated_at', 'required'),
			array('Access_point_ID, Access_point_group_ID, Is_active', 'numerical', 'integerOnly'=>true),
			array('Name', 'length', 'max'=>32),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Access_point_ID, Access_point_group_ID, Name, Is_active, Created_at, Updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Access_point_ID' => 'ID',
			'Access_point_group_ID' => 'Access Point Group',
			'Name' => 'Name',
			'Is_active' => 'Status',
			'Created_at' => 'Date added',
			'Updated_at' => 'Date updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Access_point_ID',$this->Access_point_ID);
		$criteria->compare('Access_point_group_ID',$this->Access_point_group_ID);
		$criteria->compare('Name',$this->Name,true);
		$criteria->compare('Is_active',$this->Is_active);
		$criteria->compare('Created_at',$this->Created_at,true);
		$criteria->compare('Updated_at',$this->Updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cms;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AccessPoint the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
