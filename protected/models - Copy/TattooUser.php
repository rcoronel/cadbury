<?php

/**
 * This is the model class for table "tattoo_user".
 *
 * The followings are the available columns in table 'tattoo_user':
 * @property integer $Tattoo_user_ID
 * @property integer $Network_service_ID
 * @property string $Account_no
 * @property string $Name
 * @property string $Created_at
 */
class TattooUser extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tattoo_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Network_service_ID, Account_no, Name, Created_at', 'required'),
			array('Network_service_ID', 'numerical', 'integerOnly'=>true),
			array('Account_no', 'length', 'max'=>12),
			array('Name', 'length', 'max'=>128),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Tattoo_user_ID, Network_service_ID, Account_no, Name, Created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Tattoo_user_ID' => 'ID',
			'Network_service_ID' => 'Network Service ID',
			'Account_no' => 'Account No.',
			'Name' => 'Name',
			'Created_at' => 'Date Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Tattoo_user_ID',$this->Tattoo_user_ID);
		$criteria->compare('Network_service_ID',$this->Network_service_ID);
		$criteria->compare('Account_no',$this->Account_no,true);
		$criteria->compare('Name',$this->Name,true);
		$criteria->compare('Created_at',$this->Created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TattooUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Initialize new database connection
	 * 
	 * @return type
	 */
	public function getDbConnection()
	{
		return Yii::app()->getComponent('db_cms');
	}
}
