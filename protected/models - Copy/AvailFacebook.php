<?php

/**
 * This is the model class for table "avail_facebook".
 *
 * The followings are the available columns in table 'avail_facebook':
 * @property integer $Avail_facebook_ID
 * @property integer $Device_ID
 * @property integer $Fb_ID
 * @property string $Email
 * @property string $Firstname
 * @property string $Lastname
 * @property string $Access_token
 * @property string $Access_expiry
 * @property string $Created_at
 * @property string $Updated_at
 * @property string $Correlor_ID
 */
class AvailFacebook extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Cadbury_Captive.avail_facebook';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Fb_ID, Device_ID, Firstname, Lastname, Created_at, Updated_at', 'required'),
			array('Avail_facebook_ID, Fb_ID, Device_ID', 'numerical', 'integerOnly'=>true),
			array('Firstname, Lastname', 'length', 'max'=>50),
			array('Email', 'length', 'max'=>128),
			array('Gender', 'length', 'max'=>6),
			array('Correlor_ID', 'length', 'max'=>64),
			array('Created_at, Updated_at', 'date', 'format'=>'yyyy-MM-dd hh:mm:ss'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'nas'=>array(self::BELONGS_TO, 'RadiusNas', 'Nas_ID'),
			'device'=>array(self::BELONGS_TO, 'Device', 'Device_ID'),
			'mobile'=>array(self::HAS_ONE, 'AvailMobile', array('Device_ID'=>'Device_ID', 'Nas_ID'=>'Nas_ID'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Avail_facebook_ID' => 'ID',
			'Device_ID' => 'Device',
			'Fb_ID' => ' Facebook ID',
			'Email' => 'E-mail Address',
			'Firstname' => 'First name',
			'Lastname' => 'Last name',
			'Access_token' => 'Access Token',
			'Access_expiry' => 'Token expiry',
			'Created_at' => 'Date Added',
			'Updated_at' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Avail_facebook_ID',$this->Avail_facebook_ID);
		$criteria->compare('Device_ID',$this->Device_ID);
		$criteria->compare('Firstname',$this->Firstname,true);
		$criteria->compare('Lastname',$this->Lastname,true);
		$criteria->compare('Email',$this->Email,true);
		$criteria->compare('Gender',$this->Gender);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserFacebook the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Initialize new database connection
	 * 
	 * @return type
	 */
	public function getDbConnection()
	{
		return Yii::app()->getComponent('db_cp');
	}
	
	/**
	 * Show complete name of user
	 * 
	 * 
	 * @return type
	 */
	public function getName()
	{
		return "$this->Firstname $this->Lastname";
	}
	
	public static function sendDetails($id)
	{
		$fb_user = AvailFacebook::model()->findByPk($id);
		
		$url = 'https://api.correlor.com/scr/api/customers/19/users';
		$data = array(
			'snUserId' => $fb_user->Fb_ID,
			'accessToken' => $fb_user->Access_token,
			'accessTokenExpires' => $fb_user->Access_expiry);
		
		$data_string = json_encode($data);                                                                                   

        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(       
                'Accept  application/json',
                'Content-Type: application/json; charset=UTF-8',                                                                                
                'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                   

        $result = curl_exec($ch);	
        $http_status_json = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
		
        //return '{"data":'.$result.',"https_status":"'.$http_status_json.'"}';
	}
	
	public static function poll($from, $to, $nas_id = 0)
	{
		$where = "DATE(af.Created_at) BETWEEN '{$from}' AND '{$to}'";
		if ($nas_id) {
			$where .= " AND Nas_ID = {$nas_id}";
		}
		$count = Yii::app()->db_cp->createCommand()
			->select('COUNT(DISTINCT d.Mac_address, af.Nas_ID) as count')
			->from('avail_facebook af')
			->join('device d', 'd.Device_ID = af.Device_ID')
			->where($where)
			->queryRow();
		
		return (int)$count['count'];
	}
}
