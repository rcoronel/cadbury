<?php

/**
 * This is the model class for table "avail_registration".
 *
 * The followings are the available columns in table 'avail_registration':
 * @property integer $Avail_registration_ID
 * @property integer $Nas_ID
 * @property integer $Site_ID
 * @property integer $Portal_ID
 * @property integer $Access_point_group_ID
 * @property integer $Access_point_ID
 * @property integer $Device_ID
 * @property string $F_name
 * @property string $L_name
 * @property string $Email
 * @property string $Birthday
 * @property integer $Gender
 * @property string $Created_at
 */
class AvailRegistration extends ActiveRecord
{
    
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'avail_registration';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Nas_ID, Site_ID, Portal_ID, Access_point_group_ID, Access_point_ID, Device_ID, F_name, L_name, Gender, Email, Birthday, Created_at', 'required'),
			array('Nas_ID, Site_ID, Portal_ID, Access_point_group_ID, Access_point_ID, Device_ID, Gender', 'numerical', 'integerOnly'=>true),
			array('F_name, L_name', 'length', 'max'=>32),
			array('Email', 'length', 'max'=>128),
			array('Email', 'email'),
			array('Created_at', 'date', 'format'=>'yyyy-MM-dd hh:mm:ss'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Avail_registration_ID' => 'ID',
			'Device_ID' => 'Device',
			'F_name' => 'First name',
			'L_name' => 'Last name',
			'Gender' => 'Gender',
			'Email' => 'E-mail Address',
			'Birthday' => 'Birthday',
			'Created_at' => 'Date Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Avail_registration_ID',$this->Avail_registration_ID);
		$criteria->compare('Device_ID',$this->Device_ID);
		$criteria->compare('F_name',$this->F_name,true);
		$criteria->compare('L_name',$this->L_name,true);
		$criteria->compare('Email',$this->Email,true);
		$criteria->compare('Birthday',$this->Birthday,true);
		$criteria->compare('Gender',$this->Gender);
		$criteria->compare('Created_at',$this->Created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Registration the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
	/**
	 * Initialize new database connection
	 * 
	 * @return type
	 */
	public function getDbConnection()
	{
			return Yii::app()->getComponent('db_cp');
	}
        
	public function getName()
	{
		return "$this->F_name $this->L_name";
	}
	
	public static function poll($from, $to, $nas_id = 0)
	{
		$where = "DATE(ar.Created_at) BETWEEN '{$from}' AND '{$to}'";
		if ($nas_id) {
			$where .= " AND Nas_ID = {$nas_id}";
		}
		$count = Yii::app()->db_cp->createCommand()
			->select('COUNT(DISTINCT d.Mac_address, ar.Nas_ID) as count')
			->from('avail_registration ar')
			->join('device d', 'd.Device_ID = ar.Device_ID')
			->where($where)
			->queryRow();
		
		return (int)$count['count'];
	}
}
