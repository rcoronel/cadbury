<?php

/**
 * This is the model class for table "nas_availment".
 *
 * The followings are the available columns in table 'nas_availment':
 * @property integer $Nas_ID
 * @property integer $Availment_ID
 * @property integer $Duration
 * @property integer $Is_recurring
 */
class NasAvailment extends ActiveRecord
{
	public $total_duration;
	public $max_level;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Cadbury_CMS.nas_availment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Nas_ID, Availment_ID, Duration', 'required'),
			array('Nas_ID, Availment_ID, Duration, Is_recurring', 'numerical', 'integerOnly'=>true),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'availment'=>array(self::BELONGS_TO, 'Availment', 'Availment_ID', 'together'=>TRUE),
			//'nas'=>array(self::BELONGS_TO, 'RadiusNas', array('Nas_ID'=>'id'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Nas_ID' => 'NAS',
			'Availment_ID' => 'Availment',
			'Duration' => 'Duration',
			'Is_recurring' => 'Is Recurring'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Nas_ID',$this->Nas_ID);
		$criteria->compare('Availment_ID',$this->Availment_ID);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return NasAvailment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Initialize new database connection
	 * 
	 * @return type
	 */
	public function getDbConnection()
	{
		return Yii::app()->getComponent('db_cms');
	}
}
