<?php

/**
 * This is the model class for table "radcheck".
 *
 * The followings are the available columns in table 'radcheck':
 * @property int $id
 * @property string $acctsessiontime
 */
class RadiusAcct extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'radacct';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'raddacctid'=>'ID',
			'acctsessionid'=>'Account Session ID',
			'acctuniqueid'=>'Account Unique ID',
			'username'=>'Username',
			'groupname'=>'Group name',
			'realm'=>'Realm',
			'nasipaddress'=>'NAS IP Address',
			'nasportid'=>'NAS Port ID',
			'nasporttype'=>'NAS Port Type',
			'acctstarttime'=>'Account Start Time',
			'acctstoptime'=>'Account Stop Time',
			'acctsessiontime'=>'Account Session Time',
			'acctauthentic'=>'Account Authentic',
			'connectinfo_start'=>'Connect Info Start',
			'connectinfo_stop'=>'Connect Info Stop',
			'acctinputoctets'=>'Account Input Octets',
			'acctoutputoctets'=>'Account Output Octets',
			'calledstationid'=>'Called Station ID',
			'callingstationid'=>'Calling Station ID',
			'acctterminatecause'=>'Account Terminate Cause',
			'servicetype'=>'Service Type',
			'framedprotocol'=>'Framed Protocol',
			'framedipaddress'=>'Framed IP Address',
			'acctstartdelay'=>'Account Start Delay',
			'acctstopdelay'=>'Account Stop Delay',
			'xascendsessionsvrkey'=>'Session Key',
			'availment_id'=>'Availment ID',
			'accesspointname'=>'Access Point Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('acctsessiontime',$this->acctsessiontime);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Configuration the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Initialize new database connection
	 * 
	 * @return type
	 */
	public function getDbConnection()
	{
		return Yii::app()->getComponent('db_radius');
	}
	
}
