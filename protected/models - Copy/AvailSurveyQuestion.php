<?php

/**
 * This is the model class for table "_question".
 *
 * The followings are the available columns in table '_question':
 * @property integer $Avail_survey_question_ID
 * @property integer $Avail_survey_ID
 * @property integer $Device_ID
 * @property integer $Survey_question_ID
 * @property string $Question
 * @property string $Created_at
 */
class AvailSurveyQuestion extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'avail_survey_question';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Device_ID, Survey_question_ID, Question, Created_at', 'required'),
			array('Avail_survey_ID, Device_ID, Survey_question_ID', 'numerical', 'integerOnly'=>true),
			array('Question', 'length', 'max'=>256),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Avail_survey_question_ID, Avail_survey_ID, Device_ID, Survey_question_ID, Question, Created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Avail_survey_question_ID' => 'ID',
			'Avail_survey_ID' => 'Survey',
			'Device_ID' => 'Device',
			'Survey_question_ID' => 'Question',
			'Question' => 'Question',
			'Created_at' => 'Date Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Avail_survey_question_ID',$this->Avail_survey_question_ID);
		$criteria->compare('Avail_survey_ID',$this->Avail_survey_ID);
		$criteria->compare('Device_ID',$this->Device_ID);
		$criteria->compare('Survey_question_ID',$this->Survey_question_ID);
		$criteria->compare('Question',$this->Question,true);
		$criteria->compare('Created_at',$this->Created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AvailSurveyQuestion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        /**
	 * Initialize new database connection
	 * 
	 * @return type
	 */
        public function getDbConnection()
        {
            return Yii::app()->getComponent('db_cp');
        }
}
