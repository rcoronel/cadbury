<?php

/**
 * This is the model class for table "nas_configuration".
 *
 * The followings are the available columns in table 'nas_configuration':
 * @property integer $Nas_ID
 * @property string $Config
 * @property string $Value
 * @property string $Created_at
 */
class NasConfiguration extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'nas_configuration';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Nas_ID, Config, Created_at', 'required'),
			array('Nas_ID', 'numerical', 'integerOnly'=>true),
			array('Config, Value', 'length', 'max'=>128),
			array('Created_at', 'date', 'format'=>'yyyy-MM-dd hh:mm:ss'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Nas_ID' => 'NAS',
			'Config' => 'Config',
			'Value' => 'Value',
			'Created_at' => 'Date Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Nas_ID',$this->Nas_ID);
		$criteria->compare('Config',$this->Config,true);
		$criteria->compare('Value',$this->Value,true);
		$criteria->compare('Created_at',$this->Created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return NasConfiguration the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Initialize new database connection
	 * 
	 * @return type
	 */
	public function getDbConnection()
	{
		return Yii::app()->getComponent('db_cms');
	}
	
	/**
	 * Get Configuration Value
	 * 
	 * @access	public
	 * @param string $config Configuration Value to be search
	 * @param mixed $default Default Value to be set
	 * @return string
	 */
	public static function getValue($config = '', $default = '')
	{
		$nas_id = 0;
		$context = Yii::app()->controller->context;
		if ($context->nas) {
			$nas_id = $context->nas->id;
		}
		else {
			$nas_id = Yii::app()->controller->nas->id;
		}
		$conf = NasConfiguration::model()->findByAttributes(array('Config' => $config, 'Nas_ID'=>$nas_id));
		
		if (empty($conf)) {
			return $default;
		}
		return $conf->Value;
	}
	
	/**
	 * Set Configuration Value
	 * 
	 * @access	public
	 * @param string $config Configuration Value
	 * @param mixed $value
	 * @return string
	 */
	public static function setValue($config, $value = '')
	{
		$context = Context::getContext();
		
		// Delete previous Config
		NasConfiguration::model()->deleteAllByAttributes(array('Nas_ID'=>$context->nas->id, 'Config'=>$config));
		
		// Insert
		$object = new NasConfiguration;
		$object->Nas_ID = $context->nas->id;
		$object->Config = $config;
		$object->Value = $value;
		$object->validate() && $object->save();
		
		return TRUE;
	}
}
