<?php

/**
 * This is the model class for table "facebook_setting".
 *
 * The followings are the available columns in table 'facebook_setting':
 * @property string $Config
 * @property string $Value
 * @property integer $Nas_ID
 */
class FacebookSetting extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Cadbury_CMS.facebook_setting';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Config, Nas_ID', 'required'),
			array('Nas_ID', 'numerical', 'integerOnly'=>true),
			array('Config', 'length', 'max'=>32),
			array('Value', 'length', 'max'=>128),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Config' => 'Config',
			'Value' => 'Value',
			'Nas_ID' => 'Nas',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Config',$this->Config,true);
		$criteria->compare('Value',$this->Value,true);
		$criteria->compare('Nas_ID',$this->Nas_ID);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FacebookSetting the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Initialize new database connection
	 * 
	 * @return type
	 */
	public function getDbConnection()
	{
		return Yii::app()->getComponent('db_cms');
	}
	
	/**
	 * Get Configuration Value
	 * 
	 * @access	public
	 * @param string $config Configuration Value to be search
	 * @param int $nas_id Nas ID
	 * @param mixed $default Default Value to be set
	 * @return string
	 */
	public static function getValue($config = '', $nas_id = '', $default = '')
	{
		$conf = FacebookSetting::model()->findByAttributes(array('Config' => $config, 'Nas_ID'=>$nas_id));
		
		if (empty($conf)) {
			return $default;
		}
		return $conf->Value;
	}
	
	/**
	 * Set Configuration Value
	 * 
	 * @access	public
	 * @param string $config Configuration Value
	 * @param mixed $value
	 * @return string
	 */
	public static function setValue($config, $value = '')
	{
		// Delete previous Config
		FacebookSetting::model()->deleteAllByAttributes(array('Config' => $config, 'Nas_ID'=>$context->nas->id));
		
		// Insert
		$object = new Configuration;
		$object->Config = $config;
		$object->Value = $value;
		$object->Nas_ID = $context->nas->id;
		$object->validate() && $object->save();
		
		return TRUE;
	}
}
