<?php

/**
 * This is the model class for table "radcheck".
 *
 * The followings are the available columns in table 'radcheck':
 * @property int $id
 * @property string $username
 * @property string $attribute
 * @property string $op
 * @property string $value
 */
class RadiusCheck extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'radcheck';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('username, attribute, op, value', 'required'),
			array('username, attribute', 'length', 'max'=>64),
			array('op', 'length', 'max'=>2),
			array('value', 'length', 'max'=>253),
			array('id', 'numerical', 'integerOnly'=>true),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'=>'ID',
			'username'=>'Username',
			'attribute'=>'Attribute',
			'op'=>'Operation',
			'value'=>'Value'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Configuration the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Initialize new database connection
	 * 
	 * @return type
	 */
	public function getDbConnection()
	{
		return Yii::app()->getComponent('db_radius');
	}
	
	/**
	 * Add user to RadiusCheck
	 * 
	 * @access public
	 * @param object $connection DeviceConnection instance
	 * @param int $availment_id
	 * @return bool
	 */
	public static function addUser(DeviceConnection $connection, $availment_id)
	{
		// input credentials
		$cred = RadiusCheck::model()->findByAttributes(array('username'=>$connection->Username, 'attribute'=>'Cleartext-Password'));
		if (empty($cred)) {
			$cred = new RadiusCheck;
		}
		$cred->username = $connection->Username;
		$cred->attribute = 'Cleartext-Password';
		$cred->op = ':=';
		$cred->value = $connection->Password;
		$cred->validate() && $cred->save();
	
		// input max daily session
		$daily_sess = RadiusCheck::model()->findByAttributes(array('username'=>$connection->Username, 'attribute'=>'Max-Daily-Session'));
		if (empty($daily_sess)) {
			$daily_sess = new RadiusCheck;
		}
		$daily_sess->username = $connection->Username;
		$daily_sess->attribute = 'Max-Daily-Session';
		$daily_sess->op = ':=';
		$daily_sess->value = 86400;
		//$daily_sess->value = 3600;
		$daily_sess->save();
		
		// Get availment details
		$model = Yii::app()->controller->scene_model;
		$level = Yii::app()->controller->level;
		$availment = NasAvailment::model()->findByAttributes(array('Nas_ID'=>$connection->Nas_ID, 'Availment_ID'=>$availment_id));
		$reply = RadiusReply::model()->findByAttributes(array('username'=>$connection->Username));
		if (empty($reply)) {
			$reply = new RadiusReply;
		}
		
		$reply->username = $connection->Username;
		$reply->attribute = 'Session-Timeout';
		$reply->op = ':=';
		$reply->value = $availment->Duration;
		$reply->save();
		
		return TRUE;
	}
}
