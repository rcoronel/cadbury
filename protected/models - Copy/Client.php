<?php

/**
 * This is the model class for table "client".
 *
 * The followings are the available columns in table 'client':
 * @property integer $Client_ID
 * @property integer $Nas_ID
 * @property integer $Client_role_ID
 * @property string $Email
 * @property string $Password
 * @property string $Firstname
 * @property string $Lastname
 * @property integer $Is_active
 * @property string $Created_at
 * @property string $Updated_at
 */
class Client extends ActiveRecord
{
	// string Confirm Password
	public $Confirm_password;
	
	// string New Password
	public $New_password;
	
	// bool Cookie login
	public $Cookie_login;
	
	// object Identity
	private $_identity;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Cadbury_CMS.client';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			
			// INSERT scenario
			array('Client_role_ID, Email, Password, Firstname, Lastname, Is_active, Created_at, Updated_at', 'required', 'on'=>'insert'),
			array('Email', 'unique', 'on' => 'insert'),
			array('Confirm_password', 'compare', 'on' => 'insert', 'compareAttribute' => 'Password'),
			
			// UPDATE scenario
			array('Client_role_ID, Client_ID, Email, Firstname, Lastname, Is_active, Updated_at', 'required', 'on' => 'update'),
			array('Email', 'unique', 'on' => 'update'),
			
			// LOGIN scenario
			array('Email, Password', 'required', 'on' => 'login'),
			
			// CHANGE PASSWORD scenario
			array('Client_ID, Password, New_password, Confirm_password', 'required', 'on'=>'change_Password'),
			array('Confirm_password', 'compare', 'on' => 'change_Password', 'compareAttribute' => 'New_password'),
			array('New_password', 'length', 'min'=>6),
			
			// UPDATE IMAGE
			array('Client_ID', 'required', 'on' => 'update_image'),
			array('Image', 'file', 'allowEmpty'=>true, 'types'=>'jpg, jpeg, png, gif', 'on' => 'update_image'),
			
			// ALL scenario
			array('Client_ID, Nas_ID, Client_role_ID, Is_active', 'numerical', 'integerOnly'=>true),
			array('Email, Password', 'length', 'max'=>128, 'min'=>6),
			array('Firstname, Lastname', 'length', 'max'=>24),
			array('Image', 'length', 'max'=>64),
			array('Created_at, Updated_at', 'date', 'format'=>'yyyy-MM-dd hh:mm:ss'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'nas'=>array(self::BELONGS_TO, 'RadiusNas', 'Nas_ID'),
			'role'=>array(self::BELONGS_TO, 'ClientRole', 'Client_role_ID')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Client_ID' => 'ID',
			'Nas_ID' => 'NAS Device',
			'Client_role_ID' => 'Role',
			'Email' => 'E-mail Address',
			'Password' => 'Password',
			'Confirm_password'=>'Confirm Password',
			'Firstname' => 'First Name',
			'Lastname' => 'Last Name',
			'Is_active' => 'Status',
			'Created_at' => 'Date Added',
			'Updated_at' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('Client_ID',$this->Client_ID);
		$criteria->compare('Nas_ID',$this->Nas_ID);
		$criteria->compare('Client_role_ID',$this->Client_role_ID);
		$criteria->compare('Email',$this->Email,true);
		$criteria->compare('Password',$this->Password,true);
		$criteria->compare('Firstname',$this->Firstname,true);
		$criteria->compare('Lastname',$this->Lastname,true);
		$criteria->compare('Is_active',$this->Is_active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Client the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Initialize new database connection
	 * 
	 * @return type
	 */
	public function getDbConnection()
	{
		return Yii::app()->getComponent('db_cms');
	}
	
	/**
	 * Login the client
	 * 
	 * @access	public
	 * @return void
	 */
	public function login()
	{
		// No identity set
		if ($this->_identity === NULL) {
			$this->_identity = new ClientIdentity($this->Email, $this->Password);
		}
		
		// Authenticate identity
		$auth = $this->_identity->authenticate();
		
               
		if ( ! $auth)
		{
			switch ($this->_identity->errorCode)
			{
				case 1:
					$this->addError('Email', $this->_identity->errorMessage);
				break;
			
				case 2:
					$this->addError('Password', $this->_identity->errorMessage);
				break;
                            
                case 3:
					$this->addError('Inactive', $this->_identity->errorMessage);
				break;
			}
			return FALSE;
		}
		else
		{
			$duration = 0;
			if ($this->Cookie_login) {
				$duration = 3600*24*30; #30 days
			}
			
			Yii::app()->getModule('controlpanel')->client->login($this->_identity, $duration);
			
			// Context object
			$context = Context::getContext();
			$context->client = Client::model()->findByPk($this->_identity->getId());
			
			return true;
		}
	}
	
	/**
	 * Change Password
	 * 
	 * @param string $pw Current hashed Password
	 * @return bool
	 */
	public function change_Password($pw = '')
	{
		// check if correct current Password
		if ( ! CPasswordHelper::verifyPassword($this->Password, $pw))
		{
			$this->addError('current_Password', 'Supplied current Password is incorrect');
			return FALSE;
		}
		
		// check for Password
		if (CPasswordHelper::verifyPassword($this->New_password, $pw))
		{
			$this->addError('Password', 'New Password cannot be the same as the current Password');
			return FALSE;
		}
		
		return TRUE;
	}
	
	public function getName()
	{
		return "{$this->Firstname} {$this->Lastname}";
	}
}
