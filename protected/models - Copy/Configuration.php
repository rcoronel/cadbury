<?php

/**
 * This is the model class for table "Configuration".
 *
 * The followings are the available columns in table 'Configuration':
 * @property string $config
 * @property string $value
 * @property string $Created_at
 */
class Configuration extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'configuration';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Config, Value, Created_at', 'required'),
			array('Config', 'length', 'max'=>16),
			array('Value', 'length', 'max'=>128),
			array('Created_at', 'date', 'format'=>'yyyy-MM-dd hh:mm:ss'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Config' => 'Config',
			'Value' => 'Value',
			'Created_at' => 'Date Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with Values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Config',$this->Config,true);
		$criteria->compare('Value',$this->Value,true);
		$criteria->compare('Created_at',$this->Created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Configuration the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Get Configuration Value
	 * 
	 * @access	public
	 * @param string $config Configuration Value to be search
	 * @param mixed $default Default Value to be set
	 * @return string
	 */
	public static function getValue($config = '', $default = '')
	{
		$conf = Configuration::model()->findByAttributes(array('Config' => $config));
		
		if (empty($conf)) {
			return $default;
		}
		return $conf->Value;
	}
	
	/**
	 * Set Configuration Value
	 * 
	 * @access	public
	 * @param string $config Configuration Value
	 * @param mixed $value
	 * @return string
	 */
	public static function setValue($config, $value = '')
	{
		// Delete previous Config
		Configuration::model()->deleteAllByAttributes(array('Config' => $config));
		
		// Insert
		$object = new Configuration;
		$object->Config = $config;
		$object->Value = $value;
		$object->validate() && $object->save();
		
		return TRUE;
	}
}
