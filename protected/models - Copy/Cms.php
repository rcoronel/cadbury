<?php

/**
 * This is the model class for table "cms".
 *
 * The followings are the available columns in table 'cms':
 * @property integer $Cms_ID
 * @property integer $Nas_ID
 * @property string $Title
 * @property string $Friendly_URL
 * @property string $Content
 * @property string $Created_at
 * @property string $Updated_at
 */
class Cms extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cms';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Nas_ID, Title, Friendly_URL, Content, Created_at, Updated_at', 'required'),
			array('Cms_ID, Nas_ID', 'numerical', 'integerOnly'=>true),
			array('Title', 'length', 'max'=>64),
			array('Friendly_URL', 'length', 'max'=>32),
			array('Created_at, Updated_at', 'date', 'format'=>'yyyy-MM-dd hh:mm:ss'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Cms_ID' => 'ID',
			'Nas_ID' => 'NAS',
			'Title' => 'Title',
			'Friendly_URL' => 'Friendly URL',
			'Content' => 'Content',
			'Created_at' => 'Date Added',
			'Updated_at' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Cms_ID',$this->Cms_ID);
		$criteria->compare('Nas_ID',$this->Nas_ID);
		$criteria->compare('Title',$this->Title,true);
		$criteria->compare('Friendly_URL',$this->Friendly_URL,true);
		$criteria->compare('Content',$this->Content,true);
		$criteria->compare('Created_at',$this->Created_at,true);
		$criteria->compare('Updated_at',$this->Updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Cms the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Initialize new database connection
	 * 
	 * @return type
	 */
	public function getDbConnection()
	{
		return Yii::app()->getComponent('db_cms');
	}
}
