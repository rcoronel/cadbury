<?php

/**
 * This is the model class for table "correlor_log".
 *
 * The followings are the available columns in table 'correlor_log':
 * @property integer $Correlor_log_ID
 * @property integer $Nas_ID
 * @property integer $Device_ID
 * @property string $Url
 * @property string $Request
 * @property string $Response
 * @property integer $Status
 * @property string $Created_at
 */
class CorrelorLog extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'correlor_log_'.date('Y-m-d');
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Status, Url, Request, Created_at', 'required'),
			array('Correlor_log_ID, Nas_ID, Device_ID, Status', 'numerical', 'integerOnly'=>true),
			array('Url', 'length', 'max'=>128),
			array('Created_at', 'date', 'format'=>'yyyy-MM-dd hh:mm:ss'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Correlor_log_ID' => 'ID',
			'Nas_ID' => 'NAS',
			'Device_ID' => 'Device',
			'Url' => 'URL',
			'Request' => 'Request',
			'Response' => 'Response',
			'Status' => 'Status',
			'Created_at' => 'Date Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Correlor_log_ID',$this->Correlor_log_ID);
		$criteria->compare('Device_ID',$this->Device_ID);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('Url',$this->Url,true);
		$criteria->compare('Request',$this->Request,true);
		$criteria->compare('Response',$this->Response,true);
		$criteria->compare('Created_at',$this->Created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CorrelorLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Initialize new database connection
	 * 
	 * @return type
	 */
	public function getDbConnection()
	{
		return Yii::app()->getComponent('db_cp');
	}
	
	public static function createTable($tablename)
	{
		Yii::app()->db_cp->createCommand()->createTable($tablename,
			array(	'Correlor_log_ID' => 'pk',
					'Nas_ID' => "int(11) NOT NULL COMMENT 'NAS'",
					'Device_ID' => "int(11) NOT NULL COMMENT 'Device'",
					'Url' => "varchar(128) NOT NULL COMMENT 'URL'",
					'Request' => "text NOT NULL COMMENT 'Request'",                  
					'Response' => "text NOT NULL COMMENT 'Response'",
					'Status' => "int(11) NOT NULL COMMENT 'Status'",
					'Created_at' => "datetime NOT NULL COMMENT 'Date Added'"),
			'ENGINE=InnoDB DEFAULT CHARSET=utf8'
			);
	}
	
	public static function getToken($table)
	{
		$rows = Yii::app()->db_cp->createCommand()
			->select('*')
			->from("$table")
			->where("Request LIKE '%accessToken%'")
			->queryAll();
		
		$tokens = array();
		foreach ($rows as $r) {
			$decode_row =  CJSON::decode($r['Request']);
			$tokens[] = array('Nas_ID'=>$r['Nas_ID'], 'Device_ID'=>$r['Device_ID'], 'accessToken'=>$decode_row['accessToken'], 'accessTokenExpires'=>date('Y-m-d H:i:s', strtotime($decode_row['accessTokenExpires'])), 'Created_at'=>$r['Created_at']);
		}
		
		return $tokens;
	}
}
