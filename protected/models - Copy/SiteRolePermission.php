<?php

/**
 * This is the model class for table "client_role_permission".
 *
 * The followings are the available columns in table 'client_role_permission':
 * @property integer $Site_role_ID
 * @property integer $Site_menu_ID
 * @property integer $View
 * @property integer $Add
 * @property integer $Edit
 * @property integer $Delete
 */
class SiteRolePermission extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'site_role_permission';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Site_role_ID, Site_menu_ID, View, Add, Edit, Delete', 'required'),
			array('Site_role_ID, Site_menu_ID, View, Add, Edit, Delete', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Site_role_ID, Site_menu_ID, View, Add, Edit, Delete', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Site_role_ID' => 'Role',
			'Site_menu_ID' => 'Menu',
			'View' => 'View',
			'Add' => 'Add',
			'Edit' => 'Edit',
			'Delete' => 'Delete',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Site_role_ID',$this->Site_role_ID);
		$criteria->compare('Site_menu_ID',$this->Site_menu_ID);
		$criteria->compare('View',$this->View);
		$criteria->compare('Add',$this->Add);
		$criteria->compare('Edit',$this->Edit);
		$criteria->compare('Delete',$this->Delete);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SiteRolePermission the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Initialize new database connection
	 * 
	 * @return type
	 */
	public function getDbConnection()
	{
		return Yii::app()->getComponent('db_cms');
	}
}
