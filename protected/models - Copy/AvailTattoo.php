<?php

/**
 * This is the model class for table "tbl_avail_tattoo".
 *
 * The followings are the available columns in table 'tbl_avail_tattoo':
 * @property integer $Avail_tattoo_ID
 * @property integer $Nas_ID
 * @property integer $Site_ID
 * @property integer $Portal_ID
 * @property integer $Access_point_group_ID
 * @property integer $Access_point_ID
 * @property integer $Device_ID
 * @property string $Tattoo_number
 * @property string $Created_at
 */
class AvailTattoo extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'avail_tattoo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Nas_ID, Site_ID, Portal_ID, Access_point_group_ID, Access_point_ID, Device_ID, Tattoo_number, Created_at', 'required'),
			array('Nas_ID, Site_ID, Portal_ID, Access_point_group_ID, Access_point_ID, Device_ID', 'numerical', 'integerOnly'=>true),
			array('Tattoo_number', 'length', 'max'=>32)
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Avail_tattoo_ID' => 'ID',
			'Device_ID' => 'Device',
			'Tattoo_number' => 'Tattoo Number',
			'Created_at' => 'Date Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Avail_tattoo_ID',$this->Avail_tattoo_ID);
		$criteria->compare('Device_ID',$this->Device_ID);
		$criteria->compare('Tattoo_number',$this->Tattoo_number,true);
		$criteria->compare('Created_at',$this->Created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AvailTattoo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Initialize new database connection
	 * 
	 * @return type
	 */
	public function getDbConnection()
	{
		return Yii::app()->getComponent('db_cp');
	}
	
	public static function poll($from, $to, $nas_id = 0)
	{
		$where = "DATE(at.Created_at) BETWEEN '{$from}' AND '{$to}'";
		if ($nas_id) {
			$where .= " AND Nas_ID = {$nas_id}";
		}
		$count = Yii::app()->db_cp->createCommand()
			->select('COUNT(DISTINCT d.Mac_address, at.Nas_ID) as count')
			->from('avail_tattoo at')
			->join('device d', 'd.Device_ID = at.Device_ID')
			->where($where)
			->queryRow();
		
		return (int)$count['count'];
	}
}
