<?php

/**
 * This is the model class for table "connection_avail".
 *
 * The followings are the available columns in table 'connection_avail':
 * @property integer $Connection_avail_ID
 * @property integer $Device_connection_ID
 * @property integer $Availment_ID
 * @property integer $Level
 * @property string $Created_at
 */
class ConnectionAvail extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'connection_avail_'.date('Y-m-d');
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Device_connection_ID, Availment_ID, Level, Created_at', 'required'),
			array('Device_connection_ID, Availment_ID, Level', 'numerical', 'integerOnly'=>true),
			array('Connection_avail_ID, Device_connection_ID, Availment_ID, Created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'connection'=>array(self::BELONGS_TO, 'DeviceConnection', array('Device_connection_ID'=>'Device_connection_ID')),
			'availment'=>array(self::HAS_ONE, 'NasAvailment', array('Availment_ID'=>'Availment_ID'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Connection_avail_ID' => 'ID',
			'Device_connection_ID' => 'Connection',
			'Availment_ID' => 'Availment',
			'Created_at' => 'Date Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Connection_avail_ID',$this->Connection_avail_ID);
		$criteria->compare('Device_connection_ID',$this->Device_connection_ID);
		$criteria->compare('Availment_ID',$this->Availment_ID);
		$criteria->compare('Created_at',$this->Created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ConnectionAvail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Initialize new database connection
	 * 
	 * @return type
	 */
	public function getDbConnection()
	{
		return Yii::app()->getComponent('db_cp');
	}
	
	public static function createTable($tablename)
	{
		Yii::app()->db_cp->createCommand()->createTable($tablename,
			array(	'Connection_avail_ID' => 'pk',
					'Device_connection_ID' => "int(11) NOT NULL COMMENT 'Connection'",
					'Availment_ID' => "int(11) NOT NULL COMMENT 'Availment'",
					'Level' => "int(11) NOT NULL COMMENT 'Level'",
					'Created_at' => "datetime NOT NULL COMMENT 'Date Added'"),
			'ENGINE=InnoDB DEFAULT CHARSET=utf8'
			);
	}
}
