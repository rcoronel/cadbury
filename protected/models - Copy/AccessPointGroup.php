<?php

/**
 * This is the model class for table "access_point_group".
 *
 * The followings are the available columns in table 'access_point_group':
 * @property integer $Access_point_group_ID
 * @property integer $Portal_ID
 * @property string $Name
 * @property integer $Is_active
 * @property string $Created_at
 * @property string $Updated_at
 */
class AccessPointGroup extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'access_point_group';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Portal_ID, Name, Is_active, Created_at, Updated_at', 'required'),
			array('Access_point_group_ID, Portal_ID, Is_active', 'numerical', 'integerOnly'=>true),
			array('Name', 'length', 'max'=>32),
			array('Name', 'unique')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Access_point_group_ID' => 'ID',
			'Portal_ID' => 'Portal',
			'Name' => 'Name',
			'Is_active' => 'Status',
			'Created_at' => 'Date added',
			'Updated_at' => 'Date updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Access_point_group_ID',$this->Access_point_group_ID);
		$criteria->compare('Portal_ID',$this->Portal_ID);
		$criteria->compare('Name',$this->Name,true);
		$criteria->compare('Is_active',$this->Is_active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cms;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AccessPointGroup the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
