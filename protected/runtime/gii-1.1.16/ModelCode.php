<?php
return array (
  'template' => 'default',
  'connectionId' => 'db_cms',
  'tablePrefix' => '',
  'modelPath' => 'application.models',
  'baseClass' => 'ActiveRecord',
  'buildRelations' => '1',
  'commentsAsLabels' => '1',
);
