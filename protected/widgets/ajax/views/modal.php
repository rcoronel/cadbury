<div class="modal fade" id="loader-modal" data-backdrop="static" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content" style="transform: matrix(1, 0, 0, 1, 0, 0);">
			<div class="modal-header">
				<h4 class="modal-title">Loading...</h4>
			</div>
			<div class="modal-body">
				<div class="progress progress-striped active">
					<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="100" aria-valuemin="50" aria-valuemax="100" style="width: 50%">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="confirmation-modal" data-backdrop="static" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">
					<span class="text-success">
						<i class="entypo-info-circled"></i>
						Message
					</span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="modal-body text-center"></div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ajaxSend(function(event, request, settings) {
		var random_pct = 25 + Math.round(Math.random() * 30);
		$('.progress-bar').css('width', '0%');
		setTimeout(function() {
			$('#loader-modal').modal('show', {backdrop: 'static'});
		}, 400);
		
		//$('.progress-bar').css('width', random_pct+'%');
		//$('#modal-4').modal({backdrop: 'static', keyboard: false});
	});

	$(document).ajaxComplete(function(event, request, settings) {
		$('.progress-bar').css('width', '100%');
		setTimeout(function() {
			$('#loader-modal').modal('hide');
		}, 600);
	});
</script>