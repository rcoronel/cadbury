<?php
/**
 * This file contains core interfaces for the API extensions.
 *
 * @link http://www.yiiframework.com/
 * @copyright 2008-2013 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
interface APIInterface
{
	public static function addUser($role);
//	public function deleteUser();
	public static function queryUser();
//	public function blacklistUser();
	
	public static function deleteUser();
	
	/**
	 * Authenticate user in NAS
	 * 
	 * @access public
	 * @param string $username Device Username
	 * @param string $password Device Password
	 * @param string $key AccessController Key (defaults to 'hello')
	 * @return bool
	 */
	public static function authenticateUser($username, $password);
	
}