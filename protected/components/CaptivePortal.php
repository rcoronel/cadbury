<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class CaptivePortal extends Controller
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='/layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
	
	public $nas;
	public $site;
	public $portal;
	public $ssid;
	public $ap_group;
	public $ap;
	public $device;
	
	public $connection;
	public $scenarios;
	public $availment;
	
	// appearance settings
	public $logo;
	public $inside_logo;
	public $center_image;
	public $favicon;
	public $site_title;
	public $splash_img;
	public $css_file;
	
	// scene variables
	public $scene;
	public $scene_model;
	public $level;
	public $user;
	
	/**
	 * Do this before any action is performed
	 * 
	 * @access protected
	 * @param string $action
	 * @return boolean
	 */
	protected function beforeAction($action)
	{
		$token = Yii::app()->request->getQuery('connection_token');
		if ($token) {
			// Connection
			$this->connection = DeviceConnection::model()->findByAttributes(array('connection_token'=>$token));
			DeviceConnection::validateObject($this->connection, 'DeviceConnection');
			
			// Declare NAS
			$this->nas = RadiusNas::model()->findByPk($this->connection->nas_id);
			RadiusNas::validateObject($this->nas, 'RadiusNas');
			
			// Declare Site
			$this->site = Site::model()->findByPk($this->connection->site_id);
			Site::validateObject($this->site, 'Site');
			
			// Declare Portal
			$this->portal = Portal::model()->findByPk($this->connection->portal_id);
			Portal::validateObject($this->portal, 'Portal');
			
			// Declare AP Group
			$this->ap_group = AccessPointGroup::model()->findByPk($this->connection->access_point_group_id);
			if (empty($this->ap_group)) {
				$this->ap_group = new stdClass();
				$this->ap_group->access_point_group_id = 0;
			}
			//AccessPointGroup::validateObject($this->ap_group, 'AccessPointGroup');
			
			// Declare AP
			$this->ap = AccessPoint::model()->findByPk($this->connection->access_point_id);
			if (empty($this->ap)) {
				$this->ap = new stdClass();
				$this->ap->access_point_id = 0;
			}
			//AccessPoint::validateObject($this->ap, 'AccessPoint');
			
			// Declare device
			$this->device = Device::model()->findByPk($this->connection->device_id);
			Device::validateObject($this->device, 'Device');
			
			// --- APPEARANCE
			// Declare logo
			$this->logo = PortalConfiguration::getValue('LOGO');
			if ($this->logo) {
				if ( ! filter_var($this->logo, FILTER_VALIDATE_URL)) {
					$this->logo = Link::image_url("portal/{$this->portal->portal_id}/{$this->logo}");
				}
			}
			
			// Declare inside logo
			$this->inside_logo = PortalConfiguration::getValue('INSIDE_LOGO');
			if ($this->inside_logo) {
				if ( ! filter_var($this->inside_logo, FILTER_VALIDATE_URL)) {
					$this->inside_logo = Link::image_url("portal/{$this->portal->portal_id}/{$this->inside_logo}");
				}
			}
			
			// Declare center image on landing page
			$this->center_image = PortalConfiguration::getValue('CENTER_IMAGE');
			if ($this->center_image) {
				if ( ! filter_var($this->center_image, FILTER_VALIDATE_URL)) {
					$this->center_image = Link::image_url("portal/{$this->portal->portal_id}/{$this->center_image}");
				}
			}
			
			// Declare favicon
			$this->favicon = PortalConfiguration::getValue('FAVICON');
			if ($this->favicon) {
				if ( ! filter_var($this->favicon, FILTER_VALIDATE_URL)) {
					$this->favicon = Link::image_url("portal/{$this->portal->portal_id}/{$this->favicon}");
				}
			}
			
			// Declare splash image
			$this->splash_img = PortalConfiguration::getValue('SPLASH_IMG');
			if ($this->splash_img) {
				if ( ! filter_var($this->splash_img, FILTER_VALIDATE_URL)) {
					$this->splash_img = Link::image_url("portal/{$this->portal->portal_id}/{$this->splash_img}");
				}
			}
			
			// Declare CSS file
			$this->css_file = PortalConfiguration::getValue('CSS_FILE');
			if ($this->css_file) {
				if ( ! filter_var($this->css_file, FILTER_VALIDATE_URL)) {
					$this->css_file = Link::css_url("portal/{$this->portal->portal_id}/{$this->css_file}");
				}
			}
			
			// Declare site title
			$this->site_title = PortalConfiguration::getValue('SITE_TITLE');
		}
		
		parent::beforeAction($action);
		return TRUE;
	}
	
	/**
	 * Show the login page
	 * 
	 * @access protected
	 * @return void
	 */
	protected function initialize()
	{
		// Remove cache
		header("Cache-Control: private, must-revalidate, max-age=0");
		header("Pragma: no-cache");
		header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
		
		// Check current scenario
		self::checkScene();
		
		// Save logs pertaining to the login page
		$connected = new DeviceConnected;
		$connected->nas_id = $this->connection->nas_id;
		$connected->portal_id = $this->connection->portal_id; 
		$connected->device_id = $this->connection->device_id;
		$connected->site_id = $this->connection->site_id;
		$connected->ip_address = $this->connection->ip_address;
		$connected->access_point_group_id = $this->connection->access_point_group_id;
		$connected->access_point_id = $this->connection->access_point_id;
		$connected->ssid = $this->connection->ssid;
		$connected->validate() && $connected->save();
	}
	
	/**
	 * Check current scene of the user / device
	 * 
	 * @access protected
	 * @return void
	 */
	protected function checkScene()
	{
		// Check for current level
		$latest_availment = ConnectionAvail::model()->findByAttributes(array('device_connection_id'=>$this->connection->device_connection_id), array('order'=>'Created_at DESC'));
		if ($latest_availment) {
			$this->level = $latest_availment->level+1;
		}
		else {
			$this->level = 1;
		}
		
		$count = 0;
		$availments = Availment::model()->findAll();
		
		foreach ($availments as $a) {
			$count = DeviceAvailment::model()->exists("availment_id = {$a->availment_id}"
			. " AND device_id = {$this->device->device_id}"
			. " AND portal_id = {$this->portal->portal_id}"
			. " AND DATE(created_at) < DATE(NOW())");
			if ($count) {
				break;
			}
		}
		
		// New user means no availment method prior to the current date
		// Returning user means have prior availment method
		if ( ! $count) {
			$this->scene = 'new';
			$this->scene_model = 'ScenarioAvailment';
		}
		else {
			$this->scene = 'returning';
			$this->scene_model = 'ScenarioReturning';
		}
		
		// Get user details
		if ( ! $this->user) {
			$fb = AvailFacebook::model()->with('dev_avail')->find("device_id = {$this->device->device_id} AND portal_id = {$this->portal->portal_id}");
			if ($fb) {
				$this->user = $fb->Name;
			}
		}

		if ( ! $this->user) {
			$mobtel = AvailMobile::model()->with('dev_avail')->find("device_id = {$this->device->device_id} AND portal_id = {$this->portal->portal_id}");
			if ($mobtel) {
				$this->user = $mobtel->msisdn;
			}
		}
	}
	
	/**
	 * Show availments on the page
	 * 
	 * @access protected
	 * @return void
	 */
	protected function showAvailments()
	{
		$model = $this->scene_model;
		
		$scenarios = self::getScenarios($model);
		$availments = self::getAvailments($scenarios['scenarios']);
		
		$content = Cms::model()->findByAttributes(array('portal_id'=>$this->portal->portal_id, 'friendly_url'=>'service-agreement'));
		$url = $this->createAbsoluteUrl('scene_handler/terms', array('connection_token'=>$this->connection->connection_token));
		$this->render('index', array('availments'=>$availments, 'url'=>$url, 'content'=>$content, 'availment'=>$scenarios['availment'], 'connection_token'=>$this->connection->connection_token));
	}
	
	/**
	 * Get scenarios of the current user / device
	 * 
	 * @access protected
	 * @param string $model
	 * @return array
	 */
	protected function getScenarios($model)
	{
		// Get scenarios
		$device_avails = DeviceScenario::model()->findAllByAttributes(array('device_id'=>$this->device->device_id, 'portal_id'=>$this->portal->portal_id, 'scene'=>$this->scene));
		if ( ! empty($device_avails)) {
			$this->scenarios = $model::model()->findScenario($device_avails, $this->portal->portal_id);
		}
		
		// This conditions catches the scenario for returning users wherein scenarios are changed
		// Check if current scene is returning and empty availments
		// Check previous availments to get scenario
		if ($this->scene == 'returning' AND empty($device_avails)) {
			$device_avails = DeviceScenario::model()->findAllByAttributes(array('device_id'=>$this->device->device_id, 'portal_id'=>$this->portal->portal_id, 'scene'=>'new'));
			if ( ! empty($device_avails)) {
				$this->scenarios = ScenarioAvailment::model()->findScenario($device_avails, $this->portal->portal_id);
			}
		}
		
		// Availment container
		$availment = array();

		// Get acquired availment methods
		$latest_availment = ConnectionAvail::model()->findByAttributes(array('device_connection_id'=>$this->connection->device_connection_id), array('order'=>'created_at DESC'));
		$latest_availments = ConnectionAvail::model()->findAllByAttributes(array('device_connection_id'=>$this->connection->device_connection_id), array('order'=>'created_at DESC'));

		// Has acquired availments within the day
		if ( ! empty($latest_availment) &&  ! empty($latest_availments)) {
			// But empty scenario list
			if (empty($this->scenarios)) {
				// Set where string
				$where_string = "({$this->scene}.availment_id = {$latest_availment->availment_id} AND {$this->scene}.level = {$latest_availment->level} AND portal_id = {$this->portal->portal_id})";

				// Append to where string
				foreach ($latest_availments as $la) {
					if ($la->availment_id != $latest_availment->availment_id && $la->level != $latest_availment->level) {
						$where_string .= " OR ({$this->scene}.availment_id = {$la->availment_id} AND {$this->scene}.level = {$la->level})";
					}
				}
				
				// Get scenarios based on where string
				$alt_scenarios = Scenario::model()->with($this->scene)->findAll($where_string);
				$count = count($latest_availments);

				// Remove scenarios incomplete scenarios
				foreach ($alt_scenarios as $index=>$as) {
					if ($count != count($as->{$this->scene})) {
						unset($alt_scenarios[$index]);
					}
				}

				// Get scenarios
				if ( ! empty($alt_scenarios)) {
					foreach ($alt_scenarios as $as) {
						$scenarios[] = Scenario::model()->with($this->scene)->findByAttributes(array('portal_id'=>$this->portal->portal_id, 'scenario_id'=>$as->scenario_id), array('order'=>'level ASC'));
					}
				}
				else {
					$scenarios = Scenario::model()->with($this->scene)->findAllByAttributes(array('portal_id'=>$this->portal->portal_id), array('order'=>'level ASC'));
				}
			}
			// Or with scenario list
			else {
				$criteria = new CDbCriteria;
				$criteria->addCondition("level >= {$this->level}");
				$criteria->addInCondition('t.scenario_id', $this->scenarios);
				$criteria->order = 'level ASC';
				$scenarios = Scenario::model()->with($this->scene)->findAll($criteria);
			}
			
			// latest availment
			$availment = $latest_availment;
		}
		// No acquired availments within the day
		else {
			// Without scenario list
			if (empty($this->scenarios)) {
				$scenarios = Scenario::model()->with($this->scene)->findAllByAttributes(array('portal_id'=>$this->portal->portal_id), array('order'=>'level ASC'));
			}
			// With scenario list
			else {
				$criteria = new CDbCriteria;
				$criteria->addCondition("level >= {$this->level}");
				$criteria->addInCondition('t.scenario_id', $this->scenarios);
				$criteria->order = 'level ASC';
				$scenarios = Scenario::model()->with($this->scene)->findAll($criteria);
			}
		}
		
		return array('scenarios'=>$scenarios, 'availment'=>$availment);
	}
	
	/**
	 * Get availments based on scenarios
	 * 
	 * @access protected
	 * @param array $scenarios
	 * @return type
	 */
	protected function getAvailments($scenarios)
	{
		// Get acquired availment methods
		$latest_availment = ConnectionAvail::model()->findByAttributes(array('device_connection_id'=>$this->connection->device_connection_id), array('order'=>'Created_at DESC'));
		$latest_availments = ConnectionAvail::model()->findAllByAttributes(array('device_connection_id'=>$this->connection->device_connection_id), array('order'=>'Created_at DESC'));
		
		// Get all availments from all scenarios
		$availments = array();
		foreach ($scenarios as $s) {
			if ( ! empty($s)) {
				foreach ($s->{$this->scene} as $index => $a) {
					$availments[] = $a;
				}
			}
		}
		
		// If there are availments available
		if ( ! empty($availments)) {
			// Remove currently availed for today
			// Useful when the scenarios are updated in the CMS
			if ( ! empty ($latest_availments)) {
				foreach ($availments as $index => $a) {
					foreach ($latest_availments as $la) {
						if ($la->availment_id == $a->availment_id) {
							unset($availments[$index]);
						}
					}
				}
			}
			
			// Remove non-recurring which were already availed
			foreach ($availments as $index => $a) {
				// Get availment model
				$model = $a->availment->model;

				// Check if existing
				$is_exists = DeviceAvailment::model()->exists("availment_id = {$a->availment_id}"
			. " AND device_id = {$this->device->device_id}"
			. " AND portal_id = {$this->portal->portal_id}"
			. " AND DATE(created_at) < DATE(NOW())");

				// Check configuration on NAS
				$nas_availment = PortalAvailment::model()->findByAttributes(array('portal_id'=>$a->scenario->portal_id, 'availment_id'=>$a->availment_id));
				$a->availment->duration = $nas_availment->duration / 60;
				$a->availment->description = $nas_availment->description;
				
				// Check if already existing and non-recurring
				if ($is_exists AND ! $nas_availment->is_recurring) {
					unset($availments[$index]);
				}
			}
			
			// Remove duplicates
			foreach ($availments as $index_a => $a) {
				foreach ($availments as $index_b => $b) {
					if ($index_a != $index_b && $a->level == $b->level && $a->availment_id == $b->availment_id) {
						unset($availments[$index_a]);
					}
				}
			}
			
			// Sort availments by level in ascending order
			usort($availments, function($a, $b) {  
				if( $a->level == $b->level) {
					return 0; 
				}
				return $a->level > $b->level ? 1 : -1;
			});
		}
		
		if ( ! empty($availments)) {
			// Change level to the first level available
			$this->level = $availments[0]->level;
			$this->connection->level = $this->level;
			if ( ! $this->connection->validate() OR ! $this->connection->save()) {
				$this->redirect(array('scene_handler/', 'connection_token'=>$this->connection->connection_token));
			}

			// Remove availments which is not equal to current level
			foreach ($availments as $index=>$a) {
				if ($a->level != $this->connection->level) {
					unset($availments[$index]);
				}
			}
		}
		
		// Sort by name
		if ( ! empty($availments)) {
			usort($availments, function($a, $b) {
				return strcmp($a->availment->name, $b->availment->name);
			});
		}
		
		return $availments;
	}
	
	/**
	 * Get remaining session
	 * 
	 * @access protected
	 * @return array
	 */
	protected function remainingSession()
	{
		// Check if there is existing duration
		$session_duration = Device::getSessionTime($this->connection);
		$session_max = RadiusReply::getSessionMax($this->connection->username);
		
		// Here, we're catch 0 session time recorded in Radius
		// We set it to 1 in order to let the system add the duration to the computation
		if (isset($session_duration['current_session_time']) AND $session_duration['current_session_time'] == 0) {
			$session_duration['current_session_time'] = 1;
		}
		
		if ($session_duration['current_session_time'] AND ($session_duration['current_session_time'] < $session_max->value)) {
			return array('duration'=>$session_duration['current_session_time'], 'max'=>$session_max->value);
		}
		return array();
	}
	
	/**
	 * Show the remaining duration on the page
	 * 
	 * @access protected
	 * @param type $current_session_time
	 * @param type $max_session_time
	 * @return void
	 */
	protected function showRemaining($current_session_time, $max_session_time)
	{
		$remaining_session = (int)$max_session_time - (int)$current_session_time;
		$duration = 0;
		$unit = 'minutes';
		// If duration is less than 90 seconds
		if ($remaining_session < 90 ) {
			$duration = 1;
			$unit = 'minute';
		}
		else {
			if ($remaining_session >= 3600) {
				$duration = number_format(($remaining_session / 60) / 60, 2);
				$unit = 'hours';
			}
			else {
				$duration = number_format(($remaining_session / 60), 2);
				$unit = 'minutes';
			}
		}
		
		// Get latest availed method
		$latest_avail = ConnectionAvail::model()->findByAttributes(array('device_connection_id'=>$this->connection->device_connection_id), array('order'=>'created_at DESC'));
		$availment = PortalAvailment::model()->findByAttributes(array('portal_id'=>$this->portal->portal_id, 'availment_id'=>$latest_avail->availment_id));
		
		if (Yii::app()->request->isPostRequest) {
			$this->connect($availment);
		}
		
		$this->render('remaining_session', array('availment'=>$latest_avail, 'duration'=>$duration, 'unit'=>$unit,));
	}
	
	/**
	 * Sign-in based on POST protocol
	 * 
	 * @access private
	 * @param int $id
	 * @param string $action
	 * @return void
	 */
	protected function signIn($id, $action = 'login')
	{
		if (empty($id)) {
			$id = Yii::app()->request->getPost('availment_id');
		}
		
		$availment = PortalAvailment::model()->findByAttributes(array('portal_id'=>$this->portal->portal_id, 'availment_id'=>$id));

		// Get availment of Mobile
		$globe_duration = 0;
		$xglobe_duration = 0;
		$mobile_availment = Availment::model()->findByAttributes(array('model'=>'AvailMobile'));

		if ($mobile_availment && $availment->availment_id == $mobile_availment->availment_id) {
			// Get availment settings
			$globe_duration = AvailmentSetting::getValue($mobile_availment->availment_id, 'GLOBE_DURATION');
			$xglobe_duration = AvailmentSetting::getValue($mobile_availment->availment_id, 'XGLOBE_DURATION');

			// If settings are declared, check for AvailMobile object
			if ($globe_duration || $xglobe_duration) {
				$mobile = AvailMobile::model()->with('dev_avail')->find("portal_id = {$this->portal->portal_id} AND device_id = {$this->device->device_id} AND is_validated = 1", array('order'=>'created_at DESC'));
				if ( ! empty($mobile)) {
					$msisdn = substr($mobile->msisdn, 1);
					$prefix = Yii::app()->db->createCommand()->select('*')
						->from('Unifi_Captive.plan_prefix')->where("LEFT(prefix, 3) = LEFT({$msisdn}, 3)")
						->queryRow();

					if ($prefix) {
						if ($prefix['plan_id'] == 1 OR $prefix['plan_id'] == 2 OR $prefix['plan_id'] == 3 OR $prefix['plan_id'] == 9) {
							if ($globe_duration) {
								$availment->duration = $globe_duration * 60;
							}
						}
						else {
							if ($xglobe_duration) {
								$availment->duration = $xglobe_duration * 60;
							}
						}
					}
				}
			}
		}
		
		$session_array = self::remainingSession();
		if (isset($session_array['duration']) AND $session_array['duration'] < $session_array['max']) {
			$this->connect($availment);
		}
		
		$this->render('login', array('availment'=>$availment, 'action'=>$action));
	}
	
	/**
	 * Give temporary access to enable third party login
	 * 
	 * @access public
	 * @return bool
	 */
	public function giveTempAccess($username = 'fb_guest', $password = 'p@ssword123')
	{
		if (empty($this->nas) OR empty($this->device) OR empty($this->connection)) {
			throw new CHttpException(400,Yii::t('yii','Connection cannot be set. Please try again'));
		}
		
		// Add record
		$guest = RadiusCheck::model()->findByAttributes(array('username'=>$username));
		if (empty($guest)) {
			// Add to RadiusCheck
			$guest = new RadiusCheck;
			$guest->username = $username;
			$guest->attribute = 'Cleartext-password';
			$guest->op = ':=';
			$guest->value = $password;
			$guest->validate() && $guest->save();
			
			$guest = new RadiusCheck;
			$guest->username = $username;
			$guest->attribute = 'Simultaneous-Use';
			$guest->op = ':=';
			$guest->value = 200;
			$guest->validate() && $guest->save();
			
			// Add to RadiusReply
			$reply = new RadiusReply;
			$reply->username = $username;
			$reply->attribute = 'Session-Timeout';
			$reply->op = ':=';
			$reply->value = 300;
			
			$reply->validate() && $reply->save();
		}
		
		switch ($this->nas->brand) {
			case 'aruba':
				if (Aruba::authenticateUser($username, $password)) {
					return TRUE;
				}
			break;
		}
	}
	
	/**
	 * Remove temporary access
	 * 
	 * @access public
	 * @return bool
	 */
	public function removeTempAccess()
	{
		if (empty($this->nas) OR empty($this->device) OR empty($this->connection)) {
			throw new CHttpException(400,Yii::t('yii','Connection cannot be set. Please try again'));
		}
		
		switch ($this->nas->brand) {
			case 'aruba':
				if (Aruba::deleteUser()) {
					return TRUE;
				}
			break;
		}
	}
	
	/**
	 * Change Role of the user
	 * 
	 * @access public
	 * @return boolean
	 * @throws CHttpException
	 */
	public function changeRole($role)
	{
		return TRUE;
		
		if (empty($this->nas) OR empty($this->device) OR empty($this->connection)) {
			throw new CHttpException(400,Yii::t('yii','Connection cannot be set. Please try again'));
		}
		
		switch ($this->nas->brand) {
			case 'aruba':
				if (Aruba::addUser($role)) {
					return TRUE;
				}
			break;
		}
	}
	
	public function changeRoleTest($role)
	{
		if (empty($this->nas) OR empty($this->device) OR empty($this->connection)) {
			throw new CHttpException(400,Yii::t('yii','Connection cannot be set. Please try again'));
		}
		
		switch ($this->nas->brand) {
			case 'aruba':
				if (Aruba::addUser($role)) {
					return TRUE;
				}
			break;
		}
	}
	
	/**
	 * Authenticate the user
	 * 
	 * @access public
	 * @return boolean
	 * @throws CHttpException
	 */
	public function authenticateAccess()
	{
		if (empty($this->nas) OR empty($this->device) OR empty($this->connection)) {
			throw new CHttpException(400,Yii::t('yii','Connection cannot be set. Please try again'));
		}
		
		switch ($this->nas->brand) {
			case 'aruba':
				if (Aruba::authenticateUser($this->connection->username, $this->connection->password)) {
					return TRUE;
				}
			break;
		}
	}
	
	/**
	 * Create or save device availment record
	 * 
	 * @param int $availment_id
	 * @param boolean $save
	 * @return \DeviceAvailment
	 */
	public function saveDeviceAvailment($availment_id, $save = FALSE)
	{
		// Create record in device availment
		$dev_avail = new DeviceAvailment;
		$dev_avail->availment_id = $availment_id;
		$dev_avail->nas_id = $this->nas->id;
		$dev_avail->site_id = $this->site->site_id;
		$dev_avail->portal_id = $this->portal->portal_id;
		$dev_avail->device_id = $this->device->device_id;
		$dev_avail->access_point_id = $this->ap->access_point_id;
		$dev_avail->access_point_group_id = $this->ap_group->access_point_group_id;
		$dev_avail->ssid = $this->connection->ssid;
		if ($save) {
			$dev_avail->validate() && $dev_avail->save();
		}
		
		return $dev_avail;
	}
	
	/**
	 * Verify connection details
	 * 
	 * @param int $availment
	 * @param bool $ajax_redirect
	 * @return boolean
	 */
	public function connect(PortalAvailment $availment, $ajax_redirect = 0)
	{
		// Check level, must not be 0
		if ( ! $this->connection->level) {
			$this->redirect(array('login/', 'connection_token'=>$this->connection->connection_token));
		}
		
		// Check if correct availment
		// Based on Availment ID, level, Portal ID
		$model = $this->scene_model;
		$avail_scenario = $model::model()->with('scenario')->find("availment_id = {$availment->availment_id} AND level = {$this->connection->level} AND portal_id = {$this->portal->portal_id}");
		if (empty($avail_scenario)) {
			$this->redirect(array('login/', 'connection_token'=>$this->connection->connection_token));
		}
		
		// Add user to Radius DB
		RadiusCheck::addUser($this->connection, $availment);
		$this->changeRole('Cadbury-Cloud-logon');
		$this->authenticateAccess();
		
		// Insert record for connection availment
		$connect_avail = ConnectionAvail::model()->findByAttributes(array('device_connection_id'=>$this->connection->device_connection_id, 'availment_id'=>$availment->availment_id));
		if (empty($connect_avail)) {
			$connect_avail = new ConnectionAvail;
		}
		$connect_avail->device_connection_id = $this->connection->device_connection_id;
		$connect_avail->availment_id = $availment->availment_id;
		$connect_avail->level = $this->connection->level;
		$connect_avail->duration = $availment->duration;
		$connect_avail->validate() && $connect_avail->save();
		
		// Insert record for device scenario
		$device_scenario = DeviceScenario::model()->findByAttributes(array('device_id'=>$this->device->device_id, 'portal_id'=>$this->portal->portal_id, 'availment_id'=>$availment->availment_id, 'level'=>$this->connection->level));
		if (empty($device_scenario)) {
			$device_scenario = new DeviceScenario;
		}
		$device_scenario->portal_id = $this->portal->portal_id;
		$device_scenario->device_id = $this->device->device_id;
		$device_scenario->availment_id = $availment->availment_id;
		$device_scenario->level = $this->connection->level;
		$device_scenario->scene = $this->scene;
		$device_scenario->validate() && $device_scenario->save();
		
		if ( ! $ajax_redirect) {
			$this->redirect(array(	'access/',
									'connection_token'=>$this->connection->connection_token,
									'availment_id'=>$availment->availment_id));
		}
		
		return TRUE;
	}
}