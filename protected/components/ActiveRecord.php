<?php
/**
 * ActiveRecord is the customized base CActiveRecord class.
 * All model classes for this application should extend from this base class.
 * 
 * @author Ryan Coronel <ryancoronel@gmail.com>
 * @category Components
 */
class ActiveRecord extends CActiveRecord
{
	/**
	 * Check if object is properly instantiated
	 * 
	 * @access public
	 * @param object $object
	 * @param string $class Classname
	 * @param string $url URL to redirect
	 * @return void
	 */
	public static function validateObject($object, $class = '', $url = '')
	{
		$controller_name = Yii::app()->controller->id;
		//$module = Yii::app()->controller->module->id;
		
		if ( ! is_object($object)) {
			Yii::app()->user->setFlash('fail', 'No record found from the reference');
			if ($url) {
				Yii::app()->controller->redirect($url);
			}
			throw new CHttpException(500,Yii::t('yii','No details found regarding your request. Please try again.'));
		}
		
		if ( ! empty($class)) {
			// check for proper instance
			if ( ! $object instanceof $class) {
				Yii::app()->user->setFlash('fail', 'Object reference is invalid');
				Yii::app()->controller->redirect(array("{$controller_name}/"));
				//throw new CHttpException(500,Yii::t('yii','Details not found. Error encountered. Please try again.'));
			}
		}
	}
	
	/**
	 * Set attributes on search to session
	 * If session is already set, set it to attributes
	 * 
	 * @access public
	 * @param array $post POST data
	 * @return array
	 */
	public static function setSessionAttributes($post)
	{
		// Get current controller
		$controller_name = Yii::app()->controller->id;
		$session_name = Yii::app()->request->requestUri;
		
		// Check if form is submitted
		if ( ! empty($post)) {
			// Set session by controller name
			// Redirect to avoid POST confirmation on browsers
			Yii::app()->user->setState($session_name, $post);
			//Yii::app()->controller->redirect(array("{$controller_name}/"));
		}
		else {
			
			// Check if resetting the search form
			// Empty POST but form is submitted
			// Redirect to avoid POST confirmation on browsers
			if (Yii::app()->request->isPostRequest) {
				Yii::app()->user->setState($session_name, NULL);
				//Yii::app()->controller->redirect(array("{$controller_name}/"));
			}
		}
		
		// For data checking purposes, pass the session instead of the post
		return Yii::app()->user->getState($session_name);
	}
	
	/**
	 * Override the CActiveRecord beforeValidate() function
	 * Function to be executed before validation
	 * 
	 * @access protected
	 * @return bool
	 */
	protected function beforeValidate()
	{
		$scenario = $this->getScenario();
		
		// Automatically add / update date parameters based on scenario
		switch ($scenario) {
			case 'insert':
			case 'forgot_password':
				if (array_key_exists('created_at', $this->attributes)) {
					$this->created_at = date('Y-m-d H:i:s');
				}

				if (array_key_exists('updated_at', $this->attributes)) {
					$this->updated_at = date('Y-m-d H:i:s');
				}
			break;	
		
			case 'update':
				if (array_key_exists('updated_at', $this->attributes)) {
					$this->updated_at = date('Y-m-d H:i:s');
				}
			break;
		}
		
		return parent::beforeValidate();
	}
}