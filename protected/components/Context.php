<?php
/*
|--------------------------------------------------------------------------
| Context Class
|--------------------------------------------------------------------------
|
| Handles context variables that are used throughout the system
|
| @category		Components
| @author		Ryan Coronel
*/
class Context
{
	/* object instance */
	protected static $instance;
	
	/* object Menu */
	public $menus;
	public $submenus;
	
	/* object Admin */
	public $admin;
	
	/* object Device */
	public $device;
	
	/* object Connection */
	public $connection;
	
	/* object NAS */
	public $nas;
	
	/* object SiteAdmin */
	public $site_admin;
	public $site;
	
	public $portal_admin;
	public $portal;

	// --------------------------------------------------------------------

	/*
	 * Get object Context
	 *
	 * @access	public
	 * @return	object $instance
	 */
	public static function getContext()
	{
		if ( ! isset(self::$instance)) {
			self::$instance = new Context();
		}
		
		return self::$instance;
	}
	
}
