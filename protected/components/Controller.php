<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
	
	public $context;
	/**
	 * Constructor
	 *
	 * Called automatically
	 * Inherits functions from the parent class
	 * 
	 * @access	public
	 * @param	string $id
	 * @param	string $module
	 */
	public function __construct($id, $module = null)
	{
		parent::__construct($id, $module);
		
		$this->context = Context::getContext();
	}
	
	/**
	 * Return form validation errors of an object
	 * 
	 * @access	protected
	 * @param	string $message
	 * @param	string $url Redirect URL
	 * @return	json
	 */
	protected function displayFormSuccess($message = 'Record successfully saved!', $url = '')
	{
		// check if AJAX request
		if (Yii::app()->request->getPost('ajax') OR Yii::app()->request->isAjaxRequest) {
			$json_array = array('status'=>1, 'message'=>$message, 'url'=>$url);
			die(CJSON::encode($json_array));
		}
	}
	
	/**
	 * Return form validation errors of an object
	 * 
	 * @access	protected
	 * @param	object $object CModel instance
	 * @return	json
	 */
	protected function displayFormError($object)
	{
		if (Yii::app()->request->getPost('ajax') OR Yii::app()->request->isAjaxRequest) {
			$error_fields = array();
			foreach ($object->errors as $field => $error_message) {
				array_push($error_fields, CHtml::modelName($object).'_'.$field);
			}
			$json_array = array('status'=>0, 'message'=>CHtml::errorSummary($object, '<strong>Ooops!</strong>'), 'fields' => $error_fields);
			die(CJSON::encode($json_array));
		}
	}
}