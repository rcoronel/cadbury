<?php

class AvailmentWidget extends CWidget
{
	public $self;
	public $self_portal;
	public $action;
	public $reference;
	
	/**
	 * Check if the given availment is already availed
	 */
	public function checkIfAvailed($availment_id = 0, $portal_id = 0, $device_id = 0)
	{
		$availment_id = ($availment_id) ? $availment_id : $this->self->availment_id;
		$portal_id = ($portal_id) ? $portal_id : $this->owner->portal->portal_id;
		$device_id = ($device_id) ? $device_id : $this->owner->device->device_id;
		
		// Check if already availed for today
		$is_availed = DeviceAvailment::model()->exists("device_id = {$device_id} AND portal_id = {$portal_id} AND availment_id = {$availment_id} AND DATE(created_at) = DATE(NOW())");
		if ($is_availed) {
			return TRUE;
		}
		
		return FALSE;
	}
}