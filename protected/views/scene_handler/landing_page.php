<div class="globe-container" id="continue-holder">
	<div class="landing-message">
		<?php echo (PortalConfiguration::getValue('LANDING_MESSAGE') ? PortalConfiguration::getValue('LANDING_MESSAGE') : "");?>
	</div>
	<?php if ($this->logo):?>
		<?php echo CHtml::image($this->logo, $this->site_title, array('class'=>'img-responsive continue-logo'));?>
	<?php endif;?>
	<div class="welcome-text">
		<?php if ($this->center_image):?>
			<?php echo CHtml::image($this->center_image, $this->site_title, array('class'=>'img-responsive continue-logo middle-logo'));?>
		<?php endif;?>
	</div>
	<div class="row">
		<div id="globe-btn-holder"  class="btn-holder">
			<?php echo CHtml::htmlButton('GET CONNECTED', array('class'=>'btn btn-wifi btn-continue btn-shadow', 'onclick'=>"window.location='{$url}'"));?>
			<div class="terms-policy-holder">
				<p class="terms-policy">By clicking the button, you agree to the </br> <a href="<?php echo $terms;?>">Terms of Service</a> and <a href="<?php echo $policy;?>">Privacy Policy</a></p>
			</div>
		</div>
	</div>
</div>
		
<div class="modal fade" id="loader-modal" data-backdrop="static" data-keyboard="false" tabindex="-1">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('button').on('click',function(e){
			$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		});
	});
</script>