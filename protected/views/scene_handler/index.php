<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 text-center">
		<?php if ( ! empty($availments)):?>
			<?php if ($this->user):?>
				<?php echo str_replace('::user::', $this->user, NasConfiguration::getValue('WELCOME_BACK'));?>
			<?php else:?>
				<h3>Welcome</h3>
			<?php endif; ?>
			<?php if ( ! empty($availment)):?>
				<h3>Your FREE <?php echo $availment->Duration / 60;?> minutes has expired</h3>

				<!-- <br>
				<p>Should you wish to continue using our free WiFi service, please select an availment method below.</p>
				<p>By logging in to <?php //$this->site_title;?> wireless service, you have read, understood, and agreed to the <a href="<?php //echo $url;?>">Terms & Conditions</a> regarding the use of service</p> -->
			<?php endif;?>
		
			<div class="availment-holder globe-availment-holder">
				<div class="reg-btn-holder reg-btn-holder-with-text">
					<ul class="btn-list list-unstyled btn-list-with-title">
						<?php foreach ($availments as $a):?>
							<li>
								<div class="lbl-button-container">
									<div class="lbl-button-text">
									<?php echo $a->availment->Description; ?>
									</div>
								</div>
								<?php $this->widget("widgets.{$a->availment->Code}", array('self' => $a));?>
							</li>
						<?php endforeach;?>
					</ul>
				</div>
			</div>
		<?php else:?>
			<h3>Your FREE <?php echo $availment->Duration / 60;?> minutes has expired for today</h3>
			<p>Thank you for using Globe's <strong>FREE</strong>WiFi service</p>
			<p>See you again soon!</p>
		<?php endif;?>
	</div>
</div>

<div class="modal fade" id="loader-modal" data-backdrop="static" data-keyboard="false" tabindex="-1">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	
	$('form').on('submit',function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		//e.preventDefault();
	});
	
	<?php if (isset($connection_token)):?>
	$("a").each(function(e) {
		
		var href = $(this).attr('href');
		var connection_token = "<?php echo $connection_token; ?>";
		href += "&connection_token=" + connection_token;
		$(this).attr("href",href);
		
	});
	<?php endif;?>

//	$(".send").on('click',function(e){
//		var availment_id = $(this).prev().val();
//		console.log("a: "+availment_id);
//		// return false;
//			$.ajax(
//			{
//				url : '<?php //echo $a_url; ?>',
//				type: "GET",
//				data: {'availment_id': availment_id},
//				dataType: 'json',
//				success:function(data, textStatus, jqXHR)
//				{
//					if(data.message==="success"){
//						console.log("ok");
//					}else{
//						console.log("error");
//					}
//					
//					console.log(data);
//					// return false;
//				},
//				error: function(jqXHR, textStatus, errorThrown)
//				{
//					console.log(errorThrown);
//					// return false;
//				}
//			});
//			// e.preventDefault();
//		});

});
</script>