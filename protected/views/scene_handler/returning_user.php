<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 text-center tos-adj">
		<h3>Hooray! You're back!</h3>
		<br>
		<h4>Welcome,</h4>
		<h2><?php echo $name;?></h2>
		<br>
		<h3>Terms of Service Agreement</h3>
		<?php echo $content->Content;?>

		<?php echo CHtml::beginForm();?>
			<?php echo CHtml::hiddenField('availment_id', 99);?>
			<?php echo CHtml::htmlButton('CONTINUE BROWSING', array('type'=>'submit', 'class'=>'btn btn-wifi terms-btn'));?>
		<?php echo CHtml::endForm();?>
	</div>
</div>

<script>
$(document).ready(function(){
	$('form').on('submit',function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		//e.preventDefault();
	});
});
</script>