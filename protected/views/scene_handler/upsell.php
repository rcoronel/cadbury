<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 text-center">
		<h3>Your free <?php echo $availment->Duration / 60;?> minutes has expired</h3>
		<br>
		<p>Thank you for using <?php echo $this->site_title;?> wireless internet service!</p>
		<p>You may upgrade to Tattoo Platinum to enjoy additional internet from <?php echo $this->site_title;?></p>
		<div class="availment-holder globe-availment-holder">
			<div class="reg-btn-holder reg-btn-holder-with-text">
				<ul class="btn-list list-unstyled btn-list-with-title">
					<li>
						<?php echo CHtml::beginForm();?>
							<?php echo CHtml::hiddenField('availment_id', 7);?>
							<?php echo CHtml::htmlButton(html_entity_decode('Sign in with Tattoo Platinum'), array('type'=>'submit', 'class'=>'btn btn-wifi btn-tattoo'));?>
						<?php echo CHtml::endForm();?>
					</li>
					<li>
						<?php echo CHtml::beginForm();?>
							<?php echo CHtml::hiddenField('availment_id', 7);?>
							<?php echo CHtml::hiddenField('view_plan', 1);?>`
							<?php echo CHtml::htmlButton(html_entity_decode('View Plans'), array('type'=>'submit', 'class'=>'btn btn-wifi btn-tattoo'));?>
						<?php echo CHtml::endForm();?>
					</li>
					<li>
						<a class="btn btn-wifi btn-close btn-shadow with-cancel" id="cancelBtn" href="<?php echo $url;?>">Cancel</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="loader-modal" data-backdrop="static" data-keyboard="false" tabindex="-1">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	
	$('form').on('submit',function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		//e.preventDefault();
	});
});
</script>