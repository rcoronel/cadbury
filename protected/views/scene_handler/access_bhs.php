<!--Ads Image Containter-->
		<section class="container-fluid" id="bhs-bg-v1">
			<div class="row">
				<div class="col-xs-12 text-right remove-padding ">
					 <a class="close-btn" href="http://tattoo.globe.com.ph/" ><i class="glyphicon glyphicon-remove-circle"></i></a>
				</div>
				<div class="col-xs-12 text-center sm-header">
					<p>FOLLOW US</p>
					<h3>#iloveBHS</h3>
				</div>

				<!--Social Media links-->
				<div class="col-xs-12 sm-container">
					<ul class="list-unstyled social-media-content text-center">
						<li>
							<a class="fb-bhs" href="https://www.facebook.com/BoniHighStreet">
								<img src="<?php echo Link::image_url("client/{$this->nas->id}/facebook-bhs-logo.png");?>" alt="like us on facebook" class="img-responsive">
							</a>
						</li>
						<li>
							<a class="tw-bhs" href="http://twitter.com/BoniHighStreet">
								<img src="<?php echo Link::image_url("client/{$this->nas->id}/twitter-bhs-logo.png");?>" alt="follow us on twitter" class="img-responsive">
							</a></li>
						<li>
							<a class="ig-bhs" href="https://instagram.com/BonifacioHighStreet/">
								<img src="<?php echo Link::image_url("client/{$this->nas->id}/instagram-bhs-logo.png");?>" alt="follow us on Instagram" class="img-responsive">
							</a>
						</li>
					</ul>
				</div>
			</div>
		</section>