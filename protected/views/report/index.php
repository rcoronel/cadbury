<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>
<div class="">
	<?php $form = $this->beginWidget('CActiveForm', array( 'id'=>'report-form', 'enableClientValidation'=>true, 'clientOptions' => array( 'validateOnSubmit' => true, ), )); ?>	
	<ul>
		<li>
			<label>Select Date</label>
			<div id="datepicker"></div>
			<?php echo $form->hiddenField($model, 'dates', array('id' => 'reportform-dates')); ?>
		</li>
		<li>
			<?php 
				$sites = array(
					"Consolidated Sites"	=> "Consolidated Sites",
					"Alabang Town Center"	=> "Alabang Town Center",
					"Ayala Center Cebu"		=> "Ayala Center Cebu",
					"Bonifacio High Street" => "Bonifacio High Street",
					"Boracay" 				=> "Boracay",
					"Cebu Mactan Airport"	=> "Cebu Mactan Airport",
					"Centrio CDO"			=> "Centrio CDO",
					"Eastwood"				=> "Eastwood",
					"Fairview Terraces" 	=> "Fairview Terraces",
					"Glorietta"				=> "Glorietta",
					"Greenbelt"				=> "Greenbelt",
					"Harbor Point"			=> "Harbor Point",
					"Lucky China Town"		=> "Lucky China Town",
					"Market Market"			=> "Market Market",
					"Marquee Mall"			=> "Marquee Mall",
					"Serendra"				=> "Serendra",
					"SM North Gen 3"		=> "SM North Gen 3",
					"TGT"					=> "TGT",
					"Trinoma"				=> "Trinoma",
					"UP Town Center"		=> "UP Town Center",
					"Valkyrie At The Palace"=> "Valkyrie At The Palace",
					"Wendy's"				=> "Wendy's"
				);
				
				//echo $form->field($model, 'sites')->dropDownList($sites, ['prompt'=>'Select Option']);
				echo $form->dropDownList($model, 'sites', $sites);
			?>
		</li>
		<li>
			<?php 
				$choose_report = array(
					'All Reports'					=> 'All Reports',
					'Total Unique Visits' 						=> 'Total Unique Visits',
					'Total Unique Auth Visitors' 				=> 'Total Unique Auth Visitors',
					'Unique New Auth Visitors'					=> 'Unique New Auth Visitors',
					'Unique Returning Auth Visitors'			=> 'Unique Returning Auth Visitors',
					'Bounce Rate' 					=> 'Bounce Rate',
					'Total Registration'			=> array(
						'All Registered' 			=> 'All Registered',
						'Unique Facebook'				=> 'Unique Facebook',
						'Unique Tattoo'					=> 'Unique Tattoo',
						'Unique Registration'			=> 'Unique Registration',
						'Unique Survey'					=> 'Unique Survey',
						'Unique Easy'					=> 'Unique Easy',
						'Unique Free'					=> 'Unique Free',
						'Unique Via Mobile Number'		=> array(
							'Unique Mobile'					=> 'Unique Mobile',
							'Unique Globe'					=> 'Unique Globe',
							'Unique Smart'					=> 'Unique Smart',
							'Unique Sun'					=> 'Unique Sun',
							'Undefined'						=> 'Undefined',
						),
					),
					'Total Session'							=> array(
						'Total Session Duration'				=> 'Total Session Duration',
						'Average Duration Per Authentication'	=> 'Average Duration Per Authentication',
						'In Hours'								=> 'In Hours',
					),
					'Total Input Volume (In GB)'		=> 'Total Input Volume (In GB)',
					'Total Output Volume (In GB)'		=> 'Total Output Volume (In GB)',
					'Total Volume'						=> 'Total Volume',
					'total_volume_per_unique_auth'		=> 'Total Volume Per Unique Auth',
					'Total Page Views'					=> 'Total Page Views',
					'Total Success Logs'				=> 'Total Success Logs',
					'Total Error Logs'					=> 'Total Error Logs',
				);
				
				echo $form->dropDownList($model, 'choose_report', $choose_report);
			?>
		</li>
		<li>
			<input type="submit" name="submit" value="Submit" />
		</li>
	</ul>
	<?php $this->endWidget()?>
</div>