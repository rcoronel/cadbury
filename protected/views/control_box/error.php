
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 text-center alert-block">
				<h2 class="alert alert-danger">
					<span>
						<i class="fa fa-exclamation-triangle"></i> 
						Something went wrong
					</span>
				</h2>
				<h4><?php echo CHtml::encode($message); ?></h4>
			</div>
		</div>
