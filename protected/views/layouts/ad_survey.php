<?php $this->beginContent('//layouts/main_web'); ?>
        <section class="container" id="main-wrapper">
                <div class="bg-white shadow-effect">
                    <?php echo $content;?>
                </div>
        </section>
<?php $this->endContent(); ?>