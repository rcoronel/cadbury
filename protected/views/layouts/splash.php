<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
	<div class="container globe-container" id="continue-holder">
		
		<!--User Login here add "hide-content" class to hide the content -->
		<?php echo $content;?>
		
	</div>

	<footer class="row powered-container">
		<?php if (NasConfiguration::getvalue('POWERED_WELCOME')):?>
			<?php echo CHtml::image(Link::image_url('powered-by-globe-white.png'), 'Globe');?>
		<?php endif;?>
	</footer>

<?php $this->endContent(); ?>