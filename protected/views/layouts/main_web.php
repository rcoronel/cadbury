<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!--CSS Script-->
		<?php echo CHtml::cssFile(Link::css_url('captiveportal/normalize.css'));?>
		<?php echo CHtml::cssFile(Link::css_url('bootstrap/v3/bootstrap.min.css'));?>
		<?php echo CHtml::cssFile(Link::css_url('font-awesome/font-awesome.css'));?>
		<?php echo CHtml::cssFile(Link::css_url('captiveportal/styles.css'));?>
		<?php echo CHtml::scriptFile(Link::js_url('modernizr.js'));?>
		<?php echo CHtml::scriptFile(Link::js_url('jquery/jquery-1.11.2.min.js'));?>
		<?php echo CHtml::scriptFile(Link::js_url('bootstrap/bootstrap.min.js'));?>

		<script>
			var baseURL = "<?php echo Link::base_url()?>";
			var currentIndex = "<?php echo $this->createUrl($this->id.'/');?>";
			var currentURL = "<?php echo $this->createUrl('/'.$this->route,$_GET);?>";
			var token = "<?php echo Yii::app()->request->csrfToken;?>";
		</script>

	</head>
	
		<body id="globe-bg-body">
			<?php echo $content;?>
		</body>
</html>