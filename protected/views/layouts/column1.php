<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<section class="container" id="main-wrapper">
	<div class="bg-white shadow-effect">
		

		<?php echo $content;?>

	</div>
</section>

<?php $this->endContent(); ?>