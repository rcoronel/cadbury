<?php

class Scene_alphaController extends CaptivePortal
{
	protected function beforeAction($action)
	{
		return TRUE;
	}
	public function actionIndex()
	{
		$token = Yii::app()->request->getQuery('connection_token');
		if ($token) {
			// Connection
			$this->connection = DeviceConnection::model()->findByAttributes(array('connection_token'=>$token));
			DeviceConnection::validateObject($this->connection, 'DeviceConnection');
			
			// Declare NAS
			$this->nas = RadiusNas::model()->findByPk($this->connection->nas_id);
			RadiusNas::validateObject($this->nas, 'RadiusNas');
			
			// Declare Site
			$this->site = Site::model()->findByPk($this->connection->site_id);
			Site::validateObject($this->site, 'Site');
			
			// Declare Portal
			$this->portal = Portal::model()->findByPk($this->connection->portal_id);
			Portal::validateObject($this->portal, 'Portal');
			
			// Declare AP Group
			$this->ap_group = AccessPointGroup::model()->findByPk($this->connection->access_point_group_id);
			AccessPointGroup::validateObject($this->ap_group, 'AccessPointGroup');
			
			// Declare AP
			$this->ap = AccessPoint::model()->findByPk($this->connection->access_point_id);
			AccessPoint::validateObject($this->ap, 'AccessPoint');

			// Declare device
			$this->device = Device::model()->findByPk($this->connection->device_id);
			Device::validateObject($this->device, 'Device');
			
			$this->changeRoleTest('authenticated');	
			echo "SUCCESS";
			die();
			
		}
		
	}
}