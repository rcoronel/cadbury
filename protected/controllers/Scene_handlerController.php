<?php

class Scene_handlerController extends CaptivePortal
{
	/**
	 * Always check if device, connection and NAS is declared
	 * 
	 * @access protected
	 * @param object $action CAction instance
	 * @return bool
	 */
	protected function beforeAction($action)
	{
		$token = Yii::app()->request->getQuery('connection_token');
		if ($token) {
			// Connection
			$this->connection = DeviceConnection::model()->findByAttributes(array('Connection_token'=>$token));
			DeviceConnection::validateObject($this->connection, 'DeviceConnection');
			
			// NAS
			$this->nas = RadiusNas::model()->findByPk($this->connection->Nas_ID);
			RadiusNas::validateObject($this->nas, 'RadiusNas');
			
			// Device
			$this->device = Device::model()->findByPk($this->connection->Device_ID);
			Device::validateObject($this->device, 'Device');
			
			// Scenarios
			//$this->scenarios = DeviceScenario::model()->findByAttributes(array('Device_ID'=>$this->device->Device_ID, 'Nas_ID'=>$this->nas->id));
			
			// COOKIES FOR AD SURVEY
			Yii::app()->request->cookies['connection_token'] = new CHttpCookie('connection_token', $token);
			Yii::app()->request->cookies['nas_id'] = new CHttpCookie('nas_id', $this->nas->id);
			Yii::app()->request->cookies['device_id'] = new CHttpCookie('device_id', $this->device->Device_ID);
			
			parent::beforeAction($action);
		}
		
		return TRUE;
	}
	
	/**
	 * Display splash page
	 * 
	 * @access public
	 * @return void
	 */
	public function actionLanding()
	{
		$this->layout = '//layouts/splash';
		$terms = $this->createAbsoluteUrl('scene_handler/terms', array('connection_token'=>$this->connection->Connection_token));
		$policy = $this->createAbsoluteUrl('scene_handler/policy', array('connection_token'=>$this->connection->Connection_token));
		$url = $this->createAbsoluteUrl('scene_handler/', array('connection_token'=>$this->connection->Connection_token));
		$this->render('landing_page', array('url'=>$url, 'terms'=>$terms, 'policy'=>$policy));
	}
	
	/**
	 * Default action
	 * 
	 * @access public
	 * @return void
	 */
	public function actionIndex()
	{
		header("Cache-Control: private, must-revalidate, max-age=0");
		header("Pragma: no-cache");
		header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
		
		self::_checkScene();
		
		// Check if form is submitted
		if (Yii::app()->request->isPostRequest OR Yii::app()->request->getParam('availment_id')) {
			self::_signIn(Yii::app()->request->getParam('availment_id'));
		}
		else {
			// Check if there is existing duration
			$session_duration = Device::getSessionTime($this->connection);
			$session_max = RadiusReply::getSessionMax($this->connection->Username);
			if ($session_duration['current_session_time'] AND ($session_duration['current_session_time'] < $session_max->value)) {
				self::_showRemaining($session_duration['current_session_time'], $session_max->value);
			}
			else {
				self::_displayAvailments();
			}
		}
	}
	
	
	
	/**
	 * Display availments
	 * 
	 * @access private
	 * @return void
	 */
	private function _displayAvailments()
	{
		$model = $this->scene_model;
		
		// Get scenarios
		$device_avails = DeviceAvail::model()->findAllByAttributes(array('Device_ID'=>$this->device->Device_ID, 'Nas_ID'=>$this->nas->id, 'Scene'=>$this->scene));
		if ( ! empty($device_avails)) {
			$this->scenarios = $model::model()->findScenario($device_avails, $this->nas->id);
		}
		
		// Check if current scene is returning and empty availments
		// Check previous availments to get scenario
		if ($this->scene == 'returning' AND empty($device_avails)) {
			$device_avails = DeviceAvail::model()->findAllByAttributes(array('Device_ID'=>$this->device->Device_ID, 'Nas_ID'=>$this->nas->id, 'Scene'=>'new'));
			if ( ! empty($device_avails)) {
				$this->scenarios = ScenarioAvailment::model()->findScenario($device_avails, $this->nas->id);
			}
		}
		
		// Availment container
		$availment = array();

		// Get acquired availment methods
		$latest_availment = ConnectionAvail::model()->findByAttributes(array('Device_connection_ID'=>$this->connection->Device_connection_ID), array('order'=>'Created_at DESC'));
		$latest_availments = ConnectionAvail::model()->findAllByAttributes(array('Device_connection_ID'=>$this->connection->Device_connection_ID), array('order'=>'Created_at DESC'));

		if ( ! empty($latest_availment) &&  ! empty($latest_availments)) {
			if ($latest_availment->Availment_ID == 4) {
				$url = $this->createAbsoluteUrl('scene_handler/landing', array('connection_token'=>$this->connection->Connection_token));
				$availment = NasAvailment::model()->find("Nas_ID = {$this->nas->id} AND Availment_ID = {$latest_availment->Availment_ID}");
				$this->render('upsell', array('availment'=>$availment, 'url'=>$url));
				die();
			}
			
			if (empty($this->scenarios)) {
				// Set where string
				$where_string = "({$this->scene}.Availment_ID = {$latest_availment->Availment_ID} AND {$this->scene}.Level = {$latest_availment->Level})";

				// Append to where string
				foreach ($latest_availments as $la) {
					if ($la->Availment_ID != $latest_availment->Availment_ID && $la->Level != $latest_availment->Level) {
						$where_string .= " OR ({$this->scene}.Availment_ID = {$la->Availment_ID} AND {$this->scene}.Level = {$la->Level})";
					}
				}
				
				// Get scenarios based on where string
				$alt_scenarios = Scenario::model()->with($this->scene)->findAll($where_string);
				$count = count($latest_availments);

				// Remove scenarios incomplete scenarios
				foreach ($alt_scenarios as $index=>$as) {
					if ($count != count($as->{$this->scene})) {
						unset($alt_scenarios[$index]);
					}
				}

				// Get scenarios
				if ( ! empty($alt_scenarios)) {
					foreach ($alt_scenarios as $as) {
						$scenarios[] = Scenario::model()->with($this->scene)->findByAttributes(array('Nas_ID'=>$this->nas->id, 'Scenario_ID'=>$as->Scenario_ID), array('order'=>'Level ASC'));
					}
				}
				else {
					$scenarios = Scenario::model()->with($this->scene)->findAllByAttributes(array('Nas_ID'=>$this->nas->id), array('order'=>'Level ASC'));
				}
			}
			else {
				$criteria = new CDbCriteria;
				$criteria->addCondition("Level >= {$this->level}");
				$criteria->addInCondition('t.Scenario_ID', $this->scenarios);
				$criteria->order = 'Level ASC';
				$scenarios = Scenario::model()->with($this->scene)->findAll($criteria);
			}
			
			// latest availment
			$availment = NasAvailment::model()->find("Nas_ID = {$this->nas->id} AND Availment_ID = {$latest_availment->Availment_ID}");
		}
		else {
			if (empty($this->scenarios)) {
				$scenarios = Scenario::model()->with($this->scene)->findAllByAttributes(array('Nas_ID'=>$this->nas->id), array('order'=>'Level ASC'));
			}
			else {
				$criteria = new CDbCriteria;
				$criteria->addCondition("Level >= {$this->level}");
				$criteria->addInCondition('t.Scenario_ID', $this->scenarios);
				$criteria->order = 'Level ASC';
				$scenarios = Scenario::model()->with($this->scene)->findAll($criteria);
			}
		}
		
		// Get all availments from all scenarios
		$availments = array();
		foreach ($scenarios as $s) {
			foreach ($s->{$this->scene} as $index => $a) {
				$availments[] = $a;
			}
		}
		
		// Remove non-recurring
		if ( ! empty($availments)) {
			foreach ($availments as $index => $a) {
				// Get availment model
				$model = $a->availment->Model;

				// Check if existing
				$is_exists = $model::model()->exists("Device_ID = {$this->device->Device_ID} AND Nas_ID = {$this->nas->id} AND DATE(Created_at) != DATE(NOW())");

				// Check configuration on NAS
				$nas_availment = NasAvailment::model()->findByAttributes(array('Nas_ID'=>$a->scenario->Nas_ID, 'Availment_ID'=>$a->Availment_ID));
				$a->availment->duration = $nas_availment->Duration / 60;
				$a->availment->Description = $nas_availment->Description;
				
				// Check if already existing and non-recurring
				if ($is_exists AND ! $nas_availment->Is_recurring) {
					unset($availments[$index]);
				}
			}
		}
		
		// Remove duplicates
		if ( ! empty($availments)) {
			foreach ($availments as $index_a => $a) {
				foreach ($availments as $index_b => $b) {
					if ($index_a != $index_b && $a->Level == $b->Level && $a->Availment_ID == $b->Availment_ID) {
						unset($availments[$index_a]);
					}
				}
			}
		}
		
		// Remove duplicates
		if ( ! empty($availments)) {
			// Sort by level in ascending order
			usort($availments, function($a, $b) {  
				if( $a->Level == $b->Level) {
					return 0; 
				}
				return $a->Level > $b->Level ? 1 : -1;
			});
			
			$this->level = $availments[0]->Level;
			// Update connection level
			$this->connection->Level = $this->level;
			if ( ! $this->connection->validate() OR ! $this->connection->save()) {
				$this->redirect(array('scene_handler/', 'connection_token'=>$this->connection->Connection_token));
			}
			
			// Remove availment which is not equal to current level
			foreach ($availments as $index=>$a) {
				if ($a->Level != $this->connection->Level) {
					unset($availments[$index]);
				}
			}
			
			
		}
		
		// Sort by name
		if ( ! empty($availments)) {
			usort($availments, function($a, $b) {
				return strcmp($a->availment->Name, $b->availment->Name);
			});
		}
		
		$content = Cms::model()->findByAttributes(array('Nas_ID'=>$this->nas->id, 'Friendly_URL'=>'service-agreement'));
		$url = $this->createAbsoluteUrl('scene_handler/terms', array('connection_token'=>$this->connection->Connection_token));
		$a_url = $this->createAbsoluteUrl('scene_handler/attempt', array('connection_token'=>$this->connection->Connection_token));
		$this->render('index', array('availments'=>$availments, 'url'=>$url, 'content'=>$content, 'availment'=>$availment, 'connection_token'=>$this->connection->Connection_token, 'a_url'=>$a_url));
	}

	/**
	 * Show remaining duration of the current availment method chosen
	 * 
	 * @access private
	 * @return void
	 */
	private function _showRemaining($current_session_time, $max_session_time)
	{
		$remaining_session = (int)$max_session_time - (int)$current_session_time;
		$minutes = 0;
		// If duration is less than 90 seconds
		if ($remaining_session < 90 ) {
			$minutes = 1;
		}
		else {
			$minutes = $remaining_session / 60;
		}
		
		// Get latest availed method
		$latest_avail = ConnectionAvail::model()->findByAttributes(array('Device_connection_ID'=>$this->connection->Device_connection_ID), array('order'=>'Created_at DESC'));
		
		if (Yii::app()->request->isPostRequest) {
			$this->verifyConnection($latest_avail->Availment_ID);
		}
		
		$name = 'user';
		$fb = AvailFacebook::model()->findByAttributes(array('Device_ID'=>$this->device->Device_ID, 'Nas_ID'=>$this->nas->id));
		if ($fb) {
			$name = $fb->Name;
		}
		else {
			$reg = AvailRegistration::model()->findByAttributes(array('Device_ID'=>$this->device->Device_ID, 'Nas_ID'=>$this->nas->id));
			if ($reg) {
				$name = $reg->Name;
			}
		}
		
		$content = Cms::model()->findByAttributes(array('Nas_ID'=>$this->nas->id, 'Friendly_URL'=>'service-agreement'));
		$this->render('remaining_session', array('availment'=>$latest_avail, 'name'=>$name, 'content'=>$content, 'minutes'=>$minutes));
	}
	
	/**
	 * Show Terms and Conditions page
	 * 
	 * @access public
	 * @return void
	 */
	public function actionTerms()
	{
		$terms = Cms::model()->findByAttributes(array('Nas_ID'=>$this->nas->id, 'Friendly_URL'=>'terms-and-conditions'));
		$url = $this->createAbsoluteUrl('scene_handler/landing', array('connection_token'=>$this->connection->Connection_token));
		$this->render('terms', array('url'=>$url, 'terms'=>$terms));
	}
	/**
	 * Show Service Agreement page
	 * 
	 * @access public
	 * @return void
	 */
	public function actionPolicy()
	{
		$policy = Cms::model()->findByAttributes(array('Nas_ID'=>$this->nas->id, 'Friendly_URL'=>'service-agreement'));
		$url = $this->createAbsoluteUrl('scene_handler/landing', array('connection_token'=>$this->connection->Connection_token));
		$this->render('policy', array('url'=>$url, 'policy'=>$policy));
	}
	
	
	/**
	 * Give the user internet access
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAccess()
	{
		$availment_id = Yii::app()->request->getQuery('availment_id');
		
		// Check if there is a connection made
		$connect = ConnectionAvail::model()->findByAttributes(array('Device_connection_ID'=>$this->connection->Device_connection_ID, 'Availment_ID'=>$availment_id));
		if (empty($connect)) {
			throw new CHttpException(400,Yii::t('yii','No connection made. Please try again.'));
		}
		
		// If redirect URL is given, redirect after authentication
		if (NasConfiguration::getValue('REDIRECT_URL')) {
			$this->redirect(NasConfiguration::getValue('REDIRECT_URL'));
		}
		
		$this->layout = '//layouts/ads';
		
		$criteria = new CDbCriteria();
		$criteria->compare('Nas_ID',$this->nas->id);
		$criteria->order="RAND()";
		$criteria->limit = 1;
		$ads = Ads::model()->find($criteria);
		
		if ($this->nas->id == 6) {
			$this->render('access_bhs', array('ads'=>$ads));
		}
		else {
			$this->render('access', array('ads'=>$ads));
		}
	}
	
	/**
	 * Sign-in based on POST protocol
	 * 
	 * @access private
	 * @return void
	 */
	private function _signIn($id)
	{
		if (empty($id)) {
			$id = Yii::app()->request->getPost('availment_id');
		}
		
		$availment = NasAvailment::model()->findByAttributes(array('Nas_ID'=>$this->nas->id, 'Availment_ID'=>$id));
		
		// Check if there is existing duration
		$session_duration = Device::getSessionTime($this->connection);
		$session_max = RadiusReply::getSessionMax($this->connection->Username);
		if ($session_duration['current_session_time'] AND ($session_duration['current_session_time'] < $session_max->value)) {
			$this->verifyConnection($id);
		}
		
		$this->render('login', array('availment'=>$availment));
	}


	public function actionAttempt(){

		$availment_id = Yii::app()->request->getParam('availment_id');

		$ca = new ConnectionAttempt;
		$ca->Nas_ID = $this->nas->id;
		$ca->Device_ID = $this->device->Device_ID;
		$ca->Availment_ID = $availment_id;
		$ca->Status = 0;

		if (!($ca->validate() && $ca->save())) {
			throw new CHttpException(400,Yii::t('yii','No attempt made. Please try again.'));
		}else{
			die(CJSON::encode(array('status'=>1, 'message'=>'success')));
		}
			
	}

}