<?php

class Control_boxController extends CaptivePortal
{
	/**
	 * Always check if device, connection and access controller is declared
	 *
	 * @param object $action CAction instance
	 * @return boolean
	 * @throws CHttpException
	 * @return bool
	 */
	protected function beforeAction($action)
	{
		$exempt = array('error', 'terminate');
		if ( ! in_array($action->id, $exempt)) {
			// Get query variables
			$cmd = Yii::app()->request->getQuery('cmd');
			$switch_ip = Yii::app()->request->getQuery('switchip');
			$mac = Yii::app()->request->getQuery('mac');
			$ip = Yii::app()->request->getQuery('ip');
			$ssid = Yii::app()->request->getQuery('essid');
			$ap_name = Yii::app()->request->getQuery('apname');
			$ap_group = Yii::app()->request->getQuery('apgroup');
			$url = Yii::app()->request->getQuery('url');

			$uri = $this->createAbsoluteUrl('/'.$this->route,$_GET);

			// Get NAS
			$this->_getNas($switch_ip, $uri);
			
			// Get SSID
			$this->_getSsid($ssid, $uri);
			
			// Get AP Group
			$this->_getApg($ap_group, $uri);
			
			// Get AP
			$this->_getAp($ap_name, $uri);
			
			// Get portal
			$this->_getPortal($uri);
			
			// Get site
			$this->_getSite($uri);
			
			// Register device and connection
			// If no error, automatically calls index function
			if (self::_registerDevice($mac) && self::_registerConnection($ip, $ssid)) {
				return TRUE;
			}

			throw new CHttpException(400,Yii::t('yii','Critical exception found'));
		}
		return TRUE;
	}
	
	/**
	 * Get access controller details
	 * 
	 * @access private
	 * @param string $switch_ip
	 * @param string $uri Request URL
	 * @throws CHttpException
	 */
	private function _getNas($switch_ip, $uri)
	{
		$this->nas = RadiusNas::model()->findByAttributes(array('switchip'=>$switch_ip));
		if (empty($this->nas)) {
			$log = new ErrorLog;
			$log->error_code = 'SITE-001';
			$log->url = $uri;
			$log->reference = $switch_ip;
			$log->validate() && $log->save();
			throw new CHttpException(400, Yii::t('yii',"We've encountered an issue. Please try again later. ({$log->error_code})"));
		}
	}
	
	/**
	 * Get SSID details
	 * 
	 * @access private
	 * @param string $ssid
	 * @param string $uri Request URL
	 * @throws CHttpException
	 */
	private function _getSsid($ssid, $uri)
	{
		$this->ssid = ServiceSet::model()->findByAttributes(array('ssid'=>$ssid, 'is_active'=>1));
		if (empty($this->ssid)) {
			$log = new ErrorLog;
			$log->error_code = 'SITE-006';
			$log->url = $uri;
			$log->reference = $ssid;
			$log->validate() && $log->save();

			throw new CHttpException(400, Yii::t('yii',"We've encountered an issue. Please try again later. ({$log->error_code})"));
		}
	}
	
	/**
	 * Get Access Point Group details
	 * If not existing, save new record
	 * 
	 * @access private
	 * @param string $ap_group AP Group name
	 * @param string $uri Request URL
	 * @throws CHttpException
	 */
	private function _getApg($ap_group, $uri)
	{
		$this->ap_group = AccessPointGroup::model()->findByAttributes(array('name'=>$ap_group, 'is_active'=>1));
		if (empty($this->ap_group)) {
			$log = new ErrorLog;
			$log->error_code = 'SITE-002';
			$log->url = $uri;
			$log->reference = $ap_group;
			$log->validate() && $log->save();

			throw new CHttpException(400, Yii::t('yii',"We've encountered an issue. Please try again later. ({$log->error_code})"));
		}
	}
	
	/**
	 * Get Access Point details
	 * If not existing, save new record
	 * 
	 * @access private
	 * @param string $ap_name AP name
	 * @param string $uri Request URL
	 * @throws CHttpException
	 */
	private function _getAp($ap_name, $uri)
	{
		if ( ! empty($ap_name)) {
			$ap_exists = AccessPoint::model()->exists('name = "'.$ap_name.'"');
			if ( ! $ap_exists) {
				$ap = new AccessPoint();
				$ap->access_point_group_id = $this->ap_group->access_point_group_id;
				$ap->name = $ap_name;
				$ap->is_active = 1;
				if ( ! $ap->validate() OR  ! $ap->save()) {
					throw new CHttpException(400,Yii::t('yii','Cannot save Access Point'));
				}
			}
			$this->ap = AccessPoint::model()->findByAttributes(array('name'=>$ap_name, 'is_active'=>1));
		}
	}
	
	/**
	 * Get portal details
	 * 
	 * @access private
	 * @param string $uri Request URL
	 * @throws CHttpException
	 */
	private function _getPortal($uri)
	{
		$group = PortalSsidGroup::model()->findByAttributes(array('service_set_id'=>$this->ssid->service_set_id, 'access_point_group_id'=>$this->ap_group->access_point_group_id));
		if ( ! empty($group)) {
			$this->portal = Portal::model()->findByAttributes(array('portal_id'=>$group->portal_id, 'is_active'=>1));
		}
		
		if (empty($group) OR empty($this->portal)) {
			$log = new ErrorLog;
			$log->error_code = 'SITE-004';
			$log->url = $uri;
			$log->reference = "{$this->ssid->ssid} | {$this->ap_group->access_point_group_id}";
			$log->validate() && $log->save();
			throw new CHttpException(400, Yii::t('yii',"We've encountered an issue. Please try again later. ({$log->error_code})"));
		}
	}
	
	/**
	 * Get site details
	 * 
	 * @access private
	 * @param string $uri Request URL
	 * @throws CHttpException
	 */
	private function _getSite($uri)
	{
		$this->site = Portal::model()->findByAttributes(array('site_id'=>$this->portal->site_id));
		if (empty($this->site)) {
			$log = new ErrorLog;
			$log->error_code = 'SITE-005';
			$log->url = $uri;
			$log->reference = $this->portal->code;
			$log->validate() && $log->save();
			throw new CHttpException(400, Yii::t('yii',"We've encountered an issue. Please try again later. ({$log->error_code})"));
		}
	}

	/**
	 * Register device
	 *
	 * @access private
	 * @param string $mac MAC address
	 * @throws CHttpException
	 * return boolean
	 */
	private function _registerDevice($mac)
	{
		sleep(1);

		// Check if device is already existing
		$count = Device::model()->exists("mac_address = '{$mac}'");

		// Register device if non-existing
		if ( ! $count) {
			$device = new Device;
			$device->mac_address = $mac;
			if ( ! $device->validate() OR  ! $device->save()) {
				throw new CHttpException(400,Yii::t('yii','Cannot register device'));
			}
		}
		else {
			$device = Device::model()->findByAttributes(array('mac_address'=>$mac));
		}

		$this->device = $device;

		return TRUE;
	}

	/**
	 * Register connection details
	 *
	 * @access private
	 * @param string $ip
	 * @param string $ssid
	 * @return boolean
	 */
	private function _registerConnection($ip, $ssid)
	{
		// Check if connection exists
		// Based on device_id and portal_id
		sleep(1);

		$count = DeviceConnection::model()->exists("device_id = {$this->device->device_id}"
		. " AND portal_id = {$this->portal->portal_id}");

		if ( ! $count) {
			// If connection does not exists, create new instance
			$connection = new DeviceConnection;
			$connection->connection_token = Yii::app()->getSecurityManager()->generateRandomString(128);
			$connection->portal_id = $this->portal->portal_id;
			$connection->access_point_group_id = ( ! empty($this->ap_group)) ? $this->ap_group->access_point_group_id : 0;
			$connection->access_point_id = ( ! empty($this->ap)) ? $this->ap->access_point_id : 0;
			$connection->device_id = $this->device->device_id;
		}
		else {
			// If connection already exists, get instance
			$connection = DeviceConnection::model()->findByAttributes(array('device_id'=>$this->device->device_id,
				'portal_id'=>$this->portal->portal_id));
		}

		// Set NAS, Site, IP Address, and SSID
		$connection->nas_id = $this->nas->id;
		$connection->site_id = $this->site->site_id;
		$connection->ip_address = $ip;
		$connection->ssid = $ssid;

		// Set location, if declared
		$location = Location::model()->findByAttributes(array('portal_id'=>$this->portal->portal_id));
		if ($location) {
			$connection->location_id = $location->location_id;
		}

		if ($connection->validate() && $connection->save()) {
			$this->connection = $connection;
			return TRUE;
		}

		throw new CHttpException(400,Yii::t('yii','Cannot establish connection'));
	}

	/**
	 * Default action
	 * Redirect to landing page
	 *
	 * @access public
	 * @return void
	 */
	public function actionIndex()
	{
		//$url = 'http://unifiapps.yondu.com/index.php?'.http_build_query(array("r"=>"{$this->portal->code}/landing", 'connection_token'=>$this->connection->connection_token));
		//$url=Yii::$app->urlManager->createUrl(array('landing', 'connection_token' => $this->connection->connection_token));
		//$this->redirect($url);
		$this->redirect(array("{$this->portal->code}/landing", 'connection_token'=>$this->connection->connection_token));
	}

	public function actionTerminate($username, $cause)
	{
		if (substr($username, 0, 8) != 'fb_guest') {
			$connection = DeviceConnection::model()->findByAttributes(array('username'=>$username));
		}
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error) {
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
}
