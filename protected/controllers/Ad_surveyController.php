<?php

class Ad_surveyController extends Controller
{
        public $layout = '//layouts/ad_survey';
        public function actionIndex()
	{   
                $connection = DeviceConnection::model()->findByAttributes(array('Connection_token'=>Yii::app()->request->cookies['connection_token']));
                DeviceConnection::validateObject($connection, 'DeviceConnection');
                $availment = NasAvailment::model()->findByAttributes(array('Nas_ID'=>Yii::app()->request->cookies['nas_id']->value, 'Availment_ID' => 5));
		$this->render('index', 
                        array(
                            'availment'=>$availment, 
                            'connection'=>$connection,
                        )
                );
	}
}