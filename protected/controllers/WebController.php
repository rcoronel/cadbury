<?php

class WebController extends Controller
{
	public $layout = '//layouts/web';
	
	public function actionAds()
	{
		$criteria = new CDbCriteria();
		$criteria->order="RAND()";
		$criteria->limit = 1;
		$ads = Ads::model()->find($criteria);
		$this->render('ads', array('ads'=>$ads));
	}
	
	public function actionIndex()
	{
		$ca = ConnectionAvail::model()->with('connection', 'availment')->findByPk(2);
		$device = Device::model()->findByPk($ca->connection->Device_ID);
		$availment = Availment::model()->findByPk($ca->Availment_ID);
		
		$arr = array('device'=>array('id'=>$device->Device_ID, 'mac_address'=>$device->Mac_address),
					'availment'=>array('id'=>$availment->Availment_ID, 'availment'=>$availment->Name),
					'location'=>array());
		echo "<pre>";
		print_r(CJSON::encode($arr));
//		print_r($device);
//		print_r($availment);
		die();
		die('ASD');
		$url = Yii::app()->request->getQuery('url');
		$web = self::_getWebContent($url);
		$this->render('index', array('web'=>$web));
	}
	
	public function actionFacebook()
	{
		$from = '2015-06-03';
		$to = '2015-07-06';
		
		while (strtotime($from) <= strtotime($to)) {
			$table = "correlor_log_$from";
			
			$tokens = CorrelorLog::getToken($table);
			foreach ($tokens as $token) {
				$link = curl_init("https://graph.facebook.com/me?access_token={$token['accessToken']}");
				curl_setopt($link, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($link, CURLOPT_RETURNTRANSFER, true);
				$data = curl_exec($link);
				curl_close($link);
				
				$decode_data = CJSON::decode($data);
				$fb_user = AvailFacebook::model()->findByAttributes(array('Fb_ID'=>$decode_data['id'], 'Device_ID'=>$token['Device_ID'], 'Nas_ID'=>$token['Nas_ID']));
				if (empty($fb_user)) {
					$fb_user = new AvailFacebook();
					$fb_user->Nas_ID = $token['Nas_ID'];
					$fb_user->Fb_ID = $decode_data['id'];
					$fb_user->Device_ID = $token['Device_ID'];
					$fb_user->Email = $decode_data['email'];
					$fb_user->Firstname = $decode_data['first_name'];
					$fb_user->Lastname = $decode_data['last_name'];
					$fb_user->Gender = $decode_data['gender'];
					$fb_user->Access_token = $token['accessToken'];
					$fb_user->Access_expiry = $token['accessTokenExpires'];
					$fb_user->Created_at = $token['Created_at'];
					$fb_user->Correlor_ID = $decode_data['id'].'@fb';
					$fb_user->validate() && $fb_user->save();
					if ( ! empty($fb_user->errors)) {
						echo "<pre>";
						print_r($fb_user);
						print_r($fb_user->errors);
						die();
					}
				}
			
			}

			// increment $from
			$from = date('Y-m-d', strtotime($from . "+1 days"));
		}
	}
	
	public function actionFacebook_Pic()
	{
		$users = AvailFacebook::model()->findAll();
		foreach ($users as $u) {
			$link = curl_init("http://graph.facebook.com/{$u->Fb_ID}/picture?type=large&redirect=false");
			curl_setopt($link, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($link, CURLOPT_RETURNTRANSFER, true);
			$data = curl_exec($link);
			curl_close($link);

			$decode_data = CJSON::decode($data);
			
			$u->Picture = $decode_data['data']['url'];
			$u->validate() && $u->save();
			echo "<pre>";
			print_r($u->Fb_ID);
		}
		die();
	}
	
	
	private function _getWebContent($url)
	{
		$link = curl_init($url);
		curl_setopt($link, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($link, CURLOPT_RETURNTRANSFER, true);
		$data = curl_exec($link);
		curl_close($link);
		
		echo "<pre>";
		print_r($data);
		die();
		return $output;
	}
}