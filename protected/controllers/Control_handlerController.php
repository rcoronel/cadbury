<?php

class Control_handlerController extends CaptivePortal
{
	/**
	 * Always check if device, connection and access controller is declared
	 * 
	 * @param object $action CAction instance
	 * @return boolean
	 * @throws CHttpException
	 * @return bool
	 */
	protected function beforeAction($action)
	{
		echo $this->createUrl('/'.$this->route,$_GET);
		die();
		if ($action->id != 'error' AND $action->id != 'terminate') {
			// Get NAS
			$switch_ip = Yii::app()->request->getQuery('switchip');
			$this->nas = RadiusNas::model()->findByAttributes(array('switchip'=>$switch_ip));
			if (empty($this->nas)) {
				throw new CHttpException(400,Yii::t('yii','NAS not defined'));
			}

			$mac = Yii::app()->request->getQuery('mac');
			$ip = Yii::app()->request->getQuery('ip');

			$device = new Device;
			$device->Mac_address = $mac;

			if (self::_registerDevice($device) && self::_registerConnection($ip)) {
				self::_detectDevice();
				setcookie("MacAddress", "{$mac}@{$switch_ip}", time() + (86400 * 30), "/");
				parent::beforeAction($action);
				return TRUE;
			}
			throw new CHttpException(400,Yii::t('yii','Critical exception found'));
		}
		return TRUE;
	}
	
	/**
	 * Register device
	 * 
	 * @access private
	 * @param object $device Device instance
	 * @throws CHttpException
	 * return boolean
	 */
	private function _registerDevice(Device $device)
	{
		sleep(1);
		$count = Device::model()->exists("Mac_address = '{$device->Mac_address}'");
		
		// Register device if non-existing
		if ( ! $count) {
			if ( ! $device->validate() OR  ! $device->save()) {
				throw new CHttpException(400,Yii::t('yii','Cannot register device'));
			}
		}
		else {
			$device = Device::model()->findByAttributes(array('Mac_address'=>$device->Mac_address));
		}
		
		if ( ! $device->Correlor_ID) {
			Correlor::userAnonymous($device->Device_ID, 'Device');
			//Correlor::updateDevice($device->Device_ID);
		}
		$this->device = $device;
		
		return TRUE;
	}
	
	/**
	 * Register connection
	 * 
	 * @access private
	 * @return boolean
	 */
	private function _registerConnection($ip)
	{
		$log = new ConnectionLog();
		$log->Nas_ID = $this->nas->id;
		$log->Device_ID = $this->device->Device_ID;
		$log->validate() && $log->save();
		
		// Check if connection exists
		// Compare Device_ID and Nas_ID
		sleep(1);
		$count = DeviceConnection::model()->exists("Device_ID = {$this->device->Device_ID} AND Nas_ID = '{$this->nas->id}'");
		
		if ( ! $count) {
			$connection = new DeviceConnection;
			$connection->Connection_token = Yii::app()->getSecurityManager()->generateRandomString(128);
			$connection->Nas_ID = $this->nas->id;
			$connection->Device_ID = $this->device->Device_ID;
		}
		else {
			$connection = DeviceConnection::model()->findByAttributes(array('Device_ID'=>$this->device->Device_ID, 'Nas_ID'=>$this->nas->id));
		}
		
		$connection->Ip_address = $ip;
		
		$location = Location::model()->findByAttributes(array('Nas_ID'=>$this->nas->id));
		if ($location) {
			$connection->Location_ID = $location->Location_ID;
		}
		
		if ($connection->validate() && $connection->save()) {
			$this->connection = $connection;
			return TRUE;
		}
		
		throw new CHttpException(400,Yii::t('yii','Cannot establish connection'));
	}
	
	/**
	 * Detect device using user agent string
	 * 
	 * @access private
	 * @return void
	 */
	private function _detectDevice()
	{
		$device_info = DeviceInfo::model()->exists("Device_ID = {$this->device->Device_ID}");
		if (empty($device_info)) {
			$detect = new Mobile_Detect;
			$brand = '';
			$detect_os = '';
			$detect_os_version = '';
			
			// is mobile
			if ($detect->isMobile()) {
				$type = 'mobile';
			}
			
			// is tablet
			if ($detect->isTablet()) {
				$type = 'tablet';
			}
			
			// is computer
			if ( ! $detect->isMobile() && ! $detect->isTablet()) {
				$type = 'computer';
			}
			
			$phone_rules = $detect->getPhoneDevices();
			foreach ($phone_rules as $phone=>$phone_regex) {
				$check = $detect->{'is'.$phone}();
				if ($check) {
					$brand = $phone;
					break;
				}
			}
			
			if (empty($brand)) {
				$tablet_rules = $detect->getTabletDevices();
				foreach ($tablet_rules as $tablet =>$tablet_regex) {
					$check = $detect->{'is'.$tablet}();
					if ($check) {
						$brand = $tablet;
						break;
					}
				}
			}

			$os_rules = $detect->getOperatingSystems();
			foreach ($os_rules as $os => $os_regex) {
				$check = $detect->{'is'.$os}();
				if ($check) {
					$detect_os = $os;
					break;
				}
			}
			$detect_os_version = $detect->version($detect_os);

			$info = new DeviceInfo;
			$info->Device_ID = $this->device->Device_ID;
			$info->Brand = $brand;
			$info->Model = '';
			$info->OS = $detect_os;
			$info->OS_ver = $detect_os_version;
			$info->Device_type = $type;
			$info->validate() && $info->save();
		}
	}
	
	/**
	 * Default action
	 * Redirect to landing page
	 * 
	 * @access public
	 * @return void
	 */
	public function actionIndex()
	{
		$this->redirect(array('scene_handler/landing', 'connection_token'=>$this->connection->Connection_token));
	}
	
	public function actionTerminate($username, $cause)
	{
		if (substr($username, 0, 8) != 'fb_guest') {
			$connection = DeviceConnection::model()->findByAttributes(array('Username'=>$username));
			
			$data = array('actionType'=>'access_point_logout',
				'itemId'=>$connection->Location_ID,
				'actiontime'=>date('YmdHis+0800'),
				'details'=>array(array('name'=>'terminate_cause', 'value'=>$cause)));

			Correlor::reportAction($connection->Device_ID, 'access_point_logout', $data);
		}
	}
	
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
}