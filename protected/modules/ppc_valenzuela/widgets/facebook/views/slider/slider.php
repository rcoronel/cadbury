<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('.db-carousel').slick({
				dots: false,
			  infinite: false,
			  speed: 300,
			  slidesToShow: 3,
			  touchMove: true,
			  slidesToScroll: 1,
			  responsive: [
			    {
			      breakpoint: 600,
			      settings: {
			        slidesToShow: 2,
			        slidesToScroll: 2
			      }
			    },
			    {
			      breakpoint: 480,
			      settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1
			      }
			    }
			  ]
		});
	});
</script>

<div class="dashboard-carousel">
	<div class="db-carousel">
		<?php foreach ($sliders as $s):?>
			<div class="db-carousel-item">
				<div class="db-carousel-wrapper">
					<div class="db-carousel-title">
						<?php echo $s->title;?>
					</div>
					<div class="db-carousel-image">						
						<a href="<?php echo $s->url;?>" style="background-image:url('<?php echo Link::base_url('uploads/dashboard_slider/'.$s->image);?>')">
							<?php echo CHtml::image(Link::base_url('uploads/dashboard_slider/'.$s->image));?>
						</a>
					</div>
				  </div>
			  </div>
		<?php endforeach;?>
	</div>
</div>