<?php

class Tattoo_home extends CWidget
{
    public $self;
	public $self_portal;
	public $action;
	public $reference;
    
    public $status = 0;
	
	public $home = 4;
	public $platinum = 7;
    
    public function init()
    {
		// Set PortalAvailment instance
		$this->self_portal = PortalAvailment::model()->findByAttributes(array('portal_id'=>$this->owner->portal->portal_id, 'availment_id'=>$this->self->availment_id));
	}
    
    public function run()
    {        
        switch ($this->action)
        {
            // Login or Register function
            case 'login':
                self::_login();
            break;

            // Display the buttons
            default:
                self::_display();  
            break;
        }
    }
    
    private function _display()
    {
        $this->render('login', array('availment_id'=>$this->self->availment_id));
    }
    
    private function _login()
    {
		// Check if already availed for today
		$is_availed = DeviceAvailment::model()->exists("device_id = {$this->owner->device->device_id} AND portal_id = {$this->owner->portal->portal_id} AND availment_id = {$this->self->availment_id} AND DATE(created_at) = DATE(NOW())");
		if ($is_availed) {
			$this->owner->redirect(array('login/', 'connection_token'=>$this->owner->connection->connection_token));
		}
		
		$object = new TattooSubscriber;
		if (Yii::app()->request->getPost('TattooSubscriber')) {
			self::_validateLogin($object);
		}
		
		$cancel_url = Yii::app()->createUrl('scene_handler',array('connection_token'=>$this->owner->connection->connection_token));
		
		$plans = array('1'=>'Tattoo @Home', '2'=>'Tattoo Platinum');
		$home_user = AvailTattoo::model()->with('dev_avail')->find("device_id = {$this->owner->device->device_id} AND portal_id = {$this->owner->portal->portal_id} AND plan = 1");
		if ($home_user) {
			$plans = array('2'=>'Tattoo Platinum');
		}
		$this->render('index', array('availment_id'=>$this->self->availment_id, 'plans'=>$plans,
			'object'=>$object, 'cancel'=>$cancel_url));
    }
	
	/**
	 * Verify login details
	 * 
	 * @access private
	 * @param object $object TattooSubscriber instance
	 * @return JSON
	 */
	private function _validateLogin(TattooSubscriber $object)
	{
		$post = Yii::app()->request->getPost('TattooSubscriber');
		$object->attributes = $post;
		$object->setScenario('login');
		
		if ($object->validate()) {
			$subscriber = array();
			$str = str_replace('-', '', $post['account_no']);
			$strlen = strlen($str);
			
			$str = substr($str, -7);
			$subscriber = TattooSubscriber::model()->find("network_service_id LIKE '%{$str}' AND plan = {$post['plan']}");
			
			if (empty($subscriber)) {
				$subscriber = TattooSubscriber::model()->findByAttributes(array('account_no'=>$post['account_no'], 'plan'=>$post['plan']));
			}
			
			if ($subscriber) {
				// Checked if already used
				$availed = AvailTattoo::model()->findByAttributes(array('account_no'=>$subscriber->account_no, 'plan'=>$subscriber->plan));
				if ($availed) {
					$json_array = array('status'=>0, 'message'=>'<i class="glyphicon glyphicon-exclamation-sign"></i> Account already used.');
					die(CJSON::encode($json_array));
				}
				
				$object = new AvailTattoo();
				$object->fullname = $subscriber->name;
				$object->account_no = $subscriber->account_no;
				$object->plan = $subscriber->plan;
				
				if ($object->validate()) {
					// New instance of DeviceAvailment
					$dev_avail = $this->owner->saveDeviceAvailment($this->self->availment_id);
					
					if ($dev_avail->validate()) {
						// Update connection and save to DB
						if (self::_updateConnection($object, $this->self_portal) && $dev_avail->save()) {
							// Save record
							$object->device_availment_id = $dev_avail->device_availment_id;
							$object->save();
							
							$url = $this->owner->createAbsoluteUrl('access/', array('connection_token'=>$this->owner->connection->connection_token,
								'availment_id'=>$this->self->availment_id));
							$minutes = $this->self_portal->duration/60;
							$json_array = array('status'=>1, 'message'=>"<h3>You now have {$minutes} minutes FREE access </h3> </br> Thank you and enjoy your <strong>FREE WiFi access", 'redirect'=>$url);
							die(CJSON::encode($json_array));
						}
					}
				}
			}
			else {
				$json_array = array('status'=>0, 'message'=>'<i class="glyphicon glyphicon-exclamation-sign"></i> Account details not found.');
				die(CJSON::encode($json_array));
			}
		}
		
		$error_fields = array();
		foreach ($object->errors as $field => $error_message) {
			array_push($error_fields, CHtml::modelName($object).'_'.$field);
		}
		$json_array = array('status'=>0, 'message'=>'<i class="glyphicon glyphicon-exclamation-sign"></i> All fields are required', 'fields'=>$error_fields);
		die(CJSON::encode($json_array));
	}
    
    /**
	 * Update connection details
	 * 
	 * @access private
	 * @param object $object AvailTattoo instance
	 * @param object $availment PortalAvailment instance
	 * @return boolean
	 */
	private function _updateConnection(AvailTattoo $object, PortalAvailment $availment)
	{
		$id = 0;
		switch ($object->plan) {
			case 2:
				$id = $this->platinum;
			break;
			default:
				$id = $this->home;
			break;
		}
				
		$minutes = $availment->duration/60;
		$connection = $this->owner->connection;
		$connection->username = $this->owner->device->mac_address.'@'.$this->owner->portal->code;
		$connection->password = $object->account_no;
		
		if ($connection->validate() && $connection->save()) {
			// Correlor log
			$data = array('actionType'=>'access_point_login',
				'itemId'=>$this->owner->connection->location_id,
				'actiontime'=>date('YmdHis+0800'),
				'details'=>array(array('name'=>'max_session_time', 'value'=>$minutes)));
			Correlor::reportAction($this->owner->device->device_id, 'access_point_login', $data);
			
			// if home
			if ($object->plan == 1) {
				DeviceScenario::model()->deleteAll("device_id = {$this->owner->device->device_id} AND portal_id = {$this->owner->portal->portal_id} AND availment_id <> {$this->home} AND scene = '{$this->owner->scene}'");
			}
			// if platinum
			if ($object->plan == 2) {
				DeviceScenario::model()->deleteAll("device_id = {$this->owner->device->device_id} AND portal_id = {$this->owner->portal->portal_id} AND availment_id <> {$this->platinum}");
			}
				
			// Verify connection
			if ($this->owner->connect($availment, TRUE)) {
				return TRUE;
			}
		}
		throw new CHttpException(500, Yii::t('yii','Cannot update connection. Please try again.'));
	}
}