<?php

class Free extends CWidget
{
	public $self;
	public $self_portal;
	public $action;
	public $reference;
	
	public function init()
	{
		// Set PortalAvailment instance
		$this->self_portal = PortalAvailment::model()->findByAttributes(array('portal_id'=>$this->owner->portal->portal_id, 'availment_id'=>$this->self->availment_id));
	}
	
	public function run()
	{
		switch ($this->action)
		{
			// Login or Register function
			case 'login':
				self::_login();
			break;
		
			// Display the buttons
			default:
				self::_display();
			break;
		}
		
	}
	
	/**
	 * Display the widget
	 * 
	 * @access private
	 * @return void
	 */
	private function _display()
	{
		$this->render('login', array('availment_id'=>$this->self->availment_id));
	}
	
	/**
	 * Login or Register the user to the NAS
	 * 
	 * @access private
	 * @return void
	 */
	private function _login()
	{
		// Check if already availed for today
		$is_availed = DeviceAvailment::model()->exists("device_id = {$this->owner->device->device_id} AND portal_id = {$this->owner->portal->portal_id} AND availment_id = {$this->self->availment_id} AND DATE(created_at) = DATE(NOW())");
		if ($is_availed) {
			$this->owner->redirect(array('login/', 'connection_token'=>$this->owner->connection->connection_token));
		}
		
		// Get availment of Mobile
		$globe_duration = 0;
		$xglobe_duration = 0;
		$mobile_availment = Availment::model()->findByAttributes(array('model'=>'AvailMobile'));
		if ($mobile_availment) {
			// Get availment settings
			$globe_duration = AvailmentSetting::getValue($mobile_availment->availment_id, 'GLOBE_DURATION');
			$xglobe_duration = AvailmentSetting::getValue($mobile_availment->availment_id, 'XGLOBE_DURATION');
		}
		
		// If settings are declared, check for AvailMobile object
		if ($globe_duration || $xglobe_duration) {
			$mobile = AvailMobile::model()->with('dev_avail')->find("portal_id = {$this->owner->portal->portal_id} AND device_id = {$this->owner->device->device_id} AND is_validated = 1", array('order'=>'created_at DESC'));
			if ( ! empty($mobile)) {
				$msisdn = substr($mobile->msisdn, 1);
				$prefix = Yii::app()->db->createCommand()->select('*')
					->from('Unifi_Captive.plan_prefix')->where("LEFT(prefix, 3) = LEFT({$msisdn}, 3)")
					->queryRow();

				if ($prefix) {
					if ($prefix['plan_id'] == 1 OR $prefix['plan_id'] == 2 OR $prefix['plan_id'] == 3 OR $prefix['plan_id'] == 9) {
						if ($globe_duration) {
							$this->self_portal->duration = $globe_duration * 60;
						}
					}
					else {
						if ($xglobe_duration) {
							$this->self_portal->duration = $xglobe_duration * 60;
						}
					}
				}
			}
		}
		
		// New instance of DeviceAvailment
		$dev_avail = $this->owner->saveDeviceAvailment($this->self->availment_id);
		
		// Check if validation paseses
		if ($dev_avail->validate()) {
			// Update connection and save to DB
			if (self::_updateConnection($this->self_portal) && $dev_avail->save()) {
				$this->owner->redirect(array('access/',
					'connection_token'=>$this->owner->connection->connection_token,
					'availment_id'=>$this->self->availment_id));
			}
		}
	}
	
	/**
	 * Update connection details
	 * 
	 * @access private
	 * @return boolean
	 */
	private function _updateConnection(PortalAvailment $availment)
	{
		$connection = $this->owner->connection;
		$connection->username = $this->owner->device->mac_address.'@'.$this->owner->portal->code;
		$connection->password = $this->owner->device->mac_address;
		
		if ($connection->validate() && $connection->save()) {
			// Verify connection
			if ($this->owner->connect($availment, TRUE)) {
				return TRUE;
			}
		}
		
		throw new CHttpException(500, Yii::t('yii','Cannot update connection. Please try again.'));
	}
}