<?php

class AccessController extends CaptivePortal
{
	public function actionIndex()
	{
		$availment_id = Yii::app()->request->getQuery('availment_id');
		
		// Check if there is a connection made
		$connect = ConnectionAvail::model()->findByAttributes(array('device_connection_id'=>$this->connection->device_connection_id, 'availment_id'=>$availment_id));
		if (empty($connect)) {
			throw new CHttpException(400,Yii::t('yii','No connection made. Please try again.'));
		}
		
		$session_duration = Device::getSessionTime($this->connection);
		if ($session_duration['current_session_time']) {
			if (PortalConfiguration::getValue('REDIRECT_URL')) {
			   $this->redirect(PortalConfiguration::getValue('REDIRECT_URL'));
			}
		}
		
		Yii::app()->clientScript->registerCssFile(Link::css_url('portal/ayala-app-style.css'));

		$this->render('index', array('minutes'=>$connect->duration/60));
	}
}