<?php
//session_start();

class SurveyLogin extends CWidget
{
	public $self;
	public $self_portal;
	public $action;
	public $reference;
    
	private $_survey;
	private $_questions;
	private $_answers;
	private $_count;
	
	public function init()
	{
		// Set PortalAvailment instance
		$this->self_portal = PortalAvailment::model()->findByAttributes(array('portal_id'=>$this->owner->portal->portal_id, 'availment_id'=>$this->self->availment_id));
	}
	
	public function run()
	{
		switch ($this->action)
		{
			// Login or Register function
			case 'login':
				self::_login();
			break;
		
			// Display the buttons
			default:
				self::_display();
			break;
		}
		
	}
	
	/**
	 * Display the widget
	 * 
	 * @access private
	 * @return void
	 */
	private function _display()
	{
        $this->render('login', array('availment_id'=>$this->self->availment_id));
    }
	
	/**
	 * Login or Register the user to the NAS
	 * 
	 * @access private
	 * @return void
	 */
	private function _login()
	{
		// Check if already availed for today
		$availed = DeviceScenario::model()->exists("device_id = {$this->owner->device->device_id} AND portal_id = {$this->owner->portal->portal_id} AND availment_id = {$this->self->availment_id} AND DATE(updated_at) = DATE(NOW())");
		if ($availed) {
			$this->owner->redirect(array('access', 'connection_token'=>$this->owner->connection->connection_token, 'availment_id'=>$this->self->availment_id));
		}
        
		if (Yii::app()->request->isPostRequest) {
			self::_validatePost();
		}
		
		//Set application variables
		self::_setApplication();
		
		$this->render('form', array('survey'=>$this->_survey, 'questions'=>$this->_questions, 'answers'=>$this->_answers));
	}
	
	private function _validatePost()
	{
		$this->_survey = Survey::model()->findByPk(Yii::app()->request->getPost('survey_id'));
		$this->_count = SurveyQuestion::model()->countByAttributes(array('survey_id'=>$this->_survey->survey_id), array('order'=>'position ASC'));
		
		$question_post = Yii::app()->request->getPost('question');
		
		if ( ! empty($question_post)) {
			
		}
		if (Yii::app()->request->getPost('position') < $this->_count) {
			// Display question
			$position = Yii::app()->request->getPost('position') + 1;
			self::_setQuestion($position);
			$view = $this->render('questions', array('questions'=>$this->_questions,
				'answers'=>$this->_answers,
				'position'=>$position,
				'count'=>$this->_count,
				'question_post'=>$question_post), TRUE);
			die(CJSON::encode(array('status'=>1, 'message'=>$view, 'position'=>$position)));
		}
		
		$question_post = Yii::app()->request->getPost('question');
		
		foreach ($question_post as $id=>$value) {
			$question = SurveyQuestion::model()->findByPk($id);
			
			if (is_array($value)) {
				reset($value);
				$first_index = (key($value));
				
				if ($question->is_required && (empty($question_post[$id][$first_index]))) {
					$json_array = array('status'=>0, 'message'=>'<i class="glyphicon glyphicon-exclamation-sign"></i> Please answer all required items');
					die(CJSON::encode($json_array));
				}
			}
			else {
				if ($question->is_required && (empty($value))) {
					$json_array = array('status'=>0, 'message'=>'<i class="glyphicon glyphicon-exclamation-sign"></i> Please answer all required items');
					die(CJSON::encode($json_array));
				}
			}
		}
		
		// New instance of DeviceAvailment
		$dev_avail = $this->owner->saveDeviceAvailment($this->self->availment_id);
		
		if ($dev_avail->validate()) {
			if (self::_updateConnection($this->self_portal) && $dev_avail->save()) {
				// Save to AvailSurvey
				$avail_survey = new AvailSurvey;
				$avail_survey->survey_id = $this->_survey->survey_id;
				$avail_survey->device_availment_id = $dev_avail->device_availment_id;
				$avail_survey->title = $this->_survey->title;
				$avail_survey->validate() && $avail_survey->save();
				
				foreach ($question_post as $id=>$value) {
					$question = SurveyQuestion::model()->findByPk($id);

					// Save to AvailQuestion
					$avail_question = new AvailSurveyQuestion;
					$avail_question->avail_survey_id = $avail_survey->avail_survey_id;
					$avail_question->question = $question->question;
					$avail_question->validate() && $avail_question->save();

					foreach ($value as $answer) {
						// Save to AvailAnswer
						$avail_answer = new AvailSurveyAnswer;
						$avail_answer->avail_question_id = $avail_question->avail_question_id;
						$avail_answer->answer = $answer;
						$avail_answer->validate() && $avail_answer->save();
					}
				}

				$url = $this->owner->createAbsoluteUrl('access/', array('connection_token'=>$this->owner->connection->connection_token,
					'availment_id'=>$this->self->availment_id));
				$minutes = $this->self_portal->duration/60;
				$json_array = array('status'=>2, 'message'=>"You can use Globe GoWifi, FREE trial for {$minutes} minutes today! Happy Surfing.", 'redirect'=>$url);
				die(CJSON::encode($json_array));
			}
		}
	}
	
	/**
	 * Update connection details
	 * 
	 * @access private
	 * @param object $availment PortalAvailment instance
	 * @return boolean
	 */
	private function _updateConnection(PortalAvailment $availment)
	{
		$connection = $this->owner->connection;
		$connection->username = $this->owner->device->mac_address.'@'.$this->owner->portal->code;
		$connection->password = "SURVEY";
		
		if ($connection->validate() && $connection->save()) {
			// Verify connection
			if ($this->owner->connect($availment, TRUE)) {
				return TRUE;
			}
		}
		throw new CHttpException(500, Yii::t('yii','Cannot update connection. Please try again.'));
	}
	
	/**
	 * Set application variables
	 * 
	 * @access private
	 * @param int position
	 * @return void
	 */
	private function _setApplication()
	{
		$position = 0;
		$availed_survey = AvailSurvey::model()->with('dev_avail')->find("portal_id = {$this->owner->portal->portal_id} AND device_id = {$this->owner->device->device_id} AND availment_id = {$this->self->availment_id}");
		if ($availed_survey) {
			$survey = Survey::model()->findByPk($availed_survey->survey_id);
			$position = $survey->position;
		}
		$this->_survey = Survey::model()->findByAttributes(array('portal_id'=>$this->owner->portal->portal_id, 'position'=>$position+1), array('order'=>'position ASC'));
		if ( ! empty($this->_survey)) {
			return TRUE;
		}
		
		return FALSE;
	}
	
	private function _setQuestion($position = 1)
	{
		if (empty($this->_survey)) {
			return FALSE;
		}
		
		$this->_questions = SurveyQuestion::model()->findAllByAttributes(array('survey_id'=>$this->_survey->survey_id, 'position'=>$position), array('order'=>'position ASC'));
		if ( ! empty($this->_questions)) {
			$answer_types = array('text', 'dropdown', 'checkbox', 'radio');
			foreach ($this->_questions as $q) {
				foreach ($answer_types as $type) {
					$this->_answers[$q->survey_question_id][$type] = SurveyAnswer::model()->findAllByAttributes(array('survey_question_id'=>$q->survey_question_id, 'answer_type'=>$type), array('order'=>'position ASC'));
				}
			}
		}
	}
}