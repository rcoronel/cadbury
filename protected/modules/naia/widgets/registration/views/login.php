<?php $form=$this->beginWidget('CActiveForm', array('method'=>'post', 'action'=>$url)); ?>
	<div id="form-error" class="alert alert-danger col-sm-4 col-sm-offset-4" role="alert" style="display:none;"></div>
	<div class="form-group">
		<?php echo $form->labelEx($object,'email'); ?>
		<?php echo $form->textField($object,'email', array('class'=>'form-control', 'placeholder'=>'Email Address')); ?>
		<?php echo $form->error($object,'email', array('class'=>'alert alert-danger')); ?>
		<?php echo CHtml::hiddenField('availment_id', $this->self->availment_id); ?>
	</div>
	<?php echo CHtml::htmlButton(html_entity_decode('Submit'), array('type'=>'submit', 'class'=>'getconnectedbtn yellow'));?>
<?php $this->endWidget();?>


<script>
	$('form').on('submit', function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		// hide errors
		$('div#form-error').slideUp( 300 ).html();

		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");

		$.ajax( {
			url : formURL,
			type: "POST",
			data : postData,
			dataType: 'json',
			success:function(data, textStatus, jqXHR)
			{
				if ( ! data.status) {
					setTimeout(function() {
						$('#loader-modal').modal('hide');
						$('div#form-error').slideDown(300).delay(800).html(data.message);
						$('div#form-error').show();
					}, 900);
				}
				else {
					setTimeout(function(){
						window.location.href = data.redirect;
					}, 3000);
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				setTimeout(function() {
					$('#loader-modal').modal('hide');
					$('div#form-error').slideDown(300).delay(800).html(jqXHR.responseText);
					$('div#form-error').show();
				}, 900);
			}
		});
		e.preventDefault();
	});
</script>