<?php

require __DIR__.'/api/mobile_api.php';
class Mobile extends CWidget
{
    public $self;
	public $self_portal;
	public $action;
	public $reference;
	
	public function init()
	{
		// Set PortalAvailment instance
		$this->self_portal = PortalAvailment::model()->findByAttributes(array('portal_id'=>$this->owner->portal->portal_id, 'availment_id'=>$this->self->availment_id));
	}
        
	public function run()
	{
		switch ($this->action)
		{
			// Login or Register function
			case 'login':
				self::_login();
			break;
		
			// verify code
			case 'verify':
				self::_verify();
			break;
		
			// Display the buttons
			default:
				self::_display();
			break;
		}
	}
	
	/**
	 * Display the widget
	 * 
	 * @access private
	 * @return void
	 */
	private function _display()
	{

		$url = Yii::app()->createUrl('login/attempt', array('connection_token'=>$this->owner->connection->connection_token,'availment_id'=>$this->self->availment_id));
		
		$this->render('login', array('availment_id'=>$this->self->availment_id, 'url'=>$url));

	}
	
	/**
	 * Login or Register
	 * 
	 * @access private
	 * @return void
	 */
	private function _login()
	{
		// Check if already availed for today
		$is_availed = AvailMobile::model()->with('dev_avail')->exists("device_id = {$this->owner->device->device_id} AND portal_id = {$this->owner->portal->portal_id} AND availment_id = {$this->self->availment_id} AND DATE(dev_avail.created_at) = DATE(NOW()) AND is_validated = 1");
		if ($is_availed) {
			$this->owner->redirect(array('login/', 'connection_token'=>$this->owner->connection->connection_token));
		}
		$availed = AvailMobile::model()->with('dev_avail')->find("device_id = {$this->owner->device->device_id} AND portal_id = {$this->owner->portal->portal_id} AND availment_id = {$this->self->availment_id} AND DATE(dev_avail.created_at) = DATE(NOW()) AND is_validated = 0");
		if($availed && $availed->auth_limit != 0){
			$this->owner->redirect(array('login/',	'connection_token'=>$this->owner->connection->connection_token,
							'availment_id'=>$this->self->availment_id,
							'mobile'=>$availed->msisdn,
							'action'=>'verify'));
		}

		$mobile = new AvailMobile;
		if (Yii::app()->request->isPostRequest) {
			self::_validatePost($mobile);
		}
		
		$this->render('index', array('availment_id'=>$this->self->availment_id,
			'object'=>$mobile,
			'duration'=>$this->self_portal->duration/60));
	}
	
	/**
	 * Validate mobile no.
	 * 
	 * @access private
	 * @param object $object AvailMobile
	 * @return JSON
	 */
	private function _validatePost(AvailMobile $object)
	{
		$post = Yii::app()->request->getPost('AvailMobile');
		$object->attributes = $post;
		
		if ($object->validate()){
			// Check if record already existed
			$mobile = AvailMobile::model()->with('dev_avail')->find("msisdn = {$object->msisdn} AND portal_id = {$this->owner->portal->portal_id} AND device_id = {$this->owner->device->device_id} AND DATE(dev_avail.created_at) = DATE(NOW())");
			if ( ! empty($mobile)) {
				$mobile->msisdn = $object->msisdn;
				$mobile->auth_limit++;
				$object = $mobile;
			}
			else {
				$object->auth_limit = 1;
			}
			
			// Check for limit
			if ($object->auth_limit > 3) {
				$json_array = array('status'=>0, 'message'=>'Resending of code limit already reached');
				die(CJSON::encode($json_array));
			}
			
			// new authentication code
			$object->auth_code = mt_rand(10000, 99999);
			$message = "{$object->auth_code} - This is your Globe GoWifi verification code.";
			if ( ! Mobile360::send($object->msisdn, $message)) {
				$json_array = array('status'=>0, 'message'=>'Error encountered. Try again later. (SITE-008)');
				die(CJSON::encode($json_array));
			}
			
			if ($object->save()) {
				if (isset($object->dev_avail) && ! empty($object->dev_avail)) {
					$dev_avail = $object->dev_avail;
				}
				else {
					$dev_avail = $this->owner->saveDeviceAvailment($this->self->availment_id, TRUE);
					
					// update object
					$object->device_availment_id = $dev_avail->device_availment_id;
				}
				
				$object->validate() && $object->save();
				
				$url = $this->owner->createAbsoluteUrl('login/',
					array(	'connection_token'=>$this->owner->connection->connection_token,
							'availment_id'=>$this->self->availment_id,
							'mobile'=>$object->msisdn,
							'action'=>'verify'));
				$json_array = array('status'=>1, 'message'=>'Kindly verify your mobile number by using the verification code sent to you', 'redirect'=>$url);
				die(CJSON::encode($json_array));
			}
		}
		else {
			$json_array = array('status'=>0, 'message'=>'<i class="glyphicon glyphicon-exclamation-sign"></i> Invalid mobile no.');
			die(CJSON::encode($json_array));
		}
	}
	
	/**
	 * Verify the 5 digit code
	 * 
	 * @access private
	 * @return mixed
	 */
	private function _verify()
	{
		$mobile_no = Yii::app()->request->getParam('mobile');
		$mobile = AvailMobile::model()->with('dev_avail')->find("msisdn = {$mobile_no} AND portal_id = {$this->owner->portal->portal_id} AND device_id = {$this->owner->device->device_id} AND DATE(dev_avail.created_at) = DATE(NOW())");
		if ($mobile) {
			if ($mobile->is_validated) {
				// already validated, redirect to login
				// no record, redirect to login page
				$this->owner->redirect(array('login/', 'connection_token'=>$this->owner->connection->connection_token));
			}
			
			// Check if submitted via POST
			if (Yii::app()->request->isPostRequest) {
				// Check if the POST is for code resending
				if (Yii::app()->request->getPost('resend')) {
					self::_resendCode($mobile);
				}
				
				if (Yii::app()->request->getPost('change_number')) {
					self::_resetLimit($mobile);
				}

				$post = Yii::app()->request->getPost('AvailMobile');
				
				// Check if the codes match
				if ($mobile->auth_code != $post['auth_code']) {
					$json_array = array('status'=>0, 'message'=>'Incorrect 5-digit code');
					die(CJSON::encode($json_array));
				}
				
				// Check for limit
				if ($mobile->auth_limit > 3) {
					$json_array = array('status'=>0, 'message'=>'Resending of code limit already reached');
					die(CJSON::encode($json_array));
				}
				
				$mobile->is_validated = 1;
				$mobile->auth_limit++;
				
				if ($mobile->validate()) {
					// Alter duration
					$globe_duration = AvailmentSetting::getValue($this->self->availment_id, 'GLOBE_DURATION');
					$xglobe_duration = AvailmentSetting::getValue($this->self->availment_id, 'XGLOBE_DURATION');
					if ($globe_duration || $xglobe_duration) {
						$msisdn = substr($mobile->msisdn, 1);
						$prefix = Yii::app()->db->createCommand()->select('*')
							->from('Unifi_Captive.plan_prefix')->where("LEFT(prefix, 3) = LEFT({$msisdn}, 3)")
							->queryRow();
							
						if ($prefix) {
							if ($prefix['plan_id'] == 1 OR $prefix['plan_id'] == 2 OR $prefix['plan_id'] == 3 OR $prefix['plan_id'] == 9) {
								if ($globe_duration) {
									$this->self_portal->duration = $globe_duration * 60;
								}
							}
							else {
								if ($xglobe_duration) {
									$this->self_portal->duration = $xglobe_duration * 60;
								}
							}
						}
					}
					
					if (self::_updateConnection($mobile, $this->self_portal)) {
						$mobile->save();
						
						$minutes = $this->self_portal->duration/60;
						$url = $this->owner->createAbsoluteUrl('access/', array('connection_token'=>$this->owner->connection->connection_token,
							'availment_id'=>$this->self->availment_id));
						$json_array = array('status'=>1, 'message'=>"You can use Globe GoWifi, FREE trial for {$minutes} minutes today! Happy Surfing.", 'redirect'=>$url);
						die(CJSON::encode($json_array));
					}
					else {
						$json_array = array('status'=>0, 'message'=>'Something went wrong');
						die(CJSON::encode($json_array));
					}
				}
				
				$json_array = array('status'=>0, 'message'=>'Something went wrong');
				die(CJSON::encode($json_array));
				
			}
			$this->render('verify_code', array('mobile'=>$mobile));
			
		}
		else {
			// no record, redirect to login page
			$this->owner->redirect(array('login/', 'connection_token'=>$this->owner->connection->connection_token));
		}
	}
	
	/**
	 * Resend code for mobile availment
	 * 
	 * @access private
	 * @param object $object AvailMobile instance
	 * @return JSON
	 */
	private function _resendCode(AvailMobile $object)
	{
		if ($object->auth_limit >= 3) {
			$json_array = array('status'=>0, 'message'=>'Maximum limit (3) of sending code already reached');
			die(CJSON::encode($json_array));
		}
		else {
			// new authentication code
			$object->auth_code = mt_rand(10000, 99999);
			$object->auth_limit++;
			$message = "{$object->auth_code} - This is your Globe WiFi verification code.";
			Mobile360::send($object->msisdn, $message);
			
			if ($object->validate() && $object->save()) {
				$url = $this->owner->createAbsoluteUrl('login/',
					array(	'connection_token'=>$this->owner->connection->connection_token,
							'availment_id'=>$this->self->availment_id,
							'mobile'=>$object->msisdn,
							'action'=>'verify'));
				$json_array = array('status'=>1, 'message'=>'Kindly verify your mobile number by using the verification code sent to you', 'redirect'=>$url);
				die(CJSON::encode($json_array));
			}
		}
	}
	
	/**
	 * Update connection details
	 * 
	 * @access private
	 * @param object $mobile AvailMobile instance
	 * @param object $availment PortalAvailment instance
	 * @return boolean
	 */
	private function _updateConnection(AvailMobile $mobile, PortalAvailment $availment)
	{
		$connection = $this->owner->connection;
		$connection->username = $this->owner->device->mac_address.'@'.$this->owner->portal->code;
		$connection->password = $mobile->msisdn;
		
		if ($connection->validate() && $connection->save()) {
			// Verify connection
			if ($this->owner->connect($availment, TRUE)) {
				return TRUE;
			}
			return FALSE;
		}
		throw new CHttpException(500, Yii::t('yii','Cannot update connection. Please try again.'));
	}

	public function _resetLimit($mobile){

		$mobile->auth_limit = 0;

		if ($mobile->validate() && $mobile->save()) {
			$url = $this->owner->createAbsoluteUrl('login/',
				array(	'connection_token'=>$this->owner->connection->connection_token,
						'availment_id'=>$this->self->availment_id,
						'mobile'=>$mobile->msisdn,
						'action'=>'login'));
			$json_array = array('status'=>1, 'message'=>'Redirecting to mobile input page', 'redirect'=>$url);
			die(CJSON::encode($json_array));
		}
	}
}