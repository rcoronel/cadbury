<?php

class AccessController extends CaptivePortal
{
	public function actionIndex()
	{
		$availment_id = Yii::app()->request->getQuery('availment_id');
		
		// Check if there is a connection made
		$connect = ConnectionAvail::model()->findByAttributes(array('device_connection_id'=>$this->connection->device_connection_id, 'availment_id'=>$availment_id));
		if (empty($connect)) {
			throw new CHttpException(400,Yii::t('yii','No connection made. Please try again.'));
		}
		
		$url = $this->createAbsoluteUrl('access/show', array('connection_token'=>$this->connection->connection_token));
		
		$this->render('index', array('url'=>$url));
	}
	
	public function actionShow()
	{
		if ( ! empty($this->connection->username)) {
			$accounting = RadiusAcct::model()->find("username = '{$this->connection->username}' AND DATE(acctstarttime) = DATE(NOW()) ORDER BY acctstarttime DESC");
			if ( ! empty($accounting->acctstoptime)) {
				$this->redirect(array('landing/', 'connection_token'=>$this->connection->connection_token));
			}
		}
		$this->render('ads');
	}
}