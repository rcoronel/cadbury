<?php

class LandingController extends CaptivePortal
{
	/**
	 * Declare variables
	 * 
	 * @access protected
	 * @param object $action CAction instance
	 * @return bool
	 */
	protected function beforeAction($action)
	{
		parent::beforeAction($action);
		
		return TRUE;
	}
	
	/**
	 * Show landing page
	 * 
	 * @access public
	 * @return void
	 */
	public function actionIndex()
	{
		// Use custom layout
		$this->layout = '/layouts/splash';
		
		$url = $this->createAbsoluteUrl('landing/show', array('connection_token'=>$this->connection->connection_token));
		
		$this->render('index', array('url'=>$url));
	}
	
	/**
	 * Show intial landing page
	 * 
	 * @access public
	 * @return void
	 */
	public function actionShow()
	{
		$has_avail = self::_hasAvailments();
		if ($has_avail) {
			$this->redirect(array('landing/continue'));
		}
		
		// Use custom layout
		$this->layout = '/layouts/splash';
		
		$url = $this->createAbsoluteUrl('login/', array('connection_token'=>$this->connection->connection_token));
		$terms = Cms::model()->findByAttributes(array('portal_id'=>$this->portal->portal_id, 'friendly_url'=>'terms-and-conditions'));
		$policy = Cms::model()->findByAttributes(array('portal_id'=>$this->portal->portal_id, 'friendly_url'=>'service-agreement'));
		
		$this->render('landing', array('url'=>$url, 'terms'=>$terms, 'policy'=>$policy));
	}
	
	/**
	 * Show alternate landing page
	 * 
	 * @access public
	 * @return void
	 */
	public function actionContinue()
	{
		// Use custom layout
		$this->layout = '/layouts/splash';
		
		$url = $this->createAbsoluteUrl('login/', array('connection_token'=>$this->connection->connection_token));
		$terms = Cms::model()->findByAttributes(array('portal_id'=>$this->portal->portal_id, 'friendly_url'=>'terms-and-conditions'));
		$policy = Cms::model()->findByAttributes(array('portal_id'=>$this->portal->portal_id, 'friendly_url'=>'service-agreement'));
		
		$this->render('continue', array('url'=>$url, 'terms'=>$terms, 'policy'=>$policy));
	}
	
	private function _hasAvailments()
	{
		// catch remaining session usage
		$session_array = self::remainingSession();
		if (isset($session_array['duration']) AND $session_array['duration'] < $session_array['max']) {
			$this->redirect(array('login/', 'connection_token'=>$this->connection->connection_token));
		}
		
		$fb_avail = Availment::model()->findByAttributes(array('model'=>'AvailFacebook'));
		$fb = AvailFacebook::model()->with('dev_avail')->find("availment_id={$fb_avail->availment_id} AND device_id={$this->device->device_id} AND portal_id={$this->portal->portal_id}");
		
		$reg_avail = Availment::model()->findByAttributes(array('model'=>'AvailRegistration'));
		$reg = AvailRegistration::model()->with('dev_avail')->find("availment_id={$reg_avail->availment_id} AND device_id={$this->device->device_id} AND portal_id={$this->portal->portal_id}");
		
		if ( ! empty($fb) OR ! empty($reg)) {
			$this->redirect(array('landing/continue', 'connection_token'=>$this->connection->connection_token));
		}
	}
}