<section class="clearfix registration">
	<div class="reg-wrapper">
		<h1>Registration</h1>
		<p>Register with facebook or email to enjoy browsing.</p>
		<div class="regaction">
			<?php foreach ($availments as $a):?>
				<?php echo $a->availment->description; ?>
				<?php $this->widget("{$this->portal->code}.widgets.{$a->availment->code}", array('self' => $a));?>
			<?php endforeach;?>
		</div>
	</div>
	<div class="clearfix adswrapper mgt-30">
		<div class="adsholder">
			<div id='div-gpt-ad-1477039622953-0' style=' width:300px;'>
				<p>Advertisement</p>
				<script>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1477039622953-0'); });
				</script>
			</div>
		</div>
	</div>
</section>

<div class="modal modal-wifi in" id="loader-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-body small-modal-body-style">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--<script>
$(document).ready(function(){
	$('form').on('submit',function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		e.preventDefault();
	});
});
</script>-->