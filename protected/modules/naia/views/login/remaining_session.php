<section class="clearfix portal">
	<div class="logo clearfix">
		<?php if ($this->logo):?>
		   <?php echo CHtml::image($this->logo, $this->site_title, array('class'=>'img-responsive'));?>
		<?php endif;?>
		<?php echo PortalConfiguration::getValue('LANDING_MESSAGE');?>
	</div>
	<div class="clearfix btn-wrapper">
		<h3>
			You can still continue using the FREE internet connection
		</h3>
		<?php echo CHtml::beginForm();?>
			<?php echo CHtml::hiddenField('availment_id', $availment->availment_id);?>
			<?php echo CHtml::htmlButton('Continue browsing', array('type'=>'submit', 'class'=>'getconnectedbtn', 'style'=>'margin-top:0%'));?>
		<?php echo CHtml::endForm();?>
		
		<p>By clicking the button, you agree to the Terms of Service and Privacy Policy</p>
	</div>
	
	<div class="clearfix adsnippets-wrapper buttom">
		<p class="text-white">Advertisement</p>
		<div id='div-gpt-ad-1477039860576-0' style='width:100%;'>
			<script>
			googletag.cmd.push(function() { googletag.display('div-gpt-ad-1477039860576-0'); });
			</script>
		</div>
	</div>
</section>

<div class="modal modal-wifi in" id="loader-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-body small-modal-body-style">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	
	$('form').on('submit',function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		//e.preventDefault();
	});
});
</script>