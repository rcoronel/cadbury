<section class="clearfix portal">
	<div class="logo clearfix">
		<?php if ($this->logo):?>
		   <?php echo CHtml::image($this->logo, $this->site_title, array('class'=>'img-responsive'));?>
		<?php endif;?>
		<?php echo PortalConfiguration::getValue('LANDING_MESSAGE');?>
	</div>
	<!--button-->
	<div class="clearfix btn-wrapper">
		<a href="javascript:void(0)" class="getconnectedbtn" data-toggle="modal" data-target="#myModal">Get Connected</a>
	   <!--terms of service-->
	   <p>By clicking the button, you agree to the Terms of Service and Privacy Policy</p>
	</div>
	<!--small ad snippets-->
	<div class="clearfix adsnippets-wrapper hidden-lg hidden-sm hidden-md buttom">
		<div id='div-gpt-ad-1477039860576-0' style='width:320px;'>
			<p class="text-white">Advertisement</p>
			<script>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1477039860576-0'); });
			</script>
		</div>
	</div>
	
	<!--big devices  ad snippets-->
	<div class="clearfix adsnippets-wrapper hidden-xs large buttom">
		<div id='div-gpt-ad-1477453142488-0' style=' width:728px;'>\
			<p class="text-white">Advertisement</p>
			<script>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1477453142488-0'); });
			</script>
		</div>
	</div>
</section>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Terms of Service</h4>
			</div>
			<div class="modal-body">
				<p>Lorem ipsum dolor sit amet, conseco tetur oiu adipisicing elit, sed do eiusm od tempor ertun incididunt ut labore et dolore magna aliqua. Ut enim ad minimoun veniam uisno</p>

				<p>Exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>

				<p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia reno deserunt mollit anim id est. </p>

				<p>Lorem ipsum dolor sit amet, conseco tetur oiu adipisicing elit, sed do eiusm od tempor ertun incididunt ut labore et dolore magna aliqua. Ut enim ad minimoun veniam uisno</p>

				<p>Exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>

				<p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia reno deserunt mollit anim id est. </p>
				<p>Lorem ipsum dolor sit amet, conseco tetur oiu adipisicing elit, sed do eiusm od tempor ertun incididunt ut labore et dolore magna aliqua. Ut enim ad minimoun veniam uisno</p>

				<p>Exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>

				<p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia reno deserunt mollit anim id est. </p>
			</div>
			<div class="modal-footer">
				<a href="<?php echo $url;?>" class="getconnectedbtn blue">Accept</a>
			</div>
		</div>
	</div>
	<!--small devices ad snippets-->
	<div class="clearfix adsnippets-wrapper hidden-lg hidden-md hidden-sm  modalads">
		<div id='div-gpt-ad-1477039860576-0' style='width:320px;'>
			<p class="text-white">Advertisement</p>
			<script>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1477039860576-0'); });
			</script>
		</div>
	</div>
	
	<!--large devices ad snippets-->
	<div class="clearfix adsnippets-wrapper large hidden-xs modalads">
		<div id='div-gpt-ad-1477453142488-0' style=' width:728px;'>\
			<p class="text-white">Advertisement</p>
			<script>
			googletag.cmd.push(function() { googletag.display('div-gpt-ad-1477453142488-0'); });
			</script>
		</div>
	</div>
</div>

<div class="modal modal-wifi in" id="loader-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-body small-modal-body-style">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('button#get-connected').on('click',function(e){	
			$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		});
	});
</script>