<section class="clearfix">
	<div class="adcontainer whole clearfix">
		<!--header-->
		<div class="adheader">
			<div class="container">
				<div class="col-md-6 col-xs-6">
					<h1>ADVERTISMENT</h1>
				</div>
				<div class="col-md-6 col-xs-6">
					<a href="<?php echo $url;?>"><span class="close-btn pull-right">
						&times;
					</span></a>
				</div>
			</div>
		</div>
		<div class="clearfix snippets-wrapper">
			<?php echo CHtml::image(Link::image_url("portal/{$this->portal->portal_id}/whole-page.jpg"));?>
		</div>
	</div>
</section>
<div class="modal modal-wifi in" id="loader-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-body small-modal-body-style">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('button#get-connected').on('click',function(e){	
			$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		});
	});
</script>