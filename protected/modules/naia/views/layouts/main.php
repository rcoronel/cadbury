<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!--CSS Script-->
		<?php echo CHtml::cssFile(Link::css_url('captiveportal/normalize.css'));?>
		<?php echo CHtml::cssFile(Link::css_url('bootstrap/v3.3.5/bootstrap.min.css'));?>
		<?php echo CHtml::cssFile(Link::css_url('font-awesome/font-awesome.css'));?>
		<?php echo CHtml::cssFile($this->css_file);?>
		<?php echo CHtml::scriptFile(Link::js_url('modernizr.js'));?>
		<?php echo CHtml::scriptFile(Link::js_url('jquery/jquery-1.11.2.min.js'));?>
		<?php echo CHtml::scriptFile(Link::js_url('bootstrap/v3.3.5/bootstrap.min.js'));?>

		<title><?php echo $this->site_title;?></title>
		<script>
			var baseURL = "<?php echo Link::base_url()?>";
			var currentIndex = "<?php echo $this->createUrl($this->id.'/');?>";
			var currentURL = "<?php echo $this->createUrl('/'.$this->route,$_GET);?>";
			var token = "<?php echo Yii::app()->request->csrfToken;?>";
		</script>
		
		<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
		<script>
			var googletag = googletag || {};
			googletag.cmd = googletag.cmd || [];
		</script>

		<script>
			googletag.cmd.push(function() {
				googletag.defineSlot('/135209611/gowifi.ios.landing.bn', [300, 250], 'div-gpt-ad-1477039622953-0').addService(googletag.pubads());
				googletag.pubads().enableSingleRequest();
				googletag.enableServices();
			});
		</script>

		<script>
			googletag.cmd.push(function() {
			  googletag.defineSlot('/135209611/gowifi.ios.landing.lb', [320, 50], 'div-gpt-ad-1477039860576-0').addService(googletag.pubads());
			  googletag.pubads().enableSingleRequest();
			  googletag.enableServices();
			});
		</script>
	
		<script>
			googletag.cmd.push(function() {
			  googletag.defineSlot('/135209611/gowifi.TEST.landing.header', [728, 90], 'div-gpt-ad-1477453142488-0').addService(googletag.pubads());
			  googletag.pubads().enableSingleRequest();
			  googletag.enableServices();
			});
		</script>

	</head>
	<?php if ($this->id == 'access' && $this->action->id == 'show'):?>
		<body>
			<?php echo $content;?>
		</body>
	<?php else:?>
		<body class="bg-blue">
			<?php echo $content;?>
		</body>
	<?php endif;?>
</html>