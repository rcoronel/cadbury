<section class="clearfix congrats">
	<div class="reg-wrapper">
		<h1>Congratulations</h1>
		<p>You are now connected to <span class="terminal"><?php echo $this->site_title;?></span>. Enjoy 2 hour of internet access.</p>

		<div class="regaction">
			<a href="<?php echo $url;?>" class="getconnectedbtn blue">Continue</a>
		</div>
	</div>

	<div class="clearfix adswrapper">
		<div class="adsholder">
			<div id='div-gpt-ad-1477039622953-0' style=' width:300px;'>
				<p>Advertisement</p>
				<script>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1477039622953-0'); });
				</script>
			</div>
		</div>
	</div>
</section>