<section class="clearfix homepage">
	<div class="clearfix banner">
		<h1>Welcome to</h1>
		<div class="logo">
			<?php if ($this->logo):?>
				<?php echo CHtml::image($this->logo, $this->site_title, array('class'=>'img-responsive'));?>
			<?php endif;?>
		</div>
	</div>

   <div class="container">
		<!--ads static-->
		<div class="clearfix ads-widget hidden-xs large static">
			<div id='div-gpt-ad-1477453142488-0' style='height:90px; width:728px;'>
				<script>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1477453142488-0'); });
				</script>
			</div>
		</div>
		
		<!--small-devices devices ads static-->
		<div class="clearfix ads-widget hidden-lg hidden-md hidden-sm static">
			<div id='div-gpt-ad-1477039860576-0' style=' width:320px;'>
				<script>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1477039860576-0'); });
				</script>
			</div>
		</div>

		<!--facebook widgets-->
		<div class="clearfix widgets">
			<div class="fb-page" data-href="https://www.facebook.com/MIAAGovPh/" data-tabs="timeline" data-small-header="false" data-width='500' data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>
		</div>
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>

		<!--big devices ad snippets-->
		<div class="clearfix adsnippets-wrapper hidden-xs large static mgb-30">
			<p class="">Advertisement</p>
			<div id='div-gpt-ad-1477453142488-0' style='height:90px; width:728px;'>
				<script>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1477453142488-0'); });
				</script>
			</div>
		</div>
		
		<!--small devices ad snippets-->
		<div class="clearfix adsnippets-wrapper hidden-lg hidden-md hidden-sm large static mgb-30">
			<p class="">Advertisement</p>
			<div id='div-gpt-ad-1477039860576-0' style=' width:320px;'>
				<script>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1477039860576-0'); });
				</script>
			</div>
		</div>
   </div>
</section>