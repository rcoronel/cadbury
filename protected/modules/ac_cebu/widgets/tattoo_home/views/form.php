<div class="row">
    <div class="col-md-12">
        <div class="form-panel panel panel-default" data-collapsed="0">
            <div class="panel-body">
		<h2>Upload Tattoo Accounts<small> CSV File</small></h2>

                <?php
                $form = $this->beginWidget(
                    'CActiveForm',
                    array(
                        'method' => 'post',
                        'action'=>Yii::app()->createUrl('controlpanel/availment_list/view',
                            array('id'=>$this->self->Availment_ID)
                        ),
                        'id' => 'upload-form',
                        'enableAjaxValidation' => false,
                        'htmlOptions' => array('enctype' => 'multipart/form-data'),
                    )
                );
                
                echo $form->labelEx($model, 'file');
                echo $form->fileField($model, 'file');
                echo $form->error($model, 'file');
                
                echo CHtml::submitButton('Upload');
                $this->endWidget();

                ?>
		<?php if(Yii::app()->user->hasFlash('message')): ?>

                <div class="flash-error">
                    <?php echo Yii::app()->user->getFlash('message'); ?>
                </div>

                <?php endif; ?>
            </div>
        </div>  
    </div>
</div>a
<?php $this->widget('widgets.ajax.Modal');?>