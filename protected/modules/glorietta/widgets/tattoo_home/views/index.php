<div class="row">
    <div class="col-sm-6 col-sm-offset-3 text-field">
        <div class="row">
            <div class="col-xs-12">
                <h3>Tattoo Account Login</h3>
            </div>
        </div>
		<div class="col-md-6 col-md-offset-3">
			<div id="error_message" class="alert alert-danger" style="display: none;"></div>
		</div>
		<?php $form=$this->beginWidget('CActiveForm', array('method'=>'post', 'action'=>'')); ?>
            <div class="form-group">
				<?php echo $form->labelEx($object, 'plan', array('class' => 'control-label col-lg-12'));?>
				<?php echo $form->dropDownList($object, 'plan', $plans, array('class'=>'form-control', 'prompt'=>'---'));?>
            </div>
			<div class="form-group">
				<?php echo $form->labelEx($object, 'account_no', array('class' => 'control-label col-lg-12'));?>
				<?php echo $form->textField($object, 'account_no', array('class'=>'form-control', 'placeholder'=>'XXXXXXX'));?>
            </div>
			<?php echo CHtml::hiddenField('availment_id', $availment_id);?>
			<div class="conditions-content-btn">
				<?php echo CHtml::htmlButton('LOGIN', array('type'=>'submit', 'class'=>'s-btn serendra-primary-btn'));?>
				<?php echo CHtml::htmlButton('BACK', array('type'=>'button', 'class'=>'s-btn serendra-secondary-btn', 'onclick'=>"window.location='{$this->owner->createAbsoluteUrl('login/', array('connection_token'=>$this->owner->connection->connection_token))}'"));?>
            </div>
        <?php $this->endWidget();?>
    </div>
</div>

<div class="modal fade" id="loader-modal" data-backdrop="static" data-keyboard="false" tabindex="-1">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="loader-modal" data-backdrop="static" data-keyboard="false" tabindex="-1">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	$('form').on('submit',function(e){
		
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");
		
		// remove error style
		$('div.form-group').removeClass('has-error');
		$('div#error_message').hide();
		$.ajax({
			url : formURL,
			type: "POST",
			data : postData,
			dataType: 'json',
			success:function(data, textStatus, jqXHR) {
				if ( ! data.status) {
					setTimeout(function() {
						$('#loader-modal').modal('hide');
					}, 900);
					
					$('div#error_message').slideDown(300).delay(800).html(data.message);
					$('div#error_message').show();
					if (typeof data.fields !== 'undefined'){
						$.each(data.fields, function(index, value){
							$('label[for="'+value+'"]').parent().addClass('has-error');
						});
					}
				}
				else {
					setTimeout(function() {
						$('div.modal-body p').html(data.message);
					}, 900);
					setTimeout(function() {
						window.location.href = data.redirect;
					  }, 2000);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				//if fails     
			}
		});
		e.preventDefault(); //STOP default action
	});
});
</script>