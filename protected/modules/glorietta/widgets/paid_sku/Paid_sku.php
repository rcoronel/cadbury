<?php
//session_start();

class Paid_sku extends CWidget
{
	public $self;
	public $self_portal;
	public $action;
	public $reference;
	
	public function init()
	{
		// Set PortalAvailment instance
		$this->self_portal = PortalAvailment::model()->findByAttributes(array('portal_id'=>$this->owner->portal->portal_id, 'availment_id'=>$this->self->availment_id));
	}
	
	public function run()
	{
		switch ($this->action)
		{
			// Login or Register function
			case 'login':
				self::_login();
			break;
		
			// verify code
			case 'verify':
				self::_verify();
			break;

			// purchase
			case 'purchase':
				self::_purchase();
			break;
		
		
			// Display the buttons
			default:
				self::_display();
			break;
		}
		
	}
	
	/**
	 * Display the widget
	 * 
	 * @access private
	 * @return void
	 */
	private function _display()
	{
		$this->render('login', array('availment_id'=>$this->self->availment_id));
	}
	
	/**
	 * Login or Register
	 * 
	 * @access private
	 * @return void
	 */
	private function _login()
	{
		//check if has SKU availed for the day AND portal_id = {$this->owner->portal->portal_id} 
		$sku_availed = AvailSku::model()->with('dev_avail')->find("device_id = {$this->owner->device->device_id} AND is_validated = 1", array('order'=>'dev_avail.created_at desc'));

		// //check if existing sku is over
		if(!empty($sku_availed)){

			//has sku availed today 
			//reset to invalid and no free yet for the day
			$sku_availed->is_validated = 0;
			$sku_availed->promo_id = null;
			$sku_availed->validate() && $sku_availed->save();

			//check if free or sku by looking in the avail_sku_promo table
			// $availed_promo = AvailSkuPromo::model()->findByAttributes(array('avail_sku_id'=>$sku_availed->avail_sku_id), array('order' => 'created_at desc','limit' => 1));

			// if($availed_promo && $availed_promo->valid_until < date('Y:m:d H:i:s')){
			// 	//sku not yet consumed
			// 	$this->owner->redirect(array('login/', 'connection_token'=>$this->owner->connection->connection_token));
			// }

			

			$this->owner->redirect(array('login/',	'connection_token'=>$this->owner->connection->connection_token,
						'availment_id'=>$this->self->availment_id,
						'mobile'=>$sku_availed->msisdn,
						'action'=>'purchase'));
		}

		//check if already has free
		// $free_availed = DeviceAvailment::model()->exists("device_id = {$this->owner->device->device_id} AND portal_id = {$this->owner->portal->portal_id} AND availment_id = {$this->self->availment_id} AND DATE(created_at) = DATE(NOW())");
		// if ($free_availed) {
		// 	$this->owner->redirect(array('login/',	'connection_token'=>$this->owner->connection->connection_token,
		// 					'availment_id'=>$this->self->availment_id,
		// 					'mobile'=>$sku_availed->msisdn,
		// 					'action'=>'purchase'));
		// }

		//give free internet 
		// $dev_avail = $this->owner->saveDeviceAvailment($this->self->availment_id);
		// $new_sku = new AvailSku;

		// // Check if validation paseses
		// if ($dev_avail->validate()) {
		// 	// Update connection and save to DB
		// 	if (self::_updateConnection($new_sku, $this->self_portal, true) && $dev_avail->save()) {
				
		// 		$minutes = $this->self_portal->duration / 60;
		// 		$message = "You can use Globe Wifi, FREE trial for {$minutes} minutes today! Happy Surfing.";
		// 		$url = $this->owner->createAbsoluteUrl('access/', array('connection_token'=>$this->owner->connection->connection_token,
		// 			'availment_id'=>$this->self->availment_id));
		// 		$this->render('access', array('message'=>$message, 'url'=>$url));
		// 	}
		// }

		// $is_availed = AvailSku::model()->with('dev_avail')->exists("device_id = {$this->owner->device->device_id} AND portal_id = {$this->owner->portal->portal_id} AND availment_id = {$this->self->availment_id} AND DATE(dev_avail.created_at) = DATE(NOW()) AND is_validated = 1");
		// if ($is_availed) {
		// 	$this->owner->redirect(array('login/', 'connection_token'=>$this->owner->connection->connection_token));
		// }
		// / AND availment_id = {$this->self->availment_id} AND DATE(dev_avail.created_at) = DATE(NOW())
		$availed = AvailSku::model()->with('dev_avail')->find("device_id = {$this->owner->device->device_id} AND portal_id = {$this->owner->portal->portal_id} AND is_validated = 0");

		if($availed && $availed->auth_limit != 0){
			$this->owner->redirect(array('login/',	'connection_token'=>$this->owner->connection->connection_token,
							'availment_id'=>$this->self->availment_id,
							'mobile'=>$availed->msisdn,
							'action'=>'verify'));
		}

		$sku = new AvailSku;
		if (Yii::app()->request->isPostRequest) {
			self::_validatePost($sku);
		}
		
		$this->render('index', array('availment_id'=>$this->self->availment_id,
			'object'=>$sku));
	}
	
	/**
	 * Validate mobile no.
	 * 
	 * @access private
	 * @param object $object AvailMobile
	 * @return JSON
	 */
	private function _validatePost(AvailSku $object)
	{
		$post = Yii::app()->request->getPost('AvailSku');

		$existing = AvailSku::model()->find("msisdn = {$post['msisdn']}");
		// echo "<pre>";
		// print_r($existing);
		// die();
		if(!empty($existing)){
			$object = $existing;
		}else{
			$object->attributes = $post;
		}
		
		if ($object->validate()){
			// Check account type
			$acct_type = 0;
			$prefix = substr($object->msisdn, 1, 4);
			$prefix_plan = PlanPrefix::model()->with('plan')->find("prefix LIKE '%{$prefix}%'");
			if($prefix_plan){
				$acct = AccountType::model()->findByAttributes(array('name'=>ucfirst($prefix_plan->plan->plan_type)));
				$acct_type = $acct->account_type_id;
			}
			
			// Check if record already existed
			// add account type and vn for ng
			// AND DATE(dev_avail.created_at) = DATE(NOW())
			$sku = AvailSku::model()->with('dev_avail')->find("msisdn = {$object->msisdn} AND device_id = {$this->owner->device->device_id}");
			
			if ( ! empty($sku)) {
				$sku->account_type_id = $acct_type;
				$sku->msisdn = $object->msisdn;
				$sku->auth_limit++;
				$object = $sku;
			}
			else {
				$object->auth_limit = 1;
				$object->account_type_id = $acct_type;
			}
			
			// Check for limit
			if ($object->auth_limit > 3) {
				$json_array = array('status'=>0, 'message'=>'Resending of code limit already reached');
				die(CJSON::encode($json_array));
			}
			
			// new authentication code
			$object->auth_code = mt_rand(10000, 99999);
			$message = "{$object->auth_code} - This is your Globe GoWifi verification code.";
			if ( ! self::_sendcode($object->msisdn, $message)) {
				$json_array = array('status'=>0, 'message'=>'Error encountered. Try again later. (SITE-008)');
				die(CJSON::encode($json_array));
			}

			
			
			if ($object->save()) {
				if (isset($object->dev_avail) && ! empty($object->dev_avail)) {
					$dev_avail = $object->dev_avail;
				}
				else {
					$dev_avail = $this->owner->saveDeviceAvailment($this->self->availment_id, TRUE);
					$object->device_availment_id = $dev_avail->device_availment_id;
					$object->validate() && $object->save();
				}


				// echo "<pre>";
				// 	print_r($object->dev_avail);
				// 	die();
				
				$url = $this->owner->createAbsoluteUrl('login/',
					array(	'connection_token'=>$this->owner->connection->connection_token,
							'availment_id'=>$this->self->availment_id,
							'mobile'=>$object->msisdn,
							'type'=>$prefix_plan->plan_id,
							'action'=>'verify'));
				$json_array = array('status'=>1, 'message'=>'Kindly verify your mobile number by using the verification code sent to you', 'redirect'=>$url);
				die(CJSON::encode($json_array));
			}
		}
		else {
			$json_array = array('status'=>0, 'message'=>'<i class="glyphicon glyphicon-exclamation-sign"></i> Invalid mobile no.');
			die(CJSON::encode($json_array));
		}
	}
	
	/**
	 * Verify the 5 digit code
	 * 
	 * @access private
	 * @return mixed
	 */
	private function _verify()
	{
		$mobile_no = Yii::app()->request->getParam('mobile');
		$sku = AvailSku::model()->with('dev_avail')->find("msisdn = {$mobile_no} AND device_id = {$this->owner->device->device_id} "); //AND DATE(dev_avail.created_at) = DATE(NOW())

		if ($sku) {
			if ($sku->is_validated) {
				// already validated, redirect to login
				// no record, redirect to login page
				$this->owner->redirect(array('login/', 'connection_token'=>$this->owner->connection->connection_token));
			}
			
			// Check if submitted via POST
			if (Yii::app()->request->isPostRequest) {
				// Check if the POST is for code resending
				if (Yii::app()->request->getPost('resend')) {
					self::_resendCode($sku);
				}
				
				if (Yii::app()->request->getPost('change_number')) {
					self::_resetLimit($sku);
				}

				$post = Yii::app()->request->getPost('AvailSku');
				
				// Check if the codes match
				if ($sku->auth_code != $post['auth_code']) {
					$json_array = array('status'=>0, 'message'=>'Incorrect 5-digit code');
					die(CJSON::encode($json_array));
				}
				
				// Check for limit
				if ($sku->auth_limit > 3) {
					$json_array = array('status'=>0, 'message'=>'Resending of code limit already reached');
					die(CJSON::encode($json_array));
				}
				
				$sku->is_validated = 1;
				$sku->auth_limit++;
				
					
				if ($sku->validate()) {
					self::_assignDuration($sku);

					
					//check if availed is free
					$free = Availment::model()->findByAttributes(array('model'=>'AvailFree'));
					$availed_free = DeviceAvailment::model()->find("device_id = {$this->owner->device->device_id} AND  availment_id = {$free->availment_id} AND  DATE(created_at) = DATE(NOW())");
					
					//check if free or sku by looking in the avail_sku_promo table
					$availed_promo = AvailSkuPromo::model()->findByAttributes(array('avail_sku_id'=>$sku->avail_sku_id), array('order' => 'created_at desc','limit' => 1));

					if($availed_promo && date('Y-m-d H:i:s') < $availed_promo->valid_until){
						$diff = (strtotime(date('Y-m-d H:i:s')) - strtotime($availed_promo->created_at));
						// echo "<pre>";
						// print_r($diff);
						// die();
						if($diff < $availed_promo->duration){
							
							$this->self_portal->duration = ($availed_promo->duration - $diff)/60;
							//sku not yet consumed
							// Update connection and save to DB
							if (self::_updateConnection($sku, $this->self_portal, $availed_promo, false, $mobile_no)) {
								$sku->save();
								$minutes = $this->self_portal->duration / 60;
								$message = "You still have {$minutes} minutes remaining from your purchased credits! Happy Surfing.";
								$url = $this->owner->createAbsoluteUrl('access/', array('connection_token'=>$this->owner->connection->connection_token,
									'availment_id'=>$this->self->availment_id));
								$json_array = array('status'=>1, 'message'=>$message, 'redirect'=>$url);
								die(CJSON::encode($json_array));
							}
						}

					}

					if ($availed_free){
						//already has free redirect to purchase
						$url = $this->owner->createAbsoluteUrl('login/',
						array(	'connection_token'=>$this->owner->connection->connection_token,
								'availment_id'=>$this->self->availment_id,
								'mobile'=>$sku->msisdn,
								'action'=>'purchase'));
						$json_array = array('status'=>1, 'message'=>'Free internet already used. Purchase credits to continue using the service.', 'redirect'=>$url);
						die(CJSON::encode($json_array));
					}
					else {
						//no free yet so give free internet
						$dev_avail = $this->owner->saveDeviceAvailment($free->availment_id);
						$this->self_portal->availment_id = $free->availment_id;
						// Check if validation paseses
						if ($dev_avail->validate()) {
							// Update connection and save to DB
							if (self::_updateConnection($sku, $this->self_portal, false, true) && $dev_avail->save()) {
								$sku->save();
								$minutes = $this->self_portal->duration / 60;
								$message = "You can use Globe Wifi, FREE trial for {$minutes} minutes today! Happy Surfing.";
								$url = $this->owner->createAbsoluteUrl('access/', array('connection_token'=>$this->owner->connection->connection_token,
									'availment_id'=>$free->availment_id));
								$json_array = array('status'=>1, 'message'=>$message, 'redirect'=>$url);
								die(CJSON::encode($json_array));
							}
						}
					}
				}
				
				$json_array = array('status'=>0, 'message'=>'Something went wrong');
				die(CJSON::encode($json_array));
				
			}
			$this->render('verify_code', array('mobile'=>$sku));
			
		}
		else {

			// echo "<pre>";
			// print_r($sku);
			// die();
			// no record, redirect to login page
			$this->owner->redirect(array('login/', 'connection_token'=>$this->owner->connection->connection_token));
		}
	}
	
	/**
	 * Resend code for mobile availment
	 * 
	 * @access private
	 * @param object $object AvailMobile instance
	 * @return JSON
	 */
	private function _resendCode(AvailSku $object)
	{
		if ($object->auth_limit >= 3) {
			$json_array = array('status'=>0, 'message'=>'Maximum limit (3) of sending code already reached');
			die(CJSON::encode($json_array));
		}
		else {
			// new authentication code
			$object->auth_code = mt_rand(10000, 99999);
			$object->auth_limit++;
			$message = "{$object->auth_code} - This is your Globe WiFi verification code.";
			self::_sendcode($object->msisdn, $message);
			
			if ($object->validate() && $object->save()) {
				$url = $this->owner->createAbsoluteUrl('login/',
					array(	'connection_token'=>$this->owner->connection->connection_token,
							'availment_id'=>$this->self->availment_id,
							'mobile'=>$object->msisdn,
							'action'=>'verify'));
				$json_array = array('status'=>1, 'message'=>'Kindly verify your mobile number by using the verification code sent to you', 'redirect'=>$url);
				die(CJSON::encode($json_array));
			}
		}
	}
	
	/**
	 * Update connection details
	 * 
	 * @access private
	 * @param object $mobile AvailMobile instance
	 * @param object $availment PortalAvailment instance
	 * @return boolean
	 */
	private function _updateConnection(AvailSku $avail_sku, PortalAvailment $availment, $avail_sku_promo = false, $free = false, $mobile = false)
	{

		$connection = $this->owner->connection;
		// $connection->username = $this->owner->device->mac_address.'@'.$this->owner->portal->code;
		// $connection->password = ($free ? $this->owner->device->mac_address : $sku->msisdn);
		if($free){
			$connection->username = $this->owner->device->mac_address.'@'.$this->owner->portal->code;
			$connection->password = $this->owner->device->mac_address;

			if ($connection->validate() && $connection->save()) {
				// Verify connection
				if ($this->owner->connect($availment, TRUE)) {
					return TRUE;
				}
				return FALSE;
			}

		}else{
			$expiration =  date('d M Y H:i', strtotime("+{$avail_sku_promo->duration} seconds"));
			// $expiration2 =  date('d M Y H:i', strtotime("+300 seconds"));
			
			$connection->username = $avail_sku_promo->code.'/'.$mobile;
			$connection->password = $mobile;

			if ($connection->validate() && $connection->save()) {

		// 		echo "<pre>";
		// print_r($connection);
		// die();
				// Verify connection
				if ($this->owner->connect_paid($availment, TRUE, $expiration)) {
					return TRUE;
				}
				return FALSE;
			}
		}
		
		
		throw new CHttpException(500, Yii::t('yii','Cannot update connection. Please try again.'));
	}
	
	/**
	 * Send SMS containing the 5-digit code
	 * 
	 * @access private
	 * @param int $msisdn
	 * @param string $message
	 * @return boolean
	 */
	private function _sendcode($msisdn, $message)
	{
		Sms::send($msisdn, $message);
		return true;
	}

	public function _resetLimit($mobile){

		$mobile->auth_limit = 0;
		$mobile->auth_code = '';
		

		if ($mobile->validate() && $mobile->save()) {
			$url = $this->owner->createAbsoluteUrl('login/',
				array(	'connection_token'=>$this->owner->connection->connection_token,
						'availment_id'=>$this->self->availment_id,
						'mobile'=>$mobile->msisdn,
						'action'=>'login'));
			
			$json_array = array('status'=>1, 'message'=>'Redirecting to mobile input page', 'redirect'=>$url);
			die(CJSON::encode($json_array));
		}
	}

	/**
	 * Purchase SKU
	 * 
	 * @access private
	 */
	private function _purchase()
	{

		$mobile_no = Yii::app()->request->getParam('mobile');
		$sku = AvailSku::model()->with('dev_avail')->find("msisdn = {$mobile_no} AND device_id = {$this->owner->device->device_id} AND DATE(dev_avail.created_at) = DATE(NOW())");
		$promos = Promo::model()->findAllByAttributes(array('account_type_id'=>$sku->account_type_id));

		//check if free or sku by looking in the avail_sku_promo table
		$availed_promo = AvailSkuPromo::model()->findByAttributes(array('avail_sku_id'=>$sku->avail_sku_id), array('order' => 'created_at desc','limit' => 1));
		//check if availed is free
		$free = Availment::model()->findByAttributes(array('model'=>'AvailFree'));
		$availed_free = DeviceAvailment::model()->find("device_id = {$this->owner->device->device_id} AND  availment_id = {$free->availment_id} AND  DATE(created_at) = DATE(NOW())");
		if($availed_promo){
			$diff = (strtotime(date('Y-m-d H:i:s')) - strtotime($availed_promo->created_at));

			if($diff < $availed_promo->duration){

				$this->self_portal->duration = $availed_promo->duration - $diff;
				$minutes = floor($this->self_portal->duration / 60);
				
				if (self::_updateConnection($sku, $this->self_portal, $availed_promo, false, $mobile_no)){
					$message = "You still have {$minutes} minutes remaining from your previous purchase! Happy Surfing.";
					$url = $this->owner->createAbsoluteUrl('access/', array('connection_token'=>$this->owner->connection->connection_token,
						'availment_id'=>$this->self->availment_id));
					$this->render('remaining_landing', array('message'=>$message, 'url'=>$url));
					return true;
				}else{
					$json_array = array('status'=>0, 'message'=>'Something went wrong');
					die(CJSON::encode($json_array));
				}

			}

		}
		// echo "<pre>";
		// print_r($availed_free);
		// die();
		if (empty($availed_free)){
			self::_assignDuration($sku);
			//no free yet so give free internet
			$dev_avail = $this->owner->saveDeviceAvailment($free->availment_id);
			$this->self_portal->availment_id = $free->availment_id;
			// Check if validation paseses
			if ($dev_avail->validate()) {
				// Update connection and save to DB
				if (self::_updateConnection($sku, $this->self_portal, false, true) && $dev_avail->save()) {
					$minutes = floor($this->self_portal->duration / 60);
					$message = "You can use Globe Wifi, FREE trial for {$minutes} minutes today! Happy Surfing.";
					$url = $this->owner->createAbsoluteUrl('access/', array('connection_token'=>$this->owner->connection->connection_token,
						'availment_id'=>$free->availment_id));
					$this->render('remaining_landing', array('message'=>$message, 'url'=>$url));
					return true;
				}
			}
		}

		if (Yii::app()->request->isPostRequest) {
			$post = Yii::app()->request->getPost('promo');
			self::_purchaseSKU($post);
		}
		
		$this->render('purchase', array('availment_id'=>$this->self->availment_id, 'object'=>$sku, 'promos'=>$promos));
	
	
	}

	/**
	 * Process Purchase SKU
	 * 
	 * @access private
	 */
	private function _purchaseSKU($promo)
	{
		$mobile_no = Yii::app()->request->getParam('mobile');
		$avail_sku = AvailSku::model()->find("msisdn = {$mobile_no}");
		$promo_details = Promo::model()->find("promo_id = {$promo}");
		$avail_sku_promo = new AvailSkuPromo;
		$dev_avail = $this->owner->saveDeviceAvailment($this->self->availment_id);

		$avail_sku_promo->avail_sku_id = $avail_sku->avail_sku_id;
		$avail_sku_promo->promo_id = $promo_details->promo_id;
		$avail_sku_promo->promo_type_id = $promo_details->promo_type_id;
		$avail_sku_promo->account_type_id = $promo_details->account_type_id;
		$avail_sku_promo->subscription_type_id = $promo_details->subscription_type_id;
		$avail_sku_promo->service_type_id = $promo_details->service_type_id;
		$avail_sku_promo->currency_id = $promo_details->currency_id;
		$avail_sku_promo->code = $promo_details->code;
		$avail_sku_promo->valid_from = $promo_details->valid_from;
		$avail_sku_promo->valid_until = $promo_details->valid_until;
		$avail_sku_promo->download_speed = $promo_details->download_speed;
		$avail_sku_promo->upload_speed = $promo_details->upload_speed;
		$avail_sku_promo->amount = $promo_details->amount;
		$avail_sku_promo->duration = $promo_details->duration;
		$avail_sku_promo->created_at = date('Y-m-d H:i:s'); 
		$this->self_portal->duration = $promo_details->duration;

		if($avail_sku_promo->validate()){
			// Update connection and save to DB
			if (self::_updateConnection($avail_sku, $this->self_portal, $avail_sku_promo, false, $mobile_no) && $dev_avail->save()) {
				$avail_sku->device_availment_id = $dev_avail->device_availment_id;
				$avail_sku->save();
				$avail_sku_promo->save();
				$minutes = $this->self_portal->duration / 60;
				$message = "You can use Globe Wifi, for {$minutes} minutes! Happy Surfing.";
				$url = $this->owner->createAbsoluteUrl('access/', array('connection_token'=>$this->owner->connection->connection_token,
					'availment_id'=>$this->self->availment_id));
				$json_array = array('status'=>1, 'message'=>$message, 'redirect'=>$url);
				die(CJSON::encode($json_array));
			}
		}

		$json_array = array('status'=>0, 'message'=>'Something went wrong');
		die(CJSON::encode($json_array));

			// echo "<pre>";
			// print_r($promo);
			// die();


		// $mobile_no = Yii::app()->request->getParam('mobile');
		// $sku = AvailSku::model()->with('dev_avail')->find("msisdn = {$mobile_no} AND device_id = {$this->owner->device->device_id} AND DATE(dev_avail.created_at) = DATE(NOW())");
		
		// $promos = Promo::model()->findAllByAttributes(array('account_type_id'=>$sku->account_type_id));

		// $this->render('purchase', array('availment_id'=>$this->self->availment_id, 'object'=>$sku, 'promos'=>$promos));
	}
	
	private function _assignDuration(AvailSku $sku)
	{
		// Alter duration
		$globe_duration = AvailmentSetting::getValue($this->self->availment_id, 'GLOBE_DURATION');
		$xglobe_duration = AvailmentSetting::getValue($this->self->availment_id, 'XGLOBE_DURATION');
		if ($globe_duration || $xglobe_duration) {
			$msisdn = substr($sku->msisdn, 1);
			$prefix = Yii::app()->db->createCommand()->select('*')
				->from('Unifi_Captive.plan_prefix')->where("LEFT(prefix, 3) = LEFT({$msisdn}, 3)")
				->queryRow();

			if ($prefix) {
				if ($prefix['plan_id'] == 1 OR $prefix['plan_id'] == 2 OR $prefix['plan_id'] == 3 OR $prefix['plan_id'] == 9) {
					if ($globe_duration) {
						$this->self_portal->duration = $globe_duration * 60;
					}
				}
				else {
					if ($xglobe_duration) {
						$this->self_portal->duration = $xglobe_duration * 60;
					}
				}
			}
		}
	}
}