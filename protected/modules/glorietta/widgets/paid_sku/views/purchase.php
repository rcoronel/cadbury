
<div class="row">
    <div class="col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
		<div class="default-wrapper">
			<div class="default-content">
				  <p><span class="strong">To continue enjoying this service,</span> kindly ensure you have sufficient load on your prepaid wallet to subscribe to Globe Home Wi-Fi promos.</p>
			</div>

			<div id="cadbury-paid-single-content-body" class="col-md-10 col-md-offset-1">
			<!--Error Message -->
			<div class="error-notification animated bounceIn" role="alert">
				<h5>Insufficient balance</h5>
			</div>
			<!--Error Message -->
			<div class="cadbury-paid-heading-static">
				<h5>Select GoWiFi Promo you want to purchase</h5>
			</div>

			<?php $form=$this->beginWidget('CActiveForm', array('method'=>'post', 'id'=>'choosepromo-form')); ?>

				<?php if (! empty($promos)):?>
				<ul class="cadbury-panel-body-static">
					<?php foreach ($promos as $p):?>
					<li class="cadbury-radio">
						<div>
							<label for="unli-wifi-radio1"><?php echo $p->description;?></label>
							<!-- <div class="radio-box checked" style="position: relative;"> -->
								<!-- <input type="radio" name="unli-wifi-radio" id="unli-wifi-radio1" checked="" style="position: absolute; opacity: 0;"> -->
								<?php echo CHtml::radioButton('promo', FALSE, array('value'=>$p->promo_id));?>
								
								<!-- <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; cursor: pointer; background: rgb(255, 255, 255);"></ins></div> -->
						<!-- </div> -->
					</li>
					<?php endforeach;?>          
				</ul>
				<?php endif;?>
				<?php echo CHtml::hiddenField('mobile_number', $object->msisdn);?>
				<div class="inline-btn-group">
				<div class="row">
					<div class="col-sm-6">
						 <input type="submit" class="btn inline-primary-btn btn-block" data-toggle="modal" data-target="#purchaseConfirmationModal" value="purchase">
					</div>
					<div class="col-sm-6">
						<input type="submit" class="btn inline-secondary-btn btn-block" value="cancel">
					</div>
				</div>
			</div> <!-- end of inline-btn-group -->

			<?php $this->endWidget();?>
<!-- 
			<div class="inline-btn-group">
				<div class="row">
					<div class="col-sm-6">
						 <input type="submit" class="btn inline-primary-btn btn-block" data-toggle="modal" data-target="#purchaseConfirmationModal" value="purchase">
					</div>
					<div class="col-sm-6">
						<input type="submit" class="btn inline-secondary-btn btn-block" value="cancel">
					</div>
				</div>
			</div> <!-- end of inline-btn-group -->

			</div>


			<footer>
			   <?php echo CHtml::image(Link::image_url('powered-by-globe-modal.png'), 'Globe');?>
			</footer>
		</div> 
	</div>
</div>

<div class="modal modal-wifi in" id="loader-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-body small-modal-body-style">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
	    $('input').iCheck({
	        checkboxClass: 'check-box',
	        radioClass: 'radio-box',
	        cursor: true
        });
	});
	$('form').on('submit', function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		// hide errors
		$('div#form-error').slideUp( 300 ).html();

		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");

		$.ajax( {
			url : formURL,
			type: "POST",
			data : postData,
			dataType: 'json',
			success:function(data, textStatus, jqXHR)
			{
				if ( ! data.status) {
					setTimeout(function() {
						$('#loader-modal').modal('hide');
						$('div#form-error').slideDown(300).delay(800).html(data.message);
						$('div#form-error').show();
					}, 900);
				}
				else {
					$('div.modal-body p').html(data.message);
					$('#loader-modal').modal({backdrop: 'static', keyboard: false});
					setTimeout(function(){
						window.location.href = data.redirect;
					}, 3000);
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				//if fails     
			}
		});
		e.preventDefault();
	});
</script>