<?php

class Portal_groupController extends BackofficeController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action))
		{
			// action buttons
			$this->actionButtons['index'] = array('add'=>array('link'=>$this->createURL($this->id.'/add')));
			$this->actionButtons['view'] = array('back'=>array('link'=>$this->createURL($this->id.'/')));
			$this->actionButtons['add'] = array('back'=>array('link'=>$this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back'=>array('link'=>$this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		// fields to be displayed
		$row_fields = array();
		$row_fields['portal_group_id']['type'] = 'text';
		$row_fields['portal_group_id']['class'] = 'text-center col-md-1';
		
		$row_fields['name']['type'] = 'text';
		$row_fields['name']['class'] = 'col-mid-4';
		
		$row_actions = array('view', 'edit', 'delete');
		
		$object = new PortalGroup();
		$list = self::_showList($object, $row_fields, $row_actions);
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$list, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
	}
	
	/**
	 * display list
	 * 
	 * @param object $object PortalGroup instance
	 * @param array $row_fields
	 * @param array $row_actions
	 * @return mixed
	 */
	private function _showList(PortalGroup $object, $row_fields, $row_actions)
	{
		$object->attributes = PortalGroup::setSessionAttributes(Yii::app()->request->getPost('PortalGroup'));
		$criteria = $object->search()->criteria;
		$count = PortalGroup::model()->count($criteria);
		
		$pages = new CPagination($count);
		$criteria->limit = 25;
		$criteria->offset = (Yii::app()->request->getQuery('page', 1) * $criteria->limit) - $criteria->limit;
		
		$pages->pageSize=$criteria->limit;
		$pages->applyLimit($criteria);
		
		$list = PortalGroup::model()->findAll($criteria);
		$view = $this->renderPartial('/layouts/list-body', array('object'=>$object, 'objectList'=>$list, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions), TRUE);
		if (Yii::app()->request->isAjaxRequest) {
			die(CJSON::encode(array('status'=>1, 'message'=>$view)));
		}
		return $view;
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		$group = new PortalGroup;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($group);
		}
		
		self::_renderForm($group);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id PortalGroup primary key
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$object = PortalGroup::model()->findByPk($id);
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($object);
		}
		
		self::_renderForm($object);
	}
	
	/**
	 * Delete record
	 * 
	 * @access public
	 * @param int $id PortalGroup ID
	 * @return void
	 */
	public function actionDelete($id)
	{
		$group = PortalGroup::model()->findByPk($id);
		PortalGroup::validateObject($group, 'PortalGroup');
		
		if (Yii::app()->request->isPostRequest) {
			// check for security token
			if (CPasswordHelper::verifyPassword($group->admin_menu_id, Yii::app()->request->getPost('security_key'))) {
				// Delete permissions
				AdminRolePermission::model()->deleteAllByAttributes(array('admin_menu_id'=>$group->admin_menu_id));
				
				// Delete record
				PortalGroup::model()->deleteByPk($group->admin_menu_id);
				
				Yii::app()->getModule('backoffice')->admin->setFlash('success', 'Delete successful!');
				$this->redirect(array("$this->id/"));
			}
			else {
				Yii::app()->getModule('backoffice')->admin->setFlash('fail', 'Error in deletion.');
				$this->redirect(array("$this->id/"));
			}
		}
		$this->render('delete', array('menu'=>$group));
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object PortalGroup instance
	 * @return void
	 */
	private function _renderForm(PortalGroup $object)
	{
		$portals = Portal::model()->findAllByAttributes(array('is_active'=>1), array('order'=>'name ASC'));
		$groups = PortalGrouping::model()->findAllByAttributes(array('portal_group_id'=>$object->portal_group_id));
		foreach ($portals as $p) {
			foreach ($groups as $g) {
				if ($p->portal_id == $g->portal_id) {
					$p->checked = TRUE;
				}
			}
		}
		
		$this->render('form', array('object'=>$object, 'portals'=>$portals));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object PortalGroup instance
	 * @return	void
	 */
	private function _postValidate(PortalGroup $object)
	{
		$post = Yii::app()->request->getPost('PortalGroup');
		$object->attributes = $post;

		if ($object->validate() && $object->save()) {
			PortalGrouping::model()->deleteAllByAttributes(array('portal_group_id'=>$object->portal_group_id));
			if (isset($post['portals'])) {
				foreach ($post['portals'] as $portal_id) {
					$group = new PortalGrouping;
					$group->portal_group_id = $object->portal_group_id;
					$group->portal_id = $portal_id;
					$group->save();
				}
			}
			self::displayFormSuccess();
		}
		else {
			self::displayFormError($object);
		}
	}
}