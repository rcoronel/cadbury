<?php

class AppearanceController extends BackofficeController
{
	/**
	 * View website appearance settings
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		if (Yii::app()->request->isPostRequest) {
			self::_handlePost();
		}
		
		$data = array(	'site_title' => Configuration::getValue('SITE_TITLE'),
						'logo' => Configuration::getvalue('LOGO'),
						'favicon' => Configuration::getvalue('FAVICON'));
		
		$this->render('form', $data);
	}
	
	/**
	 * Set configuration value
	 * 
	 * @access private
	 * @return void
	 */
	private function _handlePost()
	{
		$post = Yii::app()->request->getPost('Configuration');
		
		foreach ($post as $config => $value) {
			Configuration::setValue($config, $value);
		}
		
		if ( ! empty($_FILES)) {
			self::_upload($_FILES);
		}
		
		$this->log();
		Yii::app()->getModule('backoffice')->admin->setFlash('success', 'Settings applied');
		$this->redirect(array($this->id.'/'));
	}
	
	/**
	 * Upload images based on configuration
	 * 
	 * @access	private
	 * @return	void
	 */
	private function _upload($files)
	{
		$upload_path = dirname(Yii::app()->request->scriptFile).'/images/';

		foreach ($files as $name => $attr) {
			if ($attr['size'] && ! $attr['error']) {
				$image = CUploadedFile::getInstanceByName($name);
				
				// delete current file
				$previous_file = Configuration::getValue($name);
				@unlink($upload_path.$previous_file);
				
				switch ($name) {
					case 'LOGO':
						$filename = 'logo.'.$image->extensionName;
					break;
				
					case 'FAVICON':
						$filename = 'favicon.'.$image->extensionName;
					break;
				}
				
				// check for successful save
				if ($image->saveAs($upload_path.$filename)) {
					Configuration::setvalue($name, $filename);
				}	
			}
		}
	}
}