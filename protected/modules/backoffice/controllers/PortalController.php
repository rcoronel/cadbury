<?php

class PortalController extends BackofficeController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action)) {
			// action buttons
			$this->actionButtons['index'] = array('add'=>array('link'=>$this->createURL($this->id.'/add')));
			$this->actionButtons['add'] = array('back'=>array('link'=>$this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back'=>array('link'=>$this->createURL($this->id.'/')));
			$this->actionButtons['view'] = array('back'=>array('link'=>$this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		$array = array_replace(array('OMG', 'AGAIN'), array('SEE', 'HERE'));
		
		// fields to be displayed
		$row_fields = array();
		$row_fields['portal_id']['type'] = 'text';
		$row_fields['portal_id']['class'] = 'text-center col-md-1';
		
		$row_fields['name']['type'] = 'text';
		$row_fields['name']['class'] = 'col-md-3';
		
		$row_fields['code']['type'] = 'text';
		$row_fields['code']['class'] = 'col-md-2';
		
		$sites = Site::model()->findAll(array('order'=>'name ASC'));
		$row_fields['site_id']['type'] = 'select';
		$row_fields['site_id']['class'] = 'col-md-2';
		$row_fields['site_id']['parent'] = 'site';
		$row_fields['site_id']['child'] = 'name';
		$row_fields['site_id']['value'] = CHtml::listData($sites, 'site_id', 'name');
		
		$row_fields['is_active']['type'] = 'select';
		$row_fields['is_active']['class'] = 'text-center col-md-1';
		$row_fields['is_active']['value'] = array('1'=>'Enabled', '0' =>'Disabled');
		$row_fields['is_active']['mode'] = 'bool';
		
		$row_actions = array('view', 'edit', 'delete');
		$object = new Portal();
		$list = self::_showList($object, array('fields'=>$row_fields, 'actions'=>$row_actions));
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$list['view'], 'pagination'=>$list['pagination'], 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
	}
	
	
	/**
	 * View portal settings
	 * 
	 * @access public
	 * @param int $id Portal ID
	 * @return void
	 */
	public function actionView($id)
	{
		$this->redirect(array('portal_setting/', 'id'=>$id));
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		$portal = new Portal;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($portal);
		}
		
		self::_renderForm($portal);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id Portal primary key
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$name = Portal::model()->findByPk($id);
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($name);
		}
		
		self::_renderForm($name);
	}
	
	/**
	 * Delete record
	 * 
	 * @access public
	 * @param int $id Portal ID
	 * @return void
	 */
	public function actionDelete($id)
	{
		$portal = Portal::model()->findByPk($id);
		Portal::validateObject($portal, 'Portal');
		
		$this->context->portal = $portal;
		
		if (Yii::app()->request->isPostRequest) {
			// check for security token
			if (CPasswordHelper::verifyPassword($portal->portal_id, Yii::app()->request->getPost('security_key'))) {
				// Delete stylesheets
				@unlink(dirname(Yii::app()->request->scriptFile).'/css/portal/'.$portal->portal_id.'/');
				
				// Delete images
				@unlink(dirname(Yii::app()->request->scriptFile).'/images/portal/'.$portal->portal_id.'/');
				
				// Delete availments
				PortalAvailment::model()->deleteAllByAttributes(array('portal_id'=>$portal->portal_id));
				AvailmentSetting::model()->deleteAllByAttributes(array('portal_id'=>$portal->portal_id));
				
				// Delete configuration details
				PortalConfiguration::model()->deleteAllByAttributes(array('portal_id'=>$portal->portal_id));
				
				// Delete SSID Group
				PortalSsidGroup::model()->deleteAllByAttributes(array('portal_id'=>$portal->portal_id));
				
				// Delete scenarios
				$scenarios = Scenario::model()->findAllByAttributes(array('portal_id'=>$portal->portal_id));
				if ( ! empty($scenarios)) {
					foreach ($scenarios as $s) {
						ScenarioAvailment::model()->deleteAllByAttributes(array('scenario_id'=>$s->scenario_id));
						ScenarioReturning::model()->deleteAllByAttributes(array('scenario_id'=>$s->scenario_id));
						Scenario::model()->deleteAllByAttributes(array('scenario_id'=>$s->scenario_id));
					}
				}
				
				// Delete surveys
				$surveys = Survey::model()->findAllByAttributes(array('portal_id'=>$portal->portal_id));
				if ( ! empty($surveys)) {
					foreach ($surveys as $s) {
						$questions = SurveyQuestion::model()->findAllByAttributes(array('survey_id'=>$s->survey_id));
						if ( ! empty($questions)) {
							foreach ($questions as $q) {
								SurveyAnswer::model()->deleteAllByAttributes(array('survey_question_id'=>$q->survey_question_id));
								SurveyQuestion::model()->deleteAllByAttributes(array('survey_question_id'=>$q->survey_question_id));
							}
						}
						Survey::model()->deleteAllByAttributes(array('survey_id'=>$s->survey_id));
					}
				}
				
				// Delete AP Group and AP names
				$ap_groups = AccessPointGroup::model()->findAllByAttributes(array('portal_id'=>$portal->portal_id));
				if ( ! empty($ap_groups)) {
					foreach ($ap_groups as $apg) {
						AccessPoint::model()->deleteAllByAttributes(array('access_point_group_id'=>$apg->access_point_group_id));
						AccessPointGroup::model()->deleteAllByAttributes(array('access_point_group_id'=>$apg->access_point_group_id));
					}
				}
				
				// Delete record
				Portal::model()->deleteByPk($portal->portal_id);
				
				// Log
				$this->log($portal->portal_id);
				
				Yii::app()->user->setFlash('success', 'Delete successful!');
				$this->redirect(array("$this->id/"));
			}
			else {
				Yii::app()->user->setFlash('fail', 'Error in deletion.');
				$this->redirect(array("$this->id/"));
			}
		}
		$this->render('delete', array('portal'=>$portal));
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object Portal instance
	 * @return void
	 */
	private function _renderForm(Portal $object)
	{
		$fields = array();
		
		$sites = Site::model()->findAll(array('order'=>'name ASC'));
		$fields['site_id']['type'] = 'select';
		$fields['site_id']['class'] = 'col-md-5';
		$fields['site_id']['value'] = CHtml::listData($sites, 'site_id', 'name');
		
		$fields['code']['type'] = 'text';
		$fields['code']['class'] = 'col-md-5';
		
		$fields['name']['type'] = 'text';
		$fields['name']['class'] = 'col-md-5';
		
		$fields['description']['type'] = 'text';
		$fields['description']['class'] = 'col-md-5';
		
		$fields['is_active']['type'] = 'radio';
		$fields['is_active']['class'] = 'col-lg-5';
		$fields['is_active']['value'] = array('1'=>'Enabled', '0'=>'Disabled');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object'=>$object, 'fields'=>$fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object Portal instance
	 * @return	void
	 */
	private function _postValidate(Portal $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('Portal');
		$object->attributes = $post;

		if ($object->validate() && $object->save()) {
			$this->log($object->{$object->tableSchema->primaryKey});
			self::displayFormSuccess();
		}
		self::displayFormError($object);
	}
}