<?php

class SiteController extends BackofficeController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action)) {
			// action buttons
			$this->actionButtons['index'] = array('add'=>array('link'=>$this->createURL($this->id.'/add')));
			$this->actionButtons['add'] = array('back'=>array('link'=>$this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back'=>array('link'=>$this->createURL($this->id.'/')));
			$this->actionButtons['view'] = array('back'=>array('link'=>$this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		// fields to be displayed
		$row_fields = array();
		
		$row_fields['name']['type'] = 'text';
		$row_fields['name']['class'] = 'col-md-4';
		
		$row_fields['description']['type'] = 'text';
		$row_fields['description']['class'] = 'col-md-4';
		
		$row_fields['is_active']['type'] = 'select';
		$row_fields['is_active']['class'] = 'text-center col-md-1';
		$row_fields['is_active']['value'] = array('1'=>'Enabled', '0' =>'Disabled');
		$row_fields['is_active']['mode'] = 'bool';
		
		$row_actions = array('edit', 'delete');
		$object = new Site();
		$list = self::_showList($object, array('fields'=>$row_fields, 'actions'=>$row_actions));
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$list['view'], 'pagination'=>$list['pagination'], 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		$site = new Site;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($site);
		}
		
		self::_renderForm($site);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id Site primary key
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$site = Site::model()->findByPk($id);
		Site::validateObject($site, 'Site', $this->createUrl('site/'));
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($site);
		}
		
		self::_renderForm($site);
	}
	
	/**
	 * Delete record
	 * 
	 * @access public
	 * @param int $id Portal ID
	 * @return void
	 */
	public function actionDelete($id)
	{
		$site = Site::model()->findByPk($id);
		Site::validateObject($site, 'Site');
		
		if (Yii::app()->request->isPostRequest) {
			// Count portals that has this site
			$count = Portal::model()->count("site_id={$site->site_id}");
			if ($count) {
				Yii::app()->user->setFlash('fail', 'Cannot delete site. There is are portals assigned to this site.');
				$this->redirect(array("$this->id/"));
			}
			else {
				// check for security token
				if (CPasswordHelper::verifyPassword($site->site_id, Yii::app()->request->getPost('security_key'))) {
					// Delete record
					Site::model()->deleteByPk($site->site_id);

					// Log
					$this->log($site->site_id);

					Yii::app()->user->setFlash('success', 'Delete successful!');
				}
				else {
					Yii::app()->user->setFlash('fail', 'Error in deletion.');
				}
				$this->redirect(array("$this->id/"));
			}
		}
		$this->render('delete', array('site'=>$site));
	}

	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object Site instance
	 * @return void
	 */
	private function _renderForm(Site $object)
	{
		$fields = array();
		
		$fields['name']['type'] = 'text';
		$fields['name']['class'] = 'col-md-5';
		
		$fields['description']['type'] = 'text';
		$fields['description']['class'] = 'col-md-5';
		
		$fields['is_active']['type'] = 'radio';
		$fields['is_active']['class'] = 'col-lg-5';
		$fields['is_active']['value'] = array('1'=>'Enabled', '0'=>'Disabled');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object'=>$object, 'fields'=>$fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object Site instance
	 * @return	void
	 */
	private function _postValidate(Site $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('Site');
		$object->attributes = $post;

		if ($object->validate() && $object->save()) {
			$this->log($object->{$object->tableSchema->primaryKey});
			self::displayFormSuccess();
		}
		self::displayFormError($object);
	}
}