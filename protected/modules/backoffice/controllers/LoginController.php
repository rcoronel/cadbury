<?php

class LoginController extends BackofficeController
{
	// layout
	public $layout = '/layouts/login';
	
	/**
	 * Show the login page
	 * 
	 * @access public
	 * @return void
	 */
	public function actionIndex()
	{
		if (Yii::app()->request->isPostRequest) {
			self::_validateLogin();
		}
		
		$admin = new Admin();
		$this->render('index', array('admin'=>$admin));
	}
	
	/**
	 * Login the Admin
	 * 
	 * @access private
	 * @return JSON
	 */
	private function _validateLogin()
	{
		$post = Yii::app()->request->getPost('Admin');
		
		if ($post) {
			sleep(1);
			
			$admin = new Admin;
			$admin->email = $post['email'];
			$admin->password = $post['password'];
			$admin->scenario = 'login';
			
			if ($admin->validate() AND $admin->login()) {
				$redirect = $this->createAbsoluteURL('dashboard/');
				self::_redirect($redirect);
			}
			else {
				self::displayFormError($admin);
			}
		}
	}
	
	/**
	 * Handle page redirection if admin is authenticated
	 * 
	 * @access private
	 * @param string $redirect Default redirect page
	 * @return JSON
	 */
	private function _redirect($redirect)
	{
		// log
		$this->log();
		
		if (Yii::app()->getModule('backoffice')->admin->getState('redirect')) {
			$redirect = Yii::app()->getModule('backoffice')->admin->getState('redirect');
		}

		// check if XMLHttpRequest request
		if (Yii::app()->request->getPost('ajax') OR Yii::app()->request->isAjaxRequest) {
			$json_array = array('status' => 1, 'message' => 'Logging in', 'redirect' => $redirect);
			die(CJSON::encode($json_array));
		}
	}
}