<?php

class SsidController extends BackofficeController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action))
		{
			// action buttons
			$this->actionButtons['index'] = array('add'=>array('link'=>$this->createURL($this->id.'/add')));
			$this->actionButtons['add'] = array('back'=>array('link'=>$this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back'=>array('link'=>$this->createURL($this->id.'/')));
			$this->actionButtons['view'] = array('back'=>array('link'=>$this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		// fields to be displayed
		$row_fields = array();
		$row_fields['service_set_id']['type'] = 'text';
		$row_fields['service_set_id']['class'] = 'text-center col-md-1';
		
		$row_fields['ssid']['type'] = 'text';
		$row_fields['ssid']['class'] = 'col-mid-2';
		
		$row_fields['is_active']['type'] = 'select';
		$row_fields['is_active']['class'] = 'text-center col-md-1';
		$row_fields['is_active']['value'] = array('1'=>'Enabled', '0' =>'Disabled');
		$row_fields['is_active']['mode'] = 'bool';
		
		$row_actions = array('edit', 'delete');
		$object = new ServiceSet();
		$list = self::_showList($object, array('fields'=>$row_fields, 'actions'=>$row_actions), 100, TRUE);
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$list['view'], 'pagination'=>$list['pagination'], 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
	}
	
	public function actionView($id)
	{
		$this->redirect(array('portal_setting/', 'id'=>$id));
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		$name = new ServiceSet;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($name);
		}
		
		self::_renderForm($name);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id ServiceSet primary key
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$name = ServiceSet::model()->findByPk($id);
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($name);
		}
		
		self::_renderForm($name);
	}
	
	/**
	 * Delete record
	 * 
	 * @access public
	 * @param int $id SSID ID
	 * @return void
	 */
	public function actionDelete($id)
	{
		$ssid = ServiceSet::model()->findByPk($id);
		ServiceSet::validateObject($ssid, 'ServiceSet');
		
		if (Yii::app()->request->isPostRequest) {
			// check for security token
			if (CPasswordHelper::verifyPassword($ssid->service_set_id, Yii::app()->request->getPost('security_key'))) {
				// Delete grouping
				PortalSsidGroup::model()->deleteAllByAttributes(array('service_set_id'=>$ssid->service_set_id));
				
				// Delete record
				ServiceSet::model()->deleteByPk($ssid->service_set_id);
				
				// Log
				$this->log($ssid->service_set_id);
				
				Yii::app()->user->setFlash('success', 'Delete successful!');
				$this->redirect(array("$this->id/"));
			}
			else {
				Yii::app()->user->setFlash('fail', 'Error in deletion.');
				$this->redirect(array("$this->id/"));
			}
		}
		$this->render('delete', array('ssid'=>$ssid));
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object ServiceSet instance
	 * @return void
	 */
	private function _renderForm(ServiceSet $object)
	{
		$fields = array();
		
		$fields['ssid']['type'] = 'text';
		$fields['ssid']['class'] = 'col-md-5';
		
		$fields['is_active']['type'] = 'radio';
		$fields['is_active']['class'] = 'col-lg-5';
		$fields['is_active']['value'] = array('1'=>'Enabled', '0'=>'Disabled');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object'=>$object, 'fields'=>$fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object ServiceSet instance
	 * @return	void
	 */
	private function _postValidate(ServiceSet $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('ServiceSet');
		$object->attributes = $post;

		if ($object->validate() && $object->save()) {
			$this->log($object->{$object->tableSchema->primaryKey});
			self::displayFormSuccess();
		}
		self::displayFormError($object);
	}
}