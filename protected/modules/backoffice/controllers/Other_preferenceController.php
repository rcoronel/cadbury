<?php

class Other_preferenceController extends BackofficeController
{
	public function actionIndex()
	{
		if (Yii::app()->request->isPostRequest) {
			self::_handlePost();
		}
		
		$this->render('index');
	}
	
	/**
	 * Set configuration value
	 * 
	 * @access private
	 * @return void
	 */
	private function _handlePost()
	{
		$post = Yii::app()->request->getPost('Configuration');
		
		foreach ($post as $config => $value) {
			Configuration::setValue($config, $value);
		}
		
		if ( ! empty($_FILES)) {
			self::_upload($_FILES);
		}
		
		$this->log();
		Yii::app()->user->setFlash('success', 'Settings applied');
		$this->redirect(array($this->id.'/'));
	}
}