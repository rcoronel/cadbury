<?php

class Tattoo_subsController extends BackofficeController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action)) {
			// action buttons
//			$this->actionButtons['index'] = array('add' => array('link' => $this->createURL($this->id.'/add')));
//			$this->actionButtons['add'] = array('back' => array('link' => $this->createURL($this->id.'/')));
//			$this->actionButtons['update'] = array('back' => array('link' => $this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		if (Yii::app()->request->isPostRequest) {

			if ( ! empty($_FILES)) {
				$type = Yii::app()->request->getPost('plan');
				self::_updateList($_FILES, $type);
			}
		}
		
		$this->render('form');
	}

	private function _updateList($file, $type)
	{
		if( isset($file['subslist']) ) {

	      $handle = fopen($file['subslist']['tmp_name'], 'r');

		    if ($handle) {
		        while(!feof($handle)) {
		           $record = fgetcsv($handle);
				   
		           $sub = new TattooSubscriber;
		           $sub->Plan = $type;
		           $sub->Account_no = $record[0];
		           $sub->Network_service_ID = $record[1];
		           $sub->Name = $record[2];
				   $sub->Service_type = $record[3];
				   $sub->Description = $record[4];
				   $sub->validate() && $sub->save();
		   //         // Validate object
					// if ($sub->validate() && $sub->save()){
					// 	// $this->log($sub->{$sub->tableSchema->primaryKey});
					// 	self::displayFormSuccess();
					// }else{
					// 	self::displayFormError($sub);
					// }
				}

		      fclose($handle);
		    }
		}
		
	}
}