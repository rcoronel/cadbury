<?php

class Site_menuController extends BackofficeController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action))
		{
			// action buttons
			$this->actionButtons['index'] = array('add' => array('link' => $this->createURL($this->id.'/add')));
			$this->actionButtons['view'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['add'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back' => array('link' => $this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		// declare scripts
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'plugins', 'jquery.tablednd.js')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/jquery.toastr.min.js'));
		
		// fields to be displayed
		$row_fields = array();
		$row_fields['site_menu_id']['type'] = 'text';
		$row_fields['site_menu_id']['class'] = 'text-center col-md-1';
		
		$row_fields['title']['type'] = 'text';
		$row_fields['title']['class'] = 'col-mid-4';
		
		$row_fields['display']['type'] = 'select';
		$row_fields['display']['class'] = 'text-center col-md-1';
		$row_fields['display']['value'] = array('1' => 'Enabled', '0' => 'Disabled');
		$row_fields['display']['mode'] = 'bool';
		
		$row_fields['position']['type'] = 'text';
		$row_fields['position']['class'] = 'text-center col-md-1 dragHandle';
		
		$row_actions = array('view', 'edit', 'delete');
		
		$object = new SiteMenu();
		$list = self::_showList($object, $row_fields, $row_actions);
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$list, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
	}
	
	/**
	 * display list
	 * 
	 * @param object $object SiteMenu instance
	 * @param array $row_fields
	 * @param array $row_actions
	 * @return mixed
	 */
	private function _showList(SiteMenu $object, $row_fields, $row_actions)
	{
		$object->attributes = SiteMenu::setSessionAttributes(Yii::app()->request->getPost('SiteMenu'));
		$criteria = $object->search()->criteria;
		$criteria->compare('parent_site_menu_id', 0);
		$criteria->order = 'position ASC';
		$count = SiteMenu::model()->count($criteria);
		
		$pages = new CPagination($count);
		$criteria->limit = 25;
		$criteria->offset = (Yii::app()->request->getQuery('page', 1) * $criteria->limit) - $criteria->limit;
		
		$pages->pageSize=$criteria->limit;
		$pages->applyLimit($criteria);
		
		$list = SiteMenu::model()->findAll($criteria);
		$view = $this->renderPartial('/layouts/list-body', array('object'=>$object, 'objectList'=>$list, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions), TRUE);
		if (Yii::app()->request->isAjaxRequest) {
			die(CJSON::encode(array('status'=>1, 'message'=>$view)));
		}
		return $view;
	}
	
	/**
	 * View records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionView($id)
	{
		// declare scripts
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'plugins', 'jquery.tablednd.js')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/jquery.toastr.min.js'));
		
		// fields to be displayed
		$row_fields = array();
		$row_fields['site_menu_id']['type'] = 'text';
		$row_fields['site_menu_id']['class'] = 'text-center col-md-1';
		
		$row_fields['title']['type'] = 'text';
		$row_fields['title']['class'] = 'col-mid-4';
		
		$row_fields['display']['type'] = 'select';
		$row_fields['display']['class'] = 'text-center col-md-1';
		$row_fields['display']['value'] = array('1' => 'Enabled', '0' => 'Disabled');
		$row_fields['display']['mode'] = 'bool';
		
		$row_fields['position']['type'] = 'text';
		$row_fields['position']['class'] = 'text-center col-md-1 dragHandle';
		
		$row_actions = array('edit', 'delete');
		$object = new SiteMenu();
		$list = self::_showView($object, $row_fields, $row_actions, $id);
		
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$list, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
	}
	
	/**
	 * display list
	 * 
	 * @param object $object SiteMenu instance
	 * @param array $row_fields
	 * @param array $row_actions
	 * @param int $id SiteMenu ID
	 * @return mixed
	 */
	private function _showView(SiteMenu $object, $row_fields, $row_actions, $id)
	{
		$object->attributes = SiteMenu::setSessionAttributes(Yii::app()->request->getPost('SiteMenu'));
		$criteria = $object->search()->criteria;
		$criteria->compare('parent_site_menu_id', $id);
		$criteria->order = 'position ASC';
		$count = SiteMenu::model()->count($criteria);
		
		$pages = new CPagination($count);
		$criteria->limit = 25;
		$criteria->offset = (Yii::app()->request->getQuery('page', 1) * $criteria->limit) - $criteria->limit;
		
		$pages->pageSize=$criteria->limit;
		$pages->applyLimit($criteria);
		
		$list = SiteMenu::model()->findAll($criteria);
		$view = $this->renderPartial('/layouts/list-body', array('object'=>$object, 'objectList'=>$list, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions), TRUE);
		if (Yii::app()->request->isAjaxRequest) {
			die(CJSON::encode(array('status'=>1, 'message'=>$view)));
		}
		return $view;
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		$menu = new SiteMenu;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($menu);
		}
		
		self::_renderForm($menu);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id SiteMenu primary key
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$menu = SiteMenu::model()->findByPk($id);
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($menu);
		}
		
		self::_renderForm($menu);
	}
	
	/**
	 * Delete record
	 * 
	 * @access public
	 * @param int $id SiteMenu ID
	 * @return void
	 */
	public function actionDelete($id)
	{
		$menu = SiteMenu::model()->findByPk($id);
		SiteMenu::validateObject($menu, 'SiteMenu');
		
		if (Yii::app()->request->isPostRequest) {
			// check for security token
			if (CPasswordHelper::verifyPassword($menu->site_menu_id, Yii::app()->request->getPost('security_key'))) {
				// Delete permissions
				SiteRolePermission::model()->deleteAllByAttributes(array('site_menu_id'=>$menu->site_menu_id));
				
				// Delete record
				SiteMenu::model()->deleteByPk($menu->site_menu_id);
				Yii::app()->getModule('backoffice')->admin->setFlash('success', 'Delete successful!');
				$this->redirect(array("$this->id/"));
			}
			else {
				Yii::app()->getModule('backoffice')->admin->setFlash('fail', 'Error in deletion.');
				$this->redirect(array("$this->id/"));
			}
		}
		$this->render('delete', array('menu'=>$menu));
	}
	
	/**
	 * Update menu position
	 * 
	 * @access active
	 * @return string
	 */
	public function actionPosition()
	{
		$query = Yii::app()->request->getQuery('page-list');
		
		$menu = new SiteMenu;
		foreach ($query as $position => $id) {
			$menu->updateByPk($id, array('position' => (int)$position + 1));
		}
		
		die('Repositioning successful. Refresh page to view changes');
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object SiteMenu instance
	 * @return void
	 */
	private function _renderForm(SiteMenu $object)
	{
		$fields = array();
		
		$parent_menus = SiteMenu::model()->findAllByAttributes(array('parent_site_menu_id' => 0, 'display'=>1), array('order'=>'position ASC'));
		$fields['parent_site_menu_id']['type'] = 'select';
		$fields['parent_site_menu_id']['class'] = 'col-lg-5';
		$fields['parent_site_menu_id']['value'] = array('Home') + CHtml::listData($parent_menus, 'site_menu_id', 'title');
		
		$fields['title']['type'] = 'text';
		$fields['title']['class'] = 'col-md-5';
		
		$fields['classname']['type'] = 'text';
		$fields['classname']['class'] = 'col-md-5';
		
		$fields['img']['type'] = 'text';
		$fields['img']['class'] = 'col-lg-5';
		
		$fields['display']['type'] = 'radio';
		$fields['display']['class'] = 'col-lg-5';
		$fields['display']['value'] = array('1' => 'Enabled', '0' => 'Disabled');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object' => $object, 'fields' => $fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object SiteMenu instance
	 * @return	void
	 */
	private function _postValidate(SiteMenu $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('SiteMenu');
		$object->attributes = $post;

		if ($object->validate() && $object->save())
		{
			// add all access to main client
			$permission = new SiteRolePermission;
			$permission->deleteAllByAttributes(array('site_menu_id' => $object->{$object->tableSchema->primaryKey}));
			$permission->site_menu_id = $object->{$object->tableSchema->primaryKey};
			$permission->site_role_id = 1;
			$permission->view = 1;
			$permission->add = 1;
			$permission->edit = 1;
			$permission->delete = 1;
			$permission->save();
			
			$this->log($object->{$object->tableSchema->primaryKey});
			self::displayFormSuccess();
		}
		self::displayFormError($object);
	}
}