<?php

class StateController extends BackofficeController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action)) {
			// action buttons
			$this->actionButtons['index'] = array('add'=>array('link'=>$this->createURL($this->id.'/add')));
			$this->actionButtons['add'] = array('back'=>array('link'=>$this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back'=>array('link'=>$this->createURL($this->id.'/')));
			$this->actionButtons['view'] = array('back'=>array('link'=>$this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{	
		// fields to be displayed
		$row_fields = array();
		
		$countries = Country::model()->findAll(array('order'=>'country ASC'));
		$row_fields['country_id']['type'] = 'select';
		$row_fields['country_id']['class'] = 'col-md-2';
		$row_fields['country_id']['value'] = CHtml::listData($countries, 'country_id', 'country');
		
		$row_fields['state']['type'] = 'text';
		$row_fields['state']['class'] = 'col-md-3';
		
		$row_fields['iso_code']['type'] = 'text';
		$row_fields['iso_code']['class'] = 'col-md-2';
		
		$row_fields['is_active']['type'] = 'select';
		$row_fields['is_active']['class'] = 'text-center col-md-1';
		$row_fields['is_active']['value'] = array('1'=>'Enabled', '0' =>'Disabled');
		$row_fields['is_active']['mode'] = 'bool';
		
		$row_actions = array('view', 'edit', 'delete');
		$object = new State();
		$list = self::_showList($object, array('fields'=>$row_fields, 'actions'=>$row_actions), 50);
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$list['view'], 'pagination'=>$list['pagination'], 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		$state = new State;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($state);
		}
		
		self::_renderForm($state);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id State primary key
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$state = State::model()->findByPk($id);
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($state);
		}
		
		self::_renderForm($state);
	}
	
	/**
	 * Delete record
	 * 
	 * @access public
	 * @param int $id State ID
	 * @return void
	 */
	public function actionDelete($id)
	{
		$state = State::model()->findByPk($id);
		State::validateObject($state, 'State');
		
		if (Yii::app()->request->isPostRequest) {
			// check for security token
			if (CPasswordHelper::verifyPassword($state->state_id, Yii::app()->request->getPost('security_key'))) {
				
				# TO DO
				die();
				
				// Delete record
				State::model()->deleteByPk($state->state_id);
				
				// Log
				$this->log($state->state_id);
				
				Yii::app()->user->setFlash('success', 'Delete successful!');
				$this->redirect(array("$this->id/"));
			}
			else {
				Yii::app()->user->setFlash('fail', 'Error in deletion.');
				$this->redirect(array("$this->id/"));
			}
		}
		$this->render('delete', array('state'=>$state));
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object State instance
	 * @return void
	 */
	private function _renderForm(State $object)
	{
		$fields = array();
		
		$countries = Country::model()->findAllByAttributes(array('is_active'=>1), array('order'=>'country ASC'));
		$fields['country_id']['type'] = 'select';
		$fields['country_id']['class'] = 'col-md-5';
		$fields['country_id']['value'] = CHtml::listData($countries, 'country_id', 'country');
		
		$fields['state']['type'] = 'text';
		$fields['state']['class'] = 'col-md-5';
		
		$fields['iso_code']['type'] = 'text';
		$fields['iso_code']['class'] = 'col-md-5';
		
		$fields['is_active']['type'] = 'radio';
		$fields['is_active']['class'] = 'col-lg-5';
		$fields['is_active']['value'] = array('1'=>'Enabled', '0'=>'Disabled');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object'=>$object, 'fields'=>$fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object State instance
	 * @return	void
	 */
	private function _postValidate(State $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('State');
		$object->attributes = $post;

		if ($object->validate() && $object->save()) {
			$this->log($object->{$object->tableSchema->primaryKey});
			self::displayFormSuccess();
		}
		self::displayFormError($object);
	}
}