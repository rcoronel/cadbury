<?php

class Ssid_groupController extends BackofficeController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action)) {
			// action buttons
			$this->actionButtons['index'] = array('add' => array('link' => $this->createURL($this->id.'/add')));
			$this->actionButtons['view'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['add'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back' => array('link' => $this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		// fields to be displayed
		$row_fields = array();
		$row_fields['portal_ssid_group_id']['type'] = 'text';
		$row_fields['portal_ssid_group_id']['class'] = 'text-center col-md-1';
		
		$portals = Portal::model()->findAll(array('order'=>'name ASC'));
		$row_fields['portal_id']['type'] = 'select';
		$row_fields['portal_id']['class'] = 'col-md-3';
		$row_fields['portal_id']['parent'] = 'portal';
		$row_fields['portal_id']['child'] = 'name';
		$row_fields['portal_id']['value'] = CHtml::listData($portals, 'portal_id', 'name');
		
		$ap_groups = AccessPointGroup::model()->findAll(array('order'=>'name ASC'));
		$row_fields['access_point_group_id']['type'] = 'select';
		$row_fields['access_point_group_id']['class'] = 'col-md-3';
		$row_fields['access_point_group_id']['parent'] = 'ap_group';
		$row_fields['access_point_group_id']['child'] = 'name';
		$row_fields['access_point_group_id']['value'] = CHtml::listData($ap_groups, 'access_point_group_id', 'name');
		
		$ssids = ServiceSet::model()->findAll(array('order'=>'ssid ASC'));
		$row_fields['service_set_id']['type'] = 'select';
		$row_fields['service_set_id']['class'] = 'col-md-3';
		$row_fields['service_set_id']['parent'] = 'ssid';
		$row_fields['service_set_id']['child'] = 'ssid';
		$row_fields['service_set_id']['value'] = CHtml::listData($ssids, 'service_set_id', 'ssid');
		
		$row_actions = array('edit', 'delete');
		$object = new PortalSsidGroup();
		$list = self::_showList($object, array('fields'=>$row_fields, 'actions'=>$row_actions), 100, TRUE);
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$list['view'], 'pagination'=>$list['pagination'], 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		$apg = new PortalSsidGroup;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($apg);
		}
		
		self::_renderForm($apg);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id PortalSsidGroup primary key
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$apg = PortalSsidGroup::model()->findByPk($id);
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($apg);
		}
		
		self::_renderForm($apg);
	}
	
	/**
	 * Delete record
	 * 
	 * @access public
	 * @param int $id Access Point Group ID
	 * @return void
	 */
	public function actionDelete($id)
	{
		$group = PortalSsidGroup::model()->findByPk($id);
		PortalSsidGroup::validateObject($group, 'PortalSsidGroup');
		
		if (Yii::app()->request->isPostRequest) {
			// check for security token
			if (CPasswordHelper::verifyPassword($group->portal_ssid_group_id, Yii::app()->request->getPost('security_key'))) {
				// Delete record
				PortalSsidGroup::model()->deleteByPk($group->portal_ssid_group_id);
				
				// Log
				$this->log($group->portal_ssid_group_id);
				
				Yii::app()->user->setFlash('success', 'Delete successful!');
			}
			else {
				Yii::app()->user->setFlash('fail', 'Error in deletion.');
			}
			$this->redirect(array("$this->id/"));
		}
		$this->render('delete', array('group'=>$group));
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object PortalSsidGroup instance
	 * @return void
	 */
	private function _renderForm(PortalSsidGroup $object)
	{
		$fields = array();
		
		$portals = Portal::model()->findAll(array('order'=>'name ASC'));
		$fields['portal_id']['type'] = 'select';
		$fields['portal_id']['class'] = 'col-md-5';
		$fields['portal_id']['value'] = CHtml::listData($portals, 'portal_id', 'name');
		
		$ap_groups = AccessPointGroup::model()->findAll(array('order'=>'name ASC'));
		$fields['access_point_group_id']['type'] = 'select';
		$fields['access_point_group_id']['class'] = 'col-md-5';
		$fields['access_point_group_id']['value'] = CHtml::listData($ap_groups, 'access_point_group_id', 'name');
		
		$ssids = ServiceSet::model()->findAll(array('order'=>'ssid ASC'));
		$fields['service_set_id']['type'] = 'select';
		$fields['service_set_id']['class'] = 'col-md-5';
		$fields['service_set_id']['value'] = CHtml::listData($ssids, 'service_set_id', 'ssid');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object' => $object, 'fields' => $fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object PortalSsidGroup instance
	 * @return	void
	 */
	private function _postValidate(PortalSsidGroup $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('PortalSsidGroup');
		$object->attributes = $post;

		if ($object->validate() && $object->save()) {
			$this->log($object->{$object->tableSchema->primaryKey});
			self::displayFormSuccess();
		}
		self::displayFormError($object);
	}

}