<?php

class CityController extends BackofficeController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action)) {
			// action buttons
			$this->actionButtons['index'] = array('add'=>array('link'=>$this->createURL($this->id.'/add')));
			$this->actionButtons['add'] = array('back'=>array('link'=>$this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back'=>array('link'=>$this->createURL($this->id.'/')));
			$this->actionButtons['view'] = array('back'=>array('link'=>$this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{	
		// fields to be displayed
		$row_fields = array();
		
		$countries = Country::model()->findAll(array('order'=>'country ASC'));
		$row_fields['country_id']['type'] = 'select';
		$row_fields['country_id']['class'] = 'col-md-2';
		$row_fields['country_id']['value'] = CHtml::listData($countries, 'country_id', 'country');
		
		$states = State::model()->findAll(array('order'=>'state ASC'));
		$row_fields['state_id']['type'] = 'select';
		$row_fields['state_id']['class'] = 'col-md-2';
		$row_fields['state_id']['value'] = CHtml::listData($states, 'state_id', 'state');
		
		$row_fields['city']['type'] = 'text';
		$row_fields['city']['class'] = 'col-md-4';
		
		$row_fields['is_active']['type'] = 'select';
		$row_fields['is_active']['class'] = 'text-center col-md-1';
		$row_fields['is_active']['value'] = array('1'=>'Enabled', '0' =>'Disabled');
		$row_fields['is_active']['mode'] = 'bool';
		
		$row_actions = array('view', 'edit', 'delete');
		$object = new City();
		$list = self::_showList($object, array('fields'=>$row_fields, 'actions'=>$row_actions), 250);
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$list['view'], 'pagination'=>$list['pagination'], 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		$city = new City;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($city);
		}
		$city->country_id = 172;
		$city->state_id = 82;
		$city->is_active = 1;
		self::_renderForm($city);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id City primary key
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$state = City::model()->findByPk($id);
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($state);
		}
		
		self::_renderForm($state);
	}
	
	/**
	 * Delete record
	 * 
	 * @access public
	 * @param int $id City ID
	 * @return void
	 */
	public function actionDelete($id)
	{
		$state = City::model()->findByPk($id);
		City::validateObject($state, 'City');
		
		if (Yii::app()->request->isPostRequest) {
			// check for security token
			if (CPasswordHelper::verifyPassword($state->state_id, Yii::app()->request->getPost('security_key'))) {
				
				# TO DO
				die();
				
				// Delete record
				City::model()->deleteByPk($state->state_id);
				
				// Log
				$this->log($state->state_id);
				
				Yii::app()->user->setFlash('success', 'Delete successful!');
				$this->redirect(array("$this->id/"));
			}
			else {
				Yii::app()->user->setFlash('fail', 'Error in deletion.');
				$this->redirect(array("$this->id/"));
			}
		}
		$this->render('delete', array('state'=>$state));
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object City instance
	 * @return void
	 */
	private function _renderForm(City $object)
	{
		$fields = array();
		
		$countries = Country::model()->findAllByAttributes(array('is_active'=>1), array('order'=>'country ASC'));
		$fields['country_id']['type'] = 'select';
		$fields['country_id']['class'] = 'col-md-5';
		$fields['country_id']['value'] = CHtml::listData($countries, 'country_id', 'country');
		
		$states = State::model()->findAllByAttributes(array('is_active'=>1), array('order'=>'state ASC'));
		$fields['state_id']['type'] = 'select';
		$fields['state_id']['class'] = 'col-md-5';
		$fields['state_id']['value'] = CHtml::listData($states, 'state_id', 'state');
		
		$fields['city']['type'] = 'text';
		$fields['city']['class'] = 'col-md-5';
		
		$fields['is_active']['type'] = 'radio';
		$fields['is_active']['class'] = 'col-lg-5';
		$fields['is_active']['value'] = array('1'=>'Enabled', '0'=>'Disabled');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object'=>$object, 'fields'=>$fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object City instance
	 * @return	void
	 */
	private function _postValidate(City $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('City');
		$object->attributes = $post;

		if ($object->validate() && $object->save()) {
			$this->log($object->{$object->tableSchema->primaryKey});
			self::displayFormSuccess();
		}
		self::displayFormError($object);
	}
}