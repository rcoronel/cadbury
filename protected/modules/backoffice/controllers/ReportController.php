<?php

class ReportController extends BackofficeController
{
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex() 
	{
		$model = new ReportForm;
		 
		// if ($model->load(Yii::app()->request->post()) && $model->validate()) {
		if (isset($_POST['submit'])) {
			$count = array(
				'title'			=> 0,
				't_u_v'			=> 0,
				't_u_a_v'		=> 0,
				'u_n_a_v'		=> 0,
				'u_r_a_v'		=> 0,
				'bounce_rate'	=> 0,
				'total_reg'		=> 0,
				'unique_fb'		=> 0,
				'unique_tattoo'	=> 0,
				'unique_reg'	=> 0,
				'unique_survey'	=> 0,
				'unique_free'	=> 0,
				'undefined'		=> 0,
				'mobtel'		=> 0,
				'globe'			=> 0,
				'smart'			=> 0,
				'sun'			=> 0,
				'total_session'	=> 0,
				'ave_duration'	=> 0,
			);
			
			if($_POST['ReportForm']['sites'] == "Consolidated Sites"):
				$count['title'] = "Consolidated Sites";
				
				if($_POST['ReportForm']['choose_report'] == "All Reports"):
					$t_u_v			= ReportForm::total_unique_visits($_POST['ReportForm']['dates']);
					$t_u_a_v		= ReportForm::total_unique_auth_visitors($_POST['ReportForm']['dates']);
					$u_n_a_v		= ReportForm::unique_new_auth_visitors($_POST['ReportForm']['dates']); 
					$u_r_a_v		= ReportForm::unique_returning_auth_visitors($_POST['ReportForm']['dates']);
					$fb				= ReportForm::fb($_POST['ReportForm']['dates']);
					$tattoo			= ReportForm::tattoo($_POST['ReportForm']['dates']);
					$registration	= ReportForm::registration($_POST['ReportForm']['dates']);
					$survey			= ReportForm::survey($_POST['ReportForm']['dates']);
					$free			= ReportForm::free($_POST['ReportForm']['dates']);
					$mobtel			= ReportForm::mobtel($_POST['ReportForm']['dates']);
					$unique_mobile	= ReportForm::unique_mobile($_POST['ReportForm']['dates']);
					
					/* Radius */
					$total_session						= ReportForm::total_session($_POST['ReportForm']['dates']);
					$average_duration_per_unique_auth	= ReportForm::average_duration_per_unique_auth($_POST['ReportForm']['dates']);
					$total_input_volume_in_gb			= ReportForm::total_input_volume_in_gb($_POST['ReportForm']['dates']);
					$total_output_volume_in_gb			= ReportForm::total_output_volume_in_gb($_POST['ReportForm']['dates']);

					foreach($t_u_v as $key=>$value):
						$count['t_u_v'] += $value['count'];
					endforeach;
					
					/* Total Unique Auth Visitor */
					foreach($t_u_a_v as $key=>$value):
						$count['t_u_a_v'] += $value['count'];
					endforeach;
					
					/* Unique New Auth Visitor */
					foreach($u_n_a_v as $key=>$value):
						$count['u_n_a_v'] += $value['count'];
					endforeach;
					
					/* Unique Returning Auth Visitor */
					foreach($u_r_a_v as $key=>$value):
						$count['u_r_a_v'] += $value['count'];
					endforeach;
					
					/* Bounce Rate */
					$count['bounce_rate'] = ($count['t_u_v'] != 0 || $count['t_u_a_v'] != 0) ? ($count['t_u_v'] - $count['t_u_a_v']) / $count['t_u_v'] : '0'; 
				
					/* Different Registration */
					if(empty($fb)):
						$count['unique_fb'] = 0;
					else:
						foreach($fb as $key=>$value): //FB Logic
							$count['unique_fb'] += $value['count'];
							
							if($value['name'] === $_POST['ReportForm']['sites']):
								$count['unique_fb'] = $value['count'];
								break;
							endif;
						endforeach;
					endif;
					
					if(empty($tattoo)): //Tattoo Logic
						$count['unique_tattoo'] = 0;
					else:
						foreach($tattoo as $key=>$value):
							$count['unique_tattoo'] += $value['count'];
							
							if($tattoo[$x]['name'] === $_POST['ReportForm']['sites']):
								$count['unique_tattoo'] = $value['count'];
								break;
							endif;
						endforeach;				
					endif;
				
					if(empty($survey)): //Survey Logic
						$count['unique_survey'] = 0;
					else:
						foreach($survey as $key=>$value):
							$count['unique_survey'] += $value['count'];
							
							if($value['name'] == $_POST['ReportForm']['sites']):
								$count['unique_survey'] = $value['count'];
								break;
							endif;
						endforeach;				
					endif;
					
					if(empty($free)): //Free Logic
						$count['unique_free'] = 0;
					else:
						foreach($free as $key=>$value):
							$count['unique_free'] += $value['count'];
							
							if($value['name'] === $_POST['ReportForm']['sites']):
								$count['unique_free'] = $value['count'];
							endif;
						endforeach;				
					endif;

					if(empty($mobtel)): //Mobtel Logic
						$count['mobtel'] = 0;
					else:
						foreach($mobtel as $key=>$value):
							$count['mobtel'] += $value['count'];
						endforeach;				
					endif;
					
					foreach($unique_mobile as $value): //Globe, Smart and Sun
						if($value['telco_brand'] === 'GLOBE'):
							$count['globe'] += $value['unique_mobile_brand_users'];
						elseif($value['telco_brand'] === 'SMART'):
							$count['smart'] += $value['unique_mobile_brand_users'];
						elseif($value['telco_brand'] === 'SUN'):
							$count['sun'] += $value['unique_mobile_brand_users'];
						endif;
					endforeach;
					
					$count['total_reg'] = $count['unique_fb'] + $count['unique_tattoo'] + $count['unique_survey'] + $count['unique_free'] + $count['mobtel'];
					$count['undefined'] = $count['mobtel'] - ($count['globe'] + $count['smart'] + $count['sun']);
					
					//Total Session
					for($x=0; $x<count($total_session); $x++):
						$count['total_session'] += $total_session[$x]['sum'];
					endfor;
					
					//Average Duration
					for($x=0; $x<count($average_duration_per_unique_auth); $x++):
						$count['ave_duration'] += $average_duration_per_unique_auth[$x]['sum'];
					endfor;
				elseif($_POST['ReportForm']['choose_report'] == "Total Unique Visits"):
					$t_u_v = ReportForm::total_unique_visits($_POST['ReportForm']['dates']);
					
					foreach($t_u_v as $key=>$value):
						$count['t_u_v'] += $value['count'];
					endforeach;
				elseif($_POST['ReportForm']['choose_report'] == "Total Unique Auth Visitors"):
					$t_u_a_v = ReportForm::total_unique_auth_visitors($_POST['ReportForm']['dates']);
					
					/* Total Unique Auth Visitor */
					foreach($t_u_a_v as $key=>$value):
						$count['t_u_a_v'] += $value['count'];
					endforeach;
				elseif($_POST['ReportForm']['choose_report'] == "Unique New Auth Visitors"):
					$u_n_a_v = ReportForm::unique_new_auth_visitors($_POST['ReportForm']['dates']);
					
					/* Unique New Auth Visitor */
					foreach($u_n_a_v as $key=>$value):
						$count['u_n_a_v'] += $value['count'];
					endforeach;
				elseif($_POST['ReportForm']['choose_report'] == "Unique Returning Auth Visitors"):
					$u_r_a_v = ReportForm::unique_returning_auth_visitors($_POST['ReportForm']['dates']);
					
					/* Unique Returning Auth Visitor */
					foreach($u_r_a_v as $key=>$value):
						$count['u_r_a_v'] += $value['count'];
					endforeach;
				elseif($_POST['ReportForm']['choose_report'] == "Bounce Rate"):
					$t_u_v = ReportForm::total_unique_visits($_POST['ReportForm']['dates']);
					$t_u_a_v = ReportForm::total_unique_auth_visitors($_POST['ReportForm']['dates']);
					
					foreach($t_u_v as $key=>$value):
						$count['t_u_v'] += $value['count'];
					endforeach;
				
					/* Total Unique Auth Visitor */
					foreach($t_u_a_v as $key=>$value):
						$count['t_u_a_v'] += $value['count'];
					endforeach;
					
					/* Bounce Rate */
					$count['bounce_rate'] = ($count['t_u_v'] != 0 || $count['t_u_a_v'] != 0) ? ($count['t_u_v'] - $count['t_u_a_v']) / $count['t_u_v'] : '0'; 
				elseif($_POST['ReportForm']['choose_report'] == "All Registered"):
					$fb				= ReportForm::fb($_POST['ReportForm']['dates']);
					$tattoo			= ReportForm::tattoo($_POST['ReportForm']['dates']);
					$registration	= ReportForm::registration($_POST['ReportForm']['dates']);
					$survey			= ReportForm::survey($_POST['ReportForm']['dates']);
					$free			= ReportForm::free($_POST['ReportForm']['dates']);
					
					/* Different Registration */
					if(empty($fb)):
						$count['unique_fb'] = 0;
					else:
						foreach($fb as $key=>$value): //FB Logic
							$count['unique_fb'] += $value['count'];
							
							if($value['name'] === $_POST['ReportForm']['sites']):
								$count['unique_fb'] = $value['count'];
								break;
							endif;
						endforeach;
					endif;
					
					if(empty($tattoo)): //Tattoo Logic
						$count['unique_tattoo'] = 0;
					else:
						foreach($tattoo as $key=>$value):
							$count['unique_tattoo'] += $value['count'];
							
							if($tattoo[$x]['name'] === $_POST['ReportForm']['sites']):
								$count['unique_tattoo'] = $value['count'];
								break;
							endif;
						endforeach;				
					endif;
				
					if(empty($survey)): //Survey Logic
						$count['unique_survey'] = 0;
					else:
						foreach($survey as $key=>$value):
							$count['unique_survey'] += $value['count'];
							
							if($value['name'] == $_POST['ReportForm']['sites']):
								$count['unique_survey'] = $value['count'];
								break;
							endif;
						endforeach;				
					endif;
					
					if(empty($free)): //Free Logic
						$count['unique_free'] = 0;
					else:
						foreach($free as $key=>$value):
							$count['unique_free'] += $value['count'];
							
							if($value['name'] === $_POST['ReportForm']['sites']):
								$count['unique_free'] = $value['count'];
							endif;
						endforeach;				
					endif;
					
					$count['total_reg'] = $count['unique_fb'] + $count['unique_tattoo'] + $count['unique_survey'] + $count['unique_free'] + $count['mobtel'];
				elseif($_POST['ReportForm']['choose_report'] == "Unique Facebook"):
					$fb	= ReportForm::fb($_POST['ReportForm']['dates']);
					
					if(empty($fb)):
						$count['unique_fb'] = 0;
					else:
						foreach($fb as $key=>$value): //FB Logic
							$count['unique_fb'] += $value['count'];
							
							if($value['name'] === $_POST['ReportForm']['sites']):
								$count['unique_fb'] = $value['count'];
								break;
							endif;
						endforeach;
					endif;
				elseif($_POST['ReportForm']['choose_report'] == "Unique Tattoo"):
					$tattoo	= ReportForm::tattoo($_POST['ReportForm']['dates']);
				
					if(empty($tattoo)): //Tattoo Logic
						$count['unique_tattoo'] = 0;
					else:
						foreach($tattoo as $key=>$value):
							$count['unique_tattoo'] += $value['count'];
							
							if($tattoo[$x]['name'] === $_POST['ReportForm']['sites']):
								$count['unique_tattoo'] = $value['count'];
								break;
							endif;
						endforeach;				
					endif;
				elseif($_POST['ReportForm']['choose_report'] == "Unique Registration"):
					$registration = ReportForm::registration($_POST['ReportForm']['dates']);				
					
					//Nothing
				elseif($_POST['ReportForm']['choose_report'] == "Unique Survey"):
					$survey = ReportForm::survey($_POST['ReportForm']['dates']);
					
					if(empty($survey)): //Survey Logic
						$count['unique_survey'] = 0;
					else:
						foreach($survey as $key=>$value):
							$count['unique_survey'] += $value['count'];
							
							if($value['name'] == $_POST['ReportForm']['sites']):
								$count['unique_survey'] = $value['count'];
								break;
							endif;
						endforeach;				
					endif;
				elseif($_POST['ReportForm']['choose_report'] == "Unique Easy"):
					//Nothing
				elseif($_POST['ReportForm']['choose_report'] == "Unique Free"):
					$free = ReportForm::free($_POST['ReportForm']['dates']);
					
					if(empty($free)): //Free Logic
						$count['unique_free'] = 0;
					else:
						foreach($free as $key=>$value):
							$count['unique_free'] += $value['count'];
							
							if($value['name'] === $_POST['ReportForm']['sites']):
								$count['unique_free'] = $value['count'];
							endif;
						endforeach;
					endif;
				elseif($_POST['ReportForm']['choose_report'] == "Unique Mobile"):
					$unique_mobile	= ReportForm::unique_mobile($_POST['ReportForm']['dates']);
					
					foreach($unique_mobile as $value): //Globe, Smart and Sun
						$count['mobtel'] += $value['unique_mobile_brand_users'];
					endforeach;
				elseif($_POST['ReportForm']['choose_report'] == "Unique Globe"):
					$unique_mobile	= ReportForm::unique_mobile($_POST['ReportForm']['dates']);
					
					foreach($unique_mobile as $value): //Globe, Smart and Sun
						if($value['telco_brand'] === 'GLOBE'):
							$count['globe'] += $value['unique_mobile_brand_users'];
						endif;
					endforeach;
				elseif($_POST['ReportForm']['choose_report'] == "Unique Smart"):
					$unique_mobile	= ReportForm::unique_mobile($_POST['ReportForm']['dates']);
					
					foreach($unique_mobile as $value): //Globe, Smart and Sun
						if($value['telco_brand'] === 'SMART'):
							$count['smart'] += $value['unique_mobile_brand_users'];
						endif;
					endforeach;
				elseif($_POST['ReportForm']['choose_report'] == "Unique Sun"):
					$unique_mobile	= ReportForm::unique_mobile($_POST['ReportForm']['dates']);
					
					foreach($unique_mobile as $value): //Globe, Smart and Sun
						if($value['telco_brand'] === 'SUN'):
							$count['sun'] += $value['unique_mobile_brand_users'];
						endif;
					endforeach;
				endif;
			else: //Location From Post
				$count['title'] = $_POST['ReportForm']['sites'];
				
				if($_POST['ReportForm']['choose_report'] == "Total Unique Visits"):
					$t_u_v = ReportForm::total_unique_visits($_POST['ReportForm']['dates']);
					
					foreach($t_u_v as $key=>$value):
						if($value['name'] === $_POST['ReportForm']['sites']):
							$count['t_u_v'] = $value['count'];
							break;
						endif;						
					endforeach;
				elseif($_POST['ReportForm']['choose_report'] == "Total Unique Auth Visitors"):
					$t_u_a_v = ReportForm::total_unique_auth_visitors($_POST['ReportForm']['dates']);
					
					foreach($t_u_a_v as $key=>$value):
						if($value['name'] === $_POST['ReportForm']['sites']):
							$count['t_u_a_v'] = $value['count'];
							break;
						endif;
					endforeach;
				elseif($_POST['ReportForm']['choose_report'] == "Unique New Auth Visitors"):
					$u_n_a_v = ReportForm::unique_new_auth_visitors($_POST['ReportForm']['dates']); 

					foreach($u_n_a_v as $key=>$value):
						if($value['name'] === $_POST['ReportForm']['sites']): 
							$count['u_n_a_v'] = $value['count'];
							break;
						endif;
					endforeach;
				elseif($_POST['ReportForm']['choose_report'] == "Unique Returning Auth Visitors"):
					$u_r_a_v = ReportForm::unique_returning_auth_visitors($_POST['ReportForm']['dates']);
									
					foreach($u_r_a_v as $key=>$value):
						if($value['name'] == $_POST['ReportForm']['sites']): 
							$count['u_r_a_v'] = $value['count'];
							break;
						endif;
					endforeach;
				elseif($_POST['ReportForm']['choose_report'] == "Bounce Rate"):
					$t_u_v 		= ReportForm::total_unique_visits($_POST['ReportForm']['dates']);
					$t_u_a_v	= ReportForm::total_unique_auth_visitors($_POST['ReportForm']['dates']);

					foreach($t_u_v as $key=>$value):
						if($value['name'] === $_POST['ReportForm']['sites']):
							$count['t_u_v'] = $value['count'];
							break;
						endif;						
					endforeach;
					
					foreach($t_u_a_v as $key=>$value):
						if($value['name'] === $_POST['ReportForm']['sites']):
							$count['t_u_a_v'] = $value['count'];
							break;
						endif;
					endforeach;
					
					/* Bounce Rate */
					$count['bounce_rate'] = ($count['t_u_v'] != 0 || $count['t_u_a_v'] != 0) ? ($count['t_u_v'] - $count['t_u_a_v']) / $count['t_u_v'] : '0'; 
				elseif($_POST['ReportForm']['choose_report'] == "All Registered"):
					$fb				= ReportForm::fb($_POST['ReportForm']['dates']);
					$tattoo			= ReportForm::tattoo($_POST['ReportForm']['dates']);
					$registration	= ReportForm::registration($_POST['ReportForm']['dates']);
					$survey			= ReportForm::survey($_POST['ReportForm']['dates']);
					$free			= ReportForm::free($_POST['ReportForm']['dates']);
					
					/* Different Registration */
					if(empty($fb)):
						$count['unique_fb'] = 0;
					else:
						foreach($fb as $key=>$value): //FB Logic
							if($value['name'] === $_POST['ReportForm']['sites']):
								$count['unique_fb'] = $value['count'];
								break;
							endif;
						endforeach;
					endif;
					
					if(empty($tattoo)): //Tattoo Logic
						$count['unique_tattoo'] = 0;
					else:
						foreach($tattoo as $key=>$value):
							if($tattoo[$x]['name'] === $_POST['ReportForm']['sites']):
								$count['unique_tattoo'] = $value['count'];
								break;
							endif;
						endforeach;				
					endif;
				
					if(empty($survey)): //Survey Logic
						$count['unique_survey'] = 0;
					else:
						foreach($survey as $key=>$value):
							if($value['name'] == $_POST['ReportForm']['sites']):
								$count['unique_survey'] = $value['count'];
								break;
							endif;
						endforeach;				
					endif;
					
					if(empty($free)): //Free Logic
						$count['unique_free'] = 0;
					else:
						foreach($free as $key=>$value):
							if($value['name'] === $_POST['ReportForm']['sites']):
								$count['unique_free'] = $value['count'];
							endif;
						endforeach;		
					endif;
					
					$count['total_reg'] = $count['unique_fb'] + $count['unique_tattoo'] + $count['unique_survey'] + $count['unique_free'] + $count['mobtel'];
				elseif($_POST['ReportForm']['choose_report'] == "Unique Facebook"):
					$fb	= ReportForm::fb($_POST['ReportForm']['dates']);
					
					if(empty($fb)):
						$count['unique_fb'] = 0;
					else:
						foreach($fb as $key=>$value): //FB Logic
							if($value['name'] === $_POST['ReportForm']['sites']):
								$count['unique_fb'] = $value['count'];
								break;
							endif;
						endforeach;
					endif;
				elseif($_POST['ReportForm']['choose_report'] == "Unique Tattoo"):
					$tattoo	= ReportForm::tattoo($_POST['ReportForm']['dates']);
					
					if(empty($tattoo)): //Tattoo Logic
						$count['unique_tattoo'] = 0;
					else:
						foreach($tattoo as $key=>$value):
							if($tattoo[$x]['name'] === $_POST['ReportForm']['sites']):
								$count['unique_tattoo'] = $value['count'];
								break;
							endif;
						endforeach;				
					endif;
				elseif($_POST['ReportForm']['choose_report'] == "Unique Registration"):
					$registration = ReportForm::registration($_POST['ReportForm']['dates']);
				elseif($_POST['ReportForm']['choose_report'] == "Unique Survey"):
					$survey	= ReportForm::survey($_POST['ReportForm']['dates']);
					
					if(empty($survey)): //Survey Logic
						$count['unique_survey'] = 0;
					else:
						foreach($survey as $key=>$value):
							if($value['name'] == $_POST['ReportForm']['sites']):
								$count['unique_survey'] = $value['count'];
								break;
							endif;
						endforeach;				
					endif;
				elseif($_POST['ReportForm']['choose_report'] == "Unique Easy"):
					//Nothing
				elseif($_POST['ReportForm']['choose_report'] == "Unique Free"):
					$free = ReportForm::free($_POST['ReportForm']['dates']);
					
					if(empty($free)): //Free Logic
						$count['unique_free'] = 0;
					else:
						foreach($free as $key=>$value):
							if($value['name'] === $_POST['ReportForm']['sites']):
								$count['unique_free'] = $value['count'];
							endif;
						endforeach;
					endif;
				elseif($_POST['ReportForm']['choose_report'] == "Unique Mobile"):
					$unique_mobile	= ReportForm::unique_mobile($_POST['ReportForm']['dates']);
					
					foreach($unique_mobile as $key=>$value):
						if($value['site'] == $_POST['ReportForm']['sites']):
							$count['mobtel'] += $value['unique_mobile_brand_users'];
						endif;
					endforeach;
				elseif($_POST['ReportForm']['choose_report'] == "Unique Globe"):
					$unique_mobile	= ReportForm::unique_mobile($_POST['ReportForm']['dates']);
					
					foreach($unique_mobile as $key=>$value):
						if($value['site'] == $_POST['ReportForm']['sites']):
							if($value['telco_brand'] == "GLOBE"):
								$count['globe'] = $value['unique_mobile_brand_users'];
							endif;
						endif;
					endforeach;
				elseif($_POST['ReportForm']['choose_report'] == "Unique Smart"):
					$unique_mobile	= ReportForm::unique_mobile($_POST['ReportForm']['dates']);
					
					foreach($unique_mobile as $key=>$value):
						if($value['site'] == $_POST['ReportForm']['sites']):							
							if($value['telco_brand'] == "SMART"):
								$count['smart'] = $value['unique_mobile_brand_users'];
							endif;
						endif;
					endforeach;
				elseif($_POST['ReportForm']['choose_report'] == "Unique Sun"):
					$unique_mobile	= ReportForm::unique_mobile($_POST['ReportForm']['dates']);
					
					foreach($unique_mobile as $key=>$value):
						if($value['site'] == $_POST['ReportForm']['sites']):
							if($value['telco_brand'] == "SUN"):
								$count['sun'] = $value['unique_mobile_brand_users'];
							endif;
						endif;
					endforeach;
				elseif($_POST['ReportForm']['choose_report'] == "Undefined"):
					$unique_mobile	= ReportForm::unique_mobile($_POST['ReportForm']['dates']);
					
					foreach($unique_mobile as $key=>$value):
						if($value['site'] == $_POST['ReportForm']['sites']):
							if($value['telco_brand'] == "GLOBE"):
								$count['globe'] = $value['unique_mobile_brand_users'];
							endif;
						endif;
					endforeach;
					
					foreach($unique_mobile as $key=>$value):
						if($value['site'] == $_POST['ReportForm']['sites']):							
							if($value['telco_brand'] == "SMART"):
								$count['smart'] = $value['unique_mobile_brand_users'];
							endif;
						endif;
					endforeach;
					
					foreach($unique_mobile as $key=>$value):
						if($value['site'] == $_POST['ReportForm']['sites']):
							if($value['telco_brand'] == "SUN"):
								$count['sun'] = $value['unique_mobile_brand_users'];
							endif;
						endif;
					endforeach;
					
					$count['undefined'] = $count['mobtel'] - ($count['globe'] + $count['smart'] + $count['sun']);
				endif;
			endif;
			
			Yii::import('ext.phpexcel.XPHPExcel');
			$objPHPExcel = XPHPExcel::createPHPExcel();

			// $objPHPExcel = new \PHPExcel();

			$sheet_title 	= array('Total Unique Visit Per Site', 'Cadbury');
			$sheet 			= 0;
			
			$objPHPExcel->setActiveSheetIndex($sheet); 
			// Create a new worksheet, after the default sheet
			$objPHPExcel->createSheet();
			
			// Add some data to the second sheet, resembling some different data types
			$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->setTitle($count['title'])
						->setCellValue('A1', '')
						->setCellValue('B1', 'Total')
						->setCellValue('C1', 'Date');
			
			$objPHPExcel->getActiveSheet()->setCellValue('A2', 'CONSOLIDATED SITES'); 
			$objPHPExcel->getActiveSheet()->setCellValue('B2', '');
			$objPHPExcel->getActiveSheet()->setCellValue('C2', '');
						
			$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Total Visits'); 
			$objPHPExcel->getActiveSheet()->setCellValue('B3', '');
			$objPHPExcel->getActiveSheet()->setCellValue('C3', '');
			
			$objPHPExcel->getActiveSheet()->setCellValue('A4', 'Total Unique Visits'); 
			$objPHPExcel->getActiveSheet()->setCellValue('B4', $count['t_u_v']);
			$objPHPExcel->getActiveSheet()->setCellValue('C4', $count['t_u_v']);
			
			$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Total Unique Auth Visitors'); 
			$objPHPExcel->getActiveSheet()->setCellValue('B5', $count['t_u_a_v']);
			$objPHPExcel->getActiveSheet()->setCellValue('C5', $count['t_u_a_v']);
			
			$objPHPExcel->getActiveSheet()->setCellValue('A6', 'Unique New Auth Visitors'); 
			$objPHPExcel->getActiveSheet()->getStyle('A6')->getAlignment()->setIndent(2);
			$objPHPExcel->getActiveSheet()->setCellValue('B6', $count['u_n_a_v']);
			$objPHPExcel->getActiveSheet()->setCellValue('C6', $count['u_n_a_v']);
			
			$objPHPExcel->getActiveSheet()->setCellValue('A7', 'Unique Returning Auth Visitors'); 
			$objPHPExcel->getActiveSheet()->getStyle('A7')->getAlignment()->setIndent(2);
			$objPHPExcel->getActiveSheet()->setCellValue('B7', $count['u_r_a_v']);
			$objPHPExcel->getActiveSheet()->setCellValue('C7', $count['u_r_a_v']);
			
			$objPHPExcel->getActiveSheet()->setCellValue('A8', 'Total Bounce Rate');
			$objPHPExcel->getActiveSheet()->setCellValue('B8', round((float)$count['bounce_rate'] * 100).'%');
			$objPHPExcel->getActiveSheet()->setCellValue('C8', round((float)$count['bounce_rate'] * 100).'%');
			
			$objPHPExcel->getActiveSheet()->setCellValue('A9', 'Total Registration (7 methods)');
			$objPHPExcel->getActiveSheet()->setCellValue('B9', $count['total_reg']);
			$objPHPExcel->getActiveSheet()->setCellValue('C9', $count['total_reg']);
			
			$objPHPExcel->getActiveSheet()->setCellValue('A10', 'Unique Via FB');
			$objPHPExcel->getActiveSheet()->getStyle('A10')->getAlignment()->setIndent(2);
			$objPHPExcel->getActiveSheet()->setCellValue('B10', (count($count['unique_fb']) <= 0 ? '0' : $count['unique_fb']));
			$objPHPExcel->getActiveSheet()->setCellValue('C10', (count($count['unique_fb']) <= 0 ? '0' : $count['unique_fb']));
			
			$objPHPExcel->getActiveSheet()->setCellValue('A11', 'Unique Via Tattoo');
			$objPHPExcel->getActiveSheet()->getStyle('A11')->getAlignment()->setIndent(2);
			$objPHPExcel->getActiveSheet()->setCellValue('B11', $count['unique_tattoo']);
			$objPHPExcel->getActiveSheet()->setCellValue('C11', $count['unique_tattoo']);
			
			$objPHPExcel->getActiveSheet()->setCellValue('A12', 'Unique Reg');
			$objPHPExcel->getActiveSheet()->getStyle('A12')->getAlignment()->setIndent(2);
			$objPHPExcel->getActiveSheet()->setCellValue('B12', '0');
			$objPHPExcel->getActiveSheet()->setCellValue('C12', '0');
			
			$objPHPExcel->getActiveSheet()->setCellValue('A13', 'Unique Survey');
			$objPHPExcel->getActiveSheet()->getStyle('A13')->getAlignment()->setIndent(2);
			$objPHPExcel->getActiveSheet()->setCellValue('B13', $count['unique_survey']);
			$objPHPExcel->getActiveSheet()->setCellValue('C13', $count['unique_survey']);
			
			$objPHPExcel->getActiveSheet()->setCellValue('A14', 'Unique Easy');
			$objPHPExcel->getActiveSheet()->getStyle('A14')->getAlignment()->setIndent(2);
			$objPHPExcel->getActiveSheet()->setCellValue('B14', '0');
			$objPHPExcel->getActiveSheet()->setCellValue('C14', '0');
			
			$objPHPExcel->getActiveSheet()->setCellValue('A15', 'Unique Free');
			$objPHPExcel->getActiveSheet()->getStyle('A15')->getAlignment()->setIndent(2);
			$objPHPExcel->getActiveSheet()->setCellValue('B15', $count['unique_free']);
			$objPHPExcel->getActiveSheet()->setCellValue('C15', $count['unique_free']);
			
			$objPHPExcel->getActiveSheet()->setCellValue('A16', 'Unique Via Mobile Number');
			$objPHPExcel->getActiveSheet()->getStyle('A16')->getAlignment()->setIndent(2);
			$objPHPExcel->getActiveSheet()->setCellValue('B16', $count['mobtel']);
			$objPHPExcel->getActiveSheet()->setCellValue('C16', $count['mobtel']);
			
			$objPHPExcel->getActiveSheet()->setCellValue('A17', 'Unique Globe');
			$objPHPExcel->getActiveSheet()->getStyle('A17')->getAlignment()->setIndent(4);
			$objPHPExcel->getActiveSheet()->setCellValue('B17', $count['globe']);
			$objPHPExcel->getActiveSheet()->setCellValue('C17', $count['globe']);
			
			$objPHPExcel->getActiveSheet()->setCellValue('A18', 'Unique Smart');
			$objPHPExcel->getActiveSheet()->getStyle('A18')->getAlignment()->setIndent(4);
			$objPHPExcel->getActiveSheet()->setCellValue('B18', $count['smart']);
			$objPHPExcel->getActiveSheet()->setCellValue('C18', $count['smart']);
			
			$objPHPExcel->getActiveSheet()->setCellValue('A19', 'Unique Sun');
			$objPHPExcel->getActiveSheet()->getStyle('A19')->getAlignment()->setIndent(4);
			$objPHPExcel->getActiveSheet()->setCellValue('B19', $count['sun']);
			$objPHPExcel->getActiveSheet()->setCellValue('C19', $count['sun']);
			
			$objPHPExcel->getActiveSheet()->setCellValue('A20', 'Undefined');
			$objPHPExcel->getActiveSheet()->getStyle('A20')->getAlignment()->setIndent(4);
			$objPHPExcel->getActiveSheet()->setCellValue('B20', $count['undefined']);
			$objPHPExcel->getActiveSheet()->setCellValue('C20', $count['undefined']);
			
			$objPHPExcel->getActiveSheet()->setCellValue('A21', 'Total Session duration (In Hrs.)');
			$objPHPExcel->getActiveSheet()->setCellValue('B21', $count['total_session']);
			$objPHPExcel->getActiveSheet()->setCellValue('C21', $count['total_session']);
			
			$objPHPExcel->getActiveSheet()->setCellValue('A22', 'Ave duration per unique auth');
			$objPHPExcel->getActiveSheet()->getStyle('A22')->getAlignment()->setIndent(2);
			$objPHPExcel->getActiveSheet()->setCellValue('B22', $count['ave_duration']);
			$objPHPExcel->getActiveSheet()->setCellValue('C22', $count['ave_duration']);
			
			$objPHPExcel->getActiveSheet()->setCellValue('A23', 'in hours');
			$objPHPExcel->getActiveSheet()->getStyle('A23')->getAlignment()->setIndent(4);
			$objPHPExcel->getActiveSheet()->setCellValue('B23', $count['ave_duration']);
			$objPHPExcel->getActiveSheet()->setCellValue('C23', $count['ave_duration']);
			
			header('Content-Type: application/vnd.ms-excel');
			$filename = "Cadbury_report".$_POST['ReportForm']['dates'].".xls";
			header('Content-Disposition: attachment;filename='.$filename .' ');
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
		} else {
			// validation failed: $errors is an array containing error messages
			// $errors = $model->errors;
			return $this->render('index', array('model' => $model));
		}
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}