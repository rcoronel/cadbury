<?php

class CountryController extends BackofficeController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action)) {
			// action buttons
			$this->actionButtons['index'] = array('add'=>array('link'=>$this->createURL($this->id.'/add')));
			$this->actionButtons['add'] = array('back'=>array('link'=>$this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back'=>array('link'=>$this->createURL($this->id.'/')));
			$this->actionButtons['view'] = array('back'=>array('link'=>$this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{	
		// fields to be displayed
		$row_fields = array();
		
		$row_fields['country']['type'] = 'text';
		$row_fields['country']['class'] = 'col-md-4';
		
		$row_fields['iso_code']['type'] = 'text';
		$row_fields['iso_code']['class'] = 'col-md-2';
		
		$row_fields['call_prefix']['type'] = 'text';
		$row_fields['call_prefix']['class'] = 'col-md-2';
		
		$row_fields['is_active']['type'] = 'select';
		$row_fields['is_active']['class'] = 'text-center col-md-1';
		$row_fields['is_active']['value'] = array('1'=>'Enabled', '0' =>'Disabled');
		$row_fields['is_active']['mode'] = 'bool';
		
		$row_actions = array('view', 'edit', 'delete');
		$object = new Country();
		$list = self::_showList($object, array('fields'=>$row_fields, 'actions'=>$row_actions), 50);
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$list['view'], 'pagination'=>$list['pagination'], 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
	}
	
	
	/**
	 * View portal settings
	 * 
	 * @access public
	 * @param int $id Country ID
	 * @return void
	 */
	public function actionView($id)
	{
		$this->redirect(array('portal_setting/', 'id'=>$id));
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		$country = new Country;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($country);
		}
		
		self::_renderForm($country);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id Country primary key
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$country = Country::model()->findByPk($id);
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($country);
		}
		
		self::_renderForm($country);
	}
	
	/**
	 * Delete record
	 * 
	 * @access public
	 * @param int $id Country ID
	 * @return void
	 */
	public function actionDelete($id)
	{
		$country = Country::model()->findByPk($id);
		Country::validateObject($country, 'Country');
		
		if (Yii::app()->request->isPostRequest) {
			// check for security token
			if (CPasswordHelper::verifyPassword($country->country_id, Yii::app()->request->getPost('security_key'))) {
				
				# TO DO
				die();
				
				// Delete record
				Country::model()->deleteByPk($country->country_id);
				
				// Log
				$this->log($country->country_id);
				
				Yii::app()->user->setFlash('success', 'Delete successful!');
				$this->redirect(array("$this->id/"));
			}
			else {
				Yii::app()->user->setFlash('fail', 'Error in deletion.');
				$this->redirect(array("$this->id/"));
			}
		}
		$this->render('delete', array('country'=>$country));
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object Country instance
	 * @return void
	 */
	private function _renderForm(Country $object)
	{
		$fields = array();
		
		$fields['country']['type'] = 'text';
		$fields['country']['class'] = 'col-md-5';
		
		$fields['iso_code']['type'] = 'text';
		$fields['iso_code']['class'] = 'col-md-5';
		
		$fields['call_prefix']['type'] = 'text';
		$fields['call_prefix']['class'] = 'col-md-5';
		
		$fields['zip_code_format']['type'] = 'text';
		$fields['zip_code_format']['class'] = 'col-md-5';
		
		$fields['is_active']['type'] = 'radio';
		$fields['is_active']['class'] = 'col-lg-5';
		$fields['is_active']['value'] = array('1'=>'Enabled', '0'=>'Disabled');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object'=>$object, 'fields'=>$fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object Country instance
	 * @return	void
	 */
	private function _postValidate(Country $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('Country');
		$object->attributes = $post;

		if ($object->validate() && $object->save()) {
			$this->log($object->{$object->tableSchema->primaryKey});
			self::displayFormSuccess();
		}
		self::displayFormError($object);
	}
}