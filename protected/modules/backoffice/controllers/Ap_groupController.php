<?php

class Ap_groupController extends BackofficeController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action))
		{
			// action buttons
			$this->actionButtons['index'] = array('add' => array('link' => $this->createURL($this->id.'/add')));
			$this->actionButtons['add'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back' => array('link' => $this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		// fields to be displayed
		$row_fields = array();
		$row_fields['access_point_group_id']['type'] = 'text';
		$row_fields['access_point_group_id']['class'] = 'text-center col-md-1';
		
		$row_fields['name']['type'] = 'text';
		$row_fields['name']['class'] = 'col-md-4';
		
		$portals = Portal::model()->findAll(array('order'=>'name ASC'));
		$row_fields['portal_id']['type'] = 'select';
		$row_fields['portal_id']['class'] = 'col-md-2';
		$row_fields['portal_id']['parent'] = 'portal';
		$row_fields['portal_id']['child'] = 'name';
		$row_fields['portal_id']['value'] = CHtml::listData($portals, 'portal_id', 'name');
		
		$row_fields['is_active']['type'] = 'select';
		$row_fields['is_active']['class'] = 'text-center col-md-1';
		$row_fields['is_active']['value'] = array('1'=>'Enabled', '0' =>'Disabled');
		$row_fields['is_active']['mode'] = 'bool';
		
		$row_actions = array('edit', 'delete');
		$object = new AccessPointGroup();
		$list = self::_showList($object, array('fields'=>$row_fields, 'actions'=>$row_actions), 50);
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$list['view'], 'pagination'=>$list['pagination'], 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		$apg = new AccessPointGroup;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($apg);
		}
		
		self::_renderForm($apg);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id AccessPointGroup primary key
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$apg = AccessPointGroup::model()->findByPk($id);
		AccessPointGroup::validateObject($apg, 'AccessPointGroup');
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($apg);
		}
		
		self::_renderForm($apg);
	}
	
	/**
	 * Delete record
	 * 
	 * @access public
	 * @param int $id Access Point Group ID
	 * @return void
	 */
	public function actionDelete($id)
	{
		$group = AccessPointGroup::model()->findByPk($id);
		AccessPointGroup::validateObject($group, 'AccessPointGroup');
		
		if (Yii::app()->request->isPostRequest) {
			// check for security token
			if (CPasswordHelper::verifyPassword($group->access_point_group_id, Yii::app()->request->getPost('security_key'))) {
				// Delete APs
				AccessPoint::model()->deleteAllByAttributes(array('access_point_group_id'=>$group->access_point_group_id));
				
				// Delete record
				AccessPointGroup::model()->deleteByPk($group->access_point_group_id);
				
				// Log
				$this->log($group->access_point_group_id);
				
				Yii::app()->user->setFlash('success', 'Delete successful!');
			}
			else {
				Yii::app()->user->setFlash('fail', 'Error in deletion.');
			}
			$this->redirect(array("$this->id/"));
		}
		$this->render('delete', array('group'=>$group));
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object AccessPointGroup instance
	 * @return void
	 */
	private function _renderForm(AccessPointGroup $object)
	{
		$fields = array();
		
		$portals = Portal::model()->findAll(array('order'=>'name ASC'));
		$fields['portal_id']['type'] = 'select';
		$fields['portal_id']['class'] = 'col-md-5';
		$fields['portal_id']['value'] = CHtml::listData($portals, 'portal_id', 'name');
		
		$fields['name']['type'] = 'text';
		$fields['name']['class'] = 'col-md-5';
		
		$fields['logon_role']['type'] = 'text';
		$fields['logon_role']['class'] = 'col-md-5';
		
		$fields['fb_role']['type'] = 'text';
		$fields['fb_role']['class'] = 'col-md-5';
		
		$fields['auth_role']['type'] = 'text';
		$fields['auth_role']['class'] = 'col-md-5';
		
		$fields['is_active']['type'] = 'radio';
		$fields['is_active']['class'] = 'col-lg-5';
		$fields['is_active']['value'] = array('1'=>'Enabled', '0'=>'Disabled');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object' => $object, 'fields' => $fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object AccessPointGroup instance
	 * @return	void
	 */
	private function _postValidate(AccessPointGroup $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('AccessPointGroup');
		$object->attributes = $post;

		if ($object->validate() && $object->save()) {
			$this->log($object->{$object->tableSchema->primaryKey});
			self::displayFormSuccess();
		}
		self::displayFormError($object);
	}
}