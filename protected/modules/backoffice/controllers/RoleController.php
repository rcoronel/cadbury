<?php

class roleController extends BackofficeController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action))
		{
			// action buttons
			$this->actionButtons['index'] = array('add' => array('link' => $this->createURL($this->id.'/add')));
			$this->actionButtons['add'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back' => array('link' => $this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		// fields to be displayed
		$row_fields = array();
		$row_fields['role']['type'] = 'text';
		$row_fields['role']['class'] = 'col-md-7';
		
		$row_fields['is_active']['type'] = 'select';
		$row_fields['is_active']['class'] = 'text-center col-md-1';
		$row_fields['is_active']['value'] = array('1'=>'Enabled', '0' =>'Disabled');
		$row_fields['is_active']['mode'] = 'bool';
		
		$row_actions = array('edit', 'delete');
		$object = new AdminRole();
		$list = self::_showList($object, array('fields'=>$row_fields, 'actions'=>$row_actions));
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$list['view'], 'pagination'=>$list['pagination'], 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		$role = new AdminRole;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($role);
		}
		
		self::_renderForm($role);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id AdminRole primary key
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$role = AdminRole::model()->findByPk($id);
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($role);
		}
		
		self::_renderForm($role);
	}
	
	/**
	 * Delete record
	 * 
	 * @access public
	 * @param int $id AdminRole ID
	 * @return void
	 */
	public function actionDelete($id)
	{
		$role = AdminRole::model()->findByPk($id);
		AdminRole::validateObject($role, 'AdminRole');
		
		if (Yii::app()->request->isPostRequest) {
			// Count admin that has this role
			$count = Admin::model()->count("admin_role_id={$role->admin_role_id}");
			if ($count) {
				Yii::app()->user->setFlash('fail', 'Cannot delete role. There is an admin assigned to the role.');
				$this->redirect(array("$this->id/"));
			}
			else {
				// check for security token
				if (CPasswordHelper::verifyPassword($role->admin_role_id, Yii::app()->request->getPost('security_key'))) {

					// Delete record
					AdminRole::model()->deleteByPk($role->admin_role_id);

					$this->log($role->{$role->tableSchema->primaryKey});
					Yii::app()->user->setFlash('success', 'Delete successful!');
					$this->redirect(array("$this->id/"));
				}
				else {
					Yii::app()->user->setFlash('fail', 'Error in deletion.');
					$this->redirect(array("$this->id/"));
				}
			}
		}
		$this->render('delete', array('role'=>$role));
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object AdminRole instance
	 * @return void
	 */
	private function _renderForm(AdminRole $object)
	{
		$fields = array();
		
		$fields['role']['type'] = 'text';
		$fields['role']['class'] = 'col-md-5';
		
		$fields['is_active']['type'] = 'radio';
		$fields['is_active']['class'] = 'col-lg-5';
		$fields['is_active']['value'] = array('1'=>'Enabled', '0'=>'Disabled');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object' => $object, 'fields' => $fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object AdminRole instance
	 * @return	void
	 */
	private function _postValidate(AdminRole $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('AdminRole');
		$object->attributes = $post;

		if ($object->validate() && $object->save()) {
			$this->log($object->{$object->tableSchema->primaryKey});
			self::displayFormSuccess();
		}
		self::displayFormError($object);
	}
}