<?php

class AvailmentController extends BackofficeController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action)) {
			// action buttons
			$this->actionButtons['index'] = array('add' => array('link' => $this->createURL($this->id.'/add')));
			$this->actionButtons['add'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back' => array('link' => $this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		// fields to be displayed
		$row_fields = array();
		$row_fields['availment_id']['type'] = 'text';
		$row_fields['availment_id']['class'] = 'text-center col-md-1';
		
		$row_fields['name']['type'] = 'text';
		$row_fields['name']['class'] = 'col-md-3';
		
		$row_fields['code']['type'] = 'text';
		$row_fields['code']['class'] = 'col-md-4';
		
		$row_fields['is_active']['type'] = 'select';
		$row_fields['is_active']['class'] = 'text-center col-md-1';
		$row_fields['is_active']['value'] = array('1'=>'Active', '0' =>'Inactive');
		$row_fields['is_active']['mode'] = 'bool';
		
		$row_actions = array('edit', 'delete');
		
		$object = new Availment();
		$list = self::_showList($object, array('fields'=>$row_fields, 'actions'=>$row_actions));
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$list['view'], 'pagination'=>$list['pagination'], 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		$client = new Availment;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($client);
		}
		
		self::_renderForm($client);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id Availment ID
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$client = Availment::model()->findByPk($id);
		Availment::validateObject($client, 'Availment');
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($client);
		}
		
		self::_renderForm($client);
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object Availment instance
	 * @return void
	 */
	private function _renderForm(Availment $object)
	{
		$fields = array();
		
		$fields['name']['type'] = 'text';
		$fields['name']['class'] = 'col-md-5';
		
		$fields['code']['type'] = 'text';
		$fields['code']['class'] = 'col-md-5';
		
		$fields['model']['type'] = 'text';
		$fields['model']['class'] = 'col-md-5';
		
		$fields['description']['type'] = 'textarea';
		$fields['description']['class'] = 'col-md-5';
		
		$fields['is_active']['type'] = 'radio';
		$fields['is_active']['class'] = 'col-md-5';
		$fields['is_active']['value'] = array('1' => 'Active', '0' => 'Inactive');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object' => $object, 'fields' => $fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object Availment instance
	 * @return	void
	 */
	private function _postValidate(Availment $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('Availment');
		$object->attributes = $post;

		// Validate object
		if ($object->validate() && $object->save())
		{
			$this->log($object->{$object->tableSchema->primaryKey});
			self::displayFormSuccess();
		}
		self::displayFormError($object);
	}
}