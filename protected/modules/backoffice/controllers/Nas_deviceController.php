<?php

class Nas_deviceController extends BackofficeController
{
	public $nas;
	
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action))
		{
			// action buttons
			$this->actionButtons['index'] = array('add' => array('link' => $this->createURL($this->id.'/add')));
			$this->actionButtons['view'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['add'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back' => array('link' => $this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		// fields to be displayed
		$row_fields = array();
		$row_fields['id']['type'] = 'text';
		$row_fields['id']['class'] = 'text-center col-md-1';
		
		$row_fields['shortname']['type'] = 'text';
		$row_fields['shortname']['class'] = 'col-md-2';
		
		$row_fields['nasname']['type'] = 'text';
		$row_fields['nasname']['class'] = 'col-md-2';
		
		$row_fields['switchip']['type'] = 'text';
		$row_fields['switchip']['class'] = 'col-md-2';
		
		$row_fields['switchip']['type'] = 'text';
		$row_fields['switchip']['class'] = 'col-md-2';
		
		$row_fields['brand']['type'] = 'select';
		$row_fields['brand']['class'] = 'col-md-2';
		$row_fields['brand']['value'] = array('aruba'=>'Aruba Networks', 'cisco'=>'Cisco Systems', 'fujitsu'=>'Fujitsu', 'hp'=>'HP');
		
		$row_actions = array('edit', 'delete');
		
		$object = new RadiusNas();
		$list = self::_showList($object, array('fields'=>$row_fields, 'actions'=>$row_actions), 25, TRUE);
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$list['view'], 'pagination'=>$list['pagination'], 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
	}
	
	/**
	 * View NAS details
	 * 
	 * @access public
	 * @param int $id NAS ID
	 * @return void
	 */
	public function actionView($id)
	{
		// declare scripts
		Yii::app()->clientScript->registerScriptFile(Link::js_url('bootstrap/bootstrap-datepicker.js'), CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/tocify/jquery.tocify.min.js'), CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/ckeditor/ckeditor.js'));
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/jquery.toastr.min.js'));
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/morris/morris.min.js'), CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/raphael/raphael-min.js'), CClientScript::POS_END);
		
		$this->nas = RadiusNas::model()->findByPk($id);
		RadiusNas::validateObject($this->nas, 'RadiusNas');
		
		$this->render('view', array('nas'=>$this->nas));
	}
	
	public function actionShow()
	{
		// Check if from AJAX request
		if ( ! Yii::app()->request->isAjaxRequest) {
			$this->redirect(array('nas_device/'));
		}
		
		$form = Yii::app()->request->getPost('form');
		$this->nas = RadiusNas::model()->findByPk(Yii::app()->request->getPost('id'));
		
		switch ($form) {
			case 'details':
				self::_showDetails();
			break;
		
			case 'appearance':
				self::_showAppearance();
			break;
		
			case 'terms':
				self::_showTerms();
			break;
		
			case 'scenarios':
				self::_showScenarios();
			break;
		
			case 'stats':
				self::_showStats();
			break;
		}
	}
	
	/**
	 * Show NAS device details
	 * 
	 * @access private
	 * @return JSON
	 */
	private function _showDetails()
	{
		RadiusNas::validateObject($this->nas, 'RadiusNas');
		$view = $this->renderPartial('details', array('nas'=>$this->nas), TRUE);
		die(CJSON::encode(array('status'=>1, 'message'=>$view)));
	}
	
	/**
	 * Show appearance form
	 * 
	 * @access private
	 * @return JSON
	 */
	private function _showAppearance()
	{
		// Check for permissions
		if ( ! $this->permissions->Edit) {
			die(CJSON::encode(array('status'=>1, 'message'=>$this->renderPartial('/dashboard/deny', array(), TRUE))));
			
		}
		
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/ckeditor/ckeditor.js'));
		
		$data = array(	'site_title' => NasConfiguration::getValue('SITE_TITLE'),
						'welcome' => NasConfiguration::getValue('WELCOME_BACK'),
						'redirect_url' => NasConfiguration::getValue('REDIRECT_URL'),
						'logo' => NasConfiguration::getvalue('LOGO'),
						'inside_logo' => NasConfiguration::getvalue('INSIDE_LOGO'),
						'center_image' => NasConfiguration::getValue('CENTER_IMAGE'),
						'favicon' => NasConfiguration::getvalue('FAVICON'),
						'splash' => NasConfiguration::getvalue('SPLASH_IMG'),
						'powered_welcome' => NasConfiguration::getvalue('POWERED_WELCOME'),
						'powered_inside' => NasConfiguration::getvalue('POWERED_INSIDE'),
						'css_file' => NasConfiguration::getValue('CSS_FILE'),
						'css_contents' => @file_get_contents(dirname(Yii::app()->request->scriptFile).'/css/client/'.$this->nas->id.'/'.NasConfiguration::getValue('CSS_FILE')));
	
		$view = $this->renderPartial('appearance', $data, TRUE);
		die(CJSON::encode(array('status'=>1, 'message'=>$view)));
	}
	
	/**
	 * Show Terms of Service
	 * 
	 * @access private
	 * @return JSON
	 */
	private function _showTerms()
	{
		// Check for permissions
		if ( ! $this->permissions->Edit) {
			die(CJSON::encode(array('status'=>1, 'message'=>$this->renderPartial('/dashboard/deny', array(), TRUE))));
			
		}
		
		$terms = Cms::model()->findByAttributes(array('Nas_ID'=>$this->nas->id, 'Friendly_URL'=>'terms-and-conditions'));
		$agreement = Cms::model()->findByAttributes(array('Nas_ID'=>$this->nas->id, 'Friendly_URL'=>'service-agreement'));
		
		// handle empty terms
		if (empty($terms)) {
			$terms = new Cms();
		}
		
		// handle empty agreement
		if (empty($agreement)) {
			$agreement = new Cms;
		}
		
		$view = $this->renderPartial('terms', array('terms'=>$terms, 'agreement'=>$agreement, 'nas'=>$this->nas), TRUE);
		die(CJSON::encode(array('status'=>1, 'message'=>$view)));
	}
	
	public function actionPost_terms()
	{
		// Check if from AJAX request
		if ( ! Yii::app()->request->isAjaxRequest) {
			$this->redirect(array('nas_device/'));
		}
		
		$this->nas = RadiusNas::model()->findByPk(Yii::app()->request->getPost('id'));
		
		// Service agreement
		$agreement = Cms::model()->findByAttributes(array('Nas_ID'=>$this->nas->id, 'Friendly_URL'=>'service-agreement'));
		if (empty($agreement)) {
			$agreement = new Cms();
		}
		$agreement->Title = 'Service Agreement';
		$agreement->Friendly_URL = 'service-agreement';
		$agreement->Nas_ID = $this->nas->id;
		$agreement->Content = Yii::app()->request->getPost('service-agreement');
		$agreement->validate() && $agreement->save();
		
		// Terms and Conditions
		$terms = Cms::model()->findByAttributes(array('Nas_ID'=>$this->nas->id, 'Friendly_URL'=>'terms-and-conditions'));
		if (empty($agreement)) {
			$terms = new Cms();
		}
		$terms->Title = 'Terms and Conditions';
		$terms->Friendly_URL = 'terms-and-conditions';
		$terms->Nas_ID = $this->nas->id;
		$terms->Content = Yii::app()->request->getPost('terms-and-conditions');
		$terms->validate() && $terms->save();
		
		die(CJSON::encode(array('status'=>1, 'message'=>'Record successfully saved')));
	}
	
	private function _showScenarios()
	{
		// Check for permissions
		if ( ! $this->permissions->Edit) {
			die(CJSON::encode(array('status'=>1, 'message'=>$this->renderPartial('/dashboard/deny', array(), TRUE))));
			
		}
		
		$scenarios = Scenario::model()->with('new', 'returning')->findAll(array('condition'=>"Nas_ID = {$this->nas->id}", 'order'=>'new.Level ASC, returning.Level ASC'));
		$view = $this->renderPartial('scenario', array('scenarios'=>$scenarios), TRUE);
		die(CJSON::encode(array('status'=>1, 'message'=>$view)));
	}
	
	/**
	 * Show stats form
	 * 
	 * @access private
	 * @return JSON
	 */
	private function _showStats()
	{
		$view = $this->renderPartial('stats', array('nas'=>$this->nas), TRUE);
		die(CJSON::encode(array('status'=>1, 'message'=>$view)));
	}
	
	public function actionPost_stats($export = FALSE)
	{
		// Check if from AJAX request
		if ( ! Yii::app()->request->isAjaxRequest) {
			$this->redirect(array('nas_device/'));
		}
		
		self::_generateStats(Yii::app()->request->getPost('id'), Yii::app()->request->getPost('from'), Yii::app()->request->getPost('to'));
	}
	
	public function actionExport_stats($nas_id, $from, $to)
	{
		self::_generateStats($nas_id, $from, $to, TRUE);
	}
	
	private function _generateStats($nas_id, $from_date, $to_date, $export = FALSE)
	{
		$from = date('Y-m-d', strtotime($from_date));
		$to = date('Y-m-d', strtotime($to_date));
		$this->nas = RadiusNas::model()->findByPk($nas_id);
		
		// availment count
		$availments = Availment::model()->findAll();
		foreach ($availments as $a) {
			$model = $a->Model;
			$availment_count[] = array('label'=>$a->Name, 'value'=>$model::poll($from, $to, $nas_id));
		}
		
		// gender count
		// male
		$criteria = new CDbCriteria();
		$criteria->addBetweenCondition('DATE(Created_at)', $from, $to);
		$criteria->compare('Gender', 'male');
		$criteria->compare('Nas_ID', $this->nas->id);
		$male_count = AvailFacebook::model()->count($criteria);
		
		// female
		$criteria = new CDbCriteria();
		$criteria->addBetweenCondition('DATE(Created_at)', $from, $to);
		$criteria->compare('Gender', 'female');
		$criteria->compare('Nas_ID', $this->nas->id);
		$female_count = AvailFacebook::model()->count($criteria);
		
		// unknown
		$criteria = new CDbCriteria();
		$criteria->addBetweenCondition('DATE(Created_at)', $from, $to);
		$criteria->addCondition("Gender = ''");
		$criteria->compare('Nas_ID', $this->nas->id);
		$unknown_count = AvailFacebook::model()->count($criteria);
		
		$gender_count[] = array('label'=>'Male', 'value'=>$male_count);
		$gender_count[] = array('label'=>'Female', 'value'=>$female_count);
		$gender_count[] = array('label'=>'Unknown', 'value'=>$unknown_count);
		
		// Get count by telco
		$telcos = Yii::app()->db_radius->createCommand("SELECT pl.brand_name, COUNT(DISTINCT d.Mac_address) AS count
		FROM Cadbury_Captive.avail_mobile AS m
		JOIN Cadbury_Captive.device AS d ON m.Device_ID = d.Device_ID
		JOIN Cadbury_Captive.plan_prefix AS pr ON SUBSTRING(m.Mobile_number,2,5) = LEFT(pr.prefix,5)
		JOIN Cadbury_Captive.plan AS pl ON pr.plan_id = pl.plan_id
		WHERE Nas_ID = {$this->nas->id} AND m.Is_Validated = 1 AND DATE(m.Created_at) BETWEEN '$from' AND '$to'
		GROUP BY pl.brand_name;")->queryAll();
		
		// Session duration
		$session_time = Yii::app()->db_radius->createCommand("SELECT SUM(acctsessiontime) as sum
		FROM radacct 
		WHERE nasipaddress = '{$this->nas->switchip}' AND username <> 'fb_guest' AND DATE(acctstarttime) BETWEEN '{$from}' AND '{$to}'")->queryScalar();
		$session_time = (int)$session_time / 3600; # convert to hours
		
		// Upload
		$upload = Yii::app()->db_radius->createCommand("SELECT SUM(acctinputoctets) as input
		FROM radacct
		WHERE nasipaddress = '{$this->nas->switchip}' AND DATE(acctstarttime) BETWEEN '{$from}' AND '{$to}'")->queryScalar();
		$upload = number_format((int)$upload/1024/1024/1024, 2);
		
		// Download
		$download = Yii::app()->db_radius->createCommand("SELECT SUM(acctoutputoctets) as input
		FROM radacct
		WHERE nasipaddress = '{$this->nas->switchip}' AND DATE(acctstarttime) BETWEEN '{$from}' AND '{$to}'")->queryScalar();
		$download = number_format((int)$download/1024/1024/1024, 2);
		
		$all_users = 0;
		$new_users = 0;
		$return_users = 0;
		$all_auth_users = 0;
		$new_auth_users = 0;
		$return_auth_users = 0;
		$log_success = 0;
		$log_fail = 0;
		while (strtotime($from) <= strtotime($to)) {
			// Connected users
			$all_users += DeviceConnection::pollAll($from, $nas_id);
			$new_users += DeviceConnection::pollAllNew($from, $nas_id);
			$return_users += DeviceConnection::pollAllReturning($from, $nas_id);
			
			// Authenticated users
			$all_auth_users += DeviceConnection::pollAuthAll($from, $nas_id);
			$new_auth_users += DeviceConnection::pollAuthNew($from, $nas_id);
			$return_auth_users += DeviceConnection::pollAuthReturning($from, $nas_id);
			
			$log_success += ArubaLog::pollSuccess($from, $nas_id);
			$log_fail += ArubaLog::pollFail($from, $nas_id);
			
			$from = date('Y-m-d', strtotime($from . "+1 days"));
		}
		
		if ($export) {
			$csv = 'report.csv';
			$f = fopen($csv, 'w');
			fputcsv($f, array($this->nas->shortname));
			fputcsv($f, array(date('F d, Y', strtotime($from_date)).' - '.date('F d, Y', strtotime($to_date))));
			fputcsv($f, array());
			fputcsv($f, array('STATS', 'COUNT'));
			fputcsv($f, array('Total Visits', ''));
			fputcsv($f, array('Total Unique Visits', $all_users));
			fputcsv($f, array('Total Unique Authenticated Visits', $all_auth_users));
			fputcsv($f, array('Unique New Authenticated Visits', $new_auth_users));
			fputcsv($f, array('Unique Returning Authenticated Visits', $return_auth_users));
			fputcsv($f, array());
			fputcsv($f, array('Total Registrations', ''));
			foreach ($availment_count as $ac) {
				fputcsv($f, array($ac['label'], $ac['value']));	
			}
			if ( ! empty($telcos)) {
				fputcsv($f, array());
				fputcsv($f, array('Telco Count', ''));
				foreach ($telcos as $t) {
					fputcsv($f, array($t['brand_name'], $t['count']));
				}
			}
			fputcsv($f, array());
			fputcsv($f, array('Total Session Hours', $session_time));
			fputcsv($f, array());
			fputcsv($f, array('Total Input Volume in GB', $upload));
			fputcsv($f, array('Total Output Volume in GB', $download));
			fputcsv($f, array());
			fputcsv($f, array('Total Page Views', ''));
			fputcsv($f, array('Total Success Logs', $log_success));
			fputcsv($f, array('Total Error Logs', $log_fail));
			
			
			$data = file_get_contents($csv);
			Yii::app()->request->sendFile($csv, $data);
			unlink($csv);
			exit();
		}
		
		$view = $this->renderPartial('poll', array('all_users'=>$all_users,
			'new_users'=>$new_users,
			'return_users'=>$return_users,
			'all_auth_users'=>$all_auth_users,
			'new_auth_users'=>$new_auth_users,
			'return_auth_users'=>$return_auth_users,
			'from'=>Yii::app()->request->getPost('from'), 'to'=>Yii::app()->request->getPost('to')), TRUE);
		die(CJSON::encode(array('status'=>1, 'message'=>$view, 'chart_data'=>$availment_count, 'bar_data'=>$gender_count)));
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		$nas = new RadiusNas;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($nas);
		}
		
		self::_renderForm($nas);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id RadiusNas ID
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$nas = RadiusNas::model()->findByPk($id);
		RadiusNas::validateObject($nas, 'RadiusNas');
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($nas);
		}
		
		self::_renderForm($nas);
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object RadiusNas instance
	 * @return void
	 */
	private function _renderForm(RadiusNas $object)
	{
		$fields = array();
		
		$fields['shortname']['type'] = 'text';
		$fields['shortname']['class'] = 'col-md-5';
		
		$fields['nasname']['type'] = 'text';
		$fields['nasname']['class'] = 'col-md-5';
		
		$fields['switchip']['type'] = 'text';
		$fields['switchip']['class'] = 'col-md-5';
		
		$fields['secret']['type'] = 'text';
		$fields['secret']['class'] = 'col-md-5';
		
		$fields['brand']['type'] = 'select';
		$fields['brand']['class'] = 'col-md-5';
		$fields['brand']['value'] = array('aruba'=>'Aruba Networks', 'cisco'=>'Cisco Systems', 'fujitsu'=>'Fujitsu', 'hp'=>'HP');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object' => $object, 'fields' => $fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object RadiusNas instance
	 * @return	void
	 */
	private function _postValidate(RadiusNas $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('RadiusNas');
		$object->attributes = $post;

		if ($object->validate() && $object->save()) {
			$this->log($object->{$object->tableSchema->primaryKey});
			self::displayFormSuccess();
		}
		self::displayFormError($object);
	}
}