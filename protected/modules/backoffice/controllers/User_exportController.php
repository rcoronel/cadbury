<?php

class User_exportController extends BackofficeController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action)) {
			// action buttons
			$this->actionButtons['view'] = array('back' => array('link' => $this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		// declare scripts
		Yii::app()->clientScript->registerScriptFile(Link::js_url('bootstrap/bootstrap-datepicker.js'), CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/tocify/jquery.tocify.min.js'), CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/morris/morris.min.js'), CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/raphael/raphael-min.js'), CClientScript::POS_END);
		
		$availments = Availment::model()->findAll(array('order'=>'name ASC'));
		$portals = Portal::model()->findAll(array('order'=>'name ASC'));
		$this->render('index', array('availments'=>$availments, 'portals'=>$portals));
	}
	
	/**
	 * Count user data
	 * 
	 * @acces public
	 * @return JSON
	 */
	public function actionPoll()
	{
		if (Yii::app()->getRequest()->getPost('export')){
			self::_export();
		}
		// Check if from AJAX request
		if ( ! Yii::app()->request->isAjaxRequest) {
			$this->redirect(array('user/'));
		}
		
		$availment_id = Yii::app()->getRequest()->getPost('availment_id');
		$portal_id = Yii::app()->getRequest()->getPost('portal_id');
		$today = date('Ymd', strtotime(Yii::app()->request->getPost('date')));
		
		$count = DeviceAvailment::poll($today, NULL, $portal_id, NULL, NULL, $availment_id);
		
		$view = $this->renderPartial('poll', array('count'=>$count, 'availment_id'=>$availment_id, 'portal_id'=>$portal_id, 'date'=>$today), TRUE);
		die(CJSON::encode(array('status'=>1, 'message'=>$view)));
	}
	
	/**
	 * Export rows to CSV
	 * 
	 * @access private
	 * @return void
	 */
	private function _export()
	{
		$availment_id = Yii::app()->getRequest()->getPost('availment_id');
		$portal_id = Yii::app()->getRequest()->getPost('portal_id');
		$today = date('Y-m-d', strtotime(Yii::app()->request->getPost('date')));
		
		$availment = Availment::model()->findByPk($availment_id);
		$model = $availment->model;
		$object = new $model();
		$users = $object::model()->with('dev_avail')->findAll("availment_id = {$availment_id} AND portal_id = {$portal_id} AND DATE(t.created_at) = '{$today}'");
		
		$fields = array();
		foreach ($object->export_fields as $field) {
			$fields[] = $object->getAttributeLabel($field);
		}
		
		$handle = fopen('users.csv', 'w');
		fputcsv($handle, $fields);
		foreach ($users as $u) {
			$list = array();
			foreach ($object->export_fields as $field) {
				$list[] = $u->$field;
			}
			fputcsv($handle, $list);
		}
		fclose($handle);
		Yii::app()->getRequest()->sendFile('users.csv', file_get_contents('users.csv'));
	}
}