<?php

class AdminController extends BackofficeController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action)) {
			// action buttons
			$this->actionButtons['index'] = array('add' => array('link' => $this->createURL($this->id.'/add')));
			$this->actionButtons['view'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['add'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back' => array('link' => $this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		// fields to be displayed
		$row_fields = array();
		$row_fields['admin_id']['type'] = 'text';
		$row_fields['admin_id']['class'] = 'text-center col-md-1';
		
		$row_fields['email']['type'] = 'text';
		$row_fields['email']['class'] = 'col-md-2';
		
		$row_fields['firstname']['type'] = 'text';
		$row_fields['firstname']['class'] = 'col-md-2';
		
		$row_fields['lastname']['type'] = 'text';
		$row_fields['lastname']['class'] = 'col-md-2';
		
		$roles = AdminRole::model()->findAll();
		$row_fields['admin_role_id']['type'] = 'select';
		$row_fields['admin_role_id']['class'] = 'col-md-2';
		$row_fields['admin_role_id']['parent'] = 'role';
		$row_fields['admin_role_id']['child'] = 'role';
		$row_fields['admin_role_id']['value'] = CHtml::listData($roles, 'admin_role_id', 'role');
		
		$row_fields['is_active']['type'] = 'select';
		$row_fields['is_active']['class'] = 'text-center col-md-1';
		$row_fields['is_active']['value'] = array('1'=>'Can login', '0' =>'Cannot login');
		$row_fields['is_active']['mode'] = 'bool';
		
		$row_actions = array('edit', 'delete');
		$object = new Admin();
		$list = self::_showList($object, array('fields'=>$row_fields, 'actions'=>$row_actions));
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$list['view'], 'pagination'=>$list['pagination'], 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		$admin = new Admin;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($admin);
		}
		
		self::_renderForm($admin);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id Admin ID
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$admin = Admin::model()->findByPk($id);
		Admin::validateObject($admin, 'Admin');
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($admin);
		}
		
		self::_renderForm($admin);
	}
	
	/**
	 * Delete record
	 * 
	 * @access public
	 * @param int $id Admin ID
	 * @return void
	 */
	public function actionDelete($id)
	{
		$admin = Admin::model()->findByPk($id);
		Admin::validateObject($admin, 'Admin');
		
		if (Yii::app()->request->isPostRequest) {
			// check for security token
			if (CPasswordHelper::verifyPassword($admin->admin_id, Yii::app()->request->getPost('security_key'))) {
				
				// Check for super admin count
				$count = Admin::model()->count("admin_role_id = 1 AND admin_id <> {$admin->admin_id}");
				if ( ! $count) {
					Yii::app()->user->setFlash('fail', 'Cannot delete the remaining root user.');
					$this->redirect(array("$this->id/"));
				}
				
				// Delete record
				Admin::model()->deleteByPk($admin->admin_id);
				
				Yii::app()->user->setFlash('success', 'Delete successful!');
			}
			else {
				Yii::app()->user->setFlash('fail', 'Error in deletion.');
			}
			$this->redirect(array("$this->id/"));
		}
		$this->render('delete', array('admin'=>$admin));
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object Admin instance
	 * @return void
	 */
	private function _renderForm(Admin $object)
	{
		$fields = array();
		
		$roles = AdminRole::model()->findAll();
		$fields['admin_role_id']['type'] = 'select';
		$fields['admin_role_id']['class'] = 'col-md-5';
		$fields['admin_role_id']['value'] = CHtml::listData($roles, 'admin_role_id', 'role');
		
		$fields['email']['type'] = 'text';
		$fields['email']['class'] = 'col-md-5';
		
		$fields['password']['type'] = 'password';
		$fields['password']['class'] = 'col-md-5';
		
		$fields['confirm_password']['type'] = 'password';
		$fields['confirm_password']['class'] = 'col-md-5';
		
		$fields['firstname']['type'] = 'text';
		$fields['firstname']['class'] = 'col-md-5';
		
		$fields['lastname']['type'] = 'text';
		$fields['lastname']['class'] = 'col-md-5';
		
		$fields['is_active']['type'] = 'radio';
		$fields['is_active']['class'] = 'col-md-5';
		$fields['is_active']['value'] = array('1' => 'Can login', '0' => 'Cannot login');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object' => $object, 'fields' => $fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object Admin instance
	 * @return	void
	 */
	private function _postValidate(Admin $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('Admin');
		$object->attributes = $post;

		if ($object->validate()) {
			$object->password = CPasswordHelper::hashpassword($object->password);
			$object->confirm_password = $object->password;
			if (empty($object->image)) {
				$object->image = 'default.jpg';
			}
			if ($object->save()) {
				$this->log($object->{$object->tableSchema->primaryKey});
				self::displayFormSuccess();
			}
		}
		self::displayFormError($object);
	}
}