<?php

class AnswerController extends BackofficeController
{
	public $question;
	public $answer;
	
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action))
		{
			// action buttons
			switch ($action->id) {
				case 'index':
				case 'add':
					// Initialize SurveyQuestion object
					$this->question = SurveyQuestion::model()->findByPk(Yii::app()->request->getParam('id'));
					SurveyQuestion::validateObject($this->question, 'SurveyQuestion', $this->createURL('portal/'));
					
					$this->actionButtons['index'] = array(
						'back'=>array('link'=>$this->createURL('question/', array('id'=>$this->question->survey_id))),
						'add'=>array('link'=>$this->createURL('answer/add', array('id'=>$this->question->survey_question_id))),
					);
					$this->actionButtons['add'] = array(
						'back'=>array('link'=>$this->createURL('answer/', array('id'=>$this->question->survey_question_id))),
					);
				break;
			
				case 'update':
					// Initialize SurveyAnswer object
					$this->answer = SurveyAnswer::model()->findByPk(Yii::app()->request->getParam('id'));
					SurveyAnswer::validateObject($this->answer, 'SurveyAnswer', $this->createURL('portal/'));
					
					$this->actionButtons['update'] = array('back'=>array('link'=>$this->createURL('answer/', array('id'=>$this->answer->survey_question_id))));
				break;
			}
			
			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @param int $id Question ID
	 * @return	void
	 */
	public function actionIndex($id)
	{
		// declare scripts
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'plugins', 'jquery.tablednd.js')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/jquery.toastr.min.js'));
		
		// fields to be displayed
		$row_fields = array();
		$row_fields['survey_answer_id']['type'] = 'text';
		$row_fields['survey_answer_id']['class'] = 'text-center col-md-1';
		
		$row_fields['answer']['type'] = 'text';
		$row_fields['answer']['class'] = 'col-md-4';
		
		$row_fields['answer_type']['type'] = 'select';
		$row_fields['answer_type']['class'] = 'col-md-2';
		$row_fields['answer_type']['value'] = array('text'=>'Textbox', 'dropdown'=>'Dropdown list', 'checkbox'=>'Checkbox', 'radio'=>'Radio button');
		
		$row_fields['position']['type'] = 'text';
		$row_fields['position']['class'] = 'text-center col-md-1 dragHandle';
		
		$row_actions = array('edit', 'delete');
		
		$object = new SurveyAnswer();
		$list = self::_showList($object, array('fields'=>$row_fields, 'actions'=>$row_actions), 25, FALSE, array('survey_question_id'=>$this->question->survey_question_id));
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$list['view'], 'pagination'=>$list['pagination'], 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @param int $id Question ID
	 * @return void
	 */
	public function actionAdd($id)
	{
		$answer = new SurveyAnswer;
		$answer->survey_question_id = $this->question->survey_question_id;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($answer);
		}
		
		self::_renderForm($answer);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id Answer ID
	 * @return void
	 */
	public function actionUpdate($id)
	{
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($this->answer);
		}
		
		self::_renderForm($this->answer);
	}
	
	/**
	 * Update menu position
	 * 
	 * @access active
	 * @return string
	 */
	public function actionPosition()
	{
		$query = Yii::app()->request->getQuery('page-list');
		
		$answer = new SurveyAnswer;
		foreach ($query as $position => $id) {
			$answer->updateByPk($id, array('position'=>(int)$position + 1));
		}
		
		die('Repositioning successful.');
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object SurveyAnswer instance
	 * @return void
	 */
	private function _renderForm(SurveyAnswer $object)
	{
		$fields = array();
		
		$fields['answer']['type'] = 'text';
		$fields['answer']['class'] = 'col-md-6';
		
		$fields['answer_type']['type'] = 'radio';
		$fields['answer_type']['class'] = 'col-lg-5';
		$fields['answer_type']['value'] = array('text'=>'Textbox', 'dropdown'=>'Dropdown list', 'checkbox'=>'Checkbox', 'radio'=>'Radio button');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object'=>$object, 'fields'=>$fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object SurveyAnswer instance
	 * @return	void
	 */
	private function _postValidate(SurveyAnswer $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('SurveyAnswer');
		
		$object->attributes = $post;
		if ( ! $object->position) {
			$position = SurveyAnswer::findMaxPosition($object->survey_question_id);
			$object->position = $position+1;
		}
		
		if ($object->validate() && $object->save()) {
			self::displayFormSuccess('Record successfully saved!', $this->createUrl('answer/', array('id'=>$object->survey_question_id)));
		}
		self::displayFormError($object);
	}
}