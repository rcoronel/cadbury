<?php

class SurveyController extends BackofficeController
{
	public $portal;
	public $survey;
	
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action)){
			
			switch ($action->id) {
				case 'index':
				case 'add':
					// Initialize Portal object
					$this->portal = Portal::model()->findByPk(Yii::app()->request->getParam('id'));
					Portal::validateObject($this->portal, 'Portal', $this->createURL('portal/'));
					$this->actionButtons['index'] = array('add'=>array('link'=>$this->createURL('survey/add', array('id'=>$this->portal->portal_id))),
						'back'=>array('link'=>$this->createURL('portal_availment/update', array('id'=>$this->portal->portal_id))));
					$this->actionButtons['add'] = array('back'=>array('link'=>$this->createURL('survey/', array('id'=>$this->portal->portal_id))));
				break;
			
				case 'update':
				case 'view':
					// Initialize Survey object
					$this->survey = Survey::model()->findByPk(Yii::app()->request->getQuery('id'));
					Survey::validateObject($this->survey, 'Survey', $this->createURL('portal/'));
					$this->actionButtons['update'] = array('back'=>array('link'=>$this->createURL('survey/', array('id'=>$this->survey->portal_id))));
				break;
			}

			return true;
		}
	}
	
	/**
	 * View surveys
	 * 
	 * @access public
	 * @param type $id
	 * @return void
	 */
	public function actionIndex($id)
	{
		// declare scripts
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'plugins', 'jquery.tablednd.js')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/jquery.toastr.min.js'));
		
		// fields to be displayed
		$row_fields = array();
		$row_fields['survey_id']['type'] = 'text';
		$row_fields['survey_id']['class'] = 'text-center col-md-1';
		
		$row_fields['title']['type'] = 'text';
		$row_fields['title']['class'] = 'col-md-5';
		
		$row_fields['is_active']['type'] = 'select';
		$row_fields['is_active']['class'] = 'text-center col-md-1';
		$row_fields['is_active']['value'] = array('1'=>'Enabled', '0' =>'Disabled');
		$row_fields['is_active']['mode'] = 'bool';
		
		$row_fields['position']['type'] = 'text';
		$row_fields['position']['class'] = 'text-center col-md-1 dragHandle';
		
		$row_actions = array('view', 'edit', 'delete');
		
		$object = new Survey();
		$list = self::_showList($object, array('fields'=>$row_fields, 'actions'=>$row_actions), 25, FALSE, array('portal_id'=>$id));
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$list['view'], 'pagination'=>$list['pagination'], 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd($id)
	{
		$survey = new Survey;
		$survey->portal_id = $this->portal->portal_id;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($survey);
		}
		
		self::_renderForm($survey);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionUpdate($id)
	{
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($this->survey);
		}
		
		self::_renderForm($this->survey);
	}
	/**
	 * View questions
	 * 
	 * @access public
	 * @param int $id Survey ID
	 * @return void
	 */
	public function actionView($id)
	{
		$url = $this->createUrl('question/', array('id'=>$this->survey->survey_id));
		$this->redirect($url);
	}
	
	/**
	 * Update menu position
	 * 
	 * @access active
	 * @return string
	 */
	public function actionPosition()
	{
		$query = Yii::app()->request->getQuery('page-list');
		
		$survey = new Survey;
		foreach ($query as $position => $id) {
			$survey->updateByPk($id, array('position'=>(int)$position + 1));
		}
		
		die('Repositioning successful.');
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object Survey instance
	 * @return void
	 */
	private function _renderForm(Survey $object)
	{
		$fields = array();
		
		$fields['title']['type'] = 'text';
		$fields['title']['class'] = 'col-md-6';
		
		$fields['is_active']['type'] = 'radio';
		$fields['is_active']['class'] = 'col-lg-5';
		$fields['is_active']['value'] = array('1'=>'Enabled', '0'=>'Disabled');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object'=>$object, 'fields'=>$fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object Survey instance
	 * @return	void
	 */
	private function _postValidate(Survey $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('Survey');
		$object->attributes = $post;
		$object->token = Yii::app()->getSecurityManager()->generateRandomString(128);
		if ( ! $object->position) {
			$position = Survey::findMaxPosition($object->portal_id);
			$object->position = $position+1;
		}
		
		if ($object->validate() && $object->save()) {
			self::displayFormSuccess('Record successfully saved!', $this->createUrl('survey/', array('id'=>$object->portal_id)));
		}
		self::displayFormError($object);
	}
}