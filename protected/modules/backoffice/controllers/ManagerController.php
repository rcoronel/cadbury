<?php

class ManagerController extends BackofficeController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action))
		{
			// action buttons
			$this->actionButtons['index'] = array('add' => array('link' => $this->createURL($this->id.'/add')));
			$this->actionButtons['add'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back' => array('link' => $this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		// fields to be displayed
		$row_fields = array();
		$row_fields['manager_id']['type'] = 'text';
		$row_fields['manager_id']['class'] = 'text-center col-md-1';
		
		$nass = RadiusNas::model()->findAll();
		$row_fields['nas_id']['type'] = 'select';
		$row_fields['nas_id']['class'] = 'col-md-2';
		$row_fields['nas_id']['value'] = CHtml::listData($nass, 'id', 'shortname');
		$row_fields['nas_id']['parent'] = 'nas';
		$row_fields['nas_id']['child'] = 'shortname';
		
		$row_fields['email']['type'] = 'text';
		$row_fields['email']['class'] = 'col-mid-2';
		
		$row_fields['firstname']['type'] = 'text';
		$row_fields['firstname']['class'] = 'col-mid-1';
		
		$row_fields['lastname']['type'] = 'text';
		$row_fields['lastname']['class'] = 'col-mid-1';
		
		$row_actions = array('edit', 'delete');
		
		$managers = Manager::model()->with('nas')->findAll();
		$this->render('/layouts/list', array('object' => new Manager, 'objectList'=>$managers, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		$manager = new Manager;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($manager);
		}
		
		self::_renderForm($manager);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id Manager ID
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$manager = Manager::model()->findByPk($id);
		Manager::validateObject($manager, 'Manager');
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($manager);
		}
		
		self::_renderForm($manager);
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object Manager instance
	 * @return void
	 */
	private function _renderForm(Manager $object)
	{
		$fields = array();
		
		$nass = RadiusNas::model()->findAll();
		$fields['nas_id']['type'] = 'select';
		$fields['nas_id']['class'] = 'col-md-5';
		$fields['nas_id']['class'] = 'col-md-5';
		$fields['nas_id']['value'] = CHtml::listData($nass, 'id', 'shortname');
		
		$fields['email']['type'] = 'text';
		$fields['email']['class'] = 'col-md-5';
		
		$fields['password']['type'] = 'password';
		$fields['password']['class'] = 'col-md-5';
		
		$fields['confirm_password']['type'] = 'password';
		$fields['confirm_password']['class'] = 'col-md-5';
		
		$fields['firstname']['type'] = 'text';
		$fields['firstname']['class'] = 'col-md-5';
		
		$fields['lastname']['type'] = 'text';
		$fields['lastname']['class'] = 'col-md-5';
		
		$fields['is_active']['type'] = 'radio';
		$fields['is_active']['class'] = 'col-md-5';
		$fields['is_active']['value'] = array('1' => 'Can login', '0' => 'Cannot login');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object' => $object, 'fields' => $fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object Manager instance
	 * @return	void
	 */
	private function _postValidate(Manager $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('Manager');
		$object->attributes = $post;

		// Validate object
		if ($object->validate())
		{
			// Forcing confirm_password to be the same as password
			// We do this in order to avoid redundant error on save
			$object->password = CPasswordHelper::hashPassword($object->password);
			$object->confirm_password = $object->password;
			if ($object->save()) {
				self::displayFormSuccess();
			}
		}
		self::displayFormError($object);
	}
}