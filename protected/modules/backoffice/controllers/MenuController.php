<?php

class MenuController extends BackofficeController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action)) {
			// action buttons
			$this->actionButtons['index'] = array('add' => array('link' => $this->createURL($this->id.'/add')));
			$this->actionButtons['view'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['add'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back' => array('link' => $this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		// declare scripts
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'plugins', 'jquery.tablednd.js')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/jquery.toastr.min.js'));
		
		// fields to be displayed
		$row_fields = array();
		$row_fields['title']['type'] = 'text';
		$row_fields['title']['class'] = 'col-mid-4';
		
		$row_fields['display']['type'] = 'select';
		$row_fields['display']['class'] = 'text-center col-md-1';
		$row_fields['display']['value'] = array('1' => 'Enabled', '0' => 'Disabled');
		$row_fields['display']['mode'] = 'bool';
		
		$row_fields['position']['type'] = 'text';
		$row_fields['position']['class'] = 'text-center col-md-1 dragHandle';
		
		$row_actions = array('view', 'edit', 'delete');
		
		$object = new AdminMenu();
		$list = self::_showList($object, array('fields'=>$row_fields, 'actions'=>$row_actions), 25, FALSE, array('parent_admin_menu_id'=>0));
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$list['view'], 'pagination'=>$list['pagination'], 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
	}
	
	/**
	 * View records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionView($id)
	{
		// declare scripts
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'plugins', 'jquery.tablednd.js')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/jquery.toastr.min.js'));
		
		// fields to be displayed
		$row_fields = array();
		$row_fields['admin_menu_id']['type'] = 'text';
		$row_fields['admin_menu_id']['class'] = 'text-center col-md-1';
		
		$row_fields['title']['type'] = 'text';
		$row_fields['title']['class'] = 'col-mid-4';
		
		$row_fields['display']['type'] = 'select';
		$row_fields['display']['class'] = 'text-center col-md-1';
		$row_fields['display']['value'] = array('1' => 'Enabled', '0' => 'Disabled');
		$row_fields['display']['mode'] = 'bool';
		
		$row_fields['position']['type'] = 'text';
		$row_fields['position']['class'] = 'text-center col-md-1 dragHandle';
		
		$row_actions = array('view', 'edit', 'delete');
		$object = new AdminMenu();
		$list = self::_showList($object, array('fields'=>$row_fields, 'actions'=>$row_actions), 25, FALSE, array('parent_admin_menu_id'=>$id));
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$list['view'], 'pagination'=>$list['pagination'], 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		$menu = new AdminMenu;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($menu);
		}
		
		self::_renderForm($menu);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id AdminMenu primary key
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$menu = AdminMenu::model()->findByPk($id);
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($menu);
		}
		
		self::_renderForm($menu);
	}
	
	/**
	 * Delete record
	 * 
	 * @access public
	 * @param int $id AdminMenu ID
	 * @return void
	 */
	public function actionDelete($id)
	{
		$menu = AdminMenu::model()->findByPk($id);
		AdminMenu::validateObject($menu, 'AdminMenu');
		
		if (Yii::app()->request->isPostRequest) {
			// check for security token
			if (CPasswordHelper::verifyPassword($menu->admin_menu_id, Yii::app()->request->getPost('security_key'))) {
				// Delete permissions
				AdminRolePermission::model()->deleteAllByAttributes(array('admin_menu_id'=>$menu->admin_menu_id));
				
				// Delete record
				AdminMenu::model()->deleteByPk($menu->admin_menu_id);
				
				Yii::app()->getModule('backoffice')->admin->setFlash('success', 'Delete successful!');
				$this->redirect(array("$this->id/"));
			}
			else {
				Yii::app()->getModule('backoffice')->admin->setFlash('fail', 'Error in deletion.');
				$this->redirect(array("$this->id/"));
			}
		}
		$this->render('delete', array('menu'=>$menu));
	}
	
	/**
	 * Update menu position
	 * 
	 * @access active
	 * @return string
	 */
	public function actionPosition()
	{
		$query = Yii::app()->request->getQuery('page-list');
		
		$menu = new AdminMenu;
		foreach ($query as $position => $id) {
			$menu->updateByPk($id, array('position' => (int)$position + 1));
		}
		$this->log($menu->{$menu->tableSchema->primaryKey});
		die('Repositioning successful.');
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object AdminMenu instance
	 * @return void
	 */
	private function _renderForm(AdminMenu $object)
	{
		$fields = array();
		
		$parent_menus = AdminMenu::model()->findAllByAttributes(array('parent_admin_menu_id' => 0, 'display'=>1), array('order'=>'position ASC'));
		$fields['parent_admin_menu_id']['type'] = 'select';
		$fields['parent_admin_menu_id']['class'] = 'col-lg-5';
		$fields['parent_admin_menu_id']['value'] = array('Home') + CHtml::listData($parent_menus, 'admin_menu_id', 'title');
		
		$fields['title']['type'] = 'text';
		$fields['title']['class'] = 'col-md-5';
		
		$fields['classname']['type'] = 'text';
		$fields['classname']['class'] = 'col-md-5';
		
		$fields['img']['type'] = 'text';
		$fields['img']['class'] = 'col-lg-5';
		
		$fields['display']['type'] = 'radio';
		$fields['display']['class'] = 'col-lg-5';
		$fields['display']['value'] = array('1' => 'Enabled', '0' => 'Disabled');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object' => $object, 'fields' => $fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object AdminMenu instance
	 * @return	void
	 */
	private function _postValidate(AdminMenu $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('AdminMenu');
		$object->attributes = $post;

		if ($object->validate() && $object->save()) {
			// add all access to super admin
			$permission = new AdminRolePermission;
			$permission->deleteAllByAttributes(array('admin_menu_id' => $object->{$object->tableSchema->primaryKey}));
			$permission->admin_menu_id = $object->{$object->tableSchema->primaryKey};
			$permission->admin_role_id = 1;
			$permission->view = 1;
			$permission->add = 1;
			$permission->edit = 1;
			$permission->delete = 1;
			$permission->save();

			$this->log($object->{$object->tableSchema->primaryKey});
			self::displayFormSuccess();
		}
		else {
			self::displayFormError($object);
		}
	}
}