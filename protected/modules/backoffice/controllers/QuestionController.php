<?php

class QuestionController extends BackofficeController
{
	public $survey;
	public $question;
	
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action)){
			// action buttons
			switch ($action->id) {
				case 'index':
				case 'add':
					// Initialize Survey object
					$this->survey = Survey::model()->findByPk(Yii::app()->request->getParam('id'));
					Survey::validateObject($this->survey, 'Survey', $this->createURL('portal/'));
					
					$this->actionButtons['index'] = array(
						'back'=>array('link'=>$this->createURL('survey/', array('id'=>$this->survey->portal_id))),
						'add'=>array('link'=>$this->createURL($this->id.'/add', array('id'=>$this->survey->survey_id))),
					);
					$this->actionButtons['add'] = array(
						'back'=>array('link'=>$this->createURL('question/', array('id'=>$this->survey->survey_id)))
					);
				break;
			
				case 'update':
				case 'view':
					// Initialize SurveyQuestion object
					$this->question = SurveyQuestion::model()->findByPk(Yii::app()->request->getParam('id'));
					SurveyQuestion::validateObject($this->question, 'SurveyQuestion', $this->createURL('portal/'));
					
					$this->actionButtons['update'] = array('back'=>array('link'=>$this->createURL('question/', array('id'=>$this->question->survey_id))));
				break;
			}
			
			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @param int $id Survey ID
	 * @return	void
	 */
	public function actionIndex($id)
	{
		// declare scripts
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'plugins', 'jquery.tablednd.js')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/jquery.toastr.min.js'));
		
		// fields to be displayed
		$row_fields = array();
		$row_fields['survey_question_id']['type'] = 'text';
		$row_fields['survey_question_id']['class'] = 'text-center col-md-1';
		
		$row_fields['question']['type'] = 'text';
		$row_fields['question']['class'] = 'col-md-3';
		
		$row_fields['is_required']['type'] = 'select';
		$row_fields['is_required']['class'] = 'text-center col-md-2';
		$row_fields['is_required']['value'] = array('1'=>'Required', '0' =>'Not required');
		$row_fields['is_required']['mode'] = 'bool';
		
		$row_fields['is_active']['type'] = 'select';
		$row_fields['is_active']['class'] = 'text-center col-md-1';
		$row_fields['is_active']['value'] = array('1'=>'Enabled', '0' =>'Disabled');
		$row_fields['is_active']['mode'] = 'bool';
		
		$row_fields['position']['type'] = 'text';
		$row_fields['position']['class'] = 'text-center col-md-1 dragHandle';
		
		$row_actions = array('view', 'edit', 'delete');
		
		$object = new SurveyQuestion();
		$list = self::_showList($object, array('fields'=>$row_fields, 'actions'=>$row_actions), 25, FALSE, array('survey_id'=>$this->survey->survey_id));
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$list['view'], 'pagination'=>$list['pagination'], 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
	}
	
	/**
	 * View answers for the specified question
	 * 
	 * @access public
	 * @param int $id Question ID
	 * @return void
	 */
	public function actionView($id)
	{
		$url = $this->createUrl('answer/', array('id'=>$this->question->survey_question_id));
		$this->redirect($url);
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @param int $id
	 * @return void
	 */
	public function actionAdd($id)
	{
		$question = new SurveyQuestion;
		$question->survey_id = $this->survey->survey_id;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($question);
		}
		
		self::_renderForm($question);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id Question ID
	 * @return void
	 */
	public function actionUpdate($id)
	{
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($this->question);
		}
		
		self::_renderForm($this->question);
	}
	
	/**
	 * Update menu position
	 * 
	 * @access active
	 * @return string
	 */
	public function actionPosition()
	{
		$query = Yii::app()->request->getQuery('page-list');
		
		$question = new SurveyQuestion;
		foreach ($query as $position => $id) {
			$question->updateByPk($id, array('position'=>(int)$position + 1));
		}
		
		die('Repositioning successful.');
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object SurveyQuestion instance
	 * @return void
	 */
	private function _renderForm(SurveyQuestion $object)
	{
		$fields = array();
		
		$fields['question']['type'] = 'text';
		$fields['question']['class'] = 'col-md-6';
		
		$fields['is_required']['type'] = 'radio';
		$fields['is_required']['class'] = 'col-lg-5';
		$fields['is_required']['value'] = array('1'=>'Required', '0'=>'Not required');
		
		$fields['is_active']['type'] = 'radio';
		$fields['is_active']['class'] = 'col-lg-5';
		$fields['is_active']['value'] = array('1'=>'Enabled', '0'=>'Disabled');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object'=>$object, 'fields'=>$fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object SurveyQuestion instance
	 * @return	void
	 */
	private function _postValidate(SurveyQuestion $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('SurveyQuestion');
		$object->attributes = $post;
		$object->token = Yii::app()->getSecurityManager()->generateRandomString(128);
		if ( ! $object->position) {
			$position = SurveyQuestion::findMaxPosition($object->survey_id);
			$object->position = $position+1;
		}

		if ($object->validate() && $object->save()) {
			self::displayFormSuccess('Record successfully saved!', $this->createUrl('question/', array('id'=>$object->survey_id)));
		}
		self::displayFormError($object);
	}
	
}