<?php

class Avail_settingController extends BackofficeController
{
	public $portal;
	
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action)) {
			// Initializa Portal object
			$id = Yii::app()->request->getParam('id');
			$this->portal = Portal::model()->findByPk($id);
			Portal::validateObject($this->portal, 'Portal', $this->createURL('portal/'));
			
			// action buttons
			$this->actionButtons['index'] = array('back'=>array('link'=>$this->createURL('portal/')));
			$this->actionButtons['add'] = array('back'=>array('link'=>$this->createURL($this->id.'/')));
			
			$this->actionButtons['view'] = array('back'=>array('link'=>$this->createURL($this->id.'/')));
			
			switch ($action->id) {
				case 'update':
					$this->actionButtons['update'] = array('back'=>array('link'=>$this->createURL('portal_availment/update', array('id'=>$this->portal->portal_id))));
				break;
			}

			return true;
		}
	}
	
	public function actionUpdate($id, $availment_id)
	{
		$availment = Availment::model()->findByPk($availment_id);
		Availment::validateObject($availment, 'Availment');
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($availment);
		}
		
		// Check for survey
		if ($availment->model == 'AvailSurvey') {
			$this->redirect(array('survey/', 'id'=>$this->portal->portal_id));
		}
		
		$this->render($availment->availment_id, array('object'=>$availment));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object Availment instance
	 * @return	void
	 */
	private function _postValidate(Availment $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('AvailmentSetting');
		foreach ($post as $config=>$value) {
			AvailmentSetting::setValue($object->availment_id, $config, $value);
		}
		self::displayFormSuccess('Settings saved!', $this->createUrl('portal_availment/update', array('id'=>$this->portal->portal_id)));
	}
}