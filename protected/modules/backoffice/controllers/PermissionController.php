<?php

class PermissionController extends BackofficeController
{
	public function actionIndex()
	{
		$this->redirect(array('permission/update', 'id' => 1));
	}
	
	public function actionUpdate($id)
	{
		if (Yii::app()->request->isPostRequest) {
			self::_handleRequest();
		}
		$role = AdminRole::model()->findByPk($id);
		AdminRole::validateObject($role, 'AdminRole');
		
		$roles = AdminRole::model()->findAll();
		
		// menus
		$menus = AdminMenu::model()->findAll(array('order' => 'Position ASC'));
		$permissions = AdminRolePermission::model()->findAll("admin_role_id = {$role->admin_role_id}");
		
		foreach ($menus as $m) {
			foreach ($permissions as $p) {
				if ($m->admin_menu_id == $p->admin_menu_id) {
					$m->view = $p->view;
					$m->add = $p->add;
					$m->edit = $p->edit;
					$m->delete = $p->delete;
				}
			}
		}
		
		$this->render('index', array('roles' => $roles, 'role'=>$role, 'menus'=>$menus));
	}
	
	private function _handleRequest()
	{
		if (Yii::app()->request->getPost('admin_role_id')) {
			$post = Yii::app()->request->getPost('admin_menu');
			foreach ($post as $p) {
				$permission = new AdminRolePermission;
				
				// delete existing permissions
				$permission->deleteAllByAttributes(array('admin_role_id'=>Yii::app()->request->getPost('admin_role_id'), 'admin_menu_id'=>$p['admin_menu_id']));
				
				$permission->admin_role_id = Yii::app()->request->getPost('admin_role_id');
				$permission->admin_menu_id = $p['admin_menu_id'];
				$permission->view = (isset($p['view'])) ? $p['view'] : 0;
				$permission->add = (isset($p['add'])) ? $p['add'] : 0;
				$permission->edit = (isset($p['edit'])) ? $p['edit'] : 0;
				$permission->delete = (isset($p['delete'])) ? $p['delete'] : 0;
				$permission->save();
			}
			
			$this->log(Yii::app()->request->getPost('admin_role_id'));
			Yii::app()->user->setFlash('success', 'Permissions updated');
			$this->redirect(array("$this->id/update", 'id'=>Yii::app()->request->getPost('admin_role_id')));
		}
	}
}