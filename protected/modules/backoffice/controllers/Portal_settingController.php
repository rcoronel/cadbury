<?php

class Portal_settingController extends BackofficeController
{
	public $portal;
	
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action)) {
			// action buttons
			$this->actionButtons['index'] = array('back'=>array('link'=>$this->createURL('portal/')));
			
			// Initializa Portal object
			$id = Yii::app()->request->getParam('id');
			$this->portal = Portal::model()->findByPk($id);
			Portal::validateObject($this->portal, 'Portal', $this->createURL('portal/'));
			
			switch ($action->id) {
				case 'update':
					$this->actionButtons['update'] = array('back'=>array('link'=>$this->createURL('portal_setting/', array('id'=>$this->portal->portal_id))));
				break;
			}

			return true;
		}
	}
	
	/**
	 * View portal details
	 * 
	 * @access public
	 * @return void
	 */
	public function actionIndex()
	{
		// declare scripts
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'plugins', 'jquery.tablednd.js')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/jquery.toastr.min.js'));
		
		$count_all = DeviceConnection::pollAll(date('Ymd'), $this->portal->site_id, $this->portal->portal_id);
		$count_auth = DeviceAvailment::poll(date('Ymd'), $this->portal->site_id, $this->portal->portal_id);
		
		// CMS
		$cms = Cms::model()->findAllByAttributes(array('portal_id'=>$this->portal->portal_id));
	
		$availments = PortalAvailment::model()->with('availment')->findAllByAttributes(array('portal_id'=>$this->portal->portal_id));
		
		$this->render('index', array('portal'=>$this->portal,
			'count_all'=>$count_all, 'count_auth'=>$count_auth,
			'cms'=>$cms, 'availments'=>$availments,
			'logo'=>PortalConfiguration::getvalue('LOGO'),
			'inside_logo'=>PortalConfiguration::getvalue('INSIDE_LOGO'),
			'center_image'=>PortalConfiguration::getValue('CENTER_IMAGE'),
			'favicon'=>PortalConfiguration::getvalue('FAVICON'),
			'splash'=>PortalConfiguration::getvalue('SPLASH_IMG'),
			'powered_welcome'=>PortalConfiguration::getvalue('POWERED_WELCOME'),
			'powered_inside'=>PortalConfiguration::getvalue('POWERED_INSIDE'),
			'css_file'=>PortalConfiguration::getValue('CSS_FILE'),
			'css_contents'=>@file_get_contents(dirname(Yii::app()->request->scriptFile).'/css/portal/'.$this->portal->portal_id.'/'.PortalConfiguration::getValue('CSS_FILE'))));
	}
	
	/**
	 * Update portal details
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionUpdate()
	{
		if (Yii::app()->request->isPostRequest) {
			self::_handlePost();
		}
		
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/ckeditor/ckeditor.js'));
		
		$this->render('view', array('portal'=>$this->portal,
			'logo'=>PortalConfiguration::getvalue('LOGO'),
			'inside_logo'=>PortalConfiguration::getvalue('INSIDE_LOGO'),
			'center_image'=>PortalConfiguration::getValue('CENTER_IMAGE'),
			'favicon'=>PortalConfiguration::getvalue('FAVICON'),
			'splash'=>PortalConfiguration::getvalue('SPLASH_IMG'),
			'powered_welcome'=>PortalConfiguration::getvalue('POWERED_WELCOME'),
			'powered_inside'=>PortalConfiguration::getvalue('POWERED_INSIDE'),
			'css_file'=>PortalConfiguration::getValue('CSS_FILE'),
			'css_contents'=>@file_get_contents(dirname(Yii::app()->request->scriptFile).'/css/portal/'.$this->portal->portal_id.'/'.PortalConfiguration::getValue('CSS_FILE')),
			'other_settings'=>$this->renderPartial('iconic', array(), TRUE)
			));
	}
	
	/**
	 * View the captive portal
	 * 
	 * @access public
	 * @param int $id Portal ID
	 * @return void
	 */
	public function actionView($id)
	{
		// Random AC
		$ac = new RadiusNas();
		$criteria = $ac->search()->criteria;
		$criteria->order = 'RAND()';
		$criteria->params = array();
		$criteria->condition = '';
		$random_ac = RadiusNas::model()->find($criteria);
		
		// Random Device
		$device = new Device();
		$criteria = $device->search()->criteria;
		$criteria->order = 'RAND()';
		$random_device = Device::model()->find($criteria);
		
		// Random IP Address
		$random_ip = long2ip(mt_rand());
		
		$group = new PortalSsidGroup();
		$criteria = $group->search()->criteria;
		$criteria->params = array();
		$criteria->condition = '';
		$criteria->compare('portal_id', $this->portal->portal_id);
		$criteria->order = 'RAND()';
		$random_group = PortalSsidGroup::model()->find($criteria);
		
		// Random SSID
		$random_ssid = ServiceSet::model()->findByPk($random_group->service_set_id);
		
		// Random AP Group
		$random_apg = AccessPointGroup::model()->findByPk($random_group->access_point_group_id);
		
		// Random AP
		$ap = new AccessPoint();
		$criteria = $ap->search()->criteria;
		$criteria->compare('access_point_group_id', $random_apg->access_point_group_id);
		$criteria->order = 'RAND()';
		$random_ap = AccessPoint::model()->find($criteria);
		
		// Build URL
		$base_url = Link::base_url();
		$query = http_build_query(array('r'=>'control_box/index',
			'cmd'=>'login',
			'switchip'=>$random_ac->switchip,
			'mac'=>$random_device->mac_address,
			'ip'=>$random_ip,
			'essid'=>$random_ssid->ssid,
			'apname'=>$random_ap->name,
			'apgroup'=>$random_apg->name,
			'url'=>'https://www.google.com'));
		$url = "{$base_url}?{$query}";
		
		$this->redirect($url);
	}
	
	/**
	 * Delete appearance settings and files
	 * 
	 * @access public
	 * @param string $config
	 * @return void
	 */
	public function actionDelete($id, $config)
	{
		switch ($config) {
			case 'LOGO':
			case 'INSIDE_LOGO':
			case 'CENTER_IMAGE':
			case 'FAVICON':
			case 'SPLASH_IMG':
				@unlink(dirname(Yii::app()->request->scriptFile).'/images/portal/'.$this->portal->portal_id.'/'.PortalConfiguration::getValue($config));
			break;

			case 'CSS_FILE':
				@unlink(dirname(Yii::app()->request->scriptFile).'/css/portal/'.$this->portal->portal_id.'/'.PortalConfiguration::getValue($config));
			break;
		}
		PortalConfiguration::setValue($config, '');
		
		Yii::app()->user->setFlash('success', 'Settings applied');
		$this->redirect(array('portal_setting/update', 'id'=>$id));
	}
	
	/**
	 * Set configuration value
	 * 
	 * @access private
	 * @return void
	 */
	private function _handlePost()
	{
		$post = Yii::app()->request->getPost('PortalConfiguration');

		// create CSS
		if ( ! empty($post['CSS_FILE'])) {
			$post['CSS_FILE'] = self::_uploadStyle($post['CSS_FILE']);
		}
		
		foreach ($post as $config=>$value) {
			PortalConfiguration::setValue($config, $value);
		}
		
		if ( ! empty($_FILES)) {
			self::_upload($_FILES);
		}
			
		Yii::app()->user->setFlash('success', 'Settings applied');
	}
	
	/**
	 * Upload images based on configuration
	 * 
	 * @access	private
	 * @return	void
	 */
	private function _upload($files)
	{
		foreach ($files as $name=>$attr) {
			if ($attr['size'] && ! $attr['error']) {
				switch ($name) {
					case 'LOGO':
						$filename = 'logo';
						self::_uploadImage($name, $filename);
					break;
				
					case 'INSIDE_LOGO':
						$filename = 'inside_logo';
						self::_uploadImage($name, $filename);
					break;
				
					case 'CENTER_IMAGE':
						$filename = 'center_image';
						self::_uploadImage($name, $filename);
					break;
				
					case 'FAVICON':
						$filename = 'favicon';
						self::_uploadImage($name, $filename);
					break;
				
					case 'SPLASH_IMG':
						$filename = 'splash';
						self::_uploadImage($name, $filename);
					break;
				}
			}
		}
	}
	
	private function _uploadImage($name, $filename)
	{
		$image = CUploadedFile::getInstanceByName($name);
		$filename = $filename.'.'.$image->extensionName;
		
		// Check for S3 config
		if (Configuration::getValue('S3_STATUS')) {
			$success = Yii::app()->s3->upload($image_path.$filename , "assets/images/portal/{$this->portal->portal_id}/{$filename}", Configuration::getValue('S3_BUCKET'));
			if ($success) {
				PortalConfiguration::setvalue($name, Configuration::getValue('S3_BUCKET_URL')."assets/images/portal/{$this->portal->portal_id}/{$filename}");
			}
		}
		else {
			$image_path = dirname(Yii::app()->request->scriptFile).'/images/portal/'.$this->portal->portal_id.'/';
			if ( ! is_dir($image_path)) {
				$old_umask = umask(0); 
				mkdir($image_path, 0777, true);
				umask($old_umask); 
			}
			
			copy(dirname(Yii::app()->request->scriptFile).'/forbidden.html', $image_path.'index.html');

			// delete current file
			$previous_file = PortalConfiguration::getValue($name);
			@unlink($image_path.$previous_file);

			// check for successful save
			if ($image->saveAs($image_path.$filename)) {
				PortalConfiguration::setvalue($name, $filename);
			}
		}
	}
	
	private function _uploadStyle($css)
	{
		$css_path = dirname(Yii::app()->request->scriptFile).'/css/portal/'.$this->portal->portal_id.'/';
		if ( ! is_dir($css_path)) {
			mkdir($css_path, 0777, true);
		}
		
		$css_file = fopen($css_path.'styles.css', 'w');
		fwrite($css_file, $css);
		fclose($css_file);
		
		// Check for S3 config
		if (Configuration::getValue('S3_STATUS')) {
			$success = Yii::app()->s3->upload($css_path.'styles.css', "assets/css/portal/{$this->portal->portal_id}/styles.css", Configuration::getValue('S3_BUCKET'));
			if ($success) {
				return Configuration::getValue('S3_BUCKET_URL')."assets/css/portal/{$this->portal->portal_id}/styles.css";
			}
		}
		
		return 'styles.css';
	}
}