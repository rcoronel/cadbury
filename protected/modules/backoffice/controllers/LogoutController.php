<?php

class LogoutController extends BackofficeController
{	
	public function actionIndex()
	{
		Yii::app()->getModule('backoffice')->admin->logout();
		$this->redirect(array('login/'));
	}
}