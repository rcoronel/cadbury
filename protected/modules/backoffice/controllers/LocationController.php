<?php

class LocationController extends BackofficeController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action))
		{
			// action buttons
			$this->actionButtons['index'] = array('add' => array('link' => $this->createURL($this->id.'/add')));
			$this->actionButtons['add'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back' => array('link' => $this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		// fields to be displayed
		$row_fields = array();
		$row_fields['location_id']['type'] = 'text';
		$row_fields['location_id']['class'] = 'text-center col-md-1';
		
		$row_fields['name']['type'] = 'text';
		$row_fields['name']['class'] = 'col-mid-2';
		
		$row_fields['type']['type'] = 'select';
		$row_fields['type']['class'] = 'col-mid-1';
		$row_fields['type']['value'] = array('country'=>'Country', 'region'=>'Region', 'province'=>'Province', 'city'=>'City', 'municipality'=>'Municipality', 'barangay'=>'Barangay', 'establishment'=>'Establishment');
		
		$row_actions = array('edit', 'delete');
		
		$object = new Location();
		$list = self::_showList($object, $row_fields, $row_actions);
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$list, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
	}
	
	/**
	 * Display list
	 * 
	 * @param object $object Location instance
	 * @param array $row_fields
	 * @param array $row_actions
	 * @return mixed
	 */
	private function _showList(Location $object, $row_fields, $row_actions)
	{
		$object->attributes = Location::setSessionAttributes(Yii::app()->request->getPost('Location'));
		$criteria = $object->search()->criteria;
		$count = Location::model()->count($criteria);
		
		$pages = new CPagination($count);
		$criteria->limit = 25;
		$criteria->offset = (Yii::app()->request->getQuery('page', 1) * $criteria->limit) - $criteria->limit;
		
		$pages->pageSize=$criteria->limit;
		$pages->applyLimit($criteria);
		
		$list = Location::model()->findAll($criteria);
		$view = $this->renderPartial('/layouts/list-body', array('object'=>$object, 'objectList'=>$list, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions), TRUE);
		if (Yii::app()->request->isAjaxRequest) {
			die(CJSON::encode(array('status'=>1, 'message'=>$view)));
		}
		return $view;
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		$location = new Location;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($location);
		}
		
		self::_renderForm($location);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$location = Location::model()->findByPk($id);
		Location::validateObject($location, 'Location');
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($location);
		}
		
		self::_renderForm($location);
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object Location instance
	 * @return void
	 */
	private function _renderForm(Location $object)
	{
		$portals = Portal::model()->findAll();
		
		$this->render('form', array('object' => $object, 'portals'=>$portals));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object Location instance
	 * @return	void
	 */
	private function _postValidate(Location $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('Location');
		$object->attributes = $post;

		if ($object->validate())
		{
			if ($object->save()) {
				$this->log($object->{$object->tableSchema->primaryKey});
				Correlor::addLocation($object->location_id);
				self::displayFormSuccess();
			}
		}
		else {
			self::displayFormError($object);
		}
	}
	
	public function actionParent()
	{
		$post = Yii::app()->request->getPost('Location');
		
		switch ($post['type']) {
			case 'region':
				$parents = Location::model()->findAll("type = 'country'");
				$parent_type = 'country';
			break;
			case 'province':
				$parents = Location::model()->findAll("type = 'region'");
				$parent_type = 'region';
			break;
			case 'city':
				$parents = Location::model()->findAll("type = 'province'");
				$parent_type = 'province';
			break;
			case 'municipality':
				$parents = Location::model()->findAll("type = 'province'");
				$parent_type = 'province';
			break;
			case 'barangay':
				$parents = Location::model()->findAll("type = 'city'");
				$parent_type = 'city';
			break;
			case 'establishment':
				$parents = Location::model()->findAll("type = 'barangay'");
				$parent_type = 'barangay';
			break;
		}
		
		$view = $this->renderPartial('parent', array('object'=>new Location, 'parents'=>$parents, 'type'=>$parent_type), TRUE);
		die(CJSON::encode(array('status'=>1, 'message'=>$view)));
	}
}