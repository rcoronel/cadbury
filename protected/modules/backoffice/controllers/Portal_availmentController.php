<?php

class Portal_availmentController extends BackofficeController
{
	public $portal;
	
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action)) {
			// Initializa Portal object
			$id = Yii::app()->request->getParam('id');
			$this->portal = Portal::model()->findByPk($id);
			
			Portal::validateObject($this->portal, 'Portal', $this->createURL('portal/'));
			
			// action buttons
			$this->actionButtons['index'] = array('back'=>array('link'=>$this->createURL('portal/')));
			$this->actionButtons['add'] = array('back'=>array('link'=>$this->createURL($this->id.'/')));
			
			$this->actionButtons['view'] = array('back'=>array('link'=>$this->createURL($this->id.'/')));
			
			switch ($action->id) {
				case 'update':
					$this->actionButtons['update'] = array('back'=>array('link'=>$this->createURL('portal_setting/', array('id'=>$this->portal->portal_id))));
				break;
			}

			return true;
		}
	}
	
	/**
	 * Update availments for the specified portal
	 * 
	 * @access public
	 * @param int $id Portal ID
	 * @return void
	 */
	public function actionUpdate($id)
	{
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/jquery.toastr.min.js'));
		
		if (Yii::app()->request->isPostRequest) {
			self::_validatePost($this->portal);
		}
		
		$availments = Availment::model()->findAll("Is_active = 1 ORDER BY Name");
		$portal_avails = PortalAvailment::model()->findAllByAttributes(array('portal_id'=>$this->portal->portal_id));
		foreach ($availments as $a) {
			$a->is_available = 0;
			$a->duration = 0;
			if ( ! empty($portal_avails)) {
				foreach ($portal_avails as $pa) {
					if ($pa->availment_id == $a->availment_id) {
						$a->is_available = 1;
						$a->duration = $pa->duration / 60;
						$a->is_recurring = $pa->is_recurring;
						$a->description = $pa->description;
					}
				}
			}
		}
		$scenarios = Scenario::model()->with('new', 'returning')->findAll(array('condition'=>"portal_id = {$this->portal->portal_id}", 'order'=>'new.level ASC, returning.level ASC'));
		$this->render('index', array('availments'=>$availments, 'scenarios'=>$scenarios));
	}
	
	/**
	 * Validate the post
	 * 
	 * @access private
	 * @param object $portal Portal instance
	 * @return string
	 */
	private function _validatePost(Portal $portal)
	{
		sleep(1);
		$post = Yii::app()->request->getPost('status');
		PortalAvailment::model()->deleteAllByAttributes(array('portal_id'=>$portal->portal_id));
		foreach ($post as $id=>$availability) {
			if ($availability) {
				$nas_avail = new PortalAvailment;
				$nas_avail->portal_id = $portal->portal_id;
				$nas_avail->availment_id = $id;
				$nas_avail->duration = (int)($_POST['duration'][$id] * 60);
				$nas_avail->is_recurring = (int)$_POST['recurring'][$id];
				$nas_avail->description = $_POST['description'][$id];
				$nas_avail->validate() && $nas_avail->save();
			}
		}
		
		die(CJSON::encode(array('status'=>1, 'message'=>'Settings saved')));
	}

}