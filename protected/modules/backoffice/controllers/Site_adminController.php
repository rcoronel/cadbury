<?php

class Site_adminController extends BackofficeController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action)) {
			// action buttons
			$this->actionButtons['index'] = array('add' => array('link' => $this->createURL($this->id.'/add')));
			$this->actionButtons['add'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		// fields to be displayed
		$row_fields = array();
		$row_fields['site_admin_id']['type'] = 'text';
		$row_fields['site_admin_id']['class'] = 'text-center col-md-1';
		
		$sites = Site::model()->findAll(array('order'=>'name ASC'));
		$row_fields['site_id']['type'] = 'select';
		$row_fields['site_id']['class'] = 'col-md-2';
		$row_fields['site_id']['parent'] = 'site';
		$row_fields['site_id']['child'] = 'name';
		$row_fields['site_id']['value'] = CHtml::listData($sites, 'site_id', 'name');
		
		$row_fields['email']['type'] = 'text';
		$row_fields['email']['class'] = 'col-mid-2';
		
		$row_fields['firstname']['type'] = 'text';
		$row_fields['firstname']['class'] = 'col-mid-1';
		
		$row_fields['lastname']['type'] = 'text';
		$row_fields['lastname']['class'] = 'col-mid-1';
		
		$row_fields['is_active']['type'] = 'select';
		$row_fields['is_active']['class'] = 'text-center col-md-1';
		$row_fields['is_active']['value'] = array('1'=>'Can login', '0' =>'Cannot login');
		$row_fields['is_active']['mode'] = 'bool';
		
		$row_actions = array('edit', 'delete');
		
		$object = new SiteAdmin();
		$list = self::_showList($object, $row_fields, $row_actions);
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$list, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
	}
	
	/**
	 * Display list
	 * 
	 * @param object $object SiteAdmin instance
	 * @param array $row_fields
	 * @param array $row_actions
	 * @return mixed
	 */
	private function _showList(SiteAdmin $object, $row_fields, $row_actions)
	{
		$object->attributes = SiteAdmin::setSessionAttributes(Yii::app()->request->getPost('SiteAdmin'));
		$criteria = $object->search()->criteria;
		$count = SiteAdmin::model()->count($criteria);
		
		$pages = new CPagination($count);
		$criteria->limit = 25;
		$criteria->offset = (Yii::app()->request->getQuery('page', 1) * $criteria->limit) - $criteria->limit;
		
		$pages->pageSize=$criteria->limit;
		$pages->applyLimit($criteria);
		
		$list = SiteAdmin::model()->findAll($criteria);
		$view = $this->renderPartial('/layouts/list-body', array('object'=>$object, 'objectList'=>$list, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions), TRUE);
		if (Yii::app()->request->isAjaxRequest) {
			die(CJSON::encode(array('status'=>1, 'message'=>$view)));
		}
		return $view;
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		$client = new SiteAdmin;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($client);
		}
		
		self::_renderForm($client);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id SiteAdmin ID
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$client = SiteAdmin::model()->findByPk($id);
		SiteAdmin::validateObject($client, 'SiteAdmin');
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($client);
		}
		
		self::_renderForm($client);
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object SiteAdmin instance
	 * @return void
	 */
	private function _renderForm(SiteAdmin $object)
	{
		$fields = array();
		
		$sites = Site::model()->findAll(array('order'=>'name ASC'));
		$fields['site_id']['type'] = 'select';
		$fields['site_id']['class'] = 'col-md-5';
		$fields['site_id']['value'] = CHtml::listData($sites, 'site_id', 'name');
		
		$fields['email']['type'] = 'text';
		$fields['email']['class'] = 'col-md-5';
		
		$fields['password']['type'] = 'password';
		$fields['password']['class'] = 'col-md-5';
		
		$fields['confirm_password']['type'] = 'password';
		$fields['confirm_password']['class'] = 'col-md-5';
		
		$fields['firstname']['type'] = 'text';
		$fields['firstname']['class'] = 'col-md-5';
		
		$fields['lastname']['type'] = 'text';
		$fields['lastname']['class'] = 'col-md-5';
		
		$fields['is_active']['type'] = 'radio';
		$fields['is_active']['class'] = 'col-md-5';
		$fields['is_active']['value'] = array('1' => 'Can login', '0' => 'Cannot login');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object' => $object, 'fields' => $fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object SiteAdmin instance
	 * @return	void
	 */
	private function _postValidate(SiteAdmin $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('SiteAdmin');
		$object->attributes = $post;
		$object->site_role_id = 1;
		
		// Validate object
		if ($object->validate()) {
			// Forcing Confirm_password to be the same as Password
			// We do this in order to avoid redundant error on save
			$object->password = CPasswordHelper::hashPassword($object->password);
			$object->confirm_password = $object->password;
			if ($object->save()) {
				$this->log($object->{$object->tableSchema->primaryKey});
				self::displayFormSuccess();
			}
		}
		self::displayFormError($object);
	}
}