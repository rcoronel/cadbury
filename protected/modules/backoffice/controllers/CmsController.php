<?php

class CmsController extends BackofficeController
{
	public $portal;
	
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action)){
			$id = Yii::app()->request->getQuery('id');
			switch ($action->id) {
				case 'add':
					$this->portal = Portal::model()->findByPk($id);
					Portal::validateObject($this->portal, 'Portal', $this->createURL('portal/'));
				break;
			
				case 'update':
				case 'delete':
					$cms = Cms::model()->findByPk($id);
					Cms::validateObject($cms, 'Cms', $this->createURL('portal/'));
					$this->actionButtons['update'] = array('back'=>array('link'=>$this->createURL('portal_setting/', array('id'=>$cms->portal_id))));
				break;
			}
			
			// action buttons
			$this->actionButtons['add'] = array('back'=>array('link'=>$this->createURL('portal_setting/', array('id'=>$id))));
			return true;
		}
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @param integer $id Portal ID
	 * @return void
	 */
	public function actionAdd($id)
	{
		$cms = new Cms;
		$cms->portal_id = $this->portal->portal_id;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($cms);
		}
		
		self::_renderForm($cms);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id Cms primary key
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$cms = Cms::model()->findByPk($id);
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($cms);
		}
		
		self::_renderForm($cms);
	}
	
	/**
	 * Delete record
	 * 
	 * @access public
	 * @param int $id Cms ID
	 * @return void
	 */
	public function actionDelete($id)
	{
		$cms = Cms::model()->findByPk($id);
		
		if (Yii::app()->request->isPostRequest) {
			// check for security token
			if (CPasswordHelper::verifyPassword($cms->cms_id, Yii::app()->request->getPost('security_key'))) {
				// Delete record
				Cms::model()->deleteByPk($cms->cms_id);
				Yii::app()->user->setFlash('success', 'Delete successful!');
			}
			else {
				Yii::app()->user->setFlash('fail', 'Error in deletion.');
			}
			$this->redirect(array('portal_setting/', 'id'=>$cms->portal_id));
		}
		$this->render('delete', array('cms'=>$cms));
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object Cms instance
	 * @return void
	 */
	private function _renderForm(Cms $object)
	{
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/ckeditor/ckeditor.js'));
		
		$fields = array();
		
		$fields['title']['type'] = 'text';
		$fields['title']['class'] = 'col-md-5';
		
		$fields['friendly_url']['type'] = 'text';
		$fields['friendly_url']['class'] = 'col-md-5';
		
		$fields['content']['type'] = 'wysiwyg';
		$fields['content']['class'] = 'col-lg-8';
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object'=>$object, 'fields'=>$fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object Cms instance
	 * @return	void
	 */
	private function _postValidate(Cms $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('Cms');
		$object->attributes = $post;
		
		if ($object->validate() && $object->save()) {
			self::displayFormSuccess('Record successfully saved!', $this->createUrl('portal_setting/', array('id'=>$object->portal_id)));
		}
		self::displayFormError($object);
	}
}