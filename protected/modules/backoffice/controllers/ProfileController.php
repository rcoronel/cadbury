<?php

class ProfileController extends BackofficeController
{
	/**
	 * Show admin profile based on the logged-in user
	 * 
	 * @access public
	 * @return void
	 */
	public function actionIndex()
	{
		// declare scripts
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/jquery.toastr.min.js'));
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/ajaxfileupload/jquery.ajaxfileupload.js'));
		
		$admin = $this->context->admin;
		$this->render('index', array('admin'=>$admin));
	}
	
	/**
	 * Update admin based on logged-in user
	 * 
	 * @access public
	 * @return json
	 */
	public function actionUpdate()
	{
		$admin = $this->context->admin;
		
		if (Yii::app()->request->isPostRequest) {
			self::_validatePost($admin);
		}
		
		die(CJSON::encode(array('status'=>1, 'message'=>$this->renderPartial('form', array('admin'=>$admin), TRUE))));
	}
	
	/**
	 * Change admin password
	 * 
	 * @access public
	 * @return void
	 */
	public function actionChange_password()
	{
		$admin = $this->context->admin;
		$admin->setScenario('change_password');
		
		if (Yii::app()->request->isPostRequest) {
			self::_validatepassword($admin);
		}
		
		die(CJSON::encode(array('status'=>1, 'message'=>$this->renderPartial('change_password', array('admin'=>$admin), TRUE))));
	}
	
	/**
	 * Update image
	 * 
	 * @access public
	 * @return void
	 */
	public function actionUpdate_image()
	{
		$admin = $this->context->admin;
		if (Yii::app()->request->isPostRequest) {
			self::_uploadImage($admin);
		}
		
		die(CJSON::encode(array('status'=>1, 'message'=>$this->renderPartial('update_image', array('admin'=>$admin), TRUE))));
	}
	
	/**
	 * Validate then update the admin
	 * 
	 * @access private
	 * @param Admin $admin
	 * @return json
	 */
	private function _validatePost(Admin $admin)
	{
		sleep(1);
		$post = Yii::app()->request->getPost('Admin');
		$admin->attributes = $post;
		
		if ($admin->validate())
		{
			if ($admin->save()) {
				self::displayFormSuccess();
			}
		}
		else {
			self::displayFormError($admin);
		}
	}

	/**
	 * Validate then change password
	 * 
	 * @access private
	 * @param Admin $admin
	 * @return json
	 */
	private function _validatepassword(Admin $admin)
	{
		sleep(1);
		$_password = $admin->password;
		$post = Yii::app()->request->getPost('Admin');
		$admin->attributes = $post;
		
		if ($admin->validate() && $admin->change_password($_password))
		{
			$admin->setScenario('update');
			$admin->password = CPasswordHelper::hashPassword($admin->New_password);
			if ($admin->save()) {
				self::displayFormSuccess();
			}
		}
		else {
			self::displayFormError($admin);
		}
	}
	
	/**
	 * Validate then update image
	 * 
	 * @access private
	 * @param Admin $admin
	 * @return json
	 */
	private function _uploadImage(Admin $admin)
	{
		sleep(1);
		$upload_path = dirname(Yii::app()->request->scriptFile).'/images/backoffice/admin/';
		if ( ! is_dir($upload_path)) {
			mkdir($upload_path, 0777, TRUE);
		}
		
		$previous_image = $admin->image;
		$admin->setScenario('update_image');
		$admin->image = CUploadedFile::getInstance($admin, 'image');
		
		if ($admin->validate()) {
			// remove previous image
			@unlink($upload_path.$previous_image);
			
			$filename = md5($admin->admin_id).'.'.$admin->image->extensionName;
			Image_Manager::resize($admin->image->tempName, $upload_path.$filename, 100, 100, $admin->image->extensionName);
			
			// save image
			$admin->image = $filename;

			if ($admin->save()) {
				$json = array('status'=>1, 'message'=>'Image updated!');
				die(CJSON::encode($json));
			}
			else {
				$json_array = (array('status'=>0, 'message'=>'Error in saving / uploading file.'));
				die(CJSON::encode($json_array));
			}
		}
		else {
			$json_array = (array('status'=>0, 'message'=>'Invalid file. Please check.'));
			die(CJSON::encode($json_array));
		}
	}
}