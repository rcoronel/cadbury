<?php

class ScenarioController extends BackofficeController
{
	public $portal;
	public $scenario;
	
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action)) {
			$id = Yii::app()->request->getParam('id');
			
			switch ($action->id){
				case 'update':
				case 'delete':
					// Initialize Scenario object
					$this->scenario = Scenario::model()->findByPk($id);
					Scenario::validateObject($this->scenario, 'Scenario', $this->createURL('portal/'));
					
					// Initialize Portal object
					$this->portal = Portal::model()->findByPk($this->scenario->portal_id);
					Portal::validateObject($this->portal, 'Portal', $this->createURL('portal/'));
					
					$this->actionButtons['update'] = array('back'=>array('link'=>$this->createURL('portal_availment/update', array('id'=>$this->scenario->portal_id))));
				break;
			
				default:
					// Initialize Portal object
					$this->portal = Portal::model()->findByPk($id);
					Portal::validateObject($this->portal, 'Portal', $this->createURL('portal/'));

					// action buttons
					$this->actionButtons['index'] = array('back'=>array('link'=>$this->createURL('portal/')));
					$this->actionButtons['add'] = array('back'=>array('link'=>$this->createURL('portal_availment/update', array('id'=>$this->portal->portal_id))));
				break;
			}
			return true;
		}
	}
	
	public function actionIndex()
	{
		$this->render('index');
	}
	
	/**
	 * Add new scenario
	 * 
	 * @access public
	 * @param int $id Portal ID
	 * @return void
	 */
	public function actionAdd($id)
	{
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'plugins', 'jquery.tablednd.js')));
		
		$scenario = new Scenario;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($scenario);
		}
		$nas_avails = PortalAvailment::model()->with('availment')->findAllByAttributes(array('portal_id'=>$this->portal->portal_id));
		$nas_avails_arr = CHtml::listdata($nas_avails, 'availment_id', function($a){return CHtml::encode($a->availment->name);});
		$this->render('form', array('availments'=>$nas_avails_arr, 'scenario'=>new Scenario));
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id Scenario primary key
	 * @return void
	 */
	public function actionUpdate($id)
	{
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'plugins', 'jquery.tablednd.js')));
		
		$scenario = Scenario::model()->findByPk($id);
		Scenario::validateObject($scenario, 'Scenario', $this->createUrl('portal'));
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($scenario);
		}
		
		$nas_avails = PortalAvailment::model()->with('availment')->findAllByAttributes(array('portal_id'=>$this->portal->portal_id));
		$nas_avails_arr = CHtml::listdata($nas_avails, 'availment_id', function($a){return CHtml::encode($a->availment->name);});
		
		// Scenarios
		$new_scenarios = ScenarioAvailment::model()->findAllByAttributes(array('scenario_id'=>$scenario->scenario_id), array('order'=>'level ASC'));
		$returning_scenarios = ScenarioReturning::model()->findAllByAttributes(array('scenario_id'=>$scenario->scenario_id), array('order'=>'level ASC'));
		
		$this->render('form', array('availments'=>$nas_avails_arr, 'scenario'=>$scenario, 'new_scenarios'=>$new_scenarios, 'returning_scenarios'=>$returning_scenarios));
	}
	
	/**
	 * Delete record
	 * 
	 * @access public
	 * @param int $id Scenario ID
	 * @return void
	 */
	public function actionDelete($id)
	{
		$scenario = Scenario::model()->findByPk($id);
		Scenario::validateObject($scenario, 'Scenario');
		
		if (Yii::app()->request->isPostRequest) {
			// check for security token
			if (CPasswordHelper::verifyPassword($scenario->scenario_id, Yii::app()->request->getPost('security_key'))) {
				// Delete scenarios
				ScenarioAvailment::model()->deleteAllByAttributes(array('scenario_id'=>$scenario->scenario_id));
				ScenarioReturning::model()->deleteAllByAttributes(array('scenario_id'=>$scenario->scenario_id));
				DeviceScenario::model()->deleteAllByAttributes(array('portal_id'=>$scenario->portal_id));
				
				// Delete record
				Scenario::model()->deleteByPk($scenario->scenario_id);
				Yii::app()->user->setFlash('success', 'Scenario deleted successfully');
			}
			else {
				Yii::app()->user->setFlash('fail', 'Error in deletion.');
			}
			$this->redirect(array('portal_availment/update', 'id'=>$scenario->portal_id));
		}
		$this->render('delete', array('scenario'=>$scenario));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object Scenario instance
	 * @return	void
	 */
	private function _postValidate(Scenario $object)
	{
		sleep(1);
		
		$post = $_POST;
		$object->title = $post['title'];
		$object->portal_id = $this->portal->portal_id;
		if ($object->validate() && $object->save()) {
			// Delete existing
			ScenarioAvailment::model()->deleteAllByAttributes(array('scenario_id'=>$object->scenario_id));
			ScenarioReturning::model()->deleteAllByAttributes(array('scenario_id'=>$object->scenario_id));
			
			// New user scenario
			if ( ! empty($post['new']['availment_id'])) {
				foreach ($post['new']['availment_id'] as $index => $availment_id) {
					$new_scenario = new ScenarioAvailment;
					$new_scenario->scenario_id = $object->scenario_id;
					$new_scenario->level = $post['new']['level'][$index];
					$new_scenario->availment_id = $post['new']['availment_id'][$index];
					$new_scenario->validate() && $new_scenario->save();
				}
				
				foreach ($post['returning']['availment_id'] as $index => $availment_id) {
					$return_scenario = new ScenarioReturning();
					$return_scenario->scenario_id = $object->scenario_id;
					$return_scenario->level = $post['returning']['level'][$index];
					$return_scenario->availment_id = $post['returning']['availment_id'][$index];
					$return_scenario->validate() && $return_scenario->save();
				}
			}
			
			// Delete DeviceScenario in order to reset availment levels
			DeviceScenario::model()->deleteAllByAttributes(array('portal_id'=>$this->portal->portal_id));
			
			self::displayFormSuccess('Record successfully saved!', $this->createUrl('portal_availment/update', array('id'=>$this->portal->portal_id)));
		}
		self::displayFormError($object);
	}

}