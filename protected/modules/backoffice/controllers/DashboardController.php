<?php

class DashboardController extends BackofficeController
{
	/**
	 * Show dashboard
	 * 
	 * @access public
	 * @return void
	 */
	public function actionIndex()
	{
//		$portals = Yii::app()->db->createCommand()
//    ->select('p.portal_id, p.name as portal, ap.access_point_id, ap.name as ap, ssid.ssid, COUNT(*) as count')
//    ->from('Unifi_Log.device_connection_20160810 dc')
//    ->join('Unifi_CMS.portal p', 'dc.portal_id = p.portal_id')
//	->join('Unifi_CMS.access_point ap', 'dc.access_point_id = ap.access_point_id')
//	->join('Unifi_CMS.service_set ssid', 'dc.ssid = ssid.ssid')
//	->group('p.name, ap.name, ssid.ssid')
//    ->queryAll();
//		
//		$portal_array = array();
//		foreach ($portals as $p) {
//			$portal_array[$p['portal_id']] = array('portal_id'=>$p['portal_id'], 'portal'=>$p['portal'], 'count'=>0);
//		}
//		
//		foreach ($portal_array as &$pa) {
//			$ap_array = array();
//			foreach ($portals as $index=>$p) {
//				if ($pa['portal_id'] == $p['portal_id']) {
//					$ap_array[] = array('access_point_id'=>$p['access_point_id'], 'ap'=>$p['ap'], 'ssid'=>$p['ssid'], 'count'=>$p['count']);
//					unset($portals[$index]);
//				}
//			}
//			$pa['ap'] = $ap_array;
//		}
//		
//		foreach ($portal_array as &$pa) {
//			$pa['count'] = DeviceConnection::pollAll('20160810', NULL, $pa['portal_id'], NULL, NULL);
//			
//		}
//		
//		echo "<pre>";
//		print_r($portal_array);
//		die();
//		die();
		// declare scripts
		Yii::app()->clientScript->registerScriptFile(Link::js_url('bootstrap/bootstrap-datepicker.js'), CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/tocify/jquery.tocify.min.js'), CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/morris/morris.min.js'), CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/raphael/raphael-min.js'), CClientScript::POS_END);
		
		$portals = Portal::model()->findAll(array('order'=>'name ASC'));
		$this->render('index', array('portals'=>$portals));
	}
	
	/**
	 * Count user data
	 * 
	 * @acces public
	 * @return JSON
	 */
	public function actionPoll()
	{
		// Check if from AJAX request
		if ( ! Yii::app()->request->isAjaxRequest) {
			$this->redirect(array('dashboard/'));
		}
		
		$today = date('Ymd', strtotime(Yii::app()->request->getPost('date')));
		
		// Connected users
		$all_users = DeviceConnection::pollAll($today, 0, Yii::app()->request->getPost('portal_id'));
		$new_users = DeviceConnection::pollAllNew($today, 0, Yii::app()->request->getPost('portal_id'));
		$return_users = DeviceConnection::pollAllReturning($today, 0, Yii::app()->request->getPost('portal_id'));
		
		// Authenticated users
		$all_auth_users = DeviceConnection::pollAuthAll($today, 0, Yii::app()->request->getPost('portal_id'));
		$new_auth_users = DeviceConnection::pollAuthNew($today, 0, Yii::app()->request->getPost('portal_id'));
		$return_auth_users = DeviceConnection::pollAuthReturning($today, 0, Yii::app()->request->getPost('portal_id'));
		
		// availment count
		$portal_availments = PortalAvailment::model()->findAllByAttributes(array('portal_id'=>Yii::app()->request->getPost('portal_id')));
		if (empty($portal_availments)) {
			$availments = Availment::model()->findAll();
			foreach ($availments as $a) {
				$availment_count[] = array('label'=>$a->name, 'value'=>DeviceAvailment::poll($today, 0, Yii::app()->request->getPost('portal_id'), 0, 0, $a->availment_id));
			}
		}
		else {
			foreach ($portal_availments as $pa) {
				$availment_count[] = array('label'=>$pa->availment->name, 'value'=>DeviceAvailment::poll($today, 0, Yii::app()->request->getPost('portal_id'), 0, 0, $pa->availment_id));
			}
		}
		
		foreach ($availment_count as $index=>$arr) {
			if ($arr['value'] == 0) {
				unset($availment_count[$index]);
			}
		}
		$availment_count = array_values($availment_count);
		
		$view = $this->renderPartial('poll', array('all_users'=>$all_users,
			'new_users'=>$new_users,
			'return_users'=>$return_users,
			'all_auth_users'=>$all_auth_users,
			'new_auth_users'=>$new_auth_users,
			'return_auth_users'=>$return_auth_users,
			'today'=>$today), TRUE);
		die(CJSON::encode(array('status'=>1, 'message'=>$view, 'availment_chart'=>$availment_count)));
	}
}