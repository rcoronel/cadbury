<div class="col-lg-12">
	<div class="panel panel-danger">
    <div class="panel-heading">
        <div class="panel-title">
			<h4>
				Delete 
				<strong><?php echo $group->name;?></strong> 
				 AP Group(#<?php echo $group->access_point_group_id;?>)?
			</h4>
		</div>
    </div>
    <div class="panel-body">
			
		<p>Deleting this will <strong>remove</strong> the record from the database. Also, corresponding AP names will also be deleted.</p>

		<?php $form=$this->beginWidget('CActiveForm', array('htmlOptions'=>array('class'=>'form-horizontal', 'role'=>'form', 'method'=>'post'))); ?>
			<?php echo CHtml::hiddenField('access_point_group_id', $group->access_point_group_id)?>
			<?php echo CHtml::hiddenField('security_key', CPasswordHelper::hashPassword($group->access_point_group_id));?>
			<div class="buttons">
				<button type="submit" class="btn btn-danger">Delete</button>
				<button type="button" class="btn btn-default" onclick="location.href = '<?php echo $this->createUrl("{$this->id}/");?>';">Cancel</button>
			</div>
		<?php $this->endWidget();?>
	</div>
</div>