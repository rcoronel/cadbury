<div class="row">
    <div class="col-md-3">
        <div id="toc" class="tocify">
			<?php foreach ($roles as $r):?>
				<ul class="tocify-header nav nav-list">
					<li class="tocify-item <?php echo (Yii::app()->request->getParam('id') == $r->admin_role_id) ? 'active' : '';?>" data-unique="<?php echo $r->role;?>" style="cursor: pointer;">
						<a href="<?php echo $this->createUrl(Yii::app()->controller->id.'/update', array('id' => $r->admin_role_id));?>"><?php echo $r->role;?></a>
					</li>
				</ul>
			<?php endforeach;?>
        </div>
    </div>
    <div class="col-md-9">
        <div class="tocify-content">
			<?php if ($role->admin_role_id == 1):?>
				<div class="alert alert-danger">
					<i class="entypo-info-circled"></i> Cannot update this profile's permissions
				</div>
			<?php else:?>
				<?php echo CHtml::beginForm();?>
					<?php echo CHtml::hiddenField('YII_CSRF_TOKEN', Yii::app()->request->csrfToken);?>
					<?php echo CHtml::hiddenField('admin_role_id', $role->admin_role_id);?>
					<table class="table">
						<thead>
							<tr>
								<th>Page</th>
								<th class="center">VIEW</th>
								<th class="center">ADD</th>
								<th class="center">EDIT</th>
								<th class="center">DELETE</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($menus as $menu):?>
								<?php if ($menu->parent_admin_menu_id == 0):?>
									<tr>
										<td>
											<?php echo CHtml::hiddenField("admin_menu[$menu->admin_menu_id][admin_menu_id]", $menu->admin_menu_id);?>
											<?php echo $menu->title;?>
										</td>
										<td class="center">
											<?php echo CHtml::checkBox("admin_menu[$menu->admin_menu_id][view]", $menu->view, array('value'=>1));?>
										</td>
										<td class="center">
											<?php echo CHtml::checkBox("admin_menu[$menu->admin_menu_id][add]", $menu->add, array('value'=>1));?>
										</td>
										<td class="center">
											<?php echo CHtml::checkBox("admin_menu[$menu->admin_menu_id][edit]", $menu->edit, array('value'=>1));?>
										</td>
										<td class="center">
											<?php echo CHtml::checkBox("admin_menu[$menu->admin_menu_id][delete]", $menu->delete, array('value'=>1));?>
										</td>
									</tr>
									<?php foreach ($menus as $submenu):?>
										<?php if ($submenu->parent_admin_menu_id == $menu->admin_menu_id):?>
											<tr>
												<td>
													<i class="entypo-right-open"></i>
													<?php echo CHtml::hiddenField("admin_menu[$submenu->admin_menu_id][admin_menu_id]", $submenu->admin_menu_id);?>
													<?php echo $submenu->title;?>
												</td>
												<td class="center">
													<?php echo CHtml::checkBox("admin_menu[$submenu->admin_menu_id][view]", $submenu->view, array('value'=>1));?>
												</td>
												<td class="center">
													<?php echo CHtml::checkBox("admin_menu[$submenu->admin_menu_id][add]", $submenu->add, array('value'=>1));?>
												</td>
												<td class="center">
													<?php echo CHtml::checkBox("admin_menu[$submenu->admin_menu_id][edit]", $submenu->edit, array('value'=>1));?>
												</td>
												<td class="center">
													<?php echo CHtml::checkBox("admin_menu[$submenu->admin_menu_id][delete]", $submenu->delete, array('value'=>1));?>
												</td>
											</tr>
										<?php endif;?>
									<?php endforeach;?>
								<?php endif;?>
							<?php endforeach;?>
							<tr>
								<td colspan="5">
									<button type="submit" class="btn btn-primary">
										Update
									</button>
								</td>
							</tr>
						</tbody>
					</table>
				<?php CHtml::endForm();?>
			<?php endif;?>
        </div>
    </div>
</div>