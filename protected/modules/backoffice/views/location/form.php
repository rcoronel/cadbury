<div class="row">

	<?php if ($object->hasErrors()):?>
		<div id="form-error" class="alert alert-danger">
			<?php echo CHtml::errorSummary($object);?>
		</div>
	<?php endif;?>
	<div class="col-md-6 col-md-offset-3">
		<div id="form-error" class="alert alert-danger" style="display: none;"></div>
	</div>

	<div class="col-md-12">

		<div class="panel panel-default" data-collapsed="0">
			<div class="panel-body">
				<?php $form=$this->beginWidget('CActiveForm', array('htmlOptions' => array('class'=>'form-horizontal form-groups-bordered', 'role' => 'form', 'method' => 'post'))); ?>
					<?php echo CHtml::hiddenField('ajax', 1)?>
					<div class="form-group">
						<?php echo $form->labelEx($object, 'portal_id', array('class' => 'control-label col-lg-3'));?>
						<div class="col-md-5">
							<?php echo $form->dropDownList($object, 'portal_id', CHtml::listData($portals, 'portal_id', 'name'), array('class'=>'form-control', 'prompt'=>'---'));?>
						</div>
					</div>
					<div class="form-group">
						<?php echo $form->labelEx($object, 'name', array('class' => 'control-label col-lg-3'));?>
						<div class="col-md-5">
							<?php echo $form->textField($object, 'name', array('class'=>'form-control', 'placeholder'=>$object->getAttributeLabel('name')));?>
						</div>
					</div>
				
					<div class="form-group">
						<?php echo $form->labelEx($object, 'type', array('class' => 'control-label col-lg-3'));?>
						<div class="col-md-5">
							<?php echo $form->dropDownList($object, 'type',
								array('country'=>'Country',
									'region'=>'Region',
									'province'=>'Province',
									'city'=>'City',
									'municipality'=>'Municipality',
									'barangay'=>'Barangay',
									'establishment'=>'Establishment'),
								array('class'=>'form-control', 'prompt'=>'---', 'onchange'=>'javascript:showParent(this.value);'));?>
						</div>
					</div>
				
				<div id="parent_location" class="form-group" style="display: none;">
					
				</div>
				
					<div class="form-group">
						<?php echo $form->labelEx($object, 'latlon', array('class' => 'control-label col-lg-3'));?>
						<div class="col-md-5">
							<?php echo $form->textField($object, 'latlon', array('class'=>'form-control', 'placeholder'=>$object->getAttributeLabel('latlon')));?>
						</div>
					</div>
				
					<div class="form-group">
						<?php echo $form->labelEx($object, 'address', array('class' => 'control-label col-lg-3'));?>
						<div class="col-md-5">
							<?php echo $form->textField($object, 'address', array('class'=>'form-control', 'placeholder'=>$object->getAttributeLabel('address')));?>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</div>
				<?php $this->endWidget();?>
			</div>
		</div>
	</div>
</div>


<script>
function showParent(value)
{
	if (value != 'country') {
		$.ajax(
		{
			url : currentIndex+'/parent',
			type: "POST",
			data : $('form').serialize(),
			dataType: 'json',
			success:function(data, textStatus, jqXHR)
			{
				if (data.status) {
					$('div#parent_location').html(data.message);
					$('div#parent_location').show();
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				//if fails     
			}
		});
	}
	else {
		$('div#parent_location').html('');
		$('div#parent_location').hide();
	}
}
$(document).ready(function(){
	showParent();
	$('form').on('submit',function(e){
		// check if submitted via AJAX
		if ($('input[name="ajax"]').length && $('input[name="ajax"]').val())
		{
			// remove error style
			$('div.form-group').removeClass('has-error');

			// hide errors
			$('div#form-error').slideUp( 300 ).delay( 800 ).html();
			//$('div#form-error').hide();

			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				dataType: 'json',
				success:function(data, textStatus, jqXHR)
				{
					if (! data.status) {
						$('div#form-error').slideDown(300).delay(800).html(data.message);
						$('div#form-error').show();

						if (typeof data.fields !== 'undefined')
						{
							$.each(data.fields, function(index, value){
								$('label[for="'+value+'"]').parent().addClass('has-error');
							});
						}
					}
					else {
						$('#confirmation-modal .modal-body').html(data.message);
						$('#confirmation-modal').modal({backdrop: 'static', keyboard: false});

						setTimeout(function() {
							window.location.href = currentIndex;
						  }, 2000);
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					//if fails     
				}
			});
			e.preventDefault(); //STOP default action
		}
	});
});
</script>