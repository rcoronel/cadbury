
<?php echo CHtml::activeLabelEx($object, 'parent_location_id', array('class' => 'control-label col-lg-3'));?>
<div class="col-md-5">
	<?php echo CHtml::activeDropDownList($object, 'parent_location_id',
		CHtml::listData($parents, 'location_id', 'name'),
		array('class'=>'form-control'));?>
	<?php echo CHtml::activeHiddenField($object, 'parent_type', array('value'=>$type));?>
</div>
				