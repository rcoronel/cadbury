<div class="col-lg-12">
	<div class="panel panel-danger">
    <div class="panel-heading">
        <div class="panel-title">
			<h4>
				Delete 
				<strong><?php echo $cms->title;?></strong> 
				(#<?php echo $cms->cms_id;?>)?
			</h4>
		</div>
    </div>
    <div class="panel-body">
			
		<p>Deleting this will <strong>remove</strong> the record from the database. The content will no longer be viewed by users.</p>

		<?php $form=$this->beginWidget('CActiveForm', array('htmlOptions'=>array('class'=>'form-horizontal', 'role'=>'form', 'method'=>'post'))); ?>
			<?php echo CHtml::hiddenField('cms_id', $cms->cms_id)?>
			<?php echo CHtml::hiddenField('security_key', CPasswordHelper::hashPassword($cms->cms_id));?>
			<div class="buttons">
				<button type="submit" class="btn btn-danger">Delete</button>
				<button type="button" class="btn btn-default" onclick="location.href = '<?php echo $this->createUrl('portal_setting/', array('id'=>$cms->portal_id));?>';">Cancel</button>
			</div>
		<?php $this->endWidget();?>
	</div>
</div>