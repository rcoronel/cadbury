<div class="col-lg-12">
	<div class="panel panel-danger">
    <div class="panel-heading">
        <div class="panel-title">
			<h4>
				Delete 
				<strong><?php echo $role->role;?></strong> 
				(#<?php echo $role->admin_role_id;?>)?
			</h4>
		</div>
    </div>
    <div class="panel-body">
			
		<p>Deleting this role will <strong>disable</strong> this role as an option. For security purposes, there must be no admin assigned to this role.</p>

		<?php $form=$this->beginWidget('CActiveForm', array('htmlOptions' => array(	'class'=>'form-horizontal', 'role' => 'form', 'method' => 'post'))); ?>
			<?php echo CHtml::hiddenField('admin_role_id', $role->admin_role_id)?>
			<?php echo CHtml::hiddenField('security_key', CPasswordHelper::hashPassword($role->admin_role_id));?>
			<div class="buttons">
				<button type="submit" class="btn btn-danger">Delete</button>
				<button type="button" class="btn btn-default" onclick="location.href = '<?php echo $this->createUrl("{$this->id}/");?>';">Cancel</button>
			</div>
		<?php $this->endWidget();?>
	</div>
</div>