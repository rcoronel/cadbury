

<?php if ( ! empty($objectList)):?>
	<?php foreach ($objectList as $row):?>
		<div class="member-entry">
			<a href="<?php echo $this->createUrl($this->id.'/view', array('id' => $row->{$object->tableSchema->primaryKey}));?>" class="member-img">
				<img src="<?php echo $row->Picture;?>" class="img-rounded">
				<i class="entypo-forward"></i>
			</a>
			<div class="member-details">
				<h4>
					<a href="<?php echo $this->createUrl($this->id.'/view', array('id' => $row->{$object->tableSchema->primaryKey}));?>">
						<?php echo $row->Name;?>
					</a>
				</h4>
				<div class="row info-list">
					<div class="col-sm-4">
						<i class="entypo-facebook"></i>
						<a href="https://www.facebook.com/<?php echo $row->Fb_ID;?>" target="_blank">
							https://www.fb.com/<?php echo $row->Fb_ID;?>
						</a>
					</div>
					<div class="col-sm-4">
						<i class="entypo-mail"></i>
						<a href="mailto:<?php echo $row->Email;?>"><?php echo $row->Email;?></a>
					</div>
					<div class="col-sm-4">
						<i class="entypo-cc-by"></i>
						<?php echo $row->Gender;?>
					</div>
					
					<div class="clear"></div>
					
					<div class="col-sm-4">
						<i class="entypo-mobile"></i>
						<?php echo $row->device->Mac_address;?>
					</div>
					<div class="col-sm-4">
						<i class="entypo-calendar"></i>
						<?php echo date('F d, Y', strtotime($row->Created_at));?>
					</div>
				</div>
			</div>
		</div>
	<?php endforeach;?>
<?php endif;?>

<?php $this->widget('CLinkPager', array(
		'pages' => $pages,
	)) ?>