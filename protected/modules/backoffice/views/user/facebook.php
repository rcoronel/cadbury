

<div class="row">
	<div class="col-lg-12">
		<div class="table-responsive">
			
		</div>
		
		<div class="col-lg-12">
			<div id="object-list">
				<?php if ( ! empty($users)):?>
				<?php foreach ($users as $row):?>
					<div class="member-entry">
						<a href="" class="member-img">
							<img src="<?php echo $row->picture;?>" class="img-rounded">
							<i class="entypo-forward"></i>
						</a>
						<div class="member-details">
							<h4>
								<a href="">
									<?php echo $row->name;?>
								</a>
							</h4>
							<div class="row info-list">
								<div class="col-sm-4">
									<i class="entypo-facebook"></i>
									<a href="https://www.facebook.com/<?php echo $row->fb_id;?>" target="_blank">
										https://www.fb.com/<?php echo $row->fb_id;?>
									</a>
								</div>
								<div class="col-sm-4">
									<i class="entypo-mail"></i>
									<a href="mailto:<?php echo $row->email;?>"><?php echo $row->email;?></a>
								</div>
								<div class="col-sm-4">
									<i class="entypo-cc-by"></i>
									<?php echo $row->gender;?>
								</div>

								<div class="clear"></div>

								<div class="col-sm-4">
									<i class="entypo-calendar"></i>
									<?php echo date('F d, Y', strtotime($row->created_at));?>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach;?>
			<?php endif;?>
			</div>
		</div>
	</div>
</div>
