
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default" data-collapsed="0">
			<div class="panel-body">
				<?php echo CHtml::beginForm('', 'post', array('class'=>'form-horizontal form-groups-bordered', 'enctype' => 'multipart/form-data')); ?>
					<div class="form-group">
						<?php echo CHtml::label('Subscriber Type', 'subslist', array('class' => 'control-label col-lg-3'));?>
						<div class="col-lg-5">
						<?php
				            echo CHtml::radioButtonList('plan', '1',
					                    // 'consumer',
					                    array(
					                        '1'=>'Home',
					                        '2'=>'Platinum',
					                    )
					                );
        				?>
        				</div>
					</div>
					<div class="form-group">
						<?php echo CHtml::label('Subscriber List', 'subslist', array('class' => 'control-label col-lg-3'));?>
						<div class="col-lg-5">
							<?php echo CHtml::fileField('subslist', '', array('class' => 'form-control')); ?>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</div>
				<?php echo CHtml::endForm(); ?>
			</div>
		</div>
	</div>
</div>

<?php $this->widget('widgets.ajax.Modal');?>

<script>
$(document).ready(function(){
	$('form').on('submit',function(e){
		// check if submitted via AJAX
		if ($('input[name="ajax"]').length && $('input[name="ajax"]').val())
		{
			// remove error style
			$('div.form-group').removeClass('has-error');

			// hide errors
			$('div#form-error').slideUp( 300 ).delay( 800 ).html();
			//$('div#form-error').hide();

			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				dataType: 'json',
				success:function(data, textStatus, jqXHR) {
					if ( ! data.status) {
						$('div#form-error').slideDown(300).delay(800).html(data.message);
						$('div#form-error').show();

						if (typeof data.fields !== 'undefined')
						{
							$.each(data.fields, function(index, value){
								$('label[for="'+value+'"]').parent().addClass('has-error');
							});
						}
					}
					else {
						setTimeout(function() {
							$('#confirmation-modal .modal-body').html(data.message);
							$('#confirmation-modal').modal({backdrop: 'static', keyboard: false});
						}, 900);
						
						setTimeout(function() {
							window.location.href = currentIndex;
						  }, 2000);
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					//if fails     
				}
			});
			e.preventDefault(); //STOP default action
		}
	});
});
</script>