<div class="col-md-12">
	<div class="panel panel-primary" data-collapsed="0">
		<div class="panel-heading">
			<div class="panel-title">
				Amazon S3 Settings
			</div>
			<div class="panel-options">
				<a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg">
					<i class="entypo-cog"></i>
				</a>
				<a href="#" data-rel="collapse">
					<i class="entypo-down-open"></i>
				</a>
			</div>
		</div>
		<div class="panel-body">
			<?php echo CHtml::beginForm('', 'post', array('class'=>'form-horizontal form-groups-bordered', 'enctype'=>'multipart/form-data')); ?>
				<div class="form-group">
					<?php echo CHtml::label('Status', 'Configuration[S3_STATUS]', array('class'=>'col-sm-3 control-label'));?>
					<div class="col-sm-5">
						<div class="radio">
							<?php echo CHtml::radioButtonList('Configuration[S3_STATUS]', Configuration::getValue('S3_STATUS'), array('1'=>'On', '0'=>'Off'));?>	
						</div>
					</div>
				</div>
				<div class="form-group">
					<?php echo CHtml::label('Access Key', 'Configuration[S3_ACCESS_KEY]', array('class'=>'col-sm-3 control-label'));?>
					<div class="col-sm-5">
						<?php echo CHtml::textField('Configuration[S3_ACCESS_KEY]', Configuration::getValue('S3_ACCESS_KEY'), array('class'=>'form-control', 'maxlength'=>64)); ?>
					</div>
				</div>
				<div class="form-group">
					<?php echo CHtml::label('Secret Key', 'Configuration[S3_SECRET_KEY]', array('class'=>'col-sm-3 control-label'));?>
					<div class="col-sm-5">
						<?php echo CHtml::textField('Configuration[S3_SECRET_KEY]', Configuration::getValue('S3_SECRET_KEY'), array('class'=>'form-control', 'maxlength'=>64)); ?>
					</div>
				</div>
				<div class="form-group">
					<?php echo CHtml::label('Bucket', 'Configuration[S3_BUCKET]', array('class'=>'col-sm-3 control-label'));?>
					<div class="col-sm-5">
						<?php echo CHtml::textField('Configuration[S3_BUCKET]', Configuration::getValue('S3_BUCKET'), array('class'=>'form-control', 'maxlength'=>64)); ?>
					</div>
				</div>
				<div class="form-group">
					<?php echo CHtml::label('Bucket URL', 'Configuration[S3_BUCKET_URL]', array('class'=>'col-sm-3 control-label'));?>
					<div class="col-sm-5">
						<?php echo CHtml::textField('Configuration[S3_BUCKET_URL]', Configuration::getValue('S3_BUCKET_URL'), array('class'=>'form-control', 'maxlength'=>256)); ?>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-5">
						<button type="submit" class="btn btn-default">Submit</button>
					</div>
				</div>
			<?php echo CHtml::endForm();?>
		</div>
	</div>
</div>