<div class="col-lg-12">
	<div class="panel panel-danger">
    <div class="panel-heading">
        <div class="panel-title">
			<h4>
				Delete 
				<strong><?php echo $site->name;?></strong> 
				site (#<?php echo $site->site_id;?>)?
			</h4>
		</div>
    </div>
    <div class="panel-body">
			
		<p>Deleting this will <strong>delete</strong> site from the database. For security purposes, sites that have portals will not be deleted.</p>

		<?php $form=$this->beginWidget('CActiveForm', array('htmlOptions'=>array('class'=>'form-horizontal', 'role'=>'form', 'method'=>'post'))); ?>
			<?php echo CHtml::hiddenField('site_id', $site->site_id)?>
			<?php echo CHtml::hiddenField('security_key', CPasswordHelper::hashPassword($site->site_id));?>
			<div class="buttons">
				<button type="submit" class="btn btn-danger">Delete</button>
				<button type="button" class="btn btn-default" onclick="location.href = '<?php echo $this->createUrl("{$this->id}/");?>';">Cancel</button>
			</div>
		<?php $this->endWidget();?>
	</div>
</div>