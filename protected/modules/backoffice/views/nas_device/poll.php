
			<div class="col-sm-12 col-xs-6">
				<div class="tile-stats tile-green">
					<div class="icon">
						<i class="entypo-users"></i>
					</div>
					<div class="num" data-start="0" data-end="<?php echo $all_users;?>" data-postfix="" data-duration="1500" data-delay="0"><?php echo number_format($all_users);?></div>
					<h3>Connected users</h3>
					<p><?php echo date('F d, Y', strtotime($from));?> to <?php echo date('F d, Y', strtotime($to));?></p>
				</div>
			</div>
			<div class="col-sm-6 col-xs-6">
				<div class="tile-stats tile-blue">
					<div class="icon">
						<i class="entypo-users"></i>
					</div>
					<div class="num" data-start="0" data-end="<?php echo $new_users;?>" data-postfix="" data-duration="1500" data-delay="0"><?php echo number_format($new_users);?></div>
					<h3>New users</h3>
					<p><?php echo date('F d, Y', strtotime($from));?> to <?php echo date('F d, Y', strtotime($to));?></p>
				</div>
			</div>
			<div class="col-sm-6 col-xs-6">
				<div class="tile-stats tile-orange">
					<div class="icon">
						<i class="entypo-users"></i>
					</div>
					<div class="num" data-start="0" data-end="<?php echo $return_users;?>" data-postfix="" data-duration="1500" data-delay="0"><?php echo number_format($return_users);?></div>
					<h3>Returning users</h3>
					<p><?php echo date('F d, Y', strtotime($from));?> to <?php echo date('F d, Y', strtotime($to));?></p>
				</div>
			</div>
			<hr>
			<div class="col-sm-12 col-xs-6">
				<div class="tile-stats tile-white-green">
					<div class="icon">
						<i class="entypo-users"></i>
					</div>
					<div class="num" data-start="0" data-end="<?php echo $all_auth_users;?>" data-postfix="" data-duration="1500" data-delay="0"><?php echo number_format($all_auth_users);?></div>
					<h3>Authenticated users</h3>
					<p><?php echo date('F d, Y', strtotime($from));?> to <?php echo date('F d, Y', strtotime($to));?></p>
				</div>
			</div>
			<div class="col-sm-6 col-xs-6">
				<div class="tile-stats tile-white-blue">
					<div class="icon">
						<i class="entypo-users"></i>
					</div>
					<div class="num" data-start="0" data-end="<?php echo $new_auth_users;?>" data-postfix="" data-duration="1500" data-delay="0"><?php echo number_format($new_auth_users);?></div>
					<h3>Authenticated new users</h3>
					<p><?php echo date('F d, Y', strtotime($from));?> to <?php echo date('F d, Y', strtotime($to));?></p>
				</div>
			</div>
			<div class="col-sm-6 col-xs-6">
				<div class="tile-stats tile-white-orange">
					<div class="icon">
						<i class="entypo-users"></i>
					</div>
					<div class="num" data-start="0" data-end="<?php echo $return_auth_users;?>" data-postfix="" data-duration="1500" data-delay="0"><?php echo number_format($return_auth_users);?></div>
					<h3>Authenticated returning users</h3>
					<p><?php echo date('F d, Y', strtotime($from));?> to <?php echo date('F d, Y', strtotime($to));?></p>
				</div>
			</div>
		