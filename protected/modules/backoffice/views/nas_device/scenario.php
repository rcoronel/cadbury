<?php if ( ! empty($scenarios)):?>
	<?php foreach ($scenarios as $s):?>
		<div class="col-md-4">
			<section class="task-panel tasks-widget">
				<div class="panel-heading">
					<div class="pull-left">
						<h5>
							<strong><?php echo $s->Title;?></strong>
						</h5>
					</div>
					<div class="pull-right">
						<h5>
							
						</h5>
					</div>
				</div>
				
			</section>
		</div>
	<?php endforeach;?>
<?php endif;?>
