<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" data-collapsed="0">
			<div class="panel-body">
				<?php echo CHtml::beginForm('', 'post', array('id'=>'form-terms', 'class'=>'form-horizontal form-groups-bordered', 'enctype' => 'multipart/form-data')); ?>
					<?php echo CHtml::hiddenField('form', 'terms');?>
					<?php echo CHtml::hiddenField('id', $nas->id);?>
					<div class="form-group">
						<?php echo CHtml::label('Service Agreement', 'service-agreement', array('class' => 'control-label col-lg-2', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'data-original-title'=>'Service Agreement'));?>
						<div class="col-lg-10">
							<?php echo CHtml::textArea('service-agreement', $agreement->Content, array('class' => 'form-control', 'rows'=>15));?>
							<script type="text/javascript">
								var editor = CKEDITOR.replace('service-agreement');
								editor.on('change', function( evt ) {
									// getData() returns CKEditor's HTML content.
									$('#service-agreement').html(evt.editor.getData());
								});
							</script>
						</div>
					</div>
					<div class="form-group">
						<?php echo CHtml::label('Terms and Conditions', 'terms-and-conditions', array('class' => 'control-label col-lg-2', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'data-original-title'=>'Terms and Conditions'));?>
						<div class="col-lg-10">
							<?php echo CHtml::textArea('terms-and-conditions', $terms->Content, array('class' => 'form-control', 'rows'=>15));?>
							<script type="text/javascript">
								var editor = CKEDITOR.replace('terms-and-conditions');
								editor.on('change', function( evt ) {
									// getData() returns CKEditor's HTML content.
									$('#terms-and-conditions').html(evt.editor.getData());
								});
							</script>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</div>
				<?php echo CHtml::endForm(); ?>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function() {
		$('form#form-terms').on('submit', function(e){
			$.ajax({
				type: "POST",
				url: currentIndex+'/post_terms',
				data: $('form#form-terms').serialize(),
				success: function(data) {
					toastr.success(data.message, '', {closeButton:true,showDuration:"300",timeOut: "3000"});
				},
				dataType: "json"
			});
			e.preventDefault();
		});
		
		$('[data-toggle="tooltip"]').each(function(i, el)
		{
			var $this = $(el),
				placement = attrDefault($this, 'placement', 'top'),
				trigger = attrDefault($this, 'trigger', 'hover'),
				popover_class = $this.hasClass('tooltip-secondary') ? 'tooltip-secondary' : ($this.hasClass('tooltip-primary') ? 'tooltip-primary' : ($this.hasClass('tooltip-default') ? 'tooltip-default' : ''));

			$this.tooltip({
				placement: placement,
				trigger: trigger
			});

			$this.on('shown.bs.tooltip', function(ev)
			{
				var $tooltip = $this.next();

				$tooltip.addClass(popover_class);
			});
		});
	});
</script>