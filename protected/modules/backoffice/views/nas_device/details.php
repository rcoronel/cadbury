<div class="panel panel-primary" data-collapsed="0">
	<div class="panel-heading">
		<div class="panel-title">
			NAS Details
		</div>
	</div>
	<div class="panel-body">
		<form role="form" class="form-horizontal form-groups-bordered">
			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">Shortname</label>
				<div class="col-sm-5">
					<?php echo $nas->shortname;?>
				</div>
			</div>
			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">Private IP</label>
				<div class="col-sm-5">
					<?php echo $nas->nasname;?>
				</div>
			</div>
			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">Public IP</label>
				<div class="col-sm-5">
					<?php echo $nas->switchip;?>
				</div>
			</div>
			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">Type</label>
				<div class="col-sm-5">
					<?php echo $nas->type;?>
				</div>
			</div>
			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">Ports</label>
				<div class="col-sm-5">
					<?php echo $nas->ports;?>
				</div>
			</div>
			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">Brand</label>
				<div class="col-sm-5">
					<?php echo $nas->brand;?>
				</div>
			</div>
			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">Description</label>
				<div class="col-sm-5">
					<?php echo $nas->description;?>
				</div>
			</div>
		</form>
	</div>
</div>