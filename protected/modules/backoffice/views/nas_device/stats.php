<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" data-collapsed="0">
			<div class="panel-heading">
				<div class="panel-title">
					<?php echo CHtml::beginForm('', 'post', array('id'=>'form-stats', 'class'=>'form-horizontal'));?>
						<input type="hidden" name="id" value="<?php echo $nas->id;?>">
						<div class="form-group">
							<label class="col-sm-3 control-label">
								<strong>From</strong>
							</label>
							<div class="col-sm-4">
								<div class="input-group">
									<input name="from" type="text" class="form-control datepicker" data-format="D, MM dd, yyyy" value="<?php echo date('D, F d, Y');?>">
									<div class="input-group-addon">
										<a href="#">
											<i class="entypo-calendar"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">
								<strong>To</strong>
							</label>
							<div class="col-sm-4">
								<div class="input-group">
									<input name="to" type="text" class="form-control datepicker" data-format="D, MM dd, yyyy" value="<?php echo date('D, F d, Y');?>">
									<div class="input-group-addon">
										<a href="#">
											<i class="entypo-calendar"></i>
										</a>
									</div>
								</div>
							</div>
							<button class="btn btn-blue" type="submit">Show</button>
						</div>
					<?php echo CHtml::endForm();?>

				</div>
			</div>
			<div class="panel-body" id="stat_numbers">
				<div class="alert alert-info">
					<i class="entypo-info-circled"></i>&nbsp;
					Select date above
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-6 col-xs-6">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<div class="panel-title">By availment methods</div>
			</div>
			<div class="panel-body">
				<div id="stat_chart" style="height: 200px">
					<div class="alert alert-info">
						<i class="entypo-info-circled"></i>&nbsp;
						Select date above
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-6 col-xs-6">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<div class="panel-title">By Facebook gender</div>
			</div>
			<div class="panel-body">
				<div id="gender_chart" style="height: 200px">
					<div class="alert alert-info">
						<i class="entypo-info-circled"></i>&nbsp;
						Select date above
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-6 col-xs-6">
		<div class="panel panel-primary" style="display:none;" id="export-panel">
			<div class="panel-body">
				<?php echo CHtml::beginForm('', 'post', array('id'=>'export-csv-form'));?>
					<button type="submit" class="btn btn-primary btn-lg btn-block btn-icon">
						Export statistics to CSV
						<i class="entypo-doc-text-inv"></i>
					</button>
				<?php echo CHtml::endForm();?>
				<br>
				
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function() {
		$('.datepicker').datepicker({
			'format':'D, MM dd, yyyy'
		});
		
		$('form#export-csv-form').on('submit', function(e){
			var nas_id = $('input[name="id"]').val();
			var from = $('input[name="from"]').val();
			var to = $('input[name="to"]').val();
			var url = currentIndex+'/export_stats&nas_id='+nas_id+'&from='+from+'&to='+to;
			$(this).attr('action', url);
		});
		
		$('form#form-stats').on('submit', function(e){
			$.ajax({
				type: "POST",
				url: currentIndex+'/post_stats',
				data: $('form#form-stats').serialize(),
				success: function(data) {
					$('#stat_numbers').html(data.message);
					$(".tile-stats").each(function(i, el) {
						var $this = $(el), $num = $this.find('.num'),
							start = attrDefault($num, 'start', 0),
							end = attrDefault($num, 'end', 0),
							prefix = attrDefault($num, 'prefix', ''),
							postfix = attrDefault($num, 'postfix', ''),
							duration = attrDefault($num, 'duration', 1000),
							delay = attrDefault($num, 'delay', 1000);

						if(start < end) {
							if(typeof scrollMonitor == 'undefined') {
								$num.html(prefix + end + postfix);
							}
							else {
								var tile_stats = scrollMonitor.create(el);
								tile_stats.fullyEnterViewport(function(){
									var o = {curr: start};
									TweenLite.to(o, duration/1000, {curr: end, ease: Power1.easeInOut, delay: delay/1000, onUpdate: function(){
										$num.html(prefix + Math.round(o.curr) + postfix);
									}
									});
								tile_stats.destroy();
								});
							}
						}
					});

					$('div#stat_chart').html('');
					new Morris.Donut({
						element: 'stat_chart',
						data: data.chart_data
					});

					$('div#gender_chart').html('');
					new Morris.Bar({
						element: 'gender_chart',
						data: data.bar_data,
						xkey: 'label',
						ykeys: ['value'],
						labels: ['Count']
					});
					
					$('div#export-panel').show();
				},
				dataType: "json"
			});
			e.preventDefault();
		});
		
		
		
		$('[data-toggle="tooltip"]').each(function(i, el)
		{
			var $this = $(el),
				placement = attrDefault($this, 'placement', 'top'),
				trigger = attrDefault($this, 'trigger', 'hover'),
				popover_class = $this.hasClass('tooltip-secondary') ? 'tooltip-secondary' : ($this.hasClass('tooltip-primary') ? 'tooltip-primary' : ($this.hasClass('tooltip-default') ? 'tooltip-default' : ''));

			$this.tooltip({
				placement: placement,
				trigger: trigger
			});

			$this.on('shown.bs.tooltip', function(ev)
			{
				var $tooltip = $this.next();

				$tooltip.addClass(popover_class);
			});
		});
	});
</script>