<div class="row">
	<?php echo CHtml::hiddenField('nas_id', $nas->id);?>
	<div class="col-md-3">
		<div id="toc" class="tocify" style="width: 237px;">
			<ul id="tocify-header" class="tocify-header nav nav-list">
				<li class="tocify-item active" data-unique="" style="cursor: pointer;">
					<a onclick="javascript:show_stats('details');">Details</a>
				</li>
			</ul>
			<ul id="tocify-header" class="tocify-header nav nav-list">
				<li class="tocify-item" data-unique="" style="cursor: pointer;">
					<a onclick="javascript:show_stats('appearance');">Appearance</a>
				</li>
			</ul>
			<ul id="tocify-header" class="tocify-header nav nav-list">
				<li class="tocify-item" data-unique="" style="cursor: pointer;">
					<a onclick="javascript:show_stats('terms');">Terms of Service</a>
				</li>
			</ul>
			<ul id="tocify-header" class="tocify-header nav nav-list">
				<li class="tocify-item" data-unique="" style="cursor: pointer;">
					<a onclick="javascript:show_stats('scenarios');">Availment scenarios</a>
				</li>
			</ul>
			<ul id="tocify-header" class="tocify-header nav nav-list">
				<li class="tocify-item" data-unique="" style="cursor: pointer;">
					<a onclick="javascript:show_stats('stats');">Statistics</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="col-md-9">
		<div class="col-sm-12">
			<div id="form-container"></div>
		</div>
	</div>
</div>
<?php $this->widget('widgets.ajax.Modal');?>
<script lang="text/javascript">
	$(document).ready(function() {
		show_stats('details');
	});
	function show_stats(form)
	{
		$.ajax({
			type: "POST",
			url: currentIndex+'/show',
			data: {'YII_CSRF_TOKEN':token, 'form':form, 'id':$('input[name="nas_id"]').val()},
			success: function(data) {
				setTimeout(function() {
					$('div#form-container').html(data.message);
				}, 900);
			},
			dataType: "json"
		});
	};
</script>