<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default" data-collapsed="0">
			<div class="panel-body">
				<?php echo CHtml::beginForm('', 'post', array('class'=>'form-horizontal form-groups-bordered', 'role'=>'form'));?>
					<?php echo CHtml::hiddenField('ajax', 1);?>
					<div class="form-group">
						<?php echo CHtml::label('App ID', 'AvailmentSetting[FB_APP_ID]', array('class'=>'control-label col-lg-3'));?>
						<div class="col-md-5">
							<?php echo CHtml::textField('AvailmentSetting[FB_APP_ID]', AvailmentSetting::getValue($object->availment_id, 'FB_APP_ID'), array('class'=>'form-control'));?>
						</div>
					</div>
					<div class="form-group">
						<?php echo CHtml::label('App Secret Key', 'AvailmentSetting[FB_SECRET_KEY]', array('class'=>'control-label col-lg-3'));?>
						<div class="col-md-5">
							<?php echo CHtml::textField('AvailmentSetting[FB_SECRET_KEY]', AvailmentSetting::getValue($object->availment_id, 'FB_SECRET_KEY'), array('class'=>'form-control'));?>
						</div>
					</div>
				
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</div>
				<?php echo CHtml::endForm();?>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	$('form').on('submit',function(e){
		// check if submitted via AJAX
		if ($('input[name="ajax"]').length && $('input[name="ajax"]').val()) {
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			$.ajax({
				url : formURL,
				type: "POST",
				data : postData,
				dataType: 'json',
				success:function(data, textStatus, jqXHR) {
					setTimeout(function() {
						$('#confirmation-modal .modal-body').html(data.message);
						$('#confirmation-modal').modal({backdrop: 'static', keyboard: false});
					}, 900);
						
					setTimeout(function() {
						if (data.url) {
							window.location.href = data.url;
						}
						else {
							window.location.href = currentIndex;
						}
					}, 2000);
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					//if fails     
				}
			});
			e.preventDefault(); //STOP default action
		}
	});
	$(document).on("keydown", ".number-only", function(event) {
		if ( event.keyCode == 46 || event.keyCode == 8  ||  event.keyCode == 9) {
		}
		else {
			if (event.keyCode < 48 || event.keyCode > 57 ) {
				event.preventDefault();	
			} 	
		}
	});
});
</script>
<?php $this->widget('widgets.ajax.Modal');?>