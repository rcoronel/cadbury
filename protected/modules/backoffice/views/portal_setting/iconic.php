<div class="col-md-12">
	<div class="panel panel-primary" data-collapsed="0">
		<div class="panel-heading">
			<div class="panel-title">Other settings</div>
		</div>
		<div class="panel-body" style="display: block;">
			<div class="form-group">
				<?php echo CHtml::label('Maximum users connected', 'PortalConfiguration[MAX_USER]', array('class'=>'col-sm-3 control-label'));?>
				<div class="col-sm-5">
					<?php echo CHtml::textField('PortalConfiguration[MAX_USER]', PortalConfiguration::getValue('MAX_USER'), array('class'=>'form-control')); ?>
				</div>
			</div>
		</div>
	</div>
</div>