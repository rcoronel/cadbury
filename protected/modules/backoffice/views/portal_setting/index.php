<div class="profile-env">
	<header class="row">
		<div class="col-sm-2">
			<div style="width: 115px; height: 115px; margin-left: 35px;">
				<span class="profile-picture">
					<?php if ( ! empty($logo)):?>
						<img src="<?php echo Link::image_url("portal/{$portal->portal_id}/{$logo}");?>" class="img-responsive img-circle">
					<?php else:?>
						<img src="<?php echo Link::image_url('default-user.jpg');?>" class="img-responsive img-circle">
					<?php endif;?>
				</span>
			</div>
		</div>
		<div class="col-sm-7">
			<ul class="profile-info-sections">
				<li>
					<div class="profile-name">
						<strong>
							<?php echo $portal->name;?>
							<?php if ($portal->is_active):?>
								<span class="user-status is-online tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Active"></span>
							<?php endif;?>
								
							<?php if ( ! $portal->is_active):?>
								<span class="user-status is-offline tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Inactive"></span>
							<?php endif;?>
							
						</strong>
						<span>
							<a href="#"><?php echo $portal->description;?></a>
						</span>
					</div>
				</li>
				<li>
					<div class="profile-stat">
						<h3><?php echo $count_all;?></h3>
						<span>connected</span>
					</div>
				</li>
				<li>
					<div class="profile-stat">
						<h3><?php echo $count_auth;?></h3>
						<span>authenticated</span>
					</div>
				</li>
			</ul>
		</div>
		<div class="col-sm-3">
			<div class="profile-buttons">
				<a href="<?php echo $this->createUrl('portal_setting/update', array('id'=>$portal->portal_id));?>" class="btn btn-default" title="Update portal">
					<i class="entypo-pencil"></i>
				</a>
				<a href="<?php echo $this->createUrl('portal_setting/view', array('id'=>$portal->portal_id));?>" class="btn btn-default" target="_blank" title="View portal">
					<i class="entypo-eye"></i>
				</a>
			</div>
		</div>
	</header>
	<section class="profile-info-tabs">
		<div class="row">
			<div class="col-sm-offset-2 col-sm-10">
				<ul class="user-details">
					<li>
						<a href="#" title="Site">
							<i class="entypo-location"></i>
							<?php echo $portal->site->name;?>
						</a>
					</li>
					<li>
						<a href="#" title="Date Added">
							<i class="entypo-calendar"></i>
							<?php echo date('F d, Y h:i A', strtotime($portal->created_at));?>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</section>
</div>

<div class="col-lg-12">
	<div class="table-responsive">
		<h2>
			CMS				
			<div class="pull-right">
				<!-- ACTION BUTTONS -->
				<a class="btn btn-default" href="<?php echo $this->createUrl('cms/add', array('id'=>$portal->portal_id));?>">
					<i class="entypo-plus-circled"></i>
					Add
				</a>																																																								<!-- /ACTION BUTTONS -->
			</div>
		</h2>
		<table id="page-list" class="table tableDnD table-bordered table-hover dataTable">
			<thead>
				<tr class="nodrag nodrop">
					<th class="col-md-6">
						<strong>Title</strong>
					</th>
					<th class="col-md-4">
						<strong>Friendly URL</strong>
					</th>
					<th class="col-md-2 text-center">
						<strong>Actions</strong>
					</th>
				</tr>
			</thead>
			<tbody>
				<?php if ( ! empty($cms)):?>
					<?php foreach ($cms as $c):?>
						<tr class="nodrag nodrop">
							<td class="col-mid-6">
								<?php echo $c->title;?>
							</td>
							<td class="col-mid-4">
								<?php echo $c->friendly_url;?>
							</td>
							<td class="text-center col-md-2">
								<div class="btn-group-action text-center">
									<div class="btn-group">
										<a href="<?php echo $this->createUrl('cms/update', array('id'=>$c->cms_id));?>" title="Edit" class="btn btn-default">
											<i class="entypo-pencil"></i>
										</a>
										<a href="<?php echo $this->createUrl('cms/delete', array('id'=>$c->cms_id));?>" title="Delete" class="btn btn-default">
											<i class="entypo-trash"></i>
										</a>
									</div>
								</div>
							</td>
						</tr>
					<?php endforeach;?>
				<?php else:?>
						<tr class="nodrag nodrop">
							<td colspan="3">No records found.</td>
						</tr>
				<?php endif;?>
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$("table#survey-list").tableDnD({
			onDragClass: "dragging",
			onDrop: function(table, row){
				$.ajax({
					type: 'GET',
					headers: { "cache-control": "no-cache" },
					async: false,
					url: '<?php echo $this->createUrl('survey/position');?>',
					data: $.tableDnD.serialize(),
					success: function(data) {
						$('td.dragHandle').each(function(index) {
							var position = index+1;
							$(this).html('<i class="entypo-arrow-combo"></i> '+ position);
						});
						toastr.success(data, '', {closeButton:true,showDuration:"300",timeOut: "3000"});
					}
				});
			},
			dragHandle: "dragHandle"
		});
	});
</script>

<div class="col-lg-12">
	<div class="table-responsive">
		<h2>
			Availments			
			<div class="pull-right">
				<!-- ACTION BUTTONS -->
				<a class="btn btn-default" href="<?php echo $this->createUrl('portal_availment/update', array('id'=>$portal->portal_id));?>">
					<i class="entypo-pencil"></i>
					Update
				</a>																																																								<!-- /ACTION BUTTONS -->
			</div>
		</h2>
		<table id="availment-list" class="table tableDnD table-bordered table-hover dataTable">
			<thead>
				<tr class="nodrag nodrop">
					<th class="col-mid-4">
						<strong>Name</strong>
					</th>
					<th class="text-center col-md-2">
						<strong>Duration</strong>
					</th>
					<th class="text-center col-md-2">
						<strong>Is Recurring</strong>
					</th>
				</tr>
			</thead>
			<tbody>
				<?php if ( ! empty($availments)):?>
					<?php foreach ($availments as $a):?>
						<tr id="<?php echo $a->availment_id;?>">
							<td class="col-mid-4">
								<?php echo $a->availment->name;?>
							</td>
							<td class="text-center col-md-2">
								<?php echo $a->duration/60;?> minutes
							</td>
							<td class="text-center col-md-1">
								<?php if ($a->is_recurring):?>
									<span class="label label-success" title="Yes">
										<i class="entypo-check"></i>
									</span>
								<?php else:?>
									<span class="label label-danger" title="No">
										<i class="entypo-minus"></i>
									</span>
								<?php endif;?>
							</td>
						</tr>
					<?php endforeach;?>
				<?php else:?>
					<tr class="nodrag nodrop">
						<td colspan="4">No records found.</td>
					</tr>
				<?php endif;?>
			</tbody>
		</table>
	</div>
</div>