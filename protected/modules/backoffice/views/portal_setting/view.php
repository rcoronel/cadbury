<?php echo CHtml::beginForm('', 'post', array('class'=>'form-horizontal form-groups-bordered', 'enctype'=>'multipart/form-data')); ?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary" data-collapsed="0">
				<div class="panel-heading">
					<div class="panel-title">Content</div>
				</div>
				<div class="panel-body" style="display: block;">
					<div class="form-group">
						<?php echo CHtml::label('Site Title', 'PortalConfiguration[SITE_TITLE]', array('class'=>'col-sm-3 control-label'));?>
						<div class="col-sm-5">
							<?php echo CHtml::textField('PortalConfiguration[SITE_TITLE]', PortalConfiguration::getValue('SITE_TITLE'), array('class'=>'form-control', 'maxlength'=>64)); ?>
						</div>
					</div>
					<div class="form-group">
						<?php echo CHtml::label('Landing page message', 'PortalConfiguration[LANDING_MESSAGE]', array('class'=>'col-sm-3 control-label'));?>
						<div class="col-sm-9">
							<?php echo CHtml::textArea('PortalConfiguration[LANDING_MESSAGE]', PortalConfiguration::getValue('LANDING_MESSAGE'), array('class'=>'form-control'));?>
							<script type="text/javascript">
								var editor = CKEDITOR.replace('PortalConfiguration_LANDING_MESSAGE');
								editor.on( 'change', function( evt ) {
									// getData() returns CKEditor's HTML content.
									$('#PortalConfiguration_LANDING_MESSAGE').html(evt.editor.getData());
								});
							</script>
						</div>
					</div>
					<div class="form-group">
						<?php echo CHtml::label('Welcome message', 'PortalConfiguration[WELCOME_BACK]', array('class'=>'col-sm-3 control-label'));?>
						<div class="col-sm-9">
							<?php echo CHtml::textArea('PortalConfiguration[WELCOME_BACK]', PortalConfiguration::getValue('WELCOME_BACK'), array('class'=>'form-control', 'rows'=>15));?>
							<script type="text/javascript">
								var editor = CKEDITOR.replace('PortalConfiguration_WELCOME_BACK');
								editor.on( 'change', function( evt ) {
									// getData() returns CKEditor's HTML content.
									$('#PortalConfiguration_WELCOME_BACK').html(evt.editor.getData());
								});
							</script>
						</div>
					</div>
					<div class="form-group">
						<?php echo CHtml::label('Redirect URL', 'PortalConfiguration[REDIRECT_URL]', array('class'=>'col-sm-3 control-label'));?>
						<div class="col-sm-5">
							<?php echo CHtml::textField('PortalConfiguration[REDIRECT_URL]', PortalConfiguration::getValue('REDIRECT_URL'), array('class'=>'form-control')); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-md-12">
			<div class="panel panel-primary" data-collapsed="0">
				<div class="panel-heading">
					<div class="panel-title">Appearance</div>
				</div>
				<div class="panel-body" style="display: block;">
					<div class="form-group">
						<?php echo CHtml::label('Logo', 'LOGO', array('class'=>'col-sm-3 control-label'));?>
						<div class="col-sm-5">
							<?php if ( ! empty($logo)):?>
								<a href="<?php echo Link::image_url("portal/{$portal->portal_id}/{$logo}");?>" target="_blank">
									<?php echo $logo;?>
								</a>
								<button type="button" class="btn btn-xs btn-danger" onclick="window.location='<?php echo $this->createUrl($this->id.'/delete', array('id'=>$portal->portal_id,'config'=>'LOGO'));?>';">
									<i class="entypo-cancel"></i>
								</button>
								<br>
							<?php endif;?>
							<?php echo CHtml::fileField('LOGO', '', array('class'=>'form-control')); ?>
						</div>
					</div>
					<div class="form-group">
						<?php echo CHtml::label('Inside Logo', 'INSIDE_LOGO', array('class'=>'col-sm-3 control-label'));?>
						<div class="col-sm-5">
							<?php if ( ! empty($inside_logo)):?>
								<a href="<?php echo Link::image_url("portal/{$portal->portal_id}/{$inside_logo}");?>" target="_blank">
									<?php echo $inside_logo;?>
								</a>
								<button type="button" class="btn btn-xs btn-danger" onclick="window.location='<?php echo $this->createUrl($this->id.'/delete', array('id'=>$portal->portal_id,'config'=>'INSIDE_LOGO'));?>';">
									<i class="entypo-cancel"></i>
								</button>
								<br>
							<?php endif;?>
							<?php echo CHtml::fileField('INSIDE_LOGO', '', array('class'=>'form-control')); ?>
						</div>
					</div>
					<div class="form-group">
						<?php echo CHtml::label('Center Image', 'CENTER_IMAGE', array('class'=>'col-sm-3 control-label'));?>
						<div class="col-sm-5">
							<?php if ( ! empty($center_image)):?>
								<a href="<?php echo Link::image_url("portal/{$portal->portal_id}/{$center_image}");?>" target="_blank">
									<?php echo $center_image;?>
								</a>
								<button type="button" class="btn btn-xs btn-danger" onclick="window.location='<?php echo $this->createUrl($this->id.'/delete', array('id'=>$portal->portal_id,'config'=>'CENTER_IMAGE'));?>';">
									<i class="entypo-cancel"></i>
								</button>
								<br>
							<?php endif;?>
							<?php echo CHtml::fileField('CENTER_IMAGE', '', array('class'=>'form-control')); ?>
						</div>
					</div>
					<div class="form-group">
						<?php echo CHtml::label('Favicon', 'FAVICON', array('class'=>'col-sm-3 control-label'));?>
						<div class="col-sm-5">
							<?php if ( ! empty($favicon)):?>
								<a href="<?php echo Link::image_url("portal/{$portal->portal_id}/{$favicon}");?>" target="_blank">
									<?php echo $favicon;?>
								</a>
								<button type="button" class="btn btn-xs btn-danger" onclick="window.location='<?php echo $this->createUrl($this->id.'/delete', array('id'=>$portal->portal_id,'config'=>'FAVICON'));?>';">
									<i class="entypo-cancel"></i>
								</button>
								<br>
							<?php endif;?>
							<?php echo CHtml::fileField('FAVICON', '', array('class'=>'form-control')); ?>
						</div>
					</div>
					<div class="form-group">
						<?php echo CHtml::label('Splash Image', 'SPLASH_IMG', array('class'=>'col-sm-3 control-label'));?>
						<div class="col-sm-5">
							<?php if ( ! empty($splash)):?>
								<a href="<?php echo Link::image_url("portal/{$portal->portal_id}/{$splash}");?>" target="_blank">
									<?php echo $splash;?>
								</a>
								<button type="button" class="btn btn-xs btn-danger" onclick="window.location='<?php echo $this->createUrl($this->id.'/delete', array('id'=>$portal->portal_id,'config'=>'SPLASH_IMG'));?>';">
									<i class="entypo-cancel"></i>
								</button>
								<br>
							<?php endif;?>
							<?php echo CHtml::fileField('SPLASH_IMG', '', array('class'=>'form-control')); ?>
						</div>
					</div>
					<div class="form-group">
						<?php echo CHtml::label('Footer Logo (Splash)', 'PortalConfiguration[POWERED_WELCOME]', array('class'=>'col-sm-3 control-label'));?>
						<div class="col-sm-5">
							<?php echo CHtml::dropDownList('PortalConfiguration[POWERED_WELCOME]', $powered_welcome, array(1=>'Display', 0=>'Hide'), array('class'=>'form-control'));?>
						</div>
					</div>
					<div class="form-group">
						<?php echo CHtml::label('Footer Logo (Inside)', 'PortalConfiguration[POWERED_INSIDE]', array('class'=>'col-sm-3 control-label'));?>
						<div class="col-sm-5">
							<?php echo CHtml::dropDownList('PortalConfiguration[POWERED_INSIDE]', $powered_inside, array(1=>'Display', 0=>'Hide'), array('class'=>'form-control'));?>
						</div>
					</div>
					<div class="form-group">
						<?php echo CHtml::label('CSS file', 'PortalConfiguration[CSS_FILE]', array('class'=>'col-sm-3 control-label'));?>
						<div class="col-sm-5">
							<?php if ( ! empty($css_file)):?>
								<a href="<?php echo Link::css_url("portal/{$portal->portal_id}/{$css_file}");?>" target="_blank">
									<?php echo $css_file;?>
								</a>
								<button type="button" class="btn btn-xs btn-danger" onclick="window.location='<?php echo $this->createUrl($this->id.'/delete', array('id'=>$portal->portal_id,'config'=>'CSS_FILE'));?>';">
									<i class="entypo-cancel"></i>
								</button>
								<br>
							<?php endif;?>
							<?php echo CHtml::textArea('PortalConfiguration[CSS_FILE]', $css_contents, array('class'=>'form-control span5', 'rows'=>15)); ?>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		<?php echo $other_settings;?>
	</div>

	<div class="form-group default-padding">
		<button type="submit" class="btn btn-primary">Submit</button>
	</div>
<?php echo CHtml::endForm(); ?>