<div class="col-md-12 mt">
	<div class="alert alert-danger alert-dismissable">
		<i class="fa fa-info-circle"></i>&nbsp;
		You don't have the necessary permission to do the action
	</div>
</div>