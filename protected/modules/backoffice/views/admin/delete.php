<div class="col-lg-12">
	<div class="panel panel-danger">
    <div class="panel-heading">
        <div class="panel-title">
			<h4>
				Delete 
				<strong><?php echo $admin->Name;?></strong> 
				(#<?php echo $admin->admin_id;?>)?
			</h4>
		</div>
    </div>
    <div class="panel-body">
			
		<p>Deleting this admin will <strong>remove</strong> the record from the database. This admin will not be able to login into the system.</p>

		<?php $form=$this->beginWidget('CActiveForm', array('htmlOptions' => array(	'class'=>'form-horizontal', 'role' => 'form', 'method' => 'post'))); ?>
			<?php echo CHtml::hiddenField('admin_id', $admin->admin_id)?>
			<?php echo CHtml::hiddenField('security_key', CPasswordHelper::hashPassword($admin->admin_id));?>
			<div class="buttons">
				<button type="submit" class="btn btn-danger">Delete</button>
				<button type="button" class="btn btn-default" onclick="location.href = '<?php echo $this->createUrl("{$this->id}/");?>';">Cancel</button>
			</div>
		<?php $this->endWidget();?>
	</div>
</div>