<div class="col-lg-12">
	<div class="panel panel-danger">
    <div class="panel-heading">
        <div class="panel-title">
			<h4>
				Delete 
				<strong><?php echo $scenario->title;?></strong> 
				scenario (#<?php echo $scenario->scenario_id;?>)?
			</h4>
		</div>
    </div>
    <div class="panel-body">
			
		<p>Deleting this scenario will <strong>disable</strong> the availments of the user.</p>

		<?php $form=$this->beginWidget('CActiveForm', array('htmlOptions' => array(	'class'=>'form-horizontal', 'role' => 'form', 'method' => 'post'))); ?>
			<?php echo CHtml::hiddenField('scenario_id', $scenario->scenario_id)?>
			<?php echo CHtml::hiddenField('security_key', CPasswordHelper::hashPassword($scenario->scenario_id));?>
			<div class="buttons">
				<button type="submit" class="btn btn-danger">Delete</button>
				<button type="button" class="btn btn-default" onclick="location.href = '<?php echo $this->createUrl("{$this->id}/");?>';">Cancel</button>
			</div>
		<?php $this->endWidget();?>
	</div>
</div>