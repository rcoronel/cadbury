
<script type="text/javascript">
	$(document).ready(function() {
		$("table.table-new-availment").tableDnD({
			onDragClass: "dragging",
			onDrop: function(table, row){
				$('table.table-new-availment td.dragHandle').each(function(index) {
					var position = index+1;
					$(this).find('span#availment-level').html(position);
					$(this).find('input#new_level').val(position);
				});
			},
			dragHandle: "dragHandle"
		});
		$("table.table-returning-availment").tableDnD({
			onDragClass: "dragging",
			onDrop: function(table, row){
				$('table.table-returning-availment td.dragHandle').each(function(index) {
					var position = index+1;
					$(this).find('span#availment-level').html(position);
					$(this).find('input#returning_level').val(position);
				});
			},
			dragHandle: "dragHandle"
		});
	});
</script>
<div class="col-md-6 col-md-offset-3">
	<div id="form-error" class="alert alert-danger" style="display: none;"></div>
</div>
<div class="col-md-12">
	<div class="" data-collapsed="0">
		<div class="">
			<?php echo CHtml::beginForm('', 'post', array('class'=>'form-horizontal form-groups-bordered', 'enctype'=>'multipart/form-data')); ?>
				<?php echo CHtml::hiddenField('ajax', 1);?>
				<div class="form-group">
					<?php echo CHtml::label('Title', 'Title', array('class'=>'control-label col-lg-3'));?>
					<div class="col-md-6">
						<?php echo CHtml::textField('title', $scenario->title, array('class'=>'form-control', 'maxlength'=>64, 'placeholder'=>'Title')); ?>
					</div>
				</div>
			
				<div class="panel panel-primary" data-collapsed="0">
					<div class="panel-heading">
						<div class="panel-title">New Users</div>
					</div>
					<div class="panel-body">
						<table class="table table-hover table-bordered tableDnD table-new-availment">
							<thead>
								<tr>
									<th class="col-md-2 text-center">Level</th>
									<th class="col-md-8"><?php echo CHtml::encode(Availment::model()->getAttributeLabel('name')); ?></th>
									<th class="col-md-2 text-center">Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php if (isset($new_scenarios) && ! empty($new_scenarios)):?>
									<?php foreach ($new_scenarios as $index=>$ns):?>
										<tr class="availments" id="<?php echo $ns->level;?>">
											<td class="availment-level text-center dragHandle">
												<i class="entypo-arrow-combo"></i> 
												<span id="availment-level"><?php echo $ns->level;?></span>
												<?php echo CHtml::hiddenField('new[level][]', $ns->level);?>
											</td>
											<td>
												<?php echo CHtml::dropDownList('new[availment_id][]', $ns->availment_id, $availments, array('class'=>'form-control'));?>
											</td>
											<td style="text-align:center">
												<button class="btn btn-danger" onclick="javascript:removeRow(this);" type="button">
													<i class="entypo-trash"></i>
												</button>
											</td>
										</tr>
									<?php endforeach;?>
								<?php else:?>
									<tr class="availments" id="1">
										<td class="availment-level text-center dragHandle">
											<i class="entypo-arrow-combo"></i> 
											<span id="availment-level">1</span>
											<?php echo CHtml::hiddenField('new[level][]', 1);?>
										</td>
										<td>
											<?php echo CHtml::dropDownList('new[availment_id][]', '', $availments, array('class'=>'form-control'));?>
										</td>
										<td style="text-align:center">
											<button class="btn btn-danger" onclick="javascript:removeRow(this);" type="button" title="Delete Row">
												<i class="entypo-trash"></i>
											</button>
										</td>
									</tr>
								<?php endif;?>
								<tr id="add-new-availment" class="nodrop nodrag">
									<td colspan="6">
										<?php echo CHtml::htmlButton('Add row', array('class'=>'btn btn-success', 'onclick'=>'javascript:addRow();'));?>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			
				<div class="panel panel-primary" data-collapsed="0">
					<div class="panel-heading">
						<div class="panel-title">Returning Users</div>
					</div>
					<div class="panel-body">
						<table class="table table-hover table-bordered tableDnD table-returning-availment">
							<thead>
								<tr>
									<th class="col-md-2 text-center">Level</th>
									<th class="col-md-8"><?php echo CHtml::encode(Availment::model()->getAttributeLabel('name')); ?></th>
									<th class="col-md-2 text-center">Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php if (isset($returning_scenarios) && ! empty($returning_scenarios)):?>
									<?php foreach ($returning_scenarios as $index=>$rs):?>
										<tr class="availments-returning">
											<td class="availment-level text-center dragHandle">
												<i class="entypo-arrow-combo"></i> 
												<span id="availment-level"><?php echo $rs->level;?></span>
												<?php echo CHtml::hiddenField('returning[level][]', $rs->level);?>
											</td>
											<td>
												<?php echo CHtml::dropDownList('returning[availment_id][]', $rs->availment_id, $availments, array('class'=>'form-control'));?>
											</td>
											<td style="text-align:center">
											<button class="btn btn-danger" onclick="javascript:removeRowReturning(this);" type="button">
												<i class="entypo-trash"></i>
											</button>
										</td>
										</tr>
									<?php endforeach;?>
								<?php else:?>
									<tr class="availments-returning">
										<td class="availment-level text-center dragHandle">
											<i class="entypo-arrow-combo"></i> 
											<span id="availment-level">1</span>
											<?php echo CHtml::hiddenField('returning[level][]', 1);?>
										</td>
										<td>
											<?php echo CHtml::dropDownList('returning[availment_id][]', '', $availments, array('class'=>'form-control'));?>
										</td>
										<td style="text-align:center">
											<button class="btn btn-danger" onclick="javascript:removeRowReturning(this);" type="button">
												<i class="entypo-trash"></i>
											</button>
										</td>
									</tr>
								<?php endif;?>
								<tr id="add-new-availment-returning" class="nodrop nodrag">
									<td colspan="6">
										<?php echo CHtml::htmlButton('Add row', array('class'=>'btn btn-success', 'onclick'=>'javascript:addRowReturning();'));?>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="">
					<div class="col-sm-offset-3 col-sm-1">
						<button type="submit" class="btn btn-primary">
							Submit
						</button>
					</div>
				</div>
			<?php echo CHtml::endForm();?>
		</div>
	</div>
</div>
<script>
	function addRow()
	{
		var cloned = $('table tbody tr.availments:first').clone().insertBefore('tr#add-new-availment');
		$(cloned).find('select option').prop('selected', false);
		updateLevel();
	}

	function addRowReturning()
	{
		var cloned = $('table tbody tr.availments-returning:first').clone().insertBefore('tr#add-new-availment-returning');
		$(cloned).find('select option').prop('selected', false);
		updateLevel();
	}

	function removeRow(source)
	{
		console.log(source);
		if ($('tr.availments').length == 1) {
			alert('Cannot delete last row');
		}
		else {
			$(source).parent().parent().remove();
			updateLevel();
		}
	}

	function removeRowReturning(source)
	{
		console.log(source);
		if ($('tr.availments-returning').length == 1) {
			alert('Cannot delete last row');
		}
		else {
			$(source).parent().parent().remove();
			updateLevel();
		}
	}

	function updateLevel()
	{
		var count = 1;
		$('tr.availments').each(function(){
			$(this).find('td.availment-level > span#availment-level').html(count);
			$(this).find('td.availment-level > input#new_level').val(count);
			$(this).attr('id', count);
			$("table.tableDnD").tableDnDUpdate();
			count++;
		});
		count = 1;
		$('tr.availments-returning').each(function(){
			$(this).find('td.availment-level > span#availment-level').html(count);
			$(this).find('td.availment-level > input#returning_level').val(count);
			$(this).attr('id', count);
			$("table.tableDnD").tableDnDUpdate();
			count++;
		});
	}
	
	$(document).ready(function(){
		$('form').on('submit',function(e){
			// check if submitted via AJAX
			if ($('input[name="ajax"]').length && $('input[name="ajax"]').val()) {
				// remove error style
				$('div.form-group').removeClass('has-error');

				// hide errors
				$('div#form-error').slideUp( 300 ).delay( 800 ).html();

				var postData = $(this).serializeArray();
				var formURL = $(this).attr("action");
				$.ajax({
					url : formURL,
					type: "POST",
					data : postData,
					dataType: 'json',
					success:function(data, textStatus, jqXHR) {
						if (! data.status) {
							$('div#form-error').slideDown(300).delay(800).html(data.message);
							$('div#form-error').show();

							if (typeof data.fields !== 'undefined') {
								$.each(data.fields, function(index, value){
									$('label[for="'+value+'"]').parent().addClass('has-error');
								});
							}
						}
						else {
							setTimeout(function() {
								$('#confirmation-modal .modal-body').html(data.message);
								$('#confirmation-modal').modal({backdrop: 'static', keyboard: false});
							}, 900);

							setTimeout(function() {
								if (data.url) {
									window.location.href = data.url;
								}
								else {
									window.location.href = currentIndex;
								}

							}, 2000);
						}
					},
					error: function(jqXHR, textStatus, errorThrown){}
				});
				e.preventDefault(); //STOP default action
			}
		});
		$(document).on("keydown", ".number-only", function(event) {
			console.log("number");
			if ( event.keyCode == 46 || event.keyCode == 8  ||  event.keyCode == 9) {
			} else {
				if (event.keyCode < 48 || event.keyCode > 57 ) {
					event.preventDefault();	
				} 	
			}
		});
	});
</script>
<?php $this->widget('widgets.ajax.Modal');?>