
<?php echo CHtml::beginForm();?>
	<?php echo CHtml::hiddenField('ajax', 1);?>
	<table class="table table-hover table-bordered">
		<thead>
			<tr>
				<th class="col-md-2"><?php echo CHtml::encode(Availment::model()->getAttributeLabel('name')); ?></th>
				<th class="col-md-5"><?php echo CHtml::encode(Availment::model()->getAttributeLabel('description')); ?></th>
				<th class="col-md-1 text-center"><?php echo CHtml::encode(Availment::model()->getAttributeLabel('Duration')); ?> (in minutes)</th>
				<th class="col-md-1 text-center"><?php echo CHtml::encode(Availment::model()->getAttributeLabel('Is_recurring')); ?></th>
				<th class="col-md-1 text-center">Status</th>
			</tr>
		</thead>
		<tbody>
			<?php if ( ! empty($availments)):?>
				<?php foreach ($availments as $a):?>
					<tr>
						<td>
							<?php echo $a->name;?>
							<button class="btn btn-xs pull-right" onclick="window.location.href='<?php echo $this->createUrl('avail_setting/update', array('id'=>$this->portal->portal_id, 'availment_id'=>$a->availment_id));?>';" type="button">
								<i class="entypo-pencil"></i>
							</button>
						</td>
						<td class="text-center">
							<?php echo CHtml::textField("description[$a->availment_id]", $a->description, array('class'=>'form-control span5 input-sm '));?>
						</td>
						<td class="text-center">
							<?php echo CHtml::textField("duration[$a->availment_id]", $a->duration, array('class'=>'form-control span1 input-sm number-only'));?>
						</td>
						<td>
							<?php echo CHtml::dropDownList("recurring[$a->availment_id]", $a->is_recurring, array(1=>'Yes', 0=>'No'), array('class'=>'form-control span2 input-sm'));?>
						</td>
						<td class="text-center">
							<?php echo CHtml::dropDownList("status[$a->availment_id]", $a->is_available, array('1'=>'Enabled', '0'=>'Disabled'), array('class'=>'form-control span2'));?>
						</td>
					</tr>
				<?php endforeach;?>
			<?php endif;?>
			<?php if($this->permissions->edit == 1): ?>
				<tr>
					<td colspan="8" class="text-right">
						<?php echo CHtml::button('Submit', array('type'=>'submit', 'class'=>'btn btn-primary'));?>
					</td>
				</tr>
			<?php endif; ?>
		</tbody>
	</table>
<?php echo CHtml::endForm();?>
<script>
$(document).ready(function(){
	$('select[id^="status"]').each(function() {
		var row = $(this).closest('tr');
		if ($(this).val() == 0) {
			$(row).find('select').prop('disabled', 'disabled');
			$(row).find('input').prop('disabled', 'disabled');
			$(this).prop('disabled', false);
		}
	});
	$('select[id^="status"]').on('change', function() {
		var row = $(this).closest('tr');
		if ($(this).val() == 0) {
			$(row).find('select').prop('disabled', 'disabled');
			$(row).find('input').prop('disabled', 'disabled');
			$(this).prop('disabled', false);
		}
		else {
			$(row).find('input').prop('disabled', false);
			$(row).find('select').prop('disabled', false);
		}

	});
	$('form').on('submit',function(e){
		// check if submitted via AJAX
		if ($('input[name="ajax"]').length && $('input[name="ajax"]').val()) {
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			$.ajax({
				url : formURL,
				type: "POST",
				data : postData,
				dataType: 'json',
				success:function(data, textStatus, jqXHR) {
					setTimeout(function() {
						toastr.success(data.message, '', {closeButton:true,showDuration:"300",timeOut: "3000"});
					  }, 1000);

				},
				error: function(jqXHR, textStatus, errorThrown){}
			});
			e.preventDefault(); //STOP default action
		}
	});
	$(document).on("keydown", ".number-only", function(event) {
		if ( event.keyCode == 46 || event.keyCode == 8  ||  event.keyCode == 9) {
		}
		else {
			if (event.keyCode < 48 || event.keyCode > 57 ) {
				event.preventDefault();	
			} 	
		}
	});
});
</script>
<?php $this->widget('widgets.ajax.Modal');?>

<h2>
	Scenarios
	<div class="pull-right">
		<a class="btn btn-default" href="<?php echo $this->createUrl('scenario/add', array('id'=>$this->portal->portal_id));?>">
			<i class="entypo-plus-circled"></i>
			Add
		</a>
	</div>
</h2>
<?php if ( ! empty($scenarios)):?>
	<?php foreach ($scenarios as $s):?>
		<div class="col-md-6">
			<div class="panel panel-primary" data-collapsed="0">
				<div class="panel-heading">
					<div class="panel-title"><?php echo $s->title;?></div>
					<div class="panel-options">
						<a class="bg" href="<?php echo $this->createUrl('scenario/update', array('id' => $s->scenario_id));?>">
							<i class="entypo-pencil"></i>
						</a>
						<a class="bg" href="<?php echo $this->createUrl('scenario/delete', array('id' => $s->scenario_id));?>">
							<i class="entypo-trash"></i>
						</a>
					</div>
				</div>
				<div class="panel-body">
					<p>New Users</p>
					<table class="table table-bordered">
						<thead>
							<tr>
								<th style="text-align:center">Level</th>
								<th>Availment</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($s->new as $new):?>
								<tr>
									<td style="text-align:center;"><?php echo $new->level;?></td>
									<td><?php echo $new->availment->name;?></td>
								</tr>
							<?php endforeach;?>
						</tbody>
					</table>

					<p>Returning Users</p>
					<table class="table table-bordered">
						<thead>
							<tr>
								<th style="text-align:center">Level</th>
								<th>Availment</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($s->returning as $returning):?>
								<tr>
									<td style="text-align:center;"><?php echo $returning->level;?></td>
									<td><?php echo $returning->availment->name;?></td>
								</tr>
							<?php endforeach;?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	<?php endforeach;?>
<?php endif;?>