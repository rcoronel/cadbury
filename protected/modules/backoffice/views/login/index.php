		<div class="login-container">
			<div class="login-header login-caret">
				<div class="login-content">
					<?php echo CHtml::image(Link::image_url('logo.png'), '', array('width'=>120));?>
					<div class="login-progressbar-indicator"> <h3>0%</h3> <span>logging in...</span> </div>
				</div>
			</div>
			<div class="login-progressbar"> <div></div> </div>
			<div class="login-form">
				<div class="login-content">
					<div id="form-login-error" class="alert alert-danger" style="display:none;"></div>
					<div class="form-login-error"></div>
					<?php $form=$this->beginWidget('CActiveForm', array('id'=>'form-login')); ?>
						<?php echo CHtml::hiddenField('ajax', 1);?>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">
									<i class="entypo-user"></i>
								</div>
								<?php echo $form->textField($admin, 'email', array('class'=>'form-control', 'placeholder'=>'E-mail Address', 'autocomplete'=>'off')); ?>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">
									<i class="entypo-key"></i>
								</div>
								<?php echo $form->passwordField($admin, 'password', array('class'=>'form-control', 'placeholder'=>'Password', 'autocomplete'=>'off', 'value'=>'')); ?>
							</div>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary btn-block btn-login">
								<i class="entypo-login"></i>
								Login In
							</button>
						</div>
					<?php $this->endWidget();?>
					<div class="login-bottom-links">
						<a href="#" class="link">
							Forgot your password?
						</a>
						<br>
						<a href="#">ToS</a> - <a href="#">Privacy Policy</a>
					</div>
				</div>
			</div>
		</div>