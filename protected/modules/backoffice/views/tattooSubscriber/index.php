<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default" data-collapsed="0">
			<div class="panel-body">
				<?php echo CHtml::beginForm('', 'post', array('class'=>'form-horizontal form-groups-bordered', 'enctype' => 'multipart/form-data')); ?>
					<div class="form-group">
						<?php echo CHtml::label('Site Title', 'Configuration[SITE_TITLE]', array('class' => 'control-label col-lg-3'));?>
						<div class="col-lg-5">
							<?php echo CHtml::textField('Configuration[SITE_TITLE]', $site_title, array('class' => 'form-control', 'maxlength' => 64)); ?>
						</div>
					</div>
					<div class="form-group">
						<?php echo CHtml::label('Logo', 'LOGO', array('class' => 'control-label col-lg-3'));?>
						<div class="col-lg-5">
							<?php if ( ! empty($logo)):?>
								<img src="<?php echo Link::image_url($logo);?>">
							<?php endif;?>
							<?php echo CHtml::fileField('LOGO', '', array('class' => 'form-control')); ?>
						</div>
					</div>
					<div class="form-group">
						<?php echo CHtml::label('Favicon', 'FAVICON', array('class' => 'control-label col-lg-3'));?>
						<div class="col-lg-5">
							<?php if ( ! empty($favicon)):?>
								<img src="<?php echo Link::image_url($favicon);?>">
							<?php endif;?>
							<?php echo CHtml::fileField('FAVICON', '', array('class' => 'form-control')); ?>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</div>
				<?php echo CHtml::endForm(); ?>
			</div>
		</div>
	</div>
</div>