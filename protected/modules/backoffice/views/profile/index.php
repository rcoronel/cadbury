<div class="profile-env">
    <header class="row">
        <div class="col-sm-2">
            <a href="#" class="profile-picture">
				<img src="<?php echo Link::Image_url("backoffice/admin/$admin->image");?>" class="img-responsive img-circle">
			</a>
        </div>
        <div class="col-sm-7">
            <ul class="profile-info-sections">
                <li>
                    <div class="profile-name">
						<strong>
							<a href="#"><?php echo $admin->firstname;?> <?php echo $admin->lastname;?></a>
							<a href="#" class="user-status is-online tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Online"></a>
						</strong>
						<span>
							<a href="#"><?php echo $admin->role->role;?></a>
						</span>
					</div>
                </li>
            </ul>
        </div>
    </header>
    <section class="profile-info-tabs">
        <div class="row">
            <div class="col-sm-offset-2 col-sm-10">
                <ul class="user-details">
                    <li>
                        <a href="#" data-toggle="tooltip" data-placement="top" data-original-title="E-mail Address">
							<i class="entypo-mail"></i>
							<?php echo $admin->email;?>
                        </a>
                    </li>
                    <li>
                        <a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Date Created">
							<i class="entypo-calendar"></i>
							<?php echo date('F d, Y', strtotime($admin->created_at));?>
                        </a>
                    </li>
                </ul>
				<!-- tabs for the profile links -->
                <ul class="nav nav-tabs">
                    <li>
						<a href="#" onclick="javascript:showForm('update');">
							<i class="entypo-pencil"></i>
							Edit Profile
						</a>
					</li>
					<li>
						<a href="#" onclick="javascript:showForm('change_password');">
							<i class="entypo-key"></i>
							Change Password
						</a>
					</li>
					<li>
						<a href="#" onclick="javascript:showForm('update_image');">
							<i class="entypo-picture"></i>
							Update Image
						</a>
					</li>
                </ul>
            </div>
        </div>
    </section>
</div>
<div id="profile-form"></div>

<?php $this->widget('widgets.ajax.Modal');?>
<script>
	function showForm(scenario)
	{
		$.ajax({
			url : currentIndex+'/'+scenario,
			type: "GET",
			dataType: 'json',
			success:function(data, textStatus, jqXHR) {
				setTimeout(function() {
					$('#loader-modal').modal('hide');
					$('div#profile-form').slideDown(300).delay(800).html(data.message);
				}, 900);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				//if fails     
			}
		});
	}
</script>