<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<div id="form-error" class="alert alert-danger" style="display: none;"></div>
	</div>
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-default" data-collapsed="0">
			<div class="panel-body">
				<?php $form=$this->beginWidget('CActiveForm', array('htmlOptions' => array('class'=>'form-horizontal form-groups-bordered', 'role' => 'form', 'method' => 'post'))); ?>
					<?php echo CHtml::hiddenField('ajax', 1);?>
					<div class="form-group">
						<?php echo $form->labelEx($admin, 'image', array('class' => 'control-label col-lg-3'));?>
						<div class="col-sm-5">
							<?php echo $form->fileField($admin, 'image', array('class'=>'form-control', 'onchange' => 'return ajaxFileUpload();'));?>
						</div>
					</div>
				<?php $this->endWidget();?>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function ajaxFileUpload()
	{
		$.ajaxFileUpload({
			url:currentIndex+'/update_image',
			secureuri:false,
			fileElementId:'Admin_image',
			dataType: 'json',
			success: function (data, status) {
				if (! data.status) {
					$('div#form-error').slideDown(300).delay(800).html(data.message);
					$('div#form-error').show();

					if (typeof data.fields !== 'undefined') {
						setTimeout(function() {
							$.each(data.fields, function(index, value){
								$('label[for="'+value+'"]').parent().addClass('has-error');
							});
						  }, 300);
					}
				}
				else {
					toastr.success(data.message, '', {closeButton:true,showDuration:"300",timeOut: "3000"});
					setTimeout(function() {
						window.location.href = currentIndex;
					  }, 2000);
				}
			},
			error: function (data, status, e) {
				alert(e);
			}
		});
		return false;
	};
</script>