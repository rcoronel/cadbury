<?php $this->beginContent('/layouts/main'); ?>

	<div class="sidebar-menu fixed">
		<div class="sidebar-menu-inner">
			<header class="logo-env">
				<div class="logo">
					<a href="<?php echo $this->createUrl('dashboard/');?>">
						<?php echo CHtml::image(Link::image_url('logo.png'), '', array('width'=>120));?>
					</a>
				</div>
				<div class="sidebar-collapse">
					<a href="#" class="sidebar-collapse-icon with-animation">
						<i class="entypo-menu"></i>
					</a>
				</div>
				<div class="sidebar-mobile-menu visible-xs">
					<a href="#" class="with-animation">
						<i class="entypo-menu"></i>
					</a>
				</div>
			</header>
			<ul id="main-menu" class="main-menu">
				<?php foreach ($this->context->menus as $menu):?>
					<li class="root-level <?php echo ($menu->has_subpages) ? 'has-sub' : '';?> <?php echo ($this->getId()==$menu->classname OR $this->menu->parent_admin_menu_id==$menu->admin_menu_id) ? 'active opened' : '';?>">
						<a href="<?php echo $this->createUrl("{$menu->classname}/");?>">
							<?php if ($menu->img):?><i class="<?php echo $menu->img;?>"></i><?php endif;?><span class="title"><?php echo $menu->title;?></span>
						</a>

						<?php if ($menu->has_subpages):?>
							<ul>
								<?php foreach ($this->context->submenus as $submenu):?>
									<?php if ($submenu->parent_admin_menu_id == $menu->admin_menu_id):?>
										<li class="<?php echo ($this->getId()==$submenu->classname) ? 'active' : '';?>">
											<a href="<?php echo $this->createUrl("{$submenu->classname}/");?>">
												<span class="title"><?php echo $submenu->title;?></span>
											</a>
										</li>
									<?php endif;?>
								<?php endforeach;?>
							</ul>
						<?php endif;?>
					</li>
				<?php endforeach;?>
			</ul>
		</div>
	</div>

	<div class="main-content">
		<div class="row">
			<!-- Profile Info and Notifications -->
			<div class="col-md-6 col-sm-8 clearfix">
				<ul class="user-info pull-left pull-none-xsm">
					<!-- Profile Info -->
					<li class="profile-info dropdown">
						<!-- add class "pull-right" if you want to place this from right -->
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<img src="<?php echo Link::image_url("backoffice/admin/{$this->context->admin->image}");?>" alt="" class="img-circle" width="44">
							<?php echo $this->context->admin->firstname;?> <?php echo $this->context->admin->lastname;?>
						</a>
						<ul class="dropdown-menu">
							<!-- Reverse Caret -->
							<li class="caret"></li>
							<li>
								<a href="<?php echo $this->createUrl('profile/');?>">
									<i class="entypo-user"></i> Edit Profile
								</a>
							</li>
						</ul>
					</li>
				</ul>

			</div>
			<!-- Raw Links -->
			<div class="col-md-6 col-sm-4 clearfix hidden-xs">
				<ul class="list-inline links-list pull-right">
					<li class="sep"></li>
					<li>
						<a href="<?php echo $this->createUrl('logout/');?>">
							Log Out <i class="entypo-logout right"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
		<hr>
		<script>
			$(document).ready(function(){
				$("h2#header-sticker").sticky({topSpacing:0, zIndex:999});
				$("div#div-sticker").sticky({topSpacing:65, zIndex:999});
			});
		</script>
		<h2 id="header-sticker" style="background:#fff;padding: 10px;">
			<?php echo $this->menu->title;?>
				
			<div class="pull-right">
				<!-- ACTION BUTTONS -->
				<?php if ( ! empty($this->actionButtons)):?>
					<?php foreach($this->actionButtons as $action => $button):?>
						<?php if ($action === $this->action->id):?>

							<?php if (isset($button['add'])):?>
								<a class="btn btn-default" href="<?php echo $button['add']['link'];?>">
									<i class="entypo-plus-circled"></i>
									Add
								</a>
							<?php endif;?>

							<?php if (isset($button['back'])):?>
								<a class="btn btn-default" href="<?php echo $button['back']['link'];?>">
									<i class="entypo-back"></i>
									Back to list
								</a>
							<?php endif;?>

						<?php endif;?>
					<?php endforeach;?>
				<?php endif;?>
				<!-- /ACTION BUTTONS -->
			</div>
		</h2>
			
		<div class="row">
			<div class="col-lg-12">
				<?php if(Yii::app()->user->hasFlash('success')): ?>
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<i class="entypo-info-circled"></i>&nbsp;
						<?php echo Yii::app()->user->getFlash('success'); ?>
					</div>
				<?php endif;?>
				<?php if(Yii::app()->user->hasFlash('fail')): ?>
					<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<i class="entypo-info-circled"></i>&nbsp;
						<?php echo Yii::app()->user->getFlash('fail'); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
		<?php echo $content;?>
	</div>
<?php $this->endContent(); ?>