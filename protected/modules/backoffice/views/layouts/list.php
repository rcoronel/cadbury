<?php if (array_key_exists('position', $object->attributes)):?>
	<script type="text/javascript">
		$(document).ready(function() {
			$("table.tableDnD").tableDnD({
				onDragClass: "dragging",
				onDrop: function(table, row){
					$.ajax({
						type: 'GET',
						headers: { "cache-control": "no-cache" },
						async: false,
						url: currentIndex+'/position',
						data: $.tableDnD.serialize(),
						success: function(data) {
							$('td.dragHandle').each(function(index) {
								var position = index+1;
								$(this).html('<i class="entypo-arrow-combo"></i> '+ position);
							});
							setTimeout(function() {
								toastr.success(data, '', {closeButton:true,showDuration:"300",timeOut: "3000"});
							}, 1000);
						}
					});
				},
				dragHandle: "dragHandle"
			});
		});
	</script>
<?php endif;?>

<div class="row">
	<div class="col-lg-12">
		<div class="table-responsive">
			<?php $form=$this->beginWidget('CActiveForm', array('action'=>$this->createUrl('/'.$this->route,$_GET), 'htmlOptions'=>array('id'=>'filter-form', 'class'=>'form-horizontal', 'role'=>'form', 'method'=>'get'))); ?>
			<div id="div-sticker">
				<table class="table tableDnD table-bordered table-hover dataTable">
					<thead>
						<tr class="nodrag nodrop">
							<?php foreach ($row_fields as $header=>$attribute):?>
								<th class="<?php echo (isset($attribute['class'])) ? $attribute['class'] : '';?>">
									<strong>
										<?php echo CHtml::encode($object->getAttributeLabel($header)); ?>
									</strong>
									<a href="<?php echo $this->createUrl('/'.$this->route, array_replace($_GET, array('order'=>$header, 'by'=>'asc')));?>" title="Ascending order" class="sort-list">
										<i class="entypo-up-dir"></i>
									</a>
									<a href="<?php echo $this->createUrl('/'.$this->route, array_replace($_GET, array('order'=>$header, 'by'=>'desc')));?>" title="Descending order" class="sort-list">
										<i class="entypo-down-dir"></i>
									</a>
								</th>
							<?php endforeach;?>
							<?php if (isset($row_actions) && ! empty($row_actions)):?>
								<th class="text-center col-md-2">
									<strong>
										<?php echo CHtml::encode('Actions');?>
									</strong>
								</th>
							<?php endif;?>
						</tr>
						<tr class="nodrag nodrop">
							<?php foreach ($row_fields as $header=>$attribute):?>
								<th class="<?php echo (isset($attribute['class'])) ? $attribute['class'] : '';?>">
									<?php if ($attribute['type'] == 'text'):?>
										<?php echo $form->textField($object, $header, array('class'=>'form-control input-sm'));?>
									<?php endif;?>

									<?php if ($attribute['type'] == 'select'):?>
										<?php echo $form->dropDownList($object, $header, $attribute['value'], array('class'=>'form-control input-sm', 'prompt'=>'---'));?>
									<?php endif;?>
								</th>
							<?php endforeach;?>
							<?php if (isset($row_actions) && ! empty($row_actions)):?>
								<th class="text-center col-md-2">
									<div class="btn-group">
										<button type="submit" name="search-filter" value="1" class="btn btn-sm" title="Search">
											<i class="entypo-search"></i>
										</button>
										<button type="submit" name="reset-filter" id="reset-filter" value="2" class="btn btn-sm" title="Reset">
											<i class="entypo-erase"></i>
										</button>
									</div>
								</th>
							<?php endif;?>
						</tr>
					</thead>
				</table>
			</div>
				
			<?php $this->endWidget();?>
			
			<?php $form=$this->beginWidget('CActiveForm', array('action'=>$this->createUrl('/'.$this->route,$_GET), 'htmlOptions'=>array('id'=>'filter-form', 'class'=>'form-horizontal', 'role'=>'form', 'method'=>'get'))); ?>
				<table id="page-list" class="table tableDnD table-bordered table-hover dataTable">
					<tbody>
						<?php echo $objectList;?>
					</tbody>
				</table>
			<?php $this->endWidget();?>
			<div id="pagination">
				<?php echo $pagination;?>
			</div>
		</div>
	</div>
</div>
<?php $this->widget('widgets.ajax.Modal');?>
<script type="text/javascript">
	$(document).ready(function() {
		$('a.sort-list').click(function(e){
			e.preventDefault();
			$('form#filter-form').attr('action', $(this).attr('href'));
			$('form#filter-form').submit();
		});
		$('form#filter-form').on('submit', function(e){
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			$.ajax({
				url : formURL,
				type: "POST",
				data : postData,
				dataType: 'json',
				success:function(data, textStatus, jqXHR) {
					if (data.status){
						setTimeout(function() {
							$('table#page-list > tbody').html(data.message);
							$('div#pagination').html(data.pagination);
						}, 900);
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					if (errorThrown == 'Forbidden') {
						location.reload();
					}
				}
			});
			e.preventDefault();
		});
		$('#reset-filter').on('click', function(){
			$('form#filter-form input[type="text"]').val('');
			$('form#filter-form select').val('');
			$('form#filter-form').find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
		});
	});
</script>