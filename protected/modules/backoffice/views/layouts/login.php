<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="author" content="Yondu">
		<title>Login</title>
		<?php echo CHtml::cssFile(Link::css_url('bootstrap/v3/bootstrap.min.css'));?>
		<?php echo CHtml::cssFile(Link::css_url('entypo/entypo.css'));?>
		<?php echo CHtml::cssFile('//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic');?>
		<?php echo CHtml::cssFile(Link::css_url('backoffice/main.css'));?>
		<?php echo CHtml::cssFile(Link::css_url('backoffice/theme.css'));?>
		<?php echo CHtml::cssFile(Link::css_url('backoffice/form.css'));?>
		<?php echo CHtml::cssFile(Link::css_url('backoffice/blue.css'));?>
		<?php echo CHtml::scriptFile(Link::js_url('jquery/jquery-1.11.2.min.js'));?>
		<!--[if lt IE 9]>
			<script src="http://demo.neontheme.com/assets/js/ie8-responsive-file-warning.js"></script>
		<![endif]-->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		<script>
			var baseURL = "<?php echo Link::base_url()?>";
			var currentIndex = "<?php echo $this->createUrl($this->id.'/');?>";
			var currentURL = "<?php echo $this->createUrl('/'.$this->route,$_GET);?>";
			var token = "<?php echo Yii::app()->request->csrfToken;?>";
		</script>
	</head>
	<body class="page-body skin-blue login-page login-form-fall loaded">
		
		<?php echo $content;?>
		
		<?php echo CHtml::scriptFile(Link::js_url('bootstrap/bootstrap.min.js'));?>
		<?php echo CHtml::scriptFile(Link::js_url('gsap/main.js'));?>
		<?php echo CHtml::scriptFile(Link::js_url('jquery/ui/jquery-ui-1.10.3.minimal.min.js'));?>
		<?php echo CHtml::scriptFile(Link::js_url('backoffice/joinable.js'));?>
		<?php echo CHtml::scriptFile(Link::js_url('backoffice/resizable.js'));?>
		<?php echo CHtml::scriptFile(Link::js_url('backoffice/api.js'));?>
		<?php echo CHtml::scriptFile(Link::js_url('backoffice/custom.js'));?>
		<?php echo CHtml::scriptFile(Link::js_url('backoffice/login.js'));?>
		<?php echo CHtml::scriptFile(Link::js_url('jquery/jquery.validate.min.js'));?>
		<!-- JavaScripts initializations and stuff -->
		<!-- Demo Settings -->
	</body>
</html>