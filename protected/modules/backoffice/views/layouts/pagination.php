<div class="dataTables_wrapper">
	<div class="dataTables_info" role="status" aria-live="polite">
		Showing <?php echo $from;?> to <?php echo $to;?> of <?php echo $pager->itemCount;?> entries
	</div>
	<div class="dataTables_paginate paging_simple_numbers">
		<a href="<?php echo $this->createUrl('/'.$this->route, array_replace($_GET, array('page'=>$current_page-1)));?>" class="paginate_button previous <?php echo ($current_page == 1) ? 'disabled' : '';?>" aria-controls="table-3" data-dt-idx="0" tabindex="0" id="table-3_previous">
			Previous
		</a>
		<span>
			<?php for($i=1;$i<=$pages;$i++):?>
				<a href="<?php echo $this->createUrl('/'.$this->route, array_replace($_GET, array('page'=>$i)));?>" class="paginate_button <?php echo ($current_page==$i) ? 'current' : '';?>" aria-controls="table-3" data-dt-idx="2" tabindex="0">
					<?php echo $i;?>
				</a>
			<?php endfor;?>
		</span>
		<a href="<?php echo $this->createUrl('/'.$this->route, array_replace($_GET, array('page'=>$current_page+1)));?>" class="paginate_button next <?php echo ($current_page == $pages) ? 'disabled' : '';?>" aria-controls="table-3" data-dt-idx="7" tabindex="0" id="table-3_next">
			Next
		</a>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('a.paginate_button').click(function(e){
			e.preventDefault();
			$('form#filter-form').attr('action', $(this).attr('href'));
			$('form#filter-form').submit();
		});
	});
</script>