<div class="row">
	<div class="col-md-12">
		<div class="col-sm-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="panel-title">
						<?php echo CHtml::beginForm('', 'post', array('id'=>'stat-form', 'class'=>'form-horizontal form-groups-bordered'));?>
							<div class="form-group">
								<label class="col-sm-2 control-label">
									<strong>Users of</strong>
								</label>
								<div class="col-sm-3">
									<div class="input-group">
										<?php echo CHtml::dropDownList('availment_id', '', CHtml::listData($availments, 'availment_id', 'name'), array('class'=>'form-control', 'prompt'=>'--- All'));?>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="input-group">
										<?php echo CHtml::dropDownList('portal_id', '', CHtml::listData($portals, 'portal_id', 'name'), array('class'=>'form-control', 'prompt'=>'--- All'));?>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="input-group">
										<input name="date" type="text" class="form-control datepicker" data-format="D, MM dd, yyyy" value="<?php echo date('D, F d, Y');?>">
										<div class="input-group-addon">
											<a href="#">
												<i class="entypo-calendar"></i>
											</a>
										</div>
									</div>
								</div>
								<button class="btn btn-blue" type="submit">Show</button>
							</div>
						<?php echo CHtml::endForm();?>
					</div>
				</div>
				<div class="panel-body" id="stat_numbers">
				</div>
			</div>
		</div>
		<div id="stat-count" class="col-sm-3">
			
		</div>
		
	</div>
	
</div>

<?php $this->widget('widgets.ajax.Modal');?>
<script lang="text/javascript">
	$(document).ready(function() {
		$('form#stat-form').on('submit', function(e){
			show_stats();
			e.preventDefault();
		});
	});
	
	function show_stats()
	{
		var postData = $('form#stat-form').serializeArray();
		$.ajax({
				type:'post',
				url: currentIndex+'/poll',
				data: postData,
				dataType: 'json',
				success: function (data){
					$('div#stat-count').html(data.message);
					
				}
			});
		
	}
//	function show_stats(id)
//	{
//		$('input[name="nas_id"]').val(id);
//		$.ajax({
//			type: "POST",
//			url: currentIndex+'/poll',
//			data: {'YII_CSRF_TOKEN':token, 'id':id, 'date':$('input[name="date"]').val()},
//			success: function(data) {
//				$('#stat_numbers').html(data.message);
//				$(".tile-stats").each(function(i, el) {
//					var $this = $(el),
//						$num = $this.find('.num'),
//						start = attrDefault($num, 'start', 0),
//						end = attrDefault($num, 'end', 0),
//						prefix = attrDefault($num, 'prefix', ''),
//						postfix = attrDefault($num, 'postfix', ''),
//						duration = attrDefault($num, 'duration', 1000),
//						delay = attrDefault($num, 'delay', 1000);
//
//					if(start < end) {
//						if(typeof scrollMonitor == 'undefined') {
//							$num.html(prefix + end + postfix);
//						}
//						else {
//							var tile_stats = scrollMonitor.create(el);
//							tile_stats.fullyEnterViewport(function(){
//								var o = {curr: start};
//								TweenLite.to(o, duration/1000, {curr: end, ease: Power1.easeInOut, delay: delay/1000, onUpdate: function(){
//									$num.html(prefix + Math.round(o.curr) + postfix);
//								}
//								});
//							tile_stats.destroy();
//							});
//						}
//					}
//				});
//				
//				$('div#stat_chart').html('');
//				new Morris.Donut({
//					element: 'stat_chart',
//					data: data.chart_data
//				});
//				
//				$('div#gender_chart').html('');
//				new Morris.Bar({
//					element: 'gender_chart',
//					data: data.bar_data,
//					xkey: 'label',
//					ykeys: ['value'],
//					labels: ['Count']
//				});
//			},
//			dataType: "json"
//		});
//	};
</script>