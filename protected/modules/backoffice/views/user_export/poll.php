<div class="tile-stats tile-white-blue">
	<div class="tile-content">
		<div class="num"><?php echo $count;?></div>
	</div>
	<div class="tile-footer">
		<?php echo CHtml::beginForm();?>
			<?php echo CHtml::hiddenField('export', '1');?>
			<?php echo CHtml::hiddenField('availment_id', $availment_id);?>
			<?php echo CHtml::hiddenField('portal_id', $portal_id);?>
			<?php echo CHtml::hiddenField('date', $date);?>
			<?php echo CHtml::htmlButton('Export', array('type'=>'submit', 'class'=>'btn btn-block'));?>
		<?php echo CHtml::endForm();?>
	</div>
</div>