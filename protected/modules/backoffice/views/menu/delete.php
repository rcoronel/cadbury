<div class="col-lg-12">
	<div class="panel panel-danger">
    <div class="panel-heading">
        <div class="panel-title">
			<h4>
				Delete 
				<strong><?php echo $menu->title;?></strong> 
				menu (#<?php echo $menu->admin_menu_id;?>)?
			</h4>
		</div>
    </div>
    <div class="panel-body">
			
		<p>Deleting this menu will <strong>disable</strong> display of links including the sub-menus. For security purposes, the actual file will not be deleted.</p>

		<?php $form=$this->beginWidget('CActiveForm', array('htmlOptions' => array(	'class'=>'form-horizontal', 'role' => 'form', 'method' => 'post'))); ?>
			<?php echo CHtml::hiddenField('admin_menu_id', $menu->admin_menu_id)?>
			<?php echo CHtml::hiddenField('security_key', CPasswordHelper::hashPassword($menu->admin_menu_id));?>
			<div class="buttons">
				<button type="submit" class="btn btn-danger">Delete</button>
				<button type="button" class="btn btn-default" onclick="location.href = '<?php echo $this->createUrl("{$this->id}/");?>';">Cancel</button>
			</div>
		<?php $this->endWidget();?>
	</div>
</div>