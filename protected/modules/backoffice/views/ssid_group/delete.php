<div class="col-lg-12">
	<div class="panel panel-danger">
    <div class="panel-heading">
        <div class="panel-title">
			<h4>
				Delete 
				<strong>
					<?php echo $group->portal->name;?> | <?php echo $group->ssid->ssid;?> | <?php echo $group->ap_group->name;?>
				</strong>
				Group(#<?php echo $group->portal_ssid_group_id;?>)?
			</h4>
		</div>
    </div>
    <div class="panel-body">
			
		<p>Deleting this will <strong>remove</strong> the record from the database.</p>

		<?php $form=$this->beginWidget('CActiveForm', array('htmlOptions'=>array('class'=>'form-horizontal', 'role'=>'form', 'method'=>'post'))); ?>
			<?php echo CHtml::hiddenField('portal_ssid_group_id', $group->portal_ssid_group_id)?>
			<?php echo CHtml::hiddenField('security_key', CPasswordHelper::hashPassword($group->portal_ssid_group_id));?>
			<div class="buttons">
				<button type="submit" class="btn btn-danger">Delete</button>
				<button type="button" class="btn btn-default" onclick="location.href = '<?php echo $this->createUrl("{$this->id}/");?>';">Cancel</button>
			</div>
		<?php $this->endWidget();?>
	</div>
</div>