<?php

class AdminIdentity extends CUserIdentity
{
	private $admin_id;
	
	/**
	 * Authenticates a user.
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		// Check for existing account
		$admin = Admin::model()->findByAttributes(array('email' => $this->username));
		if ($admin === NULL)
		{
			$this->errorCode = self::ERROR_USERNAME_INVALID;
			$this->errorMessage = 'No record found';
			return FALSE;
		}
		
		// check for password
		if ( ! CPasswordHelper::verifypassword($this->password, $admin->password)) {
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
			$this->errorMessage = 'Incorrect password';
			return FALSE;
		}
		
		$this->admin_id = $admin->admin_id;
		$this->username = $admin->email;
		$this->errorCode = self::ERROR_NONE;
		return TRUE;
	}
	
	/*
	* Override getId() method
	*/
	public function getId()
	{
		return $this->admin_id;
	}	
}
