<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class BackofficeController extends Controller
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='/layouts/column2';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	//public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
	
	/* permission by profile */
	public $permissions;
	
	// links for pages
	public $menu;
	
	/**
	 *
	 * @var array Navigational menus in each action
	 * Format is action ID => menus
	 */
	public $nav;
	public $fields_row;
	public $rowAction;
	
	/**
	 * Available buttons for each action
	 * 
	 * @var array Navigational menus in each action
	 * Format is action ID => menus
	 */
	public $actionButtons = array();
	
	/**
	 * Constructor
	 *
	 * Called automatically
	 * Inherits functions from the parent class
	 * 
	 * @access	public
	 * @param	string $id Controller ID
	 * @param	string $module Module name
	 */
	public function __construct($id, $module = null)
	{
		parent::__construct($id, $module);
	}
	
	protected function beforeAction($action)
	{
		// Public pages that can be accessed by anyone
		// Must contain the classname of the page
		$public_menus = array('login', 'logout', 'password', 'profile');
		
		$access = 1;
		if ( ! in_array(Yii::app()->controller->id, $public_menus)) {
			switch ($action->id) {
				case 'index':
				case 'view':
					$access = $this->permissions->view;
				break;

				case 'add':
					$access = $this->permissions->add;
				break;

				case 'update':
					$access = $this->permissions->edit;
				break;

				case 'delete':
					$access = $this->permissions->delete;
				break;

				default:
					$access = 1;
				break;
			}

			if ( ! $access) {
				$this->render('/dashboard/deny');
				return FALSE;
			}
		}
		return TRUE;
	}
	
	/**
	 * Display object list
	 * 
	 * @access protected
	 * @param object $object Current object
	 * @param array $rows Rows to be displayed
	 * @param int $limit Limit per page
	 * @param bool $unset Check if attributes of the current object is to be emptied
	 * @param array $compare Additional comparison check
	 * @return array
	 */
	protected function _showList($object, $rows = array(), $limit = 25, $unset = FALSE, $compare = array())
	{
		$classname = get_class($object);
		
		// Check if needed to unset default values
		if ($unset) {
			$object->unsetAttributes();
		}
		
		// Put POST data on object attributes
		$object->attributes = Yii::app()->request->getPost($classname);
		
		// Initialize object criteria
		$criteria = $object->search()->criteria;
		if ( ! empty($compare)) {
			foreach ($compare as $field=>$value) {
				$criteria->compare($field, $value);
			}
		}
		
		// Add / update order criteria based on URL query
		if ($order = Yii::app()->getRequest()->getQuery('order') AND $by = Yii::app()->getRequest()->getQuery('by')) {
			$criteria->order = "$order $by";
		}

		// Assign limit and offset
		$criteria->limit = $limit;
		$criteria->offset = (Yii::app()->request->getQuery('page', 1) * $criteria->limit) - $criteria->limit;
		
		// List object content
		$list = $object::model()->findAll($criteria);
		$view = $this->renderPartial('/layouts/list-body', array('object'=>$object, 'objectList'=>$list, 'row_fields'=>$rows['fields'], 'row_actions'=>$rows['actions']), TRUE);
		
		// Count the rows based on criteria
		$total = $object::model()->count($criteria);
		
		// Initialize pagination
		$pager = new CPagination($total);
		$pager->pageSize = $criteria->limit;
		$pager->applyLimit($criteria);
		
		// Pagination variables
		$pages = ceil($pager->itemCount/$pager->pageSize); #total no. of pages
		$current_page = $pager->currentPage+1; #current page based on pagination
		$from = ($current_page*$pager->limit)-($pager->limit-1);
		$to = ($total > $current_page*$pager->limit) ? $current_page*$pager->limit : $total;
		
		$pagination = $this->renderPartial('/layouts/pagination', array('pager'=>$pager, 'pages'=>$pages, 'current_page'=>$current_page, 'from'=>$from, 'to'=>$to), TRUE);
		
		if (Yii::app()->request->isAjaxRequest) {
			die(CJSON::encode(array('status'=>1, 'message'=>$view, 'pagination'=>$pagination)));
		}
		return array('view'=>$view, 'pagination'=>$pagination);
	}
	
	/**
	 * Log Administrator actions
	 * 
	 * @param mixed $reference Reference key for the controller
	 * @return boolean
	 */
	public function log($reference = 0)
	{
		$log = new AdminLog;
		$log->admin_id = $this->context->admin->admin_id;
		$log->admin_role_id = $this->context->admin->admin_role_id;
		$log->classname = $this->id;
		$log->action = $this->action->id;
		$log->reference = $reference;
		$log->validate() && $log->save();
		return TRUE;
	}
}