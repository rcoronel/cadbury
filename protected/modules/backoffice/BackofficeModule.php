<?php

class BackofficeModule extends CWebModule
{
	public $defaultController = 'dashboard';
	
	public function init()
	{
		// import the module-level models and components
		$this->setImport(array(
			'backoffice.models.*',
			'backoffice.components.*',
		));
		
		$this->setComponents(array(
			'errorHandler'=>array(
				'errorAction'=>'/backoffice/default/error',
			),
			'admin' => array(
				'class' => 'AdminComponent',  
				'allowAutoLogin' => true,				
				'loginUrl' => Yii::app()->createUrl('/backoffice/login'),
			),
		));
		
		Yii::app()->user->setStateKeyPrefix('_backoffice');
	}

	/**
	 * Function to be called before calling any controller action
	 * 
	 * @access public
	 * @param object $controller
	 * @param object $action
	 * @return bool
	 */
	public function beforeControllerAction($controller, $action)
	{
		if (parent::beforeControllerAction($controller, $action)) {
			// Context object
			$context = Context::getContext();
			
			switch ($controller->id) {
				case 'login':
					// Check if user is already logged in
					if (Yii::app()->getModule('backoffice')->admin->getId()) {
						Yii::app()->controller->redirect(array('dashboard/'));
					}
				break;
			
				case 'logout':
				break;
			
				default:
					// Check if user is logged in
					if (Yii::app()->getModule('backoffice')->admin->isGuest) {
						Yii::app()->getModule('backoffice')->admin->setState('redirect', $controller->createUrl('/'.$controller->route,$_GET));
						Yii::app()->getModule('backoffice')->admin->loginRequired();
					}

					$context->admin = Admin::model()->findByPk(Yii::app()->getModule('backoffice')->admin->getId());
					$context->menus = AdminMenu::getMainMenus();
					$context->submenus = AdminMenu::getSubMenus();
//					$context->menus = AdminMenu::model()->findAllByAttributes(array('parent_admin_menu_id'=>0, 'Display'=>1), array('order'=>'position ASC'));
//					$context->submenus = AdminMenu::model()->findAllByAttributes(array('Display'=>1), array('order'=>'position ASC', 'condition'=>'parent_admin_menu_id != 0'));
					foreach ($context->menus as $m) {
						$m->has_subpages = AdminMenu::model()->countByAttributes(array('parent_admin_menu_id' => $m->admin_menu_id, 'display'=>1));
					}
					
					// Get the current page details
					$controller->menu = AdminMenu::model()->findByAttributes(array('classname'=>$controller->id));
					$controller->permissions = AdminRolePermission::model()->findByAttributes(array('admin_role_id'=>$context->admin->admin_role_id, 'admin_menu_id' => $controller->menu->admin_menu_id));
				break;
			}
			return true;
		}
		else {
			return false;
		}
	}
}
