<div class="row">
	<div id="captive-portal-wrapper">
		<div id="captive-portal-image">
			<div class="col-lg-6">
				<div class="hidden-lg">
					<?php echo PortalConfiguration::getValue('LANDING_MESSAGE');?>
				</div>
				<?php if ($this->center_image):?>
					<?php echo CHtml::image($this->center_image, $this->site_title, array('class'=>'img-responsive globe-continue-logo'));?>
				<?php endif;?>
			</div>
		</div>
		<!-- captive-portal-image -->
		<div id="captive-portal-content">
			<div class="col-lg-6">
				<div class="row">
					<div class="col-lg-12 visible-lg">
						<?php echo PortalConfiguration::getValue('LANDING_MESSAGE');?>
					</div>
					 <div id="captive-portal-btn-holder" class="col-lg-12">
						<div id="globe-btn-holder" >
							<?php echo CHtml::link('Get Connected', 'javascript:void(0);', array('class'=>'btn btn-wifi btn-continue btn-shadow', 'onclick'=>"window.location='{$url}'", 'id'=>'get-connected'));?>
						</div>
						<p>By clicking the button, you agree to the <a href="#" data-toggle="modal" data-target="#terms-modal">Terms of Service</a> and <a href="#" data-toggle="modal" data-target="#policy-modal">Privacy Policy</a>.</p>
						<br>
						<p>No Facebook account? Log in using your <a href="<?php echo $url_mobile;?>">mobile number.</a></p>
					</div>
				</div>
			</div>
		</div>
		<!-- captive-portal-content -->
	</div>
	<div class="clearfix"></div>
</div>

<div class="modal modal-wifi" id="terms-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content globe-modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">Terms of Agreement</h4>
			</div>
			<div class="modal-body text-overflow">
				<p>Terms of Service ("Terms")</p>
				<?php if ( ! empty($terms)):?>
					<?php echo $terms->content;?>
				<?php endif;?>
			</div>
			<div class="modal-footer">
				<?php echo CHtml::htmlButton('Close', array('class'=>'s-btn serendra-primary-btn', 'data-dismiss'=>'modal'));?>
				<div class="modal-btn-logo">
					<?php echo CHtml::image(Link::image_url('powered-by-globe-modal.png'), 'Globe');?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal modal-wifi" id="policy-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content globe-modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">Privacy Policy</h4>
			</div>
			<div class="modal-body text-overflow">
				<p>Privacy Policy ("Policy")</p>
				<?php if ( ! empty($policy)):?>
					<?php echo $policy->content;?>
				<?php endif;?>
			</div>
			<div class="modal-footer">
				<?php echo CHtml::htmlButton('Close', array('class'=>'s-btn serendra-primary-btn', 'data-dismiss'=>'modal'));?>
				<div class="modal-btn-logo">
					<?php echo CHtml::image(Link::image_url('powered-by-globe-modal.png'), 'Globe');?>
				</div>
			</div>
		</div>
	</div>
</div>

		
<div class="modal fade" id="loader-modal" data-backdrop="static" data-keyboard="false" tabindex="-1">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('a#get-connected').on('click',function(e){
			$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		});
	});
</script>