<?php /* @var $this Controller */ ?>
<?php $this->beginContent('/layouts/main'); ?>
	<div class="container globe-container" id="continue-holder">
		
		<?php echo $content;?>
		
	</div>

	<footer class="row powered-container">
		<?php if (PortalConfiguration::getvalue('POWERED_WELCOME')):?>
			<?php echo CHtml::image(Link::image_url('powered-by-globe.png'), 'Globe');?>
		<?php endif;?>
	</footer>

<?php $this->endContent(); ?>