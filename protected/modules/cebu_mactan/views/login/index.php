<div class="row">
	<div class="col-sm-6 col-sm-offset-3  text-field">
		<?php if ( ! empty($availments)):?>
			<?php if ($this->user):?>
				<?php echo str_replace('::user::', $this->user, PortalConfiguration::getValue('WELCOME_BACK'));?>
			<?php else:?>
				<h3>Welcome</h3>
			<?php endif; ?>
			<?php if ( ! empty($availment)):?>
				<h3>Your FREE <?php echo $availment->duration / 60;?> minutes has expired</h3>
			<?php endif;?>
		
			<div class="availment-holder globe-availment-holder">
				<div class="reg-btn-holder reg-btn-holder-with-text">
					<ul class="btn-list list-unstyled btn-list-with-title">
						<?php foreach ($availments as $a):?>
							<li>
								<div class="lbl-button-container">
									<div class="lbl-button-text">
									<?php echo $a->availment->description; ?>
									</div>
								</div>
								<?php $this->widget("{$this->portal->code}.widgets.{$a->availment->code}", array('self' => $a));?>
							</li>
						<?php endforeach;?>
					</ul>
				</div>
			</div>
		<?php else:?>
			<h2>Your FREE <?php echo $availment->duration / 60;?> minutes has expired for today</h2>
			<h4>Thank you for using Globe's <strong>FREE</strong>WiFi service</h4>
			<h4>See you again soon!</h4>
		<?php endif;?>
	</div>
</div>

<div class="modal fade" id="loader-modal" data-backdrop="static" data-keyboard="false" tabindex="-1">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	
	$('form').on('submit',function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		//e.preventDefault();
	});
});
</script>