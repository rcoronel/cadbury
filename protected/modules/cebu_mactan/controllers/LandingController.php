<?php

class LandingController extends CaptivePortal
{
	/**
	 * Declare variables
	 * 
	 * @access protected
	 * @param object $action CAction instance
	 * @return bool
	 */
	protected function beforeAction($action)
	{
		parent::beforeAction($action);
		
		return TRUE;
	}
	
	/**
	 * Show landing page
	 * 
	 * @access public
	 * @return void
	 */
	public function actionIndex()
	{
		// Use custom layout
		$this->layout = '/layouts/splash';
		
		$url = $this->createAbsoluteUrl('login/', array('connection_token'=>$this->connection->connection_token));
		$url_mobile = $this->createAbsoluteUrl('login/', array('connection_token'=>$this->connection->connection_token, 'availment_id'=>3, 'action'=>'login'));
		$terms = Cms::model()->findByAttributes(array('portal_id'=>$this->portal->portal_id, 'friendly_url'=>'terms-and-conditions'));
		$policy = Cms::model()->findByAttributes(array('portal_id'=>$this->portal->portal_id, 'friendly_url'=>'service-agreement'));
		
		$this->render('index', array('url'=>$url, 'url_mobile'=>$url_mobile, 'terms'=>$terms, 'policy'=>$policy));
	}
}