<?php if (! empty($objectList)):?>
	<?php foreach ($objectList as $ol):?>
		<div class="col-md-4">
			<section class="task-panel tasks-widget">
				<div class="panel-heading">
					<div class="pull-left">
						<h5>
							<strong><?php echo $ol->Title;?></strong>
						</h5>
					</div>
					<div class="pull-right">
						<h5>
							<button class="btn btn-default btn-xs fa fa-pencil" onclick="window.location.href='<?php echo $this->createUrl($this->id.'/update', array('id' => $ol->Scenario_ID));?>'"></button>
							<button class="btn btn-default btn-xs fa fa-trash-o" onclick="window.location.href='<?php echo $this->createUrl($this->id.'/delete', array('id' => $ol->Scenario_ID));?>'"></button>
						</h5>
					</div>
				</div>
				<div class="panel-body">
					<br>
					<p><b>New Users</b></p>
					<div class="task-content">
						<ul class="task-list">
							<?php foreach ($ol->new as $new):?>
								<li>
									<div class="task-checkbox">
										<?php echo $new->Level;?>
									</div>
									<div class="task-title">
										<?php echo $new->availment->Name;?>
									</div>
								</li>
							<?php endforeach;?>                                     
						</ul>
					</div>
					<p><b>Returning Users</b></p>
					<div class="task-content">
						<ul class="task-list">
							<?php foreach ($ol->returning as $returning):?>
								<li>
									<div class="task-checkbox">
										<?php echo $returning->Level;?>
									</div>
									<div class="task-title">
										<?php echo $returning->availment->Name;?>
									</div>
								</li>
							<?php endforeach;?>                                     
						</ul>
					</div>
				</div>
			</section>
		</div>
	<?php endforeach;?>
<?php endif;?>
