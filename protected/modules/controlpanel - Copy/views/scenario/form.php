<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<div id="form-error" class="alert alert-danger" style="display: none;"></div>
	</div>
	<div class="col-md-12 mt">
		<div class="panel panel-default" data-collapsed="0">
			<div class="panel-body">
				<?php echo CHtml::beginForm('', 'post', array('class'=>'form-horizontal form-groups-bordered', 'enctype' => 'multipart/form-data')); ?>
					<?php echo CHtml::hiddenField('ajax', 1);?>
					<div class="form-group">
						<?php echo CHtml::label('Title', 'Title', array('class' => 'control-label col-lg-3'));?>
						<div class="col-lg-5">
							<?php echo CHtml::textField('Title', $scenario->Title, array('class' => 'form-control', 'maxlength' => 64)); ?>
						</div>
					</div>
					<table class="table table-hover">
						<thead>
							<tr>
								<th class="col-md-1">Level</th>
								<th class="col-md-10"><?php echo CHtml::encode(Availment::model()->getAttributeLabel('Name')); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php if (isset($new_scenarios) && ! empty($new_scenarios)):?>
								<?php foreach ($new_scenarios as $index=>$ns):?>
									<tr class="availments">
										<td class="availment-level">
											<?php echo CHtml::textField('new[level][]', $ns->Level, array('class'=>'form-control col-md-1 input-sm number-only'));?>
										</td>
										<td>
											<?php echo CHtml::dropDownList('new[availment_id][]', $ns->Availment_ID, $availments, array('class'=>'form-control'));?>
										</td>
										<?php if ($index > 0):?>
										<td>
											<button class="btn btn-danger" onclick="javascript:removeRow(this);" type="button">
												<i class="glyphicon glyphicon-remove-sign"></i>
											</button>
										</td>
										<?php endif;?>
									</tr>
								<?php endforeach;?>
							<?php else:?>
								<tr class="availments">
									<td class="availment-level">
										<?php echo CHtml::textField('new[level][]', '1', array('class'=>'form-control col-md-1 input-sm number-only'));?>
									</td>
									<td>
										<?php echo CHtml::dropDownList('new[availment_id][]', '', $availments, array('class'=>'form-control'));?>
									</td>
								</tr>
							<?php endif;?>
							
							<tr id="add-new-availment">
								<td colspan="6">
									<?php echo CHtml::htmlButton('Add row', array('class'=>'btn btn-success', 'onclick'=>'javascript:addRow();'));?>
								</td>
							</tr>
							<tr>
								<td colspan="6">
									&nbsp;
								</td>
							</tr>
							<?php if (isset($returning_scenarios) && ! empty($returning_scenarios)):?>
								<?php foreach ($returning_scenarios as $index=>$rs):?>
									<tr class="availments-returning">
										<td class="availment-level">
											<?php echo CHtml::textField('returning[level][]', $rs->Level, array('class'=>'form-control col-md-1 input-sm number-only'));?>
										</td>
										<td>
											<?php echo CHtml::dropDownList('returning[availment_id][]', $rs->Availment_ID, $availments, array('class'=>'form-control'));?>
										</td>
										<?php if ($index > 0):?>
										<td>
											<button class="btn btn-danger" onclick="javascript:removeRow(this);" type="button">
												<i class="glyphicon glyphicon-remove-sign"></i>
											</button>
										</td>
										<?php endif;?>
									</tr>
								<?php endforeach;?>
							<?php else:?>
								<tr class="availments-returning">
									<td class="availment-level">
										<?php echo CHtml::textField('returning[level][]', '1', array('class'=>'form-control col-md-1 input-sm number-only'));?>
									</td>
									<td>
										<?php echo CHtml::dropDownList('returning[availment_id][]', '', $availments, array('class'=>'form-control'));?>
									</td>
								</tr>
							<?php endif;?>
							<tr id="add-new-availment-returning">
								<td colspan="6">
									<?php echo CHtml::htmlButton('Add row', array('class'=>'btn btn-success', 'onclick'=>'javascript:addRowRetunrning();'));?>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-1">
							<button type="submit" class="btn btn-primary">
								Submit
							</button>
						</div>
					</div>
				<?php echo CHtml::endForm();?>
			</div>
		</div>
	</div>
</div>
<script>
function addRow()
{
	var cloned = $('table tbody tr:first').clone().insertBefore('tr#add-new-availment').append('<td><button class="btn btn-danger" onclick="javascript:removeRow(this);" type="button"><i class="glyphicon glyphicon-remove-sign"></i></button></td>');
	$(cloned).find('select option').prop('selected', false);
	updateLevel();
}

function addRowRetunrning()
{
	var cloned = $('table tbody tr.availments-returning:first').clone().insertBefore('tr#add-new-availment-returning').append('<td><button class="btn btn-danger" onclick="javascript:removeRow(this);" type="button"><i class="glyphicon glyphicon-remove-sign"></i></button></td>');
	$(cloned).find('select option').prop('selected', false);
	updateLevel();
}

function removeRow(source)
{
	$(source).parent().parent().remove();
	updateLevel();
	
}

function updateLevel()
{
	var count = 1;
	$('tr.availments').each(function(){
		$(this).find('td.availment-level > input#new_level').val(count);
		count++;
	});
	count = 1;
	$('tr.availments-returning').each(function(){
		$(this).find('td.availment-level > input#returning_level').val(count);
		count++;
	});
}
	
$(document).ready(function(){
	$('form').on('submit',function(e){
		// check if submitted via AJAX
		if ($('input[name="ajax"]').length && $('input[name="ajax"]').val()) {
			// remove error style
			$('div.form-group').removeClass('has-error');

			// hide errors
			$('div#form-error').slideUp( 300 ).delay( 800 ).html();
			
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			$.ajax({
				url : formURL,
				type: "POST",
				data : postData,
				dataType: 'json',
				success:function(data, textStatus, jqXHR) {
					if (! data.status) {
						$('div#form-error').slideDown(300).delay(800).html(data.message);
						$('div#form-error').show();

						if (typeof data.fields !== 'undefined')
						{
							$.each(data.fields, function(index, value){
								$('label[for="'+value+'"]').parent().addClass('has-error');
							});
						}
					}
					else {
						$('#confirmation-modal .modal-header h4#form-modalLabel').html('Success');
						$('#confirmation-modal .modal-body #form-modalBody').html(data.message);
						$('#confirmation-modal').modal({backdrop: 'static', keyboard: false});

						setTimeout(function() {
							window.location.href = currentIndex;
						}, 2000);
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					//if fails     
				}
			});
			e.preventDefault(); //STOP default action
		}
	});
	$(document).on("keydown", ".number-only", function(event) {
		console.log("number");
		if ( event.keyCode == 46 || event.keyCode == 8  ||  event.keyCode == 9) {
		} else {
			if (event.keyCode < 48 || event.keyCode > 57 ) {
				event.preventDefault();	
			} 	
		}
	});
});
</script>
<?php $this->widget('widgets.ajax.Modal');?>