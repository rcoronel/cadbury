<div class="row mt">
	<div class="col-lg-12">
		<div id="mt-form">
			<?php echo CHtml::beginForm('', 'post', array('class' => 'form-inline', 'role' => 'form', 'id' => 'connection-stat-form'));?>
				<div class="form-group" name="div-month-select">
					Month : 
					<?php echo CHtml::dropDownList('month', '', array('01'=>'January', '02'=>'February', '03'=>'March',
						'04'=>'April', '05'=>'May', '06'=>'June', '07'=>'July', '08'=>'August', '09'=>'September',
						10=>'October', 11=>'November', 12=>'December'), array('class'=>'form-control'));?>
				</div>
				<div class="form-group" name="div-month-select">
					Year : 
					<?php echo CHtml::dropDownList('year', '', $years, array('class'=>'form-control'));?>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-info" id="btnShow" name="btn-submit-show">Show</button>
				</div>
			<?php echo CHtml::endForm();?>
		</div>
		<br>
	</div>
<div>
	
<div class="col-lg-12">
	<div class="panel panel-primary">
		<div class="panel-heading">
			<i class="fa fa-bar-chart-o"></i>
		</div>
		<div class="panel-body">
			<div id="connection_chart">
				<i class="fa fa-info-circle"></i> Select date above
			</div>
		</div>
	</div>

	<?php echo CHtml::beginForm(Yii::app()->createUrl('controlpanel/stat_connection/downloadCSV'), 'post', array('class' => 'form-inline', 'role' => 'form', 'id' => 'csv-download', 'style'=>'visibility:hidden'));?>
		<div class="form-group">
			<?php echo CHtml::hiddenField('stat-year');?>
			<?php echo CHtml::hiddenField('stat-month');?>
			<button type="submit" class="btn btn-info" id="btnCSV" name="btn-submit-csv">Download CSV</button>
		</div>
	<?php echo CHtml::endForm();?>
</div>
	
<script>
	$(document).ready(function() {
		$('form#connection-stat-form').on('submit', function(e){
			$.ajax({
				type: "POST",
				url: currentURL,
				data: $('form#connection-stat-form').serialize(),
				success: function(data){
					$('div#connection_chart').html('');
					$('#stat-year').val($('#year').val());
					$('#stat-month').val($('#month').val());
					$("#csv-download").removeAttr("style");
					new Morris.Line({
						element: 'connection_chart',
						data: data.chart_data,
						xkey: 'date',
						ykeys: ['count', 'success'],
						labels: ['Total', 'Logged-in']
					});
//					$('div#mt-panel-body').html(data.message);
				},
				dataType: "json"
			});
			e.preventDefault();
		});
	});
</script>
<?php $this->widget('widgets.ajax.Modal');?>