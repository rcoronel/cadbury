<div class="row">
    <div class="col-md-3">
        <div id="toc" class="tocify">
			<?php foreach ($roles as $r):?>
				<ul class="tocify-header nav nav-list">
					<li class="tocify-item <?php echo (Yii::app()->request->getParam('id') == $r->Client_role_ID) ? 'active' : '';?>" data-unique="<?php echo $r->Role;?>" style="cursor: pointer;">
						<a href="<?php echo $this->createUrl(Yii::app()->controller->id.'/update', array('id' => $r->Client_role_ID));?>"><?php echo $r->Role;?></a>
					</li>
				</ul>
			<?php endforeach;?>
        </div>
    </div>
    <div class="col-md-9">
        <div class="tocify-content">
			<?php if ($role->Client_role_ID == 1):?>
				<div class="alert alert-danger">
					<i class="fa fa-exclamation-circle"></i> Cannot update this profile's permissions
				</div>
			<?php else:?>
				<?php echo CHtml::beginForm();?>
					<?php echo CHtml::hiddenField('YII_CSRF_TOKEN', Yii::app()->request->csrfToken);?>
					<?php echo CHtml::hiddenField('Client_role_ID', $role->Client_role_ID);?>
					<table class="table">
						<thead>
							<tr>
								<th class="col-md-4">Page</th>
								<th class="text-center col-md-2">View</th>
								<th class="text-center col-md-2">Add</th>
								<th class="text-center col-md-2">Edit</th>
								<th class="text-center col-md-2">Delete</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($menus as $menu):?>
								<?php if ($menu->Parent_client_menu_ID == 0):?>
									<tr>
										<td>
											<?php echo CHtml::hiddenField("client_menu[$menu->Client_menu_ID][Client_menu_ID]", $menu->Client_menu_ID);?>
											<?php echo $menu->Title;?>
										</td>
										<td class="text-center">
											<?php echo CHtml::checkBox("client_menu[$menu->Client_menu_ID][View]", $menu->View, array('value'=>1));?>
										</td>
										<td class="text-center">
											<?php echo CHtml::checkBox("client_menu[$menu->Client_menu_ID][Add]", $menu->Add, array('value'=>1));?>
										</td>
										<td class="text-center">
											<?php echo CHtml::checkBox("client_menu[$menu->Client_menu_ID][Edit]", $menu->Edit, array('value'=>1));?>
										</td>
										<td class="text-center">
											<?php echo CHtml::checkBox("client_menu[$menu->Client_menu_ID][Delete]", $menu->Delete, array('value'=>1));?>
										</td>
									</tr>
									<?php foreach ($menus as $submenu):?>
										<?php if ($submenu->Parent_client_menu_ID == $menu->Client_menu_ID):?>
											<tr>
												<td>
													<i class="fa fa-angle-double-right"></i>
													<?php echo CHtml::hiddenField("client_menu[$submenu->Client_menu_ID][Client_menu_ID]", $submenu->Client_menu_ID);?>
													<?php echo $submenu->Title;?>
												</td>
												<td class="text-center">
													<?php echo CHtml::checkBox("client_menu[$submenu->Client_menu_ID][View]", $submenu->View, array('value'=>1));?>
												</td>
												<td class="text-center">
													<?php echo CHtml::checkBox("client_menu[$submenu->Client_menu_ID][Add]", $submenu->Add, array('value'=>1));?>
												</td>
												<td class="text-center">
													<?php echo CHtml::checkBox("client_menu[$submenu->Client_menu_ID][Edit]", $submenu->Edit, array('value'=>1));?>
												</td>
												<td class="text-center">
													<?php echo CHtml::checkBox("client_menu[$submenu->Client_menu_ID][Delete]", $submenu->Delete, array('value'=>1));?>
												</td>
											</tr>
										<?php endif;?>
									<?php endforeach;?>
								<?php endif;?>
							<?php endforeach;?>
							<tr>
								<td colspan="5">
									<button type="submit" class="btn btn-primary">
										Update
									</button>
								</td>
							</tr>
						</tbody>
					</table>
				<?php CHtml::endForm();?>
			<?php endif;?>
        </div>
    </div>
</div>