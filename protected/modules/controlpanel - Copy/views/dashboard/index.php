<div class="col-md-12 mt">
	<!-- CONNECTION -->
	<div class="col-lg-4">
		<div class="panel panel-info">
			<div class="panel-heading">
				<div class="row">
					<div class="col-xs-12 text-right">
						<h2 id="h2-connection"><?php echo number_format($connection_count);?></h2>
						<p><?php echo date('F d, Y');?></p>
					</div>
				</div>
			</div>
			<a href="<?php echo $this->createAbsoluteUrl('stat_connection/');?>">
				<div class="panel-footer announcement-bottom">
					<div class="row">
						<div class="col-xs-8">
							Today's connection
						</div>
						<div class="col-xs-4 text-right">
							<i class="fa fa-arrow-circle-right"></i>
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<!-- /CONNECTION -->
	
	<!-- DEVICE -->
	<div class="col-lg-4">
		<div class="panel panel-warning">
			<div class="panel-heading">
				<div class="row">
					<div class="col-xs-12 text-right">
						<h2 id="h2-user"><?php echo number_format($authenticated_count);?></h2>
						<p><?php echo date('F d, Y');?></p>
					</div>
				</div>
			</div>
			<a href="<?php echo $this->createAbsoluteUrl('stat_device/');?>">
				<div class="panel-footer announcement-bottom">
					<div class="row">
						<div class="col-xs-8">
							Today's authenticated users
						</div>
						<div class="col-xs-4 text-right">
							<i class="fa fa-arrow-circle-right"></i>
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<!-- /DEVICE -->
</div>


<script lang="text/javascript">
	$(document).ready(function() {
		(function poll(){
			$.ajax({ 
				type: "POST",
				url: POLL_URL,
				data: {'YII_CSRF_TOKEN':token},
				success: function(data){
					//Update your dashboard gauge
					$("#h2-connection").html(data.connection_count);
					$("#h2-user").html(data.authenticated_count);
				},
				dataType: "json", complete: poll, timeout: 30000
			});
		})();
	});
</script>
