<div class="row mt">
	<div class="col-lg-12">
		<div id="mt-form">
			<?php echo CHtml::beginForm('', 'post', array('class' => 'form-inline', 'role' => 'form', 'id' => 'connection-stat-form'));?>
				<div class="form-group" name="div-month-select">
					From : 
					<?php echo CHtml::textField('date-from', '',array('class'=>'form-control datepicker')); ?>
				</div>
				<div class="form-group" name="div-month-select">
					To : 
					<?php echo CHtml::textField('date-to', '',array('class'=>'form-control datepicker')); ?>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-info" id="btnShow" name="btn-submit-show">Show</button>
				</div>
			<?php echo CHtml::endForm();?>
		</div>
		<br>
	</div>
<div>
	
<div class="col-lg-12">
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title"><i class="fa fa-table"></i> Stats</h3>
		</div>
		<div class="panel-body" id="mo-panel-body">
			<i class="fa fa-info-circle"></i> Select dates above
		</div>
	</div>
	<?php echo CHtml::beginForm(Yii::app()->createUrl('controlpanel/stat_avail/downloadCSV'), 'post', array('class' => 'form-inline', 'role' => 'form', 'id' => 'csv-download', 'style'=>'visibility:hidden'));?>
		<div class="form-group">
			<?php echo CHtml::hiddenField('stat-from');?>
			<?php echo CHtml::hiddenField('stat-to');?>
			<button type="submit" class="btn btn-info" id="btnCSV" name="btn-submit-csv">Download CSV</button>
		</div>
	<?php echo CHtml::endForm();?>
</div>
	
<script>
	$(document).ready(function() {
		
		$('#date-from').datetimepicker({ format:'Y/m/d', timepicker:false });
		$('#date-to').datetimepicker({ format:'Y/m/d', timepicker:false });

		$('form#connection-stat-form').on('submit', function(e){
			$('div#mo-panel-body').html('');
			$.ajax({
				type: "POST",
				url: currentURL,
				data: $('form#connection-stat-form').serialize(),
				success: function(data){
					$('#stat-from').val($('#date-from').val());
					$('#stat-to').val($('#date-to').val());
					$("#csv-download").removeAttr("style");
					$('div#mo-panel-body').html(data.message);
				},
				dataType: "json"
			});
			e.preventDefault();
		});
	});
</script>
<?php $this->widget('widgets.ajax.Modal');?>