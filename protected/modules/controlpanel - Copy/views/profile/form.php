<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<div id="form-error" class="alert alert-danger" style="display: none;"></div>
	</div>
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-default" data-collapsed="0">
			<div class="panel-body">
				<?php $form=$this->beginWidget('CActiveForm', array('htmlOptions' => array('class'=>'form-horizontal form-groups-bordered', 'role' => 'form', 'method' => 'post'))); ?>
					<?php echo CHtml::hiddenField('ajax', 1);?>
					<div class="form-group">
						<?php echo $form->labelEx($client, 'Email', array('class' => 'control-label col-lg-3'));?>
						<div class="col-sm-5">
							<?php echo $form->textField($client, 'Email', array('class'=>'form-control', 'placeholder'=>$client->getAttributeLabel('Email')));?>
						</div>
					</div>
					<div class="form-group">
						<?php echo $form->labelEx($client, 'Firstname', array('class' => 'control-label col-lg-3'));?>
						<div class="col-sm-5">
							<?php echo $form->textField($client, 'Firstname', array('class'=>'form-control', 'placeholder'=>$client->getAttributeLabel('Firstname')));?>
						</div>
					</div>
					<div class="form-group">
						<?php echo $form->labelEx($client, 'Lastname', array('class' => 'control-label col-lg-3'));?>
						<div class="col-sm-5">
							<?php echo $form->textField($client, 'Lastname', array('class'=>'form-control', 'placeholder'=>$client->getAttributeLabel('Lastname')));?>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</div>
				<?php $this->endWidget();?>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	$('form').on('submit',function(e){
		// check if submitted via AJAX
		if ($('input[name="ajax"]').length && $('input[name="ajax"]').val())
		{
			// remove error style
			$('div.form-group').removeClass('has-error');

			// hide errors
			$('div#form-error').slideUp(300).html();

			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				dataType: 'json',
				success:function(data, textStatus, jqXHR)
				{
					if (! data.status) {
						$('div#form-error').slideDown(300).delay(800).html(data.message);
						$('div#form-error').show();

						if (typeof data.fields !== 'undefined')
						{
							setTimeout(function() {
								$.each(data.fields, function(index, value){
									$('label[for="'+value+'"]').parent().addClass('has-error');
								});
							  }, 300);
						}
					}
					else {
						toastr.success(data.message, '', {closeButton:true,showDuration:"300",timeOut: "3000"});
						setTimeout(function() {
							window.location.href = currentIndex;
						  }, 2000);
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					//if fails     
				}
			});
			e.preventDefault(); //STOP default action
		}
	});
});
</script>