<div class="profile-env">
    <header class="row">
        <div class="col-sm-2">
            <a href="#" class="profile-picture">
				<?php if (empty($client->Image)):?>
					<?php echo CHtml::image(Link::image_url('client/default-user.jpg'), $client->Name, array('class'=>'img-responsive img-circle'));?>
				<?php else:?>
					<?php echo CHtml::image(Link::image_url("client/{$client->Nas_ID}/admin/{$client->Image}"), $client->Name, array('class'=>'img-responsive img-circle'));?>
				<?php endif;?>
			</a>
        </div>
        <div class="col-sm-7">
            <ul class="profile-info-sections">
                <li>
                    <div class="profile-name">
						<strong>
							<a href="#"><?php echo $client->Firstname;?> <?php echo $client->Lastname;?></a>
							<a href="#" class="user-status is-online tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Online"></a>
						</strong>
						<span>
							<a href="#"><?php echo $client->role->Role;?></a>
						</span>
					</div>
                </li>
            </ul>
        </div>
    </header>
    <section class="profile-info-tabs">
        <div class="row">
            <div class="col-sm-offset-2 col-sm-10">
                <ul class="user-details">
                    <li>
                        <a href="#" data-toggle="tooltip" data-placement="top" data-original-title="E-mail Address">
							<i class="entypo-mail"></i>
							<?php echo $client->Email;?>
                        </a>
                    </li>
                    <li>
                        <a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Date Created">
							<i class="entypo-calendar"></i>
							<?php echo date('F d, Y', strtotime($client->Created_at));?>
                        </a>
                    </li>
                </ul>
				<!-- tabs for the profile links -->
                <ul class="nav nav-tabs">
                    <li>
						<a onclick="javascript:showForm('update');">
							<i class="entypo-pencil"></i>
							Edit Profile
						</a>
					</li>
					<li>
						<a onclick="javascript:showForm('change_password');">
							<i class="entypo-key"></i>
							Change Password
						</a>
					</li>
					<li>
						<a onclick="javascript:showForm('update_Image');">
							<i class="entypo-picture"></i>
							Update Image
						</a>
					</li>
                </ul>
            </div>
        </div>
    </section>
</div>
<div id="profile-form"></div>
<script>
	function showForm(scenario)
	{
		$.ajax({
			url : currentIndex+'/'+scenario,
			type: "GET",
			dataType: 'json',
			success:function(data, textStatus, jqXHR) {
				$('div#profile-form').slideDown(300).delay(800).html(data.message);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				//if fails     
			}
		});
	}
</script>