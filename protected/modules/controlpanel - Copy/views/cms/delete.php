<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-danger">
		<div class="panel-heading">
			<div class="panel-title">
				<h4>
					Delete 
					<strong><?php echo $cms->Title;?></strong> 
					(#<?php echo $cms->Cms_ID;?>)?
				</h4>
			</div>
		</div>
		<div class="panel-body">

			<p>Deleting this <?php echo $cms->Title;?> will <strong>remove</strong> the <?php echo $cms->Title;?> from our records.</p>

			<?php $form=$this->beginWidget('CActiveForm', array('htmlOptions' => array(	'class'=>'form-horizontal', 'role' => 'form', 'method' => 'post'))); ?>
				<?php echo CHtml::hiddenField('Cms_ID', $cms->Cms_ID)?>
				<?php echo CHtml::hiddenField('security_key', CPasswordHelper::hashPassword($cms->Cms_ID));?>
				<div class="buttons">
					<button type="submit" class="btn btn-danger">Delete</button>
					<button type="button" class="btn btn-default" onclick="location.href = '<?php echo $this->createUrl("{$this->id}/");?>';">Cancel</button>
				</div>
			<?php $this->endWidget();?>
		</div>
	</div>
</div>