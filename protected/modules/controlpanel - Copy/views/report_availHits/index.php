<div class="row">
    <div class="col-md-3 rep_box">
        <div id="toc" class="tocify ">
        	<div class="showback ">
		        <?php foreach($reps as $r): ?>
					<a href='<?php echo $r['url']; ?>' class='btn btn-sm btn-block rep_font btn-default <?php echo ($r['classname'] == 'report_availHits') ? 'btn-primary' : 'btn-default';?>'><?php echo $r['title'] ?></a>
		        <?php endforeach; ?>
	        </div>
		</div>
	</div>

	<div class="col-md-9">
		<div id="mt-form">
			<?php echo CHtml::beginForm('', 'post', array('class' => 'form-inline', 'role' => 'form', 'id' => 'connection-stat-form'));?>
				<div class="form-group" name="availment-select">
					Availment : 
					<?php echo CHtml::dropDownList('availment', '', $availment, array('class'=>'form-control'));?>
				</div>
				<div class="form-group stat_type" name="stat-type">
					Type : 
					<?php echo CHtml::dropDownList('stat_type', '', array('1'=>'Success', '0'=>'Attempts'), array('class'=>'form-control'));?>
				</div>
				</br>
				</br>
				<div class="form-group" name="div-month-select">
					From : 
					<?php //echo CHtml::dropDownList('month', '', array('01'=>'January', '02'=>'February', '03'=>'March',
					//	'04'=>'April', '05'=>'May', '06'=>'June', '07'=>'July', '08'=>'August', '09'=>'September',
					//	10=>'October', 11=>'November', 12=>'December'), array('class'=>'form-control'));?>

					<?php echo CHtml::textField('date-from', '',array('class'=>'form-control datepicker')); ?>
				</div>
				<div class="form-group" name="div-month-select">
					To : 
					<?php //echo CHtml::dropDownList('year', '', $years, array('class'=>'form-control'));?>
					<?php echo CHtml::textField('date-to', '',array('class'=>'form-control datepicker')); ?>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-info" id="btnShow" name="btn-submit-show">Show</button>
				</div>
			<?php echo CHtml::endForm();?>
		</div>
		<br>

		<div class="col-lg-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<i class="fa fa-bar-chart-o"></i>
			</div>
			<div class="panel-body">
				<div id="connection_chart">
					<i class="fa fa-info-circle"></i> Select date above
				</div>
			</div>
		</div>
		<?php echo CHtml::beginForm(Yii::app()->createUrl('controlpanel/report_availHits/downloadCSV'), 'post', array('class' => 'form-inline', 'role' => 'form', 'id' => 'csv-download', 'style'=>'visibility:hidden'));?>
			<div class="form-group">
				<?php echo CHtml::hiddenField('stat-from');?>
				<?php echo CHtml::hiddenField('stat-to');?>
				<?php echo CHtml::hiddenField('availment-id');?>
				<button type="submit" class="btn btn-info" id="btnCSV" name="btn-submit-csv">Download CSV</button>
			</div>
		<?php echo CHtml::endForm();?>
	</div>

	</div>
<div>
	

	
<script>
	$(document).ready(function() {
		
		$('#date-from').datetimepicker({ format:'Y/m/d', timepicker:false });
		$('#date-to').datetimepicker({ format:'Y/m/d', timepicker:false });
		$('.stat_type').hide();

		$('#availment').on('change', function(){
			
			var avail = $(this).val();
			
			if(avail == 98){
				$('.stat_type').show();
			}else{
				$('.stat_type').hide();
			}
		});

		$('form#connection-stat-form').on('submit', function(e){
			$.ajax({
				type: "POST",
				url: currentURL,
				data: $('form#connection-stat-form').serialize(),
				success: function(data){
					$('div#connection_chart').html('');
					$('#stat-from').val($('#date-from').val());
					$('#stat-to').val($('#date-to').val());
					$('#availment-id').val($('#availment').val());
					$("#csv-download").removeAttr("style");
					console.log($('#availment').val());
					if($('#availment').val() == 98){
						console.log('here');
						new Morris.Line({
							element: 'connection_chart',
							data: data.chart_data,
							xkey: 'date',
							ykeys: ['facebook','registration', 'mobile','tattoo', 'survey','easy', 'free'],
							labels: ['Facebook','Registration', 'Mobile','Tattoo', 'Survey','Easy', 'Free']
						});
						
					}else{
						console.log('there');
						new Morris.Line({
							element: 'connection_chart',
							data: data.chart_data,
							xkey: 'date',
							ykeys: ['attempts','success'],
							labels: ['Attempts','Success']
						});
					}
//					$('div#mt-panel-body').html(data.message);
				},
				dataType: "json"
			});
			e.preventDefault();
		});
	});
</script>
<?php $this->widget('widgets.ajax.Modal');?>