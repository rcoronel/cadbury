<?php $this->beginContent('/layouts/main'); ?>

<aside>
	<div id="sidebar"  class="nav-collapse ">
		<ul class="sidebar-menu" id="nav-accordion">
			<p class="centered">
				<a href="<?php echo $this->createUrl("profile/");?>">
					<?php if (empty($this->context->client->Image)):?>
						<?php echo CHtml::image(Link::image_url('client/default-user.jpg'), $this->context->client->Name, array('class'=>'img-circle', 'width'=>60));?>
					<?php else:?>
						<?php echo CHtml::image(Link::image_url("client/{$this->context->client->Nas_ID}/admin/{$this->context->client->Image}"), $this->context->client->Name, array('class'=>'img-circle', 'width'=>60));?>
					<?php endif;?>
				</a>
			</p>
			<h5 class="centered"><?php echo $this->context->client->Name;?></h5>

			<?php foreach ($this->context->menus as $menu):?>
				<li class="<?php echo ($menu->has_subpages) ? 'sub-menu' : '';?>">
					<a href="<?php echo $this->createUrl("{$menu->Classname}/");?>" class="<?php echo ($this->getId()==$menu->Classname OR $this->menu->Parent_client_menu_ID==$menu->Client_menu_ID) ? 'active opened' : '';?>">
						<?php if ($menu->Img):?><i class="<?php echo $menu->Img;?>"></i><?php endif;?><span class="title"><?php echo $menu->Title;?></span>
					</a>
					
					<?php if ($menu->has_subpages):?>
						<ul class="sub">
							<?php foreach ($this->context->submenus as $submenu):?>
								<?php if ($submenu->Parent_client_menu_ID == $menu->Client_menu_ID):?>
									<li class="<?php echo ($this->getId()==$submenu->Classname) ? 'active' : '';?>">
										<a href="<?php echo $this->createUrl("{$submenu->Classname}/");?>">
											<span class="title"><?php echo $submenu->Title;?></span>
										</a>
									</li>
								<?php endif;?>
							<?php endforeach;?>
						</ul>
					<?php endif;?>
				</li>
			<?php endforeach;?>
		</ul>
	</div>
</aside>

<section id="main-content">
	<section class="wrapper site-min-height">
			<h2>
				<i class="fa fa-angle-right"></i> <?php echo $this->menu->Title.(isset($this->title)? ' > '.$this->title : '');?>
				<div class="pull-right">
					<!-- ACTION BUTTONS -->
					<?php if ( ! empty($this->actionButtons)):?>
						<?php foreach($this->actionButtons as $action => $button):?>
							<?php if ($action === $this->action->id):?>

								<?php if (isset($button['add'])):?>
                                                                    <?php if ((($this->menu->Title == "Question" && (isset($this->count) && $this->count < 10)) || ($this->menu->Title == "Survey" && isset($this->count) &&  $this->count < 20) || $this->menu->Title == "Answer")):?>
                                                                            <a class="btn btn-default" href="<?php echo $button['add']['link'];?>">
                                                                                    <i class="fa fa-plus-circle"></i>
                                                                                    Add
                                                                            </a>
                                                                    <?php endif;?>
								<?php endif;?>

								<?php if (isset($button['back'])):?>
									<a class="btn btn-default" href="<?php echo $button['back']['link'];?>">
										<i class="fa fa-backward"></i>
										Back to list
									</a>
								<?php endif;?>

							<?php endif;?>
						<?php endforeach;?>
					<?php endif;?>
					<!-- /ACTION BUTTONS -->
				</div>
			</h2>
			<div class="row">
				<div class="col-lg-12">
					<?php if(Yii::app()->getModule('controlpanel')->client->hasFlash('success')): ?>
						<div class="alert alert-success alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<i class="fa fa-info-circle"></i>&nbsp;
							<?php echo Yii::app()->getModule('controlpanel')->client->getFlash('success'); ?>
						</div>
					<?php endif;?>
					<?php if(Yii::app()->getModule('controlpanel')->client->hasFlash('fail')): ?>
						<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<i class="fa fa-info-circle"></i>&nbsp;
							<?php echo Yii::app()->getModule('controlpanel')->client->getFlash('fail'); ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		<div class="row">
			<div class="col-lg-12">
				<?php echo $content;?>
			</div>
		</div>
	</section>
</section>
<?php $this->endContent(); ?>