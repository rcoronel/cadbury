<?php if (array_key_exists('Position', $object->attributes)):?>
	<script type="text/javascript">
		$(document).ready(function() {
			$("table.tableDnD").tableDnD({
				onDragClass: "dragging",
				onDrop: function(table, row){
					$.ajax({
						type: 'GET',
						headers: { "cache-control": "no-cache" },
						async: false,
						url: currentIndex+'/position',
						data: $.tableDnD.serialize(),
						success: function(data) {
							toastr.success(data, '', {closeButton:true,showDuration:"300",timeOut: "3000"});
						}
					});
				},
				dragHandle: "dragHandle"
			});
		});
	</script>
<?php endif;?>
                <?php if(Yii::app()->controller->id == 'survey' || Yii::app()->controller->id == 'question' || Yii::app()->controller->id == 'answer'): ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">Guidelines</div>
                        <div class="panel-body">
                            <div class="col-lg-7">
                                <ol>
                                    <?php if(Yii::app()->controller->id == 'survey' || Yii::app()->controller->id == 'question'): ?>
                                    <li>The survey availment method won't display a survey if:
                                        <ol>
                                            <li>No survey were created.</li>
                                            <li>It doesn't contain any questions.</li>
                                            <li>Its questions doesn't contain any answer.</li>
                                            <li>All of its questions are not required.</li>
                                            <li>The Ad Survey field is enabled.(Enable when the survey will be used as a pop up survey)</li>
                                        </ol>
                                    </li>
                                    <?php endif; ?>
                                    <li>Legend:
                                        <ul>
                                            <li>
                                                <span class="label label-success"><i class="fa fa-check"></i>Enabled/True</span>
                                                <span class="label label-danger"><i class="fa fa-minus"></i>Disabled/False</span>
                                                <span class="label label-default"><i class="fa fa-arrows"></i>Draggable</span>
                                                <span class="label alert-danger">Has 0 question/answer</span>
                                            </li>
                                        </ul>
                                    </li>
                                </ol>
                            </div>
                            <?php if(Yii::app()->controller->id == 'survey' || Yii::app()->controller->id == 'question'): ?>
                            <div class="col-lg-5">
                                
                                    <div class="panel panel-<?php echo (isset($incomplete) && count($incomplete) > 0) ? "danger" : "success";?>">
                                            <div class="panel-heading">
                                                    <div class="row">
                                                            <div class="col-xs-12 text-left overflow-sm">
                                                                <?php if(isset($incomplete) && count($incomplete) > 0): ?>
                                                                        <ol>
                                                                                <?php foreach($incomplete as $inc): ?>
                                                                                        <?php if(Yii::app()->controller->id == 'survey') :?>
                                                                                            <li><h5><a href="<?php echo $this->createUrl('question/', array('id' => $inc->Survey_ID))?>"><?php echo $inc->Title; ?></a></h5></li>
                                                                                        <?php else: ?>
                                                                                            <li><h5><a href="<?php echo $this->createUrl('answer&parent_id='.$inc->Survey_ID. '/', array('id' => $inc->Survey_question_ID));?>"><?php echo $inc->Question; ?></a></h5></li>
                                                                                        <?php endif; ?>
                                                                                <?php endforeach; ?>
                                                                        </ol>
                                                                <?php else: ?>
                                                                        <h5>There are currently no incomplete items.</h5>
                                                                <?php endif; ?>
                                                            </div>
                                                    </div>
                                            </div>
                                            <div class="panel-footer announcement-bottom">
                                                    <div class="row">
                                                            <div class="col-xs-8 text-left">
                                                                   Incomplete <?php echo Yii::app()->controller->id; ?>(s)
                                                            </div>
                                                            <div class="col-xs-4 text-right">
                                                                    <i class="fa fa-warning"></i>
                                                            </div>
                                                    </div>
                                            </div>
                                    </div>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
		<div class="content-panel">
			<?php $form=$this->beginWidget('CActiveForm', array('action' => /*$this->createUrl("{$this->id}/")*/ Yii::app()->request->url, 'htmlOptions' => array('id' => 'filter-form', 'class'=>'form-horizontal', 'role' => 'form', 'method' => 'get'))); ?>
				<table id="page-list" class="table tableDnD table-hover">
					<thead>
						<tr class="nodrag nodrop">
							<?php foreach ($row_fields as $header => $attribute):?>
								<th class="<?php echo (isset($attribute['class'])) ? $attribute['class'] : '';?>">
									<strong>
										<?php echo CHtml::encode($object->getAttributeLabel($header)); ?>
									</strong>
								</th>
							<?php endforeach;?>
							<?php if (isset($row_actions) && ! empty($row_actions)):?>
								<th class="text-center col-md-2">
									<strong>
										<?php echo CHtml::encode('Actions');?>
									</strong>
								</th>
							<?php endif;?>
						</tr>
						<tr class="nodrag nodrop">
							<?php foreach ($row_fields as $header => $attribute):?>
								<th class="<?php echo (isset($attribute['class'])) ? $attribute['class'] : '';?>">
									<?php if ($attribute['type'] == 'text'):?>
										<?php echo $form->textField($object, $header, array('class'=>'form-control input-sm', 'autocomplete'=>'off'));?>
									<?php endif;?>

									<?php if ($attribute['type'] == 'select'):?>
										<?php echo $form->dropDownList($object, $header, $attribute['value'], array('class' => 'form-control input-sm', 'prompt' => '---'));?>
									<?php endif;?>
								</th>
							<?php endforeach;?>
							<?php if (isset($row_actions) && ! empty($row_actions)):?>
								<th class="text-center col-md-1">
									<div class="btn-group">
										<button type="submit" name="search-filter" value="1" class="btn btn-sm" title="Search">
											<i class="fa fa-search"></i>
										</button>
										<button type="submit" name="reset-filter" id="reset-filter" value="2" class="btn btn-sm" title="Reset">
											<i class="fa fa-eraser"></i>
										</button>
									</div>
								</th>
							<?php endif;?>
						</tr>
					</thead>
					<tbody>
						<?php if ( ! empty($objectList)):?>

							<?php foreach ($objectList as $row):?>
								<tr id="<?php echo $row->{$object->tableSchema->primaryKey};?>">

									<?php foreach ($row_fields as $field => $attribute):?>
										<td class="<?php echo (isset($attribute['class'])) ? $attribute['class'] : ''; echo (($field == 'question_count' || $field == 'answer_count') &&  $row->{$field} == 0) ? "alert-danger": "";?>">
											
											<?php if ($field == 'Position'):?><i class="fa fa-arrows"></i> &nbsp;<?php endif;?>

											<?php if (isset($attribute['mode'])):?>
												<?php if ($attribute['mode'] == 'bool'):?>
													<?php if ($row->$field):?>
														<span class="label label-success" title="<?php echo $attribute['value'][$row->$field];?>">
															<i class="fa fa-check"></i>
														</span>
													<?php else:?>
														<span class="label label-danger" title="<?php echo $attribute['value'][$row->{$field}];?>">
															<i class="fa fa-minus"></i>
														</span>
													<?php endif;?>
												<?php endif;?>
											<?php elseif ($attribute['type'] == 'image'):?>
												<?php if ($row->{$field}):?>
													<?php echo CHtml::image($attribute['url'].$row->{$field});?>
												<?php endif;?>
											<?php else:?>
												<?php if (isset($attribute['parent']) && isset($attribute['child'])):?>
													<?php if ($row->{$attribute['parent']}):?>
														<?php echo $row->{$attribute['parent']}->{$attribute['child']};?>
													<?php endif;?>
												<?php else:?>
													<?php echo $row->{$field};?>
                                                                                                        <?php if(($field == 'question_count' || $field == 'answer_count') &&  $row->{$field} == 0): ?>
                                                                                                               
                                                                                                        <?php endif; ?>
												<?php endif;?>
											<?php endif;?>
										</td>
									<?php endforeach;?>

									<?php if (isset($row_actions) && ! empty($row_actions)):?>
										<td class="text-center col-md-1">
											<div class="btn-group-action text-center">
												<div class="btn-group">
													<?php foreach ($row_actions as $action):?>
                                                                                                                        <?php if ($action == 'question'):?>
																<a href="<?php echo $this->createUrl('question/', array('id' => $row->{$object->tableSchema->primaryKey}));?>" title="View Questions" class="btn btn-default" <?php echo (isset($modalView) && $modalView) ? 'data-toggle="modal" data-target="#view-modal"' : '';?>>
																	<i class="fa fa-question"></i>
																</a>
															<?php endif;?>
                                                                                                                        <?php if ($action == 'answer'):?>
																<a href="<?php echo $this->createUrl('answer'.(isset($this->parent_id) ? '&parent_id='.$this->parent_id : ''). '/', array('id' => $row->{$object->tableSchema->primaryKey}));?>" title="View Answer" class="btn btn-default" <?php echo (isset($modalView) && $modalView) ? 'data-toggle="modal" data-target="#view-modal"' : '';?>>
																	<i class="fa fa-pencil"></i>
																</a>
															<?php endif;?>
															<?php if ($action == 'view'):?>
																<a href="<?php echo $this->createUrl($this->id.'/view', array('id' => $row->{$object->tableSchema->primaryKey}));?>" title="View" class="btn btn-default" <?php echo (isset($modalView) && $modalView) ? 'data-toggle="modal" data-target="#view-modal"' : '';?>>
																	<i class="fa fa-eye"></i>
																</a>
															<?php endif;?>
															<?php if ($action == 'edit'):?>
																<a href="<?php echo $this->createUrl($this->id.'/update'.(isset($this->parent_id) ? '&parent_id='.$this->parent_id: '').(isset($this->question_id) ? '&question_id='.$this->question_id: ''), array('id' => $row->{$object->tableSchema->primaryKey}));?>" title="Edit" class="btn btn-default">
																	<i class="fa fa-edit"></i>
																</a>
															<?php endif;?>
															<?php if ($action == 'delete'):?>
																<a href="<?php echo $this->createUrl($this->id.'/delete'.(isset($this->parent_id) ? '&parent_id='.$this->parent_id: '').(isset($this->question_id) ? '&question_id='.$this->question_id: ''), array('id' => $row->{$object->tableSchema->primaryKey}));?>" title="Delete" class="btn btn-default">
																	<i class="fa fa-trash-o"></i>
																</a>
															<?php endif; ?>
													<?php endforeach;?>
												</div>
											</div>
										</td>
									<?php endif;?>

								</tr>
							<?php endforeach;?>
						<?php else:?>
							<tr>
								<td colspan="<?php echo count($row_fields) + count($row_actions);?>">
									<i class="fa fa-exclamation-circle"></i>
									No records found.
								</td>
							</tr>
						<?php endif;?>
					</tbody>
				</table>
			<?php $this->endWidget();?>
		</div>
	<?php $this->widget('CLinkPager', array(
		'pages' => $pages,
	)) ?>

<script type="text/javascript">
	$(document).ready(function() {
		$('#reset-filter').on('click', function(){
			$('form#filter-form input[type="text"]').val('');
			$('form#filter-form select').val('');
			$('form#filter-form').find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
		});
	});
</script>