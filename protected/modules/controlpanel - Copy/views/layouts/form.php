<div class="row">

	<?php if ($object->hasErrors()):?>
		<div id="form-error" class="alert alert-danger">
			<?php echo CHtml::errorSummary($object);?>
		</div>
	<?php endif;?>
	<div class="col-md-6 col-md-offset-3">
		<div id="form-error" class="alert alert-danger" style="display: none;"></div>
	</div>

	<div class="col-md-12">

		<div class="form-panel panel panel-default" data-collapsed="0">
			<div class="panel-body">
				<?php $form=$this->beginWidget('CActiveForm', array('htmlOptions' => array('class'=>'form-horizontal style-form', 'role' => 'form', 'method' => 'post'))); ?>
					<?php foreach ($fields as $field=>$attributes):?>
						<?php if ($field == 'xmlhttprequest'):?>
							<?php echo CHtml::hiddenField('ajax', $attributes['value'])?>
						<?php else:?>
							<div class="form-group">
								<?php echo $form->labelEx($object, $field, array('class' => 'control-label col-lg-3'));?>
								<div class="<?php echo (isset($attributes['class'])) ? $attributes['class'] : '';?>">
									<?php if ($attributes['type'] == 'text'):?>
										<?php echo $form->textField($object, $field , array('disabled' => (isset($isDeleteForm) && $isDeleteForm==TRUE)? TRUE : FALSE, 'value'=> (isset($attributes['value']) ? $attributes['value'] : null), 'readonly'=>(isset($attributes['readonly']) ? $attributes['readonly'] : null), 'class'=>'form-control', 'placeholder'=>$object->getAttributeLabel($field)));?>
									<?php endif;?>
									
									<?php if ($attributes['type'] == 'wysiwyg'):?>
										<?php echo $form->textarea($object, $field, array('disabled' => (isset($isDeleteForm) && $isDeleteForm==TRUE)? TRUE : FALSE, 'class'=>'form-control', 'placeholder'=>$object->getAttributeLabel($field)));?>
										<script type="text/javascript">
											var editor = CKEDITOR.replace('<?php echo get_class($object);?>_<?php echo $field;?>');
											editor.on( 'change', function( evt ) {
												// getData() returns CKEditor's HTML content.
												$('#<?php echo get_class($object);?>_<?php echo $field;?>').html(evt.editor.getData());
											});
										</script>
									<?php endif;?>

									<?php if ($attributes['type'] == 'password'):?>
										<?php echo $form->passwordField($object, $field, array('disabled' => (isset($isDeleteForm) && $isDeleteForm==TRUE)? TRUE : FALSE, 'class'=>'form-control', 'value'=>'', 'placeholder'=>$object->getAttributeLabel($field)));?>
									<?php endif;?>

									<?php if ($attributes['type'] == 'select'):?>
										<?php echo $form->dropDownList($object, $field, $attributes['value'], array('disabled' => (isset($isDeleteForm) && $isDeleteForm==TRUE)? TRUE : FALSE, 'class' => 'form-control', 'prompt' => '---'));?>
									<?php endif;?>

									<?php if ($attributes['type'] == 'email'):?>
										<?php echo $form->emailField($object, $field, array('class'=>'form-control input-sm', 'autocomplete'=>'off'));?>
									<?php endif;?>

									<?php if ($attributes['type'] == 'radio'):?>
										<div class="col-sm-5">
											<div class="radio">
													<?php echo $form->radioButtonList($object, $field, $attributes['value'], array('disabled' => (isset($isDeleteForm) && $isDeleteForm==TRUE)? TRUE : FALSE));?>

											</div>
										</div>
									<?php endif;?>
                                                                    
                                                                        <?php // if ($attributes['type'] == 'textarea'):?>
										<?php // echo $form->textArea($object, $field, array('class'=>'form-control', 'placeholder'=>$object->getAttributeLabel($field)));?>
									<?php // endif;?>
								</div>
							</div>
						<?php endif;?>
					<?php endforeach;?>
					<div class="form-group">
                                                <div class="col-sm-offset-3 col-sm-1">
                                                            <?php if ((isset($isSurvey) && isset($isDeleteForm) && $isDeleteForm == TRUE)):?>
                                                                <button type="submit" class="btn btn-danger">
                                                                Delete
                                                            <?php else:?>
                                                                <button type="submit" class="btn btn-primary">
                                                                Submit
                                                            <?php endif;?>
                                                        
                                                        </button>
                                                </div>
                                                <?php if ((isset($isSurvey) && $isSurvey == TRUE) && (isset($isDeleteForm) && !$isDeleteForm)):?>
                                                        <div class="col-sm-offset-1  col-sm-1">
                                                                <a href="<?php echo $this->createUrl('question/', array('id' => $object->Survey_ID));?>" title="View" class="btn btn-danger">
                                                                        Manage Questions
                                                                </a>
                                                        </div>
                                                <?php endif; ?>
					</div>
				<?php $this->endWidget();?>
			</div>
		</div>
	</div>
</div>


<script>
$(document).ready(function(){
	$('form').on('submit',function(e){
		// check if submitted via AJAX
		if ($('input[name="ajax"]').length && $('input[name="ajax"]').val())
		{
			// remove error style
			$('div.form-group').removeClass('has-error');

			// hide errors
			$('div#form-error').slideUp( 300 ).delay( 800 ).html();
			//$('div#form-error').hide();

			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
                        var customRedirectPage = "<?php echo (isset($this->redirect_page)) ? $this->redirect_page : ''; ?>";
                        
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				dataType: 'json',
				success:function(data, textStatus, jqXHR)
				{
					if (! data.status) {
						$('div#form-error').slideDown(300).delay(800).html(data.message);
						$('div#form-error').show();

						if (typeof data.fields !== 'undefined')
						{
							$.each(data.fields, function(index, value){
								$('label[for="'+value+'"]').parent().addClass('has-error');
							});
						}
					}
					else {
						$('#confirmation-modal .modal-header h4#form-modalLabel').html('Success');
						$('#confirmation-modal .modal-body #form-modalBody').html(data.message);
						$('#confirmation-modal').modal({backdrop: 'static', keyboard: false});

						setTimeout(function() {
							if(!customRedirectPage) {
                                                            window.location.href = currentIndex;
                                                        } 
                                                        else {
                                                            window.location.href =  customRedirectPage;
                                                        }
						  }, 2000);
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					//if fails     
				}
			});
			e.preventDefault(); //STOP default action
		}
	});
});
</script>
<?php $this->widget('widgets.ajax.Modal');?>