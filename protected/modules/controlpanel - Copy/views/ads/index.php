<div class="row">
	<div class="col-md-12">
		<div class="content-panel">
			<?php echo CHtml::beginForm();?>
				<?php echo CHtml::hiddenField('ajax', 1);?>
				<table class="table table-hover">
					<thead>
						<tr>
							<th class="col-md-3"><?php echo CHtml::encode(Ads::model()->getAttributeLabel('Title')); ?></th>
							<th class="col-md-3"><?php echo CHtml::encode(Ads::model()->getAttributeLabel('Image')); ?></th>
							<th class="col-md-4"><?php echo CHtml::encode(Ads::model()->getAttributeLabel('URL')); ?></th>
							<th class="text-center col-md-2"><?php echo CHtml::encode(Ads::model()->getAttributeLabel('Actions')); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php if ( ! empty($ads)):?>
							<?php foreach ($ads as $a):?>
							<tr>
								<td><?php echo $a->Title;?></td>
								<td>
									<?php echo CHtml::image(Link::image_url("client/$a->Nas_ID/ads/$a->Image"), $a->Title, array('width'=>100));?>
								</td>
								<td><?php echo $a->Url;?></td>
								<td class="text-center col-md-1">
									<a href="<?php echo $this->createUrl($this->id.'/update', array('id'=>$a->Ads_ID));?>" title="Edit" class="btn btn-default">
										<i class="fa fa-edit"></i>
									</a>
									<a href="<?php echo $this->createUrl($this->id.'/delete', array('id'=>$a->Ads_ID));?>" title="Delete" class="btn btn-default">
										<i class="fa fa-trash-o"></i>
									</a>
								</td>
							</tr>
							<?php endforeach;?>
						<?php else:?>
						<tr>
							<td colspan="3">
								<i class="fa fa-exclamation-circle"></i>
								No records found.
							</td>
						</tr>
						<?php endif;?>
					</tbody>
				</table>
			<?php echo CHtml::endForm();?>
		</div>
	</div>
</div>