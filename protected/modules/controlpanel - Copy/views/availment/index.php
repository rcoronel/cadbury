<div class="col-md-12 mt">
	<div class="content-panel">
		<?php echo CHtml::beginForm();?>
			<?php echo CHtml::hiddenField('ajax', 1);?>
			<table class="table table-hover">
				<thead>
					<tr>
						<th class="col-md-2"><?php echo CHtml::encode(Availment::model()->getAttributeLabel('Name')); ?></th>
						<th class="col-md-3"><?php echo CHtml::encode(Availment::model()->getAttributeLabel('Description')); ?></th>
						<th class="col-md-2 text-center">Status</th>
						<th class="col-md-1 text-center"><?php echo CHtml::encode(Availment::model()->getAttributeLabel('Duration')); ?> (in minutes)</th>
						<th class="col-md-1 text-center"><?php echo CHtml::encode(Availment::model()->getAttributeLabel('Is_recurring')); ?></th>
						<th class="col-md-1 text-center">Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php if ( ! empty($availments)):?>
						<?php foreach ($availments as $a):?>
							<tr>
								<td><?php echo $a->Name;?></td>
								<!-- <td><?php // echo $a->Description;?></td> -->
								<td class="text-center">
									<?php echo CHtml::textField("description[$a->Availment_ID]", $a->Description, array('class'=>'form-control col-md-1 input-sm '));?>
								</td>
								<td class="text-center">
									<?php echo CHtml::dropDownList("status[$a->Availment_ID]", $a->is_available, array('1'=>'Enabled', '0'=>'Disabled'), array('class'=>'form-control'));?>
								</td>
								<td class="text-center">
									<?php echo CHtml::textField("duration[$a->Availment_ID]", $a->duration, array('class'=>'form-control col-md-1 input-sm number-only'));?>
								</td>
								<td>
									<?php echo CHtml::dropDownList("recurring[$a->Availment_ID]", $a->is_recurring, array(1=>'Yes', 0=>'No'), array('class'=>'form-control col-md-1 input-sm'));?>
								</td>
								<td class="text-center col-md-1">
								<?php if($this->permissions->Edit == 1): ?>
									<div class="btn-group-action text-center">
										<div class="btn-group">
											<a href="<?php echo $this->createUrl($this->id.'/view', array('id'=>$a->Availment_ID));?>" title="View" class="btn btn-default">
												<i class="fa fa-eye"></i>
											</a>
										</div>
									</div>
								<?php endif; ?>
								</td>
							</tr>
						<?php endforeach;?>
					<?php endif;?>
					<?php if($this->permissions->Edit == 1): ?>
						<tr>
							<td colspan="8" class="text-right">
								<?php echo CHtml::button('Submit', array('type'=>'submit', 'class'=>'btn btn-primary'));?>
							</td>
						</tr>
				<?php endif; ?>
				</tbody>
			</table>
		<?php echo CHtml::endForm();?>
	</div>
</div>
<script>
$(document).ready(function(){
	$('select[id^="status"]').on('change', function() {
		var row = $(this).closest('tr');
		if ($(this).val() == 0) {
			$(row).find('select').prop('disabled', 'disabled');
			$(row).find('input').prop('disabled', 'disabled');
			$(this).prop('disabled', false);
		}
		else {
			$(row).find('input').prop('disabled', false);
			$(row).find('select').prop('disabled', false);
		}
		
	});
	$('form').on('submit',function(e){
		// check if submitted via AJAX
		if ($('input[name="ajax"]').length && $('input[name="ajax"]').val()) {
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			$.ajax({
				url : formURL,
				type: "POST",
				data : postData,
				dataType: 'json',
				success:function(data, textStatus, jqXHR) {
					toastr.success(data.message, '', {closeButton:true,showDuration:"300",timeOut: "3000"});
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					//if fails     
				}
			});
			e.preventDefault(); //STOP default action
		}
	});
	$(document).on("keydown", ".number-only", function(event) {
		console.log("number");
		if ( event.keyCode == 46 || event.keyCode == 8  ||  event.keyCode == 9) {
		} else {
			if (event.keyCode < 48 || event.keyCode > 57 ) {
				event.preventDefault();	
			} 	
		}
	});
});
</script>
<?php $this->widget('widgets.ajax.Modal');?>