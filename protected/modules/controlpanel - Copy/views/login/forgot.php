<?php 
// echo "<pre>";
// 		print_r($email);
// 		die();

?>
<div id="login-page">
	<div class="container">

		<?php $form=$this->beginWidget('CActiveForm', array('id'=>'form-login', 'action'=>Yii::app()->createAbsoluteURL('controlpanel/login'), 'htmlOptions'=>array('class'=>'form-login login-shadow-effect'))); ?>
			<div id="form-login-error" class="alert alert-danger" style="display:none;"></div>
			<h2 class="form-login-heading">Reset Password</h2>
			<div class="login-wrap">
				<div class="input-group">
					<?php echo CHtml::passwordField('new-password', '', array('class'=>'form-control', 'placeholder'=>'New Password', 'autocomplete'=>'off')); ?>
				</div>
				<br>
				<div class="input-group">
					<?php echo CHtml::passwordField('new-password-repeat', '', array('class'=>'form-control', 'placeholder'=>'Type new password again to confirm', 'autocomplete'=>'off', 'value'=>'')); ?>
					<?php echo CHtml::hiddenField('client-email', $email);?>
				</div>
				<button id="forgot-submit" class="btn btn-theme btn-block cms-btn" type="submit"><i class="fa fa-lock"></i>SUBMIT</button>
			</div>

		<?php $this->endWidget();?>

	</div>
</div>


<script type="text/javascript">
	jQuery(document).ready(function($)
	{

		$("#error_message").hide();

		var status = <?php echo (isset($status) ? $status : 1); ?>;
		var redirect = '<?php echo (isset($redirect) ? $redirect : ''); ?>';
		var message = '<?php echo (isset($message) ? $message : ''); ?>';
		console.log("status:" + status);

		if(!status){
			$('#confirmation-modal .modal-header h4#form-modalLabel').html('Link Expired');
			$('#confirmation-modal .modal-body #form-modalBody').html(message);
			$('#confirmation-modal').modal({backdrop: 'static', keyboard: false});
			setTimeout(function() {
				window.location.href = redirect;
			  }, 2000);
		}

		$('form#form-login').on('submit', function(e){
			
			var new_password = $("#new-password").val();
			var repeat_password = $("#new-password-repeat").val();
			console.log("pass: " + new_password + " : " + repeat_password);

			if(new_password.length < 6){
				$('div.alert-danger').html("Ooops! Password is too short (minimum is 6 characters).");
				$('div.alert-danger').show();
				return false;
			}

			if(new_password != repeat_password){
				return false;
			}else{
				$('div.alert-danger').hide();
				$('div.alert-danger').html('');
			}
			
			$.post('', $(this).serialize(), function(data){
				console.log(data);
				if ( ! data.status)
				{
					$('div.alert-danger').html(data.message);
					$('div.alert-danger').show();
					$('html, body').animate({ scrollTop: $("div.alert").offset().top - 25}, 300);
				}
				else
				{
					$('#confirmation-modal .modal-header h4#form-modalLabel').html('Password Updated');
					$('#confirmation-modal .modal-body #form-modalBody').html('Password Updated');
					$('#confirmation-modal').modal({backdrop: 'static', keyboard: false});
					setTimeout(function() {
						window.location.href = data.redirect;
					  }, 2000);
				}
			}, 'json');
			
			e.preventDefault();
			return false;
		});

		$(document).on("keyup", "#new-password-repeat", function(e){
			var new_password = $("#new-password").val();
			var repeat_password = $("#new-password-repeat").val();

			if(new_password != repeat_password){
				$('div.alert-danger').html("Ooops! Password does not match.");
				$('div.alert-danger').show();
			}else{
				$('div.alert-danger').hide();
				$('div.alert-danger').html('');
			}

		});

	});
</script>
<?php $this->widget('widgets.ajax.Modal');?>