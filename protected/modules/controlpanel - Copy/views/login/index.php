<div id="login-page">
	<div class="container">

		<?php $form=$this->beginWidget('CActiveForm', array('id'=>'form-login', 'htmlOptions'=>array('class'=>'form-login login-shadow-effect'))); ?>
			<div id="form-login-error" class="alert alert-danger" style="display:none;"></div>
			<h2 class="form-login-heading">sign in now</h2>
			<div class="login-wrap">
				<div class="input-group">
					<?php echo $form->textField($admin, 'Email', array('class'=>'form-control', 'placeholder'=>'E-mail Address', 'autocomplete'=>'off')); ?>
				</div>
				<br>
				<div class="input-group">
					<?php echo $form->passwordField($admin, 'Password', array('class'=>'form-control', 'placeholder'=>'Password', 'autocomplete'=>'off', 'value'=>'')); ?>
				</div>
				<label class="checkbox">
					<span class="pull-right">
						<a data-toggle="modal" href="login.html#myModal" class="forgot-pass"> Forgot Password?</a>
					</span>
				</label>
				<button class="btn btn-theme btn-block cms-btn" href="index.html" type="submit"><i class="fa fa-lock"></i> SIGN IN</button>
			</div>

			

		<?php $this->endWidget();?>

		<?php $form=$this->beginWidget('CActiveForm', array('id'=>'forgot-pass', 'htmlOptions'=>array('class'=>'form-login login-shadow-effect'))); ?>

			<!-- Modal -->
			<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">Forgot Password ?</h4>
						</div>
						<div class="modal-body">
							<p>Enter your e-mail address below to reset your password.</p>
							<input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">

						</div>
						<div class="modal-footer">
							<button id="forgot-modal-close" data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
							<button id="forgot-submit" class="btn btn-default" href="index.html" type="submit"><i class="fa fa-lock"></i>Submit</button>
						</div>
					</div>
				</div>
			</div>
			<!-- modal -->

		<?php $this->endWidget();?>

	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($)
	{
		$('form#form-login').on('submit', function(e){
			$('div.alert-danger').hide();
			$('div.alert-danger').html('');
			$.post('', $(this).serialize(), function(data){
				if ( ! data.status)
				{
					$('div.alert-danger').html(data.message);
					$('div.alert-danger').show();
					$('html, body').animate({ scrollTop: $("div.alert").offset().top - 25}, 300);
				}
				else
				{
					setTimeout(function() {
						$('#confirmation-modal .modal-header h4#form-modalLabel').html('Account validated');
						$('#confirmation-modal .modal-body #form-modalBody').html('Logging in');
						$('#confirmation-modal').modal({backdrop: 'static', keyboard: false});
					}, 900);
					
					setTimeout(function() {
						window.location.href = data.redirect;
					  }, 2000);
				}
			}, 'json');
			
			e.preventDefault();
			return false;
		});

		$('form#forgot-pass').on('submit', function(e){
			$('div.alert-danger').hide();
			$('div.alert-danger').html('');
			
			$.post('', $(this).serialize(), function(data){
				console.log(data);
				if ( ! data.status)
				{
					$('#myModal').modal('toggle');
					$('div.alert-danger').html(data.message);
					$('div.alert-danger').show();
					$('html, body').animate({ scrollTop: $("div.alert").offset().top - 25}, 300);
				}
				else
				{
					$('#confirmation-modal .modal-header h4#form-modalLabel').html('');
					$('#confirmation-modal .modal-body #form-modalBody').html('Sending email for password reset');
					$('#confirmation-modal').modal({backdrop: 'static', keyboard: false});
					setTimeout(function() {
						window.location.href = data.redirect;
					  }, 2000);
				}
			}, 'json');
			
			e.preventDefault();
			return false;
		});

	});
</script>
<?php $this->widget('widgets.ajax.Modal');?>