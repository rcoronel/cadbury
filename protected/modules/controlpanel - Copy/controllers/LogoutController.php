<?php

class LogoutController extends ControlpanelController
{	
	public function actionIndex()
	{
		Yii::app()->getModule('controlpanel')->client->logout();
		$this->redirect(array('login/'));
	}
}