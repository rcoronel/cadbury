<?php

class SurveyController extends ControlpanelController
{
        /**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
       
        public $count; // Used for counting the rows returned
        
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action)){
			// action buttons
			$this->actionButtons['index'] = array('add' => array('link' => $this->createURL($this->id.'/add/')));
			$this->actionButtons['view'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['add'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['delete'] = array('back' => array('link' => $this->createURL($this->id.'/')));

			return true;
		}
	}
        
        
	public function actionIndex()
	{
                // declare scripts
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'plugins', 'jquery.tablednd.js')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/jquery.toastr.min.js'));
                
		// fields to be displayed
		$row_fields = array();
		$row_fields['Survey_ID']['type'] = 'text';
		$row_fields['Survey_ID']['class'] = 'text-center col-md-1';
		
		$row_fields['Title']['type'] = 'text';
		$row_fields['Title']['class'] = 'col-mid-4';
		
		$row_fields['Is_active']['type'] = 'select';
		$row_fields['Is_active']['class'] = 'text-center col-md-1';
		$row_fields['Is_active']['value'] = array('1' => 'Enabled', '0' => 'Disabled');
		$row_fields['Is_active']['mode'] = 'bool';
		
		$row_fields['Position']['type'] = 'text';
		$row_fields['Position']['class'] = 'text-center col-md-1 dragHandle';
                
                $row_fields['Is_ad']['type'] = 'select';
		$row_fields['Is_ad']['class'] = 'text-center col-md-1';
		$row_fields['Is_ad']['value'] = array('1' => 'True', '0' => 'False');
		$row_fields['Is_ad']['mode'] = 'bool';
                
                $row_fields['Updated_at']['type'] = 'text';
                
                $row_fields['question_count']['type'] = 'text';

		$row_actions = array('question', 'edit', 'delete');              
                
                // List and pagination
		$object = new Survey;
		$object->attributes = Survey::setSessionAttributes(Yii::app()->request->getPost('Survey'));
		
                $criteria = $object->search()->criteria;
		$criteria->compare('Nas_ID', $this->context->nas->id);
		
                $count = Survey::model()->count($criteria);
		
		$pages = new CPagination($count);
                $criteria->select = array(
                    new CDbExpression("*"),
                    new CDbExpression("(SELECT COUNT(*) FROM survey_question WHERE t.Survey_ID = Survey_ID) AS 'question_count'")
                );
                
		$criteria->limit = 25;
                $criteria->order = "Position ASC";
		$criteria->offset = (Yii::app()->request->getQuery('page', 1) * $criteria->limit) - $criteria->limit;
		
		$pages->pageSize=$criteria->limit;
		$pages->applyLimit($criteria);
		
		$surveys = Survey::model()->findAll($criteria);
                $this->count = count($surveys);
                
                // FOR INCOMPLETE SHORTCUT LIST
                $criteria->select = array(
                            new CDbExpression("Survey_ID"),
                            new CDbExpression("Title"),
                );
                $criteria->addCondition("(SELECT COUNT(*) FROM `survey_question` B WHERE t.Survey_ID = B.Survey_ID) < 1 OR".
                    "(SELECT COUNT(*) FROM `survey_question` B WHERE t.Survey_ID = B.Survey_ID  AND". 
                    "(SELECT COUNT(*) FROM `survey_answer` C WHERE B.Survey_question_ID = C.Survey_question_ID ) < 1) > 0");
                $count_inc = Survey::model()->findAll($criteria);
                // END INCOMPLETE SHORTCUT LIST
                                
                $this->render('/layouts/list', array('object' => $object, 'objectList'=>$surveys, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions, 'pages'=>$pages, 'incomplete'=>$count_inc));
	}
        
        // Add Form
	public function actionAdd()
	{
                $survey = new Survey;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($survey);
		}
		
		self::_renderForm($survey);
	}
        
        // Delete Form
	public function actionDelete($id)
	{       
                $survey = Survey::model()->findByPk($id);
		
		if (Yii::app()->request->isPostRequest) {
			
                          self::_postDelete($id);
		}
		
		self::_renderForm($survey, TRUE, TRUE);
	}
        
        // Update Form
	public function actionUpdate($id)
	{
		$survey = Survey::model()->findByPk($id);
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($survey);
		}
		
		self::_renderForm($survey, TRUE);
	}

	public function actionView($id)
	{
                $survey = Survey::model()->findByPk($id);
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($survey);
		}
		
		self::_renderForm($survey);
	}
        
        /**
	 * Render form
	 * 
	 * @access private
	 * @param object $object Survey instance
	 * @return void
	 */
	private function _renderForm(Survey $object, $isSurvey = null, $isDeleteForm = null)
	{
		$fields = array();
                    
                $fields['Title']['type'] = 'text';
                $fields['Title']['class'] = 'col-md-5';

                $fields['Is_active']['type'] = 'radio';
                $fields['Is_active']['class'] = 'col-lg-5';
                $fields['Is_active']['value'] = array('1' => 'Enabled', '0' => 'Disabled');
                
                $fields['Is_ad']['type'] = 'radio';
                $fields['Is_ad']['class'] = 'col-lg-5';
                $fields['Is_ad']['value'] = array('1' => 'True', '0' => 'False');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
                                
                $this->render('/layouts/form', array('object' => $object, 'fields' => $fields, 'isSurvey'=>$isSurvey, 'isDeleteForm'=>$isDeleteForm));
	}
        
        /**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object Survey instance
	 * @return	void
	 */
	private function _postValidate(Survey $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('Survey');
		$object->attributes = $post;
                $object->Nas_ID = $this->context->nas->id;
                $object->Updated_at = date("Y-m-d H:i:s",strtotime(date("Y-m-d")));
                
		if ($object->validate())
		{
			if ($object->save()) {
				// add all access to main client
				
				self::displayFormSuccess();
			}
		}
		else {
			self::displayFormError($object);
		}
	}
        
        /**
	 * Delete the form entry
	 * 
	 * @access	public
	 * @param object $object Survey instance
	 * @return	void
	 */
	private function _postDelete($id=null)
	{
		//Questions         
                $question_id = array();
                foreach(SurveyQuestion::model()->findAllByAttributes(array('Survey_ID' => $id)) as $q) {
                        array_push($question_id, $q['Survey_question_ID']);
                }
                // Answers
                // Criteria for Survey Answer
                $criteria = new CDbCriteria();
                $criteria->addInCondition("Survey_question_ID", $question_id);

                // Deletion of records
                Survey::model()->deleteAllByAttributes(array("Survey_ID"=>$id));
                SurveyQuestion::model()->deleteAllByAttributes(array("Survey_ID"=>$id));
                SurveyAnswer::model()->deleteAll($criteria);
                
                self::displayFormSuccess('Record successfully deleted!');
	}
        
        /**
	 * Update menu position
	 * 
	 * @access active
	 * @return string
	 */
	public function actionPosition()
	{
		$query = Yii::app()->request->getQuery('page-list');
		$menu = new Survey;
		foreach ($query as $position => $id) {
			$menu->updateByPk($id, array('Position' => (int)$position + 1));
		}
		
		die('Repositioning successful. Refresh page to view changes');
	}
}