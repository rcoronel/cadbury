<?php

class Availment_listController extends ControlpanelController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action)) {
			// action buttons
			$this->actionButtons['index'] = array('add' => array('link' => $this->createURL($this->id.'/add')));
			$this->actionButtons['view'] = array('back' => array('link' => $this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		
		// fields to be displayed
		$row_fields = array();
		$row_fields['Availment_ID']['type'] = 'text';
		$row_fields['Availment_ID']['class'] = 'text-center col-md-1';
		
		$row_fields['Name']['type'] = 'text';
		$row_fields['Name']['class'] = 'col-mid-2';
		
		$row_fields['Code']['type'] = 'text';
		$row_fields['Code']['class'] = 'col-mid-1';
		
		$row_actions = array('view');
		
		$availments = Availment::model()->findAll("Is_active = 1");
		$this->render('/layouts/list', array('object' => new Availment, 'objectList'=>$availments, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
	}
	
	/**
	 * View
	 * 
	 * @access public
	 * @return void
	 */
	public function actionView($id)
	{
		$availment = Availment::model()->findByPk($id);
		Availment::validateObject($availment, 'Availment');
		
		$this->render('view', array('availment'=>$availment));
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id Availment ID
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$availment = Availment::model()->findByPk($id);
		Availment::validateObject($availment, 'Availment');
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($availment);
		}
		
		self::_renderForm($availment);
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object Availment instance
	 * @return void
	 */
	private function _renderForm(Availment $object)
	{
		$fields = array();
		
		$nass = RadiusNas::model()->findAll();
		$fields['Availment_role_ID']['type'] = 'select';
		$fields['Availment_role_ID']['class'] = 'col-md-5';
		$fields['Availment_role_ID']['class'] = 'col-md-5';
		$fields['Availment_role_ID']['value'] = CHtml::listData($nass, 'id', 'shortname');
		
		$fields['Email']['type'] = 'text';
		$fields['Email']['class'] = 'col-md-5';
		
		$fields['Password']['type'] = 'password';
		$fields['Password']['class'] = 'col-md-5';
		
		$fields['Confirm_password']['type'] = 'password';
		$fields['Confirm_password']['class'] = 'col-md-5';
		
		$fields['Firstname']['type'] = 'text';
		$fields['Firstname']['class'] = 'col-md-5';
		
		$fields['Lastname']['type'] = 'text';
		$fields['Lastname']['class'] = 'col-md-5';
		
		$fields['Is_active']['type'] = 'radio';
		$fields['Is_active']['class'] = 'col-md-5';
		$fields['Is_active']['value'] = array('1' => 'Can login', '0' => 'Cannot login');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object' => $object, 'fields' => $fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object Availment instance
	 * @return	void
	 */
	private function _postValidate(Availment $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('Availment');
		$object->attributes = $post;
		$object->Nas_ID = $this->context->nas->id;
		
		// Validate object
		if ($object->validate()) {
			// Forcing Confirm_password to be the same as Password
			// We do this in order to avoid redundant error on save
			$object->Password = CPasswordHelper::hashPassword($object->Password);
			$object->Confirm_password = $object->Password;
			if ($object->save()) {
				self::displayFormSuccess();
			}
		}
		self::displayFormError($object);
	}
}