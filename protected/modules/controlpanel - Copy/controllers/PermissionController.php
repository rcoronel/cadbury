<?php

class PermissionController extends ControlpanelController
{
	public function actionIndex()
	{
		$this->redirect(array('permission/update', 'id' => 1));
	}
	
	public function actionUpdate($id)
	{
		if (Yii::app()->request->isPostRequest) {
			self::_handleRequest();
		}
		$role = ClientRole::model()->findByPk($id);
		ClientRole::validateObject($role, 'ClientRole');
		
		$roles = array_merge(ClientRole::model()->findAllByAttributes(array('Client_role_ID'=>1)), ClientRole::model()->findAll("Nas_ID = {$this->context->nas->id}"));
		
		// menus
		$menus = ClientMenu::model()->findAll(array('order' => 'Position ASC'));
		$permissions = ClientRolePermission::model()->findAll("Client_role_ID = {$role->Client_role_ID}");
		
		foreach ($menus as $m) {
			foreach ($permissions as $p) {
				if ($m->Client_menu_ID == $p->Client_menu_ID) {
					$m->View = $p->View;
					$m->Add = $p->Add;
					$m->Edit = $p->Edit;
					$m->Delete = $p->Delete;
				}
			}
		}
		
		$this->render('form', array('roles' => $roles, 'role'=>$role, 'menus'=>$menus));
	}
	
	private function _handleRequest()
	{
		if (Yii::app()->request->getPost('Client_role_ID')) {
			$post = Yii::app()->request->getPost('client_menu');
			foreach ($post as $p) {
				$permission = new ClientRolePermission;
				
				// delete existing permissions
				$permission->deleteAllByAttributes(array('Client_role_ID'=>Yii::app()->request->getPost('Client_role_ID'), 'Client_menu_ID'=>$p['Client_menu_ID']));
				
				$permission->Client_role_ID = Yii::app()->request->getPost('Client_role_ID');
				$permission->Client_menu_ID = $p['Client_menu_ID'];
				$permission->View = (isset($p['View'])) ? $p['View'] : 0;
				$permission->Add = (isset($p['Add'])) ? $p['Add'] : 0;
				$permission->Edit = (isset($p['Edit'])) ? $p['Edit'] : 0;
				$permission->Delete = (isset($p['Delete'])) ? $p['Delete'] : 0;
				$permission->save();
			}
			Yii::app()->getModule('controlpanel')->client->setFlash('success', 'Permissions updated');
			$this->redirect(array("$this->id/update", 'id'=>Yii::app()->request->getPost('Client_role_ID')));
		}
	}
}