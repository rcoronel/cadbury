<?php

class DashboardController extends ControlpanelController
{
	public function actionIndex()
	{
		// declare script
		Yii::app()->clientScript->registerScript('poll_url', "var POLL_URL = '{$this->createAbsoluteUrl('dashboard/poll')}'", CClientScript::POS_HEAD);
		
		$connection_count = DeviceConnection::pollAll(date('Y-m-d'), $this->context->nas->id);
		$authenticated_count = DeviceConnection::pollAuthAll(date('Y-m-d'), $this->context->nas->id);
		
		$this->render('index', array('connection_count'=>$connection_count,
				'authenticated_count'=>$authenticated_count));
	}
	
	/**
	 * Get count
	 * 
	 * @access public
	 * @return json
	 */
	public function actionPoll()
	{
		$today = date('Y-m-d');
		$connection_count = DeviceConnection::pollAll($today, $this->context->nas->id);
		$authenticated_count = DeviceConnection::pollAuthAll($today, $this->context->nas->id);
		
		die(CJSON::encode(array('connection_count'=>number_format($connection_count), 'authenticated_count'=>number_format($authenticated_count))));
	}
}