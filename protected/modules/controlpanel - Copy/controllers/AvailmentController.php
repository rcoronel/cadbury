<?php

class AvailmentController extends ControlpanelController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action)) {
			// action buttons
			$this->actionButtons['view'] = array('back' => array('link' => $this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * Display availment form
	 * 
	 * @access public
	 * @return void
	 */
	public function actionIndex()
	{
		
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/jquery.toastr.min.js'));
		
		if (Yii::app()->request->isPostRequest) {
			self::_validatePost();
		}
		
		$availments = Availment::model()->findAll("Is_active = 1 ORDER BY Name");
		$nas_avails = NasAvailment::model()->findAllByAttributes(array('Nas_ID'=>$this->context->nas->id));
		foreach ($availments as $a) {
			$a->is_available = 0;
			$a->duration = 0;
			if ( ! empty($nas_avails)) {
				foreach ($nas_avails as $na) {
					if ($na->Availment_ID == $a->Availment_ID) {
						$a->is_available = 1;
						$a->duration = $na->Duration / 60;
						$a->is_recurring = $na->Is_recurring;
						$a->Description = $na->Description;
					}
				}
			}
		}
		
		$this->render('index', array('availments'=>$availments));
	}

	public function actionUpdate()
	{
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/jquery.toastr.min.js'));
		
		if (Yii::app()->request->isPostRequest) {
			self::_validatePost();
		}
		
		$availments = Availment::model()->findAll("Is_active = 1");
		$nas_avails = NasAvailment::model()->findAllByAttributes(array('Nas_ID'=>$this->context->nas->id));
		foreach ($availments as $a) {
			$a->is_available = 0;
			$a->duration = 0;
			if ( ! empty($nas_avails)) {
				foreach ($nas_avails as $na) {
					if ($na->Availment_ID == $a->Availment_ID) {
						$a->is_available = 1;
						$a->duration = $na->Duration / 60;
						$a->is_recurring = $na->Is_recurring;
					}
				}
			}
		}
		
		$this->render('index', array('availments'=>$availments));
	}
	
	/**
	 * View
	 * 
	 * @access public
	 * @return void
	 */
	public function actionView($id)
	{
		$availment = Availment::model()->findByPk($id);
		Availment::validateObject($availment, 'Availment');
		
		$this->render('view', array('availment'=>$availment));
	}
	
	/**
	 * Validate the post
	 * 
	 * @access private
	 * @return string
	 */
	private function _validatePost()
	{
		sleep(1);
		$post = Yii::app()->request->getPost('status');
		NasAvailment::model()->deleteAllByAttributes(array('Nas_ID'=>$this->context->nas->id));
		foreach ($post as $id=>$availability) {
			if ($availability) {
				$nas_avail = new NasAvailment;
				$nas_avail->Nas_ID = $this->context->nas->id;
				$nas_avail->Availment_ID = $id;
				$nas_avail->Duration = (int)($_POST['duration'][$id] * 60);
				$nas_avail->Is_recurring = (int)$_POST['recurring'][$id];
				$nas_avail->Description = $_POST['description'][$id];
				$nas_avail->validate() && $nas_avail->save();
			}
		}
		
		die(CJSON::encode(array('status'=>1, 'message'=>'Settings saved')));
	}
}