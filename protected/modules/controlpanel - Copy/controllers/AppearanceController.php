<?php

class AppearanceController extends ControlpanelController
{
	/**
	 * View website appearance settings
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		if (Yii::app()->request->isPostRequest) {
			self::_handlePost();
		}
		
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/ckeditor/ckeditor.js'));
		
		$data = array(	'site_title' => NasConfiguration::getValue('SITE_TITLE'),
						'landing_message' => NasConfiguration::getValue('LANDING_MESSAGE'),
						'welcome' => NasConfiguration::getValue('WELCOME_BACK'),
						'redirect_url' => NasConfiguration::getValue('REDIRECT_URL'),
						'logo' => NasConfiguration::getvalue('LOGO'),
						'inside_logo' => NasConfiguration::getvalue('INSIDE_LOGO'),
						'center_image' => NasConfiguration::getValue('CENTER_IMAGE'),
						'favicon' => NasConfiguration::getvalue('FAVICON'),
						'splash' => NasConfiguration::getvalue('SPLASH_IMG'),
						'powered_welcome' => NasConfiguration::getvalue('POWERED_WELCOME'),
						'powered_inside' => NasConfiguration::getvalue('POWERED_INSIDE'),
						'css_file' => NasConfiguration::getValue('CSS_FILE'),
						'css_contents' => @file_get_contents(dirname(Yii::app()->request->scriptFile).'/css/client/'.$this->context->nas->id.'/'.NasConfiguration::getValue('CSS_FILE')));
		
		$this->render('form', $data);
	}
	
	/**
	 * Delete appearance settings and files
	 * 
	 * @access public
	 * @param string $config
	 * @return void
	 */
	public function actionDelete($config)
	{
		switch ($config) {
			case 'LOGO':
			case 'INSIDE_LOGO':
			case 'CENTER_IMAGE':
			case 'FAVICON':
			case 'SPLASH_IMG':
				@unlink(dirname(Yii::app()->request->scriptFile).'/images/client/'.$this->context->nas->id.'/'.NasConfiguration::getValue($config));
			break;

			case 'CSS_FILE':
				@unlink(dirname(Yii::app()->request->scriptFile).'/css/client/'.$this->context->nas->id.'/'.NasConfiguration::getValue($config));
			break;
		}
		NasConfiguration::setValue($config, '');
		
		Yii::app()->getModule('controlpanel')->client->setFlash('success', 'Settings applied');
		$this->redirect(array($this->id.'/'));
	}
	
	/**
	 * Set configuration value
	 * 
	 * @access private
	 * @return void
	 */
	private function _handlePost()
	{
		$post = Yii::app()->request->getPost('NasConfiguration');

		// echo "<pre>";
		// print_r($post);
		// die();
		
		// create CSS
		if ( ! empty($post['CSS_FILE'])) {
			$post['CSS_FILE'] = self::_uploadStyle($post['CSS_FILE']);
		}
		
		foreach ($post as $config => $value) {
			NasConfiguration::setValue($config, $value);
		}
		
		if ( ! empty($_FILES)) {
			self::_upload($_FILES);
		}
			
		Yii::app()->getModule('controlpanel')->client->setFlash('success', 'Settings applied');
	}
	
	/**
	 * Upload images based on configuration
	 * 
	 * @access	private
	 * @return	void
	 */
	private function _upload($files)
	{
		foreach ($files as $name => $attr) {
			if ($attr['size'] && ! $attr['error']) {
				switch ($name) {
					case 'LOGO':
						$filename = 'logo';
						self::_uploadImage($name, $filename);
					break;
				
					case 'INSIDE_LOGO':
						$filename = 'inside_logo';
						self::_uploadImage($name, $filename);
					break;
				
					case 'CENTER_IMAGE':
						$filename = 'center_image';
						self::_uploadImage($name, $filename);
					break;
				
					case 'FAVICON':
						$filename = 'favicon';
						self::_uploadImage($name, $filename);
					break;
				
					case 'SPLASH_IMG':
						$filename = 'splash';
						self::_uploadImage($name, $filename);
					break;
				}
			}
		}
	}
	
	private function _uploadImage($name, $filename)
	{
		$image_path = dirname(Yii::app()->request->scriptFile).'/images/client/'.$this->context->nas->id.'/';
		if ( ! is_dir($image_path)) {
			mkdir($image_path, 0777, true);
		}
		
		$image = CUploadedFile::getInstanceByName($name);
		$filename = $filename.'.'.$image->extensionName;
		
		// delete current file
		$previous_file = NasConfiguration::getValue($name);
		@unlink($image_path.$previous_file);
		
		// check for successful save
		if ($image->saveAs($image_path.$filename)) {
			NasConfiguration::setvalue($name, $filename);
		}
	}
	
	private function _uploadStyle($css)
	{
		$css_path = dirname(Yii::app()->request->scriptFile).'/css/client/'.$this->context->nas->id.'/';
		if ( ! is_dir($css_path)) {
			mkdir($css_path, 0777, true);
		}
		
		$css_file = fopen($css_path.'styles.css', 'w');
		fwrite($css_file, $css);
		fclose($css_file);
		
		return 'styles.css';
	}
}