<?php

class RoleController extends ControlpanelController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action))
		{
			// action buttons
			$this->actionButtons['index'] = array('add' => array('link' => $this->createURL($this->id.'/add')));
			$this->actionButtons['add'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back' => array('link' => $this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		// fields to be displayed
		$row_fields = array();
		$row_fields['Client_role_ID']['type'] = 'text';
		$row_fields['Client_role_ID']['class'] = 'text-center col-md-1';
		
		$row_fields['Role']['type'] = 'text';
		$row_fields['Role']['class'] = 'col-mid-2';
		
		$row_fields['Is_active']['type'] = 'select';
		$row_fields['Is_active']['class'] = 'text-center col-md-1';
		$row_fields['Is_active']['value'] = array('1'=>'Enabled', '0' =>'Disabled');
		$row_fields['Is_active']['mode'] = 'bool';
		
		$row_actions = array('edit', 'delete');
		
		// List and pagination
		$object = new ClientRole;
		$object->attributes = ClientRole::setSessionAttributes(Yii::app()->request->getPost('ClientRole'));
		
		$criteria = $object->search()->criteria;
		$criteria->compare('Nas_ID', $this->context->nas->id);
		$count = ClientRole::model()->count($criteria);
		
		$pages = new CPagination($count);
		$criteria->limit = 25;
		$criteria->offset = (Yii::app()->request->getQuery('page', 1) * $criteria->limit) - $criteria->limit;
		
		$pages->pageSize=$criteria->limit;
		$pages->applyLimit($criteria);
		
		$roles = array_merge(ClientRole::model()->findAllByAttributes(array('Client_role_ID'=>1)), ClientRole::model()->findAll($criteria));
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$roles, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions, 'pages'=>$pages));
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		$role = new ClientRole;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($role);
		}
		
		self::_renderForm($role);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id ClientRole primary key
	 * @return void
	 */
	public function actionUpdate($id)
	{
		if ($id == 1) {
			Yii::app()->getModule('controlpanel')->client->setFlash('fail', 'Cannot update the Main Account');
			$this->redirect(array($this->id.'/'));
		}
		
		$role = ClientRole::model()->findByPk($id);
		parent::checkNas($role->Nas_ID);
		
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($role);
		}
		
		self::_renderForm($role);
	}
	
	/**
	 * Delete record
	 * 
	 * @access public
	 * @param int $id ClientRole ID
	 * @return void
	 */
	public function actionDelete($id)
	{
		$role = ClientRole::model()->findByPk($id);
		ClientRole::validateObject($role, 'ClientRole');
		
		// Count client that has this Role
		$count = Client::model()->count("Client_role_ID={$role->Client_role_ID}");
		if ($count) {
			Yii::app()->getModule('controlpanel')->client->setFlash('fail', 'Cannot delete Role. There is an client assigned to the Role.');
			$this->redirect(array("$this->id/"));
		}
		
		if (Yii::app()->request->isPostRequest) {
			
			// check for security token
			if (CPasswordHelper::verifyPassword($role->Client_role_ID, Yii::app()->request->getPost('security_key'))) {

				// Delete record
				ClientRole::model()->deleteByPk($role->Client_role_ID);

				Yii::app()->getModule('controlpanel')->client->setFlash('success', 'Delete successful!');
				$this->redirect(array("$this->id/"));
			}
			else {
				Yii::app()->getModule('controlpanel')->client->setFlash('fail', 'Error in deletion.');
				$this->redirect(array("$this->id/"));
			}
		}
		$this->render('delete', array('role'=>$role));
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object ClientRole instance
	 * @return void
	 */
	private function _renderForm(ClientRole $object)
	{
		$fields = array();
		
		$fields['Role']['type'] = 'text';
		$fields['Role']['class'] = 'col-md-5';
		
		$fields['Is_active']['type'] = 'radio';
		$fields['Is_active']['class'] = 'col-lg-5';
		$fields['Is_active']['value'] = array('1' => 'Enabled', '0' => 'Disabled');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object' => $object, 'fields' => $fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object ClientRole instance
	 * @return	void
	 */
	private function _postValidate(ClientRole $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('ClientRole');
		$object->attributes = $post;
		$object->Nas_ID = $this->context->nas->id;

		if ($object->validate())
		{
			if ($object->save()) {
				self::displayFormSuccess();
			}
		}
		else {
			self::displayFormError($object);
		}
	}
}