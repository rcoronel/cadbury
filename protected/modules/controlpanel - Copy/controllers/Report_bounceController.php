<?php

class Report_bounceController extends ControlpanelController
{
	public function actionIndex()
	{
		if (Yii::app()->request->isPostRequest) {
			self::_getBounceRate();
		}
		
		// declare scripts
		Yii::app()->clientScript->registerCssFile(Link::css_url(array('jquery.datetimepicker.css')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery/jquery.datetimepicker.js')));

		Yii::app()->clientScript->registerCssFile(Link::css_url(array('morris', 'morris.css')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'raphael', 'raphael-min.js')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'morris', 'morris.min.js')));
		
		$reports = ClientMenu::model()->findAllByAttributes(array('Parent_client_menu_ID'=>20));

		$reps = array();		
		foreach($reports as $r){
			$temp = array('classname'=>$r->Classname, 'title'=>$r->Title, 'url'=>Yii::app()->createUrl('controlpanel/'.$r->Classname));
			$reps[] = $temp;
		}
		
		$this->render('index', array('reps'=>$reps));
	}
	
	private function _getBounceRate()
	{
		$date_from = Yii::app()->request->getPost('date-from');
		$date_to = Yii::app()->request->getPost('date-to');

		$dates = array();
		
		while(date(strtotime($date_from)) < date(strtotime($date_to))){
			$dates[] = date('Y-m-d', strtotime($date_from.' +1 day'));
			$date_from = date('Y-m-d', strtotime($date_from.' +1 day'));
		}

		// Loop through dates
		$data = array();
		foreach ($dates as $d) {
			$data[] = self::_getStats($d);
		}
	
		die(CJSON::encode(array('chart_data' => $data)));
		//$view = Yii::app()->controller->renderPartial('list', array('transactions'=>$data), TRUE);
		
	}
	
	private function _getStats($date)
	{

		try{

			$device_connection = Yii::app()->db_cp->createCommand()->select('COUNT(*) as dc_count')->from('device_connection_'.$date)->where('Nas_ID = '.$this->context->nas->id)->queryAll();
			$authenticated = Yii::app()->db_cp->createCommand()->select('COUNT(*) as auth_count')->from('device_connection_'.$date)->where("username <> '' AND Nas_ID = ".$this->context->nas->id)->queryAll();
		
		}catch(CDbException $e){
			return array('date'=>$date, 'bounce_rate'=>0);
		}

		$bounce_rate = 0;
		// $session_time = Yii::app()->db_radius->createCommand("SELECT SUM(`acctsessiontime`) as `sum` from radacct WHERE nasipaddress = '".$ip."' AND DATE(acctstarttime) = DATE('".$date."')")->queryScalar();
		// RadiusAcct::model()->findBySql('SELECT SUM(`acctsessiontime`) as `sum` from radacct WHERE DATE(acctstarttime) = '.$this->context->nas->id, array());

		if ((int)$device_connection[0]['dc_count'] > 0) {
			$bounce_rate = ((int)$device_connection[0]['dc_count'] - (int)$authenticated[0]['auth_count']) / (int)$device_connection[0]['dc_count'];
			return array('date'=>$date, 'bounce_rate'=>$bounce_rate * 100);
		}
		return array('date'=>$date, 'bounce_rate'=>0);
	}

	public function actionDownloadCSV(){
		$date_from = Yii::app()->request->getPost('stat-from');
		$from = Yii::app()->request->getPost('stat-from');
		$date_to = Yii::app()->request->getPost('stat-to');

		$dates = array();
		
		while(date(strtotime($date_from)) < date(strtotime($date_to))){
			$dates[] = date('Y-m-d', strtotime($date_from.' +1 day'));
			$date_from = date('Y-m-d', strtotime($date_from.' +1 day'));
		}

		// Loop through dates
		$data = array();
		$headers = array("Date", "Bounce Rate");
		array_push($data, $headers);
		foreach ($dates as $d) {
			$data[] = self::_getStats($d);
		}

		// die();
		// array_push($data, array("Monthly Total", $total, $logged_in));
		header("Content-Type: text/csv");
		header("Content-Disposition: attachment; filename=BounceReport-".$from."-".$date_to.".csv");
		// Disable caching
		header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
		header("Pragma: no-cache"); // HTTP 1.0
		header("Expires: 0"); // Proxies

		$output = fopen("php://output", "w");
	    foreach ($data as $row) {
	        fputcsv($output, $row); // here you can change delimiter/enclosure
	    }
	    fclose($output);
	}

}