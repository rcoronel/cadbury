<?php

class SiteAdminIdentity extends CUserIdentity
{
	private $site_admin_id;
	
	/**
	 * Authenticates a user.
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		// Check for existing account
		$admin = SiteAdmin::model()->findByAttributes(array('Email' => $this->username));
		if ($admin === NULL) {
			$this->errorCode = self::ERROR_USERNAME_INVALID;
			$this->errorMessage = 'No record found';
			return FALSE;
		}
		
		// check for password
		if ( ! CPasswordHelper::verifyPassword($this->password, $admin->Password)) {
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
			$this->errorMessage = 'Incorrect password';
			return FALSE;
		}
                
                // check if inactive
		if ( ! $admin->Is_active) {
			$this->errorCode = self::ERROR_USER_INACTIVE;
			$this->errorMessage = 'Inactive User';
			return FALSE;
		}
		
		$this->site_admin_id = $admin->Site_admin_ID;
		$this->username = $admin->Email;
		$this->errorCode = self::ERROR_NONE;
		return TRUE;
	}
	
	/*
	* Override getId() method
	*/
	public function getId()
	{
		return $this->site_admin_id;
	}	
}
