<?php

class ClientIdentity extends CUserIdentity
{
	private $client_id;
	
	/**
	 * Authenticates a user.
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		// Check for existing account
		$client = Client::model()->findByAttributes(array('Email' => $this->username));
		if ($client === NULL) {
			$this->errorCode = self::ERROR_USERNAME_INVALID;
			$this->errorMessage = 'No record found';
			return FALSE;
		}
		
		// check for password
		if ( ! CPasswordHelper::verifyPassword($this->password, $client->Password)) {
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
			$this->errorMessage = 'Incorrect password';
			return FALSE;
		}
                
                // check if inactive
		if ( ! $client->Is_active) {
			$this->errorCode = self::ERROR_USER_INACTIVE;
			$this->errorMessage = 'Inactive User';
			return FALSE;
		}
		
		$this->client_id = $client->Client_ID;
		$this->username = $client->Email;
		$this->errorCode = self::ERROR_NONE;
		return TRUE;
	}
	
	/*
	* Override getId() method
	*/
	public function getId()
	{
		return $this->client_id;
	}	
}
