<div class="base-content">
	<h2 class="captive-portal-heading">Mobile Registration</h2>
	<p>Type in the 5-digit verification code sent to your mobile phone below:</p>

	 <h1><?php echo $mobile->msisdn; ?></h1>
	 <?php $form=$this->beginWidget('CActiveForm', array('method'=>'post', 'id'=>'changenum-form')); ?>
		<?php echo CHtml::hiddenField('availment_id', $this->self->availment_id);?>
		<?php echo CHtml::hiddenField('mobile_number', $mobile->msisdn);?>
		<?php echo CHtml::hiddenField('change_number', 1);?>
		<a href="#" id="change_num" class="link-text-colored">Not you? Change phone number.</a>
	<?php $this->endWidget();?>
	 
	<?php $form=$this->beginWidget('CActiveForm', array('method'=>'post', 'action'=>'', 'id'=>'resend-form', 'htmlOptions'=>array('class'=>'form-block-wrapper'))); ?>
		<?php echo CHtml::hiddenField('availment_id', $this->self->availment_id);?>
		<?php echo CHtml::hiddenField('mobile_number', $mobile->msisdn);?>
		<?php echo CHtml::hiddenField('resend', 1);?>
		<?php echo CHtml::htmlButton('RESEND CODE', array('type'=>'submit', 'class'=>'resend-link'));?>
	<?php $this->endWidget();?>
	
	<?php $form=$this->beginWidget('CActiveForm', array('method'=>'post', 'action'=>'', 'id'=>'verify-form', 'htmlOptions'=>array('class'=>'form-block-wrapper'))); ?>
		<div id="form-error" class="error-notification animated bounceIn" role="alert" style="display:none;"></div>
		
		<?php echo $form->textField($mobile, 'auth_code', array('class'=>'form-control number-only', 'value'=>'', 'placeholder'=>'type in the 5-digit verification code'));?>
		<p>(eg. 12345)</p>
		<?php echo CHtml::htmlButton('Submit', array('type'=>'submit', 'class'=>'button primary-btn'));?>
		<?php echo CHtml::htmlButton('BACK', array('type'=>'button', 'onclick'=>"window.location='{$this->owner->createAbsoluteUrl('login/', array('connection_token'=>$this->owner->connection->connection_token))}'"));?>
	<?php $this->endWidget();?>

 </div>

<div class="modal fade modal-wifi" id="verify-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content centered-content">
			<div class="modal-header">
				<div class="megaworld-logo-small">
					<?php if ($this->owner->inside_logo):?>
						<?php echo CHtml::image($this->owner->inside_logo, $this->owner->site_title);?>
					<?php endif;?>
				</div>
			</div>

			<div class="modal-content-wrapper">
				<div class="modal-body">
					<h3 class="modal-title" id="myModalLabel">Registration Successful</h3>
					<p></p>
				</div>

				<?php echo CHtml::htmlButton('Close', array('class'=>'button primary-btn'));?>
				<div class="modal-footer">
					<footer>
						<?php if (PortalConfiguration::getvalue('POWERED_INSIDE')):?>
							<?php echo CHtml::image(Link::image_url('powered-by-globe-modal.png'), 'Globe');?>
						<?php endif;?>
					</footer>
				</div>
			</div>

		</div>
	</div>
</div>

<div class="modal fade" id="loader-modal" data-backdrop="static" data-keyboard="false" tabindex="-1">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$('form#resend-form').on('submit', function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		// hide errors
		$('div#form-error').slideUp( 300 ).html();

		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");

		$.ajax( {
			url : formURL,
			type: "POST",
			data : postData,
			dataType: 'json',
			success:function(data, textStatus, jqXHR)
			{
				if ( ! data.status) {
					setTimeout(function() {
						$('#loader-modal').modal('hide');
						$('div#form-error').slideDown(300).delay(800).html(data.message);
						$('div#form-error').show();
					}, 900);
				}
				else {
					$('div#verify-modal .modal-body').html(data.message);
					$('#verify-modal').modal({backdrop: 'static', keyboard: false});
					setTimeout(function(){
						window.location.href = data.redirect;
					}, 3000);
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				//if fails     
			}
		});
		e.preventDefault();
	});
	
	$('form#verify-form').on('submit', function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		// hide errors
		$('div#form-error').slideUp( 300 ).html();

		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");

		$.ajax( {
			url : formURL,
			type: "POST",
			data : postData,
			dataType: 'json',
			success:function(data, textStatus, jqXHR)
			{
				if ( ! data.status) {
					setTimeout(function() {
						$('#loader-modal').modal('hide');
						$('div#form-error').slideDown(300).delay(800).html(data.message);
						$('div#form-error').show();
					}, 900);
				}
				else {
					$('#loader-modal').modal('hide');
					$('#verify-modal div.modal-body p').html(data.message);
					$('#verify-modal').modal({backdrop: 'static', keyboard: false});
					$('#verify-modal').on('click', function () {
						window.location.href = data.redirect;
					});
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				setTimeout(function() {
					$('#loader-modal').modal('hide');
					$('div#form-error').slideDown(300).delay(800).html(jqXHR.responseText);
					$('div#form-error').show();
				}, 900);
			}
		});
		e.preventDefault();
	});

	$('#change_num').on('click', function(){
		$('form#changenum-form').submit();
	});

	$('form#changenum-form').on('submit', function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		// hide errors
		$('div#form-error').slideUp( 300 ).html();

		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");

		$.ajax( {
			url : formURL,
			type: "POST",
			data : postData,
			dataType: 'json',
			success:function(data, textStatus, jqXHR)
			{
				if ( ! data.status) {
					setTimeout(function() {
						$('#loader-modal').modal('hide');
						$('div#form-error').slideDown(300).delay(800).html(data.message);
						$('div#form-error').show();
					}, 900);
				}
				else {
					// $('div#verify-modal .modal-body').html(data.message);
					// $('#verify-modal').modal({backdrop: 'static', keyboard: false});
					setTimeout(function(){
						window.location.href = data.redirect;
					}, 3000);
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				//if fails     
			}
		});
		e.preventDefault();
	});
</script>