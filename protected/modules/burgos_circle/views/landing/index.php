<div id="megaworld-captive-portal" class="centered-content">
	<?php echo PortalConfiguration::getValue('LANDING_MESSAGE');?>
	<div class="logo-wrapper">
		<?php if ($this->logo):?>
			<?php echo CHtml::image($this->logo, $this->site_title);?>
		<?php endif;?>
	</div>
	<?php echo CHtml::htmlButton('Click here to avail free wifi', array('class'=>'cp-btn', 'onclick'=>"window.location='{$url}'"));?>
</div>

<div class="modal fade" id="loader-modal" data-backdrop="static" data-keyboard="false" tabindex="-1">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('button').on('click',function(e){
			$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		});
	});
</script>