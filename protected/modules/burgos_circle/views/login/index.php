

<div class="base-content">
	<h2>Terms and Service Agreement</h2>
	<h4>By logging in to Megaworld Lifestyle Malls wireless internet service (the "Service"), the customer/you have read and agreed to the <a href="#" data-toggle="modal" data-target="#reg-modal">Terms and Condition</a> regarding the use of the service.</h4>

	<div class="form-block-wrapper">
		<?php if ( ! empty($availments)):?>
			<?php if ( ! empty($availment)):?>
				<h3>Your FREE <?php echo $availment->duration / 60;?> minutes has expired</h3>
			<?php endif;?>

			<?php foreach ($availments as $a):?>
				<h5><?php echo $a->availment->description; ?></h5>
				<?php $this->widget("{$this->portal->code}.widgets.{$a->availment->code}", array('self' => $a));?>
			<?php endforeach;?>
		<?php else:?>
			<h2>Your FREE <?php echo $availment->duration / 60;?> minutes has expired for today</h2>
			<h4>Thank you for using Globe's <strong>FREE</strong>WiFi service</h4>
			<h4>See you again soon!</h4>
		<?php endif;?>
	</div>
</div>

<div class="modal modal-wifi" id="terms-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="myModalLabel">Terms of Agreement</h3>
			</div>
			<div class="modal-body text-overflow">
				<p>Terms of Service ("Terms")</p>
				<?php if ( ! empty($terms)):?>
					<?php echo $terms->content;?>
				<?php endif;?>
			</div>
			<div class="modal-footer">
			   <?php echo CHtml::htmlButton('Close', array('class'=>'btn primary-btn', 'data-dismiss'=>'modal'));?>
			   <div class="modal-btn-logo">
				   <?php echo CHtml::image(Link::image_url('powered-by-globe-modal.png'), 'Globe');?>
			   </div>
			</div>
		</div>
	</div>
</div>

<div class="modal modal-wifi in" id="loader-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-body small-modal-body-style">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade modal-wifi in" id="reg-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h3 class="modal-title" id="myModalLabel">Terms and Service Agreement</h3>
			</div>

		  <div class="modal-content-wrapper">
				<div id="modal-scroll-content" class="modal-body">
					<?php if ( ! empty($content)):?>
						<?php echo $content->content;?>
					<?php endif;?>
				</div>

				<div class="modal-footer">
				   <footer>
					  <?php echo CHtml::image(Link::image_url('powered-by-globe-modal.png'), 'Globe');?>
				   </footer>
			   </div>
		  </div>

		</div>
	</div>
</div>

<script>
$(document).ready(function(){

	$('form').on('submit',function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		//e.preventDefault();
	});
});
</script>
