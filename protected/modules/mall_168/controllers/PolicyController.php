<?php

class PolicyController extends CaptivePortal
{
	public function actionIndex()
	{
		$policy = Cms::model()->findByAttributes(array('portal_id'=>$this->portal->portal_id, 'friendly_url'=>'service-agreement'));
		$url = $this->createAbsoluteUrl('landing/', array('connection_token'=>$this->connection->connection_token));
		$this->render('index', array('url'=>$url, 'policy'=>$policy));
	}
}