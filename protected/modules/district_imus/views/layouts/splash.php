<?php /* @var $this Controller */ ?>
<?php $this->beginContent('/layouts/main'); ?>
	<div class="container">
		
		<?php echo $content;?>
		
	</div>

	<footer class="container">
        <div class="row">
            <div class="col-sm-12 col-md-3 col-md-offset-9">
                 <?php if (PortalConfiguration::getvalue('POWERED_WELCOME')):?>
					<?php echo CHtml::image(Link::image_url('powered-by-globe-white-logo-Small.png'), 'Globe');?>
				<?php endif;?>
            </div>
        </div>
    </footer>

<?php $this->endContent(); ?>