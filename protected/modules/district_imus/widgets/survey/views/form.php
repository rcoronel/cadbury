<section class="container">
	<div class="row">
		<div class="col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
			<div id="survey-wrapper" class="default-wrapper">
				<h1><?php echo $survey->title;?></h1>

				<div class="notification">
					<p class="animated fadeIn">Get your 30 minutes Free WiFi access by answering our short survey.</p>
				</div>
				<?php $form=$this->beginWidget('CActiveForm', array('id'=>'survey-form', 'method'=>'post', 'htmlOptions'=>array('class'=>'col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2'))); ?>
					<?php echo CHtml::hiddenField('survey_id', $survey->survey_id);?>
					<?php echo CHtml::hiddenField('position', 0);?>
					<div id="question_container"></div>
				<?php $this->endWidget();?>
				

				<footer class="row powered-container">
					<?php if (PortalConfiguration::getvalue('POWERED_INSIDE')):?>
						<?php echo CHtml::image(Link::image_url('powered-by-globe-modal.png'), 'Globe');?>
					<?php endif;?>
				</footer>
			</div> <!-- end of default-wrapper -->
		</div>
	</div>
</section>

<div class="modal modal-wifi in" id="verify-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content globe-modal-content">
			<div class="modal-header">
				<h4 class="modal-title sr-only" id="myModalLabel"></h4>
			</div>
			<div class="modal-body">
				<p></p>
			</div>
			<div class="modal-footer">
				<?php echo CHtml::htmlButton('Close', array('class'=>'btn secondary-btn'));?>
				<div class="modal-btn-logo">
					<?php echo CHtml::image(Link::image_url('powered-by-globe-modal.png'), 'Globe');?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal modal-wifi in" id="loader-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-body small-modal-body-style">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script lang="text/javascript">
	$(document).ready(function() {
		show_questions();
	});
	function show_questions()
	{
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		$.ajax({
			type: "POST",
			url: currentURL,
			data: $('form#survey-form').serialize(),
			dataType: "json",
			success: function(data) {
				if (data.status == 1) {
					setTimeout(function() {
						$('#loader-modal').modal('hide');
						$('div#question_container').html(data.message);
						$('input[name="position"]').val(data.position);
					}, 900);
				}
				
				if (data.status == 2) {
					window.location.href = data.redirect;
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				setTimeout(function() {
					$('#loader-modal').modal('hide');
					$('div#form-error').slideDown(300).delay(800).html(jqXHR.responseText);
					$('div#form-error').show();
				}, 900);
			}
		});
	}
    $('form').on('submit', function(e){
		//$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		// hide errors
		$('div#form-error').slideUp( 300 ).html();

		show_questions();
		e.preventDefault();
	});
</script>