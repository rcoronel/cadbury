<section class="container">
    <div class="row">
        <div id="cfs-cp" class="col-sm-12 col-md-12 col-lg-5 col-center-block">
                <div class="cfs-default-wrapper">
                    <div class="cfs-default-content">
                        <div class="col-xs-12 col-sm-8 col-md-5 col-lg-11 col-center-block">
                        <h1 class="heading-underline">Code<br>Verification</h1>
                        <div class="form-wrapper">
                                <h4>A <span class="highlight">5 digit verification code</span> has been sent to your mobile number.</h4>

                                <?php $form=$this->beginWidget('CActiveForm', array('method'=>'post', 'id'=>'changenum-form')); ?>
                                        <input type="number" placeholder="<?php echo $mobile->msisdn; ?>" disabled="disabled" id="user-number">
                                        <a href="#" id="change_num">Not you? Change Phone number</a>
                                        <?php echo CHtml::hiddenField('availment_id', $this->self->availment_id);?>
                                        <?php echo CHtml::hiddenField('mobile_number', $mobile->msisdn);?>
                                        <?php echo CHtml::hiddenField('change_number', 1);?>
                                <?php $this->endWidget();?>

                                <?php $form=$this->beginWidget('CActiveForm', array('method'=>'post', 'id'=>'resend-form')); ?>
                                        <div id="form-error2" class="error-message animated bounceIn" role="alert" style="display:none;"></div>
                                        <?php echo CHtml::hiddenField('availment_id', $this->self->availment_id);?>
                                        <?php echo CHtml::hiddenField('mobile_number', $mobile->msisdn);?>
                                        <?php echo CHtml::hiddenField('resend', 1);?>
                                         <?php echo CHtml::submitButton('Resend Code', array('type'=>'submit', 'class'=>'btn yellow-btn'));?>
                                <?php $this->endWidget();?>	 
                                <?php $form=$this->beginWidget('CActiveForm', array('method'=>'post', 'id'=>'verify-form')); ?>
                                         <p>Enter your code here:</p>
                                         <?php echo $form->numberField($mobile, 'auth_code', array('value'=>'', 'placeholder'=>'eg.12345'));?>
                                         <div id="form-error" class="error-message animated bounceIn" role="alert" style="display:none;"></div>
                                         
                                         <?php echo CHtml::htmlButton('verify', array('type'=>'submit', 'class'=>'btn yellow-btn','value'=>'Verify'));?>
                                         <?php echo CHtml::htmlButton('back', array('value'=>'Back','type'=>'button', 'class'=>'btn yellow-btn', 'onclick'=>"window.location='{$this->owner->createAbsoluteUrl('landing/', array('connection_token'=>$this->owner->connection->connection_token))}'"));?>
                                <?php $this->endWidget();?>
                         </div>
                    </div>
                </div>
                <div id="cfs-footer-content">
                        <?php echo CHtml::image(Link::image_url('powered-by-globe-white-logo-Small.png'), 'Globe Logo');?>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal modal-wifi in" id="verify-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content globe-modal-content">
			<div class="modal-header">
				<h4 class="modal-title sr-only" id="myModalLabel"></h4>
			</div>
			<div class="modal-body">
				<p></p>
			</div>
			<div class="modal-footer">
				<?php echo CHtml::htmlButton('Close', array('class'=>'btn yellow-btn'));?>
				<div class="modal-btn-logo">
					<?php echo CHtml::image(Link::image_url('powered-by-globe-modal.png'), 'Globe');?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal modal-wifi in" id="loader-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-body small-modal-body-style">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$('form#resend-form').on('submit', function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		// hide errors
		$('div#form-error').slideUp( 300 ).html();
		$('div#form-error2').slideUp( 300 ).html();

		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");

		$.ajax( {
			url : formURL,
			type: "POST",
			data : postData,
			dataType: 'json',
			success:function(data, textStatus, jqXHR)
			{
				if ( ! data.status) {
					setTimeout(function() {
						$('#loader-modal').modal('hide');
						$('div#form-error2').slideDown(300).delay(800).html(data.message);
						$('div#form-error2').show();
					}, 900);
				}
				else {
					$('#loader-modal').modal('hide');
					$('div#verify-modal .modal-body').html(data.message);
					$('#verify-modal').modal({backdrop: 'static', keyboard: false});
					$('#verify-modal').on('click', function () {
						window.location.href = data.redirect;
					});
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				//if fails     
			}
		});
		e.preventDefault();
	});
	
	$('form#verify-form').on('submit', function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		// hide errors
		$('div#form-error').slideUp( 300 ).html();
		$('div#form-error2').slideUp( 300 ).html();

		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");

		$.ajax( {
			url : formURL,
			type: "POST",
			data : postData,
			dataType: 'json',
			success:function(data, textStatus, jqXHR)
			{
				if ( ! data.status) {
					setTimeout(function() {
						$('#loader-modal').modal('hide');
						$('div#form-error').slideDown(300).delay(800).html(data.message);
						$('div#form-error').show();
					}, 900);
				}
				else {
					window.location.href = data.redirect;
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				setTimeout(function() {
					$('#loader-modal').modal('hide');
					$('div#form-error').slideDown(300).delay(800).html(jqXHR.responseText);
					$('div#form-error').show();
				}, 900);
			}
		});
		e.preventDefault();
	});

	$('#change_num').on('click', function(){
		$('form#changenum-form').submit();
	});

	$('form#changenum-form').on('submit', function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		// hide errors
		$('div#form-error').slideUp( 300 ).html();
		$('div#form-error2').slideUp( 300 ).html();

		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");

		$.ajax( {
			url : formURL,
			type: "POST",
			data : postData,
			dataType: 'json',
			success:function(data, textStatus, jqXHR)
			{
				if ( ! data.status) {
					setTimeout(function() {
						$('#loader-modal').modal('hide');
						$('div#form-error2').slideDown(300).delay(800).html(data.message);
						$('div#form-error2').show();
					}, 900);
				}
				else {
					// $('div#verify-modal .modal-body').html(data.message);
					// $('#verify-modal').modal({backdrop: 'static', keyboard: false});
					setTimeout(function(){
						window.location.href = data.redirect;
					}, 3000);
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				//if fails     
			}
		});
		e.preventDefault();
	});
</script>