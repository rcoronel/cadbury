<section class="container">
    <div class="row">
            <div id="cfs-cp" class="col-sm-12 col-md-12 col-lg-5 col-center-block">
                <div class="cfs-default-wrapper">
                    <div class="cfs-default-content">
                        <div class="col-xs-12 col-sm-8 col-md-5 col-lg-11 col-center-block">
                                             <h1 class="heading-underline">Welcome!</h1>
                                             <div class="form-wrapper">
                                                     <h4>If you wish to have <span class="highlight">Free WIfi</span>, please login with your <span class="highlight">Gender &amp; Mobile Number.</span></h4>
                                                     
                                                     <?php $form=$this->beginWidget('CActiveForm', array('method'=>'post', 'action'=>'')); ?>
                                                            <label>
                                                                    <?php echo $form->radioButton($object2, 'gender', array('value'=>1,'uncheckValue'=>null));?> Male
                                                            </label>
                                                            <label>
                                                                    <?php echo $form->radioButton($object2, 'gender', array('value'=>2,'uncheckValue'=>null));?> Female
                                                            </label>
                                                             <?php echo $form->numberField($object, 'msisdn', array('placeholder'=>'mobile number'));?>
                                                             <div id="form-error" class="error-message" style="display:none;">
                                                                <h6><img src="error-icon.png"> invalid mobile number</h6>
                                                            </div>
                                                             <?php echo CHtml::htmlButton('Submit', array('type'=>'submit', 'class'=>'btn yellow-btn'));?>
                                                     <?php $this->endWidget();?>
                                             </div>
                                    </div>

                            </div>
                            <div id="cfs-footer-content">
                                    <?php echo CHtml::image(Link::image_url('powered-by-globe-white-logo-Small.png'), 'Globe Logo');?>
                            </div>
                    </div>
            </div>
    </div>
</section>
<div class="modal modal-wifi in" id="loader-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-body small-modal-body-style">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$('form').on('submit', function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		// hide errors
		$('div#form-error').slideUp( 300 ).html();

		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");

		$.ajax( {
			url : formURL,
			type: "POST",
			data : postData,
			dataType: 'json',
			success:function(data, textStatus, jqXHR)
			{
				if ( ! data.status) {
					setTimeout(function() {
						$('#loader-modal').modal('hide');
						$('div#form-error').slideDown(300).delay(800).html(data.message);
						$('div#form-error').show();
					}, 900);
				}
				else {
					$('div.modal-body p').html(data.message);
					$('#loader-modal').modal({backdrop: 'static', keyboard: false});
					setTimeout(function(){
						window.location.href = data.redirect;
					}, 3000);
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				//if fails     
			}
		});
		e.preventDefault();
	});
</script>