<section class="container">
	<div class="row">
		<div id="cfs-cp" class="col-sm-12 col-md-12 col-lg-5 col-center-block">
			<div class="cfs-body-wrapper">
				<div id="cfs-welcome-content">
                                    <div class="col-xs-12 col-sm-8 col-md-12 col-center-block">
                                    <?php if ($this->user):?>
                                        <?php echo str_replace('::user::', $this->user, PortalConfiguration::getValue('WELCOME_BACK'));?>
                                    <?php else:?>
                                        <?php echo PortalConfiguration::getValue('LANDING_MESSAGE');?>
                                    <?php endif; ?>    
                                    </div>
				</div>
				<div id="cfs-footer-content">
					<?php echo CHtml::link('Get Connected', $url ,array('class'=>'btn yellow-btn', 'id'=>'get-connected')); ?>
					<div>
                                            <?php if (PortalConfiguration::getvalue('POWERED_WELCOME')):?>
                                                    <?php echo CHtml::image(Link::image_url('powered-by-globe-white-logo-Small.png'), 'Globe Logo');?>
                                            <?php endif;?>
					</div>  
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal modal-wifi in" id="loader-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-body small-modal-body-style">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('a#get-connected').on('click',function(e){	
			$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		});
	});
</script>