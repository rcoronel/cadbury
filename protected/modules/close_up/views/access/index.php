<section class="container">
	<div class="row">
		<div id="cfs-cp" class="col-sm-12 col-md-12 col-lg-5 col-center-block">
			<div class="cfs-default-wrapper">
				<div class="cfs-default-content">
					<div class="col-xs-12 col-sm-8 col-md-5 col-lg-11 col-center-block">
						<h5>You can now enjoy <span>free wifi access</span> from your <span>close up forever summer </span></h5>
						<h5 class="strong">Happy Surfing!</h5>
						<button class="btn yellow-btn" onclick="location.href='https://www.facebook.com/closeupphilippines/'">Close</button>
					</div>
					 
				</div>
				<div id="cfs-footer-content">
					<?php echo CHtml::image(Link::image_url('powered-by-globe-white-logo-Small.png'), 'Globe Logo');?>
				</div>
			</div>
		</div>
	</div>
</section>
