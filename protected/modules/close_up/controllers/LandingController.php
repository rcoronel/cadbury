<?php

class LandingController extends CaptivePortal
{
	/**
	 * Declare variables
	 * 
	 * @access protected
	 * @param object $action CAction instance
	 * @return bool
	 */
	protected function beforeAction($action)
	{
		parent::beforeAction($action);
		
		return TRUE;
	}
	
	/**
	 * Show landing page
	 * 
	 * @access public
	 * @return void
	 */
	public function actionIndex()
	{
		// Use custom layout
		$this->layout = '/layouts/splash';
                
                self::_checkScene();
                
                $mobile_availment = Availment::model()->findByAttributes(array('model'=>'AvailMobile'));
		
		$url = $this->createAbsoluteUrl('login/', array('connection_token'=>$this->connection->connection_token,'availment_id'=> $mobile_availment->availment_id,'action'=>'login'));
		$terms = Cms::model()->findByAttributes(array('portal_id'=>$this->portal->portal_id, 'friendly_url'=>'terms-and-conditions'));
		$policy = Cms::model()->findByAttributes(array('portal_id'=>$this->portal->portal_id, 'friendly_url'=>'service-agreement'));
		
		$this->render('index', array('url'=>$url, 'terms'=>$terms, 'policy'=>$policy));
	}
        
        private function _checkScene()
	{
		// Check for current level
		$latest_availment = ConnectionAvail::model()->findByAttributes(array('device_connection_id'=>$this->connection->device_connection_id), array('order'=>'Created_at DESC'));
		if ($latest_availment) {
			$this->level = $latest_availment->level+1;
		}
		else {
                $this->level = 1;
		}
		
		$count = 0;
		$availments = Availment::model()->findAll();
		
		foreach ($availments as $a) {
			$count = DeviceAvailment::model()->exists("availment_id = {$a->availment_id}"
			. " AND device_id = {$this->device->device_id}"
			. " AND portal_id = {$this->portal->portal_id}");
//			. " AND DATE(created_at) < DATE(NOW())");
			if ($count) {
				break;
			}
		}
		
		// New or returning?
		if ( ! $count) {
			$this->scene = 'new';
			$this->scene_model = 'ScenarioAvailment';
		}
		else {
			$this->scene = 'returning';
			$this->scene_model = 'ScenarioReturning';
//			if ( ! $this->user) {
//				$fb = AvailFacebook::model()->with('dev_avail')->find("device_id = {$this->device->device_id}");
//				if ($fb) {
//					$this->user = $fb->Name;
//				}
//			}
			
			if ( ! $this->user) {
				$mobtel = AvailMobile::model()->with('dev_avail')->find("device_id = {$this->device->device_id} AND portal_id = {$this->portal->portal_id}");
				if ($mobtel) {
                                    $number = substr_replace($mobtel->msisdn, ' ', 4, 0);
                                    $number = substr_replace($number, ' ', 8, 0);
                                    $this->user = $number;
				}
			}
		}
	}
}