
<div class="row">
	<div class="col-sm-6 col-sm-offset-3 text-field">
		<div class="row">
			<div class="col-xs-12">
				<h3>Mobile Registration</h3>
			</div>
			<div class="col-xs-12 title-qa">
				<span>
					Enter your 11-digit mobile number to start enjoying Globe's FREE WiFi trial.
				</span>
			</div>
			<div id="form-error" class="alert alert-danger col-sm-4 col-sm-offset-4" role="alert" style="display:none;"></div>
		</div>
		<?php $form=$this->beginWidget('CActiveForm', array('method'=>'post', 'action'=>'')); ?>
			<div class="form-group">
				<label for="msisdn">11-Digit Mobile Number</label>
				<?php echo $form->textField($object, 'msisdn', array('class'=>'form-control number-only', 'placeholder'=>'09XXXXXXXXX'));?>
				<?php echo CHtml::hiddenField('availment_id', $availment_id);?>
			</div>
		
			<div class="conditions-content-btn">
				<?php echo CHtml::htmlButton('SEND VERIFICATION CODE', array('type'=>'submit', 'class'=>'s-btn serendra-primary-btn'));?>
				<?php echo CHtml::htmlButton('BACK', array('type'=>'button', 'class'=>'s-btn serendra-secondary-btn', 'onclick'=>"window.location='{$this->owner->createAbsoluteUrl('login/', array('connection_token'=>$this->owner->connection->connection_token))}'"));?>
			</div>
		<?php $this->endWidget();?>
	</div> 
</div>


<div class="modal fade" id="loader-modal" data-backdrop="static" data-keyboard="false" tabindex="-1">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$('form').on('submit', function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		// hide errors
		$('div#form-error').slideUp( 300 ).html();

		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");

		$.ajax( {
			url : formURL,
			type: "POST",
			data : postData,
			dataType: 'json',
			success:function(data, textStatus, jqXHR)
			{
				if ( ! data.status) {
					setTimeout(function() {
						$('#loader-modal').modal('hide');
						$('div#form-error').slideDown(300).delay(800).html(data.message);
						$('div#form-error').show();
					}, 900);
				}
				else {
					$('div.modal-body p').html(data.message);
					$('#loader-modal').modal({backdrop: 'static', keyboard: false});
					setTimeout(function(){
						window.location.href = data.redirect;
					}, 3000);
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				//if fails     
			}
		});
		e.preventDefault();
	});
</script>