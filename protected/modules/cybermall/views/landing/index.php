<div id="megaworld-captive-portal" class="centered-content">
	<?php echo PortalConfiguration::getValue('LANDING_MESSAGE');?>
	<div class="logo-wrapper">
		<?php if ($this->logo):?>
			<?php echo CHtml::image($this->logo, $this->site_title);?>
		<?php endif;?>
	</div>
	
	<div id="captive-portal-button-wrapper">
		<?php echo CHtml::htmlButton('Get Connected', array('class'=>'cp-btn', 'onclick'=>"window.location='{$url}'"));?>
		<h4>By clicking the button, you agree to the <a href="#" data-toggle="modal" data-target="#terms-modal" class="link-text">Terms of Service</a> and <a href="#" data-toggle="modal" data-target="#policy-modal" class="link-text">Privacy Policy.</a></h4>
	</div>
</div>

<div class="modal fade" id="loader-modal" data-backdrop="static" data-keyboard="false" tabindex="-1">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade modal-wifi in" id="terms-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h3 class="modal-title" id="myModalLabel">Terms of Service Agreement</h3>
			</div>

		  <div class="modal-content-wrapper">
				<div id="modal-scroll-content" class="modal-body">
					<?php if ( ! empty($terms->content)):?>
						<?php echo $terms->content;?>
					<?php endif;?>
				</div>
		  </div>

		</div>
	</div>
</div>

<div class="modal fade modal-wifi in" id="policy-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h3 class="modal-title" id="myModalLabel">Privacy Policy</h3>
			</div>

		  <div class="modal-content-wrapper">
				<div id="modal-scroll-content" class="modal-body">
					<?php if ( ! empty($policy->content)):?>
						<?php echo $policy->content;?>
					<?php endif;?>
				</div>
		  </div>

		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		$('button.cp-btn').on('click',function(e){
			$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		});
	});
</script>