<?php /* @var $this Controller */ ?>
<?php $this->beginContent('/layouts/main'); ?>
<div id="captive-portal-wrapper" class="row">
	<div class="container">
		<div class="base-wrapper centered-content">
			<?php if ($this->inside_logo && $this->id != 'access'):?>
				<div class="megaworld-logo-small">
					<?php echo CHtml::image($this->inside_logo, $this->site_title);?>
				</div>
			<?php endif;?>

			<?php echo $content;?>

			<?php if (PortalConfiguration::getvalue('POWERED_INSIDE') && $this->id != 'access'):?>
				<footer>
					<?php echo CHtml::image(Link::image_url('powered-by-globe-modal.png'), 'Globe');?>
				</footer>
			<?php endif;?>
		</div>
	</div>
</div>
<?php $this->endContent(); ?>