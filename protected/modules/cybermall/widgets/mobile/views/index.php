
<div class="base-content">
	<h2 class="captive-portal-heading">Mobile Registration</h2>
	<p>Enter your 11-digit mobile number to start enjoying Globe's FREE WiFi trial.</p>

	<?php $form=$this->beginWidget('CActiveForm', array('method'=>'post', 'action'=>'', 'htmlOptions'=>array('class'=>'form-block-wrapper'))); ?>
		<div id="form-error" class="error-notification animated bounceIn" role="alert" style="display:none;"></div>
		
		<?php echo $form->textField($object, 'msisdn', array('class'=>'form-control number-only', 'placeholder'=>'type in your 11 digit mobile number'));?>
		<p>(eg. 09XX-XXX-XXXX)</p>
		
		<?php echo CHtml::hiddenField('availment_id', $availment_id);?>
		<input type="submit" class="button primary-btn" value="Submit">
	<?php $this->endWidget();?>
</div>

<div class="modal fade" id="loader-modal" data-backdrop="static" data-keyboard="false" tabindex="-1">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$('form').on('submit', function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		// hide errors
		$('div#form-error').slideUp( 300 ).html();

		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");

		$.ajax( {
			url : formURL,
			type: "POST",
			data : postData,
			dataType: 'json',
			success:function(data, textStatus, jqXHR)
			{
				if ( ! data.status) {
					setTimeout(function() {
						$('#loader-modal').modal('hide');
						$('div#form-error').slideDown(300).delay(800).html(data.message);
						$('div#form-error').show();
					}, 900);
				}
				else {
					$('div.modal-body p').html(data.message);
					$('#loader-modal').modal({backdrop: 'static', keyboard: false});
					setTimeout(function(){
						window.location.href = data.redirect;
					}, 3000);
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				//if fails     
			}
		});
		e.preventDefault();
	});
</script>