<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class ControlpanelController extends Controller
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='/layouts/column2';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	//public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
	
	/* permission by profile */
	public $permissions;
	
	// links for pages
	public $menu;
	
	/**
	 *
	 * @var array Navigational menus in each action
	 * Format is action ID => menus
	 */
	public $nav;
	public $fields_row;
	public $rowAction;
	
	/**
	 * Available buttons for each action
	 * 
	 * @var array Navigational menus in each action
	 * Format is action ID => menus
	 */
	public $actionButtons = array();
	
	/**
	 * Constructor
	 *
	 * Called automatically
	 * Inherits functions from the parent class
	 * 
	 * @access	public
	 * @param	string $id Controller ID
	 * @param	string $module Module name
	 */
	public function __construct($id, $module = null)
	{
		parent::__construct($id, $module);
	}
	
	protected function beforeAction($action)
	{
		// Public pages that can be accessed by anyone
		// Must contain the classname of the page
		$public_menus = array('login', 'logout', 'password', 'profile');
		
		$access = 1;
		if ( ! in_array(Yii::app()->controller->id, $public_menus)) {
			switch ($action->id) {
				case 'index':
				case 'view':
					$access = $this->permissions->view;
				break;

				case 'add':
					$access = $this->permissions->add;
				break;

				case 'update':
					$access = $this->permissions->edit;
				break;

				case 'delete':
					$access = $this->permissions->delete;
				break;

				default:
					$access = 1;
				break;
			}

			if ( ! $access) {
				$this->render('/dashboard/deny');
				return FALSE;
			}
		}
		
		return TRUE;
	}
	
	protected function checkNas($nas_id)
	{
		if ($nas_id != $this->context->nas->id) {
			Yii::app()->getModule('controlpanel')->client->setFlash('fail', 'Cannot update the record. Please check your settings');
			$this->redirect(array($this->id.'/'));
		}
	}
}