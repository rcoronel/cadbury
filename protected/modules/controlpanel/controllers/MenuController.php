<?php

class MenuController extends ControlpanelController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action))
		{
			// action buttons
			$this->actionButtons['index'] = array('add' => array('link' => $this->createURL($this->id.'/add')));
			$this->actionButtons['view'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['add'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back' => array('link' => $this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		// declare scripts
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'plugins', 'jquery.tablednd.js')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/jquery.toastr.min.js'));
		
		// fields to be displayed
		$row_fields = array();
		$row_fields['Client_menu_ID']['type'] = 'text';
		$row_fields['Client_menu_ID']['class'] = 'text-center col-md-1';
		
		$row_fields['Title']['type'] = 'text';
		$row_fields['Title']['class'] = 'col-mid-4';
		
		$row_fields['Display']['type'] = 'select';
		$row_fields['Display']['class'] = 'text-center col-md-1';
		$row_fields['Display']['value'] = array('1' => 'Enabled', '0' => 'Disabled');
		$row_fields['Display']['mode'] = 'bool';
		
		$row_fields['Position']['type'] = 'text';
		$row_fields['Position']['class'] = 'text-center col-md-1 dragHandle';
		
		$row_actions = array('view', 'edit', 'delete');
		
		$menus = ClientMenu::model()->findAllByAttributes(array('Parent_client_menu_ID'=>0), array('order'=>'position ASC'));
	
		$this->render('/layouts/list', array('object' => new ClientMenu, 'objectList'=>$menus, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
	}
	
	/**
	 * View records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionView($id)
	{
		// declare scripts
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'plugins', 'jquery.tablednd.js')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/jquery.toastr.min.js'));
		
		// fields to be displayed
		$row_fields = array();
		$row_fields['Client_menu_ID']['type'] = 'text';
		$row_fields['Client_menu_ID']['class'] = 'text-center col-md-1';
		
		$row_fields['Title']['type'] = 'text';
		$row_fields['Title']['class'] = 'col-mid-4';
		
		$row_fields['Display']['type'] = 'select';
		$row_fields['Display']['class'] = 'text-center col-md-1';
		$row_fields['Display']['value'] = array('1' => 'Enabled', '0' => 'Disabled');
		$row_fields['Display']['mode'] = 'bool';
		
		$row_fields['Position']['type'] = 'text';
		$row_fields['Position']['class'] = 'text-center col-md-1 dragHandle';
		
		$row_actions = array('view', 'edit', 'delete');
		
		$menus = ClientMenu::model()->findAllByAttributes(array('Parent_client_menu_ID'=>$id), array('order'=>'position ASC'));
	
		$this->render('/layouts/list', array('object' => new ClientMenu, 'objectList'=>$menus, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		$menu = new ClientMenu;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($menu);
		}
		
		self::_renderForm($menu);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id ClientMenu primary key
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$menu = ClientMenu::model()->findByPk($id);
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($menu);
		}
		
		self::_renderForm($menu);
	}
	
	/**
	 * Delete record
	 * 
	 * @access public
	 * @param int $id ClientMenu ID
	 * @return void
	 */
	public function actionDelete($id)
	{
		$menu = ClientMenu::model()->findByPk($id);
		ClientMenu::validateObject($menu, 'ClientMenu');
		
		if (Yii::app()->request->isPostRequest) {
			// check for security token
			if (CPasswordHelper::verifyPassword($menu->Client_menu_ID, Yii::app()->request->getPost('security_key'))) {
				// Delete permissions
				ClientRolePermission::model()->deleteAllByAttributes(array('Client_menu_ID'=>$menu->Client_menu_ID));
				
				// Delete record
				ClientMenu::model()->deleteByPk($menu->Client_menu_ID);
				Yii::app()->getModule('controlpanel')->client->setFlash('success', 'Delete successful!');
				$this->redirect(array("$this->id/"));
			}
			else {
				Yii::app()->getModule('controlpanel')->client->setFlash('fail', 'Error in deletion.');
				$this->redirect(array("$this->id/"));
			}
		}
		$this->render('delete', array('menu'=>$menu));
	}
	
	/**
	 * Update menu position
	 * 
	 * @access active
	 * @return string
	 */
	public function actionPosition()
	{
		$query = Yii::app()->request->getQuery('page-list');
		
		$menu = new ClientMenu;
		foreach ($query as $position => $id) {
			$menu->updateByPk($id, array('Position' => (int)$position + 1));
		}
		
		die('Repositioning successful. Refresh page to view changes');
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object ClientMenu instance
	 * @return void
	 */
	private function _renderForm(ClientMenu $object)
	{
		$fields = array();
		
		$parent_menus = ClientMenu::model()->findAllByAttributes(array('Parent_client_menu_ID' => 0, 'Display'=>1), array('order'=>'Position ASC'));
		$fields['Parent_client_menu_ID']['type'] = 'select';
		$fields['Parent_client_menu_ID']['class'] = 'col-lg-5';
		$fields['Parent_client_menu_ID']['value'] = array('Home') + CHtml::listData($parent_menus, 'Client_menu_ID', 'Title');
		
		$fields['Title']['type'] = 'text';
		$fields['Title']['class'] = 'col-md-5';
		
		$fields['Classname']['type'] = 'text';
		$fields['Classname']['class'] = 'col-md-5';
		
		$fields['Img']['type'] = 'text';
		$fields['Img']['class'] = 'col-lg-5';
		
		$fields['Display']['type'] = 'radio';
		$fields['Display']['class'] = 'col-lg-5';
		$fields['Display']['value'] = array('1' => 'Enabled', '0' => 'Disabled');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object' => $object, 'fields' => $fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object ClientMenu instance
	 * @return	void
	 */
	private function _postValidate(ClientMenu $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('ClientMenu');
		$object->attributes = $post;

		if ($object->validate())
		{
			if ($object->save()) {
				// add all access to main client
				$permission = new ClientRolePermission;
				$permission->deleteAllByAttributes(array('Client_menu_ID' => $object->{$object->tableSchema->primaryKey}));
				$permission->Client_menu_ID = $object->{$object->tableSchema->primaryKey};
				$permission->Client_role_ID = 1;
				$permission->View = 1;
				$permission->Add = 1;
				$permission->Edit = 1;
				$permission->Delete = 1;
				$permission->save();
				
				self::displayFormSuccess();
			}
		}
		else {
			self::displayFormError($object);
		}
	}
}