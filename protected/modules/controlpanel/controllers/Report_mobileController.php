<?php

class Report_mobileController extends ControlpanelController
{
	public function actionIndex()
	{
		if (Yii::app()->request->isPostRequest) {
			self::_getConnectionStats();
		}
		
		// declare scripts
		Yii::app()->clientScript->registerCssFile(Link::css_url(array('jquery.datetimepicker.css')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery/jquery.datetimepicker.js')));

		Yii::app()->clientScript->registerCssFile(Link::css_url(array('morris', 'morris.css')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'raphael', 'raphael-min.js')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'morris', 'morris.min.js')));
		
		$availment = Availment::model()->findAll();

		$availments = array();

		foreach($availment as $a){
			$availments[$a->Availment_ID] = $a->Name;
		}

		$reports = ClientMenu::model()->findAllByAttributes(array('Parent_client_menu_ID'=>20));

		$reps = array();		
		foreach($reports as $r){
			$temp = array('classname'=>$r->Classname, 'title'=>$r->Title, 'url'=>Yii::app()->createUrl('controlpanel/'.$r->Classname));
			$reps[] = $temp;
		}
		
		$this->render('index', array('availment'=>$availments, 'reps'=>$reps));
	}
	
	private function _getConnectionStats()
	{
		$date_from = Yii::app()->request->getPost('date-from');
		$date_to = Yii::app()->request->getPost('date-to');

		$dates = array();
		
		while(date(strtotime($date_from)) < date(strtotime($date_to))){
			$dates[] = date('Y-m-d', strtotime($date_from.' +1 day'));
			$date_from = date('Y-m-d', strtotime($date_from.' +1 day'));
		}

		

		// Loop through dates
		$data = array();
		foreach ($dates as $d) {
			$data[] = self::_getStats($d);
		}

		// echo "<pre>";
		// print_r($data);
		// die();

		die(CJSON::encode(array('chart_data' => $data)));
		//$view = Yii::app()->controller->renderPartial('list', array('transactions'=>$data), TRUE);
		
	}
	
	private function _getStats($date)
	{

		// $g_criteria = new CDbCriteria(array(
		// 				'condition' => 'DATE(Created_at) = DATE("'.$date.'") AND (SUBSTRING(Mobile_number, 1, 4) = "0905" OR SUBSTRING(Mobile_number, 1, 4) = "0906" OR SUBSTRING(Mobile_number, 1, 4) = "0915" OR SUBSTRING(Mobile_number, 1, 4) = "0916" OR SUBSTRING(Mobile_number, 1, 4) = "0917" OR SUBSTRING(Mobile_number, 1, 4) = "0918" OR SUBSTRING(Mobile_number, 1, 4) = "0925" OR SUBSTRING(Mobile_number, 1, 4) = "0926" OR SUBSTRING(Mobile_number, 1, 4) = "0927" OR SUBSTRING(Mobile_number, 1, 4) = "0935" OR SUBSTRING(Mobile_number, 1, 4) = "0936" OR SUBSTRING(Mobile_number, 1, 4) = "0937" OR SUBSTRING(Mobile_number, 1, 4) = "0994" OR SUBSTRING(Mobile_number, 1, 4) = "0996" OR SUBSTRING(Mobile_number, 1, 4) = "0997" OR SUBSTRING(Mobile_number, 1, 4) = "0817" OR SUBSTRING(Mobile_number, 1, 4) = "0977")'
		// 				));

		$sql = "SELECT COUNT(DISTINCT d.Mac_address) AS mobile_users, pl.brand_name, SUBSTRING(t.Mobile_number,2,5), LEFT(pr.prefix,5) 
				FROM `Cadbury_Captive`.`avail_mobile` AS `t` 
				JOIN Cadbury_Captive.device AS d ON t.Device_ID = d.Device_ID JOIN Cadbury_Captive.plan_prefix AS pr ON SUBSTRING(t.Mobile_number,2,5) = LEFT(pr.prefix,5) JOIN Cadbury_Captive.plan AS pl ON pr.plan_id = pl.plan_id 
				WHERE t.Nas_ID = ".$this->context->nas->id." AND t.Is_Validated = 1 AND DATE(t.Created_at) = DATE('".$date."')
				GROUP BY pl.brand_name";

		$users = Yii::app()->db_cp->createCommand($sql)->queryAll();

		// $criteria = new CDbCriteria;
		// $criteria->select = 'COUNT(DISTINCT d.Mac_address) as mobile_users, pl.brand_name';
		// // $criteria->distinct = true;
		// $criteria->join = 'JOIN Cadbury_Captive.device AS d ON t.Device_ID = d.Device_ID JOIN Cadbury_Captive.plan_prefix AS pr ON SUBSTRING(t.Mobile_number,2,5) = LEFT(pr.prefix,5) JOIN Cadbury_Captive.plan AS pl ON pr.plan_id = pl.plan_id';
		// // $criteria->join = 'JOIN  Cadbury_Captive.plan_prefix pr ON SUBSTRING(t.Mobile_number,2,5) = LEFT(pr.prefix,5)';
		// // $criteria->join = 'JOIN  Cadbury_Captive.plan pl ON pr.plan_id = pl.plan_id';
		// $criteria->condition = 't.Nas_ID = '.$this->context->nas->id.' AND t.Is_Validated = 1 AND DATE(t.Created_at) = DATE("'.$date.'")';
		// $criteria->group = 'pl.brand_name';

		// AND (SUBSTRING(Mobile_number, 1, 4) = "0905" OR SUBSTRING(Mobile_number, 1, 4) = "0906" OR SUBSTRING(Mobile_number, 1, 4) = "0915" OR SUBSTRING(Mobile_number, 1, 4) = "0916" OR SUBSTRING(Mobile_number, 1, 4) = "0917" OR SUBSTRING(Mobile_number, 1, 4) = "0918" OR SUBSTRING(Mobile_number, 1, 4) = "0925" OR SUBSTRING(Mobile_number, 1, 4) = "0926" OR SUBSTRING(Mobile_number, 1, 4) = "0927" OR SUBSTRING(Mobile_number, 1, 4) = "0935" OR SUBSTRING(Mobile_number, 1, 4) = "0936" OR SUBSTRING(Mobile_number, 1, 4) = "0937" OR SUBSTRING(Mobile_number, 1, 4) = "0994" OR SUBSTRING(Mobile_number, 1, 4) = "0996" OR SUBSTRING(Mobile_number, 1, 4) = "0997" OR SUBSTRING(Mobile_number, 1, 4) = "0817" OR SUBSTRING(Mobile_number, 1, 4) = "0977")

		// $users = AvailMobile::model()->findAll($criteria);

		$sql = "SELECT COUNT(DISTINCT d.Mac_address) AS total_mobile
				FROM `Cadbury_Captive`.`avail_mobile` AS `t` 
				JOIN Cadbury_Captive.device AS d ON t.Device_ID = d.Device_ID
				WHERE t.Nas_ID = ".$this->context->nas->id." AND t.Is_Validated = 1 AND DATE(t.Created_at) = DATE('".$date."')";

		$total_users = Yii::app()->db_cp->createCommand($sql)->queryAll();

		$stats = array();
		foreach($users as $u){
			$stats[strtolower(str_replace(' ', '', $u['brand_name']))] = $u['mobile_users'];
		}

		//total - defined
		$undefined = ((int)($total_users[0]['total_mobile'])) - ((isset($stats['globe']) ? (int)$stats['globe'] : 0) + (isset($stats['globelte']) ? (int)$stats['globelte'] : 0) + (isset($stats['touchmobile']) ? (int)$stats['touchmobile'] : 0) + (isset($stats['smartbuddy']) ? (int)$stats['smartbuddy'] : 0) + (isset($stats['smart']) ? (int)$stats['smart'] : 0) + (isset($stats['smarttnt']) ? (int)$stats['smarttnt'] : 0) + (isset($stats['sun']) ? (int)$stats['sun'] : 0));

		// $globe = AvailMobile::model()->findAll($criteria);

		// $criteria = new CDbCriteria;
		// $criteria->select = 'Mac_address';
		// $criteria->distinct = true;
		// $criteria->join = 'LEFT JOIN  Cadbury_Captive.device d ON t.Device_ID = d.Device_ID';
		// $criteria->condition = 't.Nas_ID = '.$this->context->nas->id.' AND DATE(t.Created_at) = DATE("'.$date.'") AND (SUBSTRING(Mobile_number, 1, 4) = "0813" OR SUBSTRING(Mobile_number, 1, 4) = "0900" OR SUBSTRING(Mobile_number, 1, 4) = "0907" OR SUBSTRING(Mobile_number, 1, 4) = "0908" OR SUBSTRING(Mobile_number, 1, 4) = "0909" OR SUBSTRING(Mobile_number, 1, 4) = "0910" OR SUBSTRING(Mobile_number, 1, 4) = "0911" OR SUBSTRING(Mobile_number, 1, 4) = "0912" OR SUBSTRING(Mobile_number, 1, 4) = "0918" OR SUBSTRING(Mobile_number, 1, 4) = "0919" OR SUBSTRING(Mobile_number, 1, 4) = "0921" OR SUBSTRING(Mobile_number, 1, 4) = "0928" OR SUBSTRING(Mobile_number, 1, 4) = "0929" OR SUBSTRING(Mobile_number, 1, 4) = "0930" OR SUBSTRING(Mobile_number, 1, 4) = "0931" OR SUBSTRING(Mobile_number, 1, 4) = "0938" OR SUBSTRING(Mobile_number, 1, 4) = "0939" OR SUBSTRING(Mobile_number, 1, 4) = "0940" OR SUBSTRING(Mobile_number, 1, 4) = "0946" OR SUBSTRING(Mobile_number, 1, 4) = "0947" OR SUBSTRING(Mobile_number, 1, 4) = "0948" OR SUBSTRING(Mobile_number, 1, 4) = "0949" OR SUBSTRING(Mobile_number, 1, 4) = "0971" OR SUBSTRING(Mobile_number, 1, 4) = "0980" OR SUBSTRING(Mobile_number, 1, 4) = "0989" OR SUBSTRING(Mobile_number, 1, 4) = "0998" OR SUBSTRING(Mobile_number, 1, 4) = "0999")';

		// $smart = AvailMobile::model()->findAll($criteria);

		// $criteria = new CDbCriteria;
		// $criteria->select = 'Mac_address';
		// $criteria->distinct = true;
		// $criteria->join = 'LEFT JOIN  Cadbury_Captive.device d ON t.Device_ID = d.Device_ID';
		// $criteria->condition = 't.Nas_ID = '.$this->context->nas->id.' AND DATE(t.Created_at) = DATE("'.$date.'") AND (SUBSTRING(Mobile_number, 1, 4) = "0922" OR SUBSTRING(Mobile_number, 1, 4) = "0923" OR SUBSTRING(Mobile_number, 1, 4) = "0925" OR SUBSTRING(Mobile_number, 1, 4) = "0932" OR SUBSTRING(Mobile_number, 1, 4) = "0933" OR SUBSTRING(Mobile_number, 1, 4) = "0934" OR SUBSTRING(Mobile_number, 1, 4) = "0942" OR SUBSTRING(Mobile_number, 1, 4) = "0943" OR SUBSTRING(Mobile_number, 1, 4) = "0944")';

		// $sun = AvailMobile::model()->findAll($criteria);

		if (count($stats) > 0) {
			return array('date'=>$date, 'globe'=> (isset($stats['globe']) ? (int)$stats['globe'] : 0) + (isset($stats['globelte']) ? (int)$stats['globelte'] : 0), 'touchmobile'=>(isset($stats['touchmobile']) ? $stats['touchmobile'] : 0), 'smartbuddy'=>(isset($stats['smartbuddy']) ? $stats['smartbuddy'] : 0), 'smart'=>(isset($stats['smart']) ? $stats['smart'] : 0), 'smarttnt'=>(isset($stats['smarttnt']) ? $stats['smarttnt'] : 0), 'sun'=>(isset($stats['sun']) ? $stats['sun'] : 0), 'undefined_users'=>$undefined);
		}
		return array('date'=>$date, 'globe'=>0, 'touchmobile'=>0, 'smartbuddy'=>0, 'smart'=>0, 'smarttnt'=>0, 'sun'=>0, 'undefined_users'=>$undefined);
	}

	public function actionDownloadCSV(){
		$date_from = Yii::app()->request->getPost('stat-from');
		$from = Yii::app()->request->getPost('stat-from');
		$date_to = Yii::app()->request->getPost('stat-to');

		$dates = array();
		
		while(date(strtotime($date_from)) < date(strtotime($date_to))){
			$dates[] = date('Y-m-d', strtotime($date_from.' +1 day'));
			$date_from = date('Y-m-d', strtotime($date_from.' +1 day'));
		}

		// Loop through dates
		$data = array();
		$headers = array("Date", "Globe", "Touchmobile", "Smart Buddy", "Smart", "Smart TNT", "Sun", "Undefined");
		array_push($data, $headers);
		foreach ($dates as $d) {
			$data[] = self::_getStats($d);
		}

		// die();
		// array_push($data, array("Monthly Total", $total, $logged_in));
		header("Content-Type: text/csv");
		header("Content-Disposition: attachment; filename=MobileReport-".$from."-".$date_to.".csv");
		// Disable caching
		header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
		header("Pragma: no-cache"); // HTTP 1.0
		header("Expires: 0"); // Proxies

		$output = fopen("php://output", "w");
	    foreach ($data as $row) {
	        fputcsv($output, $row); // here you can change delimiter/enclosure
	    }
	    fclose($output);
	}

}