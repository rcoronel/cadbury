<?php

class RoleController extends ControlpanelController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action))
		{
			// action buttons
			$this->actionButtons['index'] = array('add' => array('link' => $this->createURL($this->id.'/add')));
			$this->actionButtons['add'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back' => array('link' => $this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		// fields to be displayed
		$row_fields = array();
		$row_fields['Site_role_ID']['type'] = 'text';
		$row_fields['Site_role_ID']['class'] = 'text-center col-md-1';
		
		$row_fields['Role']['type'] = 'text';
		$row_fields['Role']['class'] = 'col-mid-2';
		
		$row_fields['Is_active']['type'] = 'select';
		$row_fields['Is_active']['class'] = 'text-center col-md-1';
		$row_fields['Is_active']['value'] = array('1'=>'Enabled', '0' =>'Disabled');
		$row_fields['Is_active']['mode'] = 'bool';
		
		$row_actions = array('edit', 'delete');
		
		// List and pagination
		$object = new SiteRole;
		$object->attributes = SiteRole::setSessionAttributes(Yii::app()->request->getPost('SiteRole'));
		
		$criteria = $object->search()->criteria;
		$criteria->compare('Site_ID', $this->context->site->Site_ID);
		$count = SiteRole::model()->count($criteria);
		
		$pages = new CPagination($count);
		$criteria->limit = 25;
		$criteria->offset = (Yii::app()->request->getQuery('page', 1) * $criteria->limit) - $criteria->limit;
		
		$pages->pageSize=$criteria->limit;
		$pages->applyLimit($criteria);
		
		$roles = array_merge(SiteRole::model()->findAllByAttributes(array('Site_role_ID'=>1)), SiteRole::model()->findAll($criteria));
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$roles, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions, 'pages'=>$pages));
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		$role = new SiteRole;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($role);
		}
		
		self::_renderForm($role);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id SiteRole primary key
	 * @return void
	 */
	public function actionUpdate($id)
	{
		if ($id == 1) {
			Yii::app()->getModule('controlpanel')->client->setFlash('fail', 'Cannot update the Main Account');
			$this->redirect(array($this->id.'/'));
		}
		
		$role = SiteRole::model()->findByPk($id);
		parent::checkNas($role->Nas_ID);
		
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($role);
		}
		
		self::_renderForm($role);
	}
	
	/**
	 * Delete record
	 * 
	 * @access public
	 * @param int $id SiteRole ID
	 * @return void
	 */
	public function actionDelete($id)
	{
		$role = SiteRole::model()->findByPk($id);
		SiteRole::validateObject($role, 'SiteRole');
		
		// Count client that has this Role
		$count = Site::model()->count("Site_role_ID={$role->Site_role_ID}");
		if ($count) {
			Yii::app()->getModule('controlpanel')->client->setFlash('fail', 'Cannot delete Role. There is an client assigned to the Role.');
			$this->redirect(array("$this->id/"));
		}
		
		if (Yii::app()->request->isPostRequest) {
			
			// check for security token
			if (CPasswordHelper::verifyPassword($role->Site_role_ID, Yii::app()->request->getPost('security_key'))) {

				// Delete record
				SiteRole::model()->deleteByPk($role->Site_role_ID);

				Yii::app()->getModule('controlpanel')->client->setFlash('success', 'Delete successful!');
				$this->redirect(array("$this->id/"));
			}
			else {
				Yii::app()->getModule('controlpanel')->client->setFlash('fail', 'Error in deletion.');
				$this->redirect(array("$this->id/"));
			}
		}
		$this->render('delete', array('role'=>$role));
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object SiteRole instance
	 * @return void
	 */
	private function _renderForm(SiteRole $object)
	{
		$fields = array();
		
		$fields['Role']['type'] = 'text';
		$fields['Role']['class'] = 'col-md-5';
		
		$fields['Is_active']['type'] = 'radio';
		$fields['Is_active']['class'] = 'col-lg-5';
		$fields['Is_active']['value'] = array('1' => 'Enabled', '0' => 'Disabled');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object' => $object, 'fields' => $fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object SiteRole instance
	 * @return	void
	 */
	private function _postValidate(SiteRole $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('SiteRole');
		$object->attributes = $post;
		$object->Nas_ID = $this->context->site->Site_ID;

		if ($object->validate())
		{
			if ($object->save()) {
				self::displayFormSuccess();
			}
		}
		else {
			self::displayFormError($object);
		}
	}
}