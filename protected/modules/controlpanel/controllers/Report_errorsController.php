<?php

class Report_errorsController extends ControlpanelController
{

	public function actionIndex()
	{
		if (Yii::app()->request->isPostRequest) {
			self::_getErrorStats();
		}
		
		// declare scripts
		Yii::app()->clientScript->registerCssFile(Link::css_url(array('jquery.datetimepicker.css')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery/jquery.datetimepicker.js')));

		Yii::app()->clientScript->registerCssFile(Link::css_url(array('morris', 'morris.css')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'raphael', 'raphael-min.js')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'morris', 'morris.min.js')));
		
		$reports = ClientMenu::model()->findAllByAttributes(array('Parent_client_menu_ID'=>20));

		$reps = array();		
		foreach($reports as $r){
			$temp = array('classname'=>$r->Classname, 'title'=>$r->Title, 'url'=>Yii::app()->createUrl('controlpanel/'.$r->Classname));
			$reps[] = $temp;
		}
		
		$this->render('index', array('reps'=>$reps));

	}
	
	private function _getErrorStats()
	{

		$date_from = Yii::app()->request->getPost('date-from');
		$date_to = Yii::app()->request->getPost('date-to');

		$dates = array();
		while(date(strtotime($date_from)) < date(strtotime($date_to))){
			$dates[] = date('Y-m-d', strtotime($date_from.' +1 day'));
			$date_from = date('Y-m-d', strtotime($date_from.' +1 day'));
		}

		// Loop through dates
		$data = array();
		foreach ($dates as $d) {
			$data[] = self::_getStats($d);
		}

		// echo "<pre>";
		// print_r($data);
		// die();

		die(CJSON::encode(array('chart_data' => $data)));
		
	}
	
	private function _getStats($date)
	{

		$errors = Yii::app()->db_cp->createCommand()->select('*')->from('aruba_xml_error')->queryAll();

		$data = array();
		foreach($errors as $e){

			$count = $this->_getCount($date,$e['status']);
			$e['count'] = $count['errors'];
			$data[$e['status']] = $e;

		}

		if ($data) {
			return array('date'=>$date, '1'=>$data[1]['count'], '3'=>$data[3]['count'], '4'=>$data[4]['count'], '5'=>$data[5]['count'], '99'=>$data[99]['count']);
		}
		return array('date'=>$date, '1'=>0, '3'=>0, '4'=>0, '5'=>0, '99'=>0);

	}

	private function _getCount($date,$id)
	{

		try{

			$errors = Yii::app()->db_cp->createCommand()->select('COUNT(*) as count')->from('aruba_log_'.$date)->where("Nas_ID = {$this->context->nas->id} AND Status = ".$id)->queryAll();
		}catch(CDbException $e){
			return array('errors'=>0);
		}
		// $errors = ArubaLog::model()->count("Nas_ID = {$this->context->nas->id} AND Status = ".$id);
		
		if ($errors) {
			return array('errors'=>$errors[0]['count']);
		}
		return array('errors'=>0);
	}

	public function actionDownloadCSV(){
		$date_from = Yii::app()->request->getPost('stat-from');
		$from = Yii::app()->request->getPost('stat-from');
		$date_to = Yii::app()->request->getPost('stat-to');

		$dates = array();
		while(date(strtotime($date_from)) < date(strtotime($date_to))){
			$dates[] = date('Y-m-d', strtotime($date_from.' +1 day'));
			$date_from = date('Y-m-d', strtotime($date_from.' +1 day'));
		}

		// Loop through dates
		$data = array();
		$headers = array("Date", "Unknown User", "Unknown External Age", "Authentication Failed", "Internal Error", "Timeout");
		array_push($data, $headers);
		foreach ($dates as $d) {
			$data[] = self::_getStats($d);
		}

		// die();
		// array_push($data, array("Monthly Total", $total, $logged_in));
		header("Content-Type: text/csv");
		header("Content-Disposition: attachment; filename=ErrorReport-".$from."-".$date_to.".csv");
		// Disable caching
		header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
		header("Pragma: no-cache"); // HTTP 1.0
		header("Expires: 0"); // Proxies

		$output = fopen("php://output", "w");
	    foreach ($data as $row) {
	        fputcsv($output, $row); // here you can change delimiter/enclosure
	    }
	    fclose($output);
	}

}