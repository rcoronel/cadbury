<?php

class ScenarioController extends ControlpanelController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action))
		{
			// action buttons
			$this->actionButtons['index'] = array('add' => array('link' => $this->createURL($this->id.'/add')));
			$this->actionButtons['add'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back' => array('link' => $this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * Display availment form
	 * 
	 * @access public
	 * @return void
	 */
	public function actionIndex()
	{
		// declare scripts
		Yii::app()->clientScript->registerCssFile(Link::css_url('controlpanel/to-do.css'));
		
		// fields to be displayed
		$row_fields = array();
		$row_fields['Scenario_ID']['type'] = 'text';
		$row_fields['Scenario_ID']['class'] = 'text-center col-md-1';
		
		$row_fields['Title']['type'] = 'text';
		$row_fields['Title']['class'] = 'col-mid-4';
		
		$row_actions = array('edit', 'delete');
		
		$scenarios = Scenario::model()->with('new', 'returning')->findAll(array('condition'=>"Nas_ID = {$this->context->nas->id}", 'order'=>'new.Level ASC, returning.Level ASC'));
		
		$this->render('index', array('object' => new Scenario, 'objectList'=>$scenarios, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		$scenario = new Scenario;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($scenario);
		}
		$nas_avails = NasAvailment::model()->with('availment')->findAllByAttributes(array('Nas_ID'=>$this->context->nas->id));
		$nas_avails_arr = CHtml::listdata($nas_avails, 'Availment_ID', function($a){return CHtml::encode($a->availment->Name);});
		$this->render('form', array('availments'=>$nas_avails_arr, 'scenario'=>new Scenario));
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id Scenario primary key
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$scenario = Scenario::model()->findByPk($id);
		Scenario::validateObject($scenario, 'Scenario');
		parent::checkNas($scenario->Nas_ID);
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($scenario);
		}
		
		$nas_avails = NasAvailment::model()->with('availment')->findAllByAttributes(array('Nas_ID'=>$this->context->nas->id));
		$nas_avails_arr = CHtml::listdata($nas_avails, 'Availment_ID', function($a){return CHtml::encode($a->availment->Name);});
		
		// Scenarios
		$new_scenarios = ScenarioAvailment::model()->findAllByAttributes(array('Scenario_ID'=>$scenario->Scenario_ID), array('order'=>'Level ASC'));
		$returning_scenarios = ScenarioReturning::model()->findAllByAttributes(array('Scenario_ID'=>$scenario->Scenario_ID), array('order'=>'Level ASC'));
		$this->render('form', array('availments'=>$nas_avails_arr, 'scenario'=>$scenario, 'new_scenarios'=>$new_scenarios, 'returning_scenarios'=>$returning_scenarios));
	}
	
	/**
	 * Delete record
	 * 
	 * @access public
	 * @param int $id Scenario ID
	 * @return void
	 */
	public function actionDelete($id)
	{
		$scenario = Scenario::model()->findByPk($id);
		Scenario::validateObject($scenario, 'Scenario');
		parent::checkNas($scenario->Nas_ID);
		
		if (Yii::app()->request->isPostRequest) {
			// check for security token
			if (CPasswordHelper::verifyPassword($scenario->Scenario_ID, Yii::app()->request->getPost('security_key'))) {
				// Delete scenarios
				ScenarioAvailment::model()->deleteAllByAttributes(array('Scenario_ID'=>$scenario->Scenario_ID));
				ScenarioReturning::model()->deleteAllByAttributes(array('Scenario_ID'=>$scenario->Scenario_ID));
				
				// Delete record
				Scenario::model()->deleteByPk($scenario->Scenario_ID);
				Yii::app()->getModule('controlpanel')->client->setFlash('success', 'Delete successful!');
				$this->redirect(array("$this->id/"));
			}
			else {
				Yii::app()->getModule('controlpanel')->client->setFlash('fail', 'Error in deletion.');
				$this->redirect(array("$this->id/"));
			}
		}
		$this->render('delete', array('scenario'=>$scenario));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object Scenario instance
	 * @return	void
	 */
	private function _postValidate(Scenario $object)
	{
		sleep(1);
		
		$post = $_POST;
		
		$object->Title = $post['Title'];
		$object->Nas_ID = $this->context->nas->id;
		if ($object->validate() && $object->save()) {
			// Delete existing
			ScenarioAvailment::model()->deleteAllByAttributes(array('Scenario_ID'=>$object->Scenario_ID));
			ScenarioReturning::model()->deleteAllByAttributes(array('Scenario_ID'=>$object->Scenario_ID));
			
			// New user scenario
			if ( ! empty($post['new']['availment_id'])) {
				foreach ($post['new']['availment_id'] as $index => $availment_id) {
					$new_scenario = new ScenarioAvailment;
					$new_scenario->Scenario_ID = $object->Scenario_ID;
					$new_scenario->Level = $post['new']['level'][$index];
					$new_scenario->Availment_ID = $post['new']['availment_id'][$index];
					$new_scenario->validate() && $new_scenario->save();
				}
				
				foreach ($post['returning']['availment_id'] as $index => $availment_id) {
					$return_scenario = new ScenarioReturning();
					$return_scenario->Scenario_ID = $object->Scenario_ID;
					$return_scenario->Level = $post['returning']['level'][$index];
					$return_scenario->Availment_ID = $post['returning']['availment_id'][$index];
					$return_scenario->validate() && $return_scenario->save();
				}
			}
			self::displayFormSuccess();
		}
		self::displayFormError($object);
	}
	
	/**
	 * View
	 * 
	 * @access public
	 * @return void
	 */
	public function actionView($id)
	{
		$availment = Availment::model()->findByPk($id);
		Availment::validateObject($availment, 'Availment');
		
		$this->render('view', array('availment'=>$availment));
	}
}