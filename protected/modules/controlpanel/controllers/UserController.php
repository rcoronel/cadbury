<?php

class UserController extends ControlpanelController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action)) {
			// action buttons
			$this->actionButtons['view'] = array('back' => array('link' => $this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		// fields to be displayed
		$row_fields = array();
		$row_fields['Fb_ID']['type'] = 'text';
		$row_fields['Fb_ID']['class'] = 'text-center col-md-1';
		
		$row_fields['Email']['type'] = 'text';
		$row_fields['Email']['class'] = 'col-mid-4';
		
		$row_fields['Firstname']['type'] = 'text';
		$row_fields['Firstname']['class'] = 'col-mid-1';
		
		$row_fields['Lastname']['type'] = 'text';
		$row_fields['Lastname']['class'] = 'col-mid-1';
		
		$row_fields['Gender']['type'] = 'select';
		$row_fields['Gender']['class'] = 'col-md-1';
		$row_fields['Gender']['value'] = array('male'=>'Male', 'female' =>'Female');
		
		$row_fields['Device_ID']['type'] = 'text';
		$row_fields['Device_ID']['class'] = 'col-md-2';
		$row_fields['Device_ID']['parent'] = 'device';
		$row_fields['Device_ID']['child'] = 'Mac_address';
		
		$row_actions = array('view');
		
		// List and pagination
		$object = new AvailFacebook;
		$object->attributes = AvailFacebook::setSessionAttributes(Yii::app()->request->getPost('AvailFacebook'));
		
		$criteria = $object->search()->criteria;
		$criteria->compare('Nas_ID', $this->context->nas->id);
		$count = AvailFacebook::model()->count($criteria);
		
		$pages = new CPagination($count);
		$criteria->limit = 25;
		$criteria->offset = (Yii::app()->request->getQuery('page', 1) * $criteria->limit) - $criteria->limit;
		
		$pages->pageSize=$criteria->limit;
		$pages->applyLimit($criteria);
		
		$clients = AvailFacebook::model()->findAll($criteria);
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$clients, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions, 'pages'=>$pages));
	}
	
	/**
	 * View record
	 * 
	 * @access public
	 * @param int $id Client ID
	 * @return void
	 */
	public function actionView($id)
	{
		$user = AvailFacebook::model()->findByPk($id);
		AvailFacebook::validateObject($user, 'AvailFacebook');
		parent::checkNas($user->Nas_ID);
		
		$this->render('view', array('user'=>$user));
	}
}