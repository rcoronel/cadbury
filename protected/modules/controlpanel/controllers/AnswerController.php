<?php

class AnswerController extends ControlpanelController
{
        public $parent_id; // Survey ID
        public $question_id; // Question ID from where the answers belongs to
        public $title; // Title of the oage
        public $redirect_page; // Redirect page
        private $answer_type; // Type of answer to limit adding different types of answer

        /**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action)){
                        $this->parent_id = Yii::app()->request->getParam('parent_id');
                        $this->question_id = Yii::app()->request->getParam('question_id') == null ?  Yii::app()->request->getParam('id') : Yii::app()->request->getParam('question_id');
                        $this->redirect_page = $this->createURL($this->id.'&parent_id='.$this->parent_id.'&id='.$this->question_id.'/');
                        $this->answer_type = ($answer_type = SurveyAnswer::model()->findByAttributes(array("Survey_question_ID"=>$this->question_id), array("limit"=>1))) ? $answer_type->Answer_type : null;

                        // action buttons
                        $this->actionButtons['index'] = array(
                                'back' => array('link' => $this->createURL('question&id='.$this->parent_id.'/'))
                            );
                        if(!$this->answer_type || $this->answer_type == "radio" || $this->answer_type == "check" || $this->answer_type == "select") {
                            $this->actionButtons['index']['add'] = array('link' => $this->createURL($this->id.'/add&parent_id='.$this->parent_id.'&id='.$this->question_id.'/'));
                        }
                        $this->actionButtons['view'] = array(
                                'add' => array('link' => $this->createURL($this->id.'/add')),
                                'back' => array('link' => $this->createURL($this->id.'&parent_id='.$this->parent_id.'&id='.$this->question_id))
                            );
                        $this->actionButtons['add'] = array(
                            'back' => array('link' => $this->createURL($this->id.'&parent_id='.$this->parent_id.'&id='.$this->question_id.'/'))
                            );
                        $this->actionButtons['update'] = array('back' => array('link' => $this->createURL($this->id.'&parent_id='.$this->parent_id.'&id='.$this->question_id.'/')));
                        $this->actionButtons['delete'] = array('back' => array('link' => $this->createURL($this->id.'&parent_id='.$this->parent_id.'&id='.$this->question_id.'/')));

                        return true;
		}
	}
        
        
	public function actionIndex($parent_id, $id)
	{
                if(!$question = SurveyQuestion::model()->findByAttributes(array("Survey_question_ID"=>$id, "Survey_ID"=>$parent_id))) {
                    $this->redirect(array('survey/')); // Redirect if record not found (query string protection)
                }
                else {
                        $this->title = $question->Question;
                        // declare scripts
                        Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'plugins', 'jquery.tablednd.js')));
                        Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/jquery.toastr.min.js'));

                        // fields to be displayed
                        $row_fields = array();
                        $row_fields['Survey_answer_ID']['type'] = 'text';
                        $row_fields['Survey_answer_ID']['class'] = 'text-center col-md-1';

                        $row_fields['Survey_question_ID']['type'] = 'text';
                        $row_fields['Survey_question_ID']['class'] = 'col-mid-4';
                        
                        $row_fields['Answer']['type'] = 'text';
                        $row_fields['Answer']['class'] = 'col-mid-4';
                        
                        $row_fields['Answer_type']['type'] = 'text';
                        $row_fields['Answer_type']['class'] = 'col-mid-4';

                        $row_fields['Position']['type'] = 'text';
                        $row_fields['Position']['class'] = 'text-center col-md-1 dragHandle';

                        $row_fields['Updated_at']['type'] = 'text';
                        $row_fields['Updated_at']['class'] = 'col-md-2';

                        $row_actions = array('edit', 'delete');
                        
                        // List and pagination
                        $object = new SurveyAnswer;
                        $object->attributes = SurveyAnswer::setSessionAttributes(Yii::app()->request->getPost('SurveyAnswer'));
                        
                        $criteria = $object->search()->criteria;
                        $criteria->compare('Survey_question_ID', $id);
                        $count = SurveyAnswer::model()->count($criteria);

                        $pages = new CPagination($count);
                        $criteria->limit = 25;
                        $criteria->order = "Position ASC";
                        $criteria->offset = (Yii::app()->request->getQuery('page', 1) * $criteria->limit) - $criteria->limit;

                        $pages->pageSize=$criteria->limit;
                        $pages->applyLimit($criteria);
                        
                        $answers = SurveyAnswer::model()->findAll($criteria);

                        // Render the view
                        $this->render('/layouts/list', array('object' => $object, 'objectList'=>$answers, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions, 'pages'=>$pages));
                }
	}
        
        // Add answer form
	public function actionAdd()
	{
                if(!$this->answer_type || $this->answer_type == "radio" || $this->answer_type == "check" || $this->answer_type == "select") {
                        $answer = new SurveyAnswer;

                        if (Yii::app()->request->isPostRequest) {
                                self::_postValidate($answer);
                        }

                        self::_renderForm($answer);
                }
                else {
                        $this->redirect(array($this->id.'&parent_id='.$this->parent_id.'&id='.$this->question_id.'/'));
                }
	}
        
        // Delete answer form
	public function actionDelete($parent_id, $question_id, $id)
	{
                if(SurveyQuestion::model()->findByAttributes(array("Survey_question_ID"=>$question_id, "Survey_ID"=>$parent_id))) {
                        $answer = SurveyAnswer::model()->findByPk($id);

                        if (Yii::app()->request->isPostRequest) {
                                self::_postDelete($id);
                        }

                        self::_renderForm($answer, FALSE, TRUE);
                }
                else {
                    $this->redirect(array('survey/'));
                }
	}
        
        // Update Form
	public function actionUpdate($parent_id, $question_id, $id)
	{
                if(SurveyQuestion::model()->findByAttributes(array("Survey_question_ID"=>$question_id, "Survey_ID"=>$parent_id))) {
                        $answer = SurveyAnswer::model()->findByPk($id);

                        if (Yii::app()->request->isPostRequest) {
                                self::_postValidate($answer);
                        }

                        self::_renderForm($answer);
                }
                else {
                    $this->redirect(array('survey/'));
                }
	}
        
        // View Form
	public function actionView()
	{
		// declare scripts
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'plugins', 'jquery.tablednd.js')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/jquery.toastr.min.js'));
		
		// fields to be displayed
		$row_fields = array();
		$row_fields['Survey_question_ID']['type'] = 'text';
		$row_fields['Survey_question_ID']['class'] = 'text-center col-md-1';
		
		$row_fields['Question']['type'] = 'text';
		$row_fields['Question']['class'] = 'col-mid-4';
		
		$row_fields['Is_required']['type'] = 'select';
		$row_fields['Is_required']['class'] = 'text-center col-md-1';
		$row_fields['Is_required']['value'] = array('1' => 'Enabled', '0' => 'Disabled');
		$row_fields['Is_required']['mode'] = 'bool';
		
		$row_fields['Position']['type'] = 'text';
		$row_fields['Position']['class'] = 'text-center col-md-1 dragHandle';
                
                $fields['Updated_at']['text'] = 'text';
                $fields['Updated_at']['class'] = 'col-lg-5';

		$row_actions = array('view', 'edit', 'delete');
		
		$questions = SurveyQuestion::model()->findAll();
	
		$this->render('/layouts/list', array('object' => new SurveyQuestion, 'objectList'=>$questions, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
	}
        
        /**
	 * Render form
	 * 
	 * @access private
	 * @param object $object Survey instance
	 * @return void
	 */
	private function _renderForm(SurveyAnswer $object, $isSurvey = null, $isDeleteForm = null)
	{       
		$fields = array();
                    
		$fields['Answer']['type'] = 'wysiwyg';
		$fields['Answer']['class'] = 'col-md-5';
                
                if(!$this->answer_type) {
                    $fields['Answer_type']['type'] = 'select';
                    $fields['Answer_type']['class'] = 'col-md-5';
                    $fields['Answer_type']['value'] = array(
                        'text' => 'Text', 'textarea' => 'Textarea',
                        'select' => 'Select',
                        'check' => 'Checkbox',
                        'radio' => 'Radio Button'
                        );
                    $fields['Answer_type']['mode'] = 'bool';
                }
                else {
                    $fields['Answer_type']['type'] = 'text';
                    $fields['Answer_type']['class'] = 'col-md-5';
                    $fields['Answer_type']['value'] = $this->answer_type;
                    $fields['Answer_type']['readonly'] = "readonly";
                }
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;

		$this->render('/layouts/form', array('object' => $object, 'fields' => $fields, 'isSurvey'=>$isSurvey, 'isDeleteForm'=>$isDeleteForm));
	}
        
        /**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object SurveyAnswer instance
	 * @return	void
	 */
	private function _postValidate(SurveyAnswer $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('SurveyAnswer');
		$object->attributes = $post;
                $object->Survey_question_ID = $this->question_id;
//                $object->Position = 0; // Comment out before deployment
                $object->Updated_at = date("Y-m-d H:i:s",strtotime(date("Y-m-d")));
                
		if ($object->validate())
		{
			if ($object->save()) {
                            	// add all access to main client
				
				self::displayFormSuccess();
			}
		}
		else {
			self::displayFormError($object);
		}
	}
        
        /**
	 * Delete the form entry
	 * 
	 * @access	public
	 * @param object $object SurveyAnswer instance
	 * @return	void
	 */
	private function _postDelete($id=null)
	{
		$answer = SurveyAnswer::model()->findByPk($id);
		SurveyAnswer::validateObject($answer, 'SurveyAnswer');
                
                $answer->delete();
                
                self::displayFormSuccess('Record successfully deleted!');
	}
        
        /**
	 * Update menu position
	 * 
	 * @access active
	 * @return string
	 */
	public function actionPosition()
	{
		$query = Yii::app()->request->getQuery('page-list');
		
		$menu = new SurveyAnswer;
		foreach ($query as $position => $id) {
			$menu->updateByPk($id, array('Position' => (int)$position + 1));
		}
		
		die('Repositioning successful. Refresh page to view changes');
	}
}