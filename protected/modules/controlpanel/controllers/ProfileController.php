<?php

class ProfileController extends ControlpanelController
{
	/**
	 * Show client profile based on the logged-in user
	 * 
	 * @access public
	 * @return void
	 */
	public function actionIndex()
	{
		// declare scripts
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/jquery.toastr.min.js'));
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/ajaxfileupload/jquery.ajaxfileupload.js'));
		
		$client = $this->context->client;
		$this->render('index', array('client'=>$client));
	}
	
	/**
	 * Update client based on logged-in user
	 * 
	 * @access public
	 * @return json
	 */
	public function actionUpdate()
	{
		$client = $this->context->client;
		
		if (Yii::app()->request->isPostRequest) {
			self::_validatePost($client);
		}
		
		die(CJSON::encode(array('status'=>1, 'message'=>$this->renderPartial('form', array('client'=>$client), TRUE))));
	}
	
	/**
	 * Change client password
	 * 
	 * @access public
	 * @return void
	 */
	public function actionChange_password()
	{
		$client = $this->context->client;
		$client->setScenario('change_password');
		
		if (Yii::app()->request->isPostRequest) {
			self::_validatePassword($client);
		}
		
		die(CJSON::encode(array('status'=>1, 'message'=>$this->renderPartial('change_password', array('client'=>$client), TRUE))));
	}
	
	/**
	 * Update image
	 * 
	 * @access public
	 * @return void
	 */
	public function actionUpdate_image()
	{
		$client = $this->context->client;
		if (Yii::app()->request->isPostRequest) {
			self::_uploadImage($client);
		}
		
		die(CJSON::encode(array('status'=>1, 'message'=>$this->renderPartial('update_image', array('client'=>$client), TRUE))));
	}
	
	/**
	 * Validate then update the client
	 * 
	 * @access private
	 * @param Client $client
	 * @return json
	 */
	private function _validatePost(Client $client)
	{
		sleep(1);
		$post = Yii::app()->request->getPost('Client');
		$client->attributes = $post;
		
		if ($client->validate())
		{
			if ($client->save()) {
				self::displayFormSuccess();
			}
		}
		else {
			self::displayFormError($client);
		}
	}

	/**
	 * Validate then change password
	 * 
	 * @access private
	 * @param Client $client
	 * @return json
	 */
	private function _validatePassword(Client $client)
	{
		sleep(1);
		$_password = $client->Password;
		$post = Yii::app()->request->getPost('Client');
		$client->attributes = $post;
		
		if ($client->validate() && $client->change_password($_password))
		{
			$client->setScenario('update');
			$client->Password = CPasswordHelper::hashPassword($client->New_password);
			if ($client->save()) {
				self::displayFormSuccess();
			}
		}
		else {
			self::displayFormError($client);
		}
	}
	
	/**
	 * Validate then update image
	 * 
	 * @access private
	 * @param Client $client
	 * @return json
	 */
	private function _uploadImage(Client $client)
	{
		sleep(1);
		$upload_path = dirname(Yii::app()->request->scriptFile)."/images/client/{$this->context->client->Nas_ID}/admin/";
		if ( ! is_dir($upload_path)) {
			mkdir($upload_path, 0777, TRUE);
		}
		
		$previous_image = $client->Image;
		$client->setScenario('update_image');
		$client->Image = CUploadedFile::getInstance($client, 'Image');
		
		if ($client->validate()) {
			// remove previous image
			@unlink($upload_path.$previous_image);
			
			$filename = md5($client->Client_ID).'.'.$client->Image->extensionName;
			Image_Manager::resize($client->Image->tempName, $upload_path.$filename, 100, 100, $client->Image->extensionName);
			
			// save image
			$client->Image = $filename;

			if ($client->save()) {
				$json = array('status'=>1, 'message'=>'Image updated!');
				die(CJSON::encode($json));
			}
			else {
				$json_array = (array('status'=>0, 'message'=>'Error in saving / uploading file.'));
				die(CJSON::encode($json_array));
			}
		}
		else {
			$json_array = (array('status'=>0, 'message'=>'Invalid file. Please check.'));
			die(CJSON::encode($json_array));
		}
	}
}