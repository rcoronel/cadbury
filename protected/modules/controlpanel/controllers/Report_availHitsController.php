<?php

class Report_availHitsController extends ControlpanelController
{
	public function actionIndex()
	{
		if (Yii::app()->request->isPostRequest) {
			self::_getConnectionStats();
		}
		
		// declare scripts
		Yii::app()->clientScript->registerCssFile(Link::css_url(array('jquery.datetimepicker.css')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery/jquery.datetimepicker.js')));

		Yii::app()->clientScript->registerCssFile(Link::css_url(array('morris', 'morris.css')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'raphael', 'raphael-min.js')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'morris', 'morris.min.js')));
		
		$availment = Availment::model()->findAll();

		$availments = array();
		foreach($availment as $a){
			$availments[$a->Availment_ID] = $a->Name;
		}

		$availments[98] = 'All';

		$reports = ClientMenu::model()->findAllByAttributes(array('Parent_client_menu_ID'=>20));

		$reps = array();		
		foreach($reports as $r){
			$temp = array('classname'=>$r->Classname, 'title'=>$r->Title, 'url'=>Yii::app()->createUrl('controlpanel/'.$r->Classname));
			$reps[] = $temp;
		}
		
		$this->render('index', array('availment'=>$availments, 'reps'=>$reps));
	}
	
	private function _getConnectionStats()
	{
		$date_from = Yii::app()->request->getPost('date-from');
		$date_to = Yii::app()->request->getPost('date-to');
		$availment = Yii::app()->request->getPost('availment');
		$type = Yii::app()->request->getPost('stat_type');

		$dates = array();
		
		while(date(strtotime($date_from)) < date(strtotime($date_to))){
			$dates[] = date('Y-m-d', strtotime($date_from.' +1 day'));
			$date_from = date('Y-m-d', strtotime($date_from.' +1 day'));
		}

		// Loop through dates
		// echo "<pre>";
		$data = array();
		foreach ($dates as $d) {
			if((int)$availment == 98){
				$data[] = self::_getAllStats($d, $type);
			}else{
				$data[] = self::_getStats($d, $availment);
			}
		}

		die(CJSON::encode(array('chart_data' => $data)));
		//$view = Yii::app()->controller->renderPartial('list', array('transactions'=>$data), TRUE);
		
	}
	
	private function _getStats($date,$id)
	{

		try{
			$attempts = Yii::app()->db_cp->createCommand()
					->select('COUNT(*) as count')
					->from('connection_attempt_'.$date)
					->where('Nas_ID = '.$this->context->nas->id.' AND Availment_ID = '.$id.' AND Status = 0')
					->queryAll();

			$success = Yii::app()->db_cp->createCommand()
					->select('COUNT(*) as count')
					->from('connection_attempt_'.$date)
					->where('Nas_ID = '.$this->context->nas->id.' AND Availment_ID = '.$id.' AND Status = 1')
					->queryAll();
		}catch(CDbException $e){
			return array('date'=>$date, 'attempts'=>0, 'success'=>0);
		}
	
		if ($attempts || $success) {
			return array('date'=>$date, 'attempts'=>$attempts[0]['count'], 'success'=>$success[0]['count']);
		}
		return array('date'=>$date, 'attempts'=>0, 'success'=>0);
	}

	private function _getAllStats($date,$type){

		$availment = Availment::model()->findAll();

		$availments = array();
		$data = array();
		$attempts = array();
		$success = array();
		$availments['date'] = $date;
		foreach($availment as $a){

			try{
				// $attempts = Yii::app()->db_cp->createCommand()
				// 		->select('COUNT(*) as count')
				// 		->from('connection_attempt_'.$date)
				// 		->where('Nas_ID = '.$this->context->nas->id.' AND Availment_ID = '.$a->Availment_ID.' AND Status = 0')
				// 		->queryAll();

				$stats = Yii::app()->db_cp->createCommand()
						->select('COUNT(*) as count')
						->from('connection_attempt_'.$date)
						->where('Nas_ID = '.$this->context->nas->id.' AND Availment_ID = '.$a->Availment_ID.' AND Status = '.(int)$type)
						->queryAll();	

				$availments[strtolower(str_replace('Avail', '', $a->Model))] = $stats[0]['count'];

			}catch(CDbException $e){

				$availments[strtolower(str_replace('Avail', '', $a->Model))] = 0;

			}
		
		}

		return $availments;

	}

	public function actionDownloadCSV(){
		$date_from = Yii::app()->request->getPost('stat-from');
		$from = Yii::app()->request->getPost('stat-from');
		$date_to = Yii::app()->request->getPost('stat-to');
		$availment = Yii::app()->request->getPost('availment-id');
		$type = Yii::app()->request->getPost('stat_type');

		$dates = array();
		
		while(date(strtotime($date_from)) < date(strtotime($date_to))){
			$dates[] = date('Y-m-d', strtotime($date_from.' +1 day'));
			$date_from = date('Y-m-d', strtotime($date_from.' +1 day'));
		}

		// Loop through dates
		$data = array();
		if((int)$availment == 98){
			$headers = array('Date', 'Facebook','Registration', 'Mobile','Tattoo', 'Survey','Easy', 'Free');
		}else{
			$headers = array("Date", "Attempts", "Success");
		}
		array_push($data, $headers);

		foreach ($dates as $d) {
			if((int)$availment == 98){
				$data[] = self::_getAllStats($d, $type);
			}else{
				$data[] = self::_getStats($d, $availment);
			}
		}

		// die();
		// array_push($data, array("Monthly Total", $total, $logged_in));
		header("Content-Type: text/csv");
		header("Content-Disposition: attachment; filename=AvailHitsReport-".$from."-".$date_to.".csv");
		// Disable caching
		header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
		header("Pragma: no-cache"); // HTTP 1.0
		header("Expires: 0"); // Proxies

		$output = fopen("php://output", "w");
	    foreach ($data as $row) {
	        fputcsv($output, $row); // here you can change delimiter/enclosure
	    }
	    fclose($output);
	}

}