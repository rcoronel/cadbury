<?php

class Portal_adminController extends ControlpanelController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action)) {
			// action buttons
			$this->actionButtons['index'] = array('add' => array('link' => $this->createURL($this->id.'/add')));
			$this->actionButtons['add'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back' => array('link' => $this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		// fields to be displayed
		$row_fields = array();
		$row_fields['portal_admin_id']['type'] = 'text';
		$row_fields['portal_admin_id']['class'] = 'text-center col-md-1';
		
		$portals = Portal::model()->findAllByAttributes(array('site_id'=>$this->context->site->site_id), array('order'=>'Name ASC'));
		$row_fields['portal_id']['type'] = 'select';
		$row_fields['portal_id']['class'] = 'col-md-2';
		$row_fields['portal_id']['value'] = CHtml::listData($portals, 'portal_id', 'name');
		
		$row_fields['email']['type'] = 'text';
		$row_fields['email']['class'] = 'col-mid-2';
		
		$row_fields['firstname']['type'] = 'text';
		$row_fields['firstname']['class'] = 'col-mid-1';
		
		$row_fields['lastname']['type'] = 'text';
		$row_fields['lastname']['class'] = 'col-mid-1';
		
		$row_fields['is_active']['type'] = 'select';
		$row_fields['is_active']['class'] = 'text-center col-md-1';
		$row_fields['is_active']['value'] = array('1'=>'Can login', '0' =>'Cannot login');
		$row_fields['is_active']['mode'] = 'bool';
		
		$row_actions = array('edit', 'delete');
		$object = new PortalAdmin();
		$list = self::_showList($object, $row_fields, $row_actions);
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$list, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
//		// List and pagination
//		$object = new PortalAdmin;
//		$object->attributes = PortalAdmin::setSessionAttributes(Yii::app()->request->getPost('PortalAdmin'));
//		
//		$criteria = $object->search()->criteria;
//		$criteria->addCondition("site.site_id = {$this->context->site->site_id}");
//		$count = PortalAdmin::model()->with('site')->count($criteria);
//		
//		$pages = new CPagination($count);
//		$criteria->limit = 25;
//		$criteria->offset = (Yii::app()->request->getQuery('page', 1) * $criteria->limit) - $criteria->limit;
//		
//		$pages->pageSize=$criteria->limit;
//		$pages->applyLimit($criteria);
//		
//		$clients = PortalAdmin::model()->with('site')->findAll($criteria);
//		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$clients, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions, 'pages'=>$pages));
	}
	
	/**
	 * Display list
	 * 
	 * @param object $object PortalAdmin instance
	 * @param array $row_fields
	 * @param array $row_actions
	 * @return mixed
	 */
	private function _showList(PortalAdmin $object, $row_fields, $row_actions)
	{
		$object->attributes = PortalAdmin::setSessionAttributes(Yii::app()->request->getPost('PortalAdmin'));
		$criteria = $object->search()->criteria;
		$criteria->addCondition("site.site_id = {$this->context->site->site_id}");
		$count = PortalAdmin::model()->with('site')->count($criteria);
		
		$pages = new CPagination($count);
		$criteria->limit = 25;
		$criteria->offset = (Yii::app()->request->getQuery('page', 1) * $criteria->limit) - $criteria->limit;
		
		$pages->pageSize=$criteria->limit;
		$pages->applyLimit($criteria);
		
		$list = PortalAdmin::model()->with('site')->findAll($criteria);
		$view = $this->renderPartial('/layouts/list-body', array('object'=>$object, 'objectList'=>$list, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions), TRUE);
		if (Yii::app()->request->isAjaxRequest) {
			die(CJSON::encode(array('status'=>1, 'message'=>$view)));
		}
		return $view;
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		$client = new PortalAdmin;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($client);
		}
		
		self::_renderForm($client);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id PortalAdmin ID
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$client = PortalAdmin::model()->findByPk($id);
		PortalAdmin::validateObject($client, 'PortalAdmin');
		//parent::checkNas($client->Nas_ID);
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($client);
		}
		
		self::_renderForm($client);
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object PortalAdmin instance
	 * @return void
	 */
	private function _renderForm(PortalAdmin $object)
	{
		$fields = array();
		
		$portals = Portal::model()->findAllByAttributes(array('site_id'=>$this->context->site->site_id));
		$fields['portal_id']['type'] = 'select';
		$fields['portal_id']['class'] = 'col-md-5';
		$fields['portal_id']['value'] = CHtml::listData($portals, 'portal_id', 'name');
		
		$fields['email']['type'] = 'text';
		$fields['email']['class'] = 'col-md-5';
		
		$fields['password']['type'] = 'password';
		$fields['password']['class'] = 'col-md-5';
		
		$fields['confirm_password']['type'] = 'password';
		$fields['confirm_password']['class'] = 'col-md-5';
		
		$fields['firstname']['type'] = 'text';
		$fields['firstname']['class'] = 'col-md-5';
		
		$fields['lastname']['type'] = 'text';
		$fields['lastname']['class'] = 'col-md-5';
		
		$fields['is_active']['type'] = 'radio';
		$fields['is_active']['class'] = 'col-md-5';
		$fields['is_active']['value'] = array('1' => 'Can login', '0' => 'Cannot login');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object' => $object, 'fields' => $fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object PortalAdmin instance
	 * @return	void
	 */
	private function _postValidate(PortalAdmin $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('PortalAdmin');
		$object->attributes = $post;
		$object->portal_role_id = 1;
		
		// Validate object
		if ($object->validate()) {
			// Forcing Confirm_password to be the same as Password
			// We do this in order to avoid redundant error on save
			$object->password = CPasswordHelper::hashPassword($object->password);
			$object->confirm_password = $object->password;
			if ($object->save()) {
				self::displayFormSuccess();
			}
		}
		self::displayFormError($object);
	}
}