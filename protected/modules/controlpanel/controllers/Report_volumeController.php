<?php

class Report_volumeController extends ControlpanelController
{
	public function actionIndex()
	{
		if (Yii::app()->request->isPostRequest) {
			self::_getConnectionStats();
		}
		
		// declare scripts
		Yii::app()->clientScript->registerCssFile(Link::css_url(array('jquery.datetimepicker.css')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery/jquery.datetimepicker.js')));

		Yii::app()->clientScript->registerCssFile(Link::css_url(array('morris', 'morris.css')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'raphael', 'raphael-min.js')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'morris', 'morris.min.js')));
		
		$reports = ClientMenu::model()->findAllByAttributes(array('Parent_client_menu_ID'=>20));

		$reps = array();		
		foreach($reports as $r){
			$temp = array('classname'=>$r->Classname, 'title'=>$r->Title, 'url'=>Yii::app()->createUrl('controlpanel/'.$r->Classname));
			$reps[] = $temp;
		}
		
		$this->render('index', array('reps'=>$reps));
	}
	
	private function _getConnectionStats()
	{
		$date_from = Yii::app()->request->getPost('date-from');
		$date_to = Yii::app()->request->getPost('date-to');

		$dates = array();
		
		while(date(strtotime($date_from)) < date(strtotime($date_to))){
			$dates[] = date('Y-m-d', strtotime($date_from.' +1 day'));
			$date_from = date('Y-m-d', strtotime($date_from.' +1 day'));
		}

		$nas = RadiusNas::model()->findByAttributes(array('id'=>$this->context->nas->id));

		// Loop through dates
		$data = array();
		foreach ($dates as $d) {
			$data[] = self::_getStats($d,$nas->switchip);
		}

		// echo "<pre>";
		// print_r($data);
		// die();

		die(CJSON::encode(array('chart_data' => $data)));
		//$view = Yii::app()->controller->renderPartial('list', array('transactions'=>$data), TRUE);
		
	}
	
	private function _getStats($date,$ip)
	{

		$volume = Yii::app()->db_radius->createCommand("SELECT SUM(`acctoutputoctets`) as `output`, SUM(`acctinputoctets`) as `input` from radacct WHERE  username <> 'fb_guest' AND nasipaddress = '".$ip."' AND DATE(acctstarttime) = DATE('".$date."')")->queryAll();
		// RadiusAcct::model()->findBySql('SELECT SUM(`acctsessiontime`) as `sum` from radacct WHERE DATE(acctstarttime) = '.$this->context->nas->id, array());

		

		if ($volume) {
			return array('date'=>$date, 'total_volume'=>((((int)$volume[0]['output']) + (int)$volume[0]['input'])/1024)/1024);
		}
		return array('date'=>$date, 'total_volume'=>0);
	}

	public function actionDownloadCSV(){
		$date_from = Yii::app()->request->getPost('stat-from');
		$from = Yii::app()->request->getPost('stat-from');
		$date_to = Yii::app()->request->getPost('stat-to');

		$dates = array();
		
		while(date(strtotime($date_from)) < date(strtotime($date_to))){
			$dates[] = date('Y-m-d', strtotime($date_from.' +1 day'));
			$date_from = date('Y-m-d', strtotime($date_from.' +1 day'));
		}

		$nas = RadiusNas::model()->findByAttributes(array('id'=>$this->context->nas->id));

		// Loop through dates
		$data = array();
		$headers = array("Date", "Total Volume(MB)");
		array_push($data, $headers);
		foreach ($dates as $d) {
			$data[] = self::_getStats($d, $nas->switchip);
		}

		// die();
		// array_push($data, array("Monthly Total", $total, $logged_in));
		header("Content-Type: text/csv");
		header("Content-Disposition: attachment; filename=TotalVolumeReport-".$from."-".$date_to.".csv");
		// Disable caching
		header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
		header("Pragma: no-cache"); // HTTP 1.0
		header("Expires: 0"); // Proxies

		$output = fopen("php://output", "w");
	    foreach ($data as $row) {
	        fputcsv($output, $row); // here you can change delimiter/enclosure
	    }
	    fclose($output);
	}

}