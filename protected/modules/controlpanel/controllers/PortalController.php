<?php

class PortalController extends ControlpanelController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action)) {
			// action buttons
			$this->actionButtons['index'] = array('add' => array('link' => $this->createURL($this->id.'/add')));
			$this->actionButtons['add'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back' => array('link' => $this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		// fields to be displayed
		$row_fields = array();
		$row_fields['portal_id']['type'] = 'text';
		$row_fields['portal_id']['class'] = 'text-center col-md-1';
		
		$sites = Site::model()->findAll(array('order'=>'name ASC'));
		$row_fields['site_id']['type'] = 'select';
		$row_fields['site_id']['class'] = 'col-mid-2';
		$row_fields['site_id']['parent'] = 'site';
		$row_fields['site_id']['child'] = 'name';
		$row_fields['site_id']['value'] = CHtml::listData($sites, 'site_id', 'name');
		
		$row_fields['code']['type'] = 'text';
		$row_fields['code']['class'] = 'col-mid-2';
		
		$row_fields['name']['type'] = 'text';
		$row_fields['name']['class'] = 'col-mid-2';
		
		$row_fields['is_active']['type'] = 'select';
		$row_fields['is_active']['class'] = 'text-center col-md-1';
		$row_fields['is_active']['value'] = array('1'=>'Enabled', '0' =>'Disabled');
		$row_fields['is_active']['mode'] = 'bool';
		
		$row_actions = array('view', 'edit', 'delete');
		$object = new Portal();
		$list = self::_showList($object, $row_fields, $row_actions);
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$list, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
	}
	
	/**
	 * Display list
	 * 
	 * @param object $object Portal instance
	 * @param array $row_fields
	 * @param array $row_actions
	 * @return mixed
	 */
	private function _showList(Portal $object, $row_fields, $row_actions)
	{
		$object->attributes = Portal::setSessionAttributes(Yii::app()->request->getPost('Portal'));
		$criteria = $object->search()->criteria;
		$criteria->compare('site_id', $this->context->site->site_id);
		$count = Portal::model()->count($criteria);
		
		$pages = new CPagination($count);
		$criteria->limit = 25;
		$criteria->offset = (Yii::app()->request->getQuery('page', 1) * $criteria->limit) - $criteria->limit;
		
		$pages->pageSize=$criteria->limit;
		$pages->applyLimit($criteria);
		
		$list = Portal::model()->findAll($criteria);
		$view = $this->renderPartial('/layouts/list-body', array('object'=>$object, 'objectList'=>$list, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions), TRUE);
		if (Yii::app()->request->isAjaxRequest) {
			die(CJSON::encode(array('status'=>1, 'message'=>$view)));
		}
		return $view;
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id Portal primary key
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$portal = Portal::model()->findByPk($id);
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($portal);
		}
		
		self::_renderForm($portal);
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object Portal instance
	 * @return void
	 */
	private function _renderForm(Portal $object)
	{
		$fields = array();
		
		$fields['code']['type'] = 'text';
		$fields['code']['class'] = 'col-md-5';
		
		$fields['name']['type'] = 'text';
		$fields['name']['class'] = 'col-md-5';
		
		$fields['description']['type'] = 'text';
		$fields['description']['class'] = 'col-md-5';
		
		$fields['is_active']['type'] = 'radio';
		$fields['is_active']['class'] = 'col-lg-5';
		$fields['is_active']['value'] = array('1'=>'Enabled', '0'=>'Disabled');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object' => $object, 'fields' => $fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object Portal instance
	 * @return	void
	 */
	private function _postValidate(Portal $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('Portal');
		$object->attributes = $post;
		$object->site_id = $this->context->site->site_id;

		if ($object->validate() && $object->save()) {
			$this->log($object->{$object->tableSchema->primaryKey});
			self::displayFormSuccess();
		}
		self::displayFormError($object);
	}
}