<?php

class CmsController extends ControlpanelController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action))
		{
			// action buttons
			$this->actionButtons['index'] = array('add' => array('link' => $this->createURL($this->id.'/add')));
			$this->actionButtons['add'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back' => array('link' => $this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		// fields to be displayed
		$row_fields = array();
		$row_fields['Cms_ID']['type'] = 'text';
		$row_fields['Cms_ID']['class'] = 'text-center col-md-1';
		
		$row_fields['Title']['type'] = 'text';
		$row_fields['Title']['class'] = 'col-mid-4';
		
		$row_fields['Friendly_URL']['type'] = 'text';
		$row_fields['Friendly_URL']['class'] = 'col-mid-4';
		
		$row_actions = array('edit', 'delete');
		
		// List and pagination
		$object = new Cms;
		$object->attributes = Cms::setSessionAttributes(Yii::app()->request->getPost('Cms'));
		
		$criteria = $object->search()->criteria;
		$criteria->compare('Nas_ID', $this->context->nas->id);
		$count = Cms::model()->count($criteria);
		
		$pages = new CPagination($count);
		$criteria->limit = 25;
		$criteria->offset = (Yii::app()->request->getQuery('page', 1) * $criteria->limit) - $criteria->limit;
		
		$pages->pageSize=$criteria->limit;
		$pages->applyLimit($criteria);
		
		$cmss = Cms::model()->findAll($criteria);
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$cmss, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions, 'pages'=>$pages));
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		$cms = new Cms;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($cms);
		}
		
		self::_renderForm($cms);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id Cms primary key
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$cms = Cms::model()->findByPk($id);
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($cms);
		}
		
		self::_renderForm($cms);
	}
	
	/**
	 * Delete record
	 * 
	 * @access public
	 * @param int $id Cms ID
	 * @return void
	 */
	public function actionDelete($id)
	{
		$cms = Cms::model()->findByPk($id);
		Cms::validateObject($cms, 'Cms');
		
		if (Yii::app()->request->isPostRequest) {
			// check for security token
			if (CPasswordHelper::verifyPassword($cms->Cms_ID, Yii::app()->request->getPost('security_key'))) {
				// Delete permissions
				// ClientRolePermission::model()->deleteAllByAttributes(array('Cms_ID'=>$cms->Cms_ID));
				
				// Delete record
				Cms::model()->deleteByPk($cms->Cms_ID);
				
				Yii::app()->getModule('controlpanel')->client->setFlash('success', 'Delete successful!');
				$this->redirect(array("$this->id/"));
			}
			else {
				Yii::app()->getModule('controlpanel')->client->setFlash('fail', 'Error in deletion.');
				$this->redirect(array("$this->id/"));
			}
		}
		$this->render('delete', array('cms'=>$cms));
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object Cms instance
	 * @return void
	 */
	private function _renderForm(Cms $object)
	{
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/ckeditor/ckeditor.js'));
		
		$fields = array();
		
		$fields['Title']['type'] = 'text';
		$fields['Title']['class'] = 'col-md-5';
		
		$fields['Friendly_URL']['type'] = 'text';
		$fields['Friendly_URL']['class'] = 'col-md-5';
		
		$fields['Content']['type'] = 'wysiwyg';
		$fields['Content']['class'] = 'col-lg-8';
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object' => $object, 'fields' => $fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object Cms instance
	 * @return	void
	 */
	private function _postValidate(Cms $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('Cms');
		$object->attributes = $post;
		$object->Nas_ID = $this->context->nas->id;

		if ($object->validate())
		{
			if ($object->save()) {
				self::displayFormSuccess();
			}
		}
		self::displayFormError($object);
	}

}