<?php

class LoginController extends ControlpanelController
{
	// layout
	public $layout = '/layouts/login';
	
	public function actionIndex()
	{
		if (Yii::app()->request->isPostRequest) {
			if(Yii::app()->request->getPost('SiteAdmin')){
				self::_validateLogin();	
			}
			else if(Yii::app()->request->getPost('email')){
				self::_forgotPassword();
			}
		}
		$admin = new SiteAdmin();
		$this->render('index', array('admin'=>$admin));
	}
	
	private function _validateLogin()
	{
		$post = Yii::app()->request->getPost('SiteAdmin');
		
		if ($post) {
			sleep(1);
			
			$admin = new SiteAdmin;
			$admin->email = $post['email'];
			$admin->password = $post['password'];
			$admin->scenario = 'login';
			
			if ($admin->validate() AND $admin->login()) {
				$redirect = $this->createAbsoluteURL('dashboard/');
				self::_redirect($redirect);
			}
			else {
				self::displayFormError($admin);
			}
		}
	}

	private function _forgotPassword()
	{
		$post = rtrim(Yii::app()->request->getPost('email'));

		if(strlen(trim($post)) != strlen($post)){
			$redirect = $this->createAbsoluteURL('dashboard/');
			$json_array = array('status' => 0, 'message' => 'Invalid email.', 'redirect' => $redirect);
			die(CJSON::encode($json_array));
		}

		$admin = SiteAdmin::model()->findByAttributes(array('email'=>$post));

		if(!$admin){
			$redirect = $this->createAbsoluteURL('dashboard/');
			$json_array = array('status' => 0, 'message' => 'Account does not exist.', 'redirect' => $redirect);
			die(CJSON::encode($json_array));
		}

		$forgot_entry = new Forgotpassword;
		$forgot_entry->email = $post;
		$forgot_entry->Created_at = date('Y-m-d');
		$forgot_entry->save();
		
		if ($post) {
			sleep(1);

			$link = $this->createAbsoluteURL('login/forgot', array('q'=>utf8_encode(Yii::app()->getSecurityManager()->encrypt($post))));

			$to      = $post;
			$subject = 'WiFi CMS password Reset';
			$message = 'Reset password link: '. $link;

			$admin = new SiteAdmin;
			$admin->email = $post; 
			$admin->password = "";
			$admin->scenario = 'login';

			Yii::import('application.extensions.phpmailer.phpmailer.JPhpMailer');
			$mail = new JPhpMailer;  // create a new object

			$mail->IsSMTP();
			$mail->SMTPDebug = 0;  // debugging: 1 = errors and messages, 2 = messages only
			$mail->SMTPAuth = true;  // authentication enabled
			$mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
			$mail->Host = 'smtp.gmail.com';
			$mail->Port = 465; 
			$mail->Username = 'unifi.cadbury.yondu@gmail.com';
			$mail->password = 'unificadburyyondu2015';
			$mail->SetFrom('unifi.cadbury.yondu@gmail.com', 'Unifi');
			$mail->Subject = 'WiFi CMS password Reset';
			$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
			$mail->MsgHTML('<p>Reset password <a href=\''.$link.'\'>Link</a>.</p>');
			$mail->AddAddress($to);

			if(!$mail->Send()) {
				$error = 'Mail error: '.$mail->ErrorInfo; 
				return false;
			} else {
				$redirect = $this->createAbsoluteURL('dashboard/');
				self::_redirect($redirect);
			}
			
		}
	}

	private function _confirmpassword()
	{

		$email = Yii::app()->request->getPost('admin-email');
		$decrypted = Yii::app()->getSecurityManager()->decrypt(utf8_decode($email));
		$password = Yii::app()->request->getPost('new-password');

		if ($decrypted) {
			sleep(1);

			$admin = SiteAdmin::model()->findByAttributes(array('email'=>$decrypted));

			$admin->password = CpasswordHelper::hashpassword($password);
			$admin->save();
			
			$redirect = $this->createAbsoluteURL('dashboard/');
			self::_redirect($redirect);

		}
	}
	
	/**
	 * Handle page redirection if admin is authenticated
	 * 
	 * @access private
	 * @param string $redirect Default redirect page
	 * @return JSON
	 */
	private function _redirect($redirect)
	{
		if (Yii::app()->getModule('backoffice')->admin->getState('redirect')) {
			$redirect = Yii::app()->getModule('backoffice')->admin->getState('redirect');
		}

		// check if XMLHttpRequest request
		if (Yii::app()->request->getPost('ajax') OR Yii::app()->request->isAjaxRequest) {
			$json_array = array('status' => 1, 'message' => 'Logging in', 'redirect' => $redirect);
			die(CJSON::encode($json_array));
		}
	}

	public function actionForgot()
	{ 

		if (Yii::app()->request->isPostRequest) {
			if(Yii::app()->request->getPost('new-password')){
				self::_confirmpassword();
			}
		}

		$email = Yii::app()->request->getQuery('q');
		$decrypted = Yii::app()->getSecurityManager()->decrypt(utf8_decode($email));

		$entry = Forgotpassword::model()->findByAttributes(array('email'=>$decrypted), array('order'=>'Created_at DESC'));
		
		if($entry){
			if($entry->Expired_at){
				$redirect = $this->createAbsoluteURL('dashboard/');
				$this->render('forgot',  array('status' => 0, 'message' => 'Link has already expired.', 'redirect' => $redirect, 'email'=>$email));
			}else{
				$entry->Expired_at = date('Y-m-d');
				$entry->save();
				$this->render('forgot', array('email'=>$email));
			}
		}else{
			$redirect = $this->createAbsoluteURL('dashboard/');
			$this->render('forgot',  array('status' => 0, 'message' => 'Link has already expired.', 'redirect' => $redirect, 'email'=>$email));
		}

	}


}