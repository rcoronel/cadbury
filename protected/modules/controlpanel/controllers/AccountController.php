<?php

class AccountController extends ControlpanelController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action)) {
			// action buttons
			$this->actionButtons['index'] = array('add' => array('link' => $this->createURL($this->id.'/add')));
			$this->actionButtons['add'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back' => array('link' => $this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		// fields to be displayed
		$row_fields = array();
		$row_fields['Client_ID']['type'] = 'text';
		$row_fields['Client_ID']['class'] = 'text-center col-md-1';
		
		$roles = ClientRole::model()->findAll("Nas_ID = {$this->context->nas->id}");
		$row_fields['Client_role_ID']['type'] = 'select';
		$row_fields['Client_role_ID']['class'] = 'col-md-2';
		$row_fields['Client_role_ID']['value'] = array(1=>'Main Account') + CHtml::listData($roles, 'Client_role_ID', 'Role');
		$row_fields['Client_role_ID']['parent'] = 'role';
		$row_fields['Client_role_ID']['child'] = 'Role';
		
		$row_fields['Email']['type'] = 'email';
		$row_fields['Email']['class'] = 'col-mid-2';
		
		$row_fields['Firstname']['type'] = 'text';
		$row_fields['Firstname']['class'] = 'col-mid-1';
		
		$row_fields['Lastname']['type'] = 'text';
		$row_fields['Lastname']['class'] = 'col-mid-1';
		
		$row_fields['Is_active']['type'] = 'select';
		$row_fields['Is_active']['class'] = 'text-center col-md-1';
		$row_fields['Is_active']['value'] = array('1'=>'Can login', '0' =>'Cannot login');
		$row_fields['Is_active']['mode'] = 'bool';
		
		$row_actions = array('edit', 'delete');
		
		// List and pagination
		$object = new Client;
		$object->attributes = Client::setSessionAttributes(Yii::app()->request->getPost('Client'));
		
		$criteria = $object->search()->criteria;
		$criteria->compare('Nas_ID', $this->context->nas->id);
		$count = Client::model()->count($criteria);
		
		$pages = new CPagination($count);
		$criteria->limit = 25;
		$criteria->offset = (Yii::app()->request->getQuery('page', 1) * $criteria->limit) - $criteria->limit;
		
		$pages->pageSize=$criteria->limit;
		$pages->applyLimit($criteria);
		
		$clients = Client::model()->findAll($criteria);
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$clients, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions, 'pages'=>$pages));
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		$client = new Client;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($client);
		}
		
		self::_renderForm($client);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id Client ID
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$client = Client::model()->findByPk($id);
		Client::validateObject($client, 'Client');
		parent::checkNas($client->Nas_ID);
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($client);
		}
		
		self::_renderForm($client);
	}
	
	/**
	 * Delete record
	 * 
	 * @access public
	 * @param int $id Client ID
	 * @return void
	 */
	public function actionDelete($id)
	{
		$client = Client::model()->findByPk($id);
		Client::validateObject($client, 'Client');
		
		if ($client->Client_ID == $this->context->client->Client_ID) {
			Yii::app()->getModule('controlpanel')->client->setFlash('fail', 'Cannot delete currently logged in user.');
			$this->redirect(array("$this->id/"));
		}
		
		if (Yii::app()->request->isPostRequest) {
			// check for security token
			if (CPasswordHelper::verifyPassword($client->Client_ID, Yii::app()->request->getPost('security_key'))) {
				// Delete record
				Client::model()->deleteByPk($client->Client_ID);
				Yii::app()->getModule('controlpanel')->client->setFlash('success', 'Delete successful!');
				$this->redirect(array("$this->id/"));
			}
			else {
				Yii::app()->getModule('controlpanel')->client->setFlash('fail', 'Error in deletion.');
				$this->redirect(array("$this->id/"));
			}
		}
		$this->render('delete', array('client'=>$client));
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object Client instance
	 * @return void
	 */
	private function _renderForm(Client $object)
	{
		$fields = array();
		
		if ($object->Client_role_ID == 1) {
			$fields['Client_role_ID']['type'] = 'hidden';
		}
		else {
			$roles = ClientRole::model()->findAll("Nas_ID = {$this->context->nas->id}");
			$fields['Client_role_ID']['type'] = 'select';
			$fields['Client_role_ID']['class'] = 'col-md-5';
			$fields['Client_role_ID']['value'] = CHtml::listData($roles, 'Client_role_ID', 'Role');
		}
		
		
		$fields['Email']['type'] = 'email';
		$fields['Email']['class'] = 'col-md-5';
		
		$fields['Password']['type'] = 'password';
		$fields['Password']['class'] = 'col-md-5';
		
		$fields['Confirm_password']['type'] = 'password';
		$fields['Confirm_password']['class'] = 'col-md-5';
		
		$fields['Firstname']['type'] = 'text';
		$fields['Firstname']['class'] = 'col-md-5';
		
		$fields['Lastname']['type'] = 'text';
		$fields['Lastname']['class'] = 'col-md-5';
		
		$fields['Is_active']['type'] = 'radio';
		$fields['Is_active']['class'] = 'col-md-5';
		$fields['Is_active']['value'] = array('1' => 'Can login', '0' => 'Cannot login');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object' => $object, 'fields' => $fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object Client instance
	 * @return	void
	 */
	private function _postValidate(Client $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('Client');
		$object->attributes = $post;
		$object->Nas_ID = $this->context->nas->id;
		
		// Validate object
		if ($object->validate()) {
			// Forcing Confirm_password to be the same as Password
			// We do this in order to avoid redundant error on save
			$object->Password = CPasswordHelper::hashPassword($object->Password);
			$object->Confirm_password = $object->Password;
			if ($object->save()) {
				self::displayFormSuccess();
			}
		}
		self::displayFormError($object);
	}
}