<?php

class PermissionController extends ControlpanelController
{
	public function actionIndex()
	{
		$this->redirect(array('permission/update', 'id' => 1));
	}
	
	public function actionUpdate($id)
	{
		if (Yii::app()->request->isPostRequest) {
			self::_handleRequest();
		}
		$role = SiteRole::model()->findByPk($id);
		SiteRole::validateObject($role, 'SiteRole');
		
		$roles = array_merge(SiteRole::model()->findAllByAttributes(array('Site_role_ID'=>1)), SiteRole::model()->findAll("Site_ID = {$this->context->site->Site_ID}"));
		
		// menus
		$menus = SiteMenu::model()->findAll(array('order' => 'Position ASC'));
		$permissions = SiteRolePermission::model()->findAll("Site_role_ID = {$role->Site_role_ID}");
		
		foreach ($menus as $m) {
			foreach ($permissions as $p) {
				if ($m->Site_menu_ID == $p->Site_menu_ID) {
					$m->View = $p->View;
					$m->Add = $p->Add;
					$m->Edit = $p->Edit;
					$m->Delete = $p->Delete;
				}
			}
		}
		
		$this->render('form', array('roles' => $roles, 'role'=>$role, 'menus'=>$menus));
	}
	
	private function _handleRequest()
	{
		if (Yii::app()->request->getPost('Site_role_ID')) {
			$post = Yii::app()->request->getPost('client_menu');
			foreach ($post as $p) {
				$permission = new SiteRolePermission;
				
				// delete existing permissions
				$permission->deleteAllByAttributes(array('Site_role_ID'=>Yii::app()->request->getPost('Site_role_ID'), 'Site_menu_ID'=>$p['Site_menu_ID']));
				
				$permission->Site_role_ID = Yii::app()->request->getPost('Site_role_ID');
				$permission->Site_menu_ID = $p['Site_menu_ID'];
				$permission->View = (isset($p['View'])) ? $p['View'] : 0;
				$permission->Add = (isset($p['Add'])) ? $p['Add'] : 0;
				$permission->Edit = (isset($p['Edit'])) ? $p['Edit'] : 0;
				$permission->Delete = (isset($p['Delete'])) ? $p['Delete'] : 0;
				$permission->save();
			}
			Yii::app()->getModule('controlpanel')->client->setFlash('success', 'Permissions updated');
			$this->redirect(array("$this->id/update", 'id'=>Yii::app()->request->getPost('Site_role_ID')));
		}
	}
}