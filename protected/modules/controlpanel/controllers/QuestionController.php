<?php

class QuestionController extends ControlpanelController
{
        public $parent_id;
//        public $question_id;
        public $title;
        public $redirect_page;
        public $count;

        /**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action)){       
                        $this->parent_id = Yii::app()->request->getParam('parent_id') == null ?  Yii::app()->request->getParam('id') : Yii::app()->request->getParam('parent_id');
                        $this->redirect_page = $this->createURL($this->id.'/&id='.$this->parent_id);
                        // action buttons
                        $this->actionButtons['index'] = array(
                                'add' => array('link' => $this->createURL($this->id.'/add&id='.$this->parent_id)),
                                'back' => array('link' => $this->createURL('survey/'))
                            );
                        $this->actionButtons['view'] = array('add' => array('link' => $this->createURL($this->id.'/add')),'back' => array('link' => $this->createURL($this->id.'/')));
                        $this->actionButtons['add'] = array('back' => array('link' => $this->createURL($this->id.'/&id='.$this->parent_id)));
                        $this->actionButtons['update'] = array('back' => array('link' => $this->createURL($this->id.'/&id='.$this->parent_id)));
                        $this->actionButtons['delete'] = array('back' => array('link' => $this->createURL($this->id.'/&id='.$this->parent_id)));

                        return true;
		}
	}
        
        
	public function actionIndex($id)
	{
                if(!$survey = Survey::model()->findByPk($id)) {
                    $this->redirect(array('survey/'));
                }
                else {
                    $this->title = $survey->Title;
                    // declare scripts
                    Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'plugins', 'jquery.tablednd.js')));
                    Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/jquery.toastr.min.js'));

                    // fields to be displayed
                    $row_fields = array();
                    $row_fields['Survey_question_ID']['type'] = 'text';
                    $row_fields['Survey_question_ID']['class'] = 'text-center col-md-1';

                    $row_fields['Question']['type'] = 'text';
                    $row_fields['Question']['class'] = 'col-mid-4';

                    $row_fields['Is_required']['type'] = 'select';
                    $row_fields['Is_required']['class'] = 'text-center col-md-1';
                    $row_fields['Is_required']['value'] = array('1' => 'True', '0' => 'False');
                    $row_fields['Is_required']['mode'] = 'bool';

                    $row_fields['Position']['type'] = 'text';
                    $row_fields['Position']['class'] = 'text-center col-md-1 dragHandle';
                    
                    $row_fields['Updated_at']['type'] = 'text';
                    $row_fields['Updated_at']['class'] = 'col-md-2';
                    
                    $row_fields['answer_count']['type'] = 'text';
                
                    $row_actions = array('answer', 'edit', 'delete');
                    
                    // List and pagination
                    $object = new SurveyQuestion;
                    $object->attributes = SurveyQuestion::setSessionAttributes(Yii::app()->request->getPost('SurveyQuestion'));
                    
                    $criteria = $object->search()->criteria;
                    $criteria->compare('Survey_ID', $id);
                    $count = SurveyQuestion::model()->count($criteria);

                    $pages = new CPagination($count);
                    $criteria->select = array(
                        new CDbExpression("*"),
                        new CDbExpression("(SELECT COUNT(*) FROM survey_answer WHERE t.Survey_question_ID = Survey_question_ID) AS 'answer_count'")
                    );
                    $criteria->limit = 25;
                    $criteria->order = "Position ASC";
                    $criteria->offset = (Yii::app()->request->getQuery('page', 1) * $criteria->limit) - $criteria->limit;

                    $pages->pageSize=$criteria->limit;
                    $pages->applyLimit($criteria);
                    
                    $questions = SurveyQuestion::model()->findAll($criteria);
                    $this->count = count($questions);
                    
                    // FOR INCOMPLETE SHORTCUT LIST
                    $criteria->select = array(
                                new CDbExpression("Survey_question_ID"),
                                new CDbExpression("Survey_ID"),
                                new CDbExpression("Question"),
                    );
                    $criteria->addCondition("(SELECT COUNT(*) FROM survey_answer WHERE t.Survey_question_ID = Survey_question_ID) < 1");
                    $count_inc = SurveyQuestion::model()->findAll($criteria);
                    // END INCOMPLETE SHORTCUT LIST
                    
                    $this->render('/layouts/list', array('object' => $object, 'objectList'=>$questions, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions, 'pages'=>$pages, 'incomplete'=>$count_inc));
                }
                
	}
        
        // Add Form
	public function actionAdd()
	{
                $question = new SurveyQuestion;
                
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($question);
		}
		
		self::_renderForm($question);
	}
        
        // Delete Form
	public function actionDelete($parent_id, $id)
	{
               if($question = SurveyQuestion::model()->findByAttributes(array("Survey_ID"=>$parent_id, "Survey_question_ID"=>$id))) {
                        if (Yii::app()->request->isPostRequest) {
                                self::_postDelete($id);
                        }

                        self::_renderForm($question, FALSE, TRUE);
                }
                else {
                        $this->redirect(array('survey/'));
                }
	}
        
        // Update Form
	public function actionUpdate($parent_id, $id)
	{
            if($question = SurveyQuestion::model()->findByAttributes(array("Survey_ID"=>$parent_id, "Survey_question_ID"=>$id))) {
                        if (Yii::app()->request->isPostRequest) {
                                self::_postValidate($question);
                        }

                        self::_renderForm($question);
                }
                else {
                        $this->redirect(array('survey/'));
                }
	}
        
        // View Form
	public function actionView()
	{
		// declare scripts
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'plugins', 'jquery.tablednd.js')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/jquery.toastr.min.js'));
		
		// fields to be displayed
		$row_fields = array();
		$row_fields['Survey_question_ID']['type'] = 'text';
		$row_fields['Survey_question_ID']['class'] = 'text-center col-md-1';
		
		$row_fields['Question']['type'] = 'text';
		$row_fields['Question']['class'] = 'col-mid-4';
		
		$row_fields['Is_required']['type'] = 'select';
		$row_fields['Is_required']['class'] = 'text-center col-md-1';
		$row_fields['Is_required']['value'] = array('1' => 'True', '0' => 'False');
		$row_fields['Is_required']['mode'] = 'bool';
		
		$row_fields['Position']['type'] = 'text';
		$row_fields['Position']['class'] = 'text-center col-md-1 dragHandle';
                
                $fields['Updated_at']['text'] = 'text';
                $fields['Updated_at']['class'] = 'col-lg-5';

		$row_actions = array('view', 'edit', 'delete');
		
		$questions = SurveyQuestion::model()->findAll();
	
		$this->render('/layouts/list', array('object' => new SurveyQuestion, 'objectList'=>$questions, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
	}
        
        /**
	 * Render form
	 * 
	 * @access private
	 * @param object $object Survey instance
	 * @return void
	 */
	private function _renderForm(SurveyQuestion $object, $isSurvey = null, $isDeleteForm = null)
	{
		$fields = array();
                    
		$fields['Question']['type'] = 'wysiwyg';
		$fields['Question']['class'] = 'col-md-5';
		
//               $fields['Position']['type'] = 'text';
//		$fields['Position']['class'] = 'text-center col-md-1 dragHandle';
                
		$fields['Is_required']['type'] = 'radio';
		$fields['Is_required']['class'] = 'col-lg-5';
		$fields['Is_required']['value'] = array('1' => 'True', '0' => 'False');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;

		$this->render('/layouts/form', array('object' => $object, 'fields' => $fields, 'isSurvey'=>$isSurvey, 'isDeleteForm'=>$isDeleteForm));
	}
        
        /**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object SurveyQuestion instance
	 * @return	void
	 */
	private function _postValidate(SurveyQuestion $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('SurveyQuestion');
		$object->attributes = $post;
                $object->Survey_ID = $this->parent_id;
//                $object->Position = 0; // Comment out before deployment
                $object->Updated_at = date("Y-m-d H:i:s",strtotime(date("Y-m-d")));
                
		if ($object->validate())
		{
			if ($object->save()) {
                            	// add all access to main client
				
				self::displayFormSuccess();
			}
		}
		else {
			self::displayFormError($object);
		}
	}
        
        /**
	 * Delete the form entry
	 * 
	 * @access	public
	 * @param object $object SurveyQuestion instance
	 * @return	void
	 */
	private function _postDelete($id=null)
	{
		$question = SurveyQuestion::model()->findByPk($id);
                SurveyQuestion::validateObject($question, 'SurveyQuestion');
                $question->delete();
                $answer = SurveyAnswer::model()->deleteAllByAttributes(array("Survey_question_ID"=>$id));
                
                self::displayFormSuccess('Record successfully deleted!');
	}
        
        /**
	 * Update menu position
	 * 
	 * @access active
	 * @return string
	 */
	public function actionPosition()
	{
		$query = Yii::app()->request->getQuery('page-list');
		
		$menu = new SurveyQuestion;
		foreach ($query as $position => $id) {
			$menu->updateByPk($id, array('Position' => (int)$position + 1));
		}
		
		die('Repositioning successful. Refresh page to view changes');
	}
}