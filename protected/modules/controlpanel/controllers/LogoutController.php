<?php

class LogoutController extends ControlpanelController
{	
	public function actionIndex()
	{
		Yii::app()->getModule('controlpanel')->admin->logout();
		$this->redirect(array('login/'));
	}
}