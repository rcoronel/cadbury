<?php

class Report_userController extends ControlpanelController
{
	public function actionIndex()
	{
		if (Yii::app()->request->isPostRequest) {
			self::_getConnectionStats();
		}
		
		// declare scripts
		Yii::app()->clientScript->registerCssFile(Link::css_url(array('jquery.datetimepicker.css')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery/jquery.datetimepicker.js')));

		Yii::app()->clientScript->registerCssFile(Link::css_url(array('morris', 'morris.css')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'raphael', 'raphael-min.js')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'morris', 'morris.min.js')));
		//Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'plugins', 'jquery-scrolltofixed-min.js')));
		
		$years = array();
		for ($i = date('Y'); $i >= date('Y')-1; $i--) {
			$years[$i] = $i;
		}
		
		$this->render('index', array('years'=>$years));
	}
	
	private function _getConnectionStats()
	{
		$date_from = Yii::app()->request->getPost('date-from');
		$date_to = Yii::app()->request->getPost('date-to');
		
		while(date(strtotime($date_from)) < date(strtotime($date_to))){
			$dates[] = date('Y-m-d', strtotime($date_from.' +1 day'));
			$date_from = date('Y-m-d', strtotime($date_from.' +1 day'));
		}

		// Loop through dates
		$data = array();
		foreach ($dates as $d) {
			$data[] = self::_getStats($d);
		}
		
		die(CJSON::encode(array('chart_data' => $data)));
		//$view = Yii::app()->controller->renderPartial('list', array('transactions'=>$data), TRUE);
		
	}
	
	private function _getStats($date)
	{
		$counter = DeviceCount::model()->findByAttributes(array('Date'=>$date, 'Nas_ID'=>$this->context->nas->id));
		if ($counter) {
			return array('date'=>$counter->Date, 'count'=>$counter->Count);
		}
		return array('date'=>$date, 'count'=>0);
	}

	public function actionDownloadCSV(){
		$date_from = Yii::app()->request->getPost('stat-from');
		$date_to = Yii::app()->request->getPost('stat-to');
		
		$from = $date_from;
		while(date(strtotime($date_from)) < date(strtotime($date_to))){
			$dates[] = date('Y-m-d', strtotime($date_from.' +1 day'));
			$date_from = date('Y-m-d', strtotime($date_from.' +1 day'));
		}

		// Loop through dates
		$data = array();
		array_push($data, array("Date","Users"));
		$total = 0;
		foreach ($dates as $key => $d) {
			$data[] = self::_getStats($d);
			if($key>0){
				$total += $data[$key]["count"];
			}
		}
		// die();
		array_push($data, array("Monthly Total", $total));
		header("Content-Type: text/csv");
		header("Content-Disposition: attachment; filename=UsersReport-".$from."-".$date_to.".csv");
		// Disable caching
		header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
		header("Pragma: no-cache"); // HTTP 1.0
		header("Expires: 0"); // Proxies

		$output = fopen("php://output", "w");
	    foreach ($data as $row) {
	        fputcsv($output, $row); // here you can change delimiter/enclosure
	    }
	    fclose($output);
	}

}