<?php

class Report_trailController extends ControlpanelController
{

	public function actionIndex()
	{
		// // if (Yii::app()->request->isPostRequest) {
		$data = self::_getErrors();


	}
	
	private function _getConnectionAttempts()
	{

		$availments = NasAvailment::model()->findAllByAttributes(array('Nas_ID'=>$this->context->nas->id));

		// Loop through availments
		$data = array();
		foreach ($availments as $a) {
			$data[$a->Availment_ID] = self::_getStats($a->Availment_ID);
		}	

		return $data;
		
	}
	
	private function _getStats($id)
	{
	
		$attempts = ConnectionAttempt::model()->with('device')->count("Nas_ID = {$this->context->nas->id} AND Availment_ID = {$id} AND Status = 0");
		$success = ConnectionAttempt::model()->with('device')->count("Nas_ID = {$this->context->nas->id} AND Availment_ID = {$id} AND Status = 1");

		
		if ($attempts || $success) {
			return array('attempts'=>$attempts, 'success'=>$success);
		}
		return array('attempts'=>0, 'success'=>0);
	}

	private function _getCount($id)
	{
	
		$errors = ArubaLog::model()->count("Nas_ID = {$this->context->nas->id} AND Status = ".$id);
		
		if ($errors) {
			return array('errors'=>$errors);
		}
		return array('errors'=>0);
	}

	private function _getErrors()
	{

		$logs = ArubaLog::model()->findAll(array(
			'select'=>'t.Status, ae.description',
            'condition'=>'t.Status!=0 AND Nas_ID = '.$this->context->nas->id,
            'join'=>'LEFT JOIN aruba_xml_error ae on t.Status = ae.status',
	        // 'params'=>array(':status'=>0, ':nas_id'=>$this->context->nas->id),
	        ));

		$errors = Yii::app()->db_cp->createCommand()
					->select('*')
					->from('aruba_xml_error')
					->queryAll();

		$data = array();

		foreach($errors as $e){

			$count = $this->_getCount($e['status']);
			$e['count'] = $count['errors'];
			$data[$e['status']][] = $e;

		}

		if ($data) {
			return array('errors'=>$data);
		}
		return array('errors'=>0);
	}

}