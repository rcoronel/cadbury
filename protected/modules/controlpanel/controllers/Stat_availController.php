<?php

class Stat_availController extends ControlpanelController
{
	public function actionIndex()
	{
		if (Yii::app()->request->isPostRequest) {
			self::_getConnectionStats();
		}
		
		// declare scripts
		Yii::app()->clientScript->registerCssFile(Link::css_url(array('jquery.datetimepicker.css')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery/jquery.datetimepicker.js')));
		
		$this->render('index');
	}
	
	private function _getConnectionStats()
	{
		$date_from = Yii::app()->request->getPost('date-from');
		$date_to = Yii::app()->request->getPost('date-to');
		
		while(date(strtotime($date_from)) <= date(strtotime('now')) && date(strtotime($date_from)) <= date(strtotime($date_to))){
			$dates[] = date('Y-m-d', strtotime($date_from));
			$date_from = date('Y-m-d', strtotime($date_from.' +1 day'));
		}
		
		// Loop through dates
		$data = array();
		foreach ($dates as $d) {
			$data[$d] = self::_getStats($d);
		}

		$availments = Availment::model()->findAll();
		$view = $this->renderPartial('avail', array('data'=>$data, 'availments'=>$availments), TRUE);
		die(CJSON::encode(array('status'=>1, 'message'=>$view)));
		
	}
	
	private function _getStats($date)
	{
		$arr = array();
		$connection_count = DeviceConnection::poll($date, $this->context->nas->id);
		$arr['count'] = $connection_count;
		
		$availments = Availment::model()->findAll();
		foreach ($availments as $a) {
			$model = $a->Model;
			$model_count = $model::model()->count("Nas_ID = {$this->context->nas->id} AND DATE(Created_at) = '{$date}'");
			$arr[$a->Availment_ID] = $model_count;
		}
		
		return $arr;
	}

	public function actionDownloadCSV(){
		$date_from = Yii::app()->request->getPost('stat-from');
		$date_to = Yii::app()->request->getPost('stat-to');
		
		while(date(strtotime($date_from)) <= date(strtotime('now')) && date(strtotime($date_from)) <= date(strtotime($date_to))){
			$dates[] = date('Y-m-d', strtotime($date_from));
			$date_from = date('Y-m-d', strtotime($date_from.' +1 day'));
		}
		
		// Loop through dates
		$data = array();
		$availments = Availment::model()->findAll();

		$headers = array("Date", "Connections");

		foreach ($availments as $a) {
			array_push($headers, $a->Name);
		}

		array_push($data, $headers);

		foreach ($dates as $d) {
			$stats = array();
			array_push($stats, $d);
			$stat2 = array_merge($stats, self::_getStats($d));
			$data[$d] = $stat2;
		}

		header("Content-Type: text/csv");
		header("Content-Disposition: attachment; filename=Availments-".Yii::app()->request->getPost('stat-from')."-".Yii::app()->request->getPost('stat-to').".csv");
		// Disable caching
		header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
		header("Pragma: no-cache"); // HTTP 1.0
		header("Expires: 0"); // Proxies

		$output = fopen("php://output", "w");
	    foreach ($data as $row) {
	        fputcsv($output, $row); // here you can change delimiter/enclosure
	    }
	    fclose($output);
	}

}