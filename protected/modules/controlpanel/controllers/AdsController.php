<?php

class AdsController extends ControlpanelController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action)) {
			// action buttons
			$this->actionButtons['index'] = array('add' => array('link' => $this->createURL($this->id.'/add')));
			$this->actionButtons['add'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back' => array('link' => $this->createURL($this->id.'/')));

			return true;
		}
	}
	
	public function actionIndex()
	{
		$ads = Ads::model()->findAllByAttributes(array('Nas_ID'=>$this->context->nas->id));
		
		$this->render('index', array('ads'=>$ads));
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		$ad = new Ads;
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($ad);
		}
		
		$this->render('form', array('ad'=>$ad));
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id Ads ID
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$ad = Ads::model()->findByPk($id);
		Ads::validateObject($ad, 'Ads');
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($ad);
		}
		
		$this->render('form', array('ad'=>$ad));
	}
	
	/**
	 * delete record
	 * 
	 * @access public
	 * @param int $id Ads ID
	 * @return void
	 */
	public function actionDelete($id)
	{
		$ad = Ads::model()->findByPk($id);
		Ads::validateObject($ad, 'Ads');
		
		if ($ad->Nas_ID != $this->context->nas->id) {
			throw new CHttpException(500,Yii::t('yii','No permission on delete'));
		}
		$upload_path = dirname(Yii::app()->request->scriptFile).'/images/client/'.$this->context->nas->id.'/ads/';
		$filename = $ad->Image;
		@unlink($upload_path.$filename);
		
		// Delete record
		Ads::model()->deleteByPk($ad->Ads_ID);
		
		Yii::app()->getModule('controlpanel')->client->setFlash('success', 'Successfully deleted ads');
		$this->redirect(array("$this->id/"));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object Ads instance
	 * @return	void
	 */
	private function _postvalidate(Ads $object)
	{
		$previous_image = $object->Image;
		
		$post = Yii::app()->request->getPost('Ads');
		$object->attributes = $post;
		$object->Nas_ID = $this->context->nas->id;
		$object->Image = CUploadedFile::getInstance($object, 'Image');
		
		if ($object->validate()) {
			$upload_path = dirname(Yii::app()->request->scriptFile).'/images/client/'.$this->context->nas->id.'/ads/';
			if ( ! is_dir($upload_path)) {
				mkdir($upload_path, 0777, true);
			}
			
			if ($object->save()) {
				@unlink($upload_path.$previous_image);
				$filename = Yii::app()->securityManager->generateRandomString(16).'.'.$object->Image->extensionName;
				$object->Image->saveAs($upload_path.$filename);
				$object->Image = $filename;
				$object->save();
				
				chmod($upload_path.$filename, 0777);
			}
			
			Yii::app()->getModule('controlpanel')->client->setFlash('success', 'Successfully added ads');
			$this->redirect(array("$this->id/"));
		}
	}
}