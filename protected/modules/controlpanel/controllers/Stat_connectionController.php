<?php

class Stat_connectionController extends ControlpanelController
{
	public function actionIndex()
	{
		if (Yii::app()->request->isPostRequest) {
			self::_getConnectionStats();
		}
		
		// declare scripts
		Yii::app()->clientScript->registerCssFile(Link::css_url(array('morris', 'morris.css')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'raphael', 'raphael-min.js')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'morris', 'morris.min.js')));
		//Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'plugins', 'jquery-scrolltofixed-min.js')));
		
		$years = array();
		for ($i = date('Y'); $i >= date('Y')-1; $i--) {
			$years[$i] = $i;
		}
		
		$this->render('index', array('years'=>$years));
	}
	
	private function _getConnectionStats()
	{
		$year = Yii::app()->request->getPost('year');
		$month = Yii::app()->request->getPost('month');
		
		$dates = array(date("{$year}-{$month}-01"));
		
		if (date('m') == $month) {
			while(end($dates) < date('Y-m-d', strtotime('yesterday'))){
				$dates[] = date('Y-m-d', strtotime(end($dates).' +1 day'));
			}
		}
		else {
			while(end($dates) < date("Y-m-t", strtotime(end($dates)))){
				$dates[] = date('Y-m-d', strtotime(end($dates).' +1 day'));
			}
		}
		
		// Loop through dates
		$data = array();
		foreach ($dates as $d) {
			$data[] = self::_getStats($d);
		}
		
		die(CJSON::encode(array('chart_data' => $data)));
		//$view = Yii::app()->controller->renderPartial('list', array('transactions'=>$data), TRUE);
		
	}
	
	private function _getStats($date)
	{
		$counter = DeviceConnectionCount::model()->findByAttributes(array('Date'=>$date, 'Nas_ID'=>$this->context->nas->id));
		if ($counter) {
			return array('date'=>$counter->Date, 'count'=>$counter->Count, 'success'=>$counter->Success);
		}
		return array('date'=>$date, 'count'=>0, 'success'=>0);
	}

	public function actionDownloadCSV(){
		$year = Yii::app()->request->getPost('stat-year');
		$month = Yii::app()->request->getPost('stat-month');
		
		$dates = array(date("{$year}-{$month}-01"));
		
		if (date('m') == $month) {
			while(end($dates) < date('Y-m-d', strtotime('yesterday'))){
				$dates[] = date('Y-m-d', strtotime(end($dates).' +1 day'));
			}
		}
		else {
			while(end($dates) < date("Y-m-t", strtotime(end($dates)))){
				$dates[] = date('Y-m-d', strtotime(end($dates).' +1 day'));
			}
		}
		
		// Loop through dates
		$data = array();
		array_push($data, array("Date","Total","Logged In"));
		$total = 0;
		$logged_in = 0;
		foreach ($dates as $key => $d) {
			$data[] = self::_getStats($d);
			if($key>0){
				$total += $data[$key]["count"];
				$logged_in += $data[$key]["success"];
			}
		}
		array_push($data, array("Monthly Total", $total, $logged_in));
		header("Content-Type: text/csv");
		header("Content-Disposition: attachment; filename=Connections-".$year."-".$month.".csv");
		// Disable caching
		header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
		header("Pragma: no-cache"); // HTTP 1.0
		header("Expires: 0"); // Proxies

		$output = fopen("php://output", "w");
	    foreach ($data as $row) {
	        fputcsv($output, $row); // here you can change delimiter/enclosure
	    }
	    fclose($output);
	}

}