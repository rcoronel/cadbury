<?php

class DashboardController extends ControlpanelController
{
	public function actionIndex()
	{
		$this->render('index');
	}
	
	/**
	 * Get count
	 * 
	 * @access public
	 * @return json
	 */
	public function actionPoll()
	{
		$today = date('Y-m-d');
		$connection_count = DeviceConnection::pollAll($today, $this->context->nas->id);
		$authenticated_count = DeviceConnection::pollAuthAll($today, $this->context->nas->id);
		
		die(CJSON::encode(array('connection_count'=>number_format($connection_count), 'authenticated_count'=>number_format($authenticated_count))));
	}

	public function actionDownloadCSV(){
		
		$from = '2015-11-25';
		$to = '2015-11-26';
		// echo "wat";
		// echo "<pre>";
		$data = array();		
		array_push($data, array("Date Created", "Site", "Email", "Firstname", "Lastname", "Gender", "MSISDN", "Brand", "Scene", "Mac Address", "Authenticated", "SSID"));

		while (strtotime($from) <= strtotime($to)) {
			// echo $from;
			$mobile = AvailMobile::model()->with('dev_avail')->findAll(array(
							    'condition' => 'portal_id = :portal_id AND DATE(dev_avail.created_at) = :date_created',
							    'select' => '*',
							    'distinct' => true,
							    'params' => array(
							     	":portal_id" => 2,
							     	":date_created" => $from

								    ),
			));

			$fb = AvailFacebook::model()->with('dev_avail')->findAll("portal_id = 2 AND DATE(dev_avail.created_at) = '{$from}'");
			// print_r($mobile);
			// print_r($fb); 
			// die();
			foreach($mobile as $m){
				$md = $m->dev_avail;
				$brand = (strpos($m->msisdn,'817') !== false || strpos($m->msisdn,'977') !== false || strpos($m->msisdn,'905') !== false || strpos($m->msisdn,'906') !== false || strpos($m->msisdn,'915') !== false || strpos($m->msisdn,'916') !== false || strpos($m->msisdn,'917') !== false || strpos($m->msisdn,'926') !== false || strpos($m->msisdn,'927') !== false || strpos($m->msisdn,'935') !== false || strpos($m->msisdn,'936') !== false || strpos($m->msisdn,'937') !== false || strpos($m->msisdn,'945') !== false || strpos($m->msisdn,'975') !== false || strpos($m->msisdn,'976') !== false || strpos($m->msisdn,'994') !== false || strpos($m->msisdn,'995') !== false || strpos($m->msisdn,'996') !== false || strpos($m->msisdn,'997') !== false ? "Globe" : "Non-globe" );
				$portal = Portal::model()->find("portal_id = {$md->portal_id}");
				$scene = DeviceScenario::model()->find("availment_id = {$md->availment_id} AND portal_id = {$md->portal_id} AND device_id = {$md->device_id}");
				$device = Device::model()->find("device_id = {$md->device_id}");
				$ssid = DeviceConnection::getDCByDate(str_replace("-", "", $from), 2, $md->device_id);
				$email = "";
				$firstname = "";
				$lastname = "";
				$gender = "";

				// print_r($ssid); 
				
				
				foreach($fb as $f){
					$fd = $f->dev_avail;
					if($md->device_id == $fd->device_id){
						$email = $f->email;
						$firstname = $f->firstname;
						$lastname = $f->lastname;
						$gender = $f->gender;
					}
				}
				
				$data[] = array($md->created_at, $portal->description, $email, $firstname, $lastname, $gender, $m->msisdn, $brand, $scene->scene, $device->mac_address, ($m->is_validated == 1 ? "Authenticated" : "Not Authenticated"), $ssid['ssid']);
						
			}
			
			// increment $from
			$from = date('Y-m-d', strtotime($from . "+1 days"));
			
		}
		
		// print_r($dc_data); 
			// print_r($data); 
			// die();
		
		
		
		header("Content-Type: text/csv");
		header("Content-Disposition: attachment; filename=WendysStats_2015-11-13-25.csv");
		// Disable caching
		header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
		header("Pragma: no-cache"); // HTTP 1.0
		header("Expires: 0"); // Proxies

		$output = fopen("php://output", "w");
	    foreach ($data as $row) {
	        fputcsv($output, $row); // here you can change delimiter/enclosure
	    }
	    fclose($output);
	}

	/*public function actionDownloadCSV2(){
		
		// echo "wat";
		// echo "<pre>";

		$data = array();
		array_push($data, array("Date Created", "Site", "Email", "Firstname", "Lastname", "Gender", "MSISDN", "Brand", "Scene", "Mac Address"));
		// print_r($dc_data); 

		$fb = AvailFacebook::model()->with('dev_avail')->findAll(array(
							    'condition' => 'portal_id = :portal_id AND DATE(dev_avail.created_at) = :date_created',
							    'select' => '*',
							    'join' => ''
							    'distinct' => true,
							    'params' => array(
							     	":portal_id" => 2,
							     	":date_created" => '2015-11-09'
							    ),
		));
		$mobile = AvailMobile::model()->with('dev_avail')->findAll("portal_id = 2 AND DATE(dev_avail.created_at) = '2015-11-09'");
		// print_r($mobile);
		// print_r($fb); 
		// die();
		foreach($fb as $f){
			$fd = $f->dev_avail;
			
			$portal = Portal::model()->find("portal_id = {$fd->portal_id}");
			$scene = DeviceScenario::model()->find("availment_id = {$fd->availment_id} AND portal_id = {$fd->portal_id} AND device_id = {$fd->device_id}");
			$device = Device::model()->find("device_id = {$fd->device_id}");
			$email = $f->email;
			$firstname = $f->firstname;
			$lastname = $f->lastname;
			$gender = $f->gender;
		
			foreach($mobile as $m){
				$md = $m->dev_avail;
				if($md->device_id == $fd->device_id){
					$brand = (strpos($m->msisdn,'817') !== false || strpos($m->msisdn,'977') !== false || strpos($m->msisdn,'905') !== false || strpos($m->msisdn,'906') !== false || strpos($m->msisdn,'915') !== false || strpos($m->msisdn,'916') !== false || strpos($m->msisdn,'917') !== false || strpos($m->msisdn,'926') !== false || strpos($m->msisdn,'927') !== false || strpos($m->msisdn,'935') !== false || strpos($m->msisdn,'936') !== false || strpos($m->msisdn,'937') !== false || strpos($m->msisdn,'945') !== false || strpos($m->msisdn,'975') !== false || strpos($m->msisdn,'976') !== false || strpos($m->msisdn,'994') !== false || strpos($m->msisdn,'995') !== false || strpos($m->msisdn,'996') !== false || strpos($m->msisdn,'997') !== false ? "Globe" : "Non-globe" );
					
				}
			}
			if($scene == "returning"){
				$data[] = array($md->created_at, $portal->description, $email, $firstname, $lastname, $gender, $m->msisdn, $brand, $scene->scene, $device->mac_address);
			}
			
					
		}
		
		
		header("Content-Type: text/csv");
		header("Content-Disposition: attachment; filename=TrinomaStats_2015-11-09.csv");
		// Disable caching
		header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
		header("Pragma: no-cache"); // HTTP 1.0
		header("Expires: 0"); // Proxies

		$output = fopen("php://output", "w");
	    foreach ($data as $row) {
	        fputcsv($output, $row); // here you can change delimiter/enclosure
	    }
	    fclose($output);
	}*/


}