<?php

class ControlpanelModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'controlpanel.models.*',
			'controlpanel.components.*',
		));
		
		$this->setComponents(array(
			'errorHandler'=>array(
				'errorAction'=>'/controlpanel/default/error',
			),
			'admin'=>array(
				'class'=>'SiteAdminComponent',  
				'allowAutoLogin'=>true,				
				'loginUrl'=>Yii::app()->createUrl('/controlpanel/login'),
			),
		));
		
		Yii::app()->user->setStateKeyPrefix('_controlpanel');
	}

	public function beforeControllerAction($controller, $action)
	{
		if (parent::beforeControllerAction($controller, $action)) {
			// Context object
			$context = Context::getContext();
			
			switch ($controller->id) {
				case 'login':
					// Check if user is already logged in
					if (Yii::app()->getModule('controlpanel')->admin->getId()) {
						Yii::app()->controller->redirect(array('dashboard/'));
					}
				break;
			
				case 'logout':
				break;
			
				default:
					// Check if user is logged in
					if (Yii::app()->getModule('controlpanel')->admin->isGuest) {
						Yii::app()->getModule('controlpanel')->admin->setState('redirect', $controller->createUrl('/'.$controller->route,$_GET));
						Yii::app()->getModule('controlpanel')->admin->loginRequired();
					}

					$context->site_admin = SiteAdmin::model()->findByPk(Yii::app()->getModule('controlpanel')->admin->getId());
					$context->site = Site::model()->findByPk($context->site_admin->site_id);
					$context->menus = SiteMenu::getMainMenus();
					$context->submenus = SiteMenu::getSubMenus();
					
					foreach ($context->menus as $m) {
						$m->has_subpages = SiteMenu::model()->countByAttributes(array('parent_site_menu_id'=>$m->site_menu_id, 'display'=>1));
					}
					
					// Get the current page details
					$controller->menu = SiteMenu::model()->findByAttributes(array('classname'=>$controller->id));
					$controller->permissions = SiteRolePermission::model()->findByAttributes(array('site_role_id'=>$context->site_admin->site_role_id, 'site_menu_id'=>$controller->menu->site_menu_id));
				break;
			}
			return true;
		}
		else {
			return false;
		}
	}
}
