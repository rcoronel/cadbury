<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="Yondu">
		<title>Login</title>

		<?php echo CHtml::cssFile(Link::css_url('bootstrap/v3.3.5/bootstrap.min.css'));?>
		<?php echo CHtml::cssFile(Link::css_url('font-awesome/font-awesome.css'));?>
		<?php echo CHtml::cssFile(Link::css_url('controlpanel/style.css'));?>
		<?php echo CHtml::cssFile(Link::css_url('controlpanel/style-responsive.css'));?>
		<?php echo CHtml::scriptFile(Link::js_url('jquery/jquery-1.11.2.min.js'));?>
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		<script>
			var baseURL = "<?php echo Link::base_url()?>";
			var currentIndex = "<?php echo $this->createUrl($this->id.'/');?>";
			var currentURL = "<?php echo $this->createUrl('/'.$this->route,$_GET);?>";
			var token = "<?php echo Yii::app()->request->csrfToken;?>";
		</script>
	</head>

	<body>
	
		<?php echo $content;?>
		
		<?php echo CHtml::scriptFile(Link::js_url('bootstrap/v3.3.5/bootstrap.min.js'));?>
		<?php echo CHtml::scriptFile(Link::js_url('jquery/jquery.backstretch.min.js'));?>
		<script>
			$.backstretch("assets/img/login-bg.jpg", {speed: 500});
		</script>


	</body>
</html>
