<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="Yondu">
		<?php echo CHtml::cssFile(Link::css_url('bootstrap/v3.3.5/bootstrap.min.css'));?>
		<?php echo CHtml::cssFile(Link::css_url('font-awesome/font-awesome.css'));?>
		<?php echo CHtml::cssFile(Link::css_url('controlpanel/style.css'));?>
		<?php echo CHtml::cssFile(Link::css_url('controlpanel/style-responsive.css'));?>
		<?php echo CHtml::scriptFile(Link::js_url('jquery/jquery-1.11.2.min.js'));?>
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		<title><?php echo $this->menu->title;?> | WiFi Dashboard</title>
		<script>
			var baseURL = "<?php echo Link::base_url()?>";
			var currentIndex = "<?php echo $this->createUrl($this->id.'/');?>";
			var currentURL = "<?php echo $this->createUrl('/'.$this->route,$_GET);?>";
			var token = "<?php echo Yii::app()->request->csrfToken;?>";
		</script>
	</head>

  <body>

  <section id="container" >
      <!--header start-->
		<header class="header black-bg">
			<div class="sidebar-toggle-box">
				<div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
			</div>
			
            <a href="index.html" class="logo">
				
			</a>
			
            
            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
                    <li><a class="logout" href="<?php echo $this->createUrl('logout/');?>">Logout</a></li>
            	</ul>
            </div>
        </header>
      <!--header end-->
	  
	  <?php echo $content;?>
      
      <!--footer start-->
<!--      <footer class="site-footer">
          <div class="text-center">
              2014 - <?php echo date('Y');?> Yondu
              <a href="#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>-->
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
	<?php echo CHtml::scriptFile(Link::js_url('bootstrap/bootstrap.min.js'));?>
    <?php echo CHtml::scriptFile(Link::js_url('jquery/jquery.dcjqaccordion.2.7.js'));?>
	<?php echo CHtml::scriptFile(Link::js_url('jquery/jquery.scrollTo.min.js'));?>
	<?php echo CHtml::scriptFile(Link::js_url('jquery/jquery.nicescroll.js'));?>
	<?php echo CHtml::scriptFile(Link::js_url('controlpanel/common-scripts.js'));?>
  </body>
</html>
