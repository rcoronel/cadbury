<?php $this->beginContent('/layouts/main'); ?>

<aside>
	<div id="sidebar"  class="nav-collapse ">
		<ul class="sidebar-menu" id="nav-accordion">
			<p class="centered">
				<a href="<?php echo $this->createUrl("profile/");?>">
					
				</a>
			</p>
			<h5 class="centered"></h5>
			
			<?php foreach ($this->context->menus as $menu):?>
				<li class="<?php echo ($menu->has_subpages) ? 'sub-menu' : 'mt';?>">
					<a href="<?php echo ($menu->has_subpages) ? 'javascript:;' : $this->createUrl("{$menu->classname}/");?>" class="<?php echo ($this->getId()==$menu->classname OR $this->menu->parent_site_menu_id==$menu->site_menu_id) ? 'active' : '';?>">
						<?php if ($menu->img):?><i class="<?php echo $menu->img;?>"></i><?php endif;?>
						<span><?php echo $menu->title;?></span></span>
					</a>
					
					<?php if ($menu->has_subpages):?>
						<ul class="sub">
							<?php foreach ($this->context->submenus as $submenu):?>
								<?php if ($submenu->parent_site_menu_id == $menu->site_menu_id):?>
									<li class="<?php echo ($this->getId()==$submenu->classname) ? 'active' : '';?>">
										<a href="<?php echo $this->createUrl("{$submenu->classname}/");?>">
											<?php echo $submenu->title;?>
										</a>
									</li>
								<?php endif;?>
							<?php endforeach;?>
						</ul>
					<?php endif;?>
				</li>
			<?php endforeach;?>
		</ul>
	</div>
</aside>

<section id="main-content">
	<section class="wrapper site-min-height">
			<h2>
				<i class="fa fa-angle-right"></i> <?php echo $this->menu->title.(isset($this->title)? ' > '.$this->title : '');?>
				<div class="pull-right">
					<!-- ACTION BUTTONS -->
					<?php if ( ! empty($this->actionButtons)):?>
						<?php foreach($this->actionButtons as $action => $button):?>
							<?php if ($action === $this->action->id):?>
								<?php if (isset($button['add'])):?>
									<a class="btn btn-default" href="<?php echo $button['add']['link'];?>">
										<i class="fa fa-plus-circle"></i>
										Add
									</a>
									
									<?php if ((($this->menu->title == "Question" && (isset($this->count) && $this->count < 10)) || ($this->menu->title == "Survey" && isset($this->count) &&  $this->count < 20) || $this->menu->title == "Answer")):?>
											<a class="btn btn-default" href="<?php echo $button['add']['link'];?>">
													<i class="fa fa-plus-circle"></i>
													Add
											</a>
									<?php endif;?>
								<?php endif;?>

								<?php if (isset($button['back'])):?>
									<a class="btn btn-default" href="<?php echo $button['back']['link'];?>">
										<i class="fa fa-backward"></i>
										Back to list
									</a>
								<?php endif;?>

							<?php endif;?>
						<?php endforeach;?>
					<?php endif;?>
					<!-- /ACTION BUTTONS -->
				</div>
			</h2>
			<div class="row">
				<div class="col-lg-12">
					<?php if(Yii::app()->getModule('controlpanel')->admin->hasFlash('success')): ?>
						<div class="alert alert-success alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<i class="fa fa-info-circle"></i>&nbsp;
							<?php echo Yii::app()->getModule('controlpanel')->admin->getFlash('success'); ?>
						</div>
					<?php endif;?>
					<?php if(Yii::app()->getModule('controlpanel')->admin->hasFlash('fail')): ?>
						<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<i class="fa fa-info-circle"></i>&nbsp;
							<?php echo Yii::app()->getModule('controlpanel')->admin->getFlash('fail'); ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		<div class="row">
			<div class="col-lg-12">
				<?php echo $content;?>
			</div>
		</div>
	</section>
</section>
<?php $this->endContent(); ?>