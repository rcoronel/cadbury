<?php if ( ! empty($objectList)):?>

							<?php foreach ($objectList as $row):?>
								<tr id="<?php echo $row->{$object->tableSchema->primaryKey};?>">

									<?php foreach ($row_fields as $field => $attribute):?>
										<td class="<?php echo (isset($attribute['class'])) ? $attribute['class'] : ''; echo (($field == 'question_count' || $field == 'answer_count') &&  $row->{$field} == 0) ? "alert-danger": "";?>">
											
											<?php if ($field == 'Position'):?><i class="fa fa-arrows"></i> &nbsp;<?php endif;?>
											
											<?php if ($attribute['type'] == 'text'):?>
												<?php if (isset($attribute['parent']) && isset($attribute['child'])):?>
													<?php if ($row->{$attribute['parent']}):?>
														<?php echo $row->{$attribute['parent']}->{$attribute['child']};?>
													<?php endif;?>
												<?php else:?>
													<?php echo $row->{$field};?>
												<?php endif;?>
											<?php endif;?>
											
											<?php if ($attribute['type'] == 'select'):?>
												<?php if (isset($attribute['mode']) && $attribute['mode'] == 'bool'):?>
													<?php if ($row->$field):?>
														<span class="label label-success" title="<?php echo $attribute['value'][$row->$field];?>">
															<i class="fa fa-check"></i>
														</span>
													<?php else:?>
														<span class="label label-danger" title="<?php echo $attribute['value'][$row->{$field}];?>">
															<i class="fa fa-minus"></i>
														</span>
													<?php endif;?>
												<?php else:?>
													<?php echo $attribute['value'][$row->$field];?>
												<?php endif;?>
											<?php endif;?>
											
											<?php if ($attribute['type'] == 'image'):?>
												<?php if ($row->{$field}):?>
													<?php echo CHtml::image($attribute['url'].$row->{$field});?>
												<?php endif;?>
											<?php endif;?>
										</td>
									<?php endforeach;?>

									<?php if (isset($row_actions) && ! empty($row_actions)):?>
										<td class="text-center col-md-1">
											<div class="btn-group-action text-center">
												<div class="btn-group">
													<?php foreach ($row_actions as $action):?>
                                                                                                                        <?php if ($action == 'question'):?>
																<a href="<?php echo $this->createUrl('question/', array('id' => $row->{$object->tableSchema->primaryKey}));?>" title="View Questions" class="btn btn-default" <?php echo (isset($modalView) && $modalView) ? 'data-toggle="modal" data-target="#view-modal"' : '';?>>
																	<i class="fa fa-question"></i>
																</a>
															<?php endif;?>
                                                                                                                        <?php if ($action == 'answer'):?>
																<a href="<?php echo $this->createUrl('answer'.(isset($this->parent_id) ? '&parent_id='.$this->parent_id : ''). '/', array('id' => $row->{$object->tableSchema->primaryKey}));?>" title="View Answer" class="btn btn-default" <?php echo (isset($modalView) && $modalView) ? 'data-toggle="modal" data-target="#view-modal"' : '';?>>
																	<i class="fa fa-pencil"></i>
																</a>
															<?php endif;?>
															<?php if ($action == 'view'):?>
																<a href="<?php echo $this->createUrl($this->id.'/view', array('id' => $row->{$object->tableSchema->primaryKey}));?>" title="View" class="btn btn-default" <?php echo (isset($modalView) && $modalView) ? 'data-toggle="modal" data-target="#view-modal"' : '';?>>
																	<i class="fa fa-eye"></i>
																</a>
															<?php endif;?>
															<?php if ($action == 'edit'):?>
																<a href="<?php echo $this->createUrl($this->id.'/update'.(isset($this->parent_id) ? '&parent_id='.$this->parent_id: '').(isset($this->question_id) ? '&question_id='.$this->question_id: ''), array('id' => $row->{$object->tableSchema->primaryKey}));?>" title="Edit" class="btn btn-default">
																	<i class="fa fa-edit"></i>
																</a>
															<?php endif;?>
															<?php if ($action == 'delete'):?>
																<a href="<?php echo $this->createUrl($this->id.'/delete'.(isset($this->parent_id) ? '&parent_id='.$this->parent_id: '').(isset($this->question_id) ? '&question_id='.$this->question_id: ''), array('id' => $row->{$object->tableSchema->primaryKey}));?>" title="Delete" class="btn btn-default">
																	<i class="fa fa-trash-o"></i>
																</a>
															<?php endif; ?>
													<?php endforeach;?>
												</div>
											</div>
										</td>
									<?php endif;?>

								</tr>
							<?php endforeach;?>
						<?php else:?>
							<tr>
								<td colspan="<?php echo count($row_fields) + count($row_actions);?>">
									<i class="fa fa-exclamation-circle"></i>
									No records found.
								</td>
							</tr>
						<?php endif;?>