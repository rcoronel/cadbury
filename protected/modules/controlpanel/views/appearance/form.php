<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default" data-collapsed="0">
			<div class="panel-body">
				<?php echo CHtml::beginForm('', 'post', array('class'=>'form-horizontal form-groups-bordered', 'enctype' => 'multipart/form-data')); ?>
					<div class="form-group">
						<?php echo CHtml::label('Site Title', 'NasConfiguration[SITE_TITLE]', array('class' => 'control-label col-lg-3'));?>
						<div class="col-lg-5">
							<?php echo CHtml::textField('NasConfiguration[SITE_TITLE]', $site_title, array('class' => 'form-control', 'maxlength' => 64)); ?>
						</div>
					</div>
					<div class="form-group">
						<?php echo CHtml::label('Landing page message', 'NasConfiguration[LANDING_MESSAGE]', array('class' => 'control-label col-lg-3'));?>
						<div class="col-lg-8">
							<?php echo CHtml::textArea('NasConfiguration[LANDING_MESSAGE]', $landing_message, array('class' => 'form-control', 'rows'=>15));?>
							<script type="text/javascript">
								var editor = CKEDITOR.replace('NasConfiguration_LANDING_MESSAGE');
								editor.on( 'change', function( evt ) {
									// getData() returns CKEditor's HTML content.
									$('#NasConfiguration_LANDING_MESSAGE').html(evt.editor.getData());
								});
							</script>
						</div>
					</div>
					<div class="form-group">
						<?php echo CHtml::label('Wecome message', 'NasConfiguration[WELCOME_BACK]', array('class' => 'control-label col-lg-3'));?>
						<div class="col-lg-8">
							<?php echo CHtml::textArea('NasConfiguration[WELCOME_BACK]', $welcome, array('class' => 'form-control', 'rows'=>15));?>
							<script type="text/javascript">
								var editor = CKEDITOR.replace('NasConfiguration_WELCOME_BACK');
								editor.on( 'change', function( evt ) {
									// getData() returns CKEditor's HTML content.
									$('#NasConfiguration_WELCOME_BACK').html(evt.editor.getData());
								});
							</script>
						</div>
					</div>
					<div class="form-group">
						<?php echo CHtml::label('Redirect URL', 'NasConfiguration[REDIRECT_URL]', array('class' => 'control-label col-lg-3'));?>
						<div class="col-lg-5">
							<?php echo CHtml::textField('NasConfiguration[REDIRECT_URL]', $redirect_url, array('class' => 'form-control')); ?>
						</div>
					</div>
					<hr>
					<div class="form-group">
						<?php echo CHtml::label('Logo', 'LOGO', array('class' => 'control-label col-lg-3'));?>
						<div class="col-lg-5">
							<?php if ( ! empty($logo)):?>
								<a href="<?php echo Link::image_url("client/{$this->context->nas->id}/{$logo}");?>" target="_blank">
									<?php echo $logo;?>
								</a>
								<button type="button" class="btn btn-sm pull-right" onclick="window.location='<?php echo $this->createUrl($this->id.'/delete', array('config'=>'LOGO'));?>';">
									<i class="fa fa-times-circle"></i>
								</button>
							<?php endif;?>
							<?php echo CHtml::fileField('LOGO', '', array('class' => 'form-control')); ?>
						</div>
					</div>
					<div class="form-group">
						<?php echo CHtml::label('Inside Logo', 'INSIDE_LOGO', array('class' => 'control-label col-lg-3'));?>
						<div class="col-lg-5">
							<?php if ( ! empty($inside_logo)):?>
								<a href="<?php echo Link::image_url("client/{$this->context->nas->id}/{$inside_logo}");?>" target="_blank">
									<?php echo $inside_logo;?>
								</a>
								<button type="button" class="btn btn-sm pull-right" onclick="window.location='<?php echo $this->createUrl($this->id.'/delete', array('config'=>'INSIDE_LOGO'));?>';">
									<i class="fa fa-times-circle"></i>
								</button>
							<?php endif;?>
							<?php echo CHtml::fileField('INSIDE_LOGO', '', array('class' => 'form-control')); ?>
						</div>
					</div>
					<div class="form-group">
						<?php echo CHtml::label('Center Image', 'CENTER_IMAGE', array('class' => 'control-label col-lg-3'));?>
						<div class="col-lg-5">
							<?php if ( ! empty($center_image)):?>
								<a href="<?php echo Link::image_url("client/{$this->context->nas->id}/{$center_image}");?>" target="_blank">
									<?php echo $center_image;?>
								</a>
								<button type="button" class="btn btn-sm pull-right" onclick="window.location='<?php echo $this->createUrl($this->id.'/delete', array('config'=>'CENTER_IMAGE'));?>';">
									<i class="fa fa-times-circle"></i>
								</button>
							<?php endif;?>
							<?php echo CHtml::fileField('CENTER_IMAGE', '', array('class' => 'form-control')); ?>
						</div>
					</div>
					<div class="form-group">
						<?php echo CHtml::label('Favicon', 'FAVICON', array('class' => 'control-label col-lg-3'));?>
						<div class="col-lg-5">
							<?php if ( ! empty($favicon)):?>
								<a href="<?php echo Link::image_url("client/{$this->context->nas->id}/{$favicon}");?>" target="_blank">
									<?php echo $favicon;?>
								</a>
								<button type="button" class="btn btn-sm pull-right" onclick="window.location='<?php echo $this->createUrl($this->id.'/delete', array('config'=>'FAVICON'));?>';">
									<i class="fa fa-times-circle"></i>
								</button>
							<?php endif;?>
							<?php echo CHtml::fileField('FAVICON', '', array('class' => 'form-control')); ?>
						</div>
					</div>
					<div class="form-group">
						<?php echo CHtml::label('Splash Image', 'SPLASH_IMG', array('class' => 'control-label col-lg-3'));?>
						<div class="col-lg-5">
							<?php if ( ! empty($splash)):?>
								<a href="<?php echo Link::image_url("client/{$this->context->nas->id}/{$splash}");?>" target="_blank">
									<?php echo $splash;?>
								</a>
								<button type="button" class="btn btn-sm pull-right" onclick="window.location='<?php echo $this->createUrl($this->id.'/delete', array('config'=>'SPLASH_IMG'));?>';">
									<i class="fa fa-times-circle"></i>
								</button>
							<?php endif;?>
							<?php echo CHtml::fileField('SPLASH_IMG', '', array('class' => 'form-control')); ?>
						</div>
					</div>
					<div class="form-group">
						<?php echo CHtml::label('Footer Logo (Splash)', 'NasConfiguration[POWERED_WELCOME]', array('class' => 'control-label col-lg-3'));?>
						<div class="col-lg-5">
							<?php echo CHtml::dropDownList('NasConfiguration[POWERED_WELCOME]', $powered_welcome, array(1=>'Display', 0=>'Hide'), array('class'=>'form-control'));?>
						</div>
					</div>
					<div class="form-group">
						<?php echo CHtml::label('Footer Logo (Inside)', 'NasConfiguration[POWERED_INSIDE]', array('class' => 'control-label col-lg-3'));?>
						<div class="col-lg-5">
							<?php echo CHtml::dropDownList('NasConfiguration[POWERED_INSIDE]', $powered_inside, array(1=>'Display', 0=>'Hide'), array('class'=>'form-control'));?>
						</div>
					</div>
					<div class="form-group">
						<?php echo CHtml::label('CSS file', 'NasConfiguration[CSS_FILE]', array('class' => 'control-label col-lg-3'));?>
						<div class="col-lg-5">
							<?php if ( ! empty($css_file)):?>
								<a href="<?php echo Link::css_url("client/{$this->context->nas->id}/{$css_file}");?>" target="_blank">
									<?php echo $css_file;?>
								</a>
								<button type="button" class="btn btn-sm pull-right" onclick="window.location='<?php echo $this->createUrl($this->id.'/delete', array('config'=>'CSS_FILE'));?>';">
									<i class="fa fa-times-circle"></i>
								</button>
							<?php endif;?>
							<?php echo CHtml::textArea('NasConfiguration[CSS_FILE]', $css_contents, array('class' => 'form-control', 'rows'=>15)); ?>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</div>
				<?php echo CHtml::endForm(); ?>
			</div>
		</div>
	</div>
</div>