<div class="col-md-offset-2 col-md-8 col-lg-offset-3 col-lg-6">
	<div class="well profile" style="min-height: 350px;">
		<div class="col-sm-12">
			<div class="col-xs-12 col-sm-8">
				<h2><?php echo $user->Name;?></h2>
				<p>
					<strong>Profile: </strong>
					<a href="https://www.facebook.com/<?php echo $user->Fb_ID;?>" target="_blank">
						https://www.facebook.com/<?php echo $user->Fb_ID;?>
					</a>
				</p>
				<p>
					<strong>E-mail Address: </strong>
					<?php echo $user->Email;?>
				</p>
				<p>
					<strong>Gender: </strong>
					<?php echo $user->Gender;?>
				</p>
				<p>
					<strong>MAC Address: </strong>
					<?php echo $user->device->Mac_address;?>
				</p>
				<p>
					<strong>Logged-in Date: </strong>
					<?php echo date('F d, Y', strtotime($user->Created_at));?>
				</p>
			</div>
			<div class="col-xs-12 col-sm-4 text-center">
				<figure>
					<img src="http://www.localcrimenews.com/wp-content/uploads/2013/07/default-user-icon-profile.png" alt="" class="img-circle img-responsive">

				</figure>
			</div>
		</div>
	</div>
</div>