<div class="row">

	<div class="col-md-12">
		<?php if ($ad->hasErrors()):?>
			<div id="form-error" class="alert alert-danger">
				<?php echo CHtml::errorSummary($ad);?>
			</div>
		<?php endif;?>
		<div class="form-panel panel panel-default" data-collapsed="0">
			<div class="panel-body">
				<?php $form=$this->beginWidget('CActiveForm', array('htmlOptions' => array('class'=>'form-horizontal style-form', 'role' => 'form', 'method' => 'post', 'enctype'=>'multipart/form-data'))); ?>
					<div class="form-group">
						<?php echo $form->labelEx($ad, 'Title', array('class' => 'control-label col-lg-3'));?>
						<div class="col-md-5">
							<?php echo $form->textField($ad, 'Title', array('class'=>'form-control', 'placeholder'=>$ad->getAttributeLabel('Title')));?>
						</div>
					</div>
					<div class="form-group">
						<?php echo $form->labelEx($ad, 'Image', array('class' => 'control-label col-lg-3'));?>
						<div class="col-md-5">
							<?php echo $form->fileField($ad, 'Image', array('class'=>'form-control'));?>
						</div>
					</div>
					<div class="form-group">
						<?php echo $form->labelEx($ad, 'Url', array('class' => 'control-label col-lg-3'));?>
						<div class="col-md-5">
							<?php echo $form->textField($ad, 'Url', array('class'=>'form-control', 'placeholder'=>$ad->getAttributeLabel('Url')));?>
						</div>
					</div>
					<div class="form-group">
						<?php echo $form->labelEx($ad, 'Close_url', array('class' => 'control-label col-lg-3'));?>
						<div class="col-md-5">
							<?php echo $form->textField($ad, 'Close_url', array('class'=>'form-control', 'placeholder'=>$ad->getAttributeLabel('Close_url')));?>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</div>
				<?php $this->endWidget();?>
			</div>
		</div>
	</div>
</div>