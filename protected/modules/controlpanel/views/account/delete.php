<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-danger">
		<div class="panel-heading">
			<div class="panel-title">
				<h4>
					Delete 
					<strong><?php echo $client->Name;?></strong> 
					account (#<?php echo $client->Client_ID;?>)?
				</h4>
			</div>
		</div>
		<div class="panel-body">

			<p>Deleting this account will <strong>remove</strong> the account from our records.</p>

			<?php $form=$this->beginWidget('CActiveForm', array('htmlOptions' => array(	'class'=>'form-horizontal', 'role' => 'form', 'method' => 'post'))); ?>
				<?php echo CHtml::hiddenField('Client_ID', $client->Client_ID)?>
				<?php echo CHtml::hiddenField('security_key', CPasswordHelper::hashPassword($client->Client_ID));?>
				<div class="buttons">
					<button type="submit" class="btn btn-danger">Delete</button>
					<button type="button" class="btn btn-default" onclick="location.href = '<?php echo $this->createUrl("{$this->id}/");?>';">Cancel</button>
				</div>
			<?php $this->endWidget();?>
		</div>
	</div>
</div>