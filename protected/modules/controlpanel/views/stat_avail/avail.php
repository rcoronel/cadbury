<div class="content-panel">
	<table id="page-list" class="table tableDnD table-hover">
		<thead>
			<tr>
				<th class="col-md-3">Date</th>
				<th class="col-md-1">Connection</th>
				<?php foreach ($availments as $a):?>
					<th class="col-md-1"><?php echo $a->Name;?></th>
				<?php endforeach;?>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($data as $date=>$value):?>
				<tr>
					<td><?php echo date('F d, Y', strtotime($date));?></td>
					<td class="text-center"><?php echo $value['count'];?></td>
					<?php foreach ($availments as $a):?>
						<td class="text-center"><?php echo $value[$a->Availment_ID];?></td>
					<?php endforeach;?>
				</tr>
			<?php endforeach;?>

		</tbody>
	</table>
</div>