
<div class="row">
	<div class="col-sm-6 col-sm-offset-3 text-field">
		<div class="row">
			<div class="col-xs-12">
				<h3>Code Verification</h3>
			</div>
			<div class="col-xs-12 title-qa">
				<span>A 5-digit verification code has been sent to your mobile number.</span>
			</div>
			<div id="form-error" class="alert alert-danger col-sm-4 col-sm-offset-4" role="alert" style="display:none;"></div>
		</div>

		<?php $form=$this->beginWidget('CActiveForm', array('method'=>'post', 'id'=>'changenum-form')); ?>
            <div id="verification-phone-number-wrapper">
                <h1><?php echo $mobile->msisdn; ?></h1>
				<?php echo CHtml::hiddenField('availment_id', $this->self->availment_id);?>
				<?php echo CHtml::hiddenField('mobile_number', $mobile->msisdn);?>
				<?php echo CHtml::hiddenField('change_number', 1);?>
				<a href="#" id="change_num" class="link-text-colored">Not you? Change phone number.</a>
            </div>
        <?php $this->endWidget();?>

		<?php $form=$this->beginWidget('CActiveForm', array('method'=>'post', 'id'=>'resend-form')); ?>
			<div class="conditions-content-btn">
				<?php echo CHtml::hiddenField('availment_id', $this->self->availment_id);?>
				<?php echo CHtml::hiddenField('mobile_number', $mobile->msisdn);?>
				<?php echo CHtml::hiddenField('resend', 1);?>
				<?php echo CHtml::htmlButton('RESEND CODE', array('type'=>'submit', 'class'=>'btn btn-shadow btn-mobile-verification'));?>
			</div>
		<?php $this->endWidget();?>
		
		<?php $form=$this->beginWidget('CActiveForm', array('method'=>'post', 'id'=>'verify-form')); ?>
			<div class="form-group">
				
				<?php echo $form->textField($mobile, 'auth_code', array('class'=>'form-control', 'value'=>''));?>
				<?php echo CHtml::hiddenField('availment_id', $this->self->availment_id);?>
				<?php echo CHtml::hiddenField('mobile_number', $mobile->msisdn);?>
			</div>

			<div class="conditions-content-btn">
				<?php echo CHtml::htmlButton('NEXT', array('type'=>'submit', 'class'=>'s-btn serendra-primary-btn'));?>
				<?php echo CHtml::htmlButton('BACK', array('type'=>'button', 'class'=>'s-btn serendra-secondary-btn', 'onclick'=>"window.location='{$this->owner->createAbsoluteUrl('login/', array('connection_token'=>$this->owner->connection->connection_token))}'"));?>
			</div>
			
		<?php $this->endWidget();?>
	</div>
</div>
<div class="modal modal-wifi in" id="verify-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-sm">
		<div class="modal-content globe-modal-content">
			<div class="modal-header">
				<h4 class="modal-title sr-only" id="myModalLabel"></h4>
			</div>
			<div class="modal-body">
				<p></p>
			</div>
			<div class="modal-footer">
				<?php echo CHtml::htmlButton('Close', array('class'=>'s-btn serendra-primary-btn'));?>
				<div class="modal-btn-logo">
					<?php echo CHtml::image(Link::image_url('powered-by-globe-modal.png'), 'Globe');?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="loader-modal" data-backdrop="static" data-keyboard="false" tabindex="-1">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$('form#resend-form').on('submit', function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		// hide errors
		$('div#form-error').slideUp( 300 ).html();

		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");

		$.ajax( {
			url : formURL,
			type: "POST",
			data : postData,
			dataType: 'json',
			success:function(data, textStatus, jqXHR)
			{
				if ( ! data.status) {
					setTimeout(function() {
						$('#loader-modal').modal('hide');
						$('div#form-error').slideDown(300).delay(800).html(data.message);
						$('div#form-error').show();
					}, 900);
				}
				else {
					$('div#verify-modal .modal-body').html(data.message);
					$('#verify-modal').modal({backdrop: 'static', keyboard: false});
					setTimeout(function(){
						window.location.href = data.redirect;
					}, 3000);
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				//if fails     
			}
		});
		e.preventDefault();
	});
	
	$('form#verify-form').on('submit', function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		// hide errors
		$('div#form-error').slideUp( 300 ).html();

		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");

		$.ajax( {
			url : formURL,
			type: "POST",
			data : postData,
			dataType: 'json',
			success:function(data, textStatus, jqXHR)
			{
				if ( ! data.status) {
					setTimeout(function() {
						$('#loader-modal').modal('hide');
						$('div#form-error').slideDown(300).delay(800).html(data.message);
						$('div#form-error').show();
					}, 900);
				}
				else {
					$('#loader-modal').modal('hide');
					$('#verify-modal div.modal-body p').html(data.message);
					$('#verify-modal').modal({backdrop: 'static', keyboard: false});
					$('#verify-modal').on('click', function () {
						window.location.href = data.redirect;
					});
//					setTimeout(function(){
//						window.location.href = data.redirect;
//					}, 3000);
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				//if fails     
			}
		});
		e.preventDefault();
	});

	$('#change_num').on('click', function(){
		$('form#changenum-form').submit();
	});

	$('form#changenum-form').on('submit', function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		// hide errors
		$('div#form-error').slideUp( 300 ).html();

		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");

		$.ajax( {
			url : formURL,
			type: "POST",
			data : postData,
			dataType: 'json',
			success:function(data, textStatus, jqXHR)
			{
				if ( ! data.status) {
					setTimeout(function() {
						$('#loader-modal').modal('hide');
						$('div#form-error').slideDown(300).delay(800).html(data.message);
						$('div#form-error').show();
					}, 900);
				}
				else {
					// $('div#verify-modal .modal-body').html(data.message);
					// $('#verify-modal').modal({backdrop: 'static', keyboard: false});
					setTimeout(function(){
						window.location.href = data.redirect;
					}, 3000);
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				//if fails     
			}
		});
		e.preventDefault();
	});
</script>