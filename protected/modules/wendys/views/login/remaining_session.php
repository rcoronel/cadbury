<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 text-center  tos-adj">
		<?php echo str_replace('::user::', $this->user, PortalConfiguration::getValue('WELCOME_BACK'));?>
		<h2>
			You still have at least <?php echo (int)$minutes;?> 
			<?php echo ((int)$minutes > 1) ? 'minutes' : 'minute';?> of internet connection
		</h2>
		
		<?php echo CHtml::beginForm();?>
			<?php echo CHtml::hiddenField('availment_id', $availment->availment_id);?>
			<?php echo CHtml::htmlButton('CONTINUE', array('type'=>'submit', 'class'=>'btn btn-wifi terms-btn'));?>
		<?php echo CHtml::endForm();?>
	</div>
</div>

<script>
$(document).ready(function(){
	
	$('form').on('submit',function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		//e.preventDefault();
	});
});
</script>