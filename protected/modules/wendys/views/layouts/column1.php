<?php /* @var $this Controller */ ?>
<?php $this->beginContent('/layouts/main'); ?>

<section class="container" id="main-wrapper">
	<div class="bg-white shadow-effect">
		<div class="top-logo">
			<?php if ($this->inside_logo):?>
				<?php echo CHtml::image($this->inside_logo, $this->site_title);?>
			<?php endif;?>
		</div>

		<?php echo $content;?>

		<footer class="row powered-container">
			<?php if (PortalConfiguration::getvalue('POWERED_INSIDE')):?>
				<?php echo CHtml::image(Link::image_url('powered-by-globe.png'), 'Globe');?>
			<?php endif;?>
		</footer> 
	</div>
</section>

<?php $this->endContent(); ?>