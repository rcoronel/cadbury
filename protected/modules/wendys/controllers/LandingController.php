<?php

class LandingController extends CaptivePortal
{
	/**
	 * Declare variables
	 * 
	 * @access protected
	 * @param object $action CAction instance
	 * @return bool
	 */
	protected function beforeAction($action)
	{
		parent::beforeAction($action);
		
		return TRUE;
	}
	
	/**
	 * Show landing page
	 * 
	 * @access public
	 * @return void
	 */
	public function actionIndex()
	{
		// Use custom layout
		$this->layout = '/layouts/splash';
		
		$url = $this->createAbsoluteUrl('login/', array('connection_token'=>$this->connection->connection_token));
		$terms_url = $this->createAbsoluteUrl('terms/', array('connection_token'=>$this->connection->connection_token));
		$policy_url = $this->createAbsoluteUrl('policy/', array('connection_token'=>$this->connection->connection_token));
		
		$this->render('index', array('url'=>$url, 'terms_url'=>$terms_url, 'policy_url'=>$policy_url));
	}
}