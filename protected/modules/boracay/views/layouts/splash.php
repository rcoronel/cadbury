<?php /* @var $this Controller */ ?>
<?php $this->beginContent('/layouts/main'); ?>
	<div class="container">
		
		<?php echo $content;?>
		
	</div>

	<footer class="row powered-container">
		<?php if (PortalConfiguration::getvalue('POWERED_WELCOME')):?>
			<?php echo CHtml::image(Link::image_url('powered-by-globe-white-logo-Small.png'), 'Globe');?>
		<?php endif;?>
	</footer>

<?php $this->endContent(); ?>