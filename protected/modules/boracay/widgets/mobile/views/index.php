
<div class="row">
            <div class="col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
                <div class="default-wrapper">

                    <h1 class="captive-portal-heading">Mobile Registration</h1>
                    <!-- captive-portal-heading -->

                    <div class="captive-portal-sub-heading">
                        <p>Enter your 11-digit mobile number to start enjoying Globe's FREE Wi-Fi trial.</p>
                    </div>
                    <!-- captive-portal-sub-heading -->
                    <?php $form=$this->beginWidget('CActiveForm', array('method'=>'post', 'action'=>'','htmlOptions'=>array('class'=>'col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2',),)); ?>
                    <!-- <form class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2"> -->

                        <!--Error Message -->
                        <div id="form-error" class="error-notification animated bounceIn" role="alert"  style="display:none;">
                            <h5>Invalid Mobile Number!</h5>
                        </div>
                        <!--Error Message -->

                        <!--Mobile Number Input-->
                        <label for="mobileNumber">11-Digit Mobile Number</label>
                        <!-- <input type="tel" class="form-control number-only" id="mobileNumber" placeholder="09XXXXXXXXX"> -->
                        <?php echo $form->textField($object, 'msisdn', array('class'=>'form-control number-only', 'placeholder'=>'09XXXXXXXXX'));?>
						<?php echo CHtml::hiddenField('availment_id', $availment_id);?>
                        <div id="default-to-action-btn-wrapper">
                            <!-- <span class="input-submit-btn-wrapper"><input type="submit" class="btn primary-btn" value="Send Verification Code"></span> -->
                            <!-- <span class="input-submit-btn-wrapper"><input type="submit" class="btn secondary-btn" value="back"></span> -->
                        	<span ><?php echo CHtml::htmlButton('SEND VERIFICATION CODE', array('type'=>'submit', 'class'=>'btn primary-btn'));?></span>
							<span ><?php echo CHtml::htmlButton('BACK', array('type'=>'button', 'class'=>'btn secondary-btn', 'onclick'=>"window.location='{$this->owner->createAbsoluteUrl('login/', array('connection_token'=>$this->owner->connection->connection_token))}'"));?></span>
                        </div>
                    <?php $this->endWidget();?>
                    <!-- </form> -->
                    <!-- end-of-form -->
                    <footer>
                    	<?php echo CHtml::image(Link::image_url('powered-by-globe-modal.png'), 'Globe');?>
                       <!-- <img src="assets/image/powered-by-globe-modal.png" alt="Globe">  -->
                    </footer>
                </div> <!-- end of default-wrapper -->
            </div>
        </div>

<div class="modal modal-wifi in" id="loader-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-body small-modal-body-style">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$('form').on('submit', function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		// hide errors
		$('div#form-error').slideUp( 300 ).html();

		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");

		$.ajax( {
			url : formURL,
			type: "POST",
			data : postData,
			dataType: 'json',
			success:function(data, textStatus, jqXHR)
			{
				if ( ! data.status) {
					setTimeout(function() {
						$('#loader-modal').modal('hide');
						$('div#form-error').slideDown(300).delay(800).html(data.message);
						$('div#form-error').show();
					}, 900);
				}
				else {
					$('div.modal-body p').html(data.message);
					$('#loader-modal').modal({backdrop: 'static', keyboard: false});
					setTimeout(function(){
						window.location.href = data.redirect;
					}, 3000);
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				//if fails     
			}
		});
		e.preventDefault();
	});
</script>