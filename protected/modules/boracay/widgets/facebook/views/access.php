<div class="row">
    <div class="col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
        <div class="default-wrapper">
            <div class="captive-portal-sub-heading">
                <h4>
					<?php echo $message;?>
				</h4>
            </div>
			<br>
            <div class="conditions-content-btn">
				<?php echo CHtml::htmlButton('CONTINUE', array('class'=>'s-btn serendra-primary-btn', 'onclick'=>"window.location='{$url}'"));?>
			</div>

            <footer>
            	<?php echo CHtml::image(Link::image_url('powered-by-globe-modal.png'), 'Globe');?>
            </footer>
        </div>
    </div>
</div>