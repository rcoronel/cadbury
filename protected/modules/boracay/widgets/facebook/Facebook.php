<?php
//session_start();

require __DIR__.'/api/autoload.php';
use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphUser;
use Facebook\FacebookRedirectLoginHelper;

class Facebook extends CWidget
{
	public $self;
	public $self_portal;
	public $action;
	public $reference;
	
	private $app_id;
	private $app_secret;
	
	public function init()
	{
		// Set PortalAvailment instance
		$this->self_portal = PortalAvailment::model()->findByAttributes(array('portal_id'=>$this->owner->portal->portal_id, 'availment_id'=>$this->self->availment_id));
	}
	
	public function run()
	{
		// Set application variables
		self::_setApplication();
		
		switch ($this->action)
		{
			// Login or Register function
			case 'login':
				self::_login();
			break;
		
			// Display the buttons
			default:
				self::_display();
			break;
		}
	}
	
	/**
	 * Display the widget
	 * 
	 * @access private
	 * @return void
	 */
	private function _display()
	{
		// Check FB parameters
		if ( ! $this->app_id OR ! $this->app_secret) {
			throw new CHttpException(500, Yii::t('yii','Facebook API not configured properly.'));
		}
		
		$url = $this->owner->createAbsoluteUrl('login/',
			array(	'connection_token'=>$this->owner->connection->connection_token,
					'availment_id'=>$this->self->availment_id));

		$this->render('login', array('availment_id'=>$this->self->availment_id, 'url'=>$url));
	}
	
	/**
	 * Login or Register the user to the NAS
	 * 
	 * @access private
	 * @return void
	 */
	private function _login()
	{
		// Check if already availed for today
		$is_availed = DeviceAvailment::model()->exists("device_id = {$this->owner->device->device_id} AND portal_id = {$this->owner->portal->portal_id} AND availment_id = {$this->self->availment_id} AND DATE(created_at) = DATE(NOW())");
		if ($is_availed) {
			$this->owner->redirect(array('login/', 'connection_token'=>$this->owner->connection->connection_token));
		}
		
		// check for cancellation of user
		if (Yii::app()->request->getParam('error')) {
			$this->owner->redirect(array('login/', 'connection_token'=>$this->owner->connection->connection_token));
		}
		
		// Give temporary access only if source is not FB
		if ( ! Yii::app()->request->getParam('code')) {
			$this->owner->giveTempAccess();
			$this->owner->changeRole('Cadbury-Cloud-logon-fb');
		}
		
		session_start();
		FacebookSession::setDefaultApplication($this->app_id, $this->app_secret);
		
		$url = $this->owner->createAbsoluteUrl('login/',
			array(	'connection_token'=>$this->owner->connection->connection_token,
					'availment_id'=>$this->self->availment_id, 'action'=>'login'));
		$url = str_replace('/login', '%2Flogin', $url);
		
		$helper = new FacebookRedirectLoginHelper($url);
		
		try {
			$session = $helper->getSessionFromRedirect();
		}
		catch(FacebookRequestException $ex) {
			throw new CHttpException(400,Yii::t('yii',$ex));
		}
		catch(\Exception $ex) {
			throw new CHttpException(400,Yii::t('yii',$ex));
		}
			
		// If session is set
		if (isset($session) && $session) {
			$user_profile = (new FacebookRequest($session, 'GET', '/me'))
				->execute()
				->getGraphObject(GraphUser::className());
			
			if ( ! empty($user_profile)) {
				// New instance of DeviceAvailment
				$dev_avail = $this->owner->saveDeviceAvailment($this->self->availment_id);
				
				if ($dev_avail->validate()) {
					// AvailFacebook instance
					$object = AvailFacebook::model()->with('dev_avail')->find("fb_id = {$user_profile->getProperty('id')} AND device_id = {$this->owner->device->device_id} AND portal_id = {$this->owner->portal->portal_id}");
					if (empty($object)) {
						$object = new AvailFacebook;
					}
					$object->fb_id = $user_profile->getProperty('id');
					$object->email = $user_profile->getProperty('email');
					$object->firstname = $user_profile->getProperty('first_name');
					$object->lastname = $user_profile->getProperty('last_name');
					$object->gender = $user_profile->getProperty('gender');
					$object->access_token = $session->getAccessToken();
					$object->access_expiry = $session->getSessionInfo()->getExpiresAt()->format('Y-m-d H:i:s');
					
					if($object->validate()) {
						// Update connection and save to DB
						if (self::_updateConnection($object, $this->self_portal) && $dev_avail->save()) {
							// Save AvailFacebook
							$object->device_availment_id = $dev_avail->device_availment_id;
							$object->save();
							
							// Correlor API calls
							Correlor::userLogin($object->avail_facebook_id);
							$data = array('actionType'=>'access_point_login',
								'itemId'=>$this->owner->connection->location_id,
								'actiontime'=>date('YmdHis+0800'),
								'details'=>array(array('name'=>'max_session_time', 'value'=>$this->self_portal->duration/60)));
							Correlor::reportAction($this->owner->device->device_id, 'access_point_login', $data);
							
							$minutes = $this->self_portal->duration / 60;
							$message = "You can use Globe Wifi, FREE trial for {$minutes} minutes today! Happy Surfing.";
							$url = $this->owner->createAbsoluteUrl('access/', array('connection_token'=>$this->owner->connection->connection_token,
								'availment_id'=>$this->self->availment_id));
							$this->render('access', array('message'=>$message, 'url'=>$url));
						}
					}
				}
			}
			else {
				throw new CHttpException(400, Yii::t('yii','Cannot retrieve Facebook profile'));
			}
		}
		else {
			$loginUrl = $helper->getLoginUrl(array('req_perms' => 'email, user_about_me, user_birthday, user_likes, user_education_history,'
				. 'user_hometown, user_location, user_relationships, user_photos,'
				. 'user_tagged_places, user_status, user_work_history, user_games_activity, user_events'));
			//$loginUrl = $helper->getLoginUrl();
			header("Location: ".$loginUrl);
			exit();
		}
	}
	
	/**
	 * Update connection details
	 * 
	 * @access private
	 * @param object $object AvailRegistration instance
	 * @param object $availment PortalAvailment instance
	 * @return boolean
	 */
	private function _updateConnection(AvailFacebook $object, PortalAvailment $availment)
	{
		$minutes = $availment->duration/60;
		$connection = $this->owner->connection;
		$connection->username = $this->owner->device->mac_address.'@'.$this->owner->portal->code;
		$connection->password = $object->fb_id;
		
		if ($connection->validate() && $connection->save()) {
			// Correlor log
			$data = array('actionType'=>'access_point_login',
							'itemId'=>$this->owner->connection->location_id,
							'actiontime'=>date('YmdHis+0800'),
							'details'=>array(array('name'=>'max_session_time', 'value'=>$minutes)));
			Correlor::reportAction($this->owner->device->device_id, 'access_point_login', $data);
			
			// Verify connection
			if ($this->owner->connect($availment, TRUE)) {
				return TRUE;
			}
			
		}
		throw new CHttpException(500, Yii::t('yii','Cannot update connection. Please try again.'));
	}
	
	/**
	 * Set application variables
	 * 
	 * @access private
	 * @return void
	 */
	private function _setApplication()
	{
		$this->app_id = AvailmentSetting::getValue($this->self->availment_id, 'FB_APP_ID');
		$this->app_secret = AvailmentSetting::getValue($this->self->availment_id, 'FB_SECRET_KEY');
	}
}