<?php

class AccessController extends CaptivePortal
{
	public function actionIndex()
	{
		$availment_id = Yii::app()->request->getQuery('availment_id');
		
		// Check if there is a connection made
		$connect = ConnectionAvail::model()->findByAttributes(array('device_connection_id'=>$this->connection->device_connection_id, 'availment_id'=>$availment_id));
		if (empty($connect)) {
			throw new CHttpException(400,Yii::t('yii','No connection made. Please try again.'));
		}
		
		// If redirect URL is given, redirect after authentication
		if (PortalConfiguration::getValue('REDIRECT_URL')) {
			$this->redirect(PortalConfiguration::getValue('REDIRECT_URL'));
		}
		
		// Check if the portal contain ads
		$criteria = new CDbCriteria();
		$criteria->compare('portal_id',$this->portal->portal_id);
		$criteria->order="RAND()";
		$criteria->limit = 1;
		$ads = Ads::model()->find($criteria);
		$ads = array();
		if ( ! empty($ads)) {
			self::_showAds($ads);
		}
		else {
			$this->render('index');
		}
	}
	
	private function _showAds($ads = array())
	{
		$this->layout = '//layouts/ads';
		$this->render('ads', array('ads'=>$ads));
	}
}