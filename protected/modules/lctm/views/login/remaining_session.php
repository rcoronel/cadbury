<div class="base-content">
	<div class="form-block-wrapper">
		<h4>
			You still have at least <?php echo (int)$minutes;?> 
			<?php echo ((int)$minutes > 1) ? 'minutes' : 'minute';?> of internet connection
		</h4>
		
		<?php echo CHtml::beginForm();?>
			<?php echo CHtml::hiddenField('availment_id', $availment->availment_id);?>
			<?php echo CHtml::htmlButton('CONTINUE', array('type'=>'submit', 'class'=>'primary-btn'));?>
		<?php echo CHtml::endForm();?>
	</div>
</div>

<script>
$(document).ready(function(){
	
	$('form').on('submit',function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		//e.preventDefault();
	});
});
</script>