<div class="row" id="survey-container">
    <div class="col-sm-6 col-sm-offset-3 text-field">
		<div class="row">
			<div class="col-xs-12">
				<h3><?php echo $survey->title;?></h3>
			</div>
			<div id="form-error" class="alert alert-danger col-sm-4 col-sm-offset-4" role="alert" style="display:none;"></div>
			<div class="col-xs-12 title-qa">
				
			</div>
		</div>
		<?php $form=$this->beginWidget('CActiveForm', array('id'=>'survey-form', 'method'=>'post')); ?>
			<?php echo CHtml::hiddenField('survey_id', $survey->survey_id);?>
			<?php foreach ($questions as $q):?>
				<div class="form-group">
					<?php echo $q->question;?>
					<?php foreach ($answers as $question_id=>$types):?>
						<?php if ($question_id == $q->survey_question_id):?>
							<?php foreach ($types as $type=>$as):?>
								<?php if ( ! empty($as)):?>
									<?php if ($type == 'text'):?>
										<?php foreach ($as as $a):?>
											<?php echo CHtml::textField("question[$q->survey_question_id][$a->survey_answer_id]", '', array('class'=>'form-control', 'placeholder'=>$a->answer));?>
										<?php endforeach;?>
									<?php endif;?>
									<?php if ($type == 'dropdown'):?>
										<?php echo CHtml::dropDownList("question[$q->survey_question_id][]", '', CHtml::listData($as, 'answer', 'answer'), array('class'=>'form-control', 'prompt'=>'---'));?>
									<?php endif;?>
									<?php if ($type == 'checkbox'):?>
										<br>
										<?php echo CHtml::hiddenField("question[$q->survey_question_id]");?>
										<?php echo CHtml::checkBoxList("question[$q->survey_question_id][]", '', CHtml::listData($as, 'answer', 'answer'));?>
									<?php endif;?>
									<?php if ($type == 'radio'):?>
										<br>
										<?php echo CHtml::hiddenField("question[$q->survey_question_id]");?>
										<?php echo CHtml::radioButtonList("question[$q->survey_question_id][]", '', CHtml::listData($as, 'answer', 'answer'));?>
									<?php endif;?>
								<?php endif;?>
							<?php endforeach;?>
						<?php endif;?>
					<?php endforeach;?>
				</div>
			<?php endforeach;?>
			<div class="conditions-content-btn">
				<?php echo CHtml::htmlButton('SUBMIT', array('type'=>'submit', 'class'=>'s-btn serendra-primary-btn'));?>
				<?php echo CHtml::htmlButton('BACK', array('type'=>'button', 'class'=>'s-btn serendra-secondary-btn', 'onclick'=>"window.location='{$this->owner->createAbsoluteUrl('login/', array('connection_token'=>$this->owner->connection->connection_token))}'"));?>
			</div>
		<?php $this->endWidget();?>
    </div>
</div>

<div class="modal modal-wifi in" id="verify-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-sm">
		<div class="modal-content globe-modal-content">
			<div class="modal-header">
				<h4 class="modal-title sr-only" id="myModalLabel"></h4>
			</div>
			<div class="modal-body">
				<p></p>
			</div>
			<div class="modal-footer">
				<?php echo CHtml::htmlButton('Close', array('class'=>'s-btn serendra-primary-btn'));?>
				<div class="modal-btn-logo">
					<?php echo CHtml::image(Link::image_url('powered-by-globe-modal.png'), 'Globe');?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="loader-modal" data-backdrop="static" data-keyboard="false" tabindex="-1">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
    $('form').on('submit', function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		// hide errors
		$('div#form-error').slideUp( 300 ).html();

		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");

		$.ajax( {
			url : formURL,
			type: "POST",
			data : postData,
			dataType: 'json',
			success:function(data, textStatus, jqXHR)
			{
				if ( ! data.status) {
					setTimeout(function() {
						$('#loader-modal').modal('hide');
						$('div#form-error').slideDown(300).delay(800).html(data.message);
						$('div#form-error').show();
					}, 900);
				}
				else {
					$('#loader-modal').modal('hide');
					$('#verify-modal div.modal-body p').html(data.message);
					$('#verify-modal').modal({backdrop: 'static', keyboard: false});
					$('#verify-modal').on('click', function () {
						window.location.href = data.redirect;
					});
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				//if fails     
			}
		});
		e.preventDefault();
	});
</script>