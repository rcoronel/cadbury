<?php

class GatewayModule extends CWebModule
{
	public $defaultController = 'dashboard';
	
	public function init()
	{
		$this->setImport(array(
			'gateway.models.*',
			'gateway.components.*',
		));
		
		$this->setComponents(array(
			'errorHandler'=>array(
				'errorAction'=>'/gateway/default/error',
			),
			'admin' => array(
				'class' => 'PortalAdminComponent',  
				'allowAutoLogin' => true,				
				'loginUrl' => Yii::app()->createUrl('/gateway/login'),
			),
		));
		
		Yii::app()->user->setStateKeyPrefix('_gateway');
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// Context object
			$context = Context::getContext();
			
			switch ($controller->id) {
				case 'login':
					// Check if user is already logged in
					if (Yii::app()->getModule('gateway')->admin->getId()) {
						Yii::app()->controller->redirect(array('dashboard/'));
					}
				break;
			
				case 'logout':
				break;
			
				default:
					// Check if user is logged in
					if (Yii::app()->getModule('gateway')->admin->isGuest) {
						Yii::app()->getModule('gateway')->admin->setState('redirect', $controller->createUrl('/'.$controller->route,$_GET));
						Yii::app()->getModule('gateway')->admin->loginRequired();
					}

					$context->portal_admin = PortalAdmin::model()->findByPk(Yii::app()->getModule('gateway')->admin->getId());
					$context->portal = Portal::model()->findByPk($context->portal_admin->portal_id);
					$context->menus = PortalMenu::getMainMenus();
					$context->submenus = PortalMenu::getSubMenus();
					
					foreach ($context->menus as $m) {
						$m->has_subpages = PortalMenu::model()->countByAttributes(array('parent_portal_menu_id' => $m->portal_menu_id, 'display'=>1));
					}
					
					// Get the current page details
					$controller->menu = PortalMenu::model()->findByAttributes(array('classname'=>$controller->id));
					$controller->permissions = PortalRolePermission::model()->findByAttributes(array('portal_role_id' => $context->portal_admin->portal_role_id, 'portal_menu_id' => $controller->menu->portal_menu_id));
				break;
			}
			return true;
		}
		else
			return false;
	}
}
