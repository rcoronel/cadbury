<div class="row">
	<div class="span9">
		<?php echo CHtml::beginForm('', 'post', array('id'=>'stat-form', 'class'=>'form-horizontal form-groups-bordered'));?>
		<div class="control-group">
			<label class="span3 control-label">
				<strong>Statistics for &nbsp;</strong>
			</label>
			<div class="">
				<div class="controls">
					<div class="input-append">
						<input name="date" type="text" class="span2 datepicker" data-format="D, MM dd, yyyy" value="<?php echo date('D, F d, Y');?>">
						<span class="add-on"><i class="icon-calendar"></i></span>
					</div>
					<button class="btn btn-blue" type="submit">Show</button>
				</div>
			</div>
		</div>
	<?php echo CHtml::endForm();?>
	</div>
	
</div>
<div class="row">
	<div class="span6">
		<div class="widget">
			<div class="widget-header">
				<i class="icon-bar-chart"></i>
				<h3>Aruba API Logs</h3>
			</div>
			<div class="widget-content">
				<div id="aruba-chart">
					
				</div>
			</div>
		</div>
	</div>
	<div class="span6">
		<div class="widget widget-nopad">
			<div class="widget-header"> <i class="icon-list-alt"></i>
				<h3>Stats for authenticated users</h3>
			</div>
			<div class="widget-content">
				<div class="widget big-stats-container">
					<div class="widget-content">
						<h6 class="bigstats"></h6>
						<div id="big_stats" class="cf authenticated_users"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="span6">
		<div class="widget">
			<div class="widget-header">
				<i class="icon-bar-chart"></i>
				<h3>Availments</h3>
			</div>
			<div class="widget-content">
				<div id="availment-chart">
					
				</div>
				
			</div>
		</div>
	</div>
</div>


<script lang="text/javascript">
	// Element Attribute Helper
function attrDefault($el, data_var, default_val)
{
	if(typeof $el.data(data_var) != 'undefined')
	{
		return $el.data(data_var);
	}

	return default_val;
}
	$(document).ready(function() {
		setTimeout(show_stats_aruba(0), 1000);
		$('form#stat-form').on('submit', function(e){
			$('div.connected_users').html('');
			$('div.authenticated_users').html('');
			$('div#availment-chart').html('');
			setTimeout(show_stats_aruba(0), 1000);
			e.preventDefault();
		});
	});
	
	function show_stats_aruba()
	{
		$.ajax({
			type: "POST",
			url: currentIndex+'/poll_aruba',
			data: {'YII_CSRF_TOKEN':token, 'date':$('input[name="date"]').val()},
			success: function(data) {
				$('div#aruba-chart').html('');
				new Morris.Donut({
					element: 'aruba-chart',
					data: data.aruba_count
				});
			},
			dataType: "json"
		});
	};
</script>