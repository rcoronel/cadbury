<div class="row">
	<div class="span12">
		<div class="widget">
			<div class="widget-header">
				<h3>Portal Appearance</h3>
			</div>
			<div class="widget-content">
				<div class="tabbable">
					<div class="tab-content">
						<div class="tab-pane active" id="formcontrols">


							<?php echo CHtml::beginForm('', 'post', array('class'=>'form-horizontal form-groups-bordered', 'enctype'=>'multipart/form-data')); ?>
								<fieldset>
									<div class="control-group">
										<?php echo CHtml::label('Site Title', 'PortalConfiguration[SITE_TITLE]', array('class'=>'control-label'));?>
										<div class="controls">
											<?php echo CHtml::textField('PortalConfiguration[SITE_TITLE]', $site_title, array('class'=>'span6', 'maxlength'=>64)); ?>
										</div>
									</div>

									<div class="control-group">
										<?php echo CHtml::label('Landing page message', 'PortalConfiguration[LANDING_MESSAGE]', array('class'=>'control-label'));?>
										<div class="controls">
											<?php echo CHtml::textArea('PortalConfiguration[LANDING_MESSAGE]', $landing_message, array('class'=>'form-control', 'rows'=>15));?>
											<script type="text/javascript">
												var editor = CKEDITOR.replace('PortalConfiguration_LANDING_MESSAGE');
												editor.on( 'change', function( evt ) {
													// getData() returns CKEditor's HTML content.
													$('#PortalConfiguration_LANDING_MESSAGE').html(evt.editor.getData());
												});
											</script>
										</div>
									</div>

									<div class="control-group">
										<?php echo CHtml::label('Welcome message', 'PortalConfiguration[WELCOME_BACK]', array('class'=>'control-label col-lg-3'));?>
										<div class="controls">
											<?php echo CHtml::textArea('PortalConfiguration[WELCOME_BACK]', $welcome, array('class'=>'form-control', 'rows'=>15));?>
											<script type="text/javascript">
												var editor = CKEDITOR.replace('PortalConfiguration_WELCOME_BACK');
												editor.on( 'change', function( evt ) {
													// getData() returns CKEditor's HTML content.
													$('#PortalConfiguration_WELCOME_BACK').html(evt.editor.getData());
												});
											</script>
										</div>
									</div>

									<div class="control-group">
										<?php echo CHtml::label('Redirect URL', 'PortalConfiguration[REDIRECT_URL]', array('class'=>'control-label'));?>
										<div class="controls">
											<?php echo CHtml::textField('PortalConfiguration[REDIRECT_URL]', $redirect_url, array('class'=>'form-control')); ?>
										</div>
									</div>

									<hr>

									<div class="control-group">
										<?php echo CHtml::label('Logo', 'LOGO', array('class'=>'control-label'));?>
										<div class="controls">
											<?php if ( ! empty($logo)):?>
												<a href="<?php echo Link::image_url("portal/{$this->context->portal->portal_id}/{$logo}");?>" target="_blank">
													<?php echo $logo;?>
												</a>
												<button type="button" class="btn btn-xs btn-danger" onclick="window.location='<?php echo $this->createUrl($this->id.'/delete', array('config'=>'LOGO'));?>';">
													<i class="icon-remove-sign"></i>
												</button>
												<br>
											<?php endif;?>
											<?php echo CHtml::fileField('LOGO', '', array('class'=>'form-control')); ?>
										</div>
									</div>

									<div class="control-group">
										<?php echo CHtml::label('Inside Logo', 'INSIDE_LOGO', array('class'=>'control-label'));?>
										<div class="controls">
											<?php if ( ! empty($inside_logo)):?>
												<a href="<?php echo Link::image_url("portal/{$this->context->portal->portal_id}/{$inside_logo}");?>" target="_blank">
													<?php echo $inside_logo;?>
												</a>
												<button type="button" class="btn btn-xs btn-danger" onclick="window.location='<?php echo $this->createUrl($this->id.'/delete', array('config'=>'INSIDE_LOGO'));?>';">
													<i class="icon-remove-sign"></i>
												</button>
												<br>
											<?php endif;?>
											<?php echo CHtml::fileField('INSIDE_LOGO', '', array('class'=>'form-control')); ?>
										</div>
									</div>

									<div class="control-group">
										<?php echo CHtml::label('Center Image', 'CENTER_IMAGE', array('class'=>'control-label'));?>
										<div class="controls">
											<?php if ( ! empty($center_image)):?>
												<a href="<?php echo Link::image_url("portal/{$this->context->portal->portal_id}/{$center_image}");?>" target="_blank">
													<?php echo $center_image;?>
												</a>
												<button type="button" class="btn btn-xs btn-danger" onclick="window.location='<?php echo $this->createUrl($this->id.'/delete', array('config'=>'CENTER_IMAGE'));?>';">
													<i class="icon-remove-sign"></i>
												</button>
												<br>
											<?php endif;?>
											<?php echo CHtml::fileField('CENTER_IMAGE', '', array('class'=>'form-control')); ?>
										</div>
									</div>

									<div class="control-group">
										<?php echo CHtml::label('Favicon', 'FAVICON', array('class'=>'control-label'));?>
										<div class="controls">
											<?php if ( ! empty($favicon)):?>
												<a href="<?php echo Link::image_url("portal/{$this->context->portal->portal_id}/{$favicon}");?>" target="_blank">
													<?php echo $favicon;?>
												</a>
												<button type="button" class="btn btn-xs btn-danger" onclick="window.location='<?php echo $this->createUrl($this->id.'/delete', array('config'=>'FAVICON'));?>';">
													<i class="icon-remove-sign"></i>
												</button>
												<br>
											<?php endif;?>
											<?php echo CHtml::fileField('FAVICON', '', array('class'=>'form-control')); ?>
										</div>
									</div>

									<div class="control-group">
										<?php echo CHtml::label('Splash Image', 'SPLASH_IMG', array('class'=>'control-label col-lg-3'));?>
										<div class="controls">
											<?php if ( ! empty($splash)):?>
												<a href="<?php echo Link::image_url("portal/{$this->context->portal->portal_id}/{$splash}");?>" target="_blank">
													<?php echo $splash;?>
												</a>
												<button type="button" class="btn btn-xs btn-danger" onclick="window.location='<?php echo $this->createUrl($this->id.'/delete', array('config'=>'SPLASH_IMG'));?>';">
													<i class="icon-remove-sign"></i>
												</button>
												<br>
											<?php endif;?>
											<?php echo CHtml::fileField('SPLASH_IMG', '', array('class'=>'form-control')); ?>
										</div>
									</div>

									<div class="control-group">
										<?php echo CHtml::label('Footer Logo (Splash)', 'PortalConfiguration[POWERED_WELCOME]', array('class'=>'control-label'));?>
										<div class="controls">
											<?php echo CHtml::dropDownList('PortalConfiguration[POWERED_WELCOME]', $powered_welcome, array(1=>'Display', 0=>'Hide'), array('class'=>'form-control'));?>
										</div>
									</div>

									<div class="control-group">
										<?php echo CHtml::label('Footer Logo (Inside)', 'PortalConfiguration[POWERED_INSIDE]', array('class'=>'control-label'));?>
										<div class="controls">
											<?php echo CHtml::dropDownList('PortalConfiguration[POWERED_INSIDE]', $powered_inside, array(1=>'Display', 0=>'Hide'), array('class'=>'form-control'));?>
										</div>
									</div>

									<div class="control-group">
										<?php echo CHtml::label('CSS file', 'PortalConfiguration[CSS_FILE]', array('class'=>'control-label'));?>
										<div class="controls">
											<?php if ( ! empty($css_file)):?>
												<a href="<?php echo Link::css_url("portal/{$this->context->portal->portal_id}/{$css_file}");?>" target="_blank">
													<?php echo $css_file;?>
												</a>
												<button type="button" class="btn btn-xs btn-danger" onclick="window.location='<?php echo $this->createUrl($this->id.'/delete', array('config'=>'CSS_FILE'));?>';">
													<i class="icon-remove-sign"></i>
												</button>
												<br>
											<?php endif;?>
											<?php echo CHtml::textArea('PortalConfiguration[CSS_FILE]', $css_contents, array('class'=>'form-control span5', 'rows'=>15)); ?>
										</div>
									</div>

									<div class="form-actions">
										<button type="submit" class="btn btn-primary">Save</button> 
										<button class="btn">Cancel</button>
									</div>

								</fieldset>
							<?php echo CHtml::endForm(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>