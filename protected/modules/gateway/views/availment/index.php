
<?php echo CHtml::beginForm();?>
	<?php echo CHtml::hiddenField('ajax', 1);?>
	<table class="table table-hover table-bordered">
		<thead>
			<tr>
				<th class="span3"><?php echo CHtml::encode(Availment::model()->getAttributeLabel('name')); ?></th>
				<th class="span5"><?php echo CHtml::encode(Availment::model()->getAttributeLabel('description')); ?></th>
				<th class="span2 text-center">Status</th>
				<th class="span1 text-center"><?php echo CHtml::encode(Availment::model()->getAttributeLabel('Duration')); ?> (in minutes)</th>
				<th class="span2 text-center"><?php echo CHtml::encode(Availment::model()->getAttributeLabel('Is_recurring')); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php if ( ! empty($availments)):?>
				<?php foreach ($availments as $a):?>
					<tr>
						<td><?php echo $a->name;?></td>
						<!-- <td><?php // echo $a->description;?></td> -->
						<td class="text-center">
							<?php echo CHtml::textField("description[$a->availment_id]", $a->description, array('class'=>'form-control span5 input-sm '));?>
						</td>
						<td class="text-center">
							<?php echo CHtml::dropDownList("status[$a->availment_id]", $a->is_available, array('1'=>'Enabled', '0'=>'Disabled'), array('class'=>'form-control span2'));?>
						</td>
						<td class="text-center">
							<?php echo CHtml::textField("duration[$a->availment_id]", $a->duration, array('class'=>'form-control span1 input-sm number-only'));?>
						</td>
						<td>
							<?php echo CHtml::dropDownList("recurring[$a->availment_id]", $a->is_recurring, array(1=>'Yes', 0=>'No'), array('class'=>'form-control span2 input-sm'));?>
						</td>
					</tr>
				<?php endforeach;?>
			<?php endif;?>
			<?php if($this->permissions->edit == 1): ?>
				<tr>
					<td colspan="8" class="text-right">
						<?php echo CHtml::button('Submit', array('type'=>'submit', 'class'=>'btn btn-primary'));?>
					</td>
				</tr>
		<?php endif; ?>
		</tbody>
	</table>
<?php echo CHtml::endForm();?>
<script>
$(document).ready(function(){
	console.log($('select[id^="status"]').length);
	$('select[id^="status"]').each(function() {
		var row = $(this).closest('tr');
		if ($(this).val() == 0) {
			$(row).find('select').prop('disabled', 'disabled');
			$(row).find('input').prop('disabled', 'disabled');
			$(this).prop('disabled', false);
		}
	});
	$('select[id^="status"]').on('change', function() {
		var row = $(this).closest('tr');
		if ($(this).val() == 0) {
			$(row).find('select').prop('disabled', 'disabled');
			$(row).find('input').prop('disabled', 'disabled');
			$(this).prop('disabled', false);
		}
		else {
			$(row).find('input').prop('disabled', false);
			$(row).find('select').prop('disabled', false);
		}
		
	});
	$('form').on('submit',function(e){
		// check if submitted via AJAX
		if ($('input[name="ajax"]').length && $('input[name="ajax"]').val()) {
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			$.ajax({
				url : formURL,
				type: "POST",
				data : postData,
				dataType: 'json',
				success:function(data, textStatus, jqXHR) {
					toastr.success(data.message, '', {closeButton:true,showDuration:"300",timeOut: "3000"});
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					//if fails     
				}
			});
			e.preventDefault(); //STOP default action
		}
	});
	$(document).on("keydown", ".number-only", function(event) {
		console.log("number");
		if ( event.keyCode == 46 || event.keyCode == 8  ||  event.keyCode == 9) {
		}
		else {
			if (event.keyCode < 48 || event.keyCode > 57 ) {
				event.preventDefault();	
			} 	
		}
	});
});
</script>
<?php $this->widget('widgets.ajax.Modal');?>