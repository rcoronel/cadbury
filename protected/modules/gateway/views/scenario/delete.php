<div class="row">
	<div class="span12">
		<div class="widget">
			<div class="widget-header">
				<i class="icon-th-large"></i>
				<h3>
					Delete 
					<strong><?php echo $scenario->title;?></strong> 
					scenario (#<?php echo $scenario->scenario_id;?>)?
				</h3>
			</div>
			<div class="widget-content">

				<p>Deleting this scenario will <strong>remove</strong> this in the possible scenarios the user will choose on availment methods</p>

				<?php $form=$this->beginWidget('CActiveForm', array('htmlOptions' => array(	'class'=>'form-horizontal', 'role' => 'form', 'method' => 'post'))); ?>
					<?php echo CHtml::hiddenField('scenario_id', $scenario->scenario_id)?>
					<?php echo CHtml::hiddenField('security_key', CPasswordHelper::hashPassword($scenario->scenario_id));?>
					<div class="buttons">
						<button type="submit" class="btn btn-danger">Delete</button>
						<button type="button" class="btn btn-default" onclick="location.href = '<?php echo $this->createUrl("{$this->id}/");?>';">Cancel</button>
					</div>
				<?php $this->endWidget();?>
			</div>
		</div>
	</div>
</div>