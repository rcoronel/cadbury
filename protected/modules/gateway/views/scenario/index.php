<?php foreach ($objectList as $ol):?>
	<div class="span4">
		<div id="target-3" class="widget">
			<div class="widget-content">
				<div class="pull-right">
					<h5>
						<button class="btn btn-default btn-xs icon-edit" onclick="window.location.href='<?php echo $this->createUrl($this->id.'/update', array('id' => $ol->scenario_id));?>'"></button>
						<button class="btn btn-default btn-xs icon-trash" onclick="window.location.href='<?php echo $this->createUrl($this->id.'/delete', array('id' => $ol->scenario_id));?>'"></button>
					</h5>
				</div>
				<h1><strong><?php echo $ol->title;?></strong></h1>
				<p><b>New Users</b></p>
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th style="text-align:center">level</th>
							<th>Availment</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($ol->new as $new):?>
							<tr>
								<td style="text-align:center;"><?php echo $new->level;?></td>
								<td><?php echo $new->availment->name;?></td>
							</tr>
						<?php endforeach;?>
					</tbody>
				</table>
				<p><b>Returning Users</b></p>
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th style="text-align:center">Level</th>
							<th>Availment</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($ol->returning as $returning):?>
							<tr>
								<td style="text-align:center;"><?php echo $returning->level;?></td>
								<td><?php echo $returning->availment->name;?></td>
							</tr>
						<?php endforeach;?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
<?php endforeach;?>