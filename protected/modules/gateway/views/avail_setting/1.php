<div class="row">
	<div class="span12">
		<div class="widget widget-table action-table">
			<div class="widget-content">
				<div class="tabbable">
					<br>
					<div class="tab-content">
						<div class="tab-pane active" id="formcontrols">
							<?php echo CHtml::beginForm('', 'post', array('class'=>'form-horizontal form-groups-bordered', 'role'=>'form'));?>
								<div class="control-group">
									<?php echo CHtml::label('App ID', 'AvailmentSetting[FB_APP_ID]', array('class'=>'control-label required'));?>
									<div class="controls">
										<?php echo CHtml::textField('AvailmentSetting[FB_APP_ID]', AvailmentSetting::getValue($object->availment_id, 'FB_APP_ID'), array('class'=>'span5'));?>
									</div>
								</div>
								<div class="control-group">
									<?php echo CHtml::label('App Secret Key', 'AvailmentSetting[FB_SECRET_KEY]', array('class'=>'control-label required'));?>
									<div class="controls">
										<?php echo CHtml::textField('AvailmentSetting[FB_SECRET_KEY]', AvailmentSetting::getValue($object->availment_id, 'FB_SECRET_KEY'), array('class'=>'span5'));?>
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn btn-primary">Submit</button>
								</div>
							<?php echo CHtml::endForm();?>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
