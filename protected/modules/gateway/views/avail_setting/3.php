<div class="row">
	<div class="span12">
		<div class="widget widget-table action-table">
			<div class="widget-content">
				<div class="tabbable">
					<br>
					<div class="tab-content">
						<div class="tab-pane active" id="formcontrols">
							<?php echo CHtml::beginForm('', 'post', array('class'=>'form-horizontal form-groups-bordered', 'role'=>'form'));?>
								<div class="control-group">
									<?php echo CHtml::label('Globe duration (in minutes)', 'AvailmentSetting[GLOBE_DURATION]', array('class'=>'control-label required'));?>
									<div class="controls">
										<?php echo CHtml::textField('AvailmentSetting[GLOBE_DURATION]', AvailmentSetting::getValue($object->availment_id, 'GLOBE_DURATION'), array('class'=>'span1'));?>
									</div>
								</div>
								<div class="control-group">
									<?php echo CHtml::label('Non-Globe duration (in minutes)', 'AvailmentSetting[XGLOBE_DURATION]', array('class'=>'control-label required'));?>
									<div class="controls">
										<?php echo CHtml::textField('AvailmentSetting[XGLOBE_DURATION]', AvailmentSetting::getValue($object->availment_id, 'XGLOBE_DURATION'), array('class'=>'span1'));?>
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn btn-primary">Submit</button>
								</div>
							<?php echo CHtml::endForm();?>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
