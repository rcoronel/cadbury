<div class="account-container">
	<div class="content clearfix">
		<?php $form=$this->beginWidget('CActiveForm', array('id'=>'form-login')); ?>
			<div id="form-login-error" class="alert alert-danger" style="display:none;"></div>
			<h1>Member Login</h1>
			<div class="login-fields">
				<p>Please provide your details</p>
				<div class="field">
					<?php echo $form->textField($admin, 'email', array('class'=>'form-control login username-field', 'placeholder'=>'E-mail Address', 'autocomplete'=>'off')); ?>
				</div>
				<br>
				<div class="field">
					<?php echo $form->passwordField($admin, 'password', array('class'=>'form-control login password-field', 'placeholder'=>'Password', 'autocomplete'=>'off', 'value'=>'')); ?>
				</div>
			</div>
			<div class="login-actions">
				<span class="login-checkbox">
					<input id="Field" name="Field" type="checkbox" class="field login-checkbox" value="First Choice" tabindex="4" />
					<label class="choice" for="Field">Keep me signed in</label>
				</span>
									
				<button class="button btn btn-success btn-large">
					SIGN IN
				</button>
				
			</div>
		<?php $this->endWidget();?>
	</div>
	
</div>
<div class="login-extra">
	<a href="#">Reset Password</a>
</div> <!-- /login-extra -->

<script type="text/javascript">
	jQuery(document).ready(function($)
	{
		$('form#form-login').on('submit', function(e){
			$('div.alert-danger').hide();
			$('div.alert-danger').html('');
			$.post('', $(this).serialize(), function(data){
				if ( ! data.status)
				{
					$('div.alert-danger').html(data.message);
					$('div.alert-danger').show();
					$('html, body').animate({ scrollTop: $("div.alert").offset().top - 25}, 300);
				}
				else
				{
					setTimeout(function() {
						$('#confirmation-modal .modal-header h4#form-modalLabel').html('Account validated');
						$('#confirmation-modal .modal-body #form-modalBody').html('Logging in');
						$('#confirmation-modal').modal({backdrop: 'static', keyboard: false});
					}, 900);
					
					setTimeout(function() {
						window.location.href = data.redirect;
					  }, 2000);
				}
			}, 'json');
			
			e.preventDefault();
			return false;
		});

		$('form#forgot-pass').on('submit', function(e){
			$('div.alert-danger').hide();
			$('div.alert-danger').html('');
			
			$.post('', $(this).serialize(), function(data){
				console.log(data);
				if ( ! data.status)
				{
					$('#myModal').modal('toggle');
					$('div.alert-danger').html(data.message);
					$('div.alert-danger').show();
					$('html, body').animate({ scrollTop: $("div.alert").offset().top - 25}, 300);
				}
				else
				{
					$('#confirmation-modal .modal-header h4#form-modalLabel').html('');
					$('#confirmation-modal .modal-body #form-modalBody').html('Sending email for password reset');
					$('#confirmation-modal').modal({backdrop: 'static', keyboard: false});
					setTimeout(function() {
						window.location.href = data.redirect;
					  }, 2000);
				}
			}, 'json');
			
			e.preventDefault();
			return false;
		});

	});
</script>
<?php $this->widget('widgets.ajax.Modal');?>