
<div class="stat">
	<i class="icon-group" style="color:green;"></i>
	<h5>Connected users</h5>
	<span class="value num" data-end="<?php echo $all_users;?>" data-postfix="" data-duration="1500" data-delay="0"><?php echo $all_users;?></span>
</div>

<div class="stat">
	<i class="icon-group" style="color:blue;"></i>
	<h5>New users</h5>
	<span class="value num"><?php echo $new_users;?></span>
</div>
<!-- .stat -->

<div class="stat">
	<i class="icon-group" style="color:orange;"></i>
	<h5>Returning users</h5>
	<span class="value num"><?php echo $return_users;?></span>
</div>
<!-- .stat -->