
<div class="stat">
	<i class="icon-group" style="color:green;"></i>
	<h5>Connected users</h5>
	<span class="value"><?php echo $all_auth_users;?></span>
</div>

<div class="stat">
	<i class="icon-group" style="color:blue;"></i>
	<h5>New users</h5>
	<span class="value"><?php echo $new_auth_users;?></span>
</div>
<!-- .stat -->

<div class="stat">
	<i class="icon-group" style="color:orange;"></i>
	<h5>Returning users</h5>
	<span class="value"><?php echo $return_auth_users;?></span>
</div>
<!-- .stat -->