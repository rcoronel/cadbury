<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="description" content="">
		<meta name="author" content="Yondu">
		<?php echo CHtml::cssFile(Link::css_url('gateway/bootstrap.min.css'));?>
		<?php echo CHtml::cssFile(Link::css_url('gateway/bootstrap-responsive.min.css'));?>
		<?php echo CHtml::cssFile(Link::css_url('gateway/font-awesome.css'));?>
		<?php echo CHtml::cssFile(Link::css_url('gateway/style.css'));?>
		<?php echo CHtml::cssFile('http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600');?>
		<?php echo CHtml::scriptFile(Link::js_url('jquery/jquery-1.11.2.min.js'));?>
		<?php echo CHtml::scriptFile(Link::js_url('bootstrap/bootstrap.min.js'));?>
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		<title><?php echo $this->menu->title;?> | WiFi Dashboard</title>
		<script>
			var baseURL = "<?php echo Link::base_url()?>";
			var currentIndex = "<?php echo $this->createUrl($this->id.'/', $_GET);?>";
			var currentURL = "<?php echo $this->createUrl('/'.$this->route,$_GET);?>";
			var token = "<?php echo Yii::app()->request->csrfToken;?>";
		</script>
	</head>

	<body>
		<div class="navbar navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container">
					<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</a>
					<a class="brand" href="<?php echo $this->createUrl("dashboard/");?>">
						<?php echo $this->context->portal->name;?>
					</a>
					<div class="nav-collapse">
						<ul class="nav pull-right">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-user"></i>
									<?php echo $this->context->portal_admin->Name;?>
									<b class="caret"></b>
								</a>
								<ul class="dropdown-menu">
									<li><a href="javascript:;">Profile</a></li>
									<li><a href="<?php echo $this->createUrl("logout/");?>">Logout</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="subnavbar">
			<div class="subnavbar-inner">
				<div class="container">
					<ul class="mainnav">
						<?php foreach ($this->context->menus as $menu):?>
							<li class="<?php echo ($this->menu->classname == $menu->classname OR $this->menu->parent_portal_menu_id == $menu->portal_menu_id) ? 'active' : '';?> <?php echo ($menu->has_subpages) ? 'dropdown' :'';?>">
								<?php if ($menu->has_subpages):?>
									<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
										<i class="<?php echo $menu->img;?>"></i>
										<span><?php echo $menu->title;?></span>
										<b class="caret"></b>
									</a>
									<ul class="dropdown-menu">
										<?php foreach ($this->context->submenus as $index => $submenu):?>
											<?php if ($submenu->parent_portal_menu_id == $menu->portal_menu_id):?>
												<li>
													<a href="<?php echo $this->createUrl("{$submenu->classname}/");?>">
														<?php echo $submenu->title;?>
													</a>
												</li>
											<?php endif;?>
										<?php endforeach;?>
									</ul>
								<?php else:?>
									<a href="<?php echo $this->createUrl("{$menu->classname}/");?>">
										<i class="<?php echo $menu->img;?>"></i>
										<span><?php echo $menu->title;?></span>
									</a>
								<?php endif;?>
							</li>
						<?php endforeach;?>
					</ul>
				</div>
			</div>
		</div>
		
		
		
		<?php echo $content;?>
		
		<div class="footer">
			<div class="footer-inner">
				<div class="container">
					<div class="row">
						<div class="span12">
							© 2014 - <?php echo date('Y');?> Yondu 
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- js placed at the end of the document so the pages load faster -->
		
		<?php echo CHtml::scriptFile(Link::js_url('jquery/ui/jquery-ui-1.10.3.minimal.min.js'));?>
		<?php echo CHtml::scriptFile(Link::js_url('gateway/base.js'));?>
	</body>
</html>
