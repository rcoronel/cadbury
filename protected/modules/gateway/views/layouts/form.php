<div class="row">
	<?php if ($object->hasErrors()):?>
		<div id="form-error" class="alert alert-danger">
			<?php echo CHtml::errorSummary($object);?>
		</div>
	<?php endif;?>
	<div class="span12">
		<div id="form-error" class="alert alert-danger" style="display: none;"></div>
	</div>
</div>
<div class="row">
	<div class="span12">
		<div class="widget widget-table action-table">
			<div class="widget-content">
				<div class="tabbable">
					<br>
					<div class="tab-content">
						<div class="tab-pane active" id="formcontrols">
							<?php $form=$this->beginWidget('CActiveForm', array('htmlOptions'=>array('class'=>'form-horizontal form-groups-bordered', 'role'=>'form', 'method'=>'post'))); ?>
								<?php foreach ($fields as $field=>$attributes):?>
									<?php if ($field == 'xmlhttprequest'):?>
										<?php echo CHtml::hiddenField('ajax', $attributes['value'])?>
									<?php else:?>
										<div class="control-group">
											<?php echo $form->labelEx($object, $field, array('class'=>'control-label'));?>
											<div class="controls">
												<?php if ($attributes['type'] == 'text'):?>
													<?php echo $form->textField($object, $field, array('class'=>$attributes['class'], 'placeholder'=>$object->getAttributeLabel($field)));?>
												<?php endif;?>

												<?php if ($attributes['type'] == 'textarea'):?>
													<?php echo $form->textarea($object, $field, array('class'=>'form-control', 'style'=>'resize:none;', 'placeholder'=>$object->getAttributeLabel($field)));?>
												<?php endif;?>
												<?php if ($attributes['type'] == 'wysiwyg'):?>
													<?php echo $form->textarea($object, $field, array('class'=>'form-control', 'placeholder'=>$object->getAttributeLabel($field)));?>
													<script type="text/javascript">
														var editor = CKEDITOR.replace('<?php echo get_class($object);?>_<?php echo $field;?>');
														editor.on( 'change', function( evt ) {
															// getData() returns CKEditor's HTML content.
															$('#<?php echo get_class($object);?>_<?php echo $field;?>').html(evt.editor.getData());
														});
													</script>
												<?php endif;?>

												<?php if ($attributes['type'] == 'password'):?>
													<?php echo $form->passwordField($object, $field, array('class'=>'form-control', 'value'=>'', 'placeholder'=>$object->getAttributeLabel($field)));?>
												<?php endif;?>

												<?php if ($attributes['type'] == 'select'):?>
													<?php echo $form->dropDownList($object, $field, $attributes['value'], array('class'=>'form-control', 'prompt'=>'---'));?>
												<?php endif;?>

												<?php if ($attributes['type'] == 'radio'):?>
													<div class="col-sm-5">
														<div class="radio inline">
															<?php echo $form->radioButtonList($object, $field, $attributes['value'], array('separator'=>''));?>
														</div>
													</div>
												<?php endif;?>
											</div>
										</div>
									<?php endif;?>
								<?php endforeach;?>
								<div class="form-actions">
									<button type="submit" class="btn btn-primary">Submit</button>
								</div>
							<?php $this->endWidget();?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->widget('widgets.ajax.Modal');?>

<script>
$(document).ready(function(){
	$('form').on('submit',function(e){
		// check if submitted via AJAX
		if ($('input[name="ajax"]').length && $('input[name="ajax"]').val())
		{
			// remove error style
			$('div.control-group').removeClass('has-error');

			// hide errors
			$('div#form-error').slideUp( 300 ).delay( 800 ).html();
			//$('div#form-error').hide();

			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				dataType: 'json',
				success:function(data, textStatus, jqXHR) {
					if ( ! data.status) {
						$('div#form-error').slideDown(300).delay(800).html(data.message);
						$('div#form-error').show();

						if (typeof data.fields !== 'undefined')
						{
							$.each(data.fields, function(index, value){
								$('label[for="'+value+'"]').parent().addClass('has-error');
							});
						}
					}
					else {
						setTimeout(function() {
							$('#confirmation-modal .modal-body').html(data.message);
							$('#confirmation-modal').modal({backdrop: 'static', keyboard: false});
						}, 900);
						
						setTimeout(function() {
							window.location.href = currentIndex;
						  }, 2000);
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					//if fails     
				}
			});
			e.preventDefault(); //STOP default action
		}
	});
});
</script>