<?php if (array_key_exists('position', $object->attributes)):?>
	<script type="text/javascript">
		$(document).ready(function() {
			$("table.tableDnD").tableDnD({
				onDragClass: "dragging",
				onDrop: function(table, row){
					$.ajax({
						type: 'GET',
						headers: { "cache-control": "no-cache" },
						async: false,
						url: '<?php echo $this->createUrl("{$this->id}/position", $_GET);?>',
						data: $.tableDnD.serialize(),
						success: function(data) {
							$('td.dragHandle').each(function(index) {
								var position = index+1;
								$(this).html('<i class="icon-move"></i> '+ position);
							});
							toastr.success(data, '', {closeButton:true,showDuration:"300",timeOut: "3000"});
						}
					});
				},
				dragHandle: "dragHandle"
			});
		});
	</script>
<?php endif;?>

<div class="row">
	<div class="span12 mt">
		<div class="table-responsive">
			<?php $form=$this->beginWidget('CActiveForm', array('action'=>$this->createUrl('/'.$this->route,$_GET), 'htmlOptions'=>array('id'=>'filter-form', 'class'=>'form-horizontal', 'role'=>'form', 'method'=>'get'))); ?>
				<table id="page-list" class="table tableDnD table-bordered table-hover dataTable">
					<thead>
						<tr class="nodrag nodrop">
							<?php foreach ($row_fields as $header=>$attribute):?>
								<th class="<?php echo (isset($attribute['class'])) ? $attribute['class'] : '';?>">
									<strong>
										<?php echo CHtml::encode($object->getAttributeLabel($header)); ?>
									</strong>
								</th>
							<?php endforeach;?>
							<?php if (isset($row_actions) && ! empty($row_actions)):?>
								<th class="span2 text-center">
									<strong>
										<?php echo CHtml::encode('Actions');?>
									</strong>
								</th>
							<?php endif;?>
						</tr>
						<tr class="nodrag nodrop">
							<?php foreach ($row_fields as $header=>$attribute):?>
								<th class="<?php echo (isset($attribute['class'])) ? $attribute['class'] : '';?>">
									<?php if ($attribute['type'] == 'text'):?>
										<?php echo $form->textField($object, $header, array('class'=>$attribute['class']));?>
									<?php endif;?>

									<?php if ($attribute['type'] == 'select'):?>
										<?php echo $form->dropDownList($object, $header, $attribute['value'], array('class'=>$attribute['class'], 'prompt'=>'---'));?>
									<?php endif;?>
								</th>
							<?php endforeach;?>
							<?php if (isset($row_actions) && ! empty($row_actions)):?>
								<th class="span2 text-center">
									<button type="submit" name="search-filter" value="1" class="btn btn-sm" title="Search">
										<i class="icon-search"></i>
									</button>
									<button type="submit" name="reset-filter" id="reset-filter" value="2" class="btn btn-sm" title="Reset">
										<i class="icon-ban-circle"></i>
									</button>
								</th>
							<?php endif;?>
						</tr>
					</thead>
					<tbody>
						<?php echo $objectList;?>
					</tbody>
				</table>
			<?php $this->endWidget();?>
		</div>
		<?php $this->widget('CLinkPager', array(
			'pages' => $pages,
		)); ?>
	</div>
</div>
<?php $this->widget('widgets.ajax.Modal');?>
<script type="text/javascript">
	$(document).ready(function() {
		$('form#filter-form').on('submit', function(e){
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				dataType: 'json',
				success:function(data, textStatus, jqXHR) {
					if (data.status){
						setTimeout(function() {
							$('tbody').html(data.message);
						}, 900);
						
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					//if fails     
				}
			});
			e.preventDefault();
		});
		$('#reset-filter').on('click', function(){
			$('form#filter-form input[type="text"]').val('');
			$('form#filter-form select').val('');
			$('form#filter-form').find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
		});
	});
</script>