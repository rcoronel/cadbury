<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="apple-mobile-web-app-capable" content="yes"> 
		<meta name="description" content="">
		<meta name="author" content="Yondu">
		<title>Login</title>

		<?php echo CHtml::cssFile(Link::css_url('gateway/bootstrap.min.css'));?>
		<?php echo CHtml::cssFile(Link::css_url('gateway/bootstrap-responsive.min.css'));?>
		<?php echo CHtml::cssFile(Link::css_url('font-awesome/font-awesome.css'));?>
		<?php echo CHtml::cssFile(Link::css_url('gateway/style.css'));?>
		<?php echo CHtml::cssFile(Link::css_url('gateway/login.css'));?>
		<?php echo CHtml::cssFile('http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600');?>
		<?php echo CHtml::scriptFile(Link::js_url('jquery/jquery-1.11.2.min.js'));?>
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		<script>
			var baseURL = "<?php echo Link::base_url()?>";
			var currentIndex = "<?php echo $this->createUrl($this->id.'/');?>";
			var currentURL = "<?php echo $this->createUrl('/'.$this->route,$_GET);?>";
			var token = "<?php echo Yii::app()->request->csrfToken;?>";
		</script>
	</head>

	<body>
		<div class="navbar navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container">
					<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</a>
			
					<a class="brand" href="index.html">
										
					</a>		
			
					<div class="nav-collapse">
						<ul class="nav pull-right">

							<li class="">						
								<a href="signup.html" class="">
									Don't have an account?
								</a>

							</li>

							<li class="">						
								<a href="index.html" class="">
									<i class="icon-chevron-left"></i>
									Back to Homepage
								</a>

							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<?php echo $content;?>
		
		<?php echo CHtml::scriptFile(Link::js_url('bootstrap/bootstrap.min.js'));?>
		<?php echo CHtml::scriptFile(Link::js_url('gateway/login.js'));?>


	</body>
</html>
