<?php if ( ! empty($objectList)):?>
	<?php foreach ($objectList as $row):?>
		<tr id="<?php echo $row->{$object->tableSchema->primaryKey};?>">

			<?php foreach ($row_fields as $field => $attribute):?>
				<td class="<?php echo (isset($attribute['class'])) ? $attribute['class'] : '';?>">
					<?php if ($field == 'position'):?><i class="icon-move"></i> &nbsp;<?php endif;?>

					<?php if ($attribute['type'] == 'text'):?>
						<?php if (isset($attribute['parent']) && isset($attribute['child'])):?>
							<?php if ($row->{$attribute['parent']}):?>
								<?php echo $row->{$attribute['parent']}->{$attribute['child']};?>
							<?php endif;?>
						<?php else:?>
							<?php echo $row->{$field};?>
						<?php endif;?>
					<?php endif;?>

					<?php if ($attribute['type'] == 'select'):?>
						<?php if (isset($attribute['mode']) && $attribute['mode'] == 'bool'):?>
							<?php if ($row->$field):?>
								<span class="label label-success" title="<?php echo $attribute['value'][$row->$field];?>">
									<?php echo $attribute['value'][$row->$field];?>
								</span>
							<?php else:?>
								<span class="label label-danger" title="<?php echo $attribute['value'][$row->{$field}];?>">
									<?php echo $attribute['value'][$row->$field];?>
								</span>
							<?php endif;?>
						<?php else:?>
							<?php echo $attribute['value'][$row->$field];?>
						<?php endif;?>
					<?php endif;?>

					<?php if ($attribute['type'] == 'image'):?>
						<?php if ($row->{$field}):?>
							<?php echo CHtml::image($attribute['url'].$row->{$field});?>
						<?php endif;?>
					<?php endif;?>
				</td>
			<?php endforeach;?>

			<?php if (isset($row_actions) && ! empty($row_actions)):?>
				<td class="text-center col-md-1">
					<div class="btn-group-action text-center">
						<div class="">
							<?php foreach ($row_actions as $action):?>
								<?php if ($action == 'view' && $this->permissions->view):?>
									<a href="<?php echo $this->createUrl($this->id.'/view', array('id' => $row->{$object->tableSchema->primaryKey}));?>" title="View" class="btn btn-default">
										<i class="icon-eye-open"></i>
									</a>
								<?php endif;?>
								<?php if ($action == 'edit' && $this->permissions->edit):?>
									<a href="<?php echo $this->createUrl($this->id.'/update', array('id' => $row->{$object->tableSchema->primaryKey}));?>" title="Edit" class="btn btn-default">
										<i class="icon-edit-sign"></i>
									</a>
								<?php endif;?>
								<?php if ($action == 'delete' && $this->permissions->delete):?>
									<a href="<?php echo $this->createUrl($this->id.'/delete', array('id' => $row->{$object->tableSchema->primaryKey}));?>" title="Delete" class="btn btn-default">
										<i class="icon-trash"></i>
									</a>
								<?php endif;?>
							<?php endforeach;?>
						</div>
					</div>
				</td>
			<?php endif;?>

		</tr>
	<?php endforeach;?>
<?php else:?>
	<tr>
		<td colspan="<?php echo count($row_fields) + count($row_actions);?>">
			<i class="entypo-info-circled"></i>
			No records found.
		</td>
	</tr>
<?php endif;?>