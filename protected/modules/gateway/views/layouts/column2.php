<?php $this->beginContent('/layouts/main'); ?>


<div class="main">
	<div class="main-inner">
		<div class="container">
			<div class="row">
				<div class="span12">
					<div class="pull-right">
						<!-- ACTION BUTTONS -->
						<?php if ( ! empty($this->actionButtons)):?>
							<?php foreach($this->actionButtons as $action => $button):?>
								<?php if ($action === $this->action->id):?>

									<?php if (isset($button['add'])):?>
										<a class="btn btn-default" href="<?php echo $button['add']['link'];?>">
											<i class="icon-plus"></i>
											Add
										</a>
									<?php endif;?>

									<?php if (isset($button['back'])):?>
										<a class="btn btn-default" href="<?php echo $button['back']['link'];?>">
											<i class="icon-arrow-left"></i>
											Back to list
										</a>
									<?php endif;?>

								<?php endif;?>
							<?php endforeach;?>
						<?php endif;?>
						<!-- /ACTION BUTTONS -->
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="span12">
					<?php if(Yii::app()->user->hasFlash('success')): ?>
						<div class="alert alert-success alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<i class="entypo-info-circled"></i>&nbsp;
							<?php echo Yii::app()->user->getFlash('success'); ?>
						</div>
					<?php endif;?>
					<?php if(Yii::app()->user->hasFlash('fail')): ?>
						<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<i class="icon-exclamation-sign"></i>&nbsp;
							<?php echo Yii::app()->user->getFlash('fail'); ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
			<?php echo $content;?>
		</div>
	</div>
</div>


<?php $this->endContent(); ?>