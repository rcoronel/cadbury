<div class="row">
	<div class="span12">
		<div class="widget">
			<div class="widget-header">
				<i class="icon-th-large"></i>
				<h3>
					Delete 
					<strong><?php echo $ap->name;?></strong> 
					(#<?php echo $ap->access_point_id;?>)?
				</h3>
			</div>
			<div class="widget-content">

				<p>Deleting this will <strong>remove</strong> the record from the database. For security purposes, this record will not be deleted if there are access points associated.</p>

				<?php $form=$this->beginWidget('CActiveForm', array('htmlOptions' => array(	'class'=>'form-horizontal', 'role' => 'form', 'method' => 'post'))); ?>
					<?php echo CHtml::hiddenField('access_point_id', $ap->access_point_id)?>
					<?php echo CHtml::hiddenField('security_key', CPasswordHelper::hashPassword($ap->access_point_id));?>
					<div class="buttons">
						<button type="submit" class="btn btn-danger">Delete</button>
						<button type="button" class="btn btn-default" onclick="location.href = '<?php echo $this->createUrl("{$this->id}/");?>';">Cancel</button>
					</div>
				<?php $this->endWidget();?>
			</div>
		</div>
	</div>
</div>