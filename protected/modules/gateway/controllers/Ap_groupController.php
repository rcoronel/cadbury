<?php

class Ap_groupController extends GatewayController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action))
		{
			// action buttons
			$this->actionButtons['index'] = array('add' => array('link' => $this->createURL($this->id.'/add')));
			$this->actionButtons['add'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back' => array('link' => $this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		// fields to be displayed
		$row_fields = array();
		$row_fields['access_point_group_id']['type'] = 'text';
		$row_fields['access_point_group_id']['class'] = 'text-center span1';
		
		$row_fields['name']['type'] = 'text';
		$row_fields['name']['class'] = 'span7';
		
		$row_fields['is_active']['type'] = 'select';
		$row_fields['is_active']['class'] = 'span2';
		$row_fields['is_active']['value'] = array('1'=>'Enabled', '0' =>'Disabled');
		$row_fields['is_active']['mode'] = 'bool';
		
		$row_actions = array('edit', 'delete');
		
		$object = new AccessPointGroup();
		$list = self::_showList($object, $row_fields, $row_actions);
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$list['content'], 'row_fields'=>$row_fields, 'row_actions'=>$row_actions, 'pages'=>$list['pages']));
	}
	
	/**
	 * Display list
	 * 
	 * @param object $object AccessPointGroup instance
	 * @param array $row_fields
	 * @param array $row_actions
	 * @return mixed
	 */
	private function _showList(AccessPointGroup $object, $row_fields, $row_actions)
	{
		$object->attributes = AccessPointGroup::setSessionAttributes(Yii::app()->request->getPost('AccessPointGroup'));
		$criteria = $object->search()->criteria;
		$criteria->compare('portal_id', $this->context->portal->portal_id);
		$count = AccessPointGroup::model()->count($criteria);
		
		$pages = new CPagination($count);
		$criteria->limit = 25;
		$criteria->offset = (Yii::app()->request->getQuery('page', 1) * $criteria->limit) - $criteria->limit;
		
		$pages->pageSize=$criteria->limit;
		$pages->applyLimit($criteria);
		
		$list = AccessPointGroup::model()->findAll($criteria);
		$view = $this->renderPartial('/layouts/list-body', array('object'=>$object, 'objectList'=>$list, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions), TRUE);
		if (Yii::app()->request->isAjaxRequest) {
			die(CJSON::encode(array('status'=>1, 'message'=>$view)));
		}
		return array('pages'=>$pages, 'content'=>$view);
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		$ap_group = new AccessPointGroup;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($ap_group);
		}
		
		self::_renderForm($ap_group);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$ap_group = AccessPointGroup::model()->findByPk($id);
		AccessPointGroup::validateObject($ap_group, 'AccessPointGroup');
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($ap_group);
		}
		
		self::_renderForm($ap_group);
	}
	
	/**
	 * Delete record
	 * 
	 * @access public
	 * @param int $id AccessPointGroup ID
	 * @return void
	 */
	public function actionDelete($id)
	{
		$ap_group = AccessPointGroup::model()->findByPk($id);
		AccessPointGroup::validateObject($ap_group, 'AccessPointGroup');
		
		if (Yii::app()->request->isPostRequest) {
			// check for security token
			if (CPasswordHelper::verifyPassword($ap_group->access_point_group_id, Yii::app()->request->getPost('security_key'))) {
				$count = AccessPoint::model()->countByAttributes(array('access_point_group_id'=>$ap_group->access_point_group_id));
				if ($count) {
					Yii::app()->user->setFlash('fail', 'Cannot delete AP Group that has associated APs.');
					$this->redirect(array("$this->id/"));
				}
			
				// Delete record
				AccessPointGroup::model()->deleteByPk($ap_group->access_point_group_id);
				
				Yii::app()->user->setFlash('success', 'Delete successful!');
			}
			else {
				Yii::app()->user->setFlash('fail', 'Error in deletion.');
			}
			$this->redirect(array("$this->id/"));
		}
		$this->render('delete', array('ap_group'=>$ap_group));
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object AccessPointGroup instance
	 * @return void
	 */
	private function _renderForm(AccessPointGroup $object)
	{
		$fields = array();
		
		$fields['name']['type'] = 'text';
		$fields['name']['class'] = 'span6';
		
		$fields['is_active']['type'] = 'radio';
		$fields['is_active']['class'] = 'col-lg-5';
		$fields['is_active']['value'] = array('1'=>'Enabled', '0'=>'Disabled');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object' => $object, 'fields' => $fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object AccessPointGroup instance
	 * @return	void
	 */
	private function _postValidate(AccessPointGroup $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('AccessPointGroup');
		$object->attributes = $post;
		$object->portal_id = $this->context->portal->portal_id;

		if ($object->validate() && $object->save()) {
			self::displayFormSuccess();
		}
		self::displayFormError($object);
	}
}