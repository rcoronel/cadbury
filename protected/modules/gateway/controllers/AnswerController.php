<?php

class AnswerController extends GatewayController
{
	public $question;
	
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		$this->question = SurveyQuestion::model()->with('survey')->findByAttributes(array('token'=>Yii::app()->request->getParam('token')));
		
		if (parent::beforeAction($action))
		{
			// action buttons
			
			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @param int $question_id
	 * @return	void
	 */
	public function actionIndex()
	{
		$id = Yii::app()->request->getParam('id');
		if ($id) {
			$answer = SurveyAnswer::model()->with('question')->findByPk($id);
			SurveyAnswer::validateObject($answer, 'SurveyAnswer', $this->createUrl('survey/'));
			
			$this->redirect($this->createURL($this->id.'/', array('token'=>$answer->question->token)));
		}
		
		// validate question
		SurveyQuestion::validateObject($this->question, 'SurveyQuestion', $this->createUrl('survey/'));
		
		$this->actionButtons['index'] = array(
			'add'=>array('link'=>$this->createURL($this->id.'/add', array('token'=>$this->question->token))),
			'back'=>array('link'=>$this->createURL('question/', array('token'=>$this->question->survey->token))));
		
		// declare scripts
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'plugins', 'jquery.tablednd.js')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/jquery.toastr.min.js'));
		
		// fields to be displayed
		$row_fields = array();
		$row_fields['survey_answer_id']['type'] = 'text';
		$row_fields['survey_answer_id']['class'] = 'text-center span1';
		
		$row_fields['answer']['type'] = 'text';
		$row_fields['answer']['class'] = 'span5';
		
		$row_fields['answer_type']['type'] = 'select';
		$row_fields['answer_type']['class'] = 'span2';
		$row_fields['answer_type']['value'] = array('text'=>'Textbox', 'dropdown'=>'Dropdown list', 'checkbox'=>'Checkbox', 'radio'=>'Radio button');
		
		$row_fields['position']['type'] = 'text';
		$row_fields['position']['class'] = 'text-center span1 dragHandle';
		
		$row_actions = array('edit', 'delete');
		
		$object = new SurveyAnswer();
		$list = self::_showList($object, $row_fields, $row_actions, $this->question->survey_question_id);
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$list['content'], 'row_fields'=>$row_fields, 'row_actions'=>$row_actions, 'pages'=>$list['pages']));
	}
	
	/**
	 * Display list
	 * 
	 * @param object $object SurveyAnswer instance
	 * @param array $row_fields
	 * @param array $row_actions
	 * @return mixed
	 */
	private function _showList(SurveyAnswer $object, $row_fields, $row_actions, $question_id)
	{
		$object->attributes = SurveyAnswer::setSessionAttributes(Yii::app()->request->getPost('SurveyAnswer'));
		$criteria = $object->search()->criteria;
		$criteria->compare('survey_question_id', $question_id);
		$count = SurveyAnswer::model()->count($criteria);
		
		$pages = new CPagination($count);
		$criteria->limit = 25;
		$criteria->offset = (Yii::app()->request->getQuery('page', 1) * $criteria->limit) - $criteria->limit;
		
		$pages->pageSize=$criteria->limit;
		$pages->applyLimit($criteria);
		
		$list = SurveyAnswer::model()->findAll($criteria);
		$view = $this->renderPartial('/layouts/list-body', array('object'=>$object, 'objectList'=>$list, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions), TRUE);
		if (Yii::app()->request->isAjaxRequest) {
			die(CJSON::encode(array('status'=>1, 'message'=>$view)));
		}
		return array('pages'=>$pages, 'content'=>$view);
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		// validate question
		SurveyQuestion::validateObject($this->question, 'SurveyQuestion', $this->createUrl('survey/'));
		
		$this->actionButtons['add'] = array('back'=>array('link'=>$this->createURL($this->id.'/', array('token'=>$this->question->token))));
		
		$answer = new SurveyAnswer;
		$answer->survey_question_id = $this->question->survey_question_id;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($answer);
		}
		
		self::_renderForm($answer);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionUpdate($id)
	{
		// validate answer
		$answer = SurveyAnswer::model()->findByPk($id);
		SurveyAnswer::validateObject($answer, 'SurveyAnswer', $this->createUrl('survey/'));
		
		// validate question
		$question = SurveyQuestion::model()->findByPk($answer->survey_question_id);
		SurveyQuestion::validateObject($question, 'SurveyQuestion', $this->createUrl('survey/'));
		
		$this->actionButtons['update'] = array('back'=>array('link'=>$this->createURL($this->id.'/', array('token'=>$question->token))));
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($answer);
		}
		
		self::_renderForm($answer);
	}
	
	/**
	 * Update menu position
	 * 
	 * @access active
	 * @return string
	 */
	public function actionPosition()
	{
		$query = Yii::app()->request->getQuery('page-list');
		
		$answer = new SurveyAnswer;
		foreach ($query as $position => $id) {
			$answer->updateByPk($id, array('position'=>(int)$position + 1));
		}
		
		die('Repositioning successful.');
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object SurveyAnswer instance
	 * @return void
	 */
	private function _renderForm(SurveyAnswer $object)
	{
		$fields = array();
		
		$fields['answer']['type'] = 'text';
		$fields['answer']['class'] = 'span6';
		
		$fields['answer_type']['type'] = 'radio';
		$fields['answer_type']['class'] = 'col-lg-5';
		$fields['answer_type']['value'] = array('text'=>'Textbox', 'dropdown'=>'Dropdown list', 'checkbox'=>'Checkbox', 'radio'=>'Radio button');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object'=>$object, 'fields'=>$fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object SurveyAnswer instance
	 * @return	void
	 */
	private function _postValidate(SurveyAnswer $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('SurveyAnswer');
		$object->attributes = $post;

		if ($object->validate() && $object->save()) {
			self::displayFormSuccess();
		}
		self::displayFormError($object);
	}
}