<?php

class Access_pointController extends GatewayController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action))
		{
			// action buttons
			$this->actionButtons['index'] = array('add' => array('link' => $this->createURL($this->id.'/add')));
			$this->actionButtons['add'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back' => array('link' => $this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		// fields to be displayed
		$row_fields = array();
		$row_fields['access_point_id']['type'] = 'text';
		$row_fields['access_point_id']['class'] = 'text-center span1';
		
		$ap_groups = AccessPointGroup::model()->findAllByAttributes(array('portal_id'=>$this->context->portal->portal_id));
		$row_fields['access_point_group_id']['type'] = 'select';
		$row_fields['access_point_group_id']['class'] = 'span3';
		$row_fields['access_point_group_id']['parent'] = 'group';
		$row_fields['access_point_group_id']['child'] = 'name';
		$row_fields['access_point_group_id']['value'] = CHtml::listData($ap_groups, 'access_point_group_id', 'name');
		
		$row_fields['name']['type'] = 'text';
		$row_fields['name']['class'] = 'span6';
		
		$row_fields['is_active']['type'] = 'select';
		$row_fields['is_active']['class'] = 'span2';
		$row_fields['is_active']['value'] = array('1'=>'Enabled', '0' =>'Disabled');
		$row_fields['is_active']['mode'] = 'bool';
		
		$row_actions = array('edit', 'delete');
		
		$object = new AccessPoint();
		$list = self::_showList($object, $row_fields, $row_actions);
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$list['content'], 'row_fields'=>$row_fields, 'row_actions'=>$row_actions, 'pages'=>$list['pages']));
	}
	
	/**
	 * Display list
	 * 
	 * @param object $object AccessPoint instance
	 * @param array $row_fields
	 * @param array $row_actions
	 * @return mixed
	 */
	private function _showList(AccessPoint $object, $row_fields, $row_actions)
	{
		$object->attributes = AccessPoint::setSessionAttributes(Yii::app()->request->getPost('AccessPoint'));
		$criteria = $object->search()->criteria;
		$criteria->compare('portal.portal_id', $this->context->portal->portal_id);
		
		$count = AccessPoint::model()->with('group', 'portal')->count($criteria);
		
		$pages = new CPagination($count);
		$criteria->limit = 25;
		$criteria->offset = (Yii::app()->request->getQuery('page', 1) * $criteria->limit) - $criteria->limit;
		
		$pages->pageSize=$criteria->limit;
		$pages->applyLimit($criteria);
		
		$list = AccessPoint::model()->with('group', 'portal')->findAll($criteria);
		$view = $this->renderPartial('/layouts/list-body', array('object'=>$object, 'objectList'=>$list, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions), TRUE);
		if (Yii::app()->request->isAjaxRequest) {
			die(CJSON::encode(array('status'=>1, 'message'=>$view)));
		}
		return array('pages'=>$pages, 'content'=>$view);
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		$ap = new AccessPoint;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($ap);
		}
		
		self::_renderForm($ap);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$ap = AccessPoint::model()->findByPk($id);
		AccessPoint::validateObject($ap, 'AccessPoint');
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($ap);
		}
		
		self::_renderForm($ap);
	}
	
	/**
	 * Delete record
	 * 
	 * @access public
	 * @param int $id AccessPoint ID
	 * @return void
	 */
	public function actionDelete($id)
	{
		$ap = AccessPoint::model()->findByPk($id);
		AccessPoint::validateObject($ap, 'AccessPoint');
		
		$count = DeviceAvailment::model()->countByAttributes(array('access_point_id'=>$ap->access_point_id));
		if ($count) {
			Yii::app()->user->setFlash('fail', 'Cannot delete Access Point that already has been referenced.');
			$this->redirect(array("$this->id/"));
		}
		
		if (Yii::app()->request->isPostRequest) {
			// check for security token
			if (CPasswordHelper::verifyPassword($ap->access_point_id, Yii::app()->request->getPost('security_key'))) {
			
				// Delete record
				AccessPoint::model()->deleteByPk($ap->access_point_id);
				
				Yii::app()->user->setFlash('success', 'Delete successful!');
			}
			else {
				Yii::app()->user->setFlash('fail', 'Error in deletion.');
			}
			$this->redirect(array("$this->id/"));
		}
		$this->render('delete', array('ap'=>$ap));
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object AccessPoint instance
	 * @return void
	 */
	private function _renderForm(AccessPoint $object)
	{
		$fields = array();
		
		$ap_groups = AccessPointGroup::model()->findAllByAttributes(array('portal_id'=>$this->context->portal->portal_id));
		$fields['access_point_group_id']['type'] = 'select';
		$fields['access_point_group_id']['class'] = 'col-md-5';
		$fields['access_point_group_id']['value'] = CHtml::listData($ap_groups, 'access_point_group_id', 'name');
		
		$fields['name']['type'] = 'text';
		$fields['name']['class'] = 'span6';
		
		$fields['is_active']['type'] = 'radio';
		$fields['is_active']['class'] = 'col-lg-5';
		$fields['is_active']['value'] = array('1'=>'Enabled', '0'=>'Disabled');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object' => $object, 'fields' => $fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object AccessPoint instance
	 * @return	void
	 */
	private function _postValidate(AccessPoint $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('AccessPoint');
		$object->attributes = $post;

		if ($object->validate() && $object->save()) {
			self::displayFormSuccess();
		}
		self::displayFormError($object);
	}
}