<?php

class AppearanceController extends GatewayController
{
	/**
	 * View website appearance settings
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		if (Yii::app()->request->isPostRequest) {
			self::_handlePost();
		}
		
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/ckeditor/ckeditor.js'));
		
		$data = array(	'site_title' => PortalConfiguration::getValue('SITE_TITLE'),
						'landing_message' => PortalConfiguration::getValue('LANDING_MESSAGE'),
						'welcome' => PortalConfiguration::getValue('WELCOME_BACK'),
						'redirect_url' => PortalConfiguration::getValue('REDIRECT_URL'),
						'logo' => PortalConfiguration::getvalue('LOGO'),
						'inside_logo' => PortalConfiguration::getvalue('INSIDE_LOGO'),
						'center_image' => PortalConfiguration::getValue('CENTER_IMAGE'),
						'favicon' => PortalConfiguration::getvalue('FAVICON'),
						'splash' => PortalConfiguration::getvalue('SPLASH_IMG'),
						'powered_welcome' => PortalConfiguration::getvalue('POWERED_WELCOME'),
						'powered_inside' => PortalConfiguration::getvalue('POWERED_INSIDE'),
						'css_file' => PortalConfiguration::getValue('CSS_FILE'),
						'css_contents' => @file_get_contents(dirname(Yii::app()->request->scriptFile).'/css/portal/'.$this->context->portal->portal_id.'/'.PortalConfiguration::getValue('CSS_FILE')));
		
		$this->render('form', $data);
	}
	
	/**
	 * Delete appearance settings and files
	 * 
	 * @access public
	 * @param string $config
	 * @return void
	 */
	public function actionDelete($config)
	{
		switch ($config) {
			case 'LOGO':
			case 'INSIDE_LOGO':
			case 'CENTER_IMAGE':
			case 'FAVICON':
			case 'SPLASH_IMG':
				@unlink(dirname(Yii::app()->request->scriptFile).'/images/portal/'.$this->context->portal->portal_id.'/'.PortalConfiguration::getValue($config));
			break;

			case 'CSS_FILE':
				@unlink(dirname(Yii::app()->request->scriptFile).'/css/portal/'.$this->context->portal->portal_id.'/'.PortalConfiguration::getValue($config));
			break;
		}
		PortalConfiguration::setValue($config, '');
		
		Yii::app()->getModule('gateway')->admin->setFlash('success', 'Settings applied');
		$this->redirect(array($this->id.'/'));
	}
	
	/**
	 * Set configuration value
	 * 
	 * @access private
	 * @return void
	 */
	private function _handlePost()
	{
		$post = Yii::app()->request->getPost('PortalConfiguration');

		// create CSS
		if ( ! empty($post['CSS_FILE'])) {
			$post['CSS_FILE'] = self::_uploadStyle($post['CSS_FILE']);
		}
		
		foreach ($post as $config => $value) {
			PortalConfiguration::setValue($config, $value);
		}
		
		if ( ! empty($_FILES)) {
			self::_upload($_FILES);
		}
			
		Yii::app()->getModule('gateway')->admin->setFlash('success', 'Settings applied');
	}
	
	/**
	 * Upload images based on configuration
	 * 
	 * @access	private
	 * @return	void
	 */
	private function _upload($files)
	{
		foreach ($files as $name => $attr) {
			if ($attr['size'] && ! $attr['error']) {
				switch ($name) {
					case 'LOGO':
						$filename = 'logo';
						self::_uploadImage($name, $filename);
					break;
				
					case 'INSIDE_LOGO':
						$filename = 'inside_logo';
						self::_uploadImage($name, $filename);
					break;
				
					case 'CENTER_IMAGE':
						$filename = 'center_image';
						self::_uploadImage($name, $filename);
					break;
				
					case 'FAVICON':
						$filename = 'favicon';
						self::_uploadImage($name, $filename);
					break;
				
					case 'SPLASH_IMG':
						$filename = 'splash';
						self::_uploadImage($name, $filename);
					break;
				}
			}
		}
	}
	
	private function _uploadImage($name, $filename)
	{
		$image_path = dirname(Yii::app()->request->scriptFile).'/images/portal/'.$this->context->portal->portal_id.'/';
		if ( ! is_dir($image_path)) {
			mkdir($image_path, 0777, true);
		}
		
		$image = CUploadedFile::getInstanceByName($name);
		$filename = $filename.'.'.$image->extensionName;
		
		// delete current file
		$previous_file = PortalConfiguration::getValue($name);
		@unlink($image_path.$previous_file);
		
		// check for successful save
		if ($image->saveAs($image_path.$filename)) {
			PortalConfiguration::setvalue($name, $filename);
		}
	}
	
	private function _uploadStyle($css)
	{
		$css_path = dirname(Yii::app()->request->scriptFile).'/css/portal/'.$this->context->portal->portal_id.'/';
		if ( ! is_dir($css_path)) {
			mkdir($css_path, 0777, true);
		}
		
		$css_file = fopen($css_path.'styles.css', 'w');
		fwrite($css_file, $css);
		fclose($css_file);
		
		return 'styles.css';
	}
}