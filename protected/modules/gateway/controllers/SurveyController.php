<?php

class SurveyController extends GatewayController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action))
		{
			// action buttons
			$this->actionButtons['index'] = array('add' => array('link' => $this->createURL($this->id.'/add')));
			$this->actionButtons['add'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back' => array('link' => $this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		// declare scripts
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'plugins', 'jquery.tablednd.js')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/jquery.toastr.min.js'));
		
		// fields to be displayed
		$row_fields = array();
		$row_fields['survey_id']['type'] = 'text';
		$row_fields['survey_id']['class'] = 'text-center span1';
		
		$row_fields['title']['type'] = 'text';
		$row_fields['title']['class'] = 'span7';
		
		$row_fields['is_active']['type'] = 'select';
		$row_fields['is_active']['class'] = 'span2';
		$row_fields['is_active']['value'] = array('1'=>'Enabled', '0' =>'Disabled');
		$row_fields['is_active']['mode'] = 'bool';
		
		$row_fields['position']['type'] = 'text';
		$row_fields['position']['class'] = 'text-center span1 dragHandle';
		
		$row_actions = array('view', 'edit', 'delete');
		
		$object = new Survey();
		$list = self::_showList($object, $row_fields, $row_actions);
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$list['content'], 'row_fields'=>$row_fields, 'row_actions'=>$row_actions, 'pages'=>$list['pages']));
	}
	
	/**
	 * Display list
	 * 
	 * @param object $object Survey instance
	 * @param array $row_fields
	 * @param array $row_actions
	 * @return mixed
	 */
	private function _showList(Survey $object, $row_fields, $row_actions)
	{
		$object->attributes = Survey::setSessionAttributes(Yii::app()->request->getPost('Survey'));
		$criteria = $object->search()->criteria;
		$criteria->compare('portal_id', $this->context->portal->portal_id);
		$count = Survey::model()->count($criteria);
		
		$pages = new CPagination($count);
		$criteria->limit = 25;
		$criteria->offset = (Yii::app()->request->getQuery('page', 1) * $criteria->limit) - $criteria->limit;
		
		$pages->pageSize=$criteria->limit;
		$pages->applyLimit($criteria);
		
		$list = Survey::model()->findAll($criteria);
		$view = $this->renderPartial('/layouts/list-body', array('object'=>$object, 'objectList'=>$list, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions), TRUE);
		if (Yii::app()->request->isAjaxRequest) {
			die(CJSON::encode(array('status'=>1, 'message'=>$view)));
		}
		return array('pages'=>$pages, 'content'=>$view);
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		$survey = new Survey;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($survey);
		}
		
		self::_renderForm($survey);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$survey = Survey::model()->findByPk($id);
		Survey::validateObject($survey, 'Survey');
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($survey);
		}
		
		self::_renderForm($survey);
	}
	
	public function actionView($id)
	{
		$survey = Survey::model()->findByPk($id);
		parent::checkPortal($survey->portal_id);
		
		$url = $this->createUrl('question/', array('token'=>$survey->token));
		$this->redirect($url);
	}
	
	/**
	 * Update menu position
	 * 
	 * @access active
	 * @return string
	 */
	public function actionPosition()
	{
		$query = Yii::app()->request->getQuery('page-list');
		
		$survey = new Survey;
		foreach ($query as $position => $id) {
			$survey->updateByPk($id, array('position'=>(int)$position + 1));
		}
		
		die('Repositioning successful.');
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object Survey instance
	 * @return void
	 */
	private function _renderForm(Survey $object)
	{
		$fields = array();
		
		$fields['title']['type'] = 'text';
		$fields['title']['class'] = 'span6';
		
		$fields['is_active']['type'] = 'radio';
		$fields['is_active']['class'] = 'col-lg-5';
		$fields['is_active']['value'] = array('1'=>'Enabled', '0'=>'Disabled');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object' => $object, 'fields' => $fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object Survey instance
	 * @return	void
	 */
	private function _postValidate(Survey $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('Survey');
		$object->attributes = $post;
		$object->portal_id = $this->context->portal->portal_id;
		$object->token = Yii::app()->getSecurityManager()->generateRandomString(128);

		if ($object->validate() && $object->save()) {
			self::displayFormSuccess();
		}
		self::displayFormError($object);
	}
}