<?php

class DashboardController extends GatewayController
{
	public function actionIndex()
	{
		// declare scripts
		Yii::app()->clientScript->registerScriptFile(Link::js_url('bootstrap/bootstrap-datepicker.js'), CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/morris/morris.min.js'), CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/raphael/raphael-min.js'), CClientScript::POS_END);
		Yii::app()->clientScript->registerCssFile(Link::css_url('gateway/dashboard.css'));
		
		$this->render('index');
	}
	
	/**
	 * Count all user data within the given date
	 * 
	 * @acces public
	 * @return JSON
	 */
	public function actionPoll_all()
	{
		// Check if from AJAX request
		if ( ! Yii::app()->request->isAjaxRequest) {
			$this->redirect(array('dashboard/'));
		}
		
		$today = date('Ymd', strtotime(Yii::app()->request->getPost('date')));
		
		// Connected users
		$all_users = DeviceConnection::pollAll($today, 0, $this->context->portal->portal_id, 0, 0);
		$new_users = DeviceConnection::pollAllNew($today, 0, $this->context->portal->portal_id, 0, 0);
		$return_users = DeviceConnection::pollAllReturning($today, 0, $this->context->portal->portal_id, 0, 0);

		$view = $this->renderPartial('poll_all', array('all_users'=>$all_users,
			'new_users'=>$new_users,
			'return_users'=>$return_users,
			'today'=>$today), TRUE);
		
		die(CJSON::encode(array('status'=>1, 'message'=>$view)));
	}
	
	/**
	 * Count all authenticated user data within the given date
	 * 
	 * @acces public
	 * @return JSON
	 */
	public function actionPoll_auth()
	{
		// Check if from AJAX request
		if ( ! Yii::app()->request->isAjaxRequest) {
			$this->redirect(array('dashboard/'));
		}
		
		$today = date('Ymd', strtotime(Yii::app()->request->getPost('date')));
	
		// Authenticated users
		$all_auth_users = DeviceConnection::pollAuthAll($today, 0, $this->context->portal->portal_id, 0, 0);
		$new_auth_users = DeviceConnection::pollAuthNew($today, 0, $this->context->portal->portal_id, 0, 0);
		$return_auth_users = DeviceConnection::pollAuthReturning($today, 0, $this->context->portal->portal_id, 0, 0);
		
		$view = $this->renderPartial('poll_auth', array(
			'all_auth_users'=>$all_auth_users,
			'new_auth_users'=>$new_auth_users,
			'return_auth_users'=>$return_auth_users,
			'today'=>$today), TRUE);
		
		die(CJSON::encode(array('status'=>1, 'message'=>$view)));
	}
	
	/**
	 * Count all availment data within the given date
	 * 
	 * @acces public
	 * @return JSON
	 */
	public function actionPoll_avail()
	{
		$date = date('Y-m-d', strtotime(Yii::app()->request->getPost('date')));
		$today = date('Ymd', strtotime(Yii::app()->request->getPost('date')));
		
		// availment count
		$availments = Availment::model()->findAll();
		foreach ($availments as $a) {
			$availment_count[] = array('label'=>$a->name, 'value'=>DeviceAvailment::model()->count("portal_id = {$this->context->portal->portal_id} AND availment_id = {$a->availment_id} AND DATE(created_at) = '{$date}'"));
		}

		die(CJSON::encode(array('status'=>1, 'availment_count'=>$availment_count)));
	}
}