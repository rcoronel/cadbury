<?php

class Avail_settingController extends GatewayController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action))
		{
			$this->actionButtons['update'] = array('back'=>array('link'=>$this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		// declare scripts
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'plugins', 'jquery.tablednd.js')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/jquery.toastr.min.js'));
		
		// fields to be displayed
		$row_fields = array();
		$row_fields['availment_id']['type'] = 'text';
		$row_fields['availment_id']['class'] = 'text-center span1';
		
		$row_fields['name']['type'] = 'text';
		$row_fields['name']['class'] = 'span7';
		
		$row_actions = array('edit');
		
		$object = new Availment();
		$list = self::_showList($object, $row_fields, $row_actions);
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$list['content'], 'row_fields'=>$row_fields, 'row_actions'=>$row_actions, 'pages'=>$list['pages']));
	}
	
	/**
	 * Display list
	 * 
	 * @param object $object Availment instance
	 * @param array $row_fields
	 * @param array $row_actions
	 * @return mixed
	 */
	private function _showList(Availment $object, $row_fields, $row_actions)
	{
		$object->attributes = Availment::setSessionAttributes(Yii::app()->request->getPost('Availment'));
		$criteria = $object->search()->criteria;
		$count = Availment::model()->count($criteria);
		
		$pages = new CPagination($count);
		$criteria->limit = 25;
		$criteria->offset = (Yii::app()->request->getQuery('page', 1) * $criteria->limit) - $criteria->limit;
		
		$pages->pageSize=$criteria->limit;
		$pages->applyLimit($criteria);
		
		$list = Availment::model()->findAll($criteria);
		$view = $this->renderPartial('/layouts/list-body', array('object'=>$object, 'objectList'=>$list, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions), TRUE);
		if (Yii::app()->request->isAjaxRequest) {
			die(CJSON::encode(array('status'=>1, 'message'=>$view)));
		}
		return array('pages'=>$pages, 'content'=>$view);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$availment = Availment::model()->findByPk($id);
		Availment::validateObject($availment, 'Availment');
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($availment);
		}
		
		self::_renderForm($availment);
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object Availment instance
	 * @return void
	 */
	private function _renderForm(Availment $object)
	{
		$this->render($object->availment_id, array('object'=>$object));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object Availment instance
	 * @return	void
	 */
	private function _postValidate(Availment $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('AvailmentSetting');
		foreach ($post as $config=>$value) {
			AvailmentSetting::setValue($object->availment_id, $config, $value);
		}
	}
}