<?php

class ScenarioController extends GatewayController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action))
		{
			// action buttons
			$this->actionButtons['index'] = array('add' => array('link' => $this->createURL($this->id.'/add')));
			$this->actionButtons['add'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back' => array('link' => $this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * Display availment form
	 * 
	 * @access public
	 * @return void
	 */
	public function actionIndex()
	{
		// declare scripts
		Yii::app()->clientScript->registerCssFile(Link::css_url('gateway/plans.css'));
		
		// fields to be displayed
		$row_fields = array();
		$row_fields['scenario_id']['type'] = 'text';
		$row_fields['scenario_id']['class'] = 'text-center col-md-1';
		
		$row_fields['title']['type'] = 'text';
		$row_fields['title']['class'] = 'col-mid-4';
		
		$row_actions = array('edit', 'delete');
		
		$scenarios = Scenario::model()->with('new', 'returning')->findAll(array('condition'=>"portal_id = {$this->context->portal->portal_id}", 'order'=>'new.level ASC, returning.level ASC'));
		
		$this->render('index', array('object' => new Scenario, 'objectList'=>$scenarios, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions));
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		$scenario = new Scenario;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($scenario);
		}
		$nas_avails = PortalAvailment::model()->with('availment')->findAllByAttributes(array('portal_id'=>$this->context->portal->portal_id));
		$nas_avails_arr = CHtml::listdata($nas_avails, 'availment_id', function($a){return CHtml::encode($a->availment->name);});
		$this->render('form', array('availments'=>$nas_avails_arr, 'scenario'=>new Scenario));
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id Scenario primary key
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$scenario = Scenario::model()->findByPk($id);
		Scenario::validateObject($scenario, 'Scenario', $this->createUrl("{$this->id}/"));
		parent::checkPortal($scenario->portal_id);
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($scenario);
		}
		
		$nas_avails = PortalAvailment::model()->with('availment')->findAllByAttributes(array('portal_id'=>$this->context->portal->portal_id));
		$nas_avails_arr = CHtml::listdata($nas_avails, 'availment_id', function($a){return CHtml::encode($a->availment->name);});
		
		// Scenarios
		$new_scenarios = ScenarioAvailment::model()->findAllByAttributes(array('scenario_id'=>$scenario->scenario_id), array('order'=>'level ASC'));
		$returning_scenarios = ScenarioReturning::model()->findAllByAttributes(array('scenario_id'=>$scenario->scenario_id), array('order'=>'level ASC'));
		$this->render('form', array('availments'=>$nas_avails_arr, 'scenario'=>$scenario, 'new_scenarios'=>$new_scenarios, 'returning_scenarios'=>$returning_scenarios));
	}
	
	/**
	 * Delete record
	 * 
	 * @access public
	 * @param int $id Scenario ID
	 * @return void
	 */
	public function actionDelete($id)
	{
		$scenario = Scenario::model()->findByPk($id);
		Scenario::validateObject($scenario, 'Scenario');
		parent::checkPortal($scenario->portal_id);
		
		if (Yii::app()->request->isPostRequest) {
			// check for security token
			if (CPasswordHelper::verifyPassword($scenario->scenario_id, Yii::app()->request->getPost('security_key'))) {
				// Delete scenarios
				ScenarioAvailment::model()->deleteAllByAttributes(array('scenario_id'=>$scenario->scenario_id));
				ScenarioReturning::model()->deleteAllByAttributes(array('scenario_id'=>$scenario->scenario_id));
				DeviceScenario::model()->deleteAllByAttributes(array('portal_id'=>$this->context->portal->portal_id));
				
				// Delete record
				Scenario::model()->deleteByPk($scenario->scenario_id);
				Yii::app()->user->setFlash('success', 'Delete successful!');
				$this->redirect(array("$this->id/"));
			}
			else {
				Yii::app()->user->setFlash('fail', 'Error in deletion.');
				$this->redirect(array("$this->id/"));
			}
		}
		$this->render('delete', array('scenario'=>$scenario));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object Scenario instance
	 * @return	void
	 */
	private function _postValidate(Scenario $object)
	{
		sleep(1);
		
		$post = $_POST;
		$object->title = $post['title'];
		$object->portal_id = $this->context->portal->portal_id;
		if ($object->validate() && $object->save()) {
			// Delete existing
			ScenarioAvailment::model()->deleteAllByAttributes(array('scenario_id'=>$object->scenario_id));
			ScenarioReturning::model()->deleteAllByAttributes(array('scenario_id'=>$object->scenario_id));
			
			// New user scenario
			if ( ! empty($post['new']['availment_id'])) {
				foreach ($post['new']['availment_id'] as $index => $availment_id) {
					$new_scenario = new ScenarioAvailment;
					$new_scenario->scenario_id = $object->scenario_id;
					$new_scenario->level = $post['new']['level'][$index];
					$new_scenario->availment_id = $post['new']['availment_id'][$index];
					$new_scenario->validate() && $new_scenario->save();
				}
				
				foreach ($post['returning']['availment_id'] as $index => $availment_id) {
					$return_scenario = new ScenarioReturning();
					$return_scenario->scenario_id = $object->scenario_id;
					$return_scenario->level = $post['returning']['level'][$index];
					$return_scenario->availment_id = $post['returning']['availment_id'][$index];
					$return_scenario->validate() && $return_scenario->save();
				}
			}
			
			// Delete DeviceScenario in order to reset availment levels
			DeviceScenario::model()->deleteAllByAttributes(array('portal_id'=>$this->context->portal->portal_id));
			
			self::displayFormSuccess();
		}
		self::displayFormError($object);
	}
	
	/**
	 * View
	 * 
	 * @access public
	 * @return void
	 */
	public function actionView($id)
	{
		$availment = Availment::model()->findByPk($id);
		Availment::validateObject($availment, 'Availment');
		
		$this->render('view', array('availment'=>$availment));
	}
}