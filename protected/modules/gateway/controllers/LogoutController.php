<?php

class LogoutController extends GatewayController
{	
	public function actionIndex()
	{
		Yii::app()->getModule('gateway')->admin->logout();
		$this->redirect(array('login/'));
	}
}