<?php

class QuestionController extends GatewayController
{
	public $survey;
	
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		$this->survey = Survey::model()->findByAttributes(array('token'=>Yii::app()->request->getParam('token')));
		$survey_id = Yii::app()->request->getParam('survey_id');
		
		if (parent::beforeAction($action))
		{
			// action buttons
			
			

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @param string $token
	 * @return	void
	 */
	public function actionIndex()
	{
		$id = Yii::app()->request->getParam('id');
		if ($id) {
			$question = SurveyQuestion::model()->with('survey')->findByPk($id);
			SurveyQuestion::validateObject($question, 'SurveyQuestion', $this->createUrl('survey/'));
			
			$this->redirect($this->createURL($this->id.'/', array('token'=>$question->survey->token)));
		}
		
		// validate survey
		Survey::validateObject($this->survey, 'Survey', $this->createUrl('survey/'));
		
		$this->actionButtons['index'] = array(
			'add'=>array('link'=>$this->createURL($this->id.'/add', array('token'=>$this->survey->token))),
			'back'=>array('link'=>$this->createURL('survey/')));
		
		// declare scripts
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'plugins', 'jquery.tablednd.js')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/jquery.toastr.min.js'));
		
		// fields to be displayed
		$row_fields = array();
		$row_fields['survey_question_id']['type'] = 'text';
		$row_fields['survey_question_id']['class'] = 'text-center span1';
		
		$row_fields['question']['type'] = 'text';
		$row_fields['question']['class'] = 'span5';
		
		$row_fields['is_required']['type'] = 'select';
		$row_fields['is_required']['class'] = 'span2';
		$row_fields['is_required']['value'] = array('1'=>'Required', '0' =>'Not required');
		$row_fields['is_required']['mode'] = 'bool';
		
		$row_fields['is_active']['type'] = 'select';
		$row_fields['is_active']['class'] = 'span2';
		$row_fields['is_active']['value'] = array('1'=>'Enabled', '0' =>'Disabled');
		$row_fields['is_active']['mode'] = 'bool';
		
		$row_fields['position']['type'] = 'text';
		$row_fields['position']['class'] = 'text-center span1 dragHandle';
		
		$row_actions = array('view', 'edit', 'delete');
		
		$object = new SurveyQuestion();
		$list = self::_showList($object, $row_fields, $row_actions, $this->survey->survey_id);
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$list['content'], 'row_fields'=>$row_fields, 'row_actions'=>$row_actions, 'pages'=>$list['pages']));
	}
	
	/**
	 * Display list
	 * 
	 * @param object $object SurveyQuestion instance
	 * @param array $row_fields
	 * @param array $row_actions
	 * @return mixed
	 */
	private function _showList(SurveyQuestion $object, $row_fields, $row_actions, $survey_id)
	{
		$object->attributes = SurveyQuestion::setSessionAttributes(Yii::app()->request->getPost('SurveyQuestion'));
		$criteria = $object->search()->criteria;
		$criteria->compare('survey_id', $survey_id);
		$count = SurveyQuestion::model()->count($criteria);
		
		$pages = new CPagination($count);
		$criteria->limit = 25;
		$criteria->offset = (Yii::app()->request->getQuery('page', 1) * $criteria->limit) - $criteria->limit;
		
		$pages->pageSize=$criteria->limit;
		$pages->applyLimit($criteria);
		
		$list = SurveyQuestion::model()->findAll($criteria);
		$view = $this->renderPartial('/layouts/list-body', array('object'=>$object, 'objectList'=>$list, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions), TRUE);
		if (Yii::app()->request->isAjaxRequest) {
			die(CJSON::encode(array('status'=>1, 'message'=>$view)));
		}
		return array('pages'=>$pages, 'content'=>$view);
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		// validate survey
		Survey::validateObject($this->survey, 'Survey', $this->createUrl('survey/'));
		
		$this->actionButtons['add'] = array('back'=>array('link'=>$this->createURL($this->id.'/', array('token'=>$this->survey->token))));
		
		$question = new SurveyQuestion;
		$question->survey_id = $this->survey->survey_id;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($question);
		}
		
		self::_renderForm($question);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionUpdate($id)
	{
		// validate question
		$question = SurveyQuestion::model()->findByPk($id);
		SurveyQuestion::validateObject($question, 'SurveyQuestion', $this->createUrl('survey/'));
		
		// validate survey
		$this->survey = Survey::model()->findByPk($question->survey_id);
		Survey::validateObject($this->survey, 'Survey', $this->createUrl('survey/'));
		
		$this->actionButtons['update'] = array('back'=>array('link'=>$this->createURL($this->id.'/', array('token'=>$this->survey->token))));
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($question);
		}
		
		self::_renderForm($question);
	}
	
	public function actionView($id)
	{
		$question = SurveyQuestion::model()->with('survey')->findByPk($id);
		parent::checkPortal($question->survey->portal_id);
		
		$url = $this->createUrl('answer/', array('token'=>$question->token));
		$this->redirect($url);
	}
	
	/**
	 * Update menu position
	 * 
	 * @access active
	 * @return string
	 */
	public function actionPosition()
	{
		$query = Yii::app()->request->getQuery('page-list');
		
		$question = new SurveyQuestion;
		foreach ($query as $position => $id) {
			$question->updateByPk($id, array('position'=>(int)$position + 1));
		}
		
		die('Repositioning successful.');
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object SurveyQuestion instance
	 * @return void
	 */
	private function _renderForm(SurveyQuestion $object)
	{
		$fields = array();
		
		$fields['question']['type'] = 'text';
		$fields['question']['class'] = 'span6';
		
		$fields['is_required']['type'] = 'radio';
		$fields['is_required']['class'] = 'col-lg-5';
		$fields['is_required']['value'] = array('1'=>'Required', '0'=>'Not required');
		
		$fields['is_active']['type'] = 'radio';
		$fields['is_active']['class'] = 'col-lg-5';
		$fields['is_active']['value'] = array('1'=>'Enabled', '0'=>'Disabled');
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object'=>$object, 'fields'=>$fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object SurveyQuestion instance
	 * @return	void
	 */
	private function _postValidate(SurveyQuestion $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('SurveyQuestion');
		$object->attributes = $post;
		$object->token = Yii::app()->getSecurityManager()->generateRandomString(128);

		if ($object->validate() && $object->save()) {
			self::displayFormSuccess();
		}
		self::displayFormError($object);
	}
}