<?php

class AvailmentController extends GatewayController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action)) {
			return true;
		}
	}
	
	/**
	 * Display availment form
	 * 
	 * @access public
	 * @return void
	 */
	public function actionIndex()
	{
		
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/jquery.toastr.min.js'));
		
		if (Yii::app()->request->isPostRequest) {
			self::_validatePost();
		}
		
		$availments = Availment::model()->findAll("Is_active = 1 ORDER BY Name");
		$nas_avails = PortalAvailment::model()->findAllByAttributes(array('portal_id'=>$this->context->portal->portal_id));
		foreach ($availments as $a) {
			$a->is_available = 0;
			$a->duration = 0;
			if ( ! empty($nas_avails)) {
				foreach ($nas_avails as $na) {
					if ($na->availment_id == $a->availment_id) {
						$a->is_available = 1;
						$a->duration = $na->duration / 60;
						$a->is_recurring = $na->is_recurring;
						$a->description = $na->description;
					}
				}
			}
		}
		
		$this->render('index', array('availments'=>$availments));
	}

	public function actionUpdate()
	{
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/jquery.toastr.min.js'));
		
		if (Yii::app()->request->isPostRequest) {
			self::_validatePost();
		}
		
		$availments = Availment::model()->findAll("Is_active = 1");
		$nas_avails = PortalAvailment::model()->findAllByAttributes(array('portal_id'=>$this->context->portal->portal_id));
		foreach ($availments as $a) {
			$a->is_available = 0;
			$a->duration = 0;
			if ( ! empty($nas_avails)) {
				foreach ($nas_avails as $na) {
					if ($na->availment_id == $a->availment_id) {
						$a->is_available = 1;
						$a->duration = $na->duration / 60;
						$a->is_recurring = $na->is_recurring;
					}
				}
			}
		}
		
		$this->render('index', array('availments'=>$availments));
	}
	
	/**
	 * View
	 * 
	 * @access public
	 * @return void
	 */
	public function actionView($id)
	{
		$availment = Availment::model()->findByPk($id);
		Availment::validateObject($availment, 'Availment');
		
		$this->render('view', array('availment'=>$availment));
	}
	
	/**
	 * Validate the post
	 * 
	 * @access private
	 * @return string
	 */
	private function _validatePost()
	{
		sleep(1);
		$post = Yii::app()->request->getPost('status');
		PortalAvailment::model()->deleteAllByAttributes(array('portal_id'=>$this->context->portal->portal_id));
		foreach ($post as $id=>$availability) {
			if ($availability) {
				$nas_avail = new PortalAvailment;
				$nas_avail->portal_id = $this->context->portal->portal_id;
				$nas_avail->availment_id = $id;
				$nas_avail->duration = (int)($_POST['duration'][$id] * 60);
				$nas_avail->is_recurring = (int)$_POST['recurring'][$id];
				$nas_avail->description = $_POST['description'][$id];
				$nas_avail->validate() && $nas_avail->save();
			}
		}
		
		die(CJSON::encode(array('status'=>1, 'message'=>'Settings saved')));
	}
	
}