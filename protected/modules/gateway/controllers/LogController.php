<?php

class LogController extends GatewayController
{
	public function actionIndex()
	{
		// declare scripts
		Yii::app()->clientScript->registerScriptFile(Link::js_url('bootstrap/bootstrap-datepicker.js'), CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/morris/morris.min.js'), CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/raphael/raphael-min.js'), CClientScript::POS_END);
		Yii::app()->clientScript->registerCssFile(Link::css_url('gateway/dashboard.css'));
		
		$this->render('index');
	}
	
	/**
	 * Count all user data within the given date
	 * 
	 * @acces public
	 * @return JSON
	 */
	public function actionPoll_aruba()
	{
		// Check if from AJAX request
		if ( ! Yii::app()->request->isAjaxRequest) {
			$this->redirect(array('log/'));
		}
		
		$today = date('Ymd', strtotime(Yii::app()->request->getPost('date')));
		$count = ArubaLog::countStatus($today, 0, $this->context->portal->portal_id, 0, 0);
		
		foreach ($count as $c) {
			$aruba_count[] = array('label'=>"CODE: {$c['status']}", 'value'=>$c['count']);
		}
		
		die(CJSON::encode(array('status'=>1, 'aruba_count'=>$aruba_count)));
	}
}