<?php

class CmsController extends GatewayController
{
	/**
	 * Invoked before any action is executed
	 * 
	 * @access	protected
	 * @param object $action CAction instance
	 * @return	void
	 */
	protected function beforeAction($action)
	{
		if (parent::beforeAction($action))
		{
			// action buttons
			$this->actionButtons['index'] = array('add' => array('link' => $this->createURL($this->id.'/add')));
			$this->actionButtons['add'] = array('back' => array('link' => $this->createURL($this->id.'/')));
			$this->actionButtons['update'] = array('back' => array('link' => $this->createURL($this->id.'/')));

			return true;
		}
	}
	
	/**
	 * List records
	 * 
	 * @access	public
	 * @return	void
	 */
	public function actionIndex()
	{
		// declare scripts
		Yii::app()->clientScript->registerScriptFile(Link::js_url(array('jquery', 'plugins', 'jquery.tablednd.js')));
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/jquery.toastr.min.js'));
		
		// fields to be displayed
		$row_fields = array();
		
		$row_fields['title']['type'] = 'text';
		$row_fields['title']['class'] = 'span7';
		
		$row_fields['friendly_url']['type'] = 'text';
		$row_fields['friendly_url']['class'] = 'span2';
		
		$row_actions = array('edit', 'delete');
		
		$object = new Cms();
		$list = self::_showList($object, $row_fields, $row_actions);
		$this->render('/layouts/list', array('object'=>$object, 'objectList'=>$list['content'], 'row_fields'=>$row_fields, 'row_actions'=>$row_actions, 'pages'=>$list['pages']));
	}
	
	/**
	 * Display list
	 * 
	 * @param object $object Cms instance
	 * @param array $row_fields
	 * @param array $row_actions
	 * @return mixed
	 */
	private function _showList(Cms $object, $row_fields, $row_actions)
	{
		$object->attributes = Cms::setSessionAttributes(Yii::app()->request->getPost('Cms'));
		$criteria = $object->search()->criteria;
		$criteria->compare('portal_id', $this->context->portal->portal_id);
		$count = Cms::model()->count($criteria);
		
		$pages = new CPagination($count);
		$criteria->limit = 25;
		$criteria->offset = (Yii::app()->request->getQuery('page', 1) * $criteria->limit) - $criteria->limit;
		
		$pages->pageSize=$criteria->limit;
		$pages->applyLimit($criteria);
		
		$list = Cms::model()->findAll($criteria);
		$view = $this->renderPartial('/layouts/list-body', array('object'=>$object, 'objectList'=>$list, 'row_fields'=>$row_fields, 'row_actions'=>$row_actions), TRUE);
		if (Yii::app()->request->isAjaxRequest) {
			die(CJSON::encode(array('status'=>1, 'message'=>$view)));
		}
		return array('pages'=>$pages, 'content'=>$view);
	}
	
	/**
	 * Add record
	 * 
	 * @access public
	 * @return void
	 */
	public function actionAdd()
	{
		$cms = new Cms;
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($cms);
		}
		
		self::_renderForm($cms);
	}
	
	/**
	 * Update record
	 * 
	 * @access public
	 * @param int $id Cms primary key
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$cms = Cms::model()->findByPk($id);
		Cms::validateObject($cms, 'Cms', $this->createUrl("{$this->id}/"));
		parent::checkPortal($cms->portal_id);
		
		if (Yii::app()->request->isPostRequest) {
			self::_postValidate($cms);
		}
		
		self::_renderForm($cms);
	}
	
	/**
	 * Delete record
	 * 
	 * @access public
	 * @param int $id Cms ID
	 * @return void
	 */
	public function actionDelete($id)
	{
		$cms = Cms::model()->findByPk($id);
		Cms::validateObject($cms, 'Cms', $this->createUrl("{$this->id}/"));
		parent::checkPortal($cms->portal_id);
		
		if (Yii::app()->request->isPostRequest) {
			// check for security token
			if (CPasswordHelper::verifyPassword($cms->cms_id, Yii::app()->request->getPost('security_key'))) {
				// Delete record
				Cms::model()->deleteByPk($cms->cms_id);
				
				Yii::app()->user->setFlash('success', 'Delete successful!');
				$this->redirect(array("$this->id/"));
			}
			else {
				Yii::app()->user->setFlash('fail', 'Error in deletion.');
				$this->redirect(array("$this->id/"));
			}
		}
		$this->render('delete', array('object'=>$cms));
	}
	
	/**
	 * Render form
	 * 
	 * @access private
	 * @param object $object Cms instance
	 * @return void
	 */
	private function _renderForm(Cms $object)
	{
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/plugins/ckeditor/ckeditor.js'));
		
		$fields = array();
		
		$fields['title']['type'] = 'text';
		$fields['title']['class'] = 'col-md-5';
		
		$fields['friendly_url']['type'] = 'text';
		$fields['friendly_url']['class'] = 'col-md-5';
		
		$fields['content']['type'] = 'wysiwyg';
		$fields['content']['class'] = 'col-lg-8';
		
		// AJAX submission
		$fields['xmlhttprequest']['value'] = true;
		
		$this->render('/layouts/form', array('object' => $object, 'fields' => $fields));
	}
	
	/**
	 * Validate the form
	 * 
	 * @access	public
	 * @param object $object Cms instance
	 * @return	void
	 */
	private function _postValidate(Cms $object)
	{
		sleep(1);
		
		$post = Yii::app()->request->getPost('Cms');
		$object->attributes = $post;
		$object->portal_id = $this->context->portal->portal_id;

		if ($object->validate() && $object->save()) {
			self::displayFormSuccess();
		}
		self::displayFormError($object);
	}
}