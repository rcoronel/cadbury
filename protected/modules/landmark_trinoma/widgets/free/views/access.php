<div class="row">
	<div class="col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
		<div class="default-wrapper">

			<div class="captive-portal-sub-heading">
				<p><?php echo $message;?></p>
			</div>
			<div id="default-to-action-btn-wrapper">
				<?php echo CHtml::htmlButton('CONTINUE', array('class'=>'btn primary-btn', 'onclick'=>"window.location='{$url}'"));?>
			</div>
			<footer>
				<?php echo CHtml::image(Link::image_url('powered-by-globe-modal.png'), 'Globe');?>
			</footer>
		</div>
	</div>
</div>