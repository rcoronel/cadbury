<div class="row terms-con-holder">
	<div class="col-xs-12 col-sm-10 col-sm-offset-1">
		<h3>Privacy Policy</h3>
		<div class="row conditions-holder">
			<div class="col-xs-12 conditions-content">
				<?php if ( ! empty($policy)):?>
					<?php echo $policy->content;?>
				<?php endif;?>

				<div class="conditions-content-btn">
					<a href="<?php echo $url;?>" class="btn btn-wifi">Go Back</a>
				</div>
			</div>
		</div>
	</div>
</div>

