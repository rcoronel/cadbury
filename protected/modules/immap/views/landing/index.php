<div class="column main-body">
	<div class="inner-body">

			<div class="header">
				<?php if ($this->logo):?>
					<?php echo CHtml::image($this->logo, $this->site_title);?>
				<?php endif;?>
			</div>

			<div class="body-content">
			   <div class="inner-body">
				<p>Sorry. WiFi service connection is only available between 07:00AM to 10:00PM</p>
			</div>
		</div>

		<div class="footer">
			<?php echo CHtml::image(Link::image_url("portal/{$this->portal->portal_id}/powered-by-logo.png"), $this->site_title);?>
		</div>
	</div>
</div>