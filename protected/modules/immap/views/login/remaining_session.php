<div class="column main-body">
	<div class="inner-body">
		<div class="header">
			<div class="logo">
				<?php if ($this->logo):?>
					<?php echo CHtml::image($this->logo, $this->site_title);?>
				<?php endif;?>
			</div>
		</div>
		<div class="body-content">
			<div class="inner-body">
				<div class="welcom-back-container">
					<h2>Welcome back</h2>
					<h1 class="welcome-num"><?php echo $this->user;?></h1>
					<p>You may continue using our free WIFI service.</p>
				</div>

				<?php echo CHtml::beginForm('', 'post', array('class'=>'form-class'));?>
					<div class="form-error" role="alert" >
						<span class="error-message"></span>
					</div>
					<?php echo CHtml::hiddenField('availment_id', $availment->availment_id);?>
					<?php echo CHtml::htmlButton('CONTINUE', array('type'=>'submit', 'class'=>'get-connect button'));?>
				<?php echo CHtml::endForm();?>
			</div>
		</div>
		<div class="footer">
			<?php echo CHtml::image(Link::image_url("portal/{$this->portal->portal_id}/powered-by-logo.png"), $this->site_title);?>
		</div>
	</div>
</div>

<div class="modal modal-wifi in" id="loader-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-body small-modal-body-style">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	
	$('form').on('submit',function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		//e.preventDefault();
	});
});
</script>