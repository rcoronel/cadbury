<div class="column main-body">
	<div class="inner-body">
		<div class="header">
			<div class="logo">
				<?php if ($this->logo):?>
					<?php echo CHtml::image($this->logo, $this->site_title);?>
				<?php endif;?>
			</div>
		</div>
		<div class="body-content">
			<div class="inner-body">
				<?php if ( ! empty($availments)):?>
					<?php foreach ($availments as $a):?>
						<?php echo $a->availment->description; ?>
						<?php $this->widget("{$this->portal->code}.widgets.{$a->availment->code}", array('self' => $a));?>
					<?php endforeach;?>
				<?php else:?>
					<div class="welcom-back-container">
						<p>Error encountered.</p>
					</div>
				<?php endif;?>
			</div>
		</div>
		<div class="footer">
			<?php echo CHtml::image(Link::image_url("portal/{$this->portal->portal_id}/powered-by-logo.png"), $this->site_title);?>
		</div>
	</div>
</div>