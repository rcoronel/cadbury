<?php

class Register extends CWidget
{
	public $self;
	public $self_portal;
	public $action;
	public $reference;
	
	public function init()
	{
		// Set PortalAvailment instance
		$this->self_portal = PortalAvailment::model()->findByAttributes(array('portal_id'=>$this->owner->portal->portal_id, 'availment_id'=>$this->self->availment_id));
	}
	
	public function run()
	{
		switch ($this->action)
		{
			// Login or Register function
			case 'login':
				self::_login();
			break;
		
			// Display the buttons
			default:
				self::_display();
			break;
		}
	}
	
	/**
	 * Display the widget
	 * 
	 * @access private
	 * @return void
	 */
	private function _display()
	{
		$this->owner->redirect(array('login/', 'connection_token'=>$this->owner->connection->connection_token, 'action'=>'login', 'availment_id'=>$this->self->availment_id));
		//$this->render('login', array('availment_id'=>$this->self->availment_id));
    }
	
	/**
	 * Login or Register the user to the NAS
	 * 
	 * @access private
	 * @return void
	 */
	private function _login()
	{
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/bootstrap-select.min.js'));
        Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/jquery.date-dropdowns.min.js'));
				
		// Check if already availed for today
		$availed = DeviceScenario::model()->exists("device_id = {$this->owner->device->device_id} AND portal_id = {$this->owner->portal->portal_id} AND availment_id = {$this->self->availment_id} AND DATE(updated_at) = DATE(NOW())");
		if ($availed) {
			$this->owner->redirect(array('access', 'connection_token'=>$this->owner->connection->connection_token, 'availment_id'=>$this->self->availment_id));
		}
		
		$object = new AvailRegistration;
		if (Yii::app()->request->isPostRequest) {
			self::_validatePost($object);
		}
		
		$this->render('index', array('availment_id'=>$this->self->availment_id,
			'object'=>$object,
			'duration'=>$this->self_portal->duration/60));
	}
	
	/**
	 * Validate registration fields
	 * 
	 * @access private
	 * @param object $object AvailRegistration
	 * @return JSON
	 */
	private function _validatePost(AvailRegistration $object)
	{
		$post = Yii::app()->request->getPost('AvailRegistration');
		$object->attributes = $post;
		
		if ($object->validate()){
			// New instance of DeviceAvailment
			$dev_avail = $this->owner->saveDeviceAvailment($this->self->availment_id);
			
			if ($dev_avail->validate()) {
				if (self::_updateConnection($object, $this->self_portal) && $dev_avail->save()) {
					// Save record
					$object->device_availment_id = $dev_avail->device_availment_id;
					$object->save();
					
					$url = $this->owner->createAbsoluteUrl('access/', array('connection_token'=>$this->owner->connection->connection_token,
						'availment_id'=>$this->self->availment_id));
					$minutes = $this->self_portal->duration/60;
					$json_array = array('status'=>1, 'message'=>"<h3>You now have {$minutes} minutes FREE access </h3> </br> Thank you and enjoy your <strong>FREE WiFi access", 'redirect'=>$url);
					die(CJSON::encode($json_array));
				}
			}
		}
		else {
			$json_array = array('status'=>0, 'message'=>'<i class="glyphicon glyphicon-exclamation-sign"></i> All fields are required');
			die(CJSON::encode($json_array));
		}
	}
	
	/**
	 * Update connection details
	 * 
	 * @access private
	 * @param object $object AvailRegistration instance
	 * @param object $availment PortalAvailment instance
	 * @return boolean
	 */
	private function _updateConnection(AvailRegistration $object, PortalAvailment $availment)
	{
		$minutes = $availment->duration/60;
		$connection = $this->owner->connection;
		$connection->username = $this->owner->device->mac_address.'@'.$this->owner->portal->code;
		$connection->password = $object->email;
		
		if ($connection->validate() && $connection->save()) {
			// Correlor log
			$data = array('actionType'=>'access_point_login',
							'itemId'=>$this->owner->connection->location_id,
							'actiontime'=>date('YmdHis+0800'),
							'details'=>array(array('name'=>'max_session_time', 'value'=>$minutes)));
			Correlor::reportAction($this->owner->device->device_id, 'access_point_login', $data);
			
			// Verify connection
			if ($this->owner->connect($availment, TRUE)) {
				return TRUE;
			}
		}
		throw new CHttpException(500, Yii::t('yii','Cannot update connection. Please try again.'));
	}
}