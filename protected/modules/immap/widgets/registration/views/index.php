<section class="row">
	
		<div class="column main-body">
			<div class="large-6 columns">
				<div class="hide-for-small-only">
					<div class="logo">
						<?php if ($this->inside_logo):?>
							<?php echo CHtml::image($this->inside_logo, $this->site_title);?>
						<?php endif;?>
					</div>
				</div>
			</div>
	
			<div class="large-6 columns">
	
				<div class="header">
					<!-- this logo will show on small devices -->
					<div class="show-for-small-only">
						<div class="logo">
							<img src="./assets/img/immap-logo.png" alt="IMMAP">
						</div>
					</div>
	
					<h1>WELCOME!</h1>
					<p>To enjoy your free WIFI please fill up the following </p>
				</div>
	
				<!-- form -->
				<form action="" class="form-class">
	
					<input type="text" name="name" placeholder="Name">
	
					<input type="text" name="email" placeholder="Email">
	
					<input type="number" name="mobile" placeholder="Mobile Number">
	
					<button class="submit-btn button" type="submit">SUBMIT</button>
				</form>
	
				<div class="footer">
					<img src="./assets/img/powered-by-logo.png" alt="" class="poweredby">
				</div>
	
			</div>
		</div><!-- end column -->
	
	</section>
<div class="row">
	<div class="col-sm-6 col-sm-offset-3 text-field">
		<div class="row">
			<div class="col-xs-12">
				<h3>Registration Form</h3>
			</div>
			<div id="form-error" class="alert alert-danger col-sm-4 col-sm-offset-4" role="alert" style="display:none;"></div>
			<div class="col-xs-12 title-qa">
				<span>
					Please input your registration details to <br>
					enjoy <?php echo $duration;?> minutes of 
					<strong>FREE WiFi access</strong>
				</span>
			</div>
		</div>
		<?php $form=$this->beginWidget('CActiveForm', array('method'=>'post', 'action'=>'')); ?>
			<?php echo CHtml::hiddenField('availment_id', $availment_id);?>
			<div class="form-group">
				<?php echo $form->labelEx($object,'firstname', array('class'=>'sr-only')); ?>
				<?php echo $form->textField($object,'firstname',array('class'=>'form-control', 'placeholder'=>'First Name')); ?>
				
			</div>
			<div class="form-group">
				<?php echo $form->labelEx($object,'lastname', array('class'=>'sr-only')); ?>
				<?php echo $form->textField($object,'lastname',array('class'=>'form-control', 'size'=>32,'maxlength'=>32, 'placeholder'=>'Last Name')); ?>
			</div>
			<div class="form-group clearfix">
				<div class="radio-holder">
				   <div class="radio-wrapper">
						<label>
						  <input type="radio" id="male-value" value="1" name="AvailRegistration[gender]">
						  <span class="fake-box" for="male-value"></span>
						  <span class="radio-text">Male</span>
						</label>
					</div>
					<div class="radio-wrapper">
						<label>
						  <input type="radio" id="female-value" value="2"  name="AvailRegistration[gender]">
						  <span class="fake-box" for="female-value"></span>
						  <span class="radio-text">Female</span>
						</label>
					</div>
				</div>
				<?php echo $form->error($object,'gender', array('class'=>'alert alert-danger'));?>
			</div>
			<div class="form-group">
				<?php echo $form->labelEx($object,'email',array('class'=>'sr-only')); ?>
				<?php echo $form->textField($object,'email', array('class'=>'form-control', 'placeholder'=>'Email Address')); ?>
				<?php echo $form->error($object,'email', array('class'=>'alert alert-danger')); ?>
			</div>
			<div class="">
				<div class="date-picker-holder">
					<div class="date-picker">
						<input type="hidden" name="AvailRegistration[birthday]" id="date-picker">
					</div>
				</div>
				<?php echo $form->error($object,'birthday', array('class'=>'alert alert-danger')); ?>
			</div>
			<div class="conditions-content-btn">
				<?php echo CHtml::htmlButton('REGISTER', array('type'=>'submit', 'class'=>'s-btn serendra-primary-btn'));?>
				<?php echo CHtml::htmlButton('BACK', array('type'=>'button', 'class'=>'s-btn serendra-secondary-btn', 'onclick'=>"window.location='{$this->owner->createAbsoluteUrl('login/', array('connection_token'=>$this->owner->connection->connection_token))}'"));?>
            </div>
		
		<?php $this->endWidget();?>
	</div> 
</div>


<div class="modal fade" id="loader-modal" data-backdrop="static" data-keyboard="false" tabindex="-1">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$("#date-picker").dateDropdowns({
		displayFormat: "mdy",
		daySuffixes: false
	});
	$('form').on('submit', function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		// hide errors
		$('div#form-error').slideUp( 300 ).html();

		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");

		$.ajax( {
			url : formURL,
			type: "POST",
			data : postData,
			dataType: 'json',
			success:function(data, textStatus, jqXHR)
			{
				if ( ! data.status) {
					setTimeout(function() {
						$('#loader-modal').modal('hide');
						$('div#form-error').slideDown(300).delay(800).html(data.message);
						$('div#form-error').show();
					}, 900);
				}
				else {
					$('div.modal-body p').html(data.message);
					$('#loader-modal').modal({backdrop: 'static', keyboard: false});
					setTimeout(function(){
						window.location.href = data.redirect;
					}, 3000);
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				//if fails     
			}
		});
		e.preventDefault();
	});
</script>