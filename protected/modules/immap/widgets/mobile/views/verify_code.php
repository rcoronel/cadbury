<div class="column main-body verification-body">
		<div class="inner-body">
			<div class="header">
				<div class="logo">
					<?php if ($this->owner->inside_logo):?>
						<?php echo CHtml::image($this->owner->inside_logo, $this->owner->site_title);?>
					<?php endif;?>
				</div>
				<p>A 4-Digit Verification Code has been sent to your mobile number</p>
			</div>
			
			<?php $form=$this->beginWidget('CActiveForm', array('method'=>'post', 'id'=>'verify-form', 'htmlOptions'=>array('class'=>'form-class'))); ?>
                <?php echo $form->textField($mobile, 'msisdn', array('type'=>'number', 'readonly'=>true));?>
				<label>
					<a href="#" id="change_num" >Not you? Change your number</a>
					<button id="resend-btn" class="submit-btn button" type="button">RESEND</button>
				</label>
				<label>
					Enter your code here:
					<?php echo $form->textField($mobile, 'auth_code', array('value'=>'', 'placeholder'=>'Enter your code here', 'maxlength'=>4));?>
					<span class="error-message" style="display:none;">Invalid verification code.</span>
				</label>
                <label for="auth_code">Enter your code here</label>
                
				<?php echo CHtml::hiddenField('availment_id', $this->self->availment_id);?>
				<?php echo CHtml::hiddenField('mobile_number', $mobile->msisdn);?>
                
                <div id="default-to-action-btn-wrapper">
                	<?php echo CHtml::htmlButton('VERIFY', array('type'=>'submit', 'class'=>'verify button'));?>
                </div>
            <?php $this->endWidget();?>
			<?php $form=$this->beginWidget('CActiveForm', array('method'=>'post', 'id'=>'resend-form', 'htmlOptions'=>array('class'=>'form-class'))); ?>
				<?php echo CHtml::hiddenField('resend', 1);?>
            <?php $this->endWidget();?>
			<?php $form=$this->beginWidget('CActiveForm', array('method'=>'post', 'id'=>'reset-form', 'htmlOptions'=>array('class'=>'form-class'))); ?>
				<?php echo CHtml::hiddenField('change_number', 1);?>
            <?php $this->endWidget();?>
			<div class="footer">
				<?php echo CHtml::image(Link::image_url("portal/{$this->owner->portal->portal_id}/powered-by-logo.png"), $this->owner->site_title);?>
			</div>
		</div>
	</div>

<div class="modal modal-wifi in" id="verify-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content globe-modal-content">
			<div class="modal-header">
				<h4 class="modal-title sr-only" id="myModalLabel"></h4>
			</div>
			<div class="modal-body">
				<p></p>
			</div>
			<div class="modal-footer">
				<?php echo CHtml::htmlButton('Close', array('class'=>'btn secondary-btn'));?>
				<div class="modal-btn-logo">
					<?php echo CHtml::image(Link::image_url('powered-by-globe-modal.png'), 'Globe');?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal modal-wifi in" id="loader-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-body small-modal-body-style">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$('button#resend-btn').on('click', function(e){
		$('form#resend-form').submit();
	});
	$('form#resend-form').on('submit', function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		var postData = $(this).serializeArray();
		var formURL = $(this).attr('action');

		$.ajax({
			url : formURL,
			type: "POST",
			data : postData,
			dataType: 'json',
			success:function(data, textStatus, jqXHR)
			{
				if ( ! data.status) {
					setTimeout(function() {
						$('#loader-modal').modal('hide');
						$('span.error-message').html(data.message);
						$('input#AvailMobile_auth_code').addClass('error');
						$('span.error-message').show();
					}, 900);
				}
				else {
					$('div.modal-body p').html(data.message);
					$('#loader-modal').modal({backdrop: 'static', keyboard: false});
					setTimeout(function(){
						window.location.href = data.redirect;
					}, 3000);
				}
			},
			error: function(jqXHR, textStatus, errorThrown){}
		});

		e.preventDefault();
	});
		
	$('#change_num').on('click', function(){
		$('form#reset-form').submit();
	});
	$('form#reset-form').on('submit', function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		var postData = $(this).serializeArray();
		var formURL = $(this).attr('action');

		$.ajax({
			url : formURL,
			type: "POST",
			data : postData,
			dataType: 'json',
			success:function(data, textStatus, jqXHR)
			{
				if ( ! data.status) {
					setTimeout(function() {
						$('#loader-modal').modal('hide');
						$('span.error-message').html(data.message);
						$('input#AvailMobile_auth_code').addClass('error');
						$('span.error-message').show();
					}, 900);
				}
				else {
					$('div.modal-body p').html(data.message);
					$('#loader-modal').modal({backdrop: 'static', keyboard: false});
					setTimeout(function(){
						window.location.href = data.redirect;
					}, 3000);
				}
			},
			error: function(jqXHR, textStatus, errorThrown){}
		});

		e.preventDefault();
	});
	$('form#verify-form').on('submit', function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		$('input#AvailMobile_auth_code').removeClass('error');
		$('span.error-message').hide();
		
		var postData = $(this).serializeArray();
		var formURL = $(this).attr('action');
		
		$.ajax( {
			url : formURL,
			type: "POST",
			data : postData,
			dataType: 'json',
			success:function(data, textStatus, jqXHR)
			{
				if ( ! data.status) {
					setTimeout(function() {
						$('#loader-modal').modal('hide');
						$('span.error-message').html(data.message);
						$('input#AvailMobile_auth_code').addClass('error');
						$('span.error-message').show();
					}, 900);
				}
				else {
					$('#loader-modal').modal({backdrop: 'static', keyboard: false});
					window.location.href = data.redirect;
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				//if fails     
			}
		});
		
		e.preventDefault();
	});
//	$('form#resend-form').on('submit', function(e){
//		
//		// hide errors
//		$('div#form-error').slideUp( 300 ).html();
//
//		var postData = $(this).serializeArray();
//		var formURL = $(this).attr("action");
//
//		$.ajax( {
//			url : formURL,
//			type: "POST",
//			data : postData,
//			dataType: 'json',
//			success:function(data, textStatus, jqXHR)
//			{
//				if ( ! data.status) {
//					setTimeout(function() {
//						$('#loader-modal').modal('hide');
//						$('div#form-error').slideDown(300).delay(800).html(data.message);
//						$('div#form-error').show();
//					}, 900);
//				}
//				else {
//					$('#loader-modal').modal('hide');
//					$('div#verify-modal .modal-body').html(data.message);
//					$('#verify-modal').modal({backdrop: 'static', keyboard: false});
//					$('#verify-modal').on('click', function () {
//						window.location.href = data.redirect;
//					});
//				}
//			},
//			error: function(jqXHR, textStatus, errorThrown)
//			{
//				//if fails     
//			}
//		});
//		e.preventDefault();
//	});
//	
//	$('form#verify-form').on('submit', function(e){
//		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
//		// hide errors
//		$('div#form-error').slideUp( 300 ).html();
//
//		var postData = $(this).serializeArray();
//		var formURL = $(this).attr("action");
//
//		$.ajax( {
//			url : formURL,
//			type: "POST",
//			data : postData,
//			dataType: 'json',
//			success:function(data, textStatus, jqXHR)
//			{
//				if ( ! data.status) {
//					setTimeout(function() {
//						$('#loader-modal').modal('hide');
//						$('div#form-error').slideDown(300).delay(800).html(data.message);
//						$('div#form-error').show();
//					}, 900);
//				}
//				else {
//					window.location.href = data.redirect;
//				}
//			},
//			error: function(jqXHR, textStatus, errorThrown)
//			{
//				setTimeout(function() {
//					$('#loader-modal').modal('hide');
//					$('div#form-error').slideDown(300).delay(800).html(jqXHR.responseText);
//					$('div#form-error').show();
//				}, 900);
//			}
//		});
//		e.preventDefault();
//	});
//
//	$('#change_num').on('click', function(){
//		$('form#changenum-form').submit();
//	});
//
//	$('form#changenum-form').on('submit', function(e){
//		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
//		// hide errors
//		$('div#form-error').slideUp( 300 ).html();
//
//		var postData = $(this).serializeArray();
//		var formURL = $(this).attr("action");
//
//		$.ajax( {
//			url : formURL,
//			type: "POST",
//			data : postData,
//			dataType: 'json',
//			success:function(data, textStatus, jqXHR)
//			{
//				if ( ! data.status) {
//					setTimeout(function() {
//						$('#loader-modal').modal('hide');
//						$('div#form-error').slideDown(300).delay(800).html(data.message);
//						$('div#form-error').show();
//					}, 900);
//				}
//				else {
//					// $('div#verify-modal .modal-body').html(data.message);
//					// $('#verify-modal').modal({backdrop: 'static', keyboard: false});
//					setTimeout(function(){
//						window.location.href = data.redirect;
//					}, 3000);
//				}
//			},
//			error: function(jqXHR, textStatus, errorThrown)
//			{
//				//if fails     
//			}
//		});
//		e.preventDefault();
//	});
</script>