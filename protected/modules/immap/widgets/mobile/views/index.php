<div class="column main-body">
	<div class="large-6 columns">
		<div class="hide-for-small-only">
			<div class="logo">
				<?php if ($this->owner->logo):?>
					<?php echo CHtml::image($this->owner->logo, $this->owner->site_title);?>
				<?php endif;?>
			</div>
		</div>
	</div>
	<div class="large-6 columns">
		<div class="header">
			<div class="show-for-small-only">
				<?php if ($this->owner->logo):?>
					<?php echo CHtml::image($this->owner->logo, $this->owner->site_title);?>
				<?php endif;?>
			</div>
			<h1>WELCOME!</h1>
			<p>To enjoy your free WIFI please fill up the following </p>
		</div>
		<?php $form=$this->beginWidget('CActiveForm', array('method'=>'post', 'action'=>'', 'htmlOptions'=>array('class'=>'form-class'))); ?>
			<?php echo $form->textField($event, 'fullname', array('placeholder'=>'Full name'));?>
			<?php echo $form->textField($event, 'email', array('placeholder'=>'E-mail'));?>
			<?php echo $form->textField($mobile, 'msisdn', array('placeholder'=>'Mobile Number'));?>
			<?php echo CHtml::htmlButton('SUBMIT', array('type'=>'submit', 'class'=>'submit-btn button'));?>
		<?php $this->endWidget();?>

		<div class="footer">
			<?php echo CHtml::image(Link::image_url("portal/{$this->owner->portal->portal_id}/powered-by-logo.png"), $this->owner->site_title);?>
		</div>
	</div>
</div>

<div class="modal modal-wifi in" id="loader-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-body small-modal-body-style">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal modal-wifi in" id="verify-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content globe-modal-content">
			<div class="modal-header">
				<h1>Error encountered</h1>
			</div>
			<div class="modal-body">
				<p></p>
			</div>
			<div class="modal-footer">
				<?php echo CHtml::htmlButton('Close', array('class'=>'submit-btn button'));?>
			</div>
		</div>
	</div>
</div>
<div class="error-body" id="error-body" style="display: none;">
	<h1><i class="glyphicon-alert"></i>Error encountered</h1>
	<ul>
		<li>Email is not a valid email address.</li>
	</ul>
	<button id="close-btn" type="button" class="btn-close" onclick=""> Close</button>
</div>
<script>
	$('form').on('submit', function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		// hide errors
		$('div.form-error').slideUp( 300 ).html();

		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");

		$.ajax( {
			url : formURL,
			type: "POST",
			data : postData,
			dataType: 'json',
			success:function(data, textStatus, jqXHR)
			{
				if ( ! data.status) {
					setTimeout(function() {
						$('#loader-modal').modal('hide');
//						$('#loader-modal').modal('show');
//						$('div#error-body').modal('show');
//						$('div.form-error span').slideDown(300).delay(800).html(data.message);
//						$('div.form-error').show();
						$('div#verify-modal .modal-body').html(data.message);
						$('#verify-modal').modal({backdrop: 'static', keyboard: false});
						$('#verify-modal').on('click', function () {
							$('#verify-modal').modal('hide');
						});
					}, 900);
				}
				else {
					$('div.modal-body p').html(data.message);
					$('#loader-modal').modal({backdrop: 'static', keyboard: false});
					setTimeout(function(){
						window.location.href = data.redirect;
					}, 3000);
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				//if fails     
			}
		});
		e.preventDefault();
	});
</script>