<div class="column main-body">
	<div class="inner-body">

			<div class="header">
				<?php if ($this->owner->logo):?>
					<?php echo CHtml::image($this->owner->logo, $this->owner->site_title);?>
				<?php endif;?>
			</div>

			<div class="body-content">
			   <div class="inner-body">
				<p>Thank you, you may now start using our Free WIFI service. </p>
				
				<?php $form=$this->beginWidget('CActiveForm', array('method'=>'post', 'action'=>'', 'htmlOptions'=>array('class'=>'form-class'))); ?>
					<div class="form-error" role="alert" >
						<span class="error-message"></span>
					</div>
					<?php echo $form->hiddenField($mobile, 'device_availment_id');?>
					<?php echo CHtml::htmlButton('GET CONNECTED', array('type'=>'submit', 'class'=>'get-connect button'));?>
				<?php $this->endWidget();?>

			</div>
		</div>

		<div class="footer">
			<?php echo CHtml::image(Link::image_url("portal/{$this->owner->portal->portal_id}/powered-by-logo.png"), $this->owner->site_title);?>
		</div>
	</div>
</div>

<div class="modal modal-wifi in" id="loader-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-body small-modal-body-style">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$('form').on('submit', function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		// hide errors
		$('div.form-error').slideUp( 300 ).html();

		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");

		$.ajax( {
			url : formURL,
			type: "POST",
			data : postData,
			dataType: 'json',
			success:function(data, textStatus, jqXHR)
			{
				if ( ! data.status) {
					setTimeout(function() {
						$('#loader-modal').modal('hide');
						$('div.form-error span').slideDown(300).delay(800).html(data.message);
						$('div.form-error').show();
					}, 900);
				}
				else {
					$('div.modal-body p').html(data.message);
					$('#loader-modal').modal({backdrop: 'static', keyboard: false});
					setTimeout(function(){
						window.location.href = data.redirect;
					}, 3000);
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				setTimeout(function() {
					$('#loader-modal').modal('hide');
					$('div.form-error span').slideDown(300).delay(800).html(jqXHR.responseText);
					$('div.form-error').show();
				}, 900);
			}
		});
		e.preventDefault();
	});
</script>