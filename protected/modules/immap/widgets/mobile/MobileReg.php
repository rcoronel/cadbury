<?php

require __DIR__.'/api/mobile_api.php';
class MobileReg extends CWidget
{
    public $self;
	public $self_portal;
	public $action;
	public $reference;
	public $branding;
	
	public function init()
	{
		// Set PortalAvailment instance
		$this->self_portal = PortalAvailment::model()->findByAttributes(array('portal_id'=>$this->owner->portal->portal_id, 'availment_id'=>$this->self->availment_id));
	}
        
	public function run()
	{
		switch ($this->action)
		{
			// Login or Register function
			case 'login':
				self::_login();
			break;
		
			// verify code
			case 'verify':
				self::_verify();
			break;
		
			case 'register':
				self::_register();
			break;
		
			// Display the buttons
			default:
				self::_display();
			break;
		}
	}
	
	/**
	 * Display the widget
	 * 
	 * @access private
	 * @return void
	 */
	private function _display()
	{
		$this->render('login', array('availment_id'=>$this->self->availment_id));
	}
	
	/**
	 * Login or Register
	 * 
	 * @access private
	 * @return void
	 */
	private function _login()
	{
		// Check if already availed for today
		$is_availed = AvailMobileReg::model()->with('dev_avail')->exists("device_id = {$this->owner->device->device_id} AND portal_id = {$this->owner->portal->portal_id} AND availment_id = {$this->self->availment_id} AND DATE(dev_avail.created_at) = DATE(NOW()) AND is_validated = 1");
		if ($is_availed) {
			$this->owner->redirect(array('login/', 'connection_token'=>$this->owner->connection->connection_token));
		}
		
		$availed = AvailMobileReg::model()->with('dev_avail')->find("device_id = {$this->owner->device->device_id} AND portal_id = {$this->owner->portal->portal_id} AND availment_id = {$this->self->availment_id} AND DATE(dev_avail.created_at) = DATE(NOW()) AND is_validated = 0");
		if($availed && $availed->auth_limit != 0){
			$this->owner->redirect(array('login/',	'connection_token'=>$this->owner->connection->connection_token,
							'availment_id'=>$this->self->availment_id,
							'mobile'=>$availed->msisdn,
							'action'=>'verify'));
		}

		$mobile = new AvailMobileReg;
		if (Yii::app()->request->isPostRequest) {
			self::_validatePost($mobile);
		}
		
		$this->render('index', array('availment_id'=>$this->self->availment_id, 'object'=>$mobile));
	}
	
	/**
	 * Validate mobile no.
	 * 
	 * @access private
	 * @param object $object AvailMobileReg
	 * @return JSON
	 */
	private function _validatePost(AvailMobileReg $object)
	{
		$post = Yii::app()->request->getPost('AvailMobileReg');
		$object->attributes = $post;
		
		if ($object->validate()){
			// Check if record already existed
			$mobile = AvailMobileReg::model()->with('dev_avail')->find("msisdn = {$object->msisdn} AND portal_id = {$this->owner->portal->portal_id} AND device_id = {$this->owner->device->device_id} AND DATE(dev_avail.created_at) = DATE(NOW())");
			if ( ! empty($mobile)) {
				$mobile->msisdn = $object->msisdn;
				$mobile->auth_limit++;
				$object = $mobile;
			}
			else {
				$object->auth_limit = 1;
			}
			
			// Check for limit
			if ($object->auth_limit > 3) {
				$json_array = array('status'=>0, 'message'=>'Resending of code limit already reached');
				die(CJSON::encode($json_array));
			}
			
			// new authentication code
			$object->auth_code = mt_rand(10000, 99999);
			if ( ! Sms::send($object->msisdn, "{$object->auth_code} - This is your Globe GoWifi verification code.")) {
				$json_array = array('status'=>0, 'message'=>'Error encountered. Try again later. (SITE-008)');
				die(CJSON::encode($json_array));
			}
			
			// Save object
			if ($object->save()) {
				// save DeviceAvailment info
				if (isset($object->dev_avail) && ! empty($object->dev_avail)) {
					$dev_avail = $object->dev_avail;
				}
				else {
					$dev_avail = $this->owner->saveDeviceAvailment($this->self->availment_id, TRUE);
					$object->device_availment_id = $dev_avail->device_availment_id;
				}
				
				$object->validate() && $object->save();
				
				$url = $this->owner->createAbsoluteUrl('login/',
					array(	'connection_token'=>$this->owner->connection->connection_token,
							'availment_id'=>$this->self->availment_id,
							'mobile'=>$object->msisdn,
							'action'=>'verify'));
				$json_array = array('status'=>1, 'message'=>'Kindly verify your mobile number by using the verification code sent to you', 'redirect'=>$url);
				die(CJSON::encode($json_array));
			}
		}
		else {
			$json_array = array('status'=>0, 'message'=>'Invalid mobile no.');
			die(CJSON::encode($json_array));
		}
	}
	
	/**
	 * Verify the 5 digit code
	 * 
	 * @access private
	 * @return mixed
	 */
	private function _verify()
	{
		$mobile_no = Yii::app()->request->getParam('mobile');
		$mobile = AvailMobileReg::model()->with('dev_avail')->find("msisdn = {$mobile_no} AND portal_id = {$this->owner->portal->portal_id} AND device_id = {$this->owner->device->device_id} AND DATE(dev_avail.created_at) = DATE(NOW())");
		if ($mobile) {
			if ($mobile->is_validated) {
				// already validated, redirect to login
				$this->owner->redirect(array('login/', 'connection_token'=>$this->owner->connection->connection_token));
			}
			
			// Check if submitted via POST
			if (Yii::app()->request->isPostRequest) {
				// Check if the POST is for code resending
				if (Yii::app()->request->getPost('resend')) {
					self::_resendCode($mobile);
				}
				
				if (Yii::app()->request->getPost('change_number')) {
					self::_resetLimit($mobile);
				}

				$post = Yii::app()->request->getPost('AvailMobileReg');
				
				// Check if the codes match
				if ($mobile->auth_code != $post['auth_code']) {
					$json_array = array('status'=>0, 'message'=>'Incorrect 5-digit code');
					die(CJSON::encode($json_array));
				}
				
				// Check for limit
				if ($mobile->auth_limit > 3) {
					$json_array = array('status'=>0, 'message'=>'Resending of code limit already reached');
					die(CJSON::encode($json_array));
				}
				
				$mobile->is_validated = 1;
				$mobile->auth_limit++;
				
				if ($mobile->validate()) {
					// Alter duration
					$globe_duration = AvailmentSetting::getValue($this->self->availment_id, 'GLOBE_DURATION');
					$xglobe_duration = AvailmentSetting::getValue($this->self->availment_id, 'XGLOBE_DURATION');
					if ($globe_duration || $xglobe_duration) {
						self::_setBrand($mobile->msisdn);
						if ($this->branding) {
							$url = $this->owner->createAbsoluteUrl('login/',
								array(	'connection_token'=>$this->owner->connection->connection_token,
										'availment_id'=>$this->self->availment_id,
										'mobile'=>$mobile->msisdn,
										'action'=>'register'));
							$json_array = array('status'=>1, 'message'=>'Please wait', 'redirect'=>$url);
							die(CJSON::encode($json_array));
						}
						else {
							if ($xglobe_duration) {
								$this->self_portal->duration = $xglobe_duration * 60;
							}
						}
					}
					
					if (self::_updateConnection($mobile, $this->self_portal)) {
						$mobile->save();
						
						$minutes = $this->self_portal->duration/60;
						$url = $this->owner->createAbsoluteUrl('access/', array('connection_token'=>$this->owner->connection->connection_token,
							'availment_id'=>$this->self->availment_id));
						$json_array = array('status'=>1, 'message'=>"You can use Globe GoWifi, FREE trial for {$minutes} minutes today! Happy Surfing.", 'redirect'=>$url);
						die(CJSON::encode($json_array));
					}
					else {
						$json_array = array('status'=>0, 'message'=>'Something went wrong');
						die(CJSON::encode($json_array));
					}
				}
				
				$json_array = array('status'=>0, 'message'=>'Something went wrong');
				die(CJSON::encode($json_array));
				
			}
			$this->render('verify_code', array('mobile'=>$mobile));
			
		}
		else {
			// no record, redirect to login page
			$this->owner->redirect(array('login/', 'connection_token'=>$this->owner->connection->connection_token));
		}
	}
	
	private function _register()
	{
		$mobile_no = Yii::app()->request->getParam('mobile');
		$mobile = AvailMobileReg::model()->with('dev_avail')->find("msisdn = {$mobile_no} AND portal_id = {$this->owner->portal->portal_id} AND device_id = {$this->owner->device->device_id} AND DATE(dev_avail.created_at) = DATE(NOW())");
		// Check branding
		self::_setBrand($mobile->msisdn);
		if ( ! $this->branding) {
			$this->owner->redirect(array('login/', 'connection_token'=>$this->owner->connection->connection_token));
		}
		
		$cities = City::model()->with('state', 'country')->findAll(array('condition'=>"country.iso_code = 'PH'", 'order'=>'city ASC'));
		
		// Check if submitted via POST
		if (Yii::app()->request->isPostRequest) {
			$mobile->setScenario('register');
			$post = Yii::app()->request->getPost('AvailMobileReg');
			$mobile->attributes = $post;
			$mobile->birthday = date('Y-m-d', strtotime($mobile->birthday));
			
			if ($mobile->validate()) {
				echo "<Pre>";
				print_r($post);
				print_r($mobile);
				die();
			}
			else {
				$json_array = array('status'=>0, 'message'=>'All fields are required / Check for correct format');
				die(CJSON::encode($json_array));
			}
			
		}
		$this->render('register', array('object'=>$mobile, 'cities'=>$cities));
	}
	
	/**
	 * Resend code for mobile availment
	 * 
	 * @access private
	 * @param object $object AvailMobileReg instance
	 * @return JSON
	 */
	private function _resendCode(AvailMobileReg $object)
	{
		if ($object->auth_limit >= 3) {
			$json_array = array('status'=>0, 'message'=>'Maximum limit (3) of sending code already reached');
			die(CJSON::encode($json_array));
		}
		else {
			// new authentication code
			$object->auth_code = mt_rand(10000, 99999);
			$object->auth_limit++;
			Sms::send($object->msisdn, "{$object->auth_code} - This is your Globe GoWifi verification code.");
			
			if ($object->validate() && $object->save()) {
				$url = $this->owner->createAbsoluteUrl('login/',
					array(	'connection_token'=>$this->owner->connection->connection_token,
							'availment_id'=>$this->self->availment_id,
							'mobile'=>$object->msisdn,
							'action'=>'verify'));
				$json_array = array('status'=>1, 'message'=>'Kindly verify your mobile number by using the verification code sent to you', 'redirect'=>$url);
				die(CJSON::encode($json_array));
			}
		}
	}
	
	/**
	 * Update connection details
	 * 
	 * @access private
	 * @param object $mobile AvailMobileReg instance
	 * @param object $availment PortalAvailment instance
	 * @return boolean
	 */
	private function _updateConnection(AvailMobileReg $mobile, PortalAvailment $availment)
	{
		$connection = $this->owner->connection;
		$connection->username = $this->owner->device->mac_address.'@'.$this->owner->portal->code;
		$connection->password = $mobile->msisdn;
		
		if ($connection->validate() && $connection->save()) {
			// Verify connection
			if ($this->owner->connect($availment, TRUE)) {
				return TRUE;
			}
			return FALSE;
		}
		throw new CHttpException(500, Yii::t('yii','Cannot update connection. Please try again.'));
	}

	public function _resetLimit($mobile)
	{
		$mobile->auth_limit = 0;

		if ($mobile->validate() && $mobile->save()) {
			$url = $this->owner->createAbsoluteUrl('login/',
				array(	'connection_token'=>$this->owner->connection->connection_token,
						'availment_id'=>$this->self->availment_id,
						'mobile'=>$mobile->msisdn,
						'action'=>'login'));
			$json_array = array('status'=>1, 'message'=>'Redirecting to mobile input page', 'redirect'=>$url);
			die(CJSON::encode($json_array));
		}
	}
	
	/**
	 * Check for brand based on MSISDN
	 * 
	 * @access private
	 * @param string $msisdn
	 * return void
	 */
	private function _setBrand($msisdn)
	{
		$msisdn = substr($msisdn, 1);
		$prefix = Yii::app()->db->createCommand()->select('*')
			->from('Unifi_Captive.plan_prefix')->where("LEFT(prefix, 3) = LEFT({$msisdn}, 3)")
			->queryRow();
		if ($prefix) {
			$globe_plans = array(1, 2, 3, 9, 10);
			if (in_array($prefix['plan_id'], $globe_plans)) {
				$this->branding = 1;
			}
			else {
				$this->branding = 0;
			}
		}
	}
}