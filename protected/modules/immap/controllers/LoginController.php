<?php

class LoginController extends CaptivePortal
{
	/**
	 * Declare variables
	 * 
	 * @access protected
	 * @param object $action CAction instance
	 * @return bool
	 */
	protected function beforeAction($action)
	{
		parent::beforeAction($action);
		
		return TRUE;
	}
	
	/**
	 * Default action
	 * 
	 * @access public
	 * @return void
	 */
	public function actionIndex()
	{
		// Initialize the login
		self::initialize();
		
		// Check if page is submitted
		if (Yii::app()->request->isPostRequest OR Yii::app()->request->getParam('availment_id')) {
			self::signIn(Yii::app()->request->getParam('availment_id'), Yii::app()->request->getParam('action'));
		}
		else {
			$session_array = self::remainingSession();
			if (isset($session_array['duration']) AND $session_array['duration'] < $session_array['max']) {
				self::showRemaining($session_array['duration'], $session_array['max']);
			}
			else {
				self::showAvailments();
			}
		}
	}
}