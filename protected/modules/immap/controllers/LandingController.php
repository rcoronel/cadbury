<?php

class LandingController extends CaptivePortal
{
	/**
	 * Declare variables
	 * 
	 * @access protected
	 * @param object $action CAction instance
	 * @return bool
	 */
	protected function beforeAction($action)
	{
		parent::beforeAction($action);
		
		return TRUE;
	}
	
	/**
	 * Show landing page
	 * 
	 * @access public
	 * @return void
	 */
	public function actionIndex()
	{
		if(date('H') >= 7 AND date('H') < 22) {
			$this->redirect(array('login/', 'connection_token'=>$this->connection->connection_token));
		}
		$this->render('index');
	}
}