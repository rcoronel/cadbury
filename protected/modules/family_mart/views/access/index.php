<section class="container">
	<div class="row">
		<div class="col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
			<div class="default-wrapper">
				<div class="default-content">
				   <h5 id="app-heading">You can now use Globe GoWiFi. Free trial for <?php echo $minutes;?> minutes today. Happy Surfing!</h5>
				</div>

				<div class="col-md-10 col-md-offset-1">
					<button class="btn secondary-btn fm-app-btn" onclick="location.href='<?php echo $url;?>'">Start Browsing</button>

					<p>FamilyMart elevates your convenience store shopping experience through its mobile-based rewards program. Don't be the last to snap - download the SNAP APP today!</p>
					
					<a href="http://www.familymart.com.ph/snap/">
						<?php echo CHtml::image(Link::image_url("portal/{$this->portal->portal_id}/app-logo.jpg"), 'Family Mart SNAP', array('class'=>'app-logo'));?>
					</a>

					<div id="app-btns">
						<a href="https://play.google.com/store/apps/details?id=com.primesoft.ayalamallsportal">
							<?php echo CHtml::image(Link::image_url('portal/google-badge-btn.png'), 'Ayala APP', array('class'=>'app-badge'));?>
						</a>
					</div>
				</div>
				<footer>
					<?php echo CHtml::image(Link::image_url('portal/powered-by-globe-modal.png'), 'Globe');?>
				</footer>
			</div>
		</div>
	</div>
</section>