<section class="container">
	<div class="row">
		<div class="col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
			<div class="default-wrapper">
				<?php echo str_replace('::user::', $this->user, PortalConfiguration::getValue('WELCOME_BACK'));?>
				<br>
				<h3>
					You still have at least <?php echo (int)$minutes;?> 
					<?php echo ((int)$minutes > 1) ? 'minutes' : 'minute';?> of internet connection
				</h3>
				<div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
					<div id="call-to-action-btn-wrapper" >
						<?php echo CHtml::beginForm();?>
							<?php echo CHtml::hiddenField('availment_id', $availment->availment_id);?>
							<?php echo CHtml::htmlButton('CONTINUE', array('type'=>'submit', 'class'=>'btn primary-btn'));?>
						<?php echo CHtml::endForm();?>
					</div>
				</div>
				
				<footer class="row powered-container">
					<?php if (PortalConfiguration::getvalue('POWERED_INSIDE')):?>
						<?php echo CHtml::image(Link::image_url('powered-by-globe-modal.png'), 'Globe');?>
					<?php endif;?>
				</footer>
			</div> <!-- end of default-wrapper -->
		</div>
	</div>
</section>

<div class="modal modal-wifi in" id="loader-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-body small-modal-body-style">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	
	$('form').on('submit',function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		//e.preventDefault();
	});
});
</script>