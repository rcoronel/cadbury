<div class="row">
    <div class="col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
        <div class="default-wrapper">
            <h1 class="captive-portal-heading">Code Verification</h1>

            <div class="captive-portal-sub-heading">
                <p>A 5-digit verification code has been sent to your mobile number.</p>
            </div>
            <?php $form=$this->beginWidget('CActiveForm', array('method'=>'post', 'id'=>'changenum-form')); ?>
	            <div id="verification-phone-number-wrapper">
	                <h1><?php echo $mobile->msisdn; ?></h1>
					<?php echo CHtml::hiddenField('availment_id', $this->self->availment_id);?>
					<?php echo CHtml::hiddenField('mobile_number', $mobile->msisdn);?>
					<?php echo CHtml::hiddenField('change_number', 1);?>
					<a href="#" id="change_num" class="link-text-colored">Not you? Change phone number.</a>
	            </div>
            <?php $this->endWidget();?>

            <?php $form=$this->beginWidget('CActiveForm', array('method'=>'post', 'id'=>'resend-form')); ?>
				<?php echo CHtml::hiddenField('availment_id', $this->self->availment_id);?>
				<?php echo CHtml::hiddenField('mobile_number', $mobile->msisdn);?>
				<?php echo CHtml::hiddenField('resend', 1);?>
				<?php echo CHtml::htmlButton('RESEND CODE', array('type'=>'submit', 'class'=>'btn'));?>
			<?php $this->endWidget();?>

            <?php $form=$this->beginWidget('CActiveForm', array('method'=>'post', 'id'=>'verify-form', 'htmlOptions'=>array('class'=>'col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2',),)); ?>
                <div id="form-error" class="error-notification animated bounceIn" role="alert" style="display:none;"></div>
                
                <label for="auth_code">Enter your code here</label>
                <?php echo $form->textField($mobile, 'auth_code', array('class'=>'form-control number-only', 'value'=>'', 'placeholder'=>'12345'));?>
				<?php echo CHtml::hiddenField('availment_id', $this->self->availment_id);?>
				<?php echo CHtml::hiddenField('mobile_number', $mobile->msisdn);?>
                
                <div id="default-to-action-btn-wrapper">
                	<span><?php echo CHtml::htmlButton('verify', array('type'=>'submit', 'class'=>'btn primary-btn'));?></span>
					<span><?php echo CHtml::htmlButton('back', array('type'=>'button', 'class'=>'btn secondary-btn group-btn', 'onclick'=>"window.location='{$this->owner->createAbsoluteUrl('login/', array('connection_token'=>$this->owner->connection->connection_token))}'"));?></span>
                </div>
            <?php $this->endWidget();?>

            <footer>
            	<?php echo CHtml::image(Link::image_url('powered-by-globe-modal.png'), 'Globe');?>
            </footer>
        </div>
    </div>
</div>

<div class="modal modal-wifi in" id="verify-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content globe-modal-content">
			<div class="modal-header">
				<h4 class="modal-title sr-only" id="myModalLabel"></h4>
			</div>
			<div class="modal-body">
				<p></p>
			</div>
			<div class="modal-footer">
				<?php echo CHtml::htmlButton('Close', array('class'=>'btn secondary-btn'));?>
				<div class="modal-btn-logo">
					<?php echo CHtml::image(Link::image_url('powered-by-globe-modal.png'), 'Globe');?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal modal-wifi in" id="loader-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-body small-modal-body-style">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$('form#resend-form').on('submit', function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		// hide errors
		$('div#form-error').slideUp( 300 ).html();

		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");

		$.ajax( {
			url : formURL,
			type: "POST",
			data : postData,
			dataType: 'json',
			success:function(data, textStatus, jqXHR)
			{
				if ( ! data.status) {
					setTimeout(function() {
						$('#loader-modal').modal('hide');
						$('div#form-error').slideDown(300).delay(800).html(data.message);
						$('div#form-error').show();
					}, 900);
				}
				else {
					$('#loader-modal').modal('hide');
					$('div#verify-modal .modal-body').html(data.message);
					$('#verify-modal').modal({backdrop: 'static', keyboard: false});
					$('#verify-modal').on('click', function () {
						window.location.href = data.redirect;
					});
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				//if fails     
			}
		});
		e.preventDefault();
	});
	
	$('form#verify-form').on('submit', function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		// hide errors
		$('div#form-error').slideUp( 300 ).html();

		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");

		$.ajax( {
			url : formURL,
			type: "POST",
			data : postData,
			dataType: 'json',
			success:function(data, textStatus, jqXHR)
			{
				if ( ! data.status) {
					setTimeout(function() {
						$('#loader-modal').modal('hide');
						$('div#form-error').slideDown(300).delay(800).html(data.message);
						$('div#form-error').show();
					}, 900);
				}
				else {
					window.location.href = data.redirect;
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				setTimeout(function() {
					$('#loader-modal').modal('hide');
					$('div#form-error').slideDown(300).delay(800).html(jqXHR.responseText);
					$('div#form-error').show();
				}, 900);
			}
		});
		e.preventDefault();
	});

	$('#change_num').on('click', function(){
		$('form#changenum-form').submit();
	});

	$('form#changenum-form').on('submit', function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		// hide errors
		$('div#form-error').slideUp( 300 ).html();

		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");

		$.ajax( {
			url : formURL,
			type: "POST",
			data : postData,
			dataType: 'json',
			success:function(data, textStatus, jqXHR)
			{
				if ( ! data.status) {
					setTimeout(function() {
						$('#loader-modal').modal('hide');
						$('div#form-error').slideDown(300).delay(800).html(data.message);
						$('div#form-error').show();
					}, 900);
				}
				else {
					// $('div#verify-modal .modal-body').html(data.message);
					// $('#verify-modal').modal({backdrop: 'static', keyboard: false});
					setTimeout(function(){
						window.location.href = data.redirect;
					}, 3000);
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				//if fails     
			}
		});
		e.preventDefault();
	});
</script>