<section class="container">
	<div class="row">
		<div class="col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
			<div id="ayala-app-wrapper" class="default-wrapper clearfix">
				<div class="default-content">
				   <h5 id="ayala-app-heading">You can now use the WiFi Free trial for today. Happy Surfing!</h5>
				</div>

				<div class="col-md-10 col-md-offset-1">
					<button class="btn secondary-btn" onclick="location.href='<?php echo $url;?>'">Start Browsing</button>

					<p>Your favorite Ayala Malls now on your mobile.</p>
					<?php echo CHtml::image(Link::image_url('portal/ayala-app-logo.jpg'), 'Ayala APP', array('class'=>'ayala-app-logo'));?>

					<div id="ayala-app-btns">
						<a href="https://play.google.com/store/apps/details?id=com.primesoft.ayalamallsportal">
							<?php echo CHtml::image(Link::image_url('portal/google-badge-btn.png'), 'Ayala APP', array('class'=>'ayala-app-badge'));?>
						</a>
						<a href="https://itunes.apple.com/ph/app/a-portal/id1094134122?mt=8">
							<?php echo CHtml::image(Link::image_url('portal/apple-badge-btn.png'), 'Ayala APP', array('class'=>'ayala-app-badge'));?>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>