<section class="container">
	<div class="row">
		<div class="col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
			<div class="default-wrapper clearfix">
				<?php if ( ! empty($availments)):?>
					<?php if ($this->user):?>
						<?php echo str_replace('::user::', $this->user, PortalConfiguration::getValue('WELCOME_BACK'));?>
					<?php else:?>
						<h3>Welcome</h3>
					<?php endif; ?>
					<?php if ( ! empty($availment)):?>
						<h3>Your FREE <?php echo $availment->duration / 60;?> minutes has expired</h3>
					<?php endif;?>

					<div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
						<div id="call-to-action-btn-wrapper" >
							<?php foreach ($availments as $a):?>
								<?php echo $a->availment->description;?>
								<?php $this->widget("{$this->portal->code}.widgets.{$a->availment->code}", array('self' => $a));?>
							<?php endforeach;?>
						</div>
					</div>
				<?php else:?>
					<h2>Your FREE <?php echo $availment->duration / 60;?> minutes has expired for today</h2>
					<br>
					<h1>Thank you for using Globe GoWiFi! We hope your free trial experience has been wonderful.</h1>
					<br>
				<?php endif;?>
			</div>
		</div>
	</div>
</section>

<div class="modal modal-wifi in" id="loader-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-body small-modal-body-style">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	
	$('form').on('submit',function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		//e.preventDefault();
	});
});
</script>