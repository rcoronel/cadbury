
<section class="container">
	<div class="row">
		<div id="bhs-cp">
			<div class="bhs-body-wrapper">
				<?php if ($this->logo):?>
					<?php echo CHtml::image($this->logo, $this->site_title, array('class'=>'bhs-logo'));?>
				<?php endif;?>
				<?php echo PortalConfiguration::getValue('LANDING_MESSAGE');?>
				<div id="bhs-footer-content">
					<?php echo CHtml::htmlButton('Get Connected', array('class'=>'btn yellow-btn', 'onclick'=>"window.location='{$url}'", 'id'=>'get-connected'));?>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal modal-wifi in" id="loader-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-body small-modal-body-style">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('button#get-connected').on('click',function(e){	
			$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		});
	});
</script>