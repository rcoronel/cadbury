<?php

class AccessController extends CaptivePortal
{
	public function actionIndex()
	{
		$availment_id = Yii::app()->request->getQuery('availment_id');
		
		// Check if there is a connection made
		$connect = ConnectionAvail::model()->findByAttributes(array('device_connection_id'=>$this->connection->device_connection_id, 'availment_id'=>$availment_id));
		if (empty($connect)) {
			throw new CHttpException(400,Yii::t('yii','No connection made. Please try again.'));
		}
		
		$url = PortalConfiguration::getValue('REDIRECT_URL');
		$session_duration = Device::getSessionTime($this->connection);
		if ($session_duration['current_session_time']) {
			if ($url) {
			   $this->redirect($url);
			}
		}
		
		Yii::app()->clientScript->registerCssFile(Link::css_url('portal/ayala-app-style.css'));

		$this->render('index', array('url'=>$url));
	}
}