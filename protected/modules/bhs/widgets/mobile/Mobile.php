<?php

class Mobile extends CWidget
{
    public $self;
	public $self_portal;
	public $action;
	public $reference;
	
	public function init()
	{
		// Set PortalAvailment instance
		$this->self_portal = PortalAvailment::model()->findByAttributes(array('portal_id'=>$this->owner->portal->portal_id, 'availment_id'=>$this->self->availment_id));
	}
        
	public function run()
	{
		switch ($this->action)
		{
			// Login or Register function
			case 'login':
				self::_login();
			break;
		
			// verify code
			case 'verify':
				self::_verify();
			break;
		
			// Display the buttons
			default:
				self::_display();
			break;
		}
	}
	
	/**
	 * Display the widget
	 * 
	 * @access private
	 * @return void
	 */
	private function _display()
	{
		$this->render('login', array('availment_id'=>$this->self->availment_id));
	}
	
	/**
	 * Login or Register
	 * 
	 * @access private
	 * @return void
	 */
	private function _login()
	{
		// Check if already availed for today
		$object = AvailMobile::model()->with('dev_avail')->find("device_id = {$this->owner->device->device_id} AND portal_id = {$this->owner->portal->portal_id} AND availment_id = {$this->self->availment_id} AND DATE(dev_avail.created_at) = DATE(NOW())");
		if ($object) {
			self::_redirect($object);
		}

		$mobile = new AvailMobile;
		if (Yii::app()->request->isPostRequest) {
			self::_validatePost($mobile);
		}
		
		$this->render('index', array('availment_id'=>$this->self->availment_id, 'object'=>$mobile));
	}
	
	/**
	 * Validate mobile no.
	 * 
	 * @access private
	 * @param object $object AvailMobile
	 * @return JSON
	 */
	private function _validatePost(AvailMobile $object)
	{
		$post = Yii::app()->request->getPost('AvailMobile');
		$object->attributes = $post;
		
		if ($object->validate()){
			// Check if record already existed
			$mobile = AvailMobile::model()->with('dev_avail')->find("msisdn = {$object->msisdn} AND portal_id = {$this->owner->portal->portal_id} AND device_id = {$this->owner->device->device_id} AND DATE(dev_avail.created_at) = DATE(NOW())");
			if ( ! empty($mobile)) {
				$mobile->msisdn = $object->msisdn;
				$mobile->auth_limit++;
				$object = $mobile;
			}
			else {
				$object->auth_limit = 1;
			}
			
			// Check for limit
			if ($object->auth_limit > 3) {
				$json_array = array('status'=>0, 'message'=>'Resending of code limit already reached');
				die(CJSON::encode($json_array));
			}
			
			// new authentication code
			$object->auth_code = mt_rand(10000, 99999);
			$message = "{$object->auth_code} - This is your WiFi verification code.";
			if ( ! Mobile360::send($object->msisdn, $message)) {
				$json_array = array('status'=>0, 'message'=>'Error encountered. Try again later. (SITE-008)');
				die(CJSON::encode($json_array));
			}
			
			// If no device availment, create new record
			if ( ! isset($object->dev_avail) OR empty($object->dev_avail)) {
				$dev_avail = $this->owner->saveDeviceAvailment($this->self->availment_id, TRUE);
				$object->device_availment_id = $dev_avail->device_availment_id;
			}
			
			// Save AvailMobile instance
			$object->validate() && $object->save();
			
			$url = $this->owner->createAbsoluteUrl('login/',
				array(	'connection_token'=>$this->owner->connection->connection_token,
						'availment_id'=>$this->self->availment_id,
						'reference'=>$object->device_availment_id,
						'action'=>'verify'));
			$json_array = array('status'=>1, 'message'=>'Kindly verify your mobile number by using the verification code sent to you', 'redirect'=>$url);
			die(CJSON::encode($json_array));
		}
		else {
			$json_array = array('status'=>0, 'message'=>'Invalid mobile no.');
			die(CJSON::encode($json_array));
		}
	}
	
	/**
	 * Verify the 5 digit code
	 * 
	 * @access private
	 * @return mixed
	 */
	private function _verify()
	{
		$device_availment_id = Yii::app()->request->getParam('reference');
		$availment_id = Yii::app()->request->getParam('availment_id');
		$mobile = AvailMobile::model()->with('dev_avail')->find("dev_avail.device_availment_id = {$device_availment_id} AND availment_id = {$availment_id} AND portal_id = {$this->owner->portal->portal_id} AND device_id = {$this->owner->device->device_id} AND DATE(dev_avail.created_at) = DATE(NOW())");
		
		if (empty($mobile)) {
			// no record, redirect to login page
			$this->owner->redirect(array('login/', 'connection_token'=>$this->owner->connection->connection_token));
		}
		
		if ($mobile && $mobile->is_validated != 0) {
			self::_redirect($mobile);
		}
		
		// Check if submitted via POST
		if (Yii::app()->request->isPostRequest) {
			// Check if the POST is for code resending
			if (Yii::app()->request->getPost('resend')) {
				self::_resendCode($mobile);
			}

			if (Yii::app()->request->getPost('change_number')) {
				self::_resetLimit($mobile);
			}

			$post = Yii::app()->request->getPost('AvailMobile');

			// Check if the codes match
			if ($mobile->auth_code != $post['auth_code']) {
				$json_array = array('status'=>0, 'message'=>'Incorrect 5-digit code');
				die(CJSON::encode($json_array));
			}

			$mobile->is_validated = 1;
			$mobile->auth_limit++;

			if ($mobile->validate() && self::_updateConnection($mobile, $this->self_portal)) {
				// Update AvailMobile instance
				$mobile->save();
				
				$url = $this->owner->createAbsoluteUrl('access/', array('connection_token'=>$this->owner->connection->connection_token,
					'availment_id'=>$this->self->availment_id));
				$json_array = array('status'=>1, 'message'=>"You can use our WiFi FREE trial for today! Happy Surfing.", 'redirect'=>$url);
				die(CJSON::encode($json_array));
			}
			$json_array = array('status'=>0, 'message'=>'Something went wrong');
			die(CJSON::encode($json_array));
		}
		
		$this->render('verify_code', array('mobile'=>$mobile));
	}
	
	/**
	 * Resend code for mobile availment
	 * 
	 * @access private
	 * @param object $object AvailMobile instance
	 * @return JSON
	 */
	private function _resendCode(AvailMobile $object)
	{
		if ($object->auth_limit >= 3) {
			$json_array = array('status'=>0, 'message'=>'Maximum limit (3) of sending code already reached');
			die(CJSON::encode($json_array));
		}
		else {
			// new authentication code
			$object->auth_code = mt_rand(10000, 99999);
			$object->auth_limit++;
			$message = "{$object->auth_code} - This is your Globe WiFi verification code.";
			Mobile360::send($object->msisdn, $message);
			
			if ($object->validate() && $object->save()) {
				$url = $this->owner->createAbsoluteUrl('login/',
					array(	'connection_token'=>$this->owner->connection->connection_token,
							'availment_id'=>$this->self->availment_id,
							'reference'=>$object->device_availment_id,
							'action'=>'verify'));
				$json_array = array('status'=>1, 'message'=>'Kindly verify your mobile number by using the verification code sent to you', 'redirect'=>$url);
				die(CJSON::encode($json_array));
			}
		}
	}
	
	/**
	 * Update connection details
	 * 
	 * @access private
	 * @param object $mobile AvailMobile instance
	 * @param object $availment PortalAvailment instance
	 * @return boolean
	 */
	private function _updateConnection(AvailMobile $mobile, PortalAvailment $availment)
	{
		$connection = $this->owner->connection;
		$connection->username = $this->owner->device->mac_address.'@'.$this->owner->portal->code;
		$connection->password = $mobile->msisdn;
		
		if ($connection->validate() && $connection->save()) {
			// Verify connection
			if ($this->owner->connect($availment, TRUE)) {
				return TRUE;
			}
			return FALSE;
		}
		throw new CHttpException(500, Yii::t('yii','Cannot update connection. Please try again.'));
	}
	
	public function _resetLimit($mobile)
	{
		// Delete
		DeviceAvailment::model()->deleteByPk($mobile->device_availment_id);
		AvailMobile::model()->deleteByPk($mobile->avail_mobile_id);
		$url = $this->owner->createAbsoluteUrl('login/',
			array(	'connection_token'=>$this->owner->connection->connection_token,
					'availment_id'=>$this->self->availment_id,
					'action'=>'login'));
		$json_array = array('status'=>1, 'message'=>'Redirecting', 'redirect'=>$url);
		die(CJSON::encode($json_array));
	}
	
	private function _redirect(AvailMobile $mobile)
	{
		switch ($mobile->is_validated) {
			// Redirect to verification page
			case 0:
				$this->owner->redirect(array('login/',	'connection_token'=>$this->owner->connection->connection_token,
					'availment_id'=>$this->self->availment_id,
					'reference'=>$mobile->device_availment_id,
					'action'=>'verify'));
			break;
			
			// Already authenticated, back to home
			case 1:
				$this->owner->redirect(array('login/',	'connection_token'=>$this->owner->connection->connection_token,
					'availment_id'=>$this->self->availment_id,
					'action'=>'login'));
			break;
		}
	}
}