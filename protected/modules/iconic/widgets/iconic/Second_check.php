<?php
//session_start();

class Second_check extends AvailmentWidget
{
	public $self;
	public $self_portal;
	public $action;
	public $reference;
	
	public function init()
	{
		// Set PortalAvailment instance
		$this->self_portal = PortalAvailment::model()->findByAttributes(array('portal_id'=>$this->owner->portal->portal_id, 'availment_id'=>$this->self->availment_id));
	}
	
	public function run()
	{
		switch ($this->action)
		{
			// Login or Register function
			case 'login':
				self::_login();
			break;
		
			// Display the buttons
			default:
				self::_display();
			break;
		}
		
	}
	
	/**
	 * Display the widget
	 * 
	 * @access private
	 * @return void
	 */
	private function _display()
	{
		if (self::checkIfAvailed()) {
			$this->owner->redirect(array('login/', 'connection_token'=>$this->owner->connection->connection_token));
		}
		else {
			$this->owner->redirect(array('login/',
				'connection_token'=>$this->owner->connection->connection_token,
				'action'=>'login',
				'availment_id'=>$this->self->availment_id));
		}
	}
	
	/**
	 * Login or Register the user to the NAS
	 * 
	 * @access private
	 * @return void
	 */
	private function _login()
	{
		if (self::checkIfAvailed()) {
			$this->owner->redirect(array('login/', 'connection_token'=>$this->owner->connection->connection_token));
		}
		
		// Check for installed status
		$iconic_device = IconicDevice::model()->findByAttributes(array('device_id'=>$this->owner->device->device_id));
		if ( ! empty($iconic_device) AND $iconic_device->is_installed) {
			$app_avail = Availment::model()->findByAttributes(array('model'=>'AvailIconicApp'));
			$this->owner->redirect(array('login/',
				'connection_token'=>$this->owner->connection->connection_token,
				'availment_id'=>$app_avail->availment_id));
		}
		
		// New instance of DeviceAvailment
		$dev_avail = $this->owner->saveDeviceAvailment($this->self->availment_id);
		
		// Check if validation paseses
		if ($dev_avail->validate()) {
			// Update connection and save to DB
			if (self::_updateConnection($this->self_portal) && $dev_avail->save()) {
				$this->owner->redirect(array('access/',
					'connection_token'=>$this->owner->connection->connection_token,
					'availment_id'=>$this->self->availment_id));
			}
		}
	}
	
	/**
	 * Update connection details
	 * 
	 * @access private
	 * @return boolean
	 */
	private function _updateConnection(PortalAvailment $availment)
	{
		$connection = $this->owner->connection;
		$connection->username = $this->owner->device->mac_address.'@'.$this->owner->portal->code;
		$connection->password = $this->owner->device->mac_address;
		
		if ($connection->validate() && $connection->save()) {
			// Verify connection
			if ($this->owner->connect($availment, TRUE)) {
				return TRUE;
			}
		}
		
		throw new CHttpException(500, Yii::t('yii','Cannot update connection. Please try again.'));
	}
}