<?php

class LandingController extends CaptivePortal
{
	/**
	 * Declare variables
	 * 
	 * @access protected
	 * @param object $action CAction instance
	 * @return bool
	 */
	protected function beforeAction($action)
	{
		parent::beforeAction($action);
		
		return TRUE;
	}
	
	/**
	 * Show landing page
	 * 
	 * @access public
	 * @return void
	 */
	public function actionIndex()
	{
		$count = RadiusAcct::model()->count("DATE(acctstarttime) = DATE(NOW()) AND username LIKE '%iconic%' AND acctstoptime IS NULL");
		if ($count >= PortalConfiguration::getValue('MAX_USER')) {
			$this->redirect(array('access/deny', 'connection_token'=>$this->connection->connection_token));
		}
		$this->redirect(array('login/', 'connection_token'=>$this->connection->connection_token));
	}
}