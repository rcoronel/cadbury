
<?php

class Send_statusController extends Controller
{
	public $user;
	public $device;
	
	public function actionIndex()
	{
		$data = $_REQUEST;
		
		// Validate fields
		self::_validateFields($data);
		
		// Validate user
		self::_validateUser($data['uname'], $data['key']);
		
		// Validate device
		self::_validateDevice($data['mac_address']);
		
		// Save record
		self::_save($data);
	}
	
	/**
	 * Validate fields if all were given
	 * 
	 * @access private
	 * @param array $data
	 * @return boolean
	 */
	private function _validateFields($data)
	{
		if ( ! isset($data['uname']) OR ! isset($data['key']) OR ! isset($data['mac_address']) OR ! isset($data['status'])) {
			self::_returnStatus(0, 'Incomplete details');
		}
		return TRUE;
	}
	
	/**
	 * Validate user
	 * 
	 * @param string $username
	 * @param string $password
	 * @return boolean
	 */
	private function _validateUser($username, $password)
	{
		$this->user = IconicUser::model()->findByAttributes(array('username'=>$username));
		
		// Check for empty record
		if (empty($this->user)) {
			self::_returnStatus(0, 'No user found');
		}
		
		// Check for password
		if ( ! CPasswordHelper::verifypassword($password, $this->user->password)) {
			self::_returnStatus(0, 'Invalid credentials');
		}
		
		return TRUE;
	}
	
	/**
	 * Validate device
	 * 
	 * @access private
	 * @param string $mac_address
	 * @return boolean
	 */
	private function _validateDevice($mac_address)
	{
		$this->device = Device::model()->findByAttributes(array('mac_address'=>$mac_address));
		// Check for empty record
		if (empty($this->device)) {
			self::_returnStatus(0, 'Device record not found');
		}
		
		return TRUE;
	}
	
	/**
	 * Save records
	 * 
	 * @access private
	 * @param array $data
	 */
	private function _save($data)
	{
		$install = IconicDevice::model()->findByAttributes(array('device_id'=>$this->device->device_id));
		if (empty($install)) {
			$install = new IconicDevice();
		}
		
		$install->iconic_user_id = $this->user->iconic_user_id;
		$install->device_id = $this->device->device_id;
		$install->is_installed = (int)$data['status'];
		
		if ($install->validate() && $install->save()) {
			self::_returnStatus(1, 'Success');
		}
		
		self::_returnStatus(0, 'Internal error found');
	}
	
	/**
	 * Return the status based on the query
	 * 
	 * @access private
	 * @param int $status
	 * @param strint $message
	 * @return JSON
	 */
	private function _returnStatus($status, $message)
	{
		die(json_encode(array('status'=>$status, 'message'=>$message)));
	}
}