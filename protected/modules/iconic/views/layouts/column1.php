<?php /* @var $this Controller */ ?>
<?php $this->beginContent('/layouts/main'); ?>

<section class="container" id="<?php echo ($this->id == 'access') ? 'iconic-ads-wrapper' : '';?>">
	<div class="row">
		<div class="col-center-block">
			<?php echo $content;?>
		</div>
	 </div>
</section>

<?php $this->endContent(); ?>