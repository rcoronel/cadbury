<?php /* @var $this Controller */ ?>
<?php $this->beginContent('/layouts/main'); ?>
	<section class="container">
		<?php echo $content;?>
	</section>
<?php $this->endContent(); ?>