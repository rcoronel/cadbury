<div class="iconic-wifi-wrapper">
	<div class="block">
		<div class="col-md-12 col-center-block">
			<?php echo CHtml::image(Link::image_url("portal/{$this->portal->portal_id}/access-expired-icon.png"), 'Expired', array('class'=>'icon-size'));?>
			<div class="block">
				<h1 class="iconic-heading">Sorry!</h1>
				<h5>There are many connected users right now.</h5>
			</div>
		</div>
	</div>
	<div class="block">
		<p>Please try again later.</p>
	</div>
</div>