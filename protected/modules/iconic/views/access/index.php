<div class="block">
	<div class="success-notification">
		<h5>You are now connected to Iconic WiFi</h5>
	</div>

	<div class="iconic-ad-image-wrapper"></div>
</div>

<div class="block">
	<h1 class="iconic-heading">Wanna experience something cool?</h1>
	<p>Get to hear what's playing on the outdoor LED screens of Globe Iconic Store.</p>
	<p>Download the FREE Globe SoundPlay app now.</p>
	<div id="iconic-app-btns">
		<a href="https://play.google.com/store/apps/details?id=com.globe.SoundPlay">
			<?php echo CHtml::image(Link::image_url('portal/google-badge-btn.png'), 'SoundPlay', array('class'=>'iconic-app-badge'));?>
		</a>
		<a href="https://itunes.apple.com/en/app/globe-soundplay/id1111506396?mt=8">
			<?php echo CHtml::image(Link::image_url('portal/apple-badge-btn.png'), 'SoundPlay', array('class'=>'iconic-app-badge'));?>
		</a>
	</div>
	<p class="disclaimer add-padding">Already have the app? <br> Just open it to start your FREE internet</p>
</div>