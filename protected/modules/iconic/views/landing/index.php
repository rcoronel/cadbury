<div class="row">
	<div class="col-center-block">
		<div class="iconic-wifi-wrapper">
			<div class="block">
				<div class="col-md-12 col-center-block">
					<?php if ($this->logo):?>
						<?php echo CHtml::image($this->logo, $this->site_title);?>
					<?php endif;?>
				</div>
			</div>

			<div class="block">
				<p>Enjoy 3 hours of FREE access to Facebook, Instagram, Twitter, Snapchat, Viber, WhatsApp, Pinterest &amp; Globe sites.</p>
				<p class="bold">For you to hear &amp; enjoy what’s playing on the  outdoor LED screens of the Globe Iconic Store, get the FREE SoundPlay app.</p>
				<button class="blue-btn">Connect to the internet now</button>
				<p class="disclaimer add-padding">By clicking the button below you agree to these <br><a href="">Terms &amp; Conditions</a></p>
			</div>

		</div>
	</div>
</div>

<div class="modal modal-wifi" id="terms-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="myModalLabel">Terms of Agreement</h3>
			</div>
			<div class="modal-body text-overflow">
				<p>Terms of Service ("Terms")</p>
				<?php if ( ! empty($terms)):?>
					<?php echo $terms->content;?>
				<?php endif;?>
			</div>
			<div class="modal-footer">
			   <?php echo CHtml::htmlButton('Close', array('class'=>'btn primary-btn', 'data-dismiss'=>'modal'));?>
			   <div class="modal-btn-logo">
				   <?php echo CHtml::image(Link::image_url('powered-by-globe-modal.png'), 'Globe');?>
			   </div>
			</div>
		</div>
	</div>
</div>
<div class="modal modal-wifi" id="policy-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">Privacy Policy</h4>
			</div>
			<div class="modal-body text-overflow">
				<p>Privacy Policy ("Policy")</p>
				<?php if ( ! empty($policy)):?>
					<?php echo $policy->content;?>
				<?php endif;?>
			</div>
			 <div class="modal-footer">
				<?php echo CHtml::htmlButton('Close', array('class'=>'btn primary-btn', 'data-dismiss'=>'modal'));?>
				<div class="modal-btn-logo">
					<?php echo CHtml::image(Link::image_url('powered-by-globe-modal.png'), 'Globe');?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal modal-wifi in" id="loader-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-body small-modal-body-style">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('button#get-connected').on('click',function(e){	
			$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		});
	});
</script>