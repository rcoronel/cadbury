<div class="iconic-wifi-wrapper">
	<div class="block">
		<div class="col-md-12 col-center-block">
			<?php if ($this->inside_logo):?>
				<?php echo CHtml::image($this->inside_logo, $this->site_title);?>
			<?php endif;?>
		</div>
	</div>

	<div class="block">
		<p>You have <?php echo $duration;?> <?php echo $unit;?> of FREE internet access.</p>
		<?php $this->widget("{$this->portal->code}.widgets.{$availment->code}", array('self' => $availment));?>
		<p class="disclaimer add-padding">
				By clicking the button above you agree to these <br>
				<a href="#" data-toggle="modal" data-target="#agreement-modal">Terms &amp; Conditions</a>
			</p>
	</div>
</div>

<div class="modal fade modal-wifi" id="agreement-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<section id="iconic-tac" class="container modal-body text-overflow">
			   <div class="row">
				   <div class="col-center-block">
					   <div class="iconic-wifi-wrapper">
						   <div class="block">
							   <div class="modal-header">
								   <h1 class="modal-title" id="myModalLabel">SoundPlay End-User License Agreement</h1>
							   </div>
						   </div>

						   <div class="block">
								<?php echo $content->content;?>
							    <?php echo CHtml::htmlButton('Close', array('class'=>'blue-btn', 'data-dismiss'=>'modal'));?>
						   </div>
					   </div>
				   </div>
				</div>   
		   </section>
		</div>
	</div>
</div>

<div class="modal modal-wifi in" id="loader-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-body small-modal-body-style">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	
	$('form').on('submit',function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		//e.preventDefault();
	});
});
</script>