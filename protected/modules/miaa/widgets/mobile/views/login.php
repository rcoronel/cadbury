<?php echo CHtml::beginForm('', 'get');?>
	<?php echo CHtml::hiddenField('availment_id', $availment_id);?>
	<?php echo CHtml::hiddenField('action', 'login');?>
	<?php echo CHtml::htmlButton(html_entity_decode('LOG IN WITH MOBILE NO.'), array('type'=>'submit', 'class'=>'btn primary-btn btn-no-icon'));?>
<?php echo CHtml::endForm();?>
<?php if ( ! empty($fb)):?>
<p>
	<a href="<?php echo $this->owner->createAbsoluteUrl('login/', array('connection_token'=>$this->owner->connection->connection_token, 'availment_id'=>$fb->availment_id, 'action'=>'login'));?>" class="link-text-colored">
		No mobile no.? Login with your Facebook account
	</a>
</p>
<?php endif;?>