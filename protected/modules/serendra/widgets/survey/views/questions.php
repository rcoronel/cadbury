<?php if ( ! empty($question_post)):?>
	<?php foreach ($question_post as $id=>$answer_post):?>
		<?php echo CHtml::hiddenField("question[$id]");?>
		<?php foreach ($answer_post as $a):?>
			<?php echo CHtml::hiddenField("question[$id][]", $a);?>
		<?php endforeach;?>
	<?php endforeach;?>
<?php endif;?>

<?php foreach ($questions as $q):?>
	<h5><?php echo $q->question;?></h5>
	<?php foreach ($answers as $question_id=>$types):?>
		<?php if ($question_id == $q->survey_question_id):?>
			<?php foreach ($types as $type=>$as):?>
				<?php if ( ! empty($as)):?>
					<?php if ($type == 'text'):?>
						<?php foreach ($as as $a):?>
							<?php echo CHtml::textField("question[$q->survey_question_id][$a->survey_answer_id]", '', array('class'=>'form-control', 'placeholder'=>$a->answer));?>
						<?php endforeach;?>
					<?php endif;?>
					<?php if ($type == 'dropdown'):?>
						<?php echo CHtml::dropDownList("question[$q->survey_question_id][]", '', CHtml::listData($as, 'answer', 'answer'), array('class'=>'form-control', 'prompt'=>'---'));?>
					<?php endif;?>
					<?php if ($type == 'checkbox'):?>
						<br>
						<div class="radio-wrapper">
							<?php echo CHtml::hiddenField("question[$q->survey_question_id]");?>
							<?php echo CHtml::checkBoxList("question[$q->survey_question_id][]", '', CHtml::listData($as, 'answer', 'answer'));?>
						</div>
					<?php endif;?>
					<?php if ($type == 'radio'):?>
						<br>
						<div class="radio-wrapper">
							<?php echo CHtml::hiddenField("question[$q->survey_question_id]");?>
							<?php echo CHtml::radioButtonList("question[$q->survey_question_id][]", '', CHtml::listData($as, 'answer', 'answer'));?>
						</div>
					<?php endif;?>
				<?php endif;?>
			<?php endforeach;?>
		<?php endif;?>
	<?php endforeach;?>
	
	<div class="conditions-content-btn">
		<?php if ($position == $count) :?>
			<?php echo CHtml::htmlButton('SUBMIT', array('type'=>'submit', 'class'=>'btn primary-btn', 'disabled'=>'disabled'));?>
		<?php endif;?>
		
		<?php if ($position < $count) :?>
			<?php echo CHtml::htmlButton('NEXT', array('type'=>'submit', 'class'=>'btn primary-btn', 'disabled'=>'disabled'));?>
		<?php endif;?>
		
		<?php if ($position != 1) :?>
			<?php echo CHtml::htmlButton('RESTART', array('type'=>'button', 'class'=>'btn secondary-btn group-btn', 'onclick'=>"window.location='{$this->owner->createAbsoluteUrl('login/', array('connection_token'=>$this->owner->connection->connection_token, 'availment_id'=>$this->self->availment_id, 'action'=>'login'))}'"));?>
		<?php endif;?>
		
		<?php if ($position == 1) :?>
			<?php echo CHtml::htmlButton('BACK', array('type'=>'button', 'class'=>'btn secondary-btn group-btn', 'onclick'=>"window.location='{$this->owner->createAbsoluteUrl('login/', array('connection_token'=>$this->owner->connection->connection_token))}'"));?>
		<?php endif;?>
		
	</div>
<?php endforeach;?>
<script lang="text/javascript">
	$('input[type="radio"]').click(function() {
		$('button[type="submit"]').prop('disabled', false);
	});
	$('input[type="checkbox"]').click(function() {
		if ($('input:checkbox:checked').length > 0) {
			$('button[type="submit"]').prop('disabled', false);
		}
		if ($('input:checkbox:checked').length < 1) {
			$('button[type="submit"]').prop('disabled', true);
		}
	});
</script>