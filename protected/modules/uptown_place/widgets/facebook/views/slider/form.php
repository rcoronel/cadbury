<div class="table-responsive">
	<table id="slider-list" class="table tableDnD table-bordered table-hover dataTable">
		<thead>
			<tr class="nodrag nodrop">
				<th class="text-center col-md-1"><?php echo CHtml::encode('ID');?></th>
				<th class="col-md-2"><?php echo CHtml::encode('Title');?></th>
				<th class="text-center col-md-1"><?php echo CHtml::encode('Image');?></th>
				<th class="text-center col-md-1"><?php echo CHtml::encode('Position');?></th>
				<th class="text-center col-md-1"><?php echo CHtml::encode('Actions');?></th>
			</tr>
		</thead>
		<tbody>
			<?php if ( ! empty($sliders)):?>
				<?php foreach ($sliders as $s):?>
					<tr id="<?php echo $s->id_widget_dashboard_slider;?>">
						<td class="text-center col-md-1">
							<?php echo $s->id_widget_dashboard_slider;?>
						</td>
						<td><?php echo $s->title;?></td>
						<td class="text-center col-md-1">
							<?php echo CHtml::image(Link::base_url('uploads/dashboard_slider/'.$s->image), $s->title, array('style' => 'height:100px;'));?>
						</td>
						<td class="text-center col-md-1 dragHandle">
							<i class="fa fa-arrows"></i> &nbsp;<?php echo $s->position;?>
						</td>
						<td class="text-center col-md-1">
							<a href="<?php echo $this->owner->createURL('view', array('id' => $this->self->id_widget, 'action' => 'delete', 'reference' => $s->id_widget_dashboard_slider));?>" title="Delete" class="btn btn-default">
								<i class="fa fa-trash-o"></i>
							</a>
						</td>
					</tr>
				<?php endforeach;?>
			<?php else:?>
				<tr>
					<td colspan="5">
						<i class="fa fa-exclamation-circle"></i>
						No records found.
					</td>
				</tr>
			<?php endif;?>
		</tbody>
	</table>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("table.tableDnD").tableDnD({
			onDragClass: "dragging",
			onDrop: function(table, row){
				$.ajax({
					type: 'GET',
					headers: { "cache-control": "no-cache" },
					async: false,
					url: '<?php echo $this->owner->createUrl('/'.$this->owner->route, $_GET);?>',
					data: $.tableDnD.serialize(),
					success: function(data) {
						growlMessage(1, data);
					}
				});
			},
			dragHandle: "dragHandle"
		});
	});
</script>

<?php if ($slider->hasErrors()):?>
	<div id="form-error" class="alert alert-danger">
		<?php echo CHtml::errorSummary($slider);?>
	</div>
<?php endif;?>

<div class="col-lg-10">
	<?php $form=$this->beginWidget('CActiveForm', array('htmlOptions' => array('class'=>'form-horizontal', 'role' => 'form', 'method' => 'post', 'enctype' => 'multipart/form-data'))); ?>
		<div class="panel panel-default">
			<div class="form-group">
				<?php echo $form->labelEx($slider, 'title', array('class' => 'control-label col-lg-3'));?>
				<div class="col-lg-5">
					<?php echo $form->textField($slider, 'title', array('class' => 'form-control'));?>
				</div>
			</div>
			
			<div class="form-group">
				<?php echo $form->labelEx($slider, 'description', array('class' => 'control-label col-lg-3'));?>
				<div class="col-lg-5">
					<?php echo $form->textField($slider, 'description', array('class' => 'form-control'));?>
				</div>
			</div>
			
			<div class="form-group">
				<?php echo $form->labelEx($slider, 'url', array('class' => 'control-label col-lg-3'));?>
				<div class="col-lg-5">
					<?php echo $form->textField($slider, 'url', array('class' => 'form-control'));?>
				</div>
			</div>
			
			<div class="form-group">
				<?php echo $form->labelEx($slider, 'image', array('class' => 'control-label col-lg-3'));?>
				<div class="col-lg-5">
					<?php echo $form->fileField($slider, 'image', array('class' => 'form-control'));?>
				</div>
			</div>
		</div>
		<div class="box-footer">
			<button type="submit" class="btn btn-primary">Submit</button>
		</div>
	<?php $this->endWidget();?>
</div>