<?php

class LandingController extends CaptivePortal
{
	/**
	 * Declare variables
	 * 
	 * @access protected
	 * @param object $action CAction instance
	 * @return bool
	 */
	protected function beforeAction($action)
	{
		parent::beforeAction($action);
		
		return TRUE;
	}
	
	/**
	 * Show landing page
	 * 
	 * @access public
	 * @return void
	 */
	public function actionIndex()
	{
		// Use custom layout
		$this->layout = '/layouts/splash';
		
		$url = $this->createAbsoluteUrl('login/', array('connection_token'=>$this->connection->connection_token));
		$terms = Cms::model()->findByAttributes(array('portal_id'=>$this->portal->portal_id, 'friendly_url'=>'terms-and-conditions'));
		$policy = Cms::model()->findByAttributes(array('portal_id'=>$this->portal->portal_id, 'friendly_url'=>'privacy-policy'));
		
		$this->render('index', array('url'=>$url, 'terms'=>$terms, 'policy'=>$policy));
	}
}