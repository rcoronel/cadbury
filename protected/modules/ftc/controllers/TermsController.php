<?php

class TermsController extends CaptivePortal
{
	public function actionIndex()
	{
		$terms = Cms::model()->findByAttributes(array('portal_id'=>$this->portal->portal_id, 'friendly_url'=>'terms-and-conditions'));
		$url = $this->createAbsoluteUrl('landing/', array('connection_token'=>$this->connection->connection_token));
		$this->render('index', array('url'=>$url, 'terms'=>$terms));
	}
}