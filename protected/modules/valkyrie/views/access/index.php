<section class="container">
	<div class="row">
		<div class="col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
			<div id="ayala-app-wrapper" class="default-wrapper">
				<div class="default-content">
				   <h5 id="ayala-app-heading">You can now use Globe GoWiFi, Free trial today. Happy Surfing!</h5>
				</div>

				<div class="col-md-10 col-md-offset-1">
					<button class="btn secondary-btn" onclick="location.href='http://www.globe.com.ph/'">Start Browsing</button>
				</div>
				<footer>
					<?php echo CHtml::image(Link::image_url('portal/powered-by-globe-modal.png'), 'Globe');?>
				</footer>
			</div>
		</div>
	</div>
</section>