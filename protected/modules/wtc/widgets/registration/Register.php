<?php

class Register extends CWidget
{
	public $self;
	public $self_portal;
	public $action;
	public $reference;
	
	public function init()
	{
		// Set PortalAvailment instance
		$this->self_portal = PortalAvailment::model()->findByAttributes(array('portal_id'=>$this->owner->portal->portal_id, 'availment_id'=>$this->self->availment_id));
	}
	
	public function run()
	{
		switch ($this->action)
		{
			// Login or Register function
			case 'login':
				self::_login();
			break;
		
			// Display the buttons
			default:
				self::_display();
			break;
		}
	}
	
	/**
	 * Display the widget
	 * 
	 * @access private
	 * @return void
	 */
	private function _display()
	{
		$this->owner->redirect(array('login/',
			'connection_token'=>$this->owner->connection->connection_token,
			'availment_id'=>$this->self->availment_id,
			'action'=>'login'));
    }
	
	/**
	 * Login or Register the user to the NAS
	 * 
	 * @access private
	 * @return void
	 */
	private function _login()
	{
		Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/bootstrap-select.min.js'));
        Yii::app()->clientScript->registerScriptFile(Link::js_url('jquery/jquery.date-dropdowns.min.js'));
				
		// Check if already availed for today
		$availed = DeviceScenario::model()->exists("device_id = {$this->owner->device->device_id} AND portal_id = {$this->owner->portal->portal_id} AND availment_id = {$this->self->availment_id} AND DATE(updated_at) = DATE(NOW())");
		if ($availed) {
			$this->owner->redirect(array('access/', 'connection_token'=>$this->owner->connection->connection_token, 'availment_id'=>$this->self->availment_id));
		}
		
		$object = new AvailRegistration;
		if (Yii::app()->request->isPostRequest) {
			self::_validatePost($object);
		}
		
		$this->render('index', array('availment_id'=>$this->self->availment_id,
			'object'=>$object,
			'duration'=>$this->self_portal->duration/60));
	}
	
	/**
	 * Validate registration fields
	 * 
	 * @access private
	 * @param object $object AvailRegistration
	 * @return JSON
	 */
	private function _validatePost(AvailRegistration $object)
	{
		$post = Yii::app()->request->getPost('AvailRegistration');
		$object->attributes = $post;
		
		if ($object->validate()){
			// New instance of DeviceAvailment
			$dev_avail = $this->owner->saveDeviceAvailment($this->self->availment_id);
			
			if ($dev_avail->validate()) {
				if (self::_updateConnection($object, $this->self_portal) && $dev_avail->save()) {
					// Save record
					$object->device_availment_id = $dev_avail->device_availment_id;
					$object->save();
					
					$url = $this->owner->createAbsoluteUrl('access/', array('connection_token'=>$this->owner->connection->connection_token,
						'availment_id'=>$this->self->availment_id));
					$json_array = array('status'=>1, 'message'=>'You can use Globe GoWifi FREE trial today! Happy Surfing.', 'redirect'=>$url);
					die(CJSON::encode($json_array));
					
				}
			}
		}
		else {
			$json_array = array('status'=>0, 'message'=>'Fields marked with * are required');
			die(CJSON::encode($json_array));
		}
	}
	
	/**
	 * Update connection details
	 * 
	 * @access private
	 * @param object $object AvailRegistration instance
	 * @param object $availment PortalAvailment instance
	 * @return boolean
	 */
	private function _updateConnection(AvailRegistration $object, PortalAvailment $availment)
	{
		$connection = $this->owner->connection;
		$connection->username = $this->owner->device->mac_address.'@'.$this->owner->portal->code;
		$connection->password = $object->email;
		
		if ($connection->validate() && $connection->save()) {
			// Verify connection
			if ($this->owner->connect($availment, TRUE)) {
				return TRUE;
			}
			return FALSE;
		}
		throw new CHttpException(500, Yii::t('yii','Cannot update connection. Please try again.'));
	}
}