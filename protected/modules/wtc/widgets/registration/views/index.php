<div class="row">
	<div class="col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
		<div class="default-wrapper">
			<h1 class="captive-portal-heading">Account Verification</h1>
			<div class="captive-portal-sub-heading">
                
            </div>
			<?php $form=$this->beginWidget('CActiveForm', array('method'=>'post', 'id'=>'changenum-form')); ?>
				<div id="form-error" class="error-notification animated bounceIn" role="alert" style="display:none;"></div>
				<?php echo $form->labelEx($object,'firstname');?>
				<?php echo $form->textField($object, 'firstname', array('class'=>'form-control', 'placeholder'=>'First Name')); ?>
				
				<?php echo $form->labelEx($object,'lastname');?>
				<?php echo $form->textField($object, 'lastname', array('class'=>'form-control', 'placeholder'=>'Last Name')); ?>
				
				<div class="col-sm-6">
					<?php echo $form->labelEx($object,'age');?>
					<?php echo $form->textField($object, 'age', array('class'=>'form-control', 'placeholder'=>'Age', 'style'=>'width:80%')); ?>
				</div>
				
				<div class="col-sm-6">
					<?php echo $form->labelEx($object,'gender_id');?>
				<?php echo $form->dropDownList($object, 'gender_id', array(1=>'Male', 2=>'Female'), array('class'=>'form-control col-sm-10', 'prompt'=>'---')); ?>
				</div>
				
				<?php echo $form->labelEx($object,'email');?>
				<?php echo $form->textField($object, 'email', array('class'=>'form-control', 'placeholder'=>'E-mail Address')); ?>
				
				<?php echo $form->labelEx($object,'msisdn');?>
				<?php echo $form->textField($object, 'msisdn', array('class'=>'form-control', 'placeholder'=>'Mobile No.')); ?>
				
				<?php echo $form->labelEx($object,'cable_operator');?>
				<?php echo $form->textField($object, 'cable_operator', array('class'=>'form-control', 'placeholder'=>'Cable Operator')); ?>
				
				 <div id="default-to-action-btn-wrapper">
                	<span><?php echo CHtml::htmlButton('sign up', array('type'=>'submit', 'class'=>'btn primary-btn'));?></span>
					
                </div>
			<?php $this->endWidget();?>
		</div>
	</div>
</div>

<div class="modal modal-wifi in" id="verify-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content globe-modal-content">
			<div class="modal-header">
				<h4 class="modal-title sr-only" id="myModalLabel"></h4>
			</div>
			<div class="modal-body">
				<p></p>
			</div>
			<div class="modal-footer">
				<?php echo CHtml::htmlButton('Close', array('class'=>'btn secondary-btn'));?>
				<div class="modal-btn-logo">
					<?php echo CHtml::image(Link::image_url('powered-by-globe-modal.png'), 'Globe');?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="loader-modal" data-backdrop="static" data-keyboard="false" tabindex="-1">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$("#date-picker").dateDropdowns({
		displayFormat: "mdy",
		daySuffixes: false
	});
	$('form').on('submit', function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		// hide errors
		$('div#form-error').slideUp( 300 ).html();

		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");

		$.ajax( {
			url : formURL,
			type: "POST",
			data : postData,
			dataType: 'json',
			success:function(data, textStatus, jqXHR)
			{
				if ( ! data.status) {
					setTimeout(function() {
						$('#loader-modal').modal('hide');
						$('div#form-error').slideDown(300).delay(800).html(data.message);
						$('div#form-error').show();
					}, 900);
				}
				else {
					$('#loader-modal').modal('hide');
					$('div#verify-modal .modal-body').html(data.message);
					$('#verify-modal').modal({backdrop: 'static', keyboard: false});
					$('#verify-modal').on('click', function () {
						window.location.href = data.redirect;
					});
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				setTimeout(function() {
					$('#loader-modal').modal('hide');
					$('div#form-error').slideDown(300).delay(800).html(jqXHR.responseText);
					$('div#form-error').show();
				}, 900);   
			}
		});
		e.preventDefault();
	});
</script>