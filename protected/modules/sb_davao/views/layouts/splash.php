<?php /* @var $this Controller */ ?>
<?php $this->beginContent('/layouts/main'); ?>
	<div class="container">
		
		<?php echo $content;?>
		
	</div>

	<footer class="container">
        <div class="row">
            <div class="col-sm-12 col-md-3 col-md-offset-9">
                 <?php if (PortalConfiguration::getvalue('POWERED_WELCOME')):?>
					<?php echo CHtml::image(Link::image_url('powered-by-globe-white-logo-Small.png'), 'Globe');?>
				<?php endif;?>
            </div>
        </div>
    </footer>

<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-73795943-1', 'auto');
 ga('send', 'pageview');

</script>

<?php $this->endContent(); ?>