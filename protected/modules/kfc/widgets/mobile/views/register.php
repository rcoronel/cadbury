<div class="row">
	<div class="col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
		<div class="default-wrapper">
			<h1 class="captive-portal-heading">Account Verification</h1>
			<div class="captive-portal-sub-heading">
                <p>Verify your account information and get 30 additional surf minutes!</p>
            </div>
			<?php $form=$this->beginWidget('CActiveForm', array('method'=>'post', 'id'=>'changenum-form')); ?>
				<div id="form-error" class="error-notification animated bounceIn" role="alert" style="display:none;"></div>
				<?php echo $form->labelEx($object,'firstname');?>
				<?php echo $form->textField($object, 'firstname', array('class'=>'form-control', 'placeholder'=>'First Name')); ?>
				
				<?php echo $form->labelEx($object,'lastname');?>
				<?php echo $form->textField($object, 'lastname', array('class'=>'form-control', 'placeholder'=>'Last Name')); ?>
				
				<?php echo $form->labelEx($object,'birthday');?>
				<?php echo $form->textField($object, 'birthday', array('class'=>'form-control datepicker', 'placeholder'=>'Birthday')); ?>
				
				<?php echo $form->labelEx($object,'email');?>
				<?php echo $form->textField($object, 'email', array('class'=>'form-control', 'placeholder'=>'E-mail Address')); ?>
				
				<?php echo $form->labelEx($object,'city_id');?>
				<?php echo $form->dropDownList($object, 'city_id', CHtml::listData($cities, 'city_id', 'Name'), array('class'=>'form-control', 'prompt'=>'--- Select City')); ?>
				
				 <div id="default-to-action-btn-wrapper">
                	<span><?php echo CHtml::htmlButton('verify', array('type'=>'submit', 'class'=>'btn primary-btn'));?></span>
					<span><?php echo CHtml::htmlButton('continue without verifying', array('type'=>'button', 'class'=>'btn secondary-btn group-btn', 'onclick'=>"window.location='{$this->owner->createAbsoluteUrl('login/', array('connection_token'=>$this->owner->connection->connection_token))}'"));?></span>
                </div>
			<?php $this->endWidget();?>
		</div>
	</div>
</div>

<div class="modal fade" id="loader-modal" data-backdrop="static" data-keyboard="false" tabindex="-1">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(function() {
		$( ".datepicker" ).datepicker({
			minDate: "-120Y",
			maxDate: "-18Y",
			changeMonth: true,
			changeYear: true,
			yearRange: "-120:+0",
			dateFormat: 'MM d, yy' });
	});
	
	$('form').on('submit', function(e){
		$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		// hide errors
		$('div#form-error').slideUp(300).html();

		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");

		$.ajax( {
			url : formURL,
			type: "POST",
			data : postData,
			dataType: 'json',
			success:function(data, textStatus, jqXHR)
			{
				if ( ! data.status) {
					setTimeout(function() {
						$('#loader-modal').modal('hide');
						$('div#form-error').slideDown(300).delay(800).html(data.message);
						$('div#form-error').show();
					}, 900);
				}
				else {
					$('div.modal-body p').html(data.message);
					$('#loader-modal').modal({backdrop: 'static', keyboard: false});
					setTimeout(function(){
						window.location.href = data.redirect;
					}, 3000);
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				//if fails     
			}
		});
		e.preventDefault();
	});
</script>