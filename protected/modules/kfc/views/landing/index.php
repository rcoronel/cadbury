
<div id="captive-portal-wrapper" class="row">
	<section id="captive-portal-image" class="col-sm-8 col-sm-offset-2 col-md-6 display-cell">
		<div id="captive-portal-content-small" class="visible-xs visibile-table-portrait">
			<?php if ($this->logo):?>
				<?php echo CHtml::image($this->logo, $this->site_title, array('class'=>'kfc-logo'));?>
			<?php endif;?>
			<?php echo PortalConfiguration::getValue('LANDING_MESSAGE');?>
		</div>
		<?php if ($this->center_image):?>
			<?php echo CHtml::image($this->center_image, $this->site_title, array('class'=>'img-responsive globe-continue-logo'));?>
		<?php endif;?>
	</section>
	<section id="captive-portal-content" class="col-sm-12 col-md-6 display-cell">
		<div class="row">
			<div id="captive-portal-welcome-message" class="hidden-xs">
				<?php if ($this->logo):?>
					<?php echo CHtml::image($this->logo, $this->site_title);?>
				<?php endif;?>
				<?php echo PortalConfiguration::getValue('LANDING_MESSAGE');?>
			</div>
			<div id="captive-portal-button-wrapper">
				<?php echo CHtml::htmlButton('Get Connected', array('class'=>'btn yellow-btn', 'onclick'=>"window.location='{$url}'", 'id'=>'get-connected'));?>
				<h4>By clicking the button, you agree to the <a href="#" data-toggle="modal" data-target="#terms-modal" class="link-text">Terms of Service</a> and <a href="#" data-toggle="modal" data-target="#policy-modal" class="link-text">Privacy Policy.</a></h4>
			</div>
		</div>
	</section>
</div>

<div class="modal modal-wifi" id="terms-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="myModalLabel">Terms of Agreement</h3>
			</div>
			<div class="modal-body text-overflow">
				<p>Terms of Service ("Terms")</p>
				<?php if ( ! empty($terms)):?>
					<?php echo $terms->content;?>
				<?php endif;?>
			</div>
			<div class="modal-footer">
			   <?php echo CHtml::htmlButton('Close', array('class'=>'btn primary-btn', 'data-dismiss'=>'modal'));?>
			   <div class="modal-btn-logo">
				   <?php echo CHtml::image(Link::image_url('powered-by-globe-modal.png'), 'Globe');?>
			   </div>
			</div>
		</div>
	</div>
</div>
<div class="modal modal-wifi" id="policy-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">Privacy Policy</h4>
			</div>
			<div class="modal-body text-overflow">
				<p>Privacy Policy ("Policy")</p>
				<?php if ( ! empty($policy)):?>
					<?php echo $policy->content;?>
				<?php endif;?>
			</div>
			 <div class="modal-footer">
				<?php echo CHtml::htmlButton('Close', array('class'=>'btn primary-btn', 'data-dismiss'=>'modal'));?>
				<div class="modal-btn-logo">
					<?php echo CHtml::image(Link::image_url('powered-by-globe-modal.png'), 'Globe');?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal modal-wifi in" id="loader-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-body small-modal-body-style">
				<p>Please wait</p>
				<div class="progress progress-striped active" style="margin-bottom: 0px;">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('button#get-connected').on('click',function(e){	
			$('#loader-modal').modal({backdrop: 'static', keyboard: false});
		});
	});
</script>