<?php
//session_start();

class Easy_login extends CWidget
{
	public $self;
	public $self_portal;
	public $action;
	public $reference;
	
	public function init()
	{
		// Set PortalAvailment instance
		$this->self_portal = PortalAvailment::model()->findByAttributes(array('portal_id'=>$this->owner->portal->portal_id, 'availment_id'=>$this->self->availment_id));
	}
	
	public function run()
	{
		switch ($this->action)
		{
			// Login or Register function
			case 'login':
				self::_login();
			break;
		
			// Display the buttons
			default:
				self::_display();
			break;
		}
		
	}
	
	/**
	 * Display the widget
	 * 
	 * @access private
	 * @return void
	 */
	private function _display()
	{
		if ($this->owner->scene == 'new') {
			$this->owner->redirect(array('login/',
				'connection_token'=>$this->owner->connection->connection_token,
				'action'=>'login',
				'availment_id'=>$this->self->availment_id));
		}
		$this->render('login', array('availment_id'=>$this->self->availment_id));
	}
	
	/**
	 * Login or Register the user to the NAS
	 * 
	 * @access private
	 * @return void
	 */
	private function _login()
	{
		// Check if already availed for today
		$is_availed = DeviceAvailment::model()->exists("device_id = {$this->owner->device->device_id} AND portal_id = {$this->owner->portal->portal_id} AND availment_id = {$this->self->availment_id} AND DATE(created_at) = DATE(NOW())");
		if ($is_availed) {
			$this->owner->redirect(array('login/', 'connection_token'=>$this->owner->connection->connection_token));
		}
		
		// New instance of DeviceAvailment
		$dev_avail = $this->owner->saveDeviceAvailment($this->self->availment_id);
		
		// Check if validation paseses
		if ($dev_avail->validate()) {
			// Update connection and save to DB
			if (self::_updateConnection($this->self_portal) && $dev_avail->save()) {
				$this->owner->redirect(array('access/',
					'connection_token'=>$this->owner->connection->connection_token,
					'availment_id'=>$this->self->availment_id));
			}
		}
	}
	
	/**
	 * Update connection details
	 * 
	 * @access private
	 * @return boolean
	 */
	private function _updateConnection(PortalAvailment $availment)
	{
		$connection = $this->owner->connection;
		$connection->username = $this->owner->device->mac_address.'@'.$this->owner->portal->code;
		$connection->password = $this->owner->device->mac_address;
		
		if ($connection->validate() && $connection->save()) {
			// Verify connection
			if ($this->owner->connect($availment, TRUE)) {
				return TRUE;
			}
		}
		
		throw new CHttpException(500, Yii::t('yii','Cannot update connection. Please try again.'));
	}
}