<div class="row">
	<div class="col-md-12">
		<div class="form-panel panel panel-default" data-collapsed="0">
			<div class="panel-body">
				<?php echo CHtml::beginForm('', 'post', array('class'=>'form-horizontal style-form'));?>
					<?php echo CHtml::hiddenField('ajax', 1);?>
					<div class="form-group">
						<?php echo CHtml::label('Application ID', 'FB_APP_ID', array('class' => 'control-label col-lg-3'));?>
						<div class="col-md-5">
							<?php echo CHtml::textField('FB_APP_ID', $app_id, array('class'=>'form-control', 'placeholder'=>'Application ID'));?>
						</div>
					</div>
					<div class="form-group">
						<?php echo CHtml::label('Application Secret Key', 'FB_APP_SECRET', array('class' => 'control-label col-lg-3'));?>
						<div class="col-md-5">
							<?php echo CHtml::textField('FB_APP_SECRET', $app_secret, array('class'=>'form-control', 'placeholder'=>'Application Secret Key'));?>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<?php echo CHtml::button('Submit', array('class'=>'btn btn-primary', 'type'=>'submit'));?>
						</div>
					</div>
				<?php echo CHtml::endForm();?>
			</div>
		</div>	
	</div>
</div>

<script>
$(document).ready(function(){
	$('form').on('submit',function(e){
		// check if submitted via AJAX
		if ($('input[name="ajax"]').length && $('input[name="ajax"]').val())
		{
			// remove error style
			$('div.form-group').removeClass('has-error');

			// hide errors
			$('div#form-error').slideUp( 300 ).delay( 800 ).html();
			//$('div#form-error').hide();

			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				dataType: 'json',
				success:function(data, textStatus, jqXHR) {
					toastr.success(data.message, '', {closeButton:true,showDuration:"300",timeOut: "3000"});
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					//if fails     
				}
			});
			e.preventDefault(); //STOP default action
		}
	});
});
</script>
<?php $this->widget('widgets.ajax.Modal');?>