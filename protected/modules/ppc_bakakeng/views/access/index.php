<section class="container">
	<div class="row">
		<div class="col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
			<div id="puregold-wrapper" class="default-wrapper">
				<div class="default-content">
				   <h3 id="puregold-add-page-heading">You can now use Globe GoWiFi. Free trial for <?php echo $minutes;?> minutes today. Happy Surfing!</h3>
				</div>

				<div class="col-md-10 col-md-offset-1">
					<button class="btn secondary-btn" onclick="location.href='http://www.globe.com.ph/'">Start Browsing</button>
					
					<h4>Get the latest updates and promos for all</h4>

					<div id="ayala-app-btns">
						<a href="https://www.facebook.com/puregold.shopping/">
							<?php echo CHtml::image(Link::image_url('portal/puregold-addition-link-logo.jpg'), 'Puregold Pricing Club', array('class'=>'puregold-badge'));?>
						</a>
					</div>
				</div>
				<footer>
					<?php echo CHtml::image(Link::image_url('portal/powered-by-globe-modal.png'), 'Globe');?>
				</footer>
			</div>
		</div>
	</div>
</section>