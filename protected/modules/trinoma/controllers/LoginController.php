<?php

class LoginController extends CaptivePortal
{
	/**
	 * Declare variables
	 * 
	 * @access protected
	 * @param object $action CAction instance
	 * @return bool
	 */
	protected function beforeAction($action)
	{
		parent::beforeAction($action);
		
		return TRUE;
	}
	
	/**
	 * Default action
	 * 
	 * @access public
	 * @return void
	 */
	public function actionIndex()
	{
		// Remove cache
		header("Cache-Control: private, must-revalidate, max-age=0");
		header("Pragma: no-cache");
		header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

		$referer = $_SERVER['HTTP_REFERER'];
		$current_url = $this->createAbsoluteUrl('login/', array('connection_token'=>$this->connection->connection_token));
		
		// $referer = $_SERVER['HTTP_REFERER'];
		// $current_url = $this->createAbsoluteUrl('login/', array('connection_token'=>$this->connection->connection_token));
		
		// Check current scenario
		self::_checkScene();
		
		// if($referer != $current_url){
			$dc = DeviceConnection::model()->with('device')->find("device.device_id = {$this->device->device_id} AND portal_id = {$this->portal->portal_id}");
			
			$connected = new DeviceConnected;
			$connected->nas_id = $dc->nas_id;
			$connected->portal_id = $dc->portal_id; 
			$connected->device_id = $dc->device_id;
			$connected->site_id = $dc->site_id;
			$connected->ip_address = $dc->ip_address;
			$connected->access_point_group_id = $dc->access_point_group_id;
			$connected->access_point_id = $dc->access_point_id;
			$connected->ssid = $dc->ssid;
			$connected->created_at = date('Y-m-d H:i:s');
			$connected->validate() && $connected->save();

		// }
		
		// Check if form is submitted
		if (Yii::app()->request->isPostRequest OR Yii::app()->request->getParam('availment_id')) {
			self::_signIn(Yii::app()->request->getParam('availment_id'), Yii::app()->request->getParam('action'));
		}
		else {
			// Check if there is existing duration
			$session_duration = Device::getSessionTime($this->connection);
			$session_max = RadiusReply::getSessionMax($this->connection->username);
			if ($session_duration['current_session_time'] AND ($session_duration['current_session_time'] < $session_max->value)) {
				self::_showRemaining($session_duration['current_session_time'], $session_max->value);
			}
			else {
				self::_displayAvailments();
			}
		}
		
	}
	
	/**
	 * Check if new or returning user
	 * 
	 * @access private
	 * @return void
	 */
	private function _checkScene()
	{
		// Check for current level
		$latest_availment = ConnectionAvail::model()->findByAttributes(array('device_connection_id'=>$this->connection->device_connection_id), array('order'=>'Created_at DESC'));
		if ($latest_availment) {
			$this->level = $latest_availment->level+1;
		}
		else {
			$this->level = 1;
		}
		
		$count = 0;
		$availments = Availment::model()->findAll();
		
		foreach ($availments as $a) {
			$count = DeviceAvailment::model()->exists("availment_id = {$a->availment_id}"
			. " AND device_id = {$this->device->device_id}"
			. " AND portal_id = {$this->portal->portal_id}"
			. " AND DATE(created_at) < DATE(NOW())");
			if ($count) {
				break;
			}
		}
		
		// New or returning?
		if ( ! $count) {
			$this->scene = 'new';
			$this->scene_model = 'ScenarioAvailment';
		}
		else {
			$this->scene = 'returning';
			$this->scene_model = 'ScenarioReturning';
			if ( ! $this->user) {
				$fb = AvailFacebook::model()->with('dev_avail')->find("device_id = {$this->device->device_id}");
				if ($fb) {
					$this->user = $fb->Name;
				}
			}
			
			if ( ! $this->user) {
				$mobtel = AvailMobile::model()->with('dev_avail')->find("device_id = {$this->device->device_id} AND portal_id = {$this->portal->portal_id}");
				if ($mobtel) {
					$this->user = $mobtel->msisdn;
				}
			}
		}
	}
	
	/**
	 * Display availments
	 * 
	 * @access private
	 * @return void
	 */
	private function _displayAvailments()
	{
		$model = $this->scene_model;
		
		// Get scenarios
		$device_avails = DeviceScenario::model()->findAllByAttributes(array('device_id'=>$this->device->device_id, 'portal_id'=>$this->portal->portal_id, 'scene'=>$this->scene));
		if ( ! empty($device_avails)) {
			$this->scenarios = $model::model()->findScenario($device_avails, $this->portal->portal_id);
		}
		
		// Check if current scene is returning and empty availments
		// Check previous availments to get scenario
		if ($this->scene == 'returning' AND empty($device_avails)) {
			$device_avails = DeviceScenario::model()->findAllByAttributes(array('device_id'=>$this->device->device_id, 'portal_id'=>$this->portal->portal_id, 'scene'=>'new'));
			if ( ! empty($device_avails)) {
				$this->scenarios = ScenarioAvailment::model()->findScenario($device_avails, $this->portal->portal_id);
			}
		}
		
		// Availment container
		$availment = array();

		// Get acquired availment methods
		$latest_availment = ConnectionAvail::model()->findByAttributes(array('device_connection_id'=>$this->connection->device_connection_id), array('order'=>'Created_at DESC'));
		$latest_availments = ConnectionAvail::model()->findAllByAttributes(array('device_connection_id'=>$this->connection->device_connection_id), array('order'=>'Created_at DESC'));

		if ( ! empty($latest_availment) &&  ! empty($latest_availments)) {
			if (empty($this->scenarios)) {
				// Set where string
				$where_string = "({$this->scene}.availment_id = {$latest_availment->availment_id} AND {$this->scene}.level = {$latest_availment->level} AND portal_id = {$this->portal->portal_id})";

				// Append to where string
				foreach ($latest_availments as $la) {
					if ($la->availment_id != $latest_availment->availment_id && $la->level != $latest_availment->level) {
						$where_string .= " OR ({$this->scene}.availment_id = {$la->availment_id} AND {$this->scene}.level = {$la->level})";
					}
				}
				
				// Get scenarios based on where string
				$alt_scenarios = Scenario::model()->with($this->scene)->findAll($where_string);
				$count = count($latest_availments);

				// Remove scenarios incomplete scenarios
				foreach ($alt_scenarios as $index=>$as) {
					if ($count != count($as->{$this->scene})) {
						unset($alt_scenarios[$index]);
					}
				}

				// Get scenarios
				if ( ! empty($alt_scenarios)) {
					foreach ($alt_scenarios as $as) {
						$scenarios[] = Scenario::model()->with($this->scene)->findByAttributes(array('portal_id'=>$this->portal->portal_id, 'scenario_id'=>$as->scenario_id), array('order'=>'level ASC'));
					}
				}
				else {
					$scenarios = Scenario::model()->with($this->scene)->findAllByAttributes(array('portal_id'=>$this->portal->portal_id), array('order'=>'level ASC'));
				}
			}
			else {
				$criteria = new CDbCriteria;
				$criteria->addCondition("level >= {$this->level}");
				$criteria->addInCondition('t.scenario_id', $this->scenarios);
				$criteria->order = 'level ASC';
				$scenarios = Scenario::model()->with($this->scene)->findAll($criteria);
			}
			
			// latest availment
			//$availment = PortalAvailment::model()->find("portal_id = {$this->portal->portal_id} AND availment_id = {$latest_availment->availment_id}");
			$availment = $latest_availment;
		}
		else {
			if (empty($this->scenarios)) {
				$scenarios = Scenario::model()->with($this->scene)->findAllByAttributes(array('portal_id'=>$this->portal->portal_id), array('order'=>'level ASC'));
			}
			else {
				$criteria = new CDbCriteria;
				$criteria->addCondition("level >= {$this->level}");
				$criteria->addInCondition('t.scenario_id', $this->scenarios);
				$criteria->order = 'level ASC';
				$scenarios = Scenario::model()->with($this->scene)->findAll($criteria);
			}
		}
		
		// Get all availments from all scenarios
		$availments = array();
		foreach ($scenarios as $s) {
			if ( ! empty($s)) {
				foreach ($s->{$this->scene} as $index => $a) {
					$availments[] = $a;
				}
			}
		}
		
		// If there are availments available
		if ( ! empty($availments)) {
			
			// Remove currently availed for today
			// Useful when the scenarios are updated in the CMS
			if ( ! empty ($latest_availments)) {
				foreach ($availments as $index => $a) {
					foreach ($latest_availments as $la) {
						if ($la->availment_id == $a->availment_id) {
							unset($availments[$index]);
						}
					}
				}
			}
			
			
			// Remove non-recurring which were already availed
			foreach ($availments as $index => $a) {
				// Get availment model
				$model = $a->availment->model;

				// Check if existing
				$is_exists = DeviceAvailment::model()->exists("availment_id = {$a->availment_id}"
			. " AND device_id = {$this->device->device_id}"
			. " AND portal_id = {$this->portal->portal_id}"
			. " AND DATE(created_at) < DATE(NOW())");

				// Check configuration on NAS
				$nas_availment = PortalAvailment::model()->findByAttributes(array('portal_id'=>$a->scenario->portal_id, 'availment_id'=>$a->availment_id));
				$a->availment->duration = $nas_availment->duration / 60;
				$a->availment->description = $nas_availment->description;
				
				// Check if already existing and non-recurring
				if ($is_exists AND ! $nas_availment->is_recurring) {
					unset($availments[$index]);
				}
			}
			
			// Remove duplicates
			foreach ($availments as $index_a => $a) {
				foreach ($availments as $index_b => $b) {
					if ($index_a != $index_b && $a->level == $b->level && $a->availment_id == $b->availment_id) {
						unset($availments[$index_a]);
					}
				}
			}
			
			// Sort availments by level in ascending order
			usort($availments, function($a, $b) {  
				if( $a->level == $b->level) {
					return 0; 
				}
				return $a->level > $b->level ? 1 : -1;
			});
		}
		
		if ( ! empty($availments)) {
			// Change level to the first level available
			$this->level = $availments[0]->level;
			$this->connection->level = $this->level;
			if ( ! $this->connection->validate() OR ! $this->connection->save()) {
				$this->redirect(array('scene_handler/', 'connection_token'=>$this->connection->connection_token));
			}

			// Remove availments which is not equal to current level
			foreach ($availments as $index=>$a) {
				if ($a->level != $this->connection->level) {
					unset($availments[$index]);
				}
			}
		}
		
		// Sort by name
		if ( ! empty($availments)) {
			usort($availments, function($a, $b) {
				return strcmp($a->availment->name, $b->availment->name);
			});
		}
		
		$content = Cms::model()->findByAttributes(array('portal_id'=>$this->portal->portal_id, 'friendly_url'=>'service-agreement'));
		$url = $this->createAbsoluteUrl('scene_handler/terms', array('connection_token'=>$this->connection->connection_token));
		$this->render('index', array('availments'=>$availments, 'url'=>$url, 'content'=>$content, 'availment'=>$availment, 'connection_token'=>$this->connection->connection_token));
	}

	/**
	 * Show remaining duration of the current availment method chosen
	 * 
	 * @access private
	 * @return void
	 */
	private function _showRemaining($current_session_time, $max_session_time)
	{
		$remaining_session = (int)$max_session_time - (int)$current_session_time;
		$minutes = 0;
		// If duration is less than 90 seconds
		if ($remaining_session < 90 ) {
			$minutes = 1;
		}
		else {
			$minutes = $remaining_session / 60;
		}
		
		// Get latest availed method
		$latest_avail = ConnectionAvail::model()->findByAttributes(array('device_connection_id'=>$this->connection->device_connection_id), array('order'=>'created_at DESC'));
		$availment = PortalAvailment::model()->findByAttributes(array('portal_id'=>$this->portal->portal_id, 'availment_id'=>$latest_avail->availment_id));
		
		if (Yii::app()->request->isPostRequest) {
			$this->connect($availment);
		}
		
		$this->render('remaining_session', array('availment'=>$latest_avail, 'minutes'=>$minutes));
	}
	
	/**
	 * Sign-in based on POST protocol
	 * 
	 * @access private
	 * @param int $id
	 * @param string $action
	 * @return void
	 */
	private function _signIn($id, $action = 'login')
	{
		if (empty($id)) {
			$id = Yii::app()->request->getPost('availment_id');
		}
		
		$availment = PortalAvailment::model()->findByAttributes(array('portal_id'=>$this->portal->portal_id, 'availment_id'=>$id));

		// Get availment of Mobile
		$globe_duration = 0;
		$xglobe_duration = 0;
		$mobile_availment = Availment::model()->findByAttributes(array('model'=>'AvailMobile'));

		if ($mobile_availment && $availment->availment_id == $mobile_availment->availment_id) {
			// Get availment settings
			$globe_duration = AvailmentSetting::getValue($mobile_availment->availment_id, 'GLOBE_DURATION');
			$xglobe_duration = AvailmentSetting::getValue($mobile_availment->availment_id, 'XGLOBE_DURATION');

			// If settings are declared, check for AvailMobile object
			if ($globe_duration || $xglobe_duration) {
				$mobile = AvailMobile::model()->with('dev_avail')->find("portal_id = {$this->portal->portal_id} AND device_id = {$this->device->device_id} AND is_validated = 1", array('order'=>'created_at DESC'));
				if ( ! empty($mobile)) {
					$msisdn = substr($mobile->msisdn, 1);
					$prefix = Yii::app()->db->createCommand()->select('*')
						->from('Unifi_Captive.plan_prefix')->where("LEFT(prefix, 3) = LEFT({$msisdn}, 3)")
						->queryRow();

					if ($prefix) {
						if ($prefix['plan_id'] == 1 OR $prefix['plan_id'] == 2 OR $prefix['plan_id'] == 3 OR $prefix['plan_id'] == 9) {
							if ($globe_duration) {
								$availment->duration = $globe_duration * 60;
							}
						}
						else {
							if ($xglobe_duration) {
								$availment->duration = $xglobe_duration * 60;
							}
						}
					}
				}
			}
		}
		
		// Check if there is existing duration
		$session_duration = Device::getSessionTime($this->connection);
		$session_max = RadiusReply::getSessionMax($this->connection->username);
		if ($session_duration['current_session_time'] AND ($session_duration['current_session_time'] < $session_max->value)) {
			$this->connect($availment);
		}
		
		$this->render('login', array('availment'=>$availment, 'action'=>$action));
	}
}