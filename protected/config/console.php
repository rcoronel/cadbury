<?php
date_default_timezone_set('Asia/Manila');
// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Console Application',

	// preloading 'log' component
	'preload'=>array('log'),
	
	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.extensions.*',
		'application.widgets.*',
	),

	// application components
	'components'=>array(

		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),
		'db_cp'=>require(dirname(__FILE__).'/database-cp.php'),
		'db_cp_old'=>require(dirname(__FILE__).'/database-cp-old.php'),
		'db_cms'=>require(dirname(__FILE__).'/database-cms.php'),
		'db_radius'=>require(dirname(__FILE__).'/database-radius.php'),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),

	),
);
