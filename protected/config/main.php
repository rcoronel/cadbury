<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// Widgets alias
Yii::setPathOfAlias('widgets', dirname(__FILE__).'/../widgets');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Web Application',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.extensions.*',
		'application.extensions.mobile-detect.*',
	),

	'modules'=>array(
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'Uvj8sbQ5qNz65DtY',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1', '112.199.104.126', '124.6.162.170'),
		), 'abreeza', 'ac_cebu', 'atc',
		'backoffice', 'bench_fix', 'bhs', 'boracay',
		'centrio_cdo', 'cebu_mactan', 'citywalk', 'controlpanel', 'close_up', 'cybermall',
		'eastwood',
		'fairview_terrace', 'family_mart', 'ftc',
		'gateway', 'glorietta', 'greenbelt',
		'harbor_point',
		'iconic', 'immap',
		'kenny_roger', 'kfc',
		'lctm',
		'mall_168', 'market_market', 'marquee', 'miaa',
		'naia',
		'ppc_binan',
		'sbc', 'sb_threetwo', 'serendra',
		'trinoma',
		'up_town', 'uptown_place',
		'valkyrie', 'vgcm',
		'wendys', 'wtc', 'wwg',
		'yondu'
	),

	// application components
	'components'=>array(

		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),

		//for encryption/decryption
		'securityManager'=>array(
           	'cryptAlgorithm' => 'cast-256',
            //'encryptionKey' => 'un1fic@dburywif1',
        	),

		//file process
		'file' => array(
	        'class' => 'application.extensions.file.CFile',
	     ),
		
		// uncomment the following to enable URLs in path-format
		/*
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		*/

		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),
		'db_cp'=>require(dirname(__FILE__).'/database-cp.php'),
		'db_cp_old'=>require(dirname(__FILE__).'/database-cp-old.php'),
		'db_cms'=>require(dirname(__FILE__).'/database-cms.php'),
		'db_log'=>require(dirname(__FILE__).'/database-log.php'),
		'db_radius'=>require(dirname(__FILE__).'/database-radius.php'),
		
		'session'=>array(
			'class'=>'CDbHttpSession',
			'connectionID'=>'db',
			'sessionTableName'=>'admin_session'
		),

		'errorHandler'=>array(
			'errorAction'=>'control_box/error',
		),

//		'log'=>array(
//			'class'=>'CLogRouter',
//			'routes'=>array(
//				array(
//					'class'=>'CFileLogRoute',
//					'levels'=>'error, warning, profile',
//				),
//				// uncomment the following to show log messages on web pages
//				
//				array(
//					'class'=>'CWebLogRoute',
//					//'showInFireBug'=>true
//				),
//				array(
//                    'class'=>'CProfileLogRoute',
//                    'report'=>'summary',
//					'showInFireBug'=>true
//                    // lists execution time of every marked code block
//                    // report can also be set to callstack
//                ),
//			),
//		),
		'request' => array(
			'enableCsrfValidation' => false,
		),
		
		's3'=>array(
			'class'=>'ext.aws.ES3',
			'aKey'=>'AKIAJG3KSP42LYRFBPPQ', 
			'sKey'=>'ueES57A3vaTjIkg1qPY60sMoNwdC7sj2Scu9j7Ue',
			'bucket'=>'yondu-unifi'
		),
		'file'=>array(
			'class'=>'application.extensions.file.CFile',
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);
