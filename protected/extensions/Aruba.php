<?php
/*
|--------------------------------------------------------------------------
| Aruba API Class
|--------------------------------------------------------------------------
|
| Handles the API calls to the Access Controller that is handled by Aruba
|
| @category		Extensions
| @author		Ryan Coronel
*/
class Aruba implements APIInterface
{
	public static function addUser($role = 'Cadbury-Cloud-logon')
	{
		$ip_address = Yii::app()->controller->connection->ip_address;
		$mac_address = Yii::app()->controller->device->mac_address;
		
		$xml = '<aruba command="user_add">
		  <ipaddr>' . $ip_address . '</ipaddr>
		  <macaddr>' . $mac_address . '</macaddr>
		  <role>' . $role . '</role>
		  <key>' . Yii::app()->controller->nas->secret . '</key>
		  <authentication>cleartext</authentication>
		  <version>1.0</version>
		</aruba>';
		
		return self::api($xml);
	}
//
//
	public static function queryUser()
	{
		$ip_address = Yii::app()->controller->connection->ip_address;
		$mac_address = Yii::app()->controller->device->mac_address;
		
		$xml = '<aruba command="user_query">
		  <ipaddr>' . $ip_address . '</ipaddr>
		  <macaddr>' . $mac_address . '</macaddr>
		  <key>' . Yii::app()->controller->nas->secret . '</key>
		  <authentication>cleartext</authentication>
		  <version>1.0</version>
		</aruba>';

		return self::api($xml, TRUE);
	}
//
//
//	public function deleteUser($ip, $mac, $key)
//	{
//		$xml = '<aruba command="user_delete">
//		  <ipaddr>' . $ip . '</ipaddr>
//		  <macaddr>' . $mac . '</macaddr>
//		  <key>' . $key . '</key>
//		  <authentication>cleartext</authentication>
//		  <version>1.0</version>
//		</aruba>';
//
//		return self::api($xml);
//	}
//
//	public function blacklistUser($url, $key, $ip, $mac)
//	{
//		$xml = '<aruba command="user_blacklist">
//		  <ipaddr>' . $ip . '</ipaddr>
//		  <macaddr>' . $mac . '</macaddr>
//		  <key>' . $key . '</key>
//		  <authentication>cleartext</authentication>
//		  <version>1.0</version>
//		</aruba>';
//
//		return self::api($url, $xml);
//	}

	/**
	 * Authenticate user in NAS
	 * 
	 * @access public
	 * @param string $username Device Username
	 * @param string $password Device Password
	 * @return bool
	 */
	public static function authenticateUser($username, $password)
	{
		$ip_address = Yii::app()->controller->connection->ip_address;
		$mac_address = Yii::app()->controller->device->mac_address;
	
		$xml = '<aruba command="user_authenticate">
		  <ipaddr>' . $ip_address . '</ipaddr>
		  <macaddr>' . $mac_address . '</macaddr>
		  <name>' . $username . '</name>
		  <password>' . $password . '</password>
		  <key>' . Yii::app()->controller->nas->secret . '</key>
		  <authentication>cleartext</authentication>
		  <version>1.0</version>
		</aruba>';

		return self::api($xml);
	}
	
	/**
	 * Delete access in NAS
	 * 
	 * @access public
	 * @return bool
	 */
	public static function deleteUser()
	{
		$ip_address = Yii::app()->controller->connection->ip_address;
		$mac_address = Yii::app()->controller->device->mac_address;
		
		$xml = '<aruba command="user_delete">
		  <ipaddr>' . $ip_address . '</ipaddr>
		  <macaddr>' . $mac_address . '</macaddr>
			<name>fb_guest</name>
		  <key>' . Yii::app()->controller->nas->secret . '</key>
		  <authentication>cleartext</authentication>
		  <version>1.0</version>
		</aruba>';

		return self::api($xml);
	}

	/**
	 * XML API handler
	 * 
	 * @access private
	 * @param string $xml XML
	 * @param bool $response
	 * @return boolean
	 * @throws CHttpException
	 */
	private static function api($xml, $response = FALSE)
	{
		$nas = Yii::app()->controller->nas;
		$device = Yii::app()->controller->device;
		
		$url = "https://{$nas->nasname}/auth/command.xml";
		if ($nas->id == 2 || $nas->id == 5 || $nas->id == 8 || $nas->id == 9 || $nas->id == 4  || $nas->id == 11  || $nas->id == 12  || $nas->id == 13 || $nas->id == 14  || $nas->id == 15 || $nas->id == 16 || $nas->id == 17  || $nas->id == 18  || $nas->id == 19  || $nas->id == 20   || $nas->id ==  21) {
			$url = "https://{$nas->nasname}:4343/auth/command.xml";
		}
		//$url = "https://124.6.162.170/auth/command.xml";
		//$url = "https://112.199.104.126/auth/command.xml";
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT , 60);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "xml=$xml");
		$output = curl_exec($ch);
		curl_close($ch);
		
		$file = simplexml_load_string($output);
		
		$log = new ArubaLog;
		$log->nas_id = $nas->id;
		$log->site_id = Yii::app()->controller->site->site_id;
		$log->portal_id = Yii::app()->controller->portal->portal_id;
		$log->access_point_group_id = Yii::app()->controller->ap_group->access_point_group_id;
		$log->access_point_id = Yii::app()->controller->ap->access_point_id;
		$log->device_id = $device->device_id;
		$log->url = $url;
		
		if (empty($file)) {
			$log->status = 99;
			$log->request = CJSON::encode($xml);
			$log->response = 'No response';
			$log->validate() && $log->save();
			
			throw new CHttpException(400,Yii::t('yii','No response on authentication. Please try again.'));
		}
		
		if ((int)$file->code != 0) {
			$log->status = (int)$file->code;
			$log->request = CJSON::encode($xml);
			$log->response = CJSON::encode($output);
			$log->validate() && $log->save();
			throw new CHttpException(400,Yii::t('yii',"Error encountered. Please try again. (ARUBA: $log->status)"));
		}
		
		$log->status = (int)$file->code;
		$log->request = CJSON::encode($xml);
		$log->response = CJSON::encode($output);
		$log->validate() && $log->save();
		
		if ($response) {
			return $output;
		}
		
		return TRUE;
	}
}