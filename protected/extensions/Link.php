<?php
/*
|--------------------------------------------------------------------------
| Link Generator Library Class
|--------------------------------------------------------------------------
|
| Handles the generation of links
| This class is mainly use for referencing of external files
| To generate links for controllers, etc., please use the framework's function
| in order to avoid dependencies
|
| @category		Extensions
| @author		Ryan Coronel
*/
class Link
{
	/**
	* Generate URL based on application
	* 
	* @param	string $link Slash-prefix url
	* @return	string
	*/
	public static function base_url($link = '')
	{
		return Yii::app()->request->getBaseUrl(true) . '/' .$link;
	}
	
	/**
	 * Generate image URL
	 * 
	 * @access public
	 * @param mixed $link Non-slash prefix url
	 * @return	string
	 */
	public static function image_url($link = '')
	{
		if ( ! is_array($link) && filter_var($link, FILTER_VALIDATE_URL)) {
			return $link;
		}
		
		if (is_array($link)) {
			array_unshift($link, 'images');
			$link = implode('/', $link);
		}
		else {
			$link = 'images/'.$link;
		}
		
		return self::base_url($link);
	}
	
	/**
	 * Generate CSS URL
	 * 
	 * @access public
	 * @param mixed $link Non-slash prefix url
	 * @return	string
	 */
	public static function css_url($link = '')
	{
		if ( ! is_array($link) && filter_var($link, FILTER_VALIDATE_URL)) {
			return $link;
		}
		
		if (is_array($link)) {
			array_unshift($link, 'css');
			$link = implode('/', $link);
		}
		else {
			$link = 'css/'.$link;
		}
		
		return self::base_url($link);
	}
	
	/**
	 * Generate Javascript URL
	 * 
	 * @access public
	 * @param mixed $link Non-slash prefix url
	 * @return	string
	 */
	public static function js_url($link = '')
	{
		if ( ! is_array($link) && filter_var($link, FILTER_VALIDATE_URL)) {
			return $link;
		}
		
		if (is_array($link)) {
			array_unshift($link, 'js');
			$link = implode('/', $link);
		}
		else {
			$link = 'js/'.$link;
		}
			
		return self::base_url($link);
	}
	
	/**
	 * Generate Javascript URL
	 * 
	 * @access public
	 * @param mixed $link Non-slash prefix url
	 * @return	string
	 */
	public static function font_url($link = '')
	{
		if ( ! is_array($link) && filter_var($link, FILTER_VALIDATE_URL)) {
			return $link;
		}
		
		if (is_array($link)) {
			array_unshift($link, 'fonts');
			$link = implode('/', $link);
		}
		else {
			$link = 'fonts/'.$link;
		}
			
		return self::base_url($link);
	}
	
	/**
	 * Generate uploads URL
	 * 
	 * @access public
	 * @param mixed $link Non-slash prefix url
	 * @return	string
	 */
	public static function upload_url($link = '')
	{
		if ( ! is_array($link) && filter_var($link, FILTER_VALIDATE_URL)) {
			return $link;
		}
		
		if (is_array($link)) {
			array_unshift($link, 'uploads');
			$link = implode('/', $link);
		}
		else
			$link = 'uploads/'.$link;
		
		return self::base_url($link);
	}
}