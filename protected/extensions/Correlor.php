<?php
/*
|--------------------------------------------------------------------------
| Correlor API Class
|--------------------------------------------------------------------------
|
| Handles the API calls for Correlor
|
| @category		Extensions
| @author		Ryan Coronel
*/
class Correlor
{
	/**
	 * Anonymous user
	 * 
	 * @param int $id
	 * @param string $model
	 * @return boolean
	 */
	public static function userAnonymous($id, $model)
	{
		return TRUE;
		
		$user = $model::model()->findByPk($id);
		$url = "https://api.correlor.com/scr/api/customers/19/users/anonymous?idAtCustomer={$id}";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		$http_status_json = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		
		$decode_result = CJSON::decode($result);
		$user->correlor_id = $decode_result['user']['id'];
		$user->save();
		
		$log = new CorrelorLog;
		$log->nas_id = Yii::app()->controller->nas->id;
		$log->site_id = Yii::app()->controller->site->site_id;
		$log->portal_id = Yii::app()->controller->portal->portal_id;
		$log->access_point_group_id = Yii::app()->controller->ap_group->access_point_group_id;
		$log->access_point_id = Yii::app()->controller->ap->access_point_id;
		$log->device_id = $id;
		$log->url = $url;
		$log->request = $url;
		$log->response = $result;
		$log->status = $http_status_json;
		$log->validate() && $log->save();
		
		return TRUE;
	}
	
	/**
	 * Login user
	 * 
	 * @param int $id Primary key of the record
	 * @param string $model Model name to search the record
	 */
	public static function userLogin($id, $model = 'AvailFacebook')
	{
		return TRUE;
		
		$user = $model::model()->findByPk($id);
		$dev_avail = DeviceAvailment::model()->findByPk($user->device_availment_id);
		$device = Device::model()->findByPk($dev_avail->device_id);
		
		$url = 'https://api.correlor.com/scr/api/customers/19/users';
		$data = array(
			'snUserId' => $user->fb_id,
			'accessToken' => $user->access_token,
			'accessTokenExpires' => date('U', strtotime($user->access_expiry)),
			'idAtCustomer' => $dev_avail->device_id,
			'anonymousId' => $device->correlor_id);
		
		$data_string = json_encode($data);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Accept  application/json',
				'Content-Type: application/json; charset=UTF-8',
				'Content-Length: ' . strlen($data_string))
		);

		$result = curl_exec($ch);
		$http_status_json = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		
		$decode_result = CJSON::decode($result);
		
		// Save to model
		$user->correlor_id = $decode_result['user']['id'];
		$user->validate() && $user->save();
		
		// Replace the correlor_id in device
		$device->correlor_id = $user->correlor_id;
		$device->validate() && $device->save();
		
		// Save logs
		$log = new CorrelorLog;
		$log->nas_id = Yii::app()->controller->nas->id;
		$log->site_id = Yii::app()->controller->site->site_id;
		$log->portal_id = Yii::app()->controller->portal->portal_id;
		$log->access_point_group_id = Yii::app()->controller->ap_group->access_point_group_id;
		$log->access_point_id = Yii::app()->controller->ap->access_point_id;
		$log->device_id = Yii::app()->controller->device->device_id;
		$log->url = $url;
		$log->request = $url;
		$log->response = $result;
		$log->status = $http_status_json;
		$log->validate() && $log->save();
		
		return TRUE;
	}
	
	/**
	 * Add / Update profile
	 * 
	 * @param int $id Primary key of the record
	 * @param string $model Model name to search the record
	 */
	public static function addProfile($id)
	{
		return TRUE;
		
		$user = AvailRegistration::model()->findByPk($id);
		$dev_avail = DeviceAvailment::model()->findByPk($user->device_availment_id);
		$device = Device::model()->findByPk($dev_avail->device_id);
		$url = "http://api.correlor.com/scr/api/customers/19/users/{$device->correlor_id}/profile";
		
		$data = array('firstName'=>$user->firstname,
			'lastName'=>$user->lastname,
			'birthday'=> date('Ymd', strtotime($user->birthday)),
			'gender'=> ($user->gender == 1) ? 'male' : 'female');
		$data_string = CJSON::encode($data);
		
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Accept  application/json',
				'Content-Type: application/json; charset=UTF-8',
				'Content-Length: ' . strlen($data_string))
		);
		
		$result = curl_exec($ch);	
		$http_status_json = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		
		// Save logs
		$log = new CorrelorLog;
		$log->nas_id = Yii::app()->controller->nas->id;
		$log->site_id = Yii::app()->controller->site->site_id;
		$log->portal_id = Yii::app()->controller->portal->portal_id;
		$log->access_point_group_id = Yii::app()->controller->ap_group->access_point_group_id;
		$log->access_point_id = Yii::app()->controller->ap->access_point_id;
		$log->device_id = Yii::app()->controller->device->device_id;
		$log->url = $url;
		$log->request = $url;
		$log->response = $result;
		$log->status = $http_status_json;
		$log->validate() && $log->save();
		
		return TRUE;
	}
	
	public static function updateDevice($id)
	{
		return TRUE;
		
		$device = Device::model()->findByPk($id);
		$url = "http://api.correlor.com/scr/api/customers/19/users/{$device->correlor_id}/devices";
		
		$data = array('type'=>'',
			'manufacturer'=>'',
			'model'=>'',
			'os'=>'Android');
		$data_string = CJSON::encode($data);
		
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Accept  application/json',
				'Content-Type: application/json; charset=UTF-8',
				'Content-Length: ' . strlen($data_string))
		);

		$result = curl_exec($ch);	
		$http_status_json = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		
		// Save logs
		$log = new CorrelorLog;
		$log->nas_id = Yii::app()->controller->nas->id;
		$log->site_id = Yii::app()->controller->site->site_id;
		$log->portal_id = Yii::app()->controller->portal->portal_id;
		$log->access_point_group_id = Yii::app()->controller->ap_group->access_point_group_id;
		$log->access_point_id = Yii::app()->controller->ap->access_point_id;
		$log->device_id = Yii::app()->controller->device->device_id;
		$log->url = $url;
		$log->request = $url;
		$log->response = $result;
		$log->status = $http_status_json;
		$log->validate() && $log->save();
		
		return TRUE;
	}
	
	public static function reportAction($id, $action = 'access_point_login', $data = array())
	{
		return TRUE;
		
		$device = Device::model()->findByPk($id);
		if (!empty($device->correlor_id)) {
			$url = "http://api.correlor.com/scr/api/customers/19/users/{$device->correlor_id}/actions";
		
			if (empty($data)) {
				$data = array('actionType'=>$action,
					'itemId'=>1,
					'actiontime'=>date('YmdHis+0800'),
					'details'=>array(array('name'=>'max_session_time', 'value'=>30)));
			}
			$data_string = CJSON::encode($data);

			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Accept  application/json',
					'Content-Type: application/json; charset=UTF-8',
					'Content-Length: ' . strlen($data_string))
			);

			$result = curl_exec($ch);	
			$http_status_json = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			curl_close($ch);

			// Save logs
			$log = new CorrelorLog;
			$log->nas_id = Yii::app()->controller->nas->id;
			$log->site_id = Yii::app()->controller->site->site_id;
			$log->portal_id = Yii::app()->controller->portal->portal_id;
			$log->access_point_group_id = Yii::app()->controller->ap_group->access_point_group_id;
			$log->access_point_id = Yii::app()->controller->ap->access_point_id;
			$log->device_id = Yii::app()->controller->device->device_id;
			$log->url = $url;
			$log->request = $url;
			$log->response = $result;
			$log->status = $http_status_json;
			$log->validate() && $log->save();

			return TRUE;
		}
		
		return TRUE;
	}
	
	public static function addLocation($id)
	{
		return TRUE;
		
		$location = Location::model()->findByPk($id);
		$url = "http://api.correlor.com/scr/api/customers/19/locations";
		
		$data = array('locations' => array(
			array('id'=>$location->location_id,
				'name'=>$location->name,
				'type'=>$location->type,
				'parentId'=>$location->parent_location_id,
				'parentType'=>$location->parent_type,
				'latlon'=>$location->latlon,
				'address'=>$location->address)));
		
		$data_string = CJSON::encode($data);
		
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Accept  application/json',
				'Content-Type: application/json; charset=UTF-8',
				'Content-Length: ' . strlen($data_string))
		);

		$result = curl_exec($ch);	
		$http_status_json = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		
		// Save logs
		$log = new CorrelorLog;
		//$log->nas_id = Yii::app()->controller->nas->id;
		//$log->site_id = Yii::app()->controller->site->site_id;
		//$log->portal_id = Yii::app()->controller->portal->portal_id;
		//$log->access_point_group_id = Yii::app()->controller->ap_group->access_point_group_id;
		//$log->access_point_id = Yii::app()->controller->ap->access_point_id;
		//$log->device_id = Yii::app()->controller->device->device_id;
		$log->url = $url;
		$log->request = $url;
		$log->response = $result;
		$log->status = $http_status_json;
		$log->validate() && $log->save();
		
		return TRUE;
	}
}