<?php
/**
 * ES3 is a wrapper for the excellent S3.php class.
 * It contains basic function instances and other function can be directy called through call function.
 *
 * @uses CFile
 * @copyright Copyright &copy; Dinesh Saini(dineshsaini@zapbuild.com)
 */
require_once "S3.php";

class ES3 extends CApplicationComponent {

    private $_s3;
    public $aKey; 
    public $sKey;	
    public $bucket;
    public $lastError = "";
    public static $defaultBucket = 'defalut_bucket_name';

	/* Instance the S3 object */
    private function getInstance() {
        if ($this->_s3 === NULL)
            $this->connect();
        if ($this->bucket === NULL)
            $this->bucket = self::$defaultBucket;
        return $this->_s3;
    }

	/* Connect with the S3 Server */
    public function connect() {
        if ($this->aKey === NULL || $this->sKey === NULL)
            throw new CException('S3 Keys are not set.');

        $this->_s3 = new S3($this->aKey, $this->sKey);
    }

    /* Get list of all the buckets on S3 */
    public function buckets() {
        $s3 = $this->getInstance();
        return $this->_s3->listBuckets();
    }
	
	/* Get list of all objects in a bucket on S3 */
    public function viewBucket($bucket) {
        $s3 = $this->getInstance();
        return $this->_s3->getBucket($bucket);
    }
	
	/* Create a bucket on S3 */
    public function createBucket($bucket) {
        $s3 = $this->getInstance();
        return $this->_s3->putBucket($bucket);
    }
	
	/* Delete a bucket on S3 */
    public function deleteBucket($bucket) {
        $s3 = $this->getInstance();
        return $this->_s3->deleteBucket($bucket);
    }
	
	/* Delete an object in a bucket on S3 */
    public function deleteObject($bucket, $object) {
        $s3 = $this->getInstance();
        return $this->_s3->deleteObject($bucket, $object);
    }
    
   /* Copy a file on S3 */ 
   public function copyFile($sourceBucket,$fileUrifilename,$targetBucket,$fileUriNewname) {
        $s3 = $this->getInstance();
        return $this->_s3->copyObject($sourceBucket,$fileUrifilename, $targetBucket, $fileUriNewname);
    }

    /* Use to Passthru function for other functions */
    public function call($func) {
        $s3 = $this->getInstance();
        return $s3->$func();
    }
   
     /**
     * @param string $original File to upload - can be any valid CFile filename
     * @param string $uploaded Name of the file on destination -- can include directory separators
     */
    public function upload($original, $uploaded = "", $bucket = "") {
        $s3 = $this->getInstance();

        if ($bucket == "") {
            $bucket = $this->bucket;
        }
		
        if ($bucket === NULL || trim($bucket) == "") {
            throw new CException('Bucket param cannot be empty');
        }

        $file = Yii::app()->file->set($original);

        if (!$file->exists)
            throw new CException('Origin file not found');

        $fs1 = $file->size;

        if (!$fs1) {
            $this->lastError = "Attempted to upload empty file.";
            return false;
        }

        if (trim($uploaded) == "") {
            $uploaded = $original;
        }

        //if (!$s3->putObject($s3->inputResource(fopen($file->getRealPath(), 'r'), $fs1), $bucket, $uploaded, S3::ACL_PUBLIC_READ))
        $file->getRealPath();
        //if (!$s3->putObject($s3->inputResource( fopen($file->getRealPath(), 'rb'), $fs1), $bucket, $uploaded, S3::ACL_PUBLIC_READ))
        if (!$s3->putObjectFile($original, $bucket, $uploaded, S3::ACL_PRIVATE)) {
            $this->lastError = "Unable to upload file.";
            return false;
        }
        return true;
    } 

}
?>
