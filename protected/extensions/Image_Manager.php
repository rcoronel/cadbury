<?php
/**
|--------------------------------------------------------------------------
| Image Manager Library Class
|--------------------------------------------------------------------------
|
| Handles image manipulation
|
| @category		Extensions
| @author		Ryan Coronel <ryancoronel@gmail.com>
*/
class Image_Manager
{
	// ------------------------------------------------------------------------

	/**
	 * Resize, cut, and optimize image
	 *
	 * @access public
	 * @param resource $source_file Source file
	 * @param resource $destination_file New generated file
	 * @param int $width New width
	 * @param int $height New height
	 * @param string $file_type File type
	 * @param array $bgcolor RGB color
	 * @return bool
	 */
	public static function resize($source_file, $destination_file, $width = NULL, $height = NULL, $file_type = 'jpg', $bgcolor = array())
	{
		clearstatcache(TRUE, $source_file);

		// Check if file exists
		if ( ! file_exists($source_file) || !filesize($source_file)) {
			return FALSE;
		}
		
		// Get image size and check
		list($source_width, $source_height, $type) = getimagesize($source_file);
		if ( ! $source_width OR  ! $source_height) {
			return FALSE;
		}
			
		// if no new size is given, maintain old size
		if ( ! $width) {
			$width = $source_width;
		}
			
		if ( ! $height) {
			$height = $source_height;
		}
		
		// create image from source
		$source_image = Image_Manager::_create($type, $source_file);

		$width_diff = $width / $source_width;
		$height_diff = $height / $source_height;

		if ($width_diff > 1 && $height_diff > 1) {
			$next_width = $source_width;
			$next_height = $source_height;
		}
		else {
			if ($width_diff > $height_diff) {
				$next_height = $height;
				$next_width = round(($source_width * $next_height) / $source_height);
				$width = $width;
			}
			else {
				$next_width = $width;
				$next_height = round($source_height * $width / $source_width);
				$height = $height;
			}
		}
		
		$next_width = $next_width;
		$next_height = $next_height;

		$destination_image = imagecreatetruecolor($width, $height);

		// If image is a PNG and the output is PNG, fill with transparency. Else fill with white background.
		if ($file_type == 'png' && $type == IMAGETYPE_PNG) {
			imagealphablending($destination_image, false);
			imagesavealpha($destination_image, true);
			$transparent = imagecolorallocatealpha($destination_image, 255, 255, 255, 127);
			imagefilledrectangle($destination_image, 0, 0, $width, $height, $transparent);
		}
		else {
			if ( ! empty($bgcolor) AND $bgcolor[0] != 'null') {
				$color = imagecolorallocate($destination_image, $bgcolor[0], $bgcolor[1], $bgcolor[2]);
			}
			else {
				$color = imagecolorallocate($destination_image, 0, 0, 0);
			}
				
			imagefilledrectangle ($destination_image, 0, 0, $width, $height, $color);
		}

		imagecopyresampled($destination_image, $source_image, (int)(($width - $next_width) / 2), (int)(($height - $next_height) / 2), 0, 0, $next_width, $next_height, $source_width, $source_height);
		return (Image_Manager::_write($file_type, $destination_image, $destination_file));
	}

	/**
	 * Create an image with GD extension from a given type
	 *
	 * @access private
	 * @param string $type
	 * @param string $filename
	 * @return resource
	 */
	private static function _create($type, $filename)
	{
		switch ($type)
		{
			case IMAGETYPE_GIF :
				return imagecreatefromgif($filename);
			break;

			case IMAGETYPE_PNG :
				return imagecreatefrompng($filename);
			break;

			case IMAGETYPE_JPEG :
			default:
				return imagecreatefromjpeg($filename);
			break;
		}
	}

	/**
	 * Generate and write image
	 *
	 * @access private
	 * @param string $type
	 * @param resource $resource
	 * @param string $filename
	 * @return bool
	 */
	private static function _write($type, $resource, $filename)
	{
		switch ($type)
		{
			case 'gif':
				$success = imagegif($resource, $filename);
			break;

			case 'png':
				$quality = 0;
				$success = imagepng($resource, $filename, (int)$quality);
			break;

			case 'jpg':
			case 'jpeg':
			default:
				$quality = 100;
				$success = imagejpeg($resource, $filename, (int)$quality);
			break;
		}
		imagedestroy($resource);
		@chmod($filename, 0777);
		return $success;
	}
}
