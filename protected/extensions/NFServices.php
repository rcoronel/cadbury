<?php
/*
|--------------------------------------------------------------------------
| NF Services API Class
|--------------------------------------------------------------------------
|
| Handles the API calls for NF Services
|
| @category		Extensions
| @author		Bien Jerico Cueto
*/
class NFServices
{
    
    public static function result_to_json($data)
    {
         
        $data_array = preg_split('/\r\n|[\r\n]/',$data);
       
        $array = array();
        foreach($data_array as $rs)
        {
            $list = explode(":", trim($rs));
            $array[$list[0]] = ltrim(preg_replace('/\s+/', ' ', $list[1]));
        }

        $result = json_encode($array);
        
        return $result;
    }
    
    public static function get_primo_vn()
    {
           
        $url = "http://52.74.216.242/cadbury/nfservices/get_primo_vn.php?Operation=37&Origin=6001&Silent=1";

        // create a new cURL resource
        $ch = curl_init();

        // set URL and other appropriate options
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,TRUE);

        // grab URL and pass it to the browser
        $curl_result = curl_exec($ch);
        
        $result = self::result_to_json($curl_result);

        // close cURL resource, and free up system resources
        curl_close($ch);
        
        return $result;
    }
    
    public static function airtime_balance_inquiry($sub_mobtel)
    {

        $url = "http://52.74.216.242/cadbury/nfservices/airtime_balance_inquiry.php?Operation=38&Origin=6001&Silent=1&SUB_Mobtel=$sub_mobtel";

        // create a new cURL resource
        $ch = curl_init();

        // set URL and other appropriate options
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,TRUE);

        // grab URL and pass it to the browser
        $curl_result = curl_exec($ch);
        
        $result = SELF::result_to_json($curl_result);

        // close cURL resource, and free up system resources
        curl_close($ch);
        
        return $result;
    }
    
    public static function get_subscriber_brand($sub_mobtel)
    {
        $sub_mobtel = $data['sub_mobtel'];
        
        $url = "http://52.74.216.242/cadbury/nfservices/get_subscriber_brand.php?Operation=34&Origin=6001&Silent=1&SUB_Mobtel=$sub_mobtel";

        // create a new cURL resource
        $ch = curl_init();

        // set URL and other appropriate options
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,TRUE);

        // grab URL and pass it to the browser
        $curl_result = curl_exec($ch);
        
        $result = SELF::result_to_json($curl_result);

        // close cURL resource, and free up system resources
        curl_close($ch);
        
        return $result;
    }
    
    
    public static function provision_service($data = array())
    {
        
        $serviceid  = $data['serviceid'];
        $param      = $data['param'];
        $sub_mobtel         = $data['sub_mobtel'];
        $returnexpirydate   = $data['returnexpirydate'];
        $returnvn           = $data['returnvn'];
        
       $url = "http://52.74.216.242/cadbury/nfservices/provision_service.php?Operation=1&Origin=6001&Silent=1&ServiceID=$serviceid&Param=$param&SUB_Mobtel=$sub_mobtel&ReturnExpiryDate=$returnexpirydate&ReturnVN=$returnvn";

        // create a new cURL resource
        $ch = curl_init();

        // set URL and other appropriate options
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,TRUE);

        // grab URL and pass it to the browser
        $curl_result = curl_exec($ch);
        $result = SELF::result_to_json($curl_result);

        // close cURL resource, and free up system resources
        curl_close($ch);
        
        return $result;
    }
    
    public static function sms_forward($data = array())
    {
        $sub_mobtel     = $data['sub_mobtel'];
        $sms_msgtxt     = $data['sms_msgtxt'];
        $sms_sourceaddr = $data['sms_sourceaddr'];
        
       $url = "http://52.74.216.242/cadbury/nfservices/sms_forward.php?Operation=36&Origin=6001&Silent=1&SUB_Mobtel=$sub_mobtel&SMS_MsgTxt=$sms_msgtxt&SMS_SourceAddr=$sms_sourceaddr";

        // create a new cURL resource
        $ch = curl_init();

        // set URL and other appropriate options
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,TRUE);

        // grab URL and pass it to the browser
        $curl_result = curl_exec($ch);
        $result = SELF::result_to_json($curl_result);

        // close cURL resource, and free up system resources
        curl_close($ch);
        
        return $result;
    }
    
    
}

