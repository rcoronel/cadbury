<?php
/*
|--------------------------------------------------------------------------
| Mobile360 SMS Broadcaster
|--------------------------------------------------------------------------
|
| Handles the sending of SMS via Adspark platform
|
| @category		Extensions
| @author		Ryan Coronel
*/
class Mobile360
{
	public static $username = 'gwifi';
	public static $password = '57E0EC';
	public static $sender_id = 'GoWiFi';
	
	/**
	 * Send SMS based on parameters
	 * 
	 * @access public
	 * @param int $msisdn
	 * @param string $message
	 * @return boolean
	 */
	public static function send($msisdn, $message)
	{
		$data = array('username'=>self::$username,
			'password'=>self::$password,
			'msisdn'=>$msisdn,
			'content'=>$message,
			'shortcode_mask'=>self::$sender_id);                                                                    
		$data_string = json_encode($data);                                                                                   

		$ch = curl_init('http://api.mobile360.ph/public/rest/v1/broadcast');                                                                      
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . strlen($data_string))                                                                       
		);                                                                                                                   

		$result = json_decode(curl_exec($ch));
		
		$log = new SmsLog();
		if ($result->code != 201) {
			$log->trans_id = "$result->code | $result->name";
			$log->msisdn = (int)$msisdn;
			$log->source = 'n/a';
			$log->status = 0;
		}
		else {
			$log->trans_id = $result->transid;
			$log->msisdn = (int)$msisdn;
			$log->source = self::$username;
			$log->status = 1;
		}
		
		$log->validate() && $log->save();
		if ($log->status) {
			return TRUE;
		}
		return FALSE;
	}
	
	/**
	 * Set the URL of the API
	 * 
	 * @access private
	 * @param int $msisdn
	 * @param string $message
	 * @return string
	 */
	private static function _set_url($msisdn, $message)
	{
		$url = self::PROTOCOL.'://';
		$url .= self::HOST.'/';
		$url .= '?account='.self::ACCOUNT;
		$url .= '&accountkey='.self::KEY;
		$url .= '&msisdn='.$msisdn;
		$url .= '&message='.$message;
		$url .= '&source='.self::SOURCE;
		$url .= '&rcvd_transid='.self::TRANS_ID;
		
		return $url;
	}
}