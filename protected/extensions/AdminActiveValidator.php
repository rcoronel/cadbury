<?php
/**
 * AdminActiveValidator class file.
 *
 * @author Ryan Coronel <ryancoronel@gmail.com>
 * @link http://www.yiiframework.com/
 * @copyright 2008-2013 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

/**
 * AdminRoleValidator validates if there is still active Super User
 *
 * This is to ensure that there may be at least 1 active Super User
 */
class AdminActiveValidator extends CValidator
{
	/**
	 * Validates the attribute of the object.
	 * If there is any error, the error message is added to the object.
	 * @param CModel $object the object being validated
	 * @param string $attribute the attribute being validated
	 */
	protected function validateAttribute($object,$attribute)
	{
		$value=$object->$attribute;
		if ((int)$value != 1) {
			$count = $object->count("{$attribute} = 1 AND Is_active = 1 AND Admin_ID != {$object->Admin_ID}");
		
			if ( ! $count) {
				$this->addError($object,$attribute,Yii::t('yii', 'Cannot change status, no other user can login.'));
			}
		}
		else {
			return ;
		}
	}
}
