<?php
/**
 * AdminRoleValidator class file.
 *
 * @author Ryan Coronel <ryancoronel@gmail.com>
 * @link http://www.yiiframework.com/
 * @copyright 2008-2013 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

/**
 * AdminRoleValidator validates if there is still existing Super Admin user before creating/updating user
 *
 * This is to ensure that there may be at least 1 Super Admin user
 */
class AdminRoleValidator extends CValidator
{
	/**
	 * Validates the attribute of the object.
	 * If there is any error, the error message is added to the object.
	 * @param CModel $object the object being validated
	 * @param string $attribute the attribute being validated
	 */
	protected function validateAttribute($object,$attribute)
	{
		$value=$object->$attribute;
		
		if ((int)$value != 1) {
			$count = $object->count("{$attribute} = 1 AND Admin_ID != {$object->Admin_ID}");
			
			if ( ! $count) {
				$this->addError($object,$attribute,Yii::t('yii', 'Cannot change role, no other super user found.'));
			}
		}
		else {
			return ;
		}
	}
}
