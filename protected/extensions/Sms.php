<?php
/*
|--------------------------------------------------------------------------
| SMS Sender library
|--------------------------------------------------------------------------
|
| Handles the sending of SMS via Adspark platform
|
| @category		Extensions
| @author		Ryan Coronel
*/
class Sms
{
//	const PROTOCOL = 'http';
//	const HOST = '162.209.21.247/broadcaster';
//	#const HOST = '10.208.134.29/broadcaster';
//	const ACCOUNT = 'EggNPD';
//	const KEY = 'nFztud';
//	const SOURCE = 'FreeWifi';
//	const TRANS_ID = 12345;
	
	public $protocol = 'http';
	public $host = '162.209.21.247/broadcaster';
	public $account;
	public $key;
	public $source;
	public $trans_id = '12345';
	
	/**
	 * Send SMS based on parameters
	 * 
	 * @access public
	 * @param int $msisdn
	 * @param string $message
	 * @return boolean
	 */
	public function send($msisdn, $message)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, self::_setUrl($msisdn, $message));
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($ch);
		curl_close($ch);
		
		$string = trim($output);
		$string_array = explode(',', $string);
		$result_string = array();
		foreach ($string_array as $value) {
			$explode = explode(':', $value);
			$result_string[$explode[0]] = $explode[1];
		}
		
		$log = new SmsLog;
		if (isset($result_string['Invalid'])) {
			$log->trans_id = 'n/a';
			$log->msisdn = (int)$msisdn;
			$log->source = 'n/a';
			$log->status = 0;
		}
		else {
			$log->trans_id = $result_string['transid'];
			$log->msisdn = (int)$msisdn;
			$log->source = $result_string['source'];
			$log->status = ($result_string['receipt'] == 'Successful') ? 1 : 0;
		}
		$log->validate() && $log->save();
		if ($log->status) {
			return TRUE;
		}
		return FALSE;
	}
	
	public function setVars($vars = array())
	{
		foreach ($vars as $index=>$value) {
			$this->{$index} = $value;
		}
	}
	
	/**
	 * Set the URL of the API
	 * 
	 * @access private
	 * @param int $msisdn
	 * @param string $message
	 * @return string
	 */
	private function _setUrl($msisdn, $message)
	{
		$url = $this->protocol.'://';
		$url .= $this->host.'/';
		$url .= '?account='.$this->account;
		$url .= '&accountkey='.$this->key;
		$url .= '&msisdn='.$msisdn;
		$url .= '&message='.$message;
		$url .= '&source='.$this->source;
		$url .= '&rcvd_transid='.$this->trans_id;
		
		return $url;
	}
}