<?php

/**
 * This is the model class for table "admin_menu".
 *
 * The followings are the available columns in table 'admin_menu':
 * @property integer $admin_menu_id
 * @property integer $parent_admin_menu_id
 * @property string $title
 * @property string $classname
 * @property string $img
 * @property integer $display
 * @property integer $position
 * @property string $created_at
 * @property string $updated_at
 */
class AdminMenu extends ActiveRecord
{
	// int Has Subpages
	public $has_subpages;
	
	public $view;
	public $add;
	public $edit;
	public $delete;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'admin_menu';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('parent_admin_menu_id, title, classname, display, created_at, updated_at', 'required'),
			array('admin_menu_id, parent_admin_menu_id, display, position, view, add, edit, delete', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>32),
			array('classname', 'length', 'max'=>16),
			array('img', 'length', 'max'=>128),
			array('created_at, updated_at', 'date', 'format'=>'yyyy-mm-dd hh:mm:ss'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'permission'=>array(self::HAS_ONE, 'AdminRolePermission', array('admin_menu_id'=>'admin_menu_id'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'admin_menu_id'=>'ID',
			'parent_admin_menu_id'=>'Parent Menu',
			'title'=>'Title',
			'classname'=>'Classname',
			'img'=>'Image',
			'display'=>'Display',
			'position'=>'Position',
			'created_at'=>'Date Added',
			'updated_at'=>'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('admin_menu_id',$this->admin_menu_id);
		$criteria->compare('parent_admin_menu_id',$this->parent_admin_menu_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('classname',$this->classname,true);
		$criteria->compare('img',$this->img,true);
		$criteria->compare('display',$this->display);
		$criteria->compare('position',$this->position);
		$criteria->order = 'position ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AdminMenu the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public static function getMainMenus()
	{
		$context = Context::getContext();
		
		$pages = AdminMenu::model()->with('permission')->findAll(
					array(	'order'=>'position', 'condition'=>'admin_role_id = :role_id AND view = 1 AND display = 1 AND parent_admin_menu_id = 0',
							'params'=>array(':role_id'=>$context->admin->admin_role_id)));
		return $pages;
	}
	
	public static function getSubMenus()
	{
		$context = Context::getContext();
		$pages = AdminMenu::model()->with('permission')->findAll(
					array(	'order'=>'position', 'condition'=>'admin_role_id = :role_id AND view = 1 AND display = 1 AND parent_admin_menu_id != 0',
							'params'=>array(':role_id'=>$context->admin->admin_role_id)));
		return $pages;
	}
}
