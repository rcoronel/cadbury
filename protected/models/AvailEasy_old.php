<?php

/**
 * This is the model class for table "avail_easy".
 *
 * The followings are the available columns in table 'avail_easy':
 * @property integer $Avail_easy_ID
 * @property integer $Device_ID
 * @property string $Created_at
 */
class AvailEasy_old extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'avail_easy';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Device_ID, Created_at', 'required'),
			array('Device_ID', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Avail_easy_ID, Device_ID, Created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Avail_easy_ID' => 'ID',
			'Device_ID' => 'Device',
			'Created_at' => 'Date Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Avail_easy_ID',$this->Avail_easy_ID);
		$criteria->compare('Device_ID',$this->Device_ID);
		$criteria->compare('Created_at',$this->Created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AvailEasy the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Initialize new database connection
	 * 
	 * @return type
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cp_old;
	}
	
	public static function poll($from, $to, $nas_id = 0)
	{
		$where = "DATE(ae.Created_at) BETWEEN '{$from}' AND '{$to}'";
		if ($nas_id) {
			$where .= " AND Nas_ID = {$nas_id}";
		}
		$count = Yii::app()->db_cp->createCommand()
			->select('COUNT(DISTINCT d.Mac_address, ae.Nas_ID) as count')
			->from('avail_easy ae')
			->join('device d', 'd.Device_ID = ae.Device_ID')
			->where($where)
			->queryRow();
		
		return (int)$count['count'];
	}
}
