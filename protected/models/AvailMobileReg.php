<?php

/**
 * This is the model class for table "avail_mobile_reg".
 *
 * The followings are the available columns in table 'avail_mobile_reg':
 * @property integer $avail_mobile_reg_id
 * @property integer $device_availment_id
 * @property string $msisdn
 * @property string $auth_code
 * @property integer $is_validated
 * @property integer $auth_limit
 * @property string $firstname
 * @property string $lastname
 * @property string $birthday
 * @property string $email
 * @property integer $city_id
 * @property string $created_at
 * @property string $updated_at
 */
class AvailMobileReg extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'avail_mobile_reg';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('msisdn', 'required'),
			array('msisdn', 'numerical', 'integerOnly'=>true, 'min'=>0),
			array('avail_mobile_reg_id, device_availment_id, is_validated, auth_limit, city_id', 'numerical', 'integerOnly'=>true),
			array('msisdn', 'length', 'max'=>11, 'min'=>11),
			array('auth_code', 'length', 'max'=>5),
			
			// REGISTER scenario
			array('firstname, lastname, birthday, email, city_id', 'required', 'on'=>'register'),
			array('birthday', 'date', 'format'=>'yyyy-MM-dd'),
			
			array('created_at, updated_at', 'date', 'format'=>'yyyy-MM-dd hh:mm:ss'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'dev_avail'=>array(self::BELONGS_TO, 'DeviceAvailment', array('device_availment_id'=>'device_availment_id'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'avail_mobile_reg_id' => 'ID',
			'device_availment_id' => 'Device Availment',
			'msisdn' => 'MSISDN',
			'auth_code' => 'Code',
			'is_validated' => 'Is Validated',
			'auth_limit' => 'Limit',
			'firstname' => 'Subscriber’s First Name',
			'lastname' => 'Subscriber’s Last Name',
			'birthday' => 'Subscriber’s Birthday',
			'email' => 'Subscriber’s Email Address',
			'city_id' => 'Subscriber’s City of Residence',
			'created_at' => 'Date Added',
			'updated_at' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('avail_mobile_reg_id',$this->avail_mobile_reg_id);
		$criteria->compare('device_availment_id',$this->device_availment_id);
		$criteria->compare('msisdn',$this->msisdn,true);
		$criteria->compare('auth_code',$this->auth_code,true);
		$criteria->compare('is_validated',$this->is_validated);
		$criteria->compare('auth_limit',$this->auth_limit);
		$criteria->compare('firstname',$this->firstname,true);
		$criteria->compare('lastname',$this->lastname,true);
		$criteria->compare('birthday',$this->birthday,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('city_id',$this->city_id);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cp;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AvailMobileReg the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
