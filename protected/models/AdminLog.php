<?php

/**
 * This is the model class for table "admin_log".
 *
 * The followings are the available columns in table 'admin_log':
 * @property integer $admin_log_id
 * @property integer $admin_id
 * @property integer $admin_role_id
 * @property string $classname
 * @property string $action
 * @property integer $reference
 * @property string $created_at
 */
class AdminLog extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'admin_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('admin_id, admin_role_id, classname, action, reference, created_at', 'required'),
			array('admin_id, admin_role_id, reference', 'numerical', 'integerOnly'=>true),
			array('classname', 'length', 'max'=>32),
			array('action', 'length', 'max'=>16),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('admin_log_id, admin_id, admin_role_id, classname, action, reference, created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'admin_log_id' => 'ID',
			'admin_id' => 'Admin',
			'admin_role_id' => 'Role',
			'classname' => 'Classname',
			'action' => 'Action',
			'reference' => 'Reference',
			'created_at' => 'Date Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('admin_log_id',$this->admin_log_id);
		$criteria->compare('admin_id',$this->admin_id);
		$criteria->compare('admin_role_id',$this->admin_role_id);
		$criteria->compare('classname',$this->classname,true);
		$criteria->compare('action',$this->action,true);
		$criteria->compare('reference',$this->reference);
		$criteria->compare('created_at',$this->created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AdminLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
