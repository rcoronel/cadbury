<?php

/**
 * This is the model class for table "survey_answer".
 *
 * The followings are the available columns in table 'survey_answer':
 * @property integer $survey_answer_id
 * @property integer $survey_question_id
 * @property string $answer
 * @property string $answer_type
 * @property integer $position
 * @property string $created_at
 * @property string $updated_at
 */
class SurveyAnswer extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'survey_answer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('survey_question_id, answer, answer_type, created_at, updated_at', 'required'),
			array('survey_answer_id, survey_question_id, position', 'numerical', 'integerOnly'=>true),
			array('answer', 'length', 'max'=>64),
			array('answer_type', 'length', 'max'=>16),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'question'=>array(self::BELONGS_TO, 'SurveyQuestion', array('survey_question_id'=>'survey_question_id'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'survey_answer_id' => 'ID',
			'survey_question_id' => 'Question',
			'answer' => 'Answer',
			'answer_type' => 'Type',
			'position' => 'Position',
			'created_at' => 'Date Added',
			'updated_at' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('survey_answer_id',$this->survey_answer_id);
		$criteria->compare('survey_question_id',$this->survey_question_id);
		$criteria->compare('answer',$this->answer,true);
		$criteria->compare('answer_type',$this->answer_type,true);
		$criteria->compare('position',$this->position);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->order = 'position ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cms;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SurveyAnswer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public static function findMaxPosition($question_id)
	{
		$model = new SurveyAnswer;
		$criteria = new CDbCriteria;
		$criteria->select='max(position) AS position';
		$criteria->compare('survey_question_id',$question_id);
		$row = $model->model()->find($criteria);
		return (int)$position = $row['position'];
	}
}
