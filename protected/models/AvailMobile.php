<?php

/**
 * This is the model class for table "avail_mobile".
 *
 * The followings are the available columns in table 'avail_mobile':
 * @property integer $avail_mobile_id
 * @property integer $msisdn
 * @property string $auth_code
 * @property integer $is_validated
 * @property integer $auth_limit
 * @property string $created_at
 * @property string $updated_at
 */
class AvailMobile extends ActiveRecord
{
	public $export_fields = array('msisdn');
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'avail_mobile';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('msisdn', 'required'),
			array('msisdn', 'numerical', 'integerOnly'=>true, 'min'=>0),
			array('avail_mobile_id, msisdn, is_validated, auth_limit', 'numerical', 'integerOnly'=>true),
			array('msisdn', 'length', 'max'=>11, 'min'=>11, 'message'=>'{attribute} must be 11 digits'),
			array('auth_code', 'length', 'max'=>5),
			array('created_at, updated_at', 'date', 'format'=>'yyyy-MM-dd hh:mm:ss'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'dev_avail'=>array(self::BELONGS_TO, 'DeviceAvailment', array('device_availment_id'=>'device_availment_id'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'avail_mobile_id'=>'Avail Mobile',
			'msisdn'=>'Mobile No.',
			'auth_code'=>'Auth Code',
			'is_validated'=>'Is Validated',
			'auth_limit'=>'Auth Limit',
			'created_at'=>'Created At',
			'updated_at'=>'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('avail_mobile_id',$this->avail_mobile_id);
		$criteria->compare('auth_code',$this->auth_code,true);
		$criteria->compare('is_validated',$this->is_validated);
		$criteria->compare('auth_limit',$this->auth_limit);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cp;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AvailMobile the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
