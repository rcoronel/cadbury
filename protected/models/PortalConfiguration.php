<?php

/**
 * This is the model class for table "portal_configuration".
 *
 * The followings are the available columns in table 'portal_configuration':
 * @property integer $portal_configuration_id
 * @property integer $portal_id
 * @property string $config
 * @property string $value
 * @property string $created_at
 */
class PortalConfiguration extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'portal_configuration';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('portal_id, config, value, created_at', 'required'),
			array('portal_id', 'numerical', 'integerOnly'=>true),
			array('config', 'length', 'max'=>128),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('portal_configuration_id, portal_id, config, value, created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'portal_configuration_id' => 'ID',
			'portal_id' => 'NAS',
			'config' => 'Config',
			'value' => 'Value',
			'created_at' => 'Date Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('portal_configuration_id',$this->portal_configuration_id);
		$criteria->compare('portal_id',$this->portal_id);
		$criteria->compare('config',$this->config,true);
		$criteria->compare('value',$this->value,true);
		$criteria->compare('created_at',$this->created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cms;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PortalConfiguration the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Get Configuration Value
	 * 
	 * @access	public
	 * @param string $config Configuration Value to be search
	 * @param mixed $default Default Value to be set
	 * @return string
	 */
	public static function getValue($config = '', $default = '')
	{
		$portal_id = 0;
		$context = Yii::app()->controller->context;
		if ($context->portal) {
			$portal_id = $context->portal->portal_id;
		}
		else {
			$portal_id = Yii::app()->controller->portal->portal_id;
		}
		$conf = PortalConfiguration::model()->findByAttributes(array('config' => $config, 'portal_id'=>$portal_id));
		
		if (empty($conf)) {
			return $default;
		}
		return $conf->value;
	}
	
	/**
	 * Set Configuration Value
	 * 
	 * @access	public
	 * @param string $config Configuration Value
	 * @param mixed $value
	 * @return string
	 */
	public static function setValue($config, $value = '')
	{
		$context = Context::getContext();
		
		if ($context->portal) {
			$portal_id = $context->portal->portal_id;
		}
		else {
			$portal_id = Yii::app()->controller->portal->portal_id;
		}
		
		// Delete previous Config
		PortalConfiguration::model()->deleteAllByAttributes(array('portal_id'=>$portal_id, 'config'=>$config));
		
		// Insert
		$object = new PortalConfiguration;
		$object->portal_id = $portal_id;
		$object->config = $config;
		$object->value = $value;
		$object->validate() && $object->save();
		
		return TRUE;
	}
}
