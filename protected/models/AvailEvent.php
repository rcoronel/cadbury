<?php

/**
 * This is the model class for table "avail_event".
 *
 * The followings are the available columns in table 'avail_event':
 * @property string $avail_event_id
 * @property string $avail_mobile_id
 * @property string $event_id
 * @property string $fullname
 * @property string $email
 */
class AvailEvent extends ActiveRecord
{
	public $msisdn;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'avail_event';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('event_id, fullname, email, msisdn', 'required'),
			array('avail_mobile_id, event_id', 'length', 'max'=>11),
			array('email', 'email'),
			array('msisdn', 'numerical', 'integerOnly'=>true, 'min'=>0),
			array('msisdn', 'length', 'max'=>11, 'min'=>11, 'tooShort'=>'{attribute} must be 11 digits'),
			array('fullname, email', 'length', 'max'=>50),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'avail_event_id' => 'ID',
			'avail_mobile_id' => 'Avail Mobile',
			'event_id' => 'Event',
			'fullname' => 'Full name',
			'email' => 'E-mail',
			'msisdn'=> 'Mobile No.'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('avail_event_id',$this->avail_event_id,true);
		$criteria->compare('avail_mobile_id',$this->avail_mobile_id,true);
		$criteria->compare('event_id',$this->event_id,true);
		$criteria->compare('fullname',$this->fullname,true);
		$criteria->compare('email',$this->email,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cp;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AvailEvent the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
