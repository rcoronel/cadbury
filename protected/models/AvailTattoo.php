<?php

/**
 * This is the model class for table "avail_tattoo".
 *
 * The followings are the available columns in table 'avail_tattoo':
 * @property integer $avail_tattoo_id
 * @property string $fullname
 * @property string $account_no
 * @property integer $plan
 * @property string $created_at
 * @property string $updated_at
 */
class AvailTattoo extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'avail_tattoo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('fullname, account_no, plan', 'required'),
			array('avail_tattoo_id, plan', 'numerical', 'integerOnly'=>true),
			array('fullname', 'length', 'max'=>64),
			array('account_no', 'length', 'max'=>32),
			array('created_at, updated_at', 'date', 'format'=>'yyyy-MM-dd hh:mm:ss'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'dev_avail'=>array(self::BELONGS_TO, 'DeviceAvailment', array('device_availment_id'=>'device_availment_id'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'avail_tattoo_id'=>'ID',
			'fullname'=>'Fullname',
			'account_no'=>'Account No',
			'plan'=>'Plan',
			'created_at'=>'Created At',
			'updated_at'=>'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('avail_tattoo_id',$this->avail_tattoo_id);
		$criteria->compare('fullname',$this->fullname,true);
		$criteria->compare('account_no',$this->account_no,true);
		$criteria->compare('plan',$this->plan);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cp;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AvailTattoo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
