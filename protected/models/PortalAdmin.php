<?php

/**
 * This is the model class for table "portal_admin".
 *
 * The followings are the available columns in table 'portal_admin':
 * @property integer $portal_admin_id
 * @property integer $portal_id
 * @property integer $portal_role_id
 * @property string $email
 * @property string $password
 * @property string $firstname
 * @property string $lastname
 * @property integer $is_active
 * @property string $created_at
 * @property string $updated_at
 */
class PortalAdmin extends ActiveRecord
{
	// string Confirm Password
	public $confirm_password;
	
	// bool Cookie login
	public $cookie_login;
	
	// object Identity
	private $_identity;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'portal_admin';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			// INSERT scenario
			array('portal_id, portal_role_id, email, password, firstname, lastname, is_active, created_at, updated_at', 'required', 'on'=>'insert'),
			array('email', 'unique', 'on'=>'insert'),
			array('confirm_password', 'compare', 'on' => 'insert', 'compareAttribute' => 'password'),
			
			// LOGIN scenario
			array('email, password', 'required', 'on'=>'login'),
			
			array('portal_admin_id, portal_id, portal_role_id, is_active', 'numerical', 'integerOnly'=>true),
			array('email, password', 'length', 'max'=>256),
			array('firstname, lastname', 'length', 'max'=>64),
			array('created_at, updated_at', 'date', 'format'=>'yyyy-MM-dd hh:mm:ss'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'portal'=>array(self::BELONGS_TO, 'Portal', array('portal_id'=>'portal_id')),
			'site'=>array(self::BELONGS_TO, 'Site', array('site_id'=>'site_id'), 'through'=>'portal')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'portal_admin_id' => 'ID',
			'portal_id' => 'Portal',
			'portal_role_id' => 'Role',
			'email' => 'E-mail Address',
			'password' => 'Password',
			'firstname' => 'First name',
			'lastname' => 'Last name',
			'is_active' => 'Status',
			'created_at' => 'Date added',
			'updated_at' => 'Date updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('portal_admin_id',$this->portal_admin_id);
		$criteria->compare('portal_id',$this->portal_id);
		$criteria->compare('portal_role_id',$this->portal_role_id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('firstname',$this->firstname,true);
		$criteria->compare('lastname',$this->lastname,true);
		$criteria->compare('is_active',$this->is_active);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cms;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PortalAdmin the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Login the admin
	 * 
	 * @access	public
	 * @return void
	 */
	public function login()
	{
		// No identity set
		if ($this->_identity === NULL) {
			$this->_identity = new PortalAdminIdentity($this->email, $this->password);
		}
		
		// Authenticate identity
		$auth = $this->_identity->authenticate();
		
               
		if ( ! $auth)
		{
			switch ($this->_identity->errorCode)
			{
				case 1:
					$this->addError('email', $this->_identity->errorMessage);
				break;
			
				case 2:
					$this->addError('password', $this->_identity->errorMessage);
				break;
                            
                case 3:
					$this->addError('inactive', $this->_identity->errorMessage);
				break;
			}
			return FALSE;
		}
		else
		{
			$duration = 0;
			if ($this->cookie_login) {
				$duration = 3600*24*30; #30 days
			}
			
			Yii::app()->getModule('gateway')->admin->login($this->_identity, $duration);
			
			// Context object
			$context = Context::getContext();
			$context->site_admin = PortalAdmin::model()->findByPk($this->_identity->getId());
			
			return true;
		}
	}
	
	/**
	 * Change Password
	 * 
	 * @param string $pw Current hashed Password
	 * @return bool
	 */
	public function change_Password($pw = '')
	{
		// check if correct current Password
		if ( ! CPasswordHelper::verifyPassword($this->Password, $pw))
		{
			$this->addError('current_Password', 'Supplied current Password is incorrect');
			return FALSE;
		}
		
		// check for Password
		if (CPasswordHelper::verifyPassword($this->New_password, $pw))
		{
			$this->addError('Password', 'New Password cannot be the same as the current Password');
			return FALSE;
		}
		
		return TRUE;
	}
	
	public function getName()
	{
		return "{$this->firstname} {$this->lastname}";
	}
}
