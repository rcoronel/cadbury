<?php

/**
 * This is the model class for table "avail_registration".
 *
 * The followings are the available columns in table 'avail_registration':
 * @property integer $avail_registration_id
 * @property integer $device_availment_id
 * @property string $msisdn
 * @property string $firstname
 * @property string $lastname
 * @property string $birthday
 * @property integer $age
 * @property integer $gender_id
 * @property string $email
 * @property string $cable_operator
 * @property string $created_at
 * @property string $updated_at
 */
class AvailRegistration extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'avail_registration';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('msisdn, firstname, lastname, age, gender_id, email', 'required', 'except'=>'naia'),
			array('device_availment_id, age, gender_id', 'numerical', 'integerOnly'=>true),
			array('msisdn', 'length', 'max'=>11, 'min'=>11),
			array('firstname, lastname, cable_operator', 'length', 'max'=>50),
			array('email', 'length', 'max'=>128),
			array('email', 'email'),
			// Specific scenarios
			array('email', 'required', 'on'=>'naia'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('avail_registration_id, device_availment_id, msisdn, firstname, lastname, birthday, age, gender_id, email, cable_operator, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'dev_avail'=>array(self::BELONGS_TO, 'DeviceAvailment', array('device_availment_id'=>'device_availment_id'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'avail_registration_id' => 'ID',
			'device_availment_id' => 'Device Availment',
			'msisdn' => 'Mobile No.',
			'firstname' => 'First name',
			'lastname' => 'Last name',
			'birthday' => 'Birthday',
			'age' => 'Age',
			'gender_id' => 'Gender',
			'email' => 'E-mail Address',
			'cable_operator' => 'Cable Operator',
			'created_at' => 'Date Added',
			'updated_at' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('avail_registration_id',$this->avail_registration_id);
		$criteria->compare('device_availment_id',$this->device_availment_id);
		$criteria->compare('msisdn',$this->msisdn,true);
		$criteria->compare('firstname',$this->firstname,true);
		$criteria->compare('lastname',$this->lastname,true);
		$criteria->compare('birthday',$this->birthday,true);
		$criteria->compare('age',$this->age);
		$criteria->compare('gender_id',$this->gender_id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('cable_operator',$this->cable_operator,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cp;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AvailRegistration the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
