<?php

/**
 * This is the model class for table "availment".
 *
 * The followings are the available columns in table 'availment':
 * @property integer $availment_id
 * @property string $name
 * @property string $code
 * @property string $description
 * @property integer $is_active
 * @property string $created_at
 * @property string $updated_at
 * @property string $model
 */
class Availment extends ActiveRecord
{
	public $is_available;
	public $duration;
	public $level;
	public $group;
	public $is_recurring = 1;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'availment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, code, description, is_active, created_at, updated_at, model', 'required'),
			array('is_active', 'numerical', 'integerOnly'=>true),
			array('name, model', 'length', 'max'=>32),
			array('code', 'length', 'max'=>64),
			array('description', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('availment_id, name, code, description, is_active, created_at, updated_at, model', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'availment_id' => 'ID',
			'name' => 'Name',
			'code' => 'Code',
			'description' => 'Description',
			'is_active' => 'Status',
			'created_at' => 'Date Added',
			'updated_at' => 'Date Updated',
			'model' => 'Model Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('availment_id',$this->availment_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('is_active',$this->is_active);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('model',$this->model,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cms;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Availment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
