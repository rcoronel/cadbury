<?php

/**
 * This is the model class for table "location".
 *
 * The followings are the available columns in table 'location':
 * @property integer $location_id
 * @property integer $portal_id
 * @property string $name
 * @property string $type
 * @property integer $parent_location_id
 * @property string $parent_type
 * @property string $latlon
 * @property string $address
 * @property string $category
 * @property string $created_at
 * @property string $updated_at
 */
class Location extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'location';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('portal_id, parent_location_id', 'numerical', 'integerOnly'=>true),
			array('name, type, parent_type', 'length', 'max'=>64),
			array('latlon', 'length', 'max'=>32),
			array('address, category, created_at, updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('location_id, portal_id, name, type, parent_location_id, parent_type, latlon, address, category, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'location_id' => 'ID',
			'portal_id' => 'Portal',
			'name' => 'Location',
			'type' => 'Type',
			'parent_location_id' => 'Parent Location',
			'parent_type' => 'Parent Type',
			'latlon' => 'Latitude Longitude',
			'address' => 'Address',
			'category' => 'Category',
			'created_at' => 'Date Added',
			'updated_at' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('location_id',$this->location_id);
		$criteria->compare('portal_id',$this->portal_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('parent_location_id',$this->parent_location_id);
		$criteria->compare('parent_type',$this->parent_type,true);
		$criteria->compare('latlon',$this->latlon,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('category',$this->category,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Location the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
