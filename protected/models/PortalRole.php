<?php

/**
 * This is the model class for table "portal_role".
 *
 * The followings are the available columns in table 'portal_role':
 * @property integer $portal_role_id
 * @property integer $portal_id
 * @property string $role
 * @property integer $is_active
 * @property string $created_at
 * @property string $updated_at
 */
class PortalRole extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'portal_role';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('portal_id, role, is_active, created_at, updated_at', 'required'),
			array('portal_id, is_active', 'numerical', 'integerOnly'=>true),
			array('role', 'length', 'max'=>16),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('portal_role_id, portal_id, role, is_active, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'portal'=>array(self::BELONGS_TO, 'Portal', array('portal_id'=>'portal_id')),
			'site'=>array(self::BELONGS_TO, 'Site', array('site_id'=>'site_id'), 'through'=>'portal')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'portal_role_id' => 'ID',
			'portal_id' => 'Portal',
			'role' => 'Role',
			'is_active' => 'Status',
			'created_at' => 'Date Added',
			'updated_at' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('portal_role_id',$this->portal_role_id);
		$criteria->compare('portal_id',$this->portal_id);
		$criteria->compare('role',$this->role,true);
		$criteria->compare('is_active',$this->is_active);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cms;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PortalRole the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
