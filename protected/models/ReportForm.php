<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class ReportForm extends CFormModel
{
	public $dates;
	public $sites;
	public $choose_report;

	private $_identity;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			array('dates', 'required'),
			array('sites', 'required'),
		);
	}

	/**
	 * Cadbury Status Queries
	 * Date: March 2, 2016
	 */
    public static function total_unique_visits($date)
    {
		$sql = "SELECT p.name, COUNT(DISTINCT d.mac_address) as count FROM Unifi_Log.device_connection_{$date} dc
				LEFT JOIN Unifi_Captive.device d ON dc.device_id = d.device_id
				LEFT JOIN Unifi_CMS.portal p ON dc.portal_id = p.portal_id
				GROUP BY p.name";

		$command = Yii::app()->db->createCommand($sql);
		$data  = $command->queryAll();
		
		return $data;
    }
	
	public static function total_unique_auth_visitors($date)
	{
		$sql = "SELECT p.name, COUNT(DISTINCT d.mac_address) as count FROM Unifi_Log.device_connection_{$date} dc
				LEFT JOIN Unifi_Captive.device d ON dc.device_id = d.device_id
				LEFT JOIN Unifi_CMS.portal p ON dc.portal_id = p.portal_id
				WHERE username <> '' AND PASSWORD <> ''
				GROUP BY p.name";

		$command = Yii::app()->db->createCommand($sql);
		$data  = $command->queryAll();
		
		return $data;
	}
	
	public static function unique_new_auth_visitors($date)
	{
		$sql = "SELECT p.name, COUNT(DISTINCT d.mac_address) as count FROM Unifi_Log.device_connection_{$date} dc
				LEFT JOIN Unifi_Captive.device d ON dc.device_id = d.device_id
				LEFT JOIN Unifi_CMS.portal p ON dc.portal_id = p.portal_id
				WHERE username <> '' AND PASSWORD <> '' AND DATE(d.created_at) = '{$date}'
				GROUP BY p.name";

		$command = Yii::app()->db->createCommand($sql);
		$data  = $command->queryAll();
		
		return $data;
	}
	
	public static function unique_returning_auth_visitors($date)
	{
		$sql = "SELECT p.name, COUNT(DISTINCT d.mac_address) as count FROM Unifi_Log.device_connection_{$date} dc
				LEFT JOIN Unifi_Captive.device d ON dc.device_id = d.device_id
				LEFT JOIN Unifi_CMS.portal p ON dc.portal_id = p.portal_id
				WHERE username <> '' AND PASSWORD <> '' AND DATE(d.created_at) < '{$date}'
				GROUP BY p.name";

		$command = Yii::app()->db->createCommand($sql);
		$data  = $command->queryAll();
		
		return $data;
	}
	
	/**
	* UNIQUE AUTH AVAILMENT VISITORS BY SITE
	*/
	
	public static function fb($date)
	{
		$sql = "SELECT p.name, COUNT(DISTINCT d.`mac_address`) as count FROM Unifi_Captive.`device_availment` da
				INNER JOIN Unifi_Captive.`avail_facebook` af ON da.`device_availment_id` = af.`device_availment_id`
				LEFT JOIN Unifi_Captive.`device` d ON da.`device_id` = d.`device_id`
				LEFT JOIN Unifi_CMS.portal p ON da.`portal_id` = p.`portal_id`
				WHERE DATE(da.created_at) = '{$date}' 
				GROUP BY p.name";

		$command = Yii::app()->db->createCommand($sql);
		$data  = $command->queryAll();
		
		return $data;
	}
	
	public static function tattoo($date)
	{
		$sql = "SELECT p.name, COUNT(DISTINCT d.`mac_address`) as count FROM Unifi_Captive.`device_availment` da
				INNER JOIN Unifi_Captive.`avail_tattoo` tat ON da.`device_availment_id` = tat.`device_availment_id`
				LEFT JOIN Unifi_Captive.`device` d ON da.`device_id` = d.`device_id`
				LEFT JOIN Unifi_CMS.portal p ON da.`portal_id` = p.`portal_id`
				WHERE DATE(da.created_at) = '{$date}' 
				GROUP BY p.name";

		$command = Yii::app()->db->createCommand($sql);
		$data  = $command->queryAll();
		
		return $data;
	}
	
	public static function registration($date) 
	{
		$sql = "SELECT p.name, COUNT(DISTINCT d.`mac_address`) as count FROM Unifi_Captive.`device_availment` da
				INNER JOIN Unifi_Captive.`avail_registration` ar ON da.`device_availment_id` = ar.`device_availment_id`
				LEFT JOIN Unifi_Captive.`device` d ON da.`device_id` = d.`device_id`
				LEFT JOIN Unifi_CMS.portal p ON da.`portal_id` = p.`portal_id`
				WHERE DATE(da.created_at) = '{$date}' 
				GROUP BY p.name";

		$command = Yii::app()->db->createCommand($sql);
		$data  = $command->queryAll();
		
		return $data;
	}
	
	public static function survey($date) 
	{
		$sql = "SELECT p.name, COUNT(d.`mac_address`) as count FROM Unifi_Captive.`device_availment` da
				INNER JOIN Unifi_Captive.`avail_survey` asurvey ON da.`device_availment_id` = asurvey.`device_availment_id`
				LEFT JOIN Unifi_Captive.`device` d ON da.`device_id` = d.`device_id`
				LEFT JOIN Unifi_CMS.portal p ON da.`portal_id` = p.`portal_id`
				WHERE DATE(da.created_at) = '{$date}' 
				GROUP BY p.name";

		$command = Yii::app()->db->createCommand($sql);
		$data  = $command->queryAll();
		
		return $data;
	}
	
	public static function free($date) 
	{
		$sql = "SELECT p.name, COUNT(DISTINCT d.`mac_address`) as count FROM Unifi_Captive.`device_availment` da
				LEFT JOIN Unifi_Captive.`device` d ON da.`device_id` = d.`device_id`
				LEFT JOIN Unifi_CMS.portal p ON da.`portal_id` = p.`portal_id`
				WHERE DATE(da.created_at) = '{$date}' AND da.`availment_id` = 99
				GROUP BY p.name";

		$command = Yii::app()->db->createCommand($sql);
		$data  = $command->queryAll();
		
		return $data;
	}
	
	public static function mobtel($date)
	{		
		$connection = Yii::app()->getDb();
		$command = $connection->createCommand("SELECT a.name, COUNT(a.mac_address) as count FROM(SELECT p.name, d.mac_address FROM Unifi_Captive.device_availment da INNER JOIN Unifi_Captive.avail_mobile am ON da.device_availment_id = am.device_availment_id LEFT JOIN Unifi_Captive.device d ON da.device_id = d.device_id LEFT JOIN Unifi_CMS.portal p ON da.portal_id = p.portal_id WHERE DATE(da.created_at) = '{$date}' AND is_validated = 1 GROUP BY p.name, d.mac_address,am.msisdn) a GROUP BY a.name");
		
		$data = $command->queryAll();
	
		return $data;
	}
	
	public static function unique_mobile($date) {
		$sql = "SELECT p.name AS `site`,
				CASE pl.`telco_id`
					WHEN 1 THEN 'GLOBE'
					WHEN 2 THEN 'SMART'
					WHEN 3 THEN 'SUN'
					END AS 'telco_brand',
				COUNT(DISTINCT d.mac_address)  AS `unique_mobile_brand_users`
				FROM Unifi_Captive.`device_availment` da
				JOIN Unifi_Captive.`avail_mobile` m ON da.`device_availment_id` = m.`device_availment_id`
				JOIN Unifi_Captive.`device` d ON da.`device_id` = d.`device_id`
				JOIN Unifi_CMS.portal p ON da.`portal_id` = p.`portal_id`
				JOIN Unifi_Captive.plan_prefix AS pr
				ON SUBSTRING(m.msisdn,2,3) = LEFT(pr.prefix,3)
				JOIN Unifi_Captive.plan AS pl
				ON pr.plan_id = pl.plan_id
				WHERE p.portal_id IN(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20) AND m.is_validated = 1 AND DATE(da.created_at) = '{$date}'
				GROUP BY p.portal_id ,pl.`telco_id`";

		$command = Yii::app()->db->createCommand($sql);
		$data  = $command->queryAll();
		
		return $data;
	}
	
	/**
	* Radius Accounting
	*/
	public static function total_session($date) 
	{
		$connection = Yii::app()->getDb();
		$command = $connection->createCommand("SELECT p.portal_id, p.name, SUM(rda.acctsessiontime)/3600 as sum FROM Unifi_Radius.radacct rda LEFT JOIN Unifi_Radius.nas nas ON rda.nasipaddress = nas.switchip LEFT JOIN Unifi_CMS.portal p ON rda.username LIKE CONCAT('%@', p.`code` ,'%') WHERE DATE(rda.acctstarttime) = '{$date}' GROUP BY p.name");
		
		$data = $command->queryAll();
	
		return $data;	
	}
	
	public static function average_duration_per_unique_auth($date) 
	{		
		$connection = Yii::app()->getDb();
		$command = $connection->createCommand("SELECT p.name, (SUM(rda.acctsessiontime)/COUNT( d.mac_address))/3600 as sum FROM Unifi_Radius.radacct rda LEFT JOIN Unifi_Radius.nas nas ON rda.nasipaddress = nas.switchip LEFT JOIN Unifi_CMS.portal p ON rda.username LIKE CONCAT('%@', p.code ,'%') LEFT JOIN Unifi_Captive.device d ON rda.username LIKE CONCAT('%', d.mac_address ,'%') LEFT JOIN Unifi_Log.device_connection_20160115 dc ON d.device_id = dc.device_id WHERE dc.username <> '' AND dc.password <> '' AND DATE(rda.acctstarttime) = '{$date}' GROUP BY p.name");
		
		$data = $command->queryAll();
	
		return $data;	
	}
	
	public static function total_input_volume_in_gb($date)
	{
		$connection = Yii::app()->getDb();
		$command = $connection->createCommand("SELECT p.name, SUM(rda.acctinputoctets)/1024/1024/1024 as sum FROM Unifi_Radius.radacct rda LEFT JOIN Unifi_Radius.nas nas ON rda.nasipaddress = nas.switchip LEFT JOIN Unifi_CMS.portal p ON rda.username LIKE CONCAT('%@', p.code ,'%') WHERE DATE(rda.acctstarttime) = '{$date}' GROUP BY p.name");
		
		$data = $command->queryAll();
	
		return $data;
	}
	
	public static function total_output_volume_in_gb($date)
	{
		$connection = Yii::app()->getDb();
		$command = $connection->createCommand("SELECT p.name, SUM(rda.acctoutputoctets)/1024/1024/1024 as sum FROM Unifi_Radius.radacct rda LEFT JOIN Unifi_Radius.nas nas ON rda.nasipaddress = nas.switchip LEFT JOIN Unifi_CMS.portal p ON rda.username LIKE CONCAT('%@', p.code ,'%') WHERE DATE(rda.`acctstarttime`) = '{$date}' GROUP BY p.name");
		
		$data = $command->queryAll();
	
		return $data;
	}
	
	/**
	* Aruba
	*/
	public static function success($date)
	{
		$from = "Unifi_Log.aruba_{$date} dc";
		$rows = (new \yii\db\Query())
			->select('p.name, COUNT(*)')
			->from($from)
			->leftJoin('Unifi_CMS.portal p', 'a.portal_id = p.portal_id')
			->where('status', '0')
			->groupBy('p.name');
			
		$command = $rows->createCommand();
		$data = $command->queryAll();

		return $data;		
	}
	
	public static function error()
	{
		$from = "Unifi_Log.aruba_{$date} dc";
		$rows = (new \yii\db\Query())
			->select('p.name, COUNT(*)')
			->from($from)
			->leftJoin('Unifi_CMS.portal p', 'a.portal_id = p.portal_id')
			->where(['!=', 'status', '0'])
			->groupBy('p.name');
			
		$command = $rows->createCommand();
		$data = $command->queryAll();

		return $data;	
	}
}
