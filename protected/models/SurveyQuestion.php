<?php

/**
 * This is the model class for table "survey_question".
 *
 * The followings are the available columns in table 'survey_question':
 * @property integer $survey_question_id
 * @property integer $survey_id
 * @property string $question
 * @property integer $position
 * @property integer $is_required
 * @property integer $is_active
 * @property string $created_at
 * @property string $updated_at
 */
class SurveyQuestion extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'survey_question';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('survey_id, question, is_required, is_active, created_at, updated_at', 'required'),
			array('survey_question_id, survey_id, position, is_required, is_active', 'numerical', 'integerOnly'=>true),
			array('question', 'length', 'max'=>256),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'survey'=>array(self::BELONGS_TO, 'Survey', array('survey_id'=>'survey_id'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'survey_question_id' => 'ID',
			'survey_id' => 'Survey',
			'question' => 'Question',
			'position' => 'Position',
			'is_required' => 'Is required',
			'is_active' => 'Status',
			'created_at' => 'Date Added',
			'updated_at' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('survey_question_id',$this->survey_question_id);
		$criteria->compare('survey_id',$this->survey_id);
		$criteria->compare('question',$this->question,true);
		$criteria->compare('position',$this->position);
		$criteria->compare('is_required',$this->is_required);
		$criteria->compare('is_active',$this->is_active);
		$criteria->order = 'position ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cms;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SurveyQuestion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public static function findMaxPosition($survey_id)
	{
		$model = new SurveyQuestion;
		$criteria = new CDbCriteria;
		$criteria->select='max(position) AS position';
		$criteria->compare('survey_id',$survey_id);
		$row = $model->model()->find($criteria);
		return (int)$position = $row['position'];
	}
}
