<?php

/**
 * This is the model class for table "device".
 *
 * The followings are the available columns in table 'device':
 * @property integer $device_id
 * @property string $correlor_id
 * @property string $mac_address
 * @property string $created_at
 * @property string $updated_at
 */
class Device extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Unifi_Captive.device';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('mac_address, created_at, updated_at', 'required'),
			array('correlor_id', 'length', 'max'=>64),
			array('mac_address', 'length', 'max'=>32),
			array('mac_address', 'unique'),
			array('device_id', 'numerical', 'integerOnly'=>true),
			array('created_at, updated_at', 'date', 'format'=>'yyyy-MM-dd hh:mm:ss'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'device_id' => 'ID',
			'correlor_id' => 'Correlor ID',
			'mac_address' => 'MAC Address',
			'created_at' => 'Date Added',
			'updated_at' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('device_id',$this->device_id);
		$criteria->compare('correlor_id',$this->correlor_id,true);
		$criteria->compare('mac_address',$this->mac_address,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cp;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Device the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Get the current session time of the Device
	 * 
	 * @access public
	 * @param DeviceConnection $connection
	 * @return int
	 */
	public static function getSessionTime(DeviceConnection $connection)
	{
		// Get latest availed method
		$latest_avail = ConnectionAvail::model()->findByAttributes(array('device_connection_id'=>$connection->device_connection_id), array('order'=>'Created_at DESC'));
		
		if ( ! empty($latest_avail)) {
			// Get sessions without availment ID and set with current availment ID
			self::_setAvailmentSession($latest_avail->availment_id, $connection->username);

			//Get session total session time
			$current_session_time = self::_getAvailmentSession($latest_avail->availment_id, $connection->username);
			return $current_session_time[0];
		}
		
		return FALSE;
	}
	
	/**
	 * Set unreferenced session with the latest availment method
	 *  
	 * @access private
	 * @param int $availment_id Latest availment ID
	 * @param string $username Connection username
	 */
	private static function _setAvailmentSession($availment_id, $username)
	{
		// Get sessions without availment ID
		$curl = "http://localhost/radius-ws/current_session.php?username={$username}";
		$link = curl_init($curl);
		curl_setopt($link, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($link, CURLOPT_RETURNTRANSFER, true);
		$data = curl_exec($link);
		curl_close($link);
		
		$data = CJSON::decode($data);
		if ( ! empty($data)) {
			foreach ($data as $d) {
				$curl = "http://localhost/radius-ws/radacct_availment.php?radacct_id={$d['radacctid']}&availment_id={$availment_id}";
				$link = curl_init($curl);
				curl_setopt($link, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($link, CURLOPT_RETURNTRANSFER, true);
				$data = curl_exec($link);
				curl_close($link);
			}
		}
	}
	
	private static function _getAvailmentSession($availment_id, $username)
	{
		// Get sessions
		$curl = "http://localhost/radius-ws/current_total_sessiontime.php?username={$username}&availment_id={$availment_id}";
		$link = curl_init($curl);
		curl_setopt($link, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($link, CURLOPT_RETURNTRANSFER, true);
		$data = curl_exec($link);
		curl_close($link);
		
		return CJSON::decode($data);
	}
}
