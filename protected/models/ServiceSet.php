<?php

/**
 * This is the model class for table "service_set".
 *
 * The followings are the available columns in table 'service_set':
 * @property string $service_set_id
 * @property string $ssid
 * @property integer $is_active
 * @property string $created_at
 * @property string $updated_at
 */
class ServiceSet extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'service_set';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('ssid', 'unique'),
			array('ssid, is_active', 'required'),
			array('service_set_id, is_active', 'numerical', 'integerOnly'=>true),
			array('ssid', 'length', 'max'=>50),
			array('created_at, updated_at', 'safe'),
			
			array('service_set_id, ssid, is_active, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'service_set_id'=>'ID',
			'ssid'=>'SSID',
			'is_active'=>'Status',
			'created_at'=>'Created At',
			'updated_at'=>'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('service_set_id',$this->service_set_id,true);
		$criteria->compare('ssid',$this->ssid,true);
		$criteria->compare('is_active',$this->is_active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cms;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ServiceSet the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
