<?php

/**
 * This is the model class for table "state".
 *
 * The followings are the available columns in table 'state':
 * @property integer $state_id
 * @property integer $country_id
 * @property string $iso_code
 * @property string $state
 * @property integer $is_active
 * @property string $created_at
 * @property string $updated_at
 */
class State extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'state';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('country_id, iso_code, state, is_active', 'required'),
			array('state_id, country_id, is_active', 'numerical', 'integerOnly'=>true),
			array('iso_code', 'length', 'max'=>8),
			array('state', 'length', 'max'=>128),
			array('iso_code', 'unique'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'country'=>array(self::BELONGS_TO, 'Country', array('country_id'=>'country_id')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'state_id' => 'ID',
			'country_id' => 'Country',
			'iso_code' => 'ISO Code',
			'state' => 'State / Province',
			'is_active' => 'Status',
			'created_at' => 'Date Added',
			'updated_at' => 'Date Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('state_id',$this->state_id);
		$criteria->compare('country_id',$this->country_id);
		$criteria->compare('iso_code',$this->iso_code,true);
		$criteria->compare('state',$this->state,true);
		$criteria->compare('is_active',$this->is_active);
		$criteria->order = 'state ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return State the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
