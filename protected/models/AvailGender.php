<?php

/**
 * This is the model class for table "avail_registration".
 *
 * The followings are the available columns in table 'avail_registration':
 * @property integer $avail_registration_id
 * @property integer $device_availment_id
 * @property string $correlor_id
 * @property string $msisdn
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $birthday
 * @property integer $gender
 * @property string $created_at
 * @property string $updated_at
 */
class AvailGender extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'avail_gender';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('msisdn, gender', 'required'),
			array('avail_gender_id, avail_mobile_id, msisdn, gender', 'numerical', 'integerOnly'=>true),
			array('msisdn', 'length', 'max'=>12),
			array('created_at, updated_at', 'date', 'format'=>'yyyy-MM-dd hh:mm:ss'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'avail_mob'=>array(self::BELONGS_TO, 'AvailMobile', array('avail_mobile_id'=>'avail_mobile_id'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'avail_gender_id' => 'Avail Gender',
			'msisdn' => 'Msisdn',
			'gender' => 'Gender',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('avail_gender_id',$this->avail_gender_id);
		$criteria->compare('msisdn',$this->msisdn,true);
		$criteria->compare('gender',$this->gender);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cp;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AvailRegistration the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
