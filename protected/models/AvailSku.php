<?php

/**
 * This is the model class for table "avail_sku".
 *
 * The followings are the available columns in table 'avail_sku':
 * @property integer $avail_sku_id
 * @property integer $device_availment_id
 * @property integer $account_type_id
 * @property string $msisdn
 * @property string $vn
 * @property string $auth_code
 * @property integer $is_validated
 * @property string $created_at
 * @property string $updated_at
 */
class AvailSku extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'avail_sku';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('msisdn', 'required'),
			array('device_availment_id, account_type_id, is_validated', 'numerical', 'integerOnly'=>true),
			array('msisdn', 'length', 'max'=>11, 'min'=>11),
			array('vn', 'length', 'max'=>11, 'min'=>11),
			array('auth_limit', 'length', 'max'=>1),
			array('auth_code', 'length', 'max'=>5),
			array('promo_id', 'length', 'max'=>3),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('avail_sku_id, device_availment_id, account_type_id, msisdn, vn, auth_code, auth_limit, is_validated, promo_id, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dev_avail'=>array(self::BELONGS_TO, 'DeviceAvailment', array('device_availment_id'=>'device_availment_id')),
			'promo'=>array(self::BELONGS_TO, 'Promo', array('promo_id'=>'promo_id'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'avail_sku_id' => 'ID',
			'device_availment_id' => 'Device Availment',
			'account_type_id' => 'Account Type',
			'msisdn' => 'MSISDN',
			'vn' => 'Virtual No.',
			'auth_code' => 'Authentication Code',
			'auth_limit' => 'Authentication Limit',
			'is_validated' => 'Is Validated',
			'promo_id' => 'Promo ID',
			'created_at' => 'Date Added',
			'updated_at' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('avail_sku_id',$this->avail_sku_id);
		$criteria->compare('device_availment_id',$this->device_availment_id);
		$criteria->compare('account_type_id',$this->account_type_id);
		$criteria->compare('msisdn',$this->msisdn,true);
		$criteria->compare('vn',$this->vn,true);
		$criteria->compare('auth_code',$this->auth_code,true);
		$criteria->compare('auth_limit',$this->auth_limit,true);
		$criteria->compare('is_validated',$this->is_validated);
		$criteria->compare('promo_id',$this->promo_id);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cp;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AvailSku the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
