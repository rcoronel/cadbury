<?php

/**
 * This is the model class for table "tattoo_subscriber".
 *
 * The followings are the available columns in table 'tattoo_subscriber':
 * @property integer $tattoo_subscriber_id
 * @property integer $plan
 * @property string $name
 * @property string $account_no
 * @property integer $network_service_id
 * @property string $service_type
 * @property string $description
 * @property string $created_at
 */
class TattooSubscriber extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tablename()
	{
		return 'tattoo_subscriber';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('plan, account_no', 'required', 'on'=>'login'),
			array('plan, account_no, name, service_type, Created_at', 'required', 'on'=>'insert'),
			array('plan, network_service_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>256),
			array('account_no', 'length', 'max'=>16),
			array('service_type', 'length', 'max'=>2),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tattoo_subscriber_id'=>'ID',
			'plan'=>'Plan',
			'name'=>'Name',
			'account_no'=>'Account No',
			'network_service_id'=>'Network Service',
			'service_type'=>'Service Type',
			'description'=>'Description',
			'created_at'=>'Created At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('tattoo_subscriber_id',$this->tattoo_subscriber_id);
		$criteria->compare('plan',$this->plan);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('account_no',$this->account_no,true);
		$criteria->compare('network_service_id',$this->network_service_id);
		$criteria->compare('service_type',$this->service_type,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('created_at',$this->created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $classname active record class name.
	 * @return TattooSubscriber the static model class
	 */
	public static function model($classname=__CLASS__)
	{
		return parent::model($classname);
	}
}
