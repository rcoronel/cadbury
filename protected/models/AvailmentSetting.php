<?php

/**
 * This is the model class for table "availment_setting".
 *
 * The followings are the available columns in table 'availment_setting':
 * @property integer $availment_setting_id
 * @property integer $portal_id
 * @property integer $availment_id
 * @property string $config
 * @property string $value
 */
class AvailmentSetting extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'availment_setting';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('portal_id, availment_id, config, value', 'required'),
			array('portal_id, availment_id', 'numerical', 'integerOnly'=>true),
			array('config', 'length', 'max'=>16),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('availment_setting_id, portal_id, availment_id, config, value', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'availment_setting_id'=>'Availment Setting',
			'portal_id'=>'Portal',
			'availment_id'=>'Availment',
			'config'=>'Config',
			'value'=>'Value',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('availment_setting_id',$this->availment_setting_id);
		$criteria->compare('portal_id',$this->portal_id);
		$criteria->compare('availment_id',$this->availment_id);
		$criteria->compare('config',$this->config,true);
		$criteria->compare('value',$this->value,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cms;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AvailmentSetting the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Get Configuration Value
	 * 
	 * @access	public
	 * @param string $config Configuration Value to be search
	 * @param mixed $default Default Value to be set
	 * @return string
	 */
	public static function getValue($availment_id, $config = '', $default = '')
	{
		$portal_id = 0;
		$context = Yii::app()->controller->context;
		if ($context->portal) {
			$portal_id = $context->portal->portal_id;
		}
		else {
			$portal_id = Yii::app()->controller->portal->portal_id;
		}
		$conf = AvailmentSetting::model()->findByAttributes(array('config'=>$config, 'portal_id'=>$portal_id, 'availment_id'=>$availment_id));
		
		if (empty($conf)) {
			return $default;
		}
		return $conf->value;
	}
	
	/**
	 * Set Configuration Value
	 * 
	 * @access	public
	 * @param string $config Configuration Value
	 * @param mixed $value
	 * @return string
	 */
	public static function setValue($availment_id, $config, $value = '')
	{
		$context = Context::getContext();
		
		if ($context->portal) {
			$portal_id = $context->portal->portal_id;
		}
		else {
			$portal_id = Yii::app()->controller->portal->portal_id;
		}
		
		// Delete previous config
		AvailmentSetting::model()->deleteAllByAttributes(array('portal_id'=>$portal_id, 'config'=>$config, 'availment_id'=>$availment_id));
		
		// Insert record
		$object = new AvailmentSetting;
		$object->portal_id = $portal_id;
		$object->availment_id = $availment_id;
		$object->config = $config;
		$object->value = $value;
		$object->validate() && $object->save();
		
		return TRUE;
	}
}
