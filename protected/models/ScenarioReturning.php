<?php

/**
 * This is the model class for table "scenario_returning".
 *
 * The followings are the available columns in table 'scenario_returning':
 * @property integer $scenario_returning_id
 * @property integer $scenario_id
 * @property integer $availment_id
 * @property integer $level
 * @property string $created_at
 */
class ScenarioReturning extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'scenario_returning';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('scenario_id, availment_id, level, created_at', 'required'),
			array('scenario_id, availment_id, level', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('scenario_returning_id, scenario_id, availment_id, level, created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'availment'=>array(self::BELONGS_TO, 'Availment', 'availment_id', 'together'=>TRUE),
			'scenario'=>array(self::BELONGS_TO, 'Scenario', 'scenario_id', 'together'=>TRUE)
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'scenario_returning_id' => 'ID',
			'scenario_id' => 'Scenario',
			'availment_id' => 'Availment',
			'level' => 'level',
			'created_at' => 'Date Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('scenario_returning_id',$this->scenario_returning_id);
		$criteria->compare('scenario_id',$this->scenario_id);
		$criteria->compare('availment_id',$this->availment_id);
		$criteria->compare('level',$this->level);
		$criteria->compare('created_at',$this->created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cms;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ScenarioReturning the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public static function findScenario($availments = array(), $portal_id = 0)
	{
		$cnt = (int)count($availments);
		$where = '';
		foreach ($availments as $index=>$a) {
			if ($index != 0) {
				$where .= " OR ";
			}
			$where .= "(level = {$a->level} AND availment_id = {$a->availment_id})";
		}
		$where .= " AND portal_id = {$portal_id}";
		
		// Check for returning availments
		$ids = Yii::app()->db_cp->createCommand()
			->select("sa.scenario_id")
			->from("Unifi_CMS.scenario_returning sa")
			->join('Unifi_CMS.scenario s', 's.scenario_id = sa.scenario_id')
			->where($where)
			->group(array("sa.scenario_id"))
			->having("COUNT(sa.scenario_id) = {$cnt}")
			->queryAll();
		
		$id_array = array();
		if ( !empty($ids)) {
			foreach ($ids as $i) {
				$id_array[] = $i['scenario_id'];
			}
		}
		
		// if empty returning availment, check previous
		if (empty($id_array)) {
			$ids = Yii::app()->db_cp->createCommand()
			->select("sa.scenario_id")
			->from("Unifi_CMS.scenario_availment sa")
			->join('Unifi_CMS.scenario s', 's.scenario_id = sa.scenario_id')
			->where($where)
			->group(array("sa.scenario_id"))
			->having("COUNT(sa.scenario_id) = {$cnt}")
			->queryAll();
		
			$id_array = array();
			if ( !empty($ids)) {
				foreach ($ids as $i) {
					$id_array[] = $i['scenario_id'];
				}
			}
		}
		
		return $id_array;
	}
}
