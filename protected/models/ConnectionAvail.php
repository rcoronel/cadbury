<?php

/**
 * This is the model class for table "connection_avail".
 *
 * The followings are the available columns in table 'connection_avail':
 * @property integer $connection_avail_id
 * @property integer $device_connection_id
 * @property integer $availment_id
 * @property integer $level
 * @property string $created_at
 */
class ConnectionAvail extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'connection_avail_'.date('Ymd');
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('device_connection_id, availment_id, level, created_at', 'required'),
			array('connection_avail_id, device_connection_id, availment_id, level', 'numerical', 'integerOnly'=>true),
			array('created_at', 'date', 'format'=>'yyyy-MM-dd hh:mm:ss'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'connection_avail_id' => 'Connection Avail',
			'device_connection_id' => 'Connection',
			'availment_id' => 'Availment',
			'level' => 'Level',
			'created_at' => 'Date Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('connection_avail_id',$this->connection_avail_id);
		$criteria->compare('device_connection_id',$this->device_connection_id);
		$criteria->compare('availment_id',$this->availment_id);
		$criteria->compare('level',$this->level);
		$criteria->compare('created_at',$this->created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_log;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ConnectionAvail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
