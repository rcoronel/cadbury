<?php

/**
 * This is the model class for table "radcheck".
 *
 * The followings are the available columns in table 'radcheck':
 * @property string $id
 * @property string $username
 * @property string $attribute
 * @property string $op
 * @property string $value
 */
class RadiusCheck extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'radcheck';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, attribute', 'length', 'max'=>64),
			array('op', 'length', 'max'=>2),
			array('value', 'length', 'max'=>253),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, username, attribute, op, value', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'username',
			'attribute' => 'Attribute',
			'op' => 'Op',
			'value' => 'Value',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('attribute',$this->attribute,true);
		$criteria->compare('op',$this->op,true);
		$criteria->compare('value',$this->value,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_radius;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RadiusCheck the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Add user to RadiusCheck
	 * 
	 * @access public
	 * @param object $connection DeviceConnection instance
	 * @param int $availment PortalAvailment instance
	 * @return bool
	 */
	public static function addUser(DeviceConnection $connection, PortalAvailment $availment)
	{
		// input credentials
		$cred = RadiusCheck::model()->findByAttributes(array('username'=>$connection->username, 'attribute'=>'Cleartext-password'));
		if (empty($cred)) {
			$cred = new RadiusCheck;
		}
		$cred->username = $connection->username;
		$cred->attribute = 'Cleartext-Password';
		$cred->op = ':=';
		$cred->value = $connection->password;
		$cred->validate() && $cred->save();
	
		// input max daily session
		$daily_sess = RadiusCheck::model()->findByAttributes(array('username'=>$connection->username, 'attribute'=>'Max-Daily-Session'));
		if (empty($daily_sess)) {
			$daily_sess = new RadiusCheck;
		}
		$daily_sess->username = $connection->username;
		$daily_sess->attribute = 'Max-Daily-Session';
		$daily_sess->op = ':=';
		$daily_sess->value = 86400;
		//$daily_sess->value = 3600;
		$daily_sess->save();
		
		// Get availment details
		$model = Yii::app()->controller->scene_model;
		$level = Yii::app()->controller->level;
		//$availment = PortalAvailment::model()->findByAttributes(array('portal_id'=>$connection->portal_id, 'availment_id'=>$availment_id));
		$reply = RadiusReply::model()->findByAttributes(array('username'=>$connection->username));
		if (empty($reply)) {
			$reply = new RadiusReply;
		}
		
		$reply->username = $connection->username;
		$reply->attribute = 'Session-Timeout';
		$reply->op = ':=';
		$reply->value = $availment->duration;
		$reply->save();
		
		return TRUE;
	}
}
