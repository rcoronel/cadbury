<?php

/**
 * This is the model class for table "device_availment".
 *
 * The followings are the available columns in table 'device_availment':
 * @property integer $device_availment_id
 * @property integer $availment_id
 * @property integer $nas_id
 * @property integer $site_id
 * @property integer $portal_id
 * @property integer $device_id
 * @property integer $access_point_id
 * @property integer $access_point_group_id
 * @property string $created_at
 */
class DeviceAvailment extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'device_availment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			#array('availment_id, nas_id, site_id, portal_id, device_id, access_point_id, access_point_group_id, created_at', 'required'),
			array('availment_id, nas_id, site_id, portal_id, device_id, created_at', 'required'),
			array('device_availment_id, availment_id, nas_id, site_id, portal_id, device_id, access_point_id, access_point_group_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('device_availment_id, availment_id, nas_id, site_id, portal_id, device_id, access_point_id, access_point_group_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'availment'=>array(self::BELONGS_TO, 'Availment', array('availment_id'=>'availment_id'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'device_availment_id' => 'Device Connection',
			'availment_id' => 'Availment',
			'nas_id' => 'Nas',
			'site_id' => 'Site',
			'portal_id' => 'Portal',
			'device_id' => 'Device',
			'access_point_id' => 'Access Point',
			'access_point_group_id' => 'Access Point Group',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('device_availment_id',$this->device_availment_id);
		$criteria->compare('availment_id',$this->availment_id);
		$criteria->compare('nas_id',$this->nas_id);
		$criteria->compare('site_id',$this->site_id);
		$criteria->compare('portal_id',$this->portal_id);
		$criteria->compare('device_id',$this->device_id);
		$criteria->compare('access_point_id',$this->access_point_id);
		$criteria->compare('access_point_group_id',$this->access_point_group_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cp;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DeviceAvailment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public static function poll($date, $site_id = 0, $portal_id = 0, $apg_id = 0, $ap_id = 0, $availment_id = 0)
	{
		$format_date = date('Y-m-d', strtotime($date));
		$where = "DATE(created_at) = '{$format_date}'";
		
		// site ID
		if ($site_id) {
			$where .= " AND site_id = {$site_id}"; 
		}
		else {
			$where .= " AND site_id IS NOT NULL";
		}
		
		// portal ID
		if ($portal_id) {
			$where .= " AND portal_id = {$portal_id}"; 
		}
		else {
			$where .= " AND portal_id IS NOT NULL";
		}
		
		// AP Group ID
		if ($apg_id) {
			$where .= " AND access_point_group_id = {$apg_id}"; 
		}
		else {
			$where .= " AND access_point_group_id IS NOT NULL";
		}
		
		// AP ID
		if ($ap_id) {
			$where .= " AND access_point_id = {$ap_id}"; 
		}
		else {
			$where .= " AND access_point_id IS NOT NULL";
		}
		
		// Availment ID
		if ($availment_id) {
			$where .= " AND availment_id = {$availment_id}"; 
		}
		else {
			$where .= " AND availment_id IS NOT NULL";
		}
		
		return $count = DeviceAvailment::model()->count($where);
	}
}
