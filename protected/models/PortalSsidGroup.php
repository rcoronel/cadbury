<?php

/**
 * This is the model class for table "portal_ssid_group".
 *
 * The followings are the available columns in table 'portal_ssid_group':
 * @property integer $portal_ssid_group_id
 * @property integer $portal_id
 * @property integer $service_set_id
 * @property integer $access_point_group_id
 * @property string $created_at
 */
class PortalSsidGroup extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'portal_ssid_group';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('portal_id, service_set_id, access_point_group_id', 'required'),
			array('portal_ssid_group_id, portal_id, service_set_id, access_point_group_id', 'numerical', 'integerOnly'=>true),
			array('portal_id', 'UniqueValidator', 'attributeName'=>array('portal_id', 'service_set_id','access_point_group_id'))
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'portal'=>array(self::BELONGS_TO, 'Portal', array('portal_id'=>'portal_id')),
			'ap_group'=>array(self::BELONGS_TO, 'AccessPointGroup', array('access_point_group_id'=>'access_point_group_id')),
			'ssid'=>array(self::BELONGS_TO, 'ServiceSet', array('service_set_id'=>'service_set_id'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'portal_ssid_group_id' => 'ID',
			'portal_id' => 'Portal',
			'service_set_id' => 'SSID',
			'access_point_group_id' => 'AP Group',
			'created_at' => 'Date Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('portal_ssid_group_id',$this->portal_ssid_group_id);
		$criteria->compare('portal_id',$this->portal_id);
		$criteria->compare('service_set_id',$this->service_set_id);
		$criteria->compare('access_point_group_id',$this->access_point_group_id);
		$criteria->compare('created_at',$this->created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cms;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PortalSsidGroup the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
