<?php

/**
 * This is the model class for table "survey".
 *
 * The followings are the available columns in table 'survey':
 * @property integer $survey_id
 * @property integer $nas_id
 * @property integer $site_id
 * @property integer $portal_id
 * @property integer $access_point_group_id
 * @property integer $access_point_id
 * @property string $title
 * @property integer $position
 * @property integer $is_active
 * @property string $created_at
 * @property string $updated_at
 * @property integer $is_ad
 */
class Survey extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'survey';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('portal_id, title, is_active, token, created_at, updated_at', 'required'),
			array('survey_id, portal_id, position, is_active, is_ad', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>64),
			array('token', 'length', 'max'=>128),
			array('created_at, updated_at', 'date', 'format'=>'yyyy-mm-dd hh:mm:ss'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'survey_id' => 'ID',
			'portal_id' => 'Portal',
			'title' => 'Title',
			'position' => 'Position',
			'is_active' => 'Status',
			'created_at' => 'Date Added',
			'updated_at' => 'Date Updated',
			'is_ad' => 'Ad Survey',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('survey_id',$this->survey_id);
		$criteria->compare('portal_id',$this->portal_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('position',$this->position);
		$criteria->compare('is_active',$this->is_active);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('is_ad',$this->is_ad);
		$criteria->order = 'position ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cms;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Survey the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public static function findMaxPosition($portal_id)
	{
		$model = new Survey;
		$criteria = new CDbCriteria;
		$criteria->select='max(position) AS position';
		$criteria->compare('portal_id',$portal_id);
		$row = $model->model()->find($criteria);
		return (int)$position = $row['position'];
	}
}
