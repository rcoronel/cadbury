<?php

/**
 * This is the model class for table "avail_facebook".
 *
 * The followings are the available columns in table 'avail_facebook':
 * @property integer $avail_facebook_id
 * @property string $correlor_id
 * @property integer $fb_id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property integer $gender
 * @property string $picture
 * @property string $access_token
 * @property string $access_expiry
 * @property string $created_at
 * @property string $updated_at
 */
class AvailFacebook_old extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'avail_facebook';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('fb_id, firstname, lastname, email, access_token', 'required'),
			array('avail_facebook_id, fb_id', 'numerical', 'integerOnly'=>true),
			array('correlor_id', 'length', 'max'=>63),
			array('firstname, lastname', 'length', 'max'=>32),
			array('email', 'length', 'max'=>128),
			array('gender', 'length', 'max'=>10),
			array('access_token', 'length', 'max'=>256),
			array('created_at, updated_at', 'date', 'format'=>'yyyy-MM-dd hh:mm:ss'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'dev_avail'=>array(self::BELONGS_TO, 'DeviceAvailment', array('device_availment_id'=>'device_availment_id'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'avail_facebook_id' => 'ID',
			'correlor_id' => 'Correlor ID',
			'fb_id' => 'FB ID',
			'firstname' => 'First name',
			'lastname' => 'Last name',
			'email' => 'Email',
			'gender' => 'Gender',
			'picture' => 'Picture',
			'access_token' => 'Access Token',
			'access_expiry' => 'Access Expiry',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('avail_facebook_id',$this->avail_facebook_id);
		$criteria->compare('correlor_id',$this->correlor_id,true);
		$criteria->compare('fb_id',$this->fb_id);
		$criteria->compare('firstname',$this->firstname,true);
		$criteria->compare('lastname',$this->lastname,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('gender',$this->gender);
		$criteria->compare('picture',$this->picture,true);
		$criteria->compare('access_token',$this->access_token,true);
		$criteria->compare('access_expiry',$this->access_expiry,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cp_old;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AvailFacebook the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
