<?php

/**
 * This is the model class for table "portal".
 *
 * The followings are the available columns in table 'portal':
 * @property integer $portal_id
 * @property integer $site_id
 * @property string $code
 * @property string $name
 * @property string $description
 * @property integer $is_active
 * @property string $created_at
 * @property string $updated_at
 */
class Portal extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'portal';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('site_id, code, name, description, is_active, created_at, updated_at', 'required'),
			array('portal_id, site_id, is_active', 'numerical', 'integerOnly'=>true),
			array('code', 'length', 'max'=>16),
			array('name', 'length', 'max'=>64),
			array('description', 'length', 'max'=>256),
			array('created_at, updated_at', 'date', 'format'=>'yyyy-mm-dd hh:mm:ss'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'site'=>array(self::BELONGS_TO, 'Site', array('site_id'=>'site_id')),
			'ap_group'=>array(self::HAS_MANY, 'AccessPointGroup', array('portal_id'=>'portal_id')),
			'ap'=>array(self::HAS_MANY, 'AccessPoint', array('access_point_group_id'=>'access_point_group_id'), 'through'=>'ap_group')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'portal_id' => 'ID',
			'site_id' => 'Site',
			'code' => 'Portal Code',
			'name' => 'Name',
			'description' => 'Description',
			'is_active' => 'Status',
			'created_at' => 'Date added',
			'updated_at' => 'Date updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('portal_id',$this->portal_id);
		$criteria->compare('site_id',$this->site_id);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('is_active',$this->is_active);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->order = 'name ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cms;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Portal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
