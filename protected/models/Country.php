<?php

/**
 * This is the model class for table "country".
 *
 * The followings are the available columns in table 'country':
 * @property integer $country_id
 * @property string $iso_code
 * @property string $call_prefix
 * @property string $zip_code_format
 * @property string $country
 * @property integer $is_active
 * @property string $created_at
 * @property string $updated_at
 */
class Country extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'country';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('iso_code, call_prefix, country, is_active', 'required'),
			array('is_active', 'numerical', 'integerOnly'=>true),
			array('iso_code', 'length', 'max'=>3),
			array('call_prefix', 'length', 'max'=>10),
			array('zip_code_format', 'length', 'max'=>12),
			array('country', 'length', 'max'=>128),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('country_id, iso_code, call_prefix, zip_code_format, country, is_active, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'country_id' => 'ID',
			'iso_code' => 'ISO Code',
			'call_prefix' => 'Call Prefix',
			'zip_code_format' => 'Zip Code Format',
			'country' => 'Country Name',
			'is_active' => 'Status',
			'created_at' => 'Date Added',
			'updated_at' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('country_id',$this->country_id);
		$criteria->compare('iso_code',$this->iso_code,true);
		$criteria->compare('call_prefix',$this->call_prefix,true);
		$criteria->compare('zip_code_format',$this->zip_code_format,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('is_active',$this->is_active);
		$criteria->order = 'country ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Country the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
