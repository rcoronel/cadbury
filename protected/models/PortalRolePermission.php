<?php

/**
 * This is the model class for table "portal_role_permission".
 *
 * The followings are the available columns in table 'portal_role_permission':
 * @property integer $portal_role_permission_id
 * @property integer $portal_role_id
 * @property integer $portal_menu_id
 * @property integer $view
 * @property integer $add
 * @property integer $edit
 * @property integer $delete
 */
class PortalRolePermission extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'portal_role_permission';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('portal_role_id, portal_menu_id, view, add, edit, delete', 'required'),
			array('portal_role_id, portal_menu_id, view, add, edit, delete', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('portal_role_permission_id, portal_role_id, portal_menu_id, view, add, edit, delete', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'portal_role_permission_id' => 'ID',
			'portal_role_id' => 'Role',
			'portal_menu_id' => 'Menu',
			'view' => 'View',
			'add' => 'Add',
			'edit' => 'Edit',
			'delete' => 'Delete',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('portal_role_permission_id',$this->portal_role_permission_id);
		$criteria->compare('portal_role_id',$this->portal_role_id);
		$criteria->compare('portal_menu_id',$this->portal_menu_id);
		$criteria->compare('view',$this->view);
		$criteria->compare('add',$this->add);
		$criteria->compare('edit',$this->edit);
		$criteria->compare('delete',$this->delete);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cms;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PortalRolePermission the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
