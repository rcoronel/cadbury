<?php

/**
 * This is the model class for table "device_connection".
 *
 * The followings are the available columns in table 'device_connection':
 * @property integer $device_connection_id
 * @property integer $nas_id
 * @property integer $site_id
 * @property integer $portal_id
 * @property integer $access_point_group_id
 * @property integer $access_point_id
 * @property integer $device_id
 * @property integer $location_id
 * @property string $ip_address
 * @property string $ssid
 * @property string $username
 * @property string $password
 * @property string $connection_token
 * @property integer $level
 * @property string $created_at
 * @property string $updated_at
 */
class DeviceConnection extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'device_connection_'.date('Ymd');
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('nas_id, site_id, portal_id, access_point_group_id, access_point_id, device_id, ip_address, connection_token, created_at, updated_at', 'required'),
			array('device_connection_id, nas_id, site_id, portal_id, access_point_group_id, access_point_id, device_id, location_id, level', 'numerical', 'integerOnly'=>true),
			array('ip_address', 'length', 'max'=>32),
			array('ssid', 'length', 'max'=>64),
			array('username, password, connection_token', 'length', 'max'=>128),
			array('created_at, updated_at', 'date', 'format'=>'yyyy-MM-dd hh:mm:ss'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'device'=>array(self::BELONGS_TO, 'Device', array('device_id'=>'device_id'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'device_connection_id' => 'Device Connection',
			'nas_id' => 'NAS',
			'site_id' => 'Site',
			'portal_id' => 'Portal',
			'access_point_group_id' => 'AP Group',
			'access_point_id' => 'AP',
			'device_id' => 'Device',
			'location_id' => 'Location',
			'ip_address' => 'IP Address',
			'username' => 'Username',
			'password' => 'Password',
			'connection_token' => 'Token',
			'level' => 'Level',
			'created_at' => 'Date Added',
			'updated_at' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('device_connection_id',$this->device_connection_id);
		$criteria->compare('nas_id',$this->nas_id);
		$criteria->compare('site_id',$this->site_id);
		$criteria->compare('portal_id',$this->portal_id);
		$criteria->compare('access_point_group_id',$this->access_point_group_id);
		$criteria->compare('access_point_id',$this->access_point_id);
		$criteria->compare('device_id',$this->device_id);
		$criteria->compare('location_id',$this->location_id);
		$criteria->compare('ip_address',$this->ip_address,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('connection_token',$this->connection_token,true);
		$criteria->compare('level',$this->level);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_log;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DeviceConnection the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Get count of all unique users
	 * 
	 * @param date $date
	 * @param int $site_id
	 * @param int $portal_id
	 * @param int $apg_id
	 * @param int $ap_id
	 * @return int
	 */
	public static function pollAll($date, $site_id = 0, $portal_id = 0, $apg_id = 0, $ap_id = 0)
	{
		$where = 1;
		
		// site ID
		if ($site_id) {
			$where = "site_id = {$site_id}"; 
		}
		else {
			$where = "site_id IS NOT NULL";
		}
		
		// portal ID
		if ($portal_id) {
			$where .= " AND portal_id = {$portal_id}"; 
		}
		else {
			$where .= " AND portal_id IS NOT NULL";
		}
		
		// AP Group ID
		if ($apg_id) {
			$where .= " AND access_point_group_id = {$apg_id}"; 
		}
		else {
			$where .= " AND access_point_group_id IS NOT NULL";
		}
		
		// AP ID
		if ($ap_id) {
			$where .= " AND access_point_id = {$ap_id}"; 
		}
		else {
			$where .= " AND access_point_id IS NOT NULL";
		}
		
		$count = Yii::app()->db_log->createCommand()
			->select('COUNT(DISTINCT d.mac_address) as count')
			->from("device_connection_{$date} dc")
			->join('Unifi_Captive.device d', 'd.device_id = dc.device_id')
			->where($where)
			->queryRow();
		
		return (int)$count['count'];
	}
	
	/**
	 * Get count of all unique users
	 * 
	 * @param date $date
	 * @param int $site_id
	 * @param int $portal_id
	 * @param int $apg_id
	 * @param int $ap_id
	 * @return int
	 */
	public static function pollAllNew($date, $site_id = 0, $portal_id = 0, $apg_id = 0, $ap_id = 0)
	{
		$format_date = date('Y-m-d', strtotime($date));
		$where = "DATE(d.created_at) = '{$format_date}'";
		
		// site ID
		if ($site_id) {
			$where .= " AND site_id = {$site_id}"; 
		}
		else {
			$where .= " AND site_id IS NOT NULL";
		}
		
		// portal ID
		if ($portal_id) {
			$where .= " AND portal_id = {$portal_id}"; 
		}
		else {
			$where .= " AND portal_id IS NOT NULL";
		}
		
		// AP Group ID
		if ($apg_id) {
			$where .= " AND access_point_group_id = {$apg_id}"; 
		}
		else {
			$where .= " AND access_point_group_id IS NOT NULL";
		}
		
		// AP ID
		if ($ap_id) {
			$where .= " AND access_point_id = {$ap_id}"; 
		}
		else {
			$where .= " AND access_point_id IS NOT NULL";
		}
		
		$count = Yii::app()->db_log->createCommand()
			->select('COUNT(DISTINCT d.mac_address) as count')
			->from("device_connection_{$date} dc")
			->join('Unifi_Captive.device d', 'd.device_id = dc.device_id')
			->where($where)
			->queryRow();
		
		return (int)$count['count'];
	}
	
	/**
	 * Get count of returning unique users
	 * 
	 * @param date $date
	 * @param int $nas_id
	 * @return int
	 */
	public static function pollAllReturning($date, $site_id = 0, $portal_id = 0, $apg_id = 0, $ap_id = 0)
	{
		$format_date = date('Y-m-d', strtotime($date));
		$where = "DATE(d.created_at) < '{$format_date}'";
		
		// site ID
		if ($site_id) {
			$where .= " AND site_id = {$site_id}"; 
		}
		else {
			$where .= " AND site_id IS NOT NULL";
		}
		
		// portal ID
		if ($portal_id) {
			$where .= " AND portal_id = {$portal_id}"; 
		}
		else {
			$where .= " AND portal_id IS NOT NULL";
		}
		
		// AP Group ID
		if ($apg_id) {
			$where .= " AND access_point_group_id = {$apg_id}"; 
		}
		else {
			$where .= " AND access_point_group_id IS NOT NULL";
		}
		
		// AP ID
		if ($ap_id) {
			$where .= " AND access_point_id = {$ap_id}"; 
		}
		else {
			$where .= " AND access_point_id IS NOT NULL";
		}
		
		$count = Yii::app()->db_log->createCommand()
			->select('COUNT(DISTINCT d.mac_address) as count')
			->from("device_connection_{$date} dc")
			->join('Unifi_Captive.device d', 'd.device_id = dc.device_id')
			->where($where)
			->queryRow();
		
		return (int)$count['count'];
	}
	
	/**
	 * Get count of all unique users
	 * 
	 * @param date $date
	 * @param int $site_id
	 * @param int $portal_id
	 * @param int $apg_id
	 * @param int $ap_id
	 * @return int
	 */
	public static function pollAuthAll($date, $site_id = 0, $portal_id = 0, $apg_id = 0, $ap_id = 0)
	{
		$where = "username <> '' AND password <> ''";
		
		// site ID
		if ($site_id) {
			$where .= " AND site_id = {$site_id}"; 
		}
		else {
			$where .= " AND site_id IS NOT NULL";
		}
		
		// portal ID
		if ($portal_id) {
			$where .= " AND portal_id = {$portal_id}"; 
		}
		else {
			$where .= " AND portal_id IS NOT NULL";
		}
		
		// AP Group ID
		if ($apg_id) {
			$where .= " AND access_point_group_id = {$apg_id}"; 
		}
		else {
			$where .= " AND access_point_group_id IS NOT NULL";
		}
		
		// AP ID
		if ($ap_id) {
			$where .= " AND access_point_id = {$ap_id}"; 
		}
		else {
			$where .= " AND access_point_id IS NOT NULL";
		}
		
		$count = Yii::app()->db_log->createCommand()
			->select('COUNT(DISTINCT d.mac_address) as count')
			->from("device_connection_{$date} dc")
			->join('Unifi_Captive.device d', 'd.device_id = dc.device_id')
			->where($where)
			->queryRow();
		
		return (int)$count['count'];
	}
	
	/**
	 * Get count of all unique users
	 * 
	 * @param date $date
	 * @param int $site_id
	 * @param int $portal_id
	 * @param int $apg_id
	 * @param int $ap_id
	 * @return int
	 */
	public static function pollAuthNew($date, $site_id = 0, $portal_id = 0, $apg_id = 0, $ap_id = 0)
	{
		$format_date = date('Y-m-d', strtotime($date));
		$where = "DATE(d.created_at) = '{$format_date}' AND username <> '' AND password <> ''";
		
		// site ID
		if ($site_id) {
			$where .= " AND site_id = {$site_id}"; 
		}
		else {
			$where .= " AND site_id IS NOT NULL";
		}
		
		// portal ID
		if ($portal_id) {
			$where .= " AND portal_id = {$portal_id}"; 
		}
		else {
			$where .= " AND portal_id IS NOT NULL";
		}
		
		// AP Group ID
		if ($apg_id) {
			$where .= " AND access_point_group_id = {$apg_id}"; 
		}
		else {
			$where .= " AND access_point_group_id IS NOT NULL";
		}
		
		// AP ID
		if ($ap_id) {
			$where .= " AND access_point_id = {$ap_id}"; 
		}
		else {
			$where .= " AND access_point_id IS NOT NULL";
		}
		
		$count = Yii::app()->db_log->createCommand()
			->select('COUNT(DISTINCT d.mac_address) as count')
			->from("device_connection_{$date} dc")
			->join('Unifi_Captive.device d', 'd.device_id = dc.device_id')
			->where($where)
			->queryRow();
		
		return (int)$count['count'];
	}
	
	/**
	 * Get count of returning unique users
	 * 
	 * @param date $date
	 * @param int $nas_id
	 * @return int
	 */
	public static function pollAuthReturning($date, $site_id = 0, $portal_id = 0, $apg_id = 0, $ap_id = 0)
	{
		$format_date = date('Y-m-d', strtotime($date));
		$where = "DATE(d.created_at) < '{$format_date}' AND username <> '' AND password <> ''";
		
		// site ID
		if ($site_id) {
			$where .= " AND site_id = {$site_id}"; 
		}
		else {
			$where .= " AND site_id IS NOT NULL";
		}
		
		// portal ID
		if ($portal_id) {
			$where .= " AND portal_id = {$portal_id}"; 
		}
		else {
			$where .= " AND portal_id IS NOT NULL";
		}
		
		// AP Group ID
		if ($apg_id) {
			$where .= " AND access_point_group_id = {$apg_id}"; 
		}
		else {
			$where .= " AND access_point_group_id IS NOT NULL";
		}
		
		// AP ID
		if ($ap_id) {
			$where .= " AND access_point_id = {$ap_id}"; 
		}
		else {
			$where .= " AND access_point_id IS NOT NULL";
		}
		
		$count = Yii::app()->db_log->createCommand()
			->select('COUNT(DISTINCT d.mac_address) as count')
			->from("device_connection_{$date} dc")
			->join('Unifi_Captive.device d', 'd.device_id = dc.device_id')
			->where($where)
			->queryRow();
		
		return (int)$count['count'];
	}

	public static function getDCByDate($date, $portal_id, $device_id){
//
		$count = Yii::app()->db_log->createCommand()
			->select('ssid')
			->from("device_connection_{$date}")
			// ->join('Unifi_Captive.device_availment da', 'dc.device_id = da.device_id')
			// ->join('Unifi_Captive.device_scenario ds', 'da.availment_id = ds.availment_id')
			// ->join('Unifi_CMS.portal p', 'dc.portal_id = p.portal_id')
			// ->join('Unifi_Captive.device d', ' dc.`device_id` = d.`device_id`')
			// ->join('Unifi_Captive.avail_mobile m', ' da.`device_availment_id` = m.device_availment_id')
			->where("portal_id = {$portal_id} AND device_id = {$device_id}")
			->queryRow();

		return $count;

	}

}
