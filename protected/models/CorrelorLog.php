<?php

/**
 * This is the model class for table "correlor".
 *
 * The followings are the available columns in table 'correlor':
 * @property integer $correlor_log_id
 * @property integer $nas_id
 * @property integer $site_id
 * @property integer $portal_id
 * @property integer $access_point_group_id
 * @property integer $access_point_id
 * @property integer $device_id
 * @property string $url
 * @property string $request
 * @property string $response
 * @property integer $status
 * @property string $created_at
 */
class CorrelorLog extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'correlor_'.date('Ymd');
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('nas_id, site_id, portal_id, access_point_group_id, access_point_id, device_id, url, request, response, status, created_at', 'required'),
			array('nas_id, site_id, portal_id, access_point_group_id, access_point_id, device_id, status', 'numerical', 'integerOnly'=>true),
			array('url', 'length', 'max'=>128),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('correlor_log_id, nas_id, site_id, portal_id, access_point_group_id, access_point_id, device_id, url, request, response, status, created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'correlor_log_id' => 'Correlor Log',
			'nas_id' => 'NAS',
			'site_id' => 'Site',
			'portal_id' => 'Portal',
			'access_point_group_id' => 'Access Point Group',
			'access_point_id' => 'Access Point',
			'device_id' => 'Device',
			'url' => 'URL',
			'request' => 'Request',
			'response' => 'Response',
			'status' => 'Status',
			'created_at' => 'Date Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('correlor_log_id',$this->correlor_log_id);
		$criteria->compare('nas_id',$this->nas_id);
		$criteria->compare('site_id',$this->site_id);
		$criteria->compare('portal_id',$this->portal_id);
		$criteria->compare('access_point_group_id',$this->access_point_group_id);
		$criteria->compare('access_point_id',$this->access_point_id);
		$criteria->compare('device_id',$this->device_id);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('request',$this->request,true);
		$criteria->compare('response',$this->response,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_at',$this->created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_log;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Correlor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
