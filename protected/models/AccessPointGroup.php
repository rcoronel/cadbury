<?php

/**
 * This is the model class for table "access_point_group".
 *
 * The followings are the available columns in table 'access_point_group':
 * @property integer $access_point_group_id
 * @property integer $portal_id
 * @property string $name
 * @property string $logon_role
 * @property string $fb_role
 * @property string $auth_role
 * @property integer $is_active
 * @property string $created_at
 * @property string $updated_at
 */
class AccessPointGroup extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'access_point_group';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('portal_id, name, is_active, created_at, updated_at', 'required'),
			array('access_point_group_id, portal_id, is_active', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>64),
			array('name', 'unique'),
			array('logon_role, fb_role, auth_role', 'length', 'max'=>32),
			array('created_at, updated_at', 'date', 'format'=>'yyyy-mm-dd hh:mm:ss'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'access_point_group_id'=>'ID',
			'portal_id'=>'Portal',
			'name'=>'Name',
			'logon_role'=>'Role (login)',
			'fb_role'=>'Role (Facebook access)',
			'auth_role'=>'Role (authenticated)',
			'is_active'=>'Status',
			'created_at'=>'Date added',
			'updated_at'=>'Date updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('access_point_group_id',$this->access_point_group_id);
		$criteria->compare('portal_id',$this->portal_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('is_active',$this->is_active);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cms;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AccessPointGroup the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
