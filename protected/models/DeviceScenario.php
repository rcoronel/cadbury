<?php

/**
 * This is the model class for table "device_scenario".
 *
 * The followings are the available columns in table 'device_scenario':
 * @property integer $device_scenario_id
 * @property integer $availment_id
 * @property integer $portal_id
 * @property integer $device_id
 * @property integer $level
 * @property string $scene
 * @property string $created_at
 * @property string $updated_at
 */
class DeviceScenario extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'device_scenario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('availment_id, portal_id, device_id, level, scene, created_at, updated_at', 'required'),
			array('device_scenario_id, availment_id, portal_id, device_id, level', 'numerical', 'integerOnly'=>true),
			array('scene', 'length', 'max'=>10),
			array('created_at, updated_at', 'date', 'format'=>'yyyy-MM-dd hh:mm:ss'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'device_scenario_id' => 'ID',
			'availment_id' => 'Availment',
			'portal_id' => 'Portal',
			'device_id' => 'Device',
			'level' => 'Level',
			'scene' => 'Scene',
			'created_at' => 'Date Added',
			'updated_at' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('device_scenario_id',$this->device_scenario_id);
		$criteria->compare('availment_id',$this->availment_id);
		$criteria->compare('portal_id',$this->portal_id);
		$criteria->compare('device_id',$this->device_id);
		$criteria->compare('level',$this->level);
		$criteria->compare('scene',$this->scene,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cp;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DeviceScenario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
