<?php

/**
 * This is the model class for table "admin".
 *
 * The followings are the available columns in table 'admin':
 * @property integer $admin_id
 * @property integer $admin_role_id
 * @property string $email
 * @property string $password
 * @property string $firstname
 * @property string $lastname
 * @property string $image
 * @property integer $is_active
 * @property string $created_at
 * @property string $updated_at
 */
class Admin extends ActiveRecord
{
	
	// string confirm password
	public $confirm_password;
	
	// string new password
	public $new_password;
	
	// bool cookie login
	public $cookie_login;
	
	// object identity
	private $_identity;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'admin';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			
			// INSERT scenario
			array('admin_role_id, email, password, firstname, lastname, is_active, created_at, updated_at', 'required', 'on'=>'insert'),
			array('email', 'unique', 'on'=>'insert'),
			array('confirm_password', 'compare', 'on' => 'insert', 'compareAttribute' => 'password'),
			//array('admin_role_id', 'AdminRoleValidator', 'on' => 'insert'),
			//array('Is_active', 'AdminActiveValidator', 'on' => 'insert'),
			
			// UPDATE scenario
			array('admin_id, admin_role_id, email, firstname, lastname, is_active, updated_at', 'required', 'on' => 'update'),
			array('email', 'unique', 'on' => 'update'),
			array('admin_role_id', 'AdminRoleValidator', 'on' => 'update'),
			array('is_active', 'AdminActiveValidator' , 'on' => 'update'),
			
			// LOGIN scenario
			array('email, password', 'required', 'on' => 'login'),
			
			// CHANGE PASSWORD scenario
			array('admin_id, password, new_password, confirm_password', 'required', 'on'=>'change_password'),
			array('confirm_password', 'compare', 'on' => 'change_password', 'compareAttribute' => 'new_password'),
			array('new_password', 'length', 'min'=>6),
			
			// UPDATE IMAGE
			array('admin_id', 'required', 'on' => 'update_image'),
			array('image', 'file', 'allowEmpty'=>true, 'types'=>'jpg, jpeg, png, gif', 'on' => 'update_image'),
			
			// ALL scenario
			array('admin_id, admin_role_id, is_active', 'numerical', 'integerOnly'=>true),
			array('email', 'email'),
			array('email, password', 'length', 'max'=>128, 'min'=>6),
			array('firstname, lastname', 'length', 'max'=>24),
			array('image', 'length', 'max'=>64),
			array('created_at, updated_at', 'date', 'format'=>'yyyy-mm-dd hh:mm:ss'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'role'=>array(self::BELONGS_TO, 'AdminRole', array('admin_role_id'=>'admin_role_id'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'admin_id' => 'ID',
			'admin_role_id' => 'Role',
			'email' => 'E-mail Address',
			'password' => 'Password',
			'firstname' => 'First name',
			'lastname' => 'Last name',
			'image' => 'Image',
			'is_active' => 'Status',
			'created_at' => 'Date Added',
			'updated_at' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('admin_id',$this->admin_id);
		$criteria->compare('admin_role_id',$this->admin_role_id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('firstname',$this->firstname,true);
		$criteria->compare('lastname',$this->lastname,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('is_active',$this->is_active);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Admin the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Login the admin
	 * 
	 * @access	public
	 * @return void
	 */
	public function login()
	{
		// No identity set
		if ($this->_identity === NULL) {
			$this->_identity = new AdminIdentity($this->email, $this->password);
		}
		
		// Authenticate identity
		$auth = $this->_identity->authenticate();
		
		if ( ! $auth)
		{
			switch ($this->_identity->errorCode)
			{
				case 1:
					$this->addError('Email', $this->_identity->errorMessage);
				break;
			
				case 2:
					$this->addError('Password', $this->_identity->errorMessage);
				break;
			}
			return FALSE;
		}
		else
		{
			$duration = 0;
			if ($this->cookie_login) {
				$duration = 3600*24*30; #30 days
			}
			
			Yii::app()->getModule('backoffice')->admin->login($this->_identity, $duration);
			
			// Context object
			$context = Context::getContext();
			$context->admin = Admin::model()->findByPk($this->_identity->getId());
			
			return true;
		}
	}
	
	/**
	 * Change Password
	 * 
	 * @param string $pw Current hashed Password
	 * @return bool
	 */
	public function change_Password($pw = '')
	{
		// check if correct current Password
		if ( ! CPasswordHelper::verifyPassword($this->password, $pw))
		{
			$this->addError('current_Password', 'Supplied current Password in incorrect');
			return FALSE;
		}
		
		// check for Password
		if (CPasswordHelper::verifyPassword($this->new_password, $pw))
		{
			$this->addError('password', 'New Password cannot be the same as the current Password');
			return FALSE;
		}
		
		return TRUE;
	}
	
	public function getName()
	{
		return $this->firstname . ' ' . $this->lastname;
	}
}
