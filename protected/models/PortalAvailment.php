<?php

/**
 * This is the model class for table "portal_availment".
 *
 * The followings are the available columns in table 'portal_availment':
 * @property integer $portal_availment_id
 * @property integer $portal_id
 * @property integer $availment_id
 * @property integer $duration
 * @property integer $is_recurring
 * @property string $description
 */
class PortalAvailment extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'portal_availment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('portal_id, availment_id', 'required'),
			array('portal_id, availment_id, duration, is_recurring', 'numerical', 'integerOnly'=>true),
			array('description', 'type', 'type'=>'string'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('portal_availment_id, portal_id, availment_id, duration, is_recurring, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'availment'=>array(self::BELONGS_TO, 'Availment', 'availment_id', 'together'=>TRUE),
			//'nas'=>array(self::BELONGS_TO, 'RadiusNas', array('Portal_ID'=>'id'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'portal_availment_id' => 'ID',
			'portal_id' => 'Portal',
			'availment_id' => 'Availament',
			'duration' => 'Duration',
			'is_recurring' => 'Availment method count',
			'description' => 'Description for text box',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('portal_availment_id',$this->portal_availment_id);
		$criteria->compare('portal_id',$this->portal_id);
		$criteria->compare('availment_id',$this->availment_id);
		$criteria->compare('duration',$this->duration);
		$criteria->compare('is_recurring',$this->is_recurring);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cms;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PortalAvailment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
