<?php

/**
 * This is the model class for table "promo".
 *
 * The followings are the available columns in table 'promo':
 * @property integer $promo_id
 * @property integer $promo_type_id
 * @property integer $account_type_id
 * @property integer $subscription_type_id
 * @property integer $service_type_id
 * @property integer $currency_id
 * @property string $code
 * @property string $name
 * @property string $description
 * @property string $valid_from
 * @property string $valid_until
 * @property integer $download_speed
 * @property integer $upload_speed
 * @property double $amount
 * @property integer $duration
 * @property integer $is_displayed
 * @property string $created_at
 * @property string $updated_at
 */
class Promo extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'promo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('promo_type_id, account_type_id, subscription_type_id, service_type_id, currency_id, code, name, description, valid_from, valid_until, download_speed, upload_speed, amount, duration, is_displayed, created_at, updated_at', 'required'),
			array('promo_type_id, account_type_id, subscription_type_id, service_type_id, currency_id, download_speed, upload_speed, duration, is_displayed', 'numerical', 'integerOnly'=>true),
			array('amount', 'numerical'),
			array('code', 'length', 'max'=>16),
			array('name', 'length', 'max'=>128),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('promo_id, promo_type_id, account_type_id, subscription_type_id, service_type_id, currency_id, code, name, description, valid_from, valid_until, download_speed, upload_speed, amount, duration, is_displayed, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'promo_id' => 'ID',
			'promo_type_id' => 'Promo Type',
			'account_type_id' => 'Account Type',
			'subscription_type_id' => 'Subscription Type',
			'service_type_id' => 'Service Type',
			'currency_id' => 'Currency',
			'code' => 'Code',
			'name' => 'Name',
			'description' => 'Description',
			'valid_from' => 'Valid From',
			'valid_until' => 'Valid Until',
			'download_speed' => 'Download speed limit (kbps)',
			'upload_speed' => 'Upload speed limit (kbps)',
			'amount' => 'Price',
			'duration' => 'Duration',
			'is_displayed' => 'Is Displayed',
			'created_at' => 'Date Added',
			'updated_at' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('promo_id',$this->promo_id);
		$criteria->compare('promo_type_id',$this->promo_type_id);
		$criteria->compare('account_type_id',$this->account_type_id);
		$criteria->compare('subscription_type_id',$this->subscription_type_id);
		$criteria->compare('service_type_id',$this->service_type_id);
		$criteria->compare('currency_id',$this->currency_id);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('valid_from',$this->valid_from,true);
		$criteria->compare('valid_until',$this->valid_until,true);
		$criteria->compare('download_speed',$this->download_speed);
		$criteria->compare('upload_speed',$this->upload_speed);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('duration',$this->duration);
		$criteria->compare('is_displayed',$this->is_displayed);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cms;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Promo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
