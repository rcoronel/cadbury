<?php

/**
 * This is the model class for table "portal_menu".
 *
 * The followings are the available columns in table 'portal_menu':
 * @property integer $portal_menu_id
 * @property integer $parent_portal_menu_id
 * @property string $title
 * @property string $classname
 * @property string $img
 * @property integer $display
 * @property integer $position
 * @property string $created_at
 * @property string $updated_at
 */
class PortalMenu extends ActiveRecord
{
	// int Has Subpages
	public $has_subpages;
	
	public $view;
	public $add;
	public $edit;
	public $delete;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'portal_menu';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('parent_portal_menu_id, title, classname, display, created_at, updated_at', 'required'),
			array('portal_menu_id, parent_portal_menu_id, display, position', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>32),
			array('classname', 'length', 'max'=>16),
			array('img', 'length', 'max'=>128),
			array('created_at, updated_at', 'date', 'format'=>'yyyy-mm-dd hh:mm:ss'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'portal'=>array(self::BELONGS_TO, 'Portal', array('portal_id'=>'portal_id')),
			'permission'=>array(self::HAS_ONE, 'PortalRolePermission', array('portal_menu_id'=>'portal_menu_id'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'portal_menu_id' => 'ID',
			'parent_portal_menu_id' => 'Parent Menu',
			'title' => 'Title',
			'classname' => 'Classname',
			'img' => 'Image',
			'display' => 'display',
			'position' => 'Position',
			'created_at' => 'Date Added',
			'updated_at' => 'Date Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridview, CListview or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('portal_menu_id',$this->portal_menu_id);
		$criteria->compare('parent_portal_menu_id',$this->parent_portal_menu_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('classname',$this->classname,true);
		$criteria->compare('img',$this->img,true);
		$criteria->compare('display',$this->display);
		$criteria->compare('position',$this->position);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cms;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PortalMenu the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public static function getMainMenus()
	{
		$context = Context::getContext();
		
		$pages = PortalMenu::model()->with('permission')->findAll(
					array(	'order' => 'position', 'condition' => 'portal_role_id = :role_id AND view = 1 AND display = 1 AND parent_portal_menu_id = 0',
							'params' => array(':role_id' => $context->portal_admin->portal_role_id)));
		return $pages;
	}
	
	public static function getSubMenus()
	{
		$context = Context::getContext();
		$pages = PortalMenu::model()->with('permission')->findAll(
					array(	'order' => 'position', 'condition' => 'portal_role_id = :role_id AND view = 1 AND display = 1 AND parent_portal_menu_id != 0',
							'params' => array(':role_id' => $context->portal_admin->portal_role_id)));
		return $pages;
	}
}
