<?php

class TableCommand extends CConsoleCommand
{
	public function actionCreate($from = '', $to = '')
	{
		// Check if date range is given
		if (empty($from)) {
			$from = date('Y-m-d');
			$to = date('Y-m-d', strtotime("+1 days"));
		}
		
		while (strtotime($from) <= strtotime($to)) {
			$table_dc = "device_connection_".$from;
			$table_ca = "connection_avail_".$from;
			$table_al = "aruba_log_".$from;
			$table_cl = "correlor_log_".$from;
			$table_ct = "connection_attempt_".$from;
			
			if (Yii::app()->db_cp->schema->getTable($table_dc,true)===null) {
				DeviceConnection::createTable($table_dc);
			}
			
			if (Yii::app()->db_cp->schema->getTable($table_ca,true)===null) {
				ConnectionAvail::createTable($table_ca);
			}
			
			if (Yii::app()->db_cp->schema->getTable($table_al,true)===null) {
				ArubaLog::createTable($table_al);
			}
			
			if (Yii::app()->db_cp->schema->getTable($table_cl,true)===null) {
				CorrelorLog::createTable($table_cl);
			}

			if (Yii::app()->db_cp->schema->getTable($table_ct,true)===null) {
				ConnectionAttempt::createTable($table_ct);
			}
			
			// increment $from
			$from = date('Y-m-d', strtotime($from . "+1 days"));
		}
	}
}