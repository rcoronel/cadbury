<?php

class ConnectionCommand extends CConsoleCommand
{
	public function actionCreate($from = '', $to = '')
	{
		// Check if date range is given
		if (empty($from)) {
			$from = date('Y-m-d', strtotime("yesterday"));
			$to = date('Y-m-d', strtotime("yesterday"));
		}
		
		while (strtotime($from) <= strtotime($to)) {
			$table = "device_connection_".$from;
			
			if (Yii::app()->db_cp->schema->getTable($table,true)) {
				$nas = RadiusNas::model()->findAll();
				foreach ($nas as $n) {
					DeviceConnectionCount::poll($table, $from, $n->id);
				}
			}
			
			// increment $from
			$from = date('Y-m-d', strtotime($from . "+1 days"));
		}
	}
}