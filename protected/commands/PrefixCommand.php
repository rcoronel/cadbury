<?php

class PrefixCommand extends CConsoleCommand
{
	public function actionCreate()
	{
		
		$path = Yii::app()->basePath.'/migrations/files/prefix.csv';
		// $file = CFile::getInstance($path);
		$handle = fopen($path,'r');
		// print_r($file);
		if ($handle) {
	        while(!feof($handle)) {
	           $record = fgetcsv($handle);
	           $prefix = PlanPrefix::model()->findByAttributes(array('prefix'=>$record[0]));
	           // print_r($prefix);
	           if(!$prefix && $record[0] != ""){

	           		echo '_____________________________________'.$record[0].'_____________________________________';
					echo "\n";

	           		$plan_prefix = new PlanPrefix;
	           		$plan_prefix->range_id = 0;
	           		$plan_prefix->prefix = $record[0];
	           		$plan_prefix->plan_id = (trim($record[1] == "TM" ? 2 : 3));
	           		$plan_prefix->date_added = date("Y-m-d H:i:s");
	           		$plan_prefix->save();

					sleep(1);

	           }else{
	           		echo '_____________________________________existing:'.$record[0].'_____________________________________';
					echo "\n";
	           }
			   
			}

		    fclose($handle);
		}

	}

}