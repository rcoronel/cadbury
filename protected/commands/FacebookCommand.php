<?php

class FacebookCommand extends CConsoleCommand
{
	public function actionCreate($from = '', $to = '')
	{
		// Check if date range is given
		if (empty($from)) {
			$from = date('Y-m-d');
			$to = date('Y-m-d', strtotime("+1 days"));
		}
		
		while (strtotime($from) <= strtotime($to)) {
			$table = "correlor_log_$from";
			echo '_____________________________________'.$table.'_____________________________________';
			echo "\n";
			
			$tokens = CorrelorLog::getToken($table);
			foreach ($tokens as $token) {
				$link = curl_init("https://graph.facebook.com/me?access_token={$token['accessToken']}");
				curl_setopt($link, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($link, CURLOPT_RETURNTRANSFER, true);
				$data = curl_exec($link);
				curl_close($link);
				
				$decode_data = CJSON::decode($data);
				if (isset($decode_data['id'])) {
					$fb_user = AvailFacebook::model()->findByAttributes(array('Fb_ID'=>$decode_data['id'], 'Device_ID'=>$token['Device_ID'], 'Nas_ID'=>$token['Nas_ID']));
					if (empty($fb_user)) {
						$fb_user = new AvailFacebook();
						$fb_user->Nas_ID = $token['Nas_ID'];
						$fb_user->Fb_ID = $decode_data['id'];
						$fb_user->Device_ID = $token['Device_ID'];
						$fb_user->Email = (isset($decode_data['email'])) ? $decode_data['email'] : '';
						$fb_user->Firstname = $decode_data['first_name'];
						$fb_user->Lastname = $decode_data['last_name'];
						$fb_user->Gender = (isset($decode_data['gender'])) ? $decode_data['gender'] : '';
						$fb_user->Access_token = $token['accessToken'];
						$fb_user->Access_expiry = $token['accessTokenExpires'];
						$fb_user->Created_at = $token['Created_at'];
						$fb_user->Correlor_ID = $decode_data['id'].'@fb';
						$fb_user->validate() && $fb_user->save();
						if ( ! empty($fb_user->errors)) {
							echo "<pre>";
							print_r($fb_user);
							print_r($fb_user->errors);
							die();
						}
					}
				}
				
				print_r($decode_data);
				echo "\n";
			}

			// increment $from
			$from = date('Y-m-d', strtotime($from . "+1 days"));
			sleep(30);
		}
	}
	
	public function actionPicture()
	{
		$users = AvailFacebook::model()->findAll("picture = '' ORDER BY created_at DESC LIMIT 0, 10000");
		$count = 1;
		foreach ($users as $u) {

			//$link = curl_init("https://graph.facebook.com/me?access_token={$u->access_token}");
			$link = curl_init('https://graph.facebook.com/'.$u->fb_id.'/picture?type=large&redirect=FALSE');
			curl_setopt($link, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($link, CURLOPT_RETURNTRANSFER, true);
			$data = curl_exec($link);
			curl_close($link);
			$decode_data = CJSON::decode($data);
			
			echo "[$count] $u->fb_id ";
			if (isset($decode_data['error'])) {
				echo " ERROR \n";
			}
			else{
				$u->picture = $decode_data['data']['url'];
				$u->validate() && $u->save();
				echo "\n";
			}
			$count++;
		}
		die();
	}
	
	public function actionTest()
	{
		$offset = 0;
		do {
			$users = AvailFacebook::model()->findAll(array('condition'=>'fb_id = 2147483647', 'limit'=>10000, 'offset'=>$offset));
			foreach ($users as $u) {
				$link = curl_init("https://graph.facebook.com/me?access_token={$u->access_token}");
				curl_setopt($link, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($link, CURLOPT_RETURNTRANSFER, true);
				$data = curl_exec($link);
				curl_close($link);

				$decode_data = CJSON::decode($data);
				if (isset($decode_data['id'])) {
					$u->fb_id = $decode_data['id'];
					$u->save(FALSE);
					echo "{$u->fb_id}\n";
				}
				else {
					print_r($decode_data);
					
					echo "{$u->fb_id} | ERROR \n";
				}
			}
			$offset = $offset + 10000;
		} while($offset < 1000000);
	}
}