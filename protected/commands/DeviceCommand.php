<?php

class DeviceCommand extends CConsoleCommand
{
	public function actionCreate($from = '', $to = '')
	{
		// Check if date range is given
		if (empty($from)) {
			$from = date('Y-m-d', strtotime("yesterday"));
			$to = date('Y-m-d', strtotime("yesterday"));
		}
		
		while (strtotime($from) <= strtotime($to)) {
			$nas = RadiusNas::model()->findAll();
			foreach ($nas as $n) {
				DeviceCount::poll($from, $n->id);
			}
			
			// increment $from
			$from = date('Y-m-d', strtotime($from . "+1 days"));
		}
	}
	
	public function actionTest()
	{
		$criteria = new CDbCriteria();
		$criteria->limit = 1000;
		$criteria->addCondition('Correlor_ID IS NULL');
		$devices = Device::model()->findAll($criteria);
		foreach ($devices as $d) {
			Correlor::userAnonymous($d->Device_ID, 'Device');
			echo $d->Device_ID;
			echo "<br>";
		}
		die('OMG');
	}
}