<?php

class ConvertCommand extends CConsoleCommand
{
	public function actionDevice()
	{
		$offset = 0;
		do {
			$users = Device_old::model()->findAll(array('limit'=>10000, 'offset'=>$offset));
			foreach ($users as $u) {
				$device = new Device;
				$device->device_id = $u->Device_ID;
				$device->mac_address = $u->Mac_address;
				$device->created_at = $u->Created_at;
				$device->updated_at = $u->Updated_at;
				//$device->validate() && $device->save();
				$device->save(FALSE);
				echo "{$u->Device_ID} | {$device->device_id} \n";
			}
			$offset = $offset + 10000;
		} while($offset < 1000000);
	}
	
	public function actionEasy()
	{
		$offset = 0;
		do {
			$users = AvailEasy_old::model()->findAll(array('limit'=>10000, 'offset'=>$offset));
			foreach ($users as $u) {
				$object = new DeviceAvailment;
				$object->availment_id = 6;
				switch ($u->Nas_ID) {
					// Glorietta
					case 1:
						$object->nas_id = 1;
						$object->site_id = 2;
						$object->portal_id = 1;
						$object->device_id = $u->Device_ID;
						$object->access_point_group_id = 0;
						$object->access_point_id = 0;
					break;
				
					// Laboracay
					case 4:
						$object->nas_id = 4;
						$object->site_id = 4;
						$object->portal_id = 5;
						$object->device_id = $u->Device_ID;
						$object->access_point_group_id = 0;
						$object->access_point_id = 0;
					break;
				
					// Trinoma
					case 5:
						$object->nas_id = 5;
						$object->site_id = 2;
						$object->portal_id = 2;
						$object->device_id = $u->Device_ID;
						$object->access_point_group_id = 0;
						$object->access_point_id = 0;
					break;
				
					// BHS
					case 6:
						$object->nas_id = 6;
						$object->site_id = 2;
						$object->portal_id = 6;
						$object->device_id = $u->Device_ID;
						$object->access_point_group_id = 0;
						$object->access_point_id = 0;
					break;
				
					// Eastwood
					case 7:
						$object->nas_id = 7;
						$object->site_id = 2;
						$object->portal_id = 7;
						$object->device_id = $u->Device_ID;
						$object->access_point_group_id = 0;
						$object->access_point_id = 0;
					break;
				
					// LCTM
					case 8:
						$object->nas_id = 8;
						$object->site_id = 3;
						$object->portal_id = 4;
						$object->device_id = $u->Device_ID;
						$object->access_point_group_id = 0;
						$object->access_point_id = 0;
					break;
				}
				
				$object->created_at = $u->Created_at;
				$object->save(FALSE);
				
				echo "{$object->device_availment_id} \n";
				
			}
			$offset = $offset + 10000;
		} while($offset < 1000000);
	}
	
	public function actionFacebook()
	{
		$offset = 0;
		do {
			$new_users = AvailFacebook::model()->findAllByAttributes(array('fb_id'=>'2147483647'), array('limit'=>10000, 'offset'=>$offset));
			foreach ($new_users as $nu) {
				$user = AvailFacebook_old::model()->findByAttributes(array('Access_token'=>$nu->access_token));
				if ($user) {
					$nu->fb_id = $user->Fb_ID;
					$nu->save(FALSE);
					echo "$nu->fb_id \n";
				}
				else {
					echo "$nu->fb_id | ERROR \n";
				}
			}
			$offset = $offset + 10000;
		} while($offset < 1000000);
		
//		$offset = 0;
//		do {
//			$users = AvailFacebook_old::model()->findAll(array('limit'=>10000, 'offset'=>$offset));
//			foreach ($users as $u) {
//				$object = new DeviceAvailment;
//				$object->availment_id = 1;
//				switch ($u->Nas_ID) {
//					// Glorietta
//					case 1:
//						$object->nas_id = 1;
//						$object->site_id = 2;
//						$object->portal_id = 1;
//						$object->device_id = $u->Device_ID;
//						$object->access_point_group_id = 0;
//						$object->access_point_id = 0;
//					break;
//				
//					// Laboracay
//					case 4:
//						$object->nas_id = 4;
//						$object->site_id = 4;
//						$object->portal_id = 5;
//						$object->device_id = $u->Device_ID;
//						$object->access_point_group_id = 0;
//						$object->access_point_id = 0;
//					break;
//				
//					// Trinoma
//					case 5:
//						$object->nas_id = 5;
//						$object->site_id = 2;
//						$object->portal_id = 2;
//						$object->device_id = $u->Device_ID;
//						$object->access_point_group_id = 0;
//						$object->access_point_id = 0;
//					break;
//				
//					// BHS
//					case 6:
//						$object->nas_id = 6;
//						$object->site_id = 2;
//						$object->portal_id = 6;
//						$object->device_id = $u->Device_ID;
//						$object->access_point_group_id = 0;
//						$object->access_point_id = 0;
//					break;
//				
//					// Eastwood
//					case 7:
//						$object->nas_id = 7;
//						$object->site_id = 2;
//						$object->portal_id = 7;
//						$object->device_id = $u->Device_ID;
//						$object->access_point_group_id = 0;
//						$object->access_point_id = 0;
//					break;
//				
//					// LCTM
//					case 8:
//						$object->nas_id = 8;
//						$object->site_id = 3;
//						$object->portal_id = 4;
//						$object->device_id = $u->Device_ID;
//						$object->access_point_group_id = 0;
//						$object->access_point_id = 0;
//					break;
//				}
//				
//				$object->created_at = $u->Created_at;
//				$object->save(FALSE);
//				
//				$new_user = new AvailFacebook;
//				$new_user->device_availment_id = $object->device_availment_id;
//				$new_user->fb_id = $u->Fb_ID;
//				$new_user->firstname = $u->Firstname;
//				$new_user->lastname = $u->Lastname;
//				$new_user->email = $u->Email;
//				$new_user->gender = $u->Gender;
//				$new_user->access_token = $u->Access_token;
//				$new_user->access_expiry = $u->Access_expiry;
//				$new_user->created_at = $u->Created_at;
//				$new_user->updated_at = $u->Updated_at;
//				$new_user->save(FALSE);
//				echo "{$u->Fb_ID} | {$object->device_availment_id} | $new_user->fb_id \n";
//				
//			}
//			$offset = $offset + 10000;
//		} while($offset < 1000000);
	}
	
	public function actionFree()
	{
		$offset = 0;
		do {
			$users = AvailFree_old::model()->findAll(array('limit'=>10000, 'offset'=>$offset));
			foreach ($users as $u) {
				$object = new DeviceAvailment;
				$object->availment_id = 99;
				switch ($u->Nas_ID) {
					// Glorietta
					case 1:
						$object->nas_id = 1;
						$object->site_id = 2;
						$object->portal_id = 1;
						$object->device_id = $u->Device_ID;
						$object->access_point_group_id = 0;
						$object->access_point_id = 0;
					break;
				
					// Laboracay
					case 4:
						$object->nas_id = 4;
						$object->site_id = 4;
						$object->portal_id = 5;
						$object->device_id = $u->Device_ID;
						$object->access_point_group_id = 0;
						$object->access_point_id = 0;
					break;
				
					// Trinoma
					case 5:
						$object->nas_id = 5;
						$object->site_id = 2;
						$object->portal_id = 2;
						$object->device_id = $u->Device_ID;
						$object->access_point_group_id = 0;
						$object->access_point_id = 0;
					break;
				
					// BHS
					case 6:
						$object->nas_id = 6;
						$object->site_id = 2;
						$object->portal_id = 6;
						$object->device_id = $u->Device_ID;
						$object->access_point_group_id = 0;
						$object->access_point_id = 0;
					break;
				
					// Eastwood
					case 7:
						$object->nas_id = 7;
						$object->site_id = 2;
						$object->portal_id = 7;
						$object->device_id = $u->Device_ID;
						$object->access_point_group_id = 0;
						$object->access_point_id = 0;
					break;
				
					// LCTM
					case 8:
						$object->nas_id = 8;
						$object->site_id = 3;
						$object->portal_id = 4;
						$object->device_id = $u->Device_ID;
						$object->access_point_group_id = 0;
						$object->access_point_id = 0;
					break;
				}
				
				$object->created_at = $u->Created_at;
				$object->save(FALSE);
				
				echo "{$object->device_availment_id} \n";
				
			}
			$offset = $offset + 10000;
		} while($offset < 1000000);
	}
	
	public function actionMobile()
	{
		$offset = 0;
		do {
			$users = AvailMobile_old::model()->findAll(array('limit'=>10000, 'offset'=>$offset));
			foreach ($users as $u) {
				$object = new DeviceAvailment;
				$object->availment_id = 3;
				switch ($u->Nas_ID) {
					// Glorietta
					case 1:
						$object->nas_id = 1;
						$object->site_id = 2;
						$object->portal_id = 1;
						$object->device_id = $u->Device_ID;
						$object->access_point_group_id = 0;
						$object->access_point_id = 0;
					break;
				
					// Laboracay
					case 4:
						$object->nas_id = 4;
						$object->site_id = 4;
						$object->portal_id = 5;
						$object->device_id = $u->Device_ID;
						$object->access_point_group_id = 0;
						$object->access_point_id = 0;
					break;
				
					// Trinoma
					case 5:
						$object->nas_id = 5;
						$object->site_id = 2;
						$object->portal_id = 2;
						$object->device_id = $u->Device_ID;
						$object->access_point_group_id = 0;
						$object->access_point_id = 0;
					break;
				
					// BHS
					case 6:
						$object->nas_id = 6;
						$object->site_id = 2;
						$object->portal_id = 6;
						$object->device_id = $u->Device_ID;
						$object->access_point_group_id = 0;
						$object->access_point_id = 0;
					break;
				
					// Eastwood
					case 7:
						$object->nas_id = 7;
						$object->site_id = 2;
						$object->portal_id = 7;
						$object->device_id = $u->Device_ID;
						$object->access_point_group_id = 0;
						$object->access_point_id = 0;
					break;
				
					// LCTM
					case 8:
						$object->nas_id = 8;
						$object->site_id = 3;
						$object->portal_id = 4;
						$object->device_id = $u->Device_ID;
						$object->access_point_group_id = 0;
						$object->access_point_id = 0;
					break;
				}
				
				$object->created_at = $u->Created_at;
				$object->save(FALSE);
				
				$new_user = new AvailMobile;
				$new_user->device_availment_id = $object->device_availment_id;
				$new_user->msisdn = $u->Mobile_number;
				$new_user->auth_code = $u->Auth_code;
				$new_user->is_validated = $u->Is_Validated;
				$new_user->created_at = $u->Created_at;
				$new_user->save(FALSE);
				echo "{$new_user->msisdn} | {$object->device_availment_id} \n";
			}
			$offset = $offset + 10000;
		} while($offset < 1000000);
	}
	
	public function actionRegister()
	{
		$offset = 0;
		do {
			$users = AvailRegistration_old::model()->findAll(array('limit'=>10000, 'offset'=>$offset));
			foreach ($users as $u) {
				$object = new DeviceAvailment;
				$object->availment_id = 2;
				switch ($u->Nas_ID) {
					// Glorietta
					case 1:
						$object->nas_id = 1;
						$object->site_id = 2;
						$object->portal_id = 1;
						$object->device_id = $u->Device_ID;
						$object->access_point_group_id = 0;
						$object->access_point_id = 0;
					break;
				
					// Laboracay
					case 4:
						$object->nas_id = 4;
						$object->site_id = 4;
						$object->portal_id = 5;
						$object->device_id = $u->Device_ID;
						$object->access_point_group_id = 0;
						$object->access_point_id = 0;
					break;
				
					// Trinoma
					case 5:
						$object->nas_id = 5;
						$object->site_id = 2;
						$object->portal_id = 2;
						$object->device_id = $u->Device_ID;
						$object->access_point_group_id = 0;
						$object->access_point_id = 0;
					break;
				
					// BHS
					case 6:
						$object->nas_id = 6;
						$object->site_id = 2;
						$object->portal_id = 6;
						$object->device_id = $u->Device_ID;
						$object->access_point_group_id = 0;
						$object->access_point_id = 0;
					break;
				
					// Eastwood
					case 7:
						$object->nas_id = 7;
						$object->site_id = 2;
						$object->portal_id = 7;
						$object->device_id = $u->Device_ID;
						$object->access_point_group_id = 0;
						$object->access_point_id = 0;
					break;
				
					// LCTM
					case 8:
						$object->nas_id = 8;
						$object->site_id = 3;
						$object->portal_id = 4;
						$object->device_id = $u->Device_ID;
						$object->access_point_group_id = 0;
						$object->access_point_id = 0;
					break;
				}
				
				$object->created_at = $u->Created_at;
				$object->save(FALSE);
				
				$new_user = new AvailRegistration;
				$new_user->device_availment_id = $object->device_availment_id;
				$new_user->firstname = $u->F_name;
				$new_user->lastname = $u->L_name;
				$new_user->email = $u->Email;
				$new_user->birthday = $u->Birthday;
				$new_user->gender = $u->Gender;
				$new_user->created_at = $u->Created_at;
				$new_user->save(FALSE);
				echo "{$new_user->email} | {$object->device_availment_id} \n";
			}
			$offset = $offset + 10000;
		} while($offset < 1000000);
	}
	
	public function actionSurvey()
	{
		$offset = 0;
		do {
			$users = AvailSurvey_old::model()->findAll(array('limit'=>10000, 'offset'=>$offset));
			foreach ($users as $u) {
				$object = new DeviceAvailment;
				$object->availment_id = 5;
				switch ($u->Nas_ID) {
					// Glorietta
					case 1:
						$object->nas_id = 1;
						$object->site_id = 2;
						$object->portal_id = 1;
						$object->device_id = $u->Device_ID;
						$object->access_point_group_id = 0;
						$object->access_point_id = 0;
					break;
				
					// Laboracay
					case 4:
						$object->nas_id = 4;
						$object->site_id = 4;
						$object->portal_id = 5;
						$object->device_id = $u->Device_ID;
						$object->access_point_group_id = 0;
						$object->access_point_id = 0;
					break;
				
					// Trinoma
					case 5:
						$object->nas_id = 5;
						$object->site_id = 2;
						$object->portal_id = 2;
						$object->device_id = $u->Device_ID;
						$object->access_point_group_id = 0;
						$object->access_point_id = 0;
					break;
				
					// BHS
					case 6:
						$object->nas_id = 6;
						$object->site_id = 2;
						$object->portal_id = 6;
						$object->device_id = $u->Device_ID;
						$object->access_point_group_id = 0;
						$object->access_point_id = 0;
					break;
				
					// Eastwood
					case 7:
						$object->nas_id = 7;
						$object->site_id = 2;
						$object->portal_id = 7;
						$object->device_id = $u->Device_ID;
						$object->access_point_group_id = 0;
						$object->access_point_id = 0;
					break;
				
					// LCTM
					case 8:
						$object->nas_id = 8;
						$object->site_id = 3;
						$object->portal_id = 4;
						$object->device_id = $u->Device_ID;
						$object->access_point_group_id = 0;
						$object->access_point_id = 0;
					break;
				}
				
				$object->created_at = $u->Created_at;
				$object->save(FALSE);
				
				$new_user = new AvailSurvey;
				$new_user->device_availment_id = $object->device_availment_id;
				$new_user->avail_survey_id = $u->Avail_Survey_ID;
				$new_user->survey_id = $u->Survey_ID;
				$new_user->title = $u->Title;
				$new_user->created_at = $u->Created_at;
				$new_user->save(FALSE);
				echo "{$new_user->title} | {$object->device_availment_id} \n";
			}
			$offset = $offset + 10000;
		} while($offset < 1000000);
	}
	
	public function actionQuestion()
	{
		$offset = 0;
		do {
			$users = AvailSurveyQuestion_old::model()->findAll(array('limit'=>10000, 'offset'=>$offset));
			foreach ($users as $u) {
				$new_user = new AvailSurveyQuestion;
				$new_user->avail_question_id = $u->Avail_survey_question_ID;
				$new_user->avail_survey_id = $u->Avail_survey_ID;
				$new_user->question = $u->Question;
				$new_user->created_at = $u->Created_at;
				$new_user->save(FALSE);
				echo "{$new_user->question} | {$new_user->avail_survey_id} \n";
			}
			$offset = $offset + 10000;
		} while($offset < 1000000);
	}
	
	public function actionAnswer()
	{
		$offset = 0;
		do {
			$users = AvailSurveyAnswer_old::model()->findAll(array('limit'=>10000, 'offset'=>$offset));
			foreach ($users as $u) {
				$new_user = new AvailSurveyAnswer;
				$new_user->avail_answer_id = $u->Avail_survey_answer_ID;
				$new_user->avail_question_id = $u->Avail_survey_question_ID;
				$new_user->answer = $u->Answer;
				$new_user->created_at = $u->Created_at;
				$new_user->save(FALSE);
				echo "{$new_user->answer} | {$new_user->avail_question_id} \n";
			}
			$offset = $offset + 10000;
		} while($offset < 1000000);
	}
}