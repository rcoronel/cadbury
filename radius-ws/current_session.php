<?php

require_once 'lib/Database.php';
require_once 'lib/Constants.php';


final class RadiusWS{
    private $db = NULL;
    
    public function __construct() {
        date_default_timezone_set('Asia/Manila'); // must be synchronus with app server's date
        $this->db = new Database(Constants::DB_TYPE, Constants::DB_HOST, Constants::DB_NAME, Constants::DB_USER, Constants::DB_PASS);
        $this->start(); // start process
    }
    
    public function start() {
        
        # catch post request from client - nest
        if (isset($_GET['username'])) {
            $username = $_GET['username'];
        
            $query = "SELECT radacctid, acctsessiontime, username, availment_id "
                    . "FROM radacct WHERE username = '$username'"
                    . " AND DATE(acctstarttime) = DATE(NOW()) AND availment_id = 0";

            $result = $this->db->select($query);
            header("Content-Type: application/json");
            echo json_encode($result);
        } 
    }
    
}

new RadiusWS();
