<?php

require_once 'lib/Database.php';
require_once 'lib/Constants.php';


final class RadiusWS{
    private $db = NULL;
    
    public function __construct() {
        date_default_timezone_set('Asia/Manila'); // must be synchronus with app server's date
        $this->db = new Database(Constants::DB_TYPE, Constants::DB_HOST, Constants::DB_NAME, Constants::DB_USER, Constants::DB_PASS);
        $this->start(); // start process
    }
    
    public function start() {
        
        # catch post request from client - nest
        if (isset($_GET['availment_id']) && isset($_GET['username'])) {
            $availment_id = (int) $_GET['availment_id'];
            $username = $_GET['username'];

            $query = "SELECT IFNULL(SUM(acctsessiontime),0) as current_session_time,username as user_name, availment_id"
                . " FROM radacct WHERE username = '$username'"
                . " AND availment_id = '$availment_id' AND DATE(acctstarttime) = DATE(NOW())";
            
            $result = $this->db->select($query);
            
            header("Content-Type: application/json");
            echo json_encode($result);
        }
    }
    
}

new RadiusWS();