<?php
require_once 'lib/Database.php';
require_once 'lib/Constants.php';


final class RadiusWS{
    private $db = NULL;
    
    public function __construct() {
        date_default_timezone_set('Asia/Manila'); // must be synchronus with app server's date
        $this->db = new Database(Constants::DB_TYPE, Constants::DB_HOST, Constants::DB_NAME, Constants::DB_USER, Constants::DB_PASS);
        $this->start(); // start process
    }
    
    public function start() {
        
        # catch post request from client - nest
        if (isset($_GET['username'])) {
            $username = $_GET['username'];
            $date = date("Y-m-d");
            
            $query = "SELECT IFNULL(time_to_sec(timediff(date_add(MAX(a.acctstarttime),INTERVAL b.value SECOND), DATE_SUB(now(),INTERVAL 0 SECOND))) "
                    ."- (SELECT IFNULL(SUM(acctsessiontime),0) FROM Cadbury_Radius.radacct WHERE DATE(acctstarttime) = DATE(NOW()) AND "
                    ."username = '$username' AND availment_id = "
                    ."(SELECT IFNULL(b.Availment_ID,0) FROM Cadbury_Captive.`device_connection_$date` as a "
                    ."LEFT JOIN Cadbury_Captive.`connection_avail_$date` as b ON a.Device_connection_ID = b.Device_connection_ID " 
                    ."WHERE username = '$username' ORDER BY b.Connection_avail_ID DESC LIMIT 1)) "
                    .",0) as sec_rem "
                    ."FROM Cadbury_Radius.radacct as a LEFT JOIN Cadbury_Radius.radreply as b ON a.username = b.username "
                    ."WHERE a.username = '$username' AND a.acctstoptime IS NULL LIMIT 1;";

            $result = $this->db->select($query);
            header("Content-Type: application/json");
            echo json_encode((int) $result[0]['sec_rem']);
        } 
    }
    
}

new RadiusWS();
