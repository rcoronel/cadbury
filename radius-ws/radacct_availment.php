<?php

require_once 'lib/Database.php';
require_once 'lib/Constants.php';


final class RadiusWS{
    private $db = NULL;
    
    public function __construct() {
        date_default_timezone_set('Asia/Manila'); // must be synchronus with app server's date
        $this->db = new Database(Constants::DB_TYPE, Constants::DB_HOST, Constants::DB_NAME, Constants::DB_USER, Constants::DB_PASS);
        $this->start(); // start process
    }
    
    public function start() {
        
        # catch post request from client - nest
        if (isset($_GET['availment_id']) && isset($_GET['radacct_id'])) {
            $availment_id = (int) $_GET['availment_id'];
            $radacct_id = (int) $_GET['radacct_id'];

            $result = $this->db->updateSingle(Constants::RADACCT,"availment_id = ".$availment_id, "radacctid = ".$radacct_id);
            if ($result) {
                $result = array("code" => 200, "message" => "success");
            } else {
                $result = array("code" => 500, "message" => "internal server error");
            }
            
            header("Content-Type: application/json");
            echo json_encode($result);
        }
    }
    
}

new RadiusWS();