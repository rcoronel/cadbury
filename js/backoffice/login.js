var boLogin = boLogin || {};;
(function($, window, undefined) {
    "use strict";
    $(document).ready(function() {
        boLogin.$container = $("#form-login");
        boLogin.$container.validate({
            rules: {
                'Admin[Email]': {
                    required: true,
					email: true
                },
                'Admin[Password]': {
                    required: true
                }
            },
            highlight: function(element) {
                $(element).closest('.input-group').addClass('validate-has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.input-group').removeClass('validate-has-error');
            },
            submitHandler: function(ev) {
                $(".login-page").addClass('logging-in');
				$('div#form-login-error').slideUp('fast');
                setTimeout(function() {
                    var random_pct = 25 + Math.round(Math.random() * 30);
                    boLogin.setPercentage(40 + random_pct);
                    $.ajax({
                        url: currentIndex,
                        method: 'POST',
                        dataType: 'json',
                        data: $("form#form-login").serialize(),
                        error: function() {
                            alert("An error occoured!");
                        },
                        success: function(response) {
                            var login_status = response.status;
                            boLogin.setPercentage(100);
                            setTimeout(function() {
                                if ( ! login_status) {
                                    $(".login-page").removeClass('logging-in');
									$('div#form-login-error').html(response.message);
									$('div#form-login-error').show();
                                    boLogin.resetProgressBar(true);
                                } else {
									setTimeout(function() {
                                        window.location.href = response.redirect;
                                    }, 400);
								}
                            }, 1000);
                        }
                    });
                }, 650);
            }
        });
        var is_lockscreen = $(".login-page").hasClass('is-lockscreen');
        if (is_lockscreen) {
            boLogin.$container = $("#form_lockscreen");
            boLogin.$ls_thumb = boLogin.$container.find('.lockscreen-thumb');
            boLogin.$container.validate({
                rules: {
                    password: {
                        required: true
                    }
                },
                highlight: function(element) {
                    $(element).closest('.input-group').addClass('validate-has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.input-group').removeClass('validate-has-error');
                },
                submitHandler: function(ev) {
                    $(".login-page").addClass('logging-in-lockscreen');
                    setTimeout(function() {
                        var random_pct = 25 + Math.round(Math.random() * 30);
                        boLogin.setPercentage(random_pct, function() {
                            setTimeout(function() {
                                boLogin.setPercentage(100, function() {
                                    setTimeout("window.location.href = '../../'", 600);
                                }, 2);
                            }, 820);
                        });
                    }, 650);
                }
            });
        }
        boLogin.$body = $(".login-page");
        boLogin.$login_progressbar_indicator = $(".login-progressbar-indicator h3");
        boLogin.$login_progressbar = boLogin.$body.find(".login-progressbar div");
        boLogin.$login_progressbar_indicator.html('0%');
        if (boLogin.$body.hasClass('login-form-fall')) {
            var focus_set = false;
            setTimeout(function() {
                boLogin.$body.addClass('login-form-fall-init')
                setTimeout(function() {
                    if (!focus_set) {
                        boLogin.$container.find('input:first').focus();
                        focus_set = true;
                    }
                }, 550);
            }, 0);
        } else {
            boLogin.$container.find('input:first').focus();
        }
        boLogin.$container.find('.form-control').each(function(i, el) {
            var $this = $(el),
                $group = $this.closest('.input-group');
            $this.prev('.input-group-addon').click(function() {
                $this.focus();
            });
            $this.on({
                focus: function() {
                    $group.addClass('focused');
                },
                blur: function() {
                    $group.removeClass('focused');
                }
            });
        });
        $.extend(boLogin, {
            setPercentage: function(pct, callback) {
                pct = parseInt(pct / 100 * 100, 10) + '%';
                if (is_lockscreen) {
                    boLogin.$lockscreen_progress_indicator.html(pct);
                    var o = {
                        pct: currentProgress
                    };
                    TweenMax.to(o, .7, {
                        pct: parseInt(pct, 10),
                        roundProps: ["pct"],
                        ease: Sine.easeOut,
                        onUpdate: function() {
                            boLogin.$lockscreen_progress_indicator.html(o.pct + '%');
                            drawProgress(parseInt(o.pct, 10) / 100);
                        },
                        onComplete: callback
                    });
                    return;
                }
                boLogin.$login_progressbar_indicator.html(pct);
                boLogin.$login_progressbar.width(pct);
                var o = {
                    pct: parseInt(boLogin.$login_progressbar.width() / boLogin.$login_progressbar.parent().width() * 100, 10)
                };
                TweenMax.to(o, .7, {
                    pct: parseInt(pct, 10),
                    roundProps: ["pct"],
                    ease: Sine.easeOut,
                    onUpdate: function() {
                        boLogin.$login_progressbar_indicator.html(o.pct + '%');
                    },
                    onComplete: callback
                });
            },
            resetProgressBar: function(display_errors) {
                TweenMax.set(boLogin.$container, {
                    css: {
                        opacity: 0
                    }
                });
                setTimeout(function() {
                    TweenMax.to(boLogin.$container, .6, {
                        css: {
                            opacity: 1
                        },
                        onComplete: function() {
                            boLogin.$container.attr('style', '');
                        }
                    });
                    boLogin.$login_progressbar_indicator.html('0%');
                    boLogin.$login_progressbar.width(0);
                    if (display_errors) {
                        var $errors_container = $(".form-login-error");
                        $errors_container.show();
                        var height = $errors_container.outerHeight();
                        $errors_container.css({
                            height: 0
                        });
                        TweenMax.to($errors_container, .45, {
                            css: {
                                height: height
                            },
                            onComplete: function() {
                                $errors_container.css({
                                    height: 'auto'
                                });
                            }
                        });
                        boLogin.$container.find('input[type="password"]').val('');
                    }
                }, 800);
            }
        });
        if (is_lockscreen) {
            boLogin.$lockscreen_progress_canvas = $('<canvas></canvas>');
            boLogin.$lockscreen_progress_indicator = boLogin.$container.find('.lockscreen-progress-indicator');
            boLogin.$lockscreen_progress_canvas.appendTo(boLogin.$ls_thumb);
            var thumb_size = boLogin.$ls_thumb.width();
            boLogin.$lockscreen_progress_canvas.attr({
                width: thumb_size,
                height: thumb_size
            });
            boLogin.lockscreen_progress_canvas = boLogin.$lockscreen_progress_canvas.get(0);
            var bg = boLogin.lockscreen_progress_canvas,
                ctx = ctx = bg.getContext('2d'),
                imd = null,
                circ = Math.PI * 2,
                quart = Math.PI / 2,
                currentProgress = 0;
            ctx.beginPath();
            ctx.strokeStyle = '#eb7067';
            ctx.lineCap = 'square';
            ctx.closePath();
            ctx.fill();
            ctx.lineWidth = 3.0;
            imd = ctx.getImageData(0, 0, thumb_size, thumb_size);
            var drawProgress = function(current) {
                ctx.putImageData(imd, 0, 0);
                ctx.beginPath();
                ctx.arc(thumb_size / 2, thumb_size / 2, 70, -(quart), ((circ) * current) - quart, false);
                ctx.stroke();
                currentProgress = current * 100;
            }
            drawProgress(0 / 100);
            boLogin.$lockscreen_progress_indicator.html('0%');
            ctx.restore();
        }
    });
})(jQuery, window);