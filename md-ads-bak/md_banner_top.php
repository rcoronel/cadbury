<?php
session_start();
$show = false;

//$cookie = "3c:47:11:75:67:ad@172.122.0.254";
if (isset($_COOKIE['MacAddress'])) {
    $cookie = $_COOKIE['MacAddress'];
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://localhost/unifi/radius-ws/end_sessiontime.php?username='.$cookie);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $contents = curl_exec($ch);
    $show = true;
} elseif (isset($_SESSION['MacAddress'])) {
    $cookie = $_SESSION['MacAddress'];
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://localhost/unifi/radius-ws/end_sessiontime.php?username='.$cookie);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $contents = curl_exec($ch);
    $show = true;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Notification Sample</title>
        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
<meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="//ajax.aspnetcdn.com/ajax/jQuery/jquery-1.10.2.min.js"></script>
        <script src="//ajax.aspnetcdn.com/ajax/jquery.ui/1.10.3/jquery-ui.min.js"></script>
        <script src="jquery.countdown.min.js"></script>

        <style type='text/css'>
            /*reset css*/
            * {
                margin: 0;
                padding: 0;
                border: 0;
                font-family: sans-serif;
                font-size: 1em;
                font-weight: normal;
                font-style: normal;
                text-decoration: none;
                background: none;
            }
            html{
                overflow: hidden;
            }

            #webflowadapternotificationframe2 {
                background: none repeat scroll 0 0 #fff;
                left: 0 !important;
                position: absolute !important;
                top: 0 !important;
                width: 99.9% !important;
		/*z-index: 1000000 !important;*/
                height: 100%;
                display: table;
            }
            #webflowadapternotificationcontent2 {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }
            .buttons{
                border: 1px solid #C3C3C3;
                font-family: monospace;
                padding: 2px 5px 2px 5px;
                text-decoration: none;
                cursor: pointer;
                background-color: wheat;
                position: fixed;
                top: 0;
                right: 0;
            }
            .buttons:hover{
                background: none repeat scroll 0 0 #C3C3C3;
                cursor: pointer;
            }
            #timer-cont {
                background-color: #000;
                color: yellow;
            }
            .timer-label {
                font-weight: bold;
            }
            
        </style>

        <script type='text/javascript'>
            // <!--
            // This script defines the interactions with the banner
            // Use jquery for the display, avoiding to conflict with web libraries

            jq = jQuery.noConflict();
            function log(event) {
                jq.ajax({
                    url: "/notificationcontent?content=notificationiframe&log=" + event,
                    cache: false,
                    async: false
                });
            }

            var win = parent;

            jq(document).ready(function () {
                <?php
                if($show) {
                    ?>
                var d = new Date();
                d.setTime(d.getTime() + parseInt(<?php echo $contents;?>)*1000);
                jq('#timer').countdown(d, function(event) {
                    jq(this).html(event.strftime('%H:%M:%S'));
                    jq("#timer-cont").show();
                }).on('finish.countdown',function(e){
                    //jq("#timer-cont").hide();
                });
                <?php
                }
                ?>
                try {
                    // Defines interactions
                    jq('#webflowadapternotificationaclose2').click(function () {
                        log("Click on notification close Button");
                        jq('#webflowadapternotificationframe2').fadeOut('slow', function () {
                            try {
                                win.postMessage("close2", "*");
                            } catch (err) {
                                alert(err);
                            }
                        });
                        return false;
                    });
                    // When document is ready, display the iframe
                    win.postMessage("show2", "*");
                } catch (globalerr) {
                    // In case of script issue, don't generate errors that would
                    // bother end-user - just exit silently
                }
                jq("#webflowadapternotificationcontent2 .ads-cont img").load(function() {
                    var h = jq(this).height();
                    //win.postMessage({size : h + 'px'}, "*");
                });
                
                var s = document.createElement("script");
                s.type = "text/javascript";
                
                if(true) {
                    s.src = "http://tags.mathtag.com/ad/js/1989/1917?pmp=8169&click=&rfr={http://mediadonuts.com/backupold/bannerpreview/globe/}&random={1}";
                } else if(screen.width <= 480) {
                    s.src = "http://tags.mathtag.com/ad/js/1989/1917?pmp=8118&click=&rfr={http://mediadonuts.com/backupold/bannerpreview/globe/}&random={1}";
                } else {
                    alert(screen.width);
                }
                //jq(".ads-cont").append(s);
            });

            // --> 
        </script>
    </head>
    <body>
        <div id='webflowadapternotificationframe2'>
            <div id='webflowadapternotificationcontent2'>
                <table cellpadding="0" cellspacing="0" border="0" style="margin: 0 auto;">
                    <tr>
                        <td class="ads-cont">
                            <script language='JavaScript' src='http://tags.mathtag.com/ad/js/1989/1917?pmp=8169&click=&rfr={http://mediadonuts.com/backupold/bannerpreview/globe/}&random={1}'></script>
                        </td>
                        <td id="timer-cont">
                            <table cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td>Timer</td>
                                </tr>
                                <tr>
                                    <td><span id='timer'>00:00:00</span></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <span class='buttons' id='webflowadapternotificationaclose2'>X</span>
    </body>
</html>
