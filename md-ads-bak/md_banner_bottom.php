<?php
$show = false;

//$cookie = "3c:47:11:75:67:ad@172.122.0.254";
//if (isset($_COOKIE['MacAddress'])) {
//    $cookie = $_COOKIE['MacAddress'];
//    $ch = curl_init();
//    curl_setopt($ch, CURLOPT_URL, 'http://localhost/unifi/radius-ws/end_sessiontime.php?username='.$cookie);
//    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//    $contents = curl_exec($ch);
//    $show = true;
//}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Notification Sample</title>
        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
<meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="//ajax.aspnetcdn.com/ajax/jQuery/jquery-1.10.2.min.js"></script>
        <script src="//ajax.aspnetcdn.com/ajax/jquery.ui/1.10.3/jquery-ui.min.js"></script>
        <script src="jquery.countdown.min.js"></script>

        <style type='text/css'>
            /*reset css*/
            * {
                margin: 0;
                padding: 0;
                border: 0;
                font-family: sans-serif;
                font-size: 1em;
                font-weight: normal;
                font-style: normal;
                text-decoration: none;
                background: none;
            }
            html{
                overflow: hidden;
            }

            #webflowadapternotificationframe {
                background: none repeat scroll 0 0 #fff;
                left: 0 !important;
                position: absolute !important;
                top: 0 !important;
                width: 99.9% !important;
                z-index: 1000000 !important;
                height: 100%;
                display: table;
            }
            #webflowadapternotificationcontent {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }
            .buttons{
                border: 1px solid #C3C3C3;
                font-family: monospace;
                padding: 2px 5px 2px 5px;
                text-decoration: none;
                cursor: pointer;
                background-color: wheat;
                position: fixed;
                top: 0;
                right: 0;
            }
            .buttons:hover{
                background: none repeat scroll 0 0 #C3C3C3;
                cursor: pointer;
            }
            
            #timer {
                left: 0 !important;
                right: auto !important;
                display: none;
            }
        </style>

        <script type='text/javascript'>
            // <!--
            // This script defines the interactions with the banner
            // Use jquery for the display, avoiding to conflict with web libraries

            jq = jQuery.noConflict();
            function log(event) {
                jq.ajax({
                    url: "/notificationcontent?content=notificationiframe&log=" + event,
                    cache: false,
                    async: false
                });
            }

            var win = parent;

            jq(document).ready(function () {
                <?php
                if($show) {
                    ?>
                var d = new Date();
                d.setSeconds(d.getSeconds() + parseInt(<?php echo $contents;?>));
                jq('#timer').countdown(d, function(event) {
                    jq(this).html(event.strftime('%H:%M:%S'));
                    jq(this).show();
                });
                <?php
                }
                ?>
                try {
                    // Defines interactions
                    jq('#webflowadapternotificationaclose').click(function () {
                        log("Click on notification close Button");
                        jq('#webflowadapternotificationframe').fadeOut('slow', function () {
                            try {
                                win.postMessage("close", "*");
                            } catch (err) {
                                alert(err);
                            }
                        });
                        return false;
                    });
                    // When document is ready, display the iframe
                    win.postMessage("show", "*");
                } catch (globalerr) {
                    // In case of script issue, don't generate errors that would
                    // bother end-user - just exit silently
                }
                jq("#webflowadapternotificationcontent img").load(function() {
                    var h = jq(this).height();
                    win.postMessage({size : h + 'px'}, "*");
                });
            });

            // --> 
        </script>
    </head>
    <body>
        <div id='webflowadapternotificationframe'>
            <div id='webflowadapternotificationcontent'>
                <script>
                    var img1 = "468x60.gif";
                    var img2 = "280x45.gif";
                    var src;
                    if(screen.width <= 360) {
                        src = img2;
                    } else {
                        src = img2;
                    }

                    var img = new Image();
                    img.onload = function()
                    {
                        // Load completed
                        document.getElementById("ads").src = this.src;
                    };
                    img.src = src;
                </script>
                <a href="http://www.globe.com.ph/" target="_blank"><img id="ads" alt="ads"></img></a>
                <!--<script language='JavaScript' src='http://tags.mathtag.com/ad/js/1989/1917?pmp=8120&click=&rfr={http://mediadonuts.com/backupold/bannerpreview/globe/}random={1}'></script>-->
                <span class='buttons' id='webflowadapternotificationaclose'>X</span>
                <span class='buttons' id='timer'></span>
            </div>
        </div>
    </body>
</html>
