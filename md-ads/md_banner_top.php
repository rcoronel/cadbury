<?php
$show = false;
$showTimer = true;

//$cookie = "3c:47:11:75:67:ad@172.122.0.254";
if (isset($_COOKIE['MacAddress']) && $showTimer) {
    $cookie = $_COOKIE['MacAddress'];
   
    if (!isset($_COOKIE['WifiExpiry'])) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://localhost/unifi/radius-ws/end_sessiontime.php?username='.$cookie);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $contents = curl_exec($ch);
    }
    $show = true;
} 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Ads Banner</title>
        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
        <script src="js/jquery-1.11.3.min.js"></script>
        <script src="js/jquery.countdown.min.js"></script>
        <script src="js/script.js"></script>
        <link rel="stylesheet" type="text/css" href="css/style.min.css">
        <script type='text/javascript'>
            jq = jQuery.noConflict();
            var win = parent;

            jq(document).ready(function () {
                <?php
                if($show) {
                    ?>
                var d = new Date();
                var expiry = getCookie("WifiExpiry");
                
                if (expiry != "") {
                    d = expiry;
                } else {
                    d.setTime(d.getTime() + parseInt(<?php echo $contents;?>)*1000);
                    setCookie("WifiExpiry", d, parseInt(<?php echo $contents;?>));
                }
                jq('#timer').countdown(Date.parse(d), function(event) {
                    jq(this).html(event.strftime('%H:%M:%S'));
                    jq("#timer-cont").show();
                }).on('finish.countdown',function(e){
                    jq("#timer-cont").hide();
                });
                <?php
                } else {
                ?>
                    jq("#timer-cont").hide();
                <?php
                }
                ?>
                try {
                    // Defines interactions
                    jq('#banner-top-close').click(function () {
                        try {
                            win.postMessage("closeTop", "*");
                        } catch (err) {
                            alert(err);
                        }
                        return false;
                    });
                    // When document is ready, display the iframe
                    win.postMessage("showTop", "*");
                } catch (globalerr) {}
            });
        </script>
    </head>
    <body>
        <div id='webflowadapternotificationframe2'>
            <div id='webflowadapternotificationcontent2'>
                <table cellpadding="0" cellspacing="0" border="0" style="margin: 0 auto;">
                    <tr>
                        <td class="ads-cont">
                            <?php
                            if (isset($_GET['bannerId'])){
                                switch ($_GET['bannerId']) {
                                    case 1:
                                        ?>
                                        <script language='JavaScript' src='http://tags.mathtag.com/ad/js/1989/1917?pmp=8169&click=&rfr={http://mediadonuts.com/backupold/bannerpreview/globe/}&random={1}'></script>
                                        <?php
                                        break;
                                    case 2:
                                        ?>
                                        <script language='JavaScript' src='http://tags.mathtag.com/ad/js/1989/1917?pmp=8118&click=&rfr={http://mediadonuts.com/backupold/bannerpreview/globe/}&random={1}'></script>
                                        <?php
                                        break;
                                    case 3:
                                        ?>
                                        <script language='JavaScript' src='http://tags.mathtag.com/ad/js/1989/1917?pmp=8120&click=&rfr={http://mediadonuts.com/backupold/bannerpreview/globe/}&random={1}'></script>
                                        <?php
                                        break;
                                }
                            }
                            ?>
                        </td>
                        <td id="timer-cont">
                            <table cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td>Timer</td>
                                </tr>
                                <tr>
                                    <td><span id='timer'>00:00:00</span></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <span class='buttons' id='banner-top-close'>X</span>
    </body>
</html>
