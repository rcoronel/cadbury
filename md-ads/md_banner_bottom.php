<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Ads Banner</title>
        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
        <script src="js/jquery-1.11.3.min.js"></script>
        <script src="js/jquery.countdown.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/style.min.css">
        <script type='text/javascript'>
            jq = jQuery.noConflict();
            var win = parent;

            jq(document).ready(function () {
                try {
                    // Defines interactions
                    jq('#banner-bottom-close').click(function () {
                        try {
                            win.postMessage("closeBottom", "*");
                        } catch (err) {
                            alert(err);
                        }
                        return false;
                    });
                    // When document is ready, display the iframe
                    win.postMessage("showBottom", "*");
                } catch (globalerr) {}
            });
        </script>
    </head>
    <body>
        <div id='webflowadapternotificationframe2'>
            <div id='webflowadapternotificationcontent2'>
                <table cellpadding="0" cellspacing="0" border="0" style="margin: 0 auto;">
                    <tr>
                        <td class="ads-cont">
                            <?php
                            if (isset($_GET['bannerId'])){
                                switch ($_GET['bannerId']) {
                                    case 1:
                                        ?>
                                        <script language='JavaScript' src='http://tags.mathtag.com/ad/js/1989/1917?pmp=8169&click=&rfr={http://mediadonuts.com/backupold/bannerpreview/globe/}&random={1}'></script>
                                        <?php
                                        break;
                                    case 2:
                                        ?>
                                        <script language='JavaScript' src='http://tags.mathtag.com/ad/js/1989/1917?pmp=8118&click=&rfr={http://mediadonuts.com/backupold/bannerpreview/globe/}&random={1}'></script>
                                        <?php
                                        break;
                                    case 3:
                                        ?>
                                        <script language='JavaScript' src='http://tags.mathtag.com/ad/js/1989/1917?pmp=8120&click=&rfr={http://mediadonuts.com/backupold/bannerpreview/globe/}&random={1}'></script>
                                        <?php
                                        break;
                                }
                            }
                            ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <span class='buttons' id='banner-bottom-close'>X</span>
    </body>
</html>
